$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2012-2013 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2012-2013 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2012-2013 by FUJITSU VLSI LIMITED, JAPAN
$  Copyright (C) 2012-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2012-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2012-2013 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2012-2013 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2012-2013 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2012-2013 by Witz Corporation, JAPAN
$  Copyright (C) 2013 by Embedded and Real-Time Systems Laboratory
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: prc.tf 189 2015-06-26 01:54:57Z t_ishikawa $
$

$
$     パス2のアーキテクチャ依存テンプレート（V850用）
$

$ 
$  kernel/kernel.tf のターゲット依存部
$ 

$ 
$  ユーザスタック領域を確保するコードを出力する
$  ARGV[1]：タスクID
$  ARGV[2]：スタックサイズ
$ 
$FUNCTION ALLOC_USTACK$
    StackType _kernel_ustack_$ARGV[1]$[COUNT_STK_T($ARGV[2]$)]
    $SPC$__attribute__((section($FORMAT("\".user_stack.%s\"", ARGV[1])$)));$NL$

    $TSK.TINIB_USTKSZ[ARGV[1]] = VALUE(FORMAT("ROUND_STK_T(%s)", ARGV[2]), +ARGV[2])$
    $TSK.TINIB_USTK[ARGV[1]] = FORMAT("_kernel_ustack_%s", ARGV[1])$
$END$

$ 
$  ユーザスタック領域のセクション名を返す
$  ARGV[1]：タスクID
$ 
$FUNCTION SECTION_USTACK$
    $RESULT = FORMAT(".user_stack.%s", ARGV[1])$
$END$

$
$  ユーザスタックのアライメント制約に合わせたサイズを返す
$  ARGV[1]：スタックサイズ（アライン前）
$  kernel.tfから呼ばれる
$
$FUNCTION USTACK_ALIGN_SIZE$
	$RESULT = (ARGV[1] + CHECK_USTKSZ_ALIGN - 1) & ~(CHECK_USTKSZ_ALIGN - 1)$
$END$

$
$  基本タスクの共有スタックの確保
$  ARGV[1]：共有スタック名
$  ARGV[2]：共有スタックID(タスク優先度)
$  ARGV[3]：スタックサイズ(アライメント調整済み)
$  kernel.tfから呼ばれる
$
$FUNCTION ALLOC_SHARED_USTACK$
	StackType $ARGV[1]$[COUNT_STK_T(ROUND_STK_T($ARGV[3]$))]
	$SPC$__attribute__((section($FORMAT("\".shared_user_stack.%s\"", ARGV[2])$)));$NL$
$END$

$
$  基本タスクの共有スタックのセクション名
$  ARGV[1]：共有スタックID(タスク優先度)
$  kernel.tfから呼ばれる
$
$FUNCTION SECTION_SHARED_USTACK$
	$RESULT = FORMAT(".shared_user_stack.%s", ARGV[1])$
$END$

$
$  非信頼フックスタックの確保
$  ARGV[1]：スタック名
$  ARGV[2]：スタックサイズ
$  kernel.tfから呼ばれる
$
$FUNCTION ALLOC_HSTACK$
	$name = ARGV[1]$
	$size = ARGV[2]$
	StackType $name$[COUNT_STK_T(ROUND_STK_T($HSTACK_SIZE_STR(size)$))] __attribute__((section(".prsv_kernel")));$NL$
$END$

$
$  非信頼フックスタックサイズのアライメント制約を満たすように拡張したサイズの文字列
$  ARGV[1]：スタックサイズ
$  kernel.tfから呼ばれる
$
$FUNCTION HSTACK_SIZE_STR$
	TOPPERS_ROUND_SZ($ARGV[1]$, CHECK_USTKSZ_ALIGN)
$END$

$ 
$  OsMemoryArea はサポートしない
$ 
$FUNCTION HOOK_ERRORCHECK_MEM$
    $ERROR$
        OsMemoryArea is not supported.$NL$
    $END$
$END$

$ 
$  標準のセクションのメモリオブジェクト属性の定義
$ 
$MEMATR_TEXT = (TA_NOWRITE|TA_EXEC)$
$MEMATR_RODATA = (TA_NOWRITE|TA_EXEC)$
$MEMATR_DATA = TA_MEMINI$
$MEMATR_BSS = TA_NULL$
$MEMATR_PRSV = TA_MEMPRSV$
$MEMATR_ROSDATA = (TA_SDATA|TA_MEMINI|TA_NOWRITE)$
$MEMATR_SDATA = (TA_SDATA|TA_MEMINI)$
$MEMATR_SBSS = TA_SDATA$

$TARGET_MEMATR_USTACK = TA_NULL$

$ 
$  標準のセクションに関する定義
$  DSEC.ORDER_LIST：IDのリスト
$  DESC.SECTION：セクション名
$  DESC.MEMREG：セクションを配置するメモリリージョン
$  DESC.MEMATR：セクションのメモリオブジェクト属性
$ 
$DSEC.ORDER_LIST = RANGE(0,7)$

$DSEC.SECTION[0] = ".text"$
$DSEC.MEMREG[0] = 1$
$DSEC.MEMATR[0] = MEMATR_TEXT$

$DSEC.SECTION[1] = ".rodata"$
$DSEC.MEMREG[1] = 1$
$DSEC.MEMATR[1] = MEMATR_RODATA$

$DSEC.SECTION[2] = ".data"$
$DSEC.MEMREG[2] = 0$
$DSEC.MEMATR[2] = MEMATR_DATA$

$DSEC.SECTION[3] = ".bss"$
$DSEC.MEMREG[3] = 0$
$DSEC.MEMATR[3] = MEMATR_BSS$

$DSEC.SECTION[4] = ".prsv"$
$DSEC.MEMREG[4] = 0$
$DSEC.MEMATR[4] = MEMATR_PRSV$

$DSEC.SECTION[5] = ".rosdata"$
$DSEC.MEMREG[5] = 0$
$DSEC.MEMATR[5] = MEMATR_ROSDATA$

$DSEC.SECTION[6] = ".sdata"$
$DSEC.MEMREG[6] = 0$
$DSEC.MEMATR[6] = MEMATR_SDATA$

$DSEC.SECTION[7] = ".sbss"$
$DSEC.MEMREG[7] = 0$
$DSEC.MEMATR[7] = MEMATR_SBSS$

$ 
$  OSAP初期化コンテキストブロックのための宣言
$ 
$FUNCTION PREPARE_OSAPINICTXB$
    $IF LENGTH(TSK.ID_LIST)$
        $FOREACH tskid TSK.ID_LIST$
            $IF !OSAP.TRUSTED[TSK.OSAPID[tskid]]$
                extern uint8 $FORMAT("__start_user_stack%s", tskid)$;$NL$
                extern uint8 $FORMAT("__limit_user_stack%s", tskid)$;$NL$
            $END$
        $END$
    $END$
    $FOREACH tskpri RANGE(TMIN_TPRI, TMAX_TPRI)$
        $IF LENGTH(shared_ustack_size[tskpri])$
            extern uint8 $FORMAT("__start_shared_user_stack%s", tskpri)$;$NL$
            extern uint8 $FORMAT("__limit_shared_user_stack%s", tskpri)$;$NL$
        $END$
    $END$


    $IF LENGTH(OSAP.ID_LIST)$
        $FOREACH domid OSAP.ID_LIST$
            $IF !OSAP.TRUSTED[domid]$
$   RX領域（専用）
                extern uint8 __start_text_$OSAP.LABEL[domid]$;$NL$
                extern uint8 __limit_text_$OSAP.LABEL[domid]$;$NL$
$   R領域（専用）
                extern uint8 __start_sram_$OSAP.LABEL[domid]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$;$NL$
                extern uint8 __limit_sram_$OSAP.LABEL[domid]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$;$NL$
$   RWX領域（専用）
                extern uint8 __start_ram_$OSAP.LABEL[domid]$;$NL$
                extern uint8 __limit_ram_$OSAP.LABEL[domid]$;$NL$
                extern uint8 __start_sram_$OSAP.LABEL[domid]$;$NL$
                extern uint8 __limit_sram_$OSAP.LABEL[domid]$;$NL$
$   共有リード専用ライト
                extern uint8 $FORMAT("__start_ram_%s_%x_%x", OSAP.LABEL[domid], +DEFAULT_ACPTN[domid], +TACP_SHARED)$;$NL$
                extern uint8 $FORMAT("__limit_ram_%s_%x_%x", OSAP.LABEL[domid], +DEFAULT_ACPTN[domid], +TACP_SHARED)$;$NL$
                extern uint8 $FORMAT("__start_sram_%s_%x_%x", OSAP.LABEL[domid], +DEFAULT_ACPTN[domid], +TACP_SHARED)$;$NL$
                extern uint8 $FORMAT("__limit_sram_%s_%x_%x", OSAP.LABEL[domid], +DEFAULT_ACPTN[domid], +TACP_SHARED)$;$NL$
            $END$
        $END$
        $NL$
    $END$$NL$

$  共有領域
    extern uint8 __start_text_$OSAP.LABEL[TDOM_NONE]$;$NL$
    extern uint8 __limit_text_$OSAP.LABEL[TDOM_NONE]$;$NL$
    extern uint8 __start_sram_$OSAP.LABEL[TDOM_NONE]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$;$NL$
    extern uint8 __limit_sram_$OSAP.LABEL[TDOM_NONE]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$;$NL$
    extern uint8 __start_ram_$OSAP.LABEL[TDOM_NONE]$;$NL$
    extern uint8 __limit_ram_$OSAP.LABEL[TDOM_NONE]$;$NL$
    extern uint8 __start_sram_$OSAP.LABEL[TDOM_NONE]$;$NL$
    extern uint8 __limit_sram_$OSAP.LABEL[TDOM_NONE]$;$NL$
$   共有リード専用ライト領域全体
    extern uint8 __start_srpw_all;$NL$
    extern uint8 __limit_srpw_all;$NL$
    extern uint8 __start_ssrpw_all;$NL$
    extern uint8 __limit_ssrpw_all;$NL$
    $NL$
$END$


$FUNCTION GENERATE_TARGET_MPUINFOB$
$ 
$  保護ドメイン初期化ブロックの変更部を生成
$ 
    $FILE "Os_Lcfg.h"$
    extern const uint32 tnum_shared_mem;$NL$
    extern uint8 * const shared_meminib_table[];$NL$
    $NL$

    $FILE "kernel_mem2.c"$

    $PREPARE_OSAPINICTXB()$

$END$

$ 
$  TSKINICTXBの初期化情報を生成
$ 
$DOMINICTXB_KERNEL = "{ NULL }"$

$FUNCTION GENERATE_OSAPINIB_MPUINFOB$

    $TAB$$TAB${ 
    $IF __v850e2v3__$
$   // v850
    $IF !OSAP.TRUSTED[ARGV[1]]$
$   RX領域（専用）
        $TAB$$TAB$$TAB$( (uint8 *)&__start_text_$OSAP.LABEL[ARGV[1]]$ ),/* iregion 0 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)&__limit_text_$OSAP.LABEL[ARGV[1]]$ ),/* iregion 0 */$NL$
$   RX領域（専用ショートデータ）
        $TAB$$TAB$$TAB$( (uint8 *)&__start_sram_$OSAP.LABEL[ARGV[1]]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$ ),/* iregion 1 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)&__limit_sram_$OSAP.LABEL[ARGV[1]]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$ ),/* iregion 1 */$NL$
$   RWX領域（専用）
        $TAB$$TAB$$TAB$( (uint8 *)&__start_ram_$OSAP.LABEL[ARGV[1]]$ ),/* dregion 1 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)&__limit_ram_$OSAP.LABEL[ARGV[1]]$ ),/* dregion 1 */$NL$
$   RWX領域（専用ショートデータ）
        $TAB$$TAB$$TAB$( (uint8 *)&__start_sram_$OSAP.LABEL[ARGV[1]]$ ),/* dregion 2 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)&__limit_sram_$OSAP.LABEL[ARGV[1]]$ ),/* dregion 2 */$NL$
$   共有リード専用ライト
        $TAB$$TAB$$TAB$$FORMAT("( (uint8 *)&__start_ram_%s_%x_%x )", OSAP.LABEL[ARGV[1]], +DEFAULT_ACPTN[ARGV[1]], +TACP_SHARED)$,/* dregion 3 */$NL$
        $TAB$$TAB$$TAB$$FORMAT("( (uint8 *)&__limit_ram_%s_%x_%x )", OSAP.LABEL[ARGV[1]], +DEFAULT_ACPTN[ARGV[1]], +TACP_SHARED)$,/* dregion 3 */$NL$
$   共有リード専用ライト（ショートデータ）
        $TAB$$TAB$$TAB$$FORMAT("( (uint8 *)&__start_sram_%s_%x_%x )", OSAP.LABEL[ARGV[1]], +DEFAULT_ACPTN[ARGV[1]], +TACP_SHARED)$,/* dregion 3 */$NL$
        $TAB$$TAB$$TAB$$FORMAT("( (uint8 *)&__limit_sram_%s_%x_%x )", OSAP.LABEL[ARGV[1]], +DEFAULT_ACPTN[ARGV[1]], +TACP_SHARED)$,/* dregion 3 */$NL$
    $ELSE$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* iregion 0 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* iregion 0 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* iregion 1 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* iregion 1 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* dregion 1 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* dregion 1 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* dregion 2 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* dregion 2 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* dregion 3 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* dregion 3 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* dregion 4 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* dregion 4 */$NL$
    $END$
    $ELIF __v850e3v5__$
$   // rh850
    $IF !OSAP.TRUSTED[ARGV[1]]$
$   共有リード専用ライト
        $TAB$$TAB$$TAB$$FORMAT("( (uint8 *)&__start_ram_%s_%x_%x )", OSAP.LABEL[ARGV[1]], +DEFAULT_ACPTN[ARGV[1]], +TACP_SHARED)$,/* MPLA 1 */$NL$
        $TAB$$TAB$$TAB$$FORMAT("( (uint8 *)&__limit_ram_%s_%x_%x )", OSAP.LABEL[ARGV[1]], +DEFAULT_ACPTN[ARGV[1]], +TACP_SHARED)$,/* MPUA 1 */$NL$
$   共有リード専用ライト（ショートデータ）
        $TAB$$TAB$$TAB$$FORMAT("( (uint8 *)&__start_sram_%s_%x_%x )", OSAP.LABEL[ARGV[1]], +DEFAULT_ACPTN[ARGV[1]], +TACP_SHARED)$,/* MPLA 2 */$NL$
        $TAB$$TAB$$TAB$$FORMAT("( (uint8 *)&__limit_sram_%s_%x_%x )", OSAP.LABEL[ARGV[1]], +DEFAULT_ACPTN[ARGV[1]], +TACP_SHARED)$,/* MPUA 2 */$NL$
        $TAB$$TAB$$TAB$( (uint32)0 ),/* MPRC */$NL$
    $ELSE$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* MPLA 1 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* MPUA 1 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* MPLA 2 */$NL$
        $TAB$$TAB$$TAB$( (uint8 *)NULL ),/* MPUA 2 */$NL$
        $TAB$$TAB$$TAB$( (uint32)0 ),/* MPRC */$NL$
    $END$
    $END$
    $TAB$$TAB$}$NL$
    $NL$
$END$

$FUNCTION GENERATE_TSKINICTXB$
	$TAB$$TAB${$NL$
    $TAB$$TAB$$TAB$$TSK.TINIB_SSTKSZ[ARGV[1]]$,$NL$
	$TAB$$TAB$$TAB$((void *)((uint8 *)($TSK.TINIB_SSTK[ARGV[1]]$)
    $SPC$+ ($TSK.TINIB_SSTKSZ[ARGV[1]]$))),$NL$
	$IF OSAP.TRUSTED[TSK.OSAPID[ARGV[1]]]$
        $TAB$$TAB$$TAB$0,$NL$
        $TAB$$TAB$$TAB$0,$NL$
    $ELSE$
        $TAB$$TAB$$TAB$$TSK.TINIB_USTKSZ[ARGV[1]]$,$NL$ 
        $TAB$$TAB$$TAB$((void *)((uint8 *)($TSK.TINIB_USTK[ARGV[1]]$)
        $SPC$+ ($TSK.TINIB_USTKSZ[ARGV[1]]$))),$NL$
    $END$
	$TAB$$TAB$},$NL$
$END$

$FUNCTION GENERATE_STKMPUINFOB$
	$TAB$$TAB${$NL$
	$IF OSAP.TRUSTED[TSK.OSAPID[ARGV[1]]]$
        $TAB$$TAB$$TAB$0,$NL$
        $TAB$$TAB$$TAB$0,$NL$
    $ELSE$
		$IF EQ(TSK.STK[ARGV[1]],"NULL")$
$			// stkがNULLの場合の処理
            $IF LENGTH(TSK.SHARED_USTK_ID[ARGV[1]])$
$               // 共有スタック
                $TAB$$TAB$$TAB$$FORMAT("&__start_shared_user_stack%s", TSK.PRIORITY[ARGV[1]])$,$NL$
                $TAB$$TAB$$TAB$$FORMAT("&__limit_shared_user_stack%s", TSK.PRIORITY[ARGV[1]])$,$NL$
            $ELSE$
$               // 固有スタック
                $TAB$$TAB$$TAB$$FORMAT("&__start_user_stack%s", ARGV[1])$,$NL$
                $TAB$$TAB$$TAB$$FORMAT("&__limit_user_stack%s", ARGV[1])$,$NL$
            $END$
        $ELSE$
$			// stkがNULLでない場合の処理
            $TAB$$TAB$$TAB$$FORMAT("(uint8 *)%s", TSK.TINIB_USTK[ARGV[1]])$,$NL$
            $TAB$$TAB$$TAB$$FORMAT("(uint8 *)((uint32)%s + %d)", TSK.TINIB_USTK[ARGV[1]], TSK.TINIB_USTKSZ[ARGV[1]])$,$NL$
        $END$
    $END$
	$TAB$$TAB$},$NL$
$END$

$INCLUDE "arch/v850_gcc/prc_common.tf"$

$IF __v850e2v3__$

$ 割込みベクタと各割込み入口処理(V850E2V3のみ)

$FILE "Os_Lcfg_asm.S"$


$
$ アセンブラ出力用の関数群
$

$FUNCTION EXCEPTION_VECTOR_SECTION$
$TAB$.section .vector,"ax"$NL$
$END$

$FUNCTION ASM_GLOBAL$
	$TAB$.global $ARGV[1]$
$END$

$FUNCTION ASM_LABEL$
	FLABEL($ARGV[1]$)
$END$

$FUNCTION ASM_COMMENT$
	//
$END$


#include <v850asm.inc>$NL$$NL$

$VECTOR_ASMOUT()$

$END$
$ =end IF __v850e2v3__


$IF __v850e3v5__$

$
$ テーブル参照方式用ベクタテーブル(v850e3v5)
$

extern void interrupt(void);$NL$
const uint32 __attribute__((aligned(512))) intbp_tbl[TNUM_INT] = {$NL$
$JOINEACH intno INTNO_VALID "\n"$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_1")$
		$TAB$(uint32)&$ISR.INT_ENTRY[isrid]$
	$ELSE$
		$TAB$(uint32)&interrupt
	$END$
$	//カンマの出力（最後の要素の後ろに出力しない）
	$IF intno != AT(INTNO_VALID,LENGTH(INTNO_VALID) - 1)$
		,
	$END$
	$TAB$$FORMAT("/* %d */", intno)$
$END$
$NL$};$NL$

$END$

$ 
$  arch/gcc/ldscript.tfのターゲット依存部
$ 

$FUNCTION GENERATE_MEMORY$
    $NOOP()$
$END$

$FUNCTION GENERATE_OUTPUT$
    OUTPUT_FORMAT("elf32-v850-rh850","elf32-v850-rh850","elf32-v850-rh850")$NL$
    OUTPUT_ARCH(v850-rh850)$NL$
    $NL$
$END$

$FUNCTION GENERATE_PROVIDE$
    PROVIDE(_hardware_init_hook = 0);$NL$
    PROVIDE(_software_init_hook = 0);$NL$
    PROVIDE(_software_term_hook = 0);$NL$
    PROVIDE(_bsssecinib_table = 0);$NL$
    PROVIDE(_tnum_bsssec = 0);$NL$
    PROVIDE(_datasecinib_table = 0);$NL$
    PROVIDE(_tnum_datasec = 0);$NL$
    $NL$

    $IF LENGTH(OSAP.ID_LIST)$
        $FOREACH domid OSAP.ID_LIST$
            $IF !OSAP.TRUSTED[domid]$
$   RX領域（専用）
                PROVIDE(___start_text_$OSAP.LABEL[domid]$ = 0xfffffff0);$NL$
                PROVIDE(___limit_text_$OSAP.LABEL[domid]$ = 0xfffffff0);$NL$
$   R領域（専用）
                PROVIDE(___start_sram_$OSAP.LABEL[domid]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$ = 0xfffffff0);$NL$
                PROVIDE(___limit_sram_$OSAP.LABEL[domid]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$ = 0xfffffff0);$NL$
$   RWX領域（専用）
                PROVIDE(___start_ram_$OSAP.LABEL[domid]$ = 0xfffffff0);$NL$
                PROVIDE(___limit_ram_$OSAP.LABEL[domid]$ = 0xfffffff0);$NL$
                PROVIDE(___start_sram_$OSAP.LABEL[domid]$ = 0xfffffff0);$NL$
                PROVIDE(___limit_sram_$OSAP.LABEL[domid]$ = 0xfffffff0);$NL$
$   共有リード専用ライト
                PROVIDE($FORMAT("___start_ram_%s_%x_%x", OSAP.LABEL[domid], +DEFAULT_ACPTN[domid], +TACP_SHARED)$ = 0xfffffff0);$NL$
                PROVIDE($FORMAT("___limit_ram_%s_%x_%x", OSAP.LABEL[domid], +DEFAULT_ACPTN[domid], +TACP_SHARED)$ = 0xfffffff0);$NL$
                PROVIDE($FORMAT("___start_sram_%s_%x_%x", OSAP.LABEL[domid], +DEFAULT_ACPTN[domid], +TACP_SHARED)$ = 0xfffffff0);$NL$
                PROVIDE($FORMAT("___limit_sram_%s_%x_%x", OSAP.LABEL[domid], +DEFAULT_ACPTN[domid], +TACP_SHARED)$ = 0xfffffff0);$NL$
            $END$
        $END$
        $NL$
    $END$$NL$

$  共有領域
    PROVIDE(___start_text_$OSAP.LABEL[TDOM_NONE]$ = 0xfffffff0);$NL$
    PROVIDE(___limit_text_$OSAP.LABEL[TDOM_NONE]$ = 0xfffffff0);$NL$
    PROVIDE(___start_sram_$OSAP.LABEL[TDOM_NONE]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$ = 0xfffffff0);$NL$
    PROVIDE(___limit_sram_$OSAP.LABEL[TDOM_NONE]$_$FORMAT("%x", MEMATR_ROSDATA & ~TA_MEMINI)$ = 0xfffffff0);$NL$
    PROVIDE(___start_ram_$OSAP.LABEL[TDOM_NONE]$ = 0xfffffff0);$NL$
    PROVIDE(___limit_ram_$OSAP.LABEL[TDOM_NONE]$ = 0xfffffff0);$NL$
    PROVIDE(___start_sram_$OSAP.LABEL[TDOM_NONE]$ = 0xfffffff0);$NL$
    PROVIDE(___limit_sram_$OSAP.LABEL[TDOM_NONE]$ = 0xfffffff0);$NL$
$   共有リード専用ライト領域全体
    PROVIDE(___start_srpw_all = 0xfffffff0);$NL$
    PROVIDE(___limit_srpw_all = 0xfffffff0);$NL$
    PROVIDE(___start_ssrpw_all = 0xfffffff0);$NL$
    PROVIDE(___limit_ssrpw_all = 0xfffffff0);$NL$
    STARTUP(start.o);$NL$
    ENTRY(__reset);$NL$
    $NL$
$END$

$FUNCTION GENERATE_GP_LABEL$
    $TAB$$TAB$__gp = . + 32K;$NL$
    $TAB$$TAB$__tp = . + 32K;$NL$
$END$

$FILE "kernel_mem2.c"$

$IF __v850e2v3__$
$   // v850
$TNUM_SHARED_REGION = 3$
const uint32 tnum_shared_mem = $TNUM_SHARED_REGION * 2$;$NL$
uint8 * const shared_meminib_table[$TNUM_SHARED_REGION * 2$] = {$NL$
$TAB$((uint8 *)&__start_text_$OSAP.LABEL[TDOM_NONE]$),$TAB$/* iregion 2 */$NL$
$TAB$((uint8 *)&__limit_text_$OSAP.LABEL[TDOM_NONE]$),$TAB$/* iregion 2 */$NL$

$TAB$((uint8 *)NULL),$TAB$/* iregion 3 */$NL$
$TAB$((uint8 *)NULL),$TAB$/* iregion 3 */$NL$

$TAB$((uint8 *)NULL),$TAB$/* dregion 5 */$NL$
$TAB$((uint8 *)NULL),$TAB$/* dregion 5 */$NL$
};$NL$
$NL$
$ELIF __v850e3v5__$
$   // rh850
$TNUM_SHARED_REGION = 1$
const uint32 tnum_shared_mem = $TNUM_SHARED_REGION * 2$;$NL$
uint8 * const shared_meminib_table[$TNUM_SHARED_REGION * 2$] = {$NL$
$TAB$((uint8 *)NULL),$TAB$/* MPLA 3 */$NL$
$TAB$((uint8 *)NULL),$TAB$/* MPUA 3 */$NL$
};$NL$
$NL$
$ const uint32 mpu_region_control = 0x01; /* MPRC */$NL$
$ $NL$
$END$

$ 
$  GCC依存部のテンプレートファイルのインクルード
$ 
$INCLUDE "gcc/ldscript.tf"$ 

$FILE "cfg2_out.tf"$
$ LNKSEC.*の出力
$$numls = $numls$$$$NL$
$NL$
$FOREACH lsid RANGE(1, numls)$
	$$LNKSEC.MEMREG[$lsid$] = $LNKSEC.MEMREG[lsid]$$$$NL$
	$$LNKSEC.SECTION[$lsid$] = $ESCSTR(LNKSEC.SECTION[lsid])$$$$NL$
	$NL$
$END$

$JOINEACH tskid TSK.ID_LIST "\n"$
	$$TSK.ID[$+tskid$] = $TSK.ID[tskid]$$$
$END$
$NL$

