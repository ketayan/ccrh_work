/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: prc_config.c 1801 2015-03-27 06:34:43Z t_ishikawa $
 */

/*
 *		プロセッサ依存モジュール（Nios2用）
 */
#include "kernel_impl.h"
/*
 *  例外（割込み/CPU例外）のネスト回数のカウント
 *  コンテキスト参照のために使用
 */
uint32				except_nest_cnt;

uint32				exception_temporary1;
uint32				exception_temporary2;

#ifdef CFG_USE_PROTECTIONHOOK
DEFINE_VAR_SEC_NOBITS(uint32, nios2_cpu_exp_no,       ".srpw_bss_kernel");
DEFINE_VAR_SEC_NOBITS(uint32, nios2_cpu_exp_pc,       ".srpw_bss_kernel");
DEFINE_VAR_SEC_NOBITS(uint32, nios2_cpu_exp_bad_addr, ".srpw_bss_kernel");
DEFINE_VAR_SEC_NOBITS(uint32, nios2_cpu_exp_sp,       ".srpw_bss_kernel");
#endif /* CFG_USE_PROTECTIONHOOK */

/*
 *  x_nested_lock_os_int()のネスト回数
 */
volatile uint8		nested_lock_os_int_cnt;

/*
 *  非信頼フックルーチンを実行する前のシステムスタックのスタックポインタ
 */
uint32				hook_savedsp;

uint32				shutdown_hook_savedsp;

uint32				trusted_hook_savedsp;

/*
 *  例外コード/エラーコード変換テーブル
 */
const StatusType	exc_code_tbl[TNUM_EXCH] = {
	E_OS_PROTECTION_EXCEPTION,  /*  0 Reset */
	E_OS_PROTECTION_EXCEPTION,  /*  1 Processor-only Reset Request */
	E_OS_PROTECTION_EXCEPTION,  /*  2 Interrupt */
	E_OS_PROTECTION_EXCEPTION,  /*  3 Trap Instruction */
	E_OS_PROTECTION_EXCEPTION,  /*  4 Unimplemented Instruction */
	E_OS_PROTECTION_EXCEPTION,  /*  5 Illegal Instruction */
	E_OS_PROTECTION_EXCEPTION,  /*  6 Misaligned Data Address */
	E_OS_PROTECTION_EXCEPTION,  /*  7 Misaligned Destination Address */
	E_OS_PROTECTION_EXCEPTION,  /*  8 Division Error */
	E_OS_PROTECTION_EXCEPTION,  /*  9 Supervisor-only Instruction Address */
	E_OS_PROTECTION_EXCEPTION,  /* 10 Supervisor-only Instruction */
	E_OS_PROTECTION_EXCEPTION,  /* 11 Supervisor-only Data Address */
	E_OS_PROTECTION_EXCEPTION,  /* 12 Fast/Double TBL miss (for MMU) */
	E_OS_PROTECTION_EXCEPTION,  /* 13 TBL Permission Violation (execute)(for MMU) */
	E_OS_PROTECTION_EXCEPTION,  /* 14 TBL Permission Violation (read)(for MMU) */
	E_OS_PROTECTION_EXCEPTION,  /* 15 TBL Permission Violation (write)(for MMU) */
	E_OS_PROTECTION_MEMORY,     /* 16 MPU Region Violation(instruction) */
	E_OS_PROTECTION_MEMORY      /* 17 MPU Region Violation(data) */
};


/*
 *  OS割込み禁止状態の時に割込み優先度マスクを保存する変数
 */
volatile uint32		saved_status_il;

void
prc_hardware_initialize(void)
{
	vic_initialize();
}

#ifdef PERF_TEST
/*
 *  キャッシュパージフラグ
 *    セットされるとソフトウェア割込みエントリの終了処理で
 *    キャッシュをパージする
 */
uint32 cache_flash_flag;
#endif /* PERF_TEST */

/*
 *  プロセッサ依存の初期化
 */
void
prc_initialize(void)
{
	/*
	 *  カーネル起動時は非タスクコンテキストとして動作させるため1に
	 */
	except_nest_cnt = 1U;

#ifdef PERF_TEST
	/*
	 *  キャッシュパージフラグ
	 *    セットされるとソフトウェア割込みエントリの終了処理で
	 *    キャッシュをパージする
	 */
	cache_flash_flag = 0U;
#endif /* PERF_TEST */

	/*
	 *  MPU初期化
	 *    MPU情報生成
	 *    MPU設定
	 *    MPU有効
	 */
	prc_init_mpu();
}

/*
 *  プロセッサ依存の終了処理
 */
void
prc_terminate(void)
{

	/* MPU 無効 */
	mpu_disable();
}

/*
 *  割込み要求ラインの属性の設定
 */
void
x_config_int(InterruptNumberType intno, AttributeType intatr, PriorityType intpri)
{
	ASSERT(VALID_INTNO(intno));

	/*
	 *  いったん割込みを禁止する
	 */
	x_disable_int(intno);

	/* 割込み優先度設定，設定しない場合最低優先度0となる */
	vic_set_int_config((uint8) intno, INT_IPM(intpri));

	if ((intatr & ENABLE) != 0U) {
		/*
		 *  初期状態が割込み許可であれば，割込みを許可
		 */
		x_enable_int(intno);
	}
}

/*
 *  startからendまでの領域が
 *  非信頼フックルーチン実行時のスタックに含まれるかチェックする
 */
AccessType
probe_nthkstk(const MemoryStartAddressType start, const MemoryStartAddressType end)
{
	AccessType result = AP_NoAccess;

	if (hook_savedsp != 0U) {
		if ((start >= (const MemoryStartAddressType) _nthkstk) &&
			(end <= (const MemoryStartAddressType) ((const MemorySizeType) _nthkstk + _nthkstksz))) {
			result = AP_Writable | AP_Readable | AP_StackSpace;
		}
	}
	return(result);
}

void
cancel_nontrusted_hook(void)
{
	MPUINFOB mpuinfo;

	if (hook_savedsp != 0U) {
		hook_savedsp = 0U;
		mpuinfo.mpubase = NIOS2_DATA_MPUBASE(NIOS2_MPU_STACK_REGION, 0U);
		mpuinfo.mpuacc = NIOS2_MPUACC(0U, 0U);
		prc_set_mpu(&mpuinfo);
	}
}

#ifndef OMIT_DEFAULT_INT_HANDLER

/*
 *  未定義の割込みが入った場合の処理
 */
void
default_int_handler(void)
{
	target_fput_str("Unregistered Interrupt occurs.");
	ASSERT(0);
}

#endif /* OMIT_DEFAULT_INT_HANDLER */

/*
 *  不正な機能コードを指定して，システムサービス発行時の処理
 */

StatusType
no_support_service(void)
{
	StatusType ercd = E_OS_SERVICEID;

#ifdef CFG_USE_ERRORHOOK
	x_nested_lock_os_int();
	call_errorhook(ercd, OSServiceId_INVALID);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

	return(ercd);
}
