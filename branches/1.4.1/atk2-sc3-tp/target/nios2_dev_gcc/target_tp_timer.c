/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2016 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2016 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_tp_timer.c 636 2016-03-22 01:35:30Z witz-itoyo $
 */

#include "Os.h"
#include "prc_sil.h"
#include "timewindow.h"
#include "interrupt.h"
#include "target_tp_timer.h"
#include "target_hw_counter.h"

void
target_initialize_systemcycle_timer(void)
{
	uint32 t_syscyc = 0U;

	/*
	 * NIOS2では，タイマのタイムアウト値を設定するときに，
	 * タイムアウト値-1の値を設定する必要がある
	 */
	if (syscyc > 1U) {
		t_syscyc = syscyc - 1U;
	}

	sil_wrw_iop((void *) TIMER_CONTROL(SYSTEMCYCLETIMER_INTNO), TIMER_CONTROL_STOP);
	sil_wrw_iop((void *) TIMER_STATUS(SYSTEMCYCLETIMER_INTNO), 0x00U);

	/* システム周期用タイマカウンターセット 上位16bit */
	sil_wrw_iop((void *) TIMER_PERIODH(SYSTEMCYCLETIMER_INTNO), (t_syscyc >> 16U));
	/* システム周期用タイマカウンターセット 下位16bit */
	sil_wrw_iop((void *) TIMER_PERIODL(SYSTEMCYCLETIMER_INTNO), (t_syscyc & 0xffffU));

	x_config_int(SYSTEMCYCLETIMER_INTNO, ENABLE, TPRI_TPTIMER);
}


void
target_start_systemcycle_timer(void)
{
	/* システム周期用タイマはタイムアウト後自動で再スタートするように設定 */
	sil_wrw_iop((void *) TIMER_CONTROL(SYSTEMCYCLETIMER_INTNO), TIMER_CONTROL_ITO | TIMER_CONTROL_CONT | TIMER_CONTROL_START);
}

void
target_intclear_systemcycle_timer(void)
{
	/* タイマ割込み要求のクリア */
	sil_wrw_iop((void *) TIMER_STATUS(SYSTEMCYCLETIMER_INTNO), 0x00U);
}

void
target_stop_systemcycle_timer(void)
{

	/* タイマ停止 */
	sil_wrw_iop((void *) TIMER_CONTROL(SYSTEMCYCLETIMER_INTNO), TIMER_CONTROL_STOP);

	/* タイマ割込み要求のクリア */
	sil_wrw_iop((void *) TIMER_STATUS(SYSTEMCYCLETIMER_INTNO), 0x00U);

}

boolean
target_get_timeoutbit_systemcycle_timer(void)
{
	/* タイムアウトビットを取得 */
	if ((sil_rew_iop((void *) TIMER_STATUS(SYSTEMCYCLETIMER_INTNO)) & 0x01U) != 0x00U) {
		return(TRUE);
	}
	else {
		return(FALSE);
	}
}

void
target_initialize_timewindow_timer(void)
{

	sil_wrw_iop((void *) TIMER_CONTROL(TIMEWINDOWTIMER_INTNO), TIMER_CONTROL_STOP);
	sil_wrw_iop((void *) TIMER_STATUS(TIMEWINDOWTIMER_INTNO), 0x00U);

	x_config_int(TIMEWINDOWTIMER_INTNO, ENABLE, TPRI_TPTIMER);
}

void
target_set_timewindow_timer(TickType tick)
{

	/*
	 * NIOS2では，タイマのタイムアウト値を設定するときに，
	 * タイムアウト値-1の値を設定する必要がある
	 */
	if (tick > 1U) {
		tick--;
	}

	/* タイムウィンドウ用タイマカウンターセット 上位16bit */
	sil_wrw_iop((void *) TIMER_PERIODH(TIMEWINDOWTIMER_INTNO), (tick >> 16U));
	/* タイムウィンドウ用タイマカウンターセット 下位16bit */
	sil_wrw_iop((void *) TIMER_PERIODL(TIMEWINDOWTIMER_INTNO), (tick & 0xffffU));
}

void
target_start_timewindow_timer(void)
{
	/* タイムウィンドウ用タイマはタイムアウト後自動で再スタートしないように設定 */
	sil_wrw_iop((void *) TIMER_CONTROL(TIMEWINDOWTIMER_INTNO), TIMER_CONTROL_ITO | TIMER_CONTROL_START);
}


void
target_stop_timewindow_timer(void)
{
	/* タイマ停止 */
	sil_wrw_iop((void *) TIMER_CONTROL(TIMEWINDOWTIMER_INTNO), TIMER_CONTROL_STOP);
}


void
target_intclear_timewindow_timer(void)
{
	/* タイマ割込み要求のクリア */
	sil_wrw_iop((void *) TIMER_STATUS(TIMEWINDOWTIMER_INTNO), 0x00U);
}
