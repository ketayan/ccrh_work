/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004-2016 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2004-2016 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: sample2.c 636 2016-03-22 01:35:30Z witz-itoyo $
 */

/*
 *		非信頼OSアプリケーション所属サンプルプログラムの本体
 *
 *  サンプルプログラムの動作はsample1.cのコメント参照
 *  本ファイルは非信頼OSアプリケーションに所属している
 *  オブジェクトを記述している
 */

#include "sample.h"
#include "sample2.h"
#include "sample3.h"

TASK(Task2_Main);
TASK(Task2_High);
TASK(Task2_Low);

TASK(Task_MemproNG);
TASK(Task2_Restart);

/*
 *  APIエラーログマクロ
 *
 *  ErrorHookが有効の場合はErrorHookから
 *  エラーログを出力し, ErrorHookが無効の場合は
 *  以下のマクロよりエラーログ出力を行う
 */
#if defined(CFG_USE_ERRORHOOK)
#define error_log(api)	(api)
#else /* !defined( CFG_USE_ERRORHOOK ) */
#define	error_log(api)										   \
	{														   \
		StatusType ercd;									   \
		ercd = api;     /* 各API実行 */						   \
		if (ercd != E_OK) {									   \
			syslog(LOG_INFO, "Error:%d", atk2_strerror(ercd)); \
		}													   \
	}
#endif /* defined( CFG_USE_ERRORHOOK ) */


TASK(Task2_Main)
{
	syslog(LOG_INFO, "Task2_Main RUNNING");
	error_log(TerminateTask());
}   /* TASK( Task2_Main ) */

TASK(Task2_High)
{
	syslog(LOG_INFO, "Task2_High RUNNING");
	error_log(TerminateTask());
}   /* TASK( Task2_High ) */

TASK(Task2_Low)
{
	syslog(LOG_INFO, "Task2_Low RUNNING");
	error_log(TerminateTask());
}   /* TASK( Task2_Low ) */

TASK(Task2_Res)
{
	syslog(LOG_INFO, "Task2_Res RUNNING");
	GetResource(ResOsapExternal12);
	ReleaseResource(ResOsapExternal12);
	error_log(TerminateTask());
}   /* TASK( Task2_Res ) */

TASK(Task2_Int)
{
	syslog(LOG_INFO, "Task2_Int RUNNING");
	DisableAllInterrupts();
	EnableAllInterrupts();
	error_log(TerminateTask());
}   /* TASK( Task2_Int ) */

/* 非信頼OSAPから非信頼OSAPのメモリにアクセスした場合，*/
/* メモリ保護違反に伴い，プロテクションフックが呼び出されることを確認 */
TASK(Task2_NonTrusted)
{
	sint32 test;

	syslog(LOG_INFO, "Task2_NonTrusted RUNNING");
	test = testmemng;
	error_log(TerminateTask());
}   /* TASK( Task2_NonTrusted ) */

/* OsApplication2のリスタートタスク */
TASK(Task2_Restart)
{
	syslog(LOG_INFO, "Task2_Restart ACTIVATE");
	AllowAccess();
	syslog(LOG_INFO, "Task2_Restart FINISH");
	TerminateTask();
}   /* TASK( Task2_Restart ) */
