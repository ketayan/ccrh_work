
		TOPPERS/ATK2-SC3-TP
		ターゲット依存部 ポーティングガイド

このドキュメントは，TOPPERS/ATK2-SC3-TP(以下，TP)を，未サポートのターゲット
システムにポーティングするために必要となるターゲット依存部の実装方法を説明
するものである．
ATK2-SC3のポーティングガイドと内容が重複する部分については，記載を省略
する．

----------------------------------------------------------------------
TOPPERS ATK2
    Toyohashi Open Platform for Embedded Real-Time Systems
    Automotive Kernel Version 2

Copyright (C) 2011-2016 by Center for Embedded Computing Systems
            Graduate School of Information Science, Nagoya Univ., JAPAN
Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
Copyright (C) 2011-2013 by Spansion LLC, USA
Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
Copyright (C) 2011-2016 by Witz Corporation
Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION

上記著作権者は，以下の (1)〜(3)の条件を満たす場合に限り，本ドキュメ
ント（本ドキュメントを改変したものを含む．以下同じ）を使用・複製・改
変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
(1) 本ドキュメントを利用する場合には，上記の著作権表示，この利用条件
    および下記の無保証規定が，そのままの形でドキュメント中に含まれて
    いること．
(2) 本ドキュメントを改変する場合には，ドキュメントを改変した旨の記述
    を，改変後のドキュメント中に含めること．ただし，改変後のドキュメ
    ントが，TOPPERSプロジェクト指定の開発成果物である場合には，この限
    りではない．
(3) 本ドキュメントの利用により直接的または間接的に生じるいかなる損害
    からも，上記著作権者およびTOPPERSプロジェクトを免責すること．また，
    本ドキュメントのユーザまたはエンドユーザからのいかなる理由に基づ
    く請求からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

本ドキュメントは，AUTOSAR（AUTomotive Open System ARchitecture）仕様
に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するものではな
い．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利用する
者に対して，AUTOSARパートナーになることを求めている．

本ドキュメントは，無保証で提供されているものである．上記著作権者およ
びTOPPERSプロジェクトは，本ドキュメントに関して，特定の使用目的に対す
る適合性も含めて，いかなる保証も行わない．また，本ドキュメントの利用
により直接的または間接的に生じたいかなる損害に関しても，その責任を負
わない．

$Id: porting.txt 636 2016-03-22 01:35:30Z witz-itoyo $
----------------------------------------------------------------------

○目次

０．ATK2-SC3とATK2-SC3-TPの差分
１．時間パーティショニング用タイマドライバ
	1.1 時間パーティショニング用タイマの定数定義
	1.2 システム周期用タイマドライバ
	1.3 タイムウィンドウ切り替え用タイマドライバ
２．ディスパッチャ本体
３．割込みハンドラの出入口処理

４．TP割込みハンドラの出入口処理
５．テンプレートファイルのターゲット依存部
６．オフセットファイル関係
７．リファレンス


０．ATK2-SC3とATK2-SC3-TPの差分
TPのポーティングでは，以下のターゲット依存部を新規で実装する必要がある．

・時間パーティショニング用タイマドライバ
・TP割込みハンドラの出入口処理

また，以下の既存ターゲット依存部の実装に変更を加える必要がある．
・ディスパッチャ本体
・割込みハンドラの出入口処理
・テンプレートファイルのターゲット依存部

１．時間パーティショニング用タイマドライバ

ATK2-SC1-TPから変更なし．

２．ディスパッチャ本体

TPのポーティングでは，ディスパッチャ本体に対して，以下の変更を加える必
要がある．

・dispatcher_1において，p_tschedtskをp_runtskに設定し，
それがNULLである場合，dispatcher_3に分岐するように変更する．

・dispatcher_1において，p_tschedtskをp_runtskに設定し，それがNULLでな
かった場合，p_runschedomにp_runtskが所属するスケジューリングドメインを
設定するように変更する．

・dispatcher_3に分岐したとき，p_runschedomにNULLを設定するように変更す
る．

以下に，上記変更を反映した擬似コードを示す．
行頭に+が記載された行がATK2-SC3からの差分である．
----------------------------------------
  dispatcher_1:
+	while ((p_runtsk = p_tschedtsk) == NULL) {
+		p_runschedomにNULLを設定する
		割込みを許可したらOS割込み禁止解除状態になるよう準備する
		非タスクコンテキストに切り換える
		OS割込み禁止を解除し，割込み発生を待つ
		OS割込み禁止状態に戻す
		タスクコンテキストに戻す
	}
	割込み・例外のネスト回数を'0'にする
+	p_runschedomにp_runtsk->p_tinib->p_scdcbを設定する
----------------------------------------

３．割込みハンドラの出入口処理

TPのポーティングでは，割込みハンドラの出入口処理に対して，以下の変更を
加える必要がある．

・入口処理にて，その割込みがタスクに割込んだ場合，スタックポインタをシ
ステムスタックに切り替えるが，その後，プロパータスクに割込んでいた場合の
み，タイムウィンドウ切り替え用タイマ停止(target_stop_timewindow_timer)を
呼び出すように変更する．呼び出し前後での，スクラッチレジスタの保存，復
帰は適宜行う．

・受け付けた割込みよりも優先度の高い割込みを受け付けるようにするまえに， 
割込み発生回数のインクリメント(increment_occurrence_count)を，発生した割込み 
番号を第1引数として呼び出すように変更する．
呼び出し前後での，スクラッチレジスタの保存，復帰は適宜行う．

・割込みハンドラ呼出し直後に，check_systemcycle_timeoutを呼び出すよう
に変更する．

・出口処理にて，その割込みがタスクに割込んだ場合，スタックポインタを
切り替えるが，その切り替え前に，プロパータスクに割込んでいた場合のみ，
タイムウィンドウ切り替え用タイマ開始(target_start_timewindow_timer)を
呼び出すように変更する．

・遅延ディスパッチ判定部分において，p_runtskとp_tschedtskを比較し，
これらが異なっていた場合，ディスパッチするように変更する．

・遅延ディスパッチ処理において，OS割込み禁止状態にした後，かつ 
  スクラッチレジスタを除くすべてのレジスタをスタックに保存 
  する前の位置にcheck_dispatch_ok_from_tpラベルを作成する 

以下に，上記変更を反映した擬似コードを示す．
行頭に+が記載された行がATK2-SC3からの差分である．
----------------------------------------
void
<割込みの出入口処理>(void)
{
	if (非特権モードで割込み発生) {
		スタックを実行状態のタスクのシステムスタックに切り換える
	}
	少なくともOS割込みを禁止した状態にする … (*f)
	スクラッチレジスタをスタックに保存する
	if (タスクコンテキストで割込み発生) {
		スタックを非タスクコンテキスト用のスタックに切り換え，
								非タスクコンテキストに切り換える
+		if(p_runschedom != p_idleschedom) {
+			target_stop_timewindow_timer();
+		}
	}
+	increment_occurrence_count(intno);
	割込み優先度マスクを，受け付けた割込み要求の割込優先度に設定し，
				OS割込み禁止解除状態にする（受け付けた割込みよりも
				優先度の高い割込みを受け付けるようにする）… (*i)

#ifdef CFG_USE_STACKMONITORING
	if (タスクコンテキストで割込み発生) {
		スタックポインタチェック方式でタスクシステムスタックの
											スタックモニタリングを行う
		マジックナンバーチェック方式でタスクシステムスタックの
											スタックモニタリングを行う
	}
	else {
		スタック残量チェック方式でC2ISR用スタックのスタックモニタ
		リングを行う
		マジックナンバーチェック方式でC2ISR用スタックのスタックモニタ
		リングを行う
	}
#endif /* CFG_USE_STACKMONITORING */

	callevel_statを保存
	callevel_statに割込みビットを立てる
	run_trustedを保存
	p_runosapを保存
	p_currentosapを保存
	p_runisrを保存
	p_runisrを実行するC2ISRに対応するisrcb_tableの要素に設定する
	p_runosapを実行するC2ISRが所属するOSアプリケーションに設定する
	p_currentosapを実行するC2ISRが所属するOSアプリケーションに設定する

	C2ISRを呼び出す
+	check_systemcycle_timeout();

#ifdef CFG_USE_STACKMONITORING
	マジックナンバーチェック方式でC2ISR用スタックのスタックモニタリン
	グを行う
#endif /* CFG_USE_STACKMONITORING */

	exit_isr2();			/* C2ISRの不正終了チェック */

	p_runisrを復帰
	p_currentosapを復帰
	p_runosapを復帰
	run_trustedを復帰
	callevel_statを復帰

  ret_int:
	if (タスクコンテキストで割込み発生) {
+		if(p_runschedom != p_idleschedom) {
+			target_start_timewindow_timer();
+		}
		（少なくとも）OS割込みを禁止した状態にする
		スタックをタスク用のスタックに戻し，タスクコンテキストに切り換える
		スタックポインタを復帰
		if (p_runtsk == NULL) {
			OS割込み禁止状態にする … (*e)
			割込み優先度マスクを，全解除状態（TIPM_ENAALL）に設定する
								 … (*h)
			dispatcherに分岐する
		}
-		else if (p_runtsk != p_schedtsk) {
+		else if (p_runtsk != p_tschedtsk) {
			OS割込み禁止状態にする							… (*e)
			割込み優先度マスクを，全解除状態（TIPM_ENAALL）に設定する
															… (*h)
+			check_dispatch_ok_from_tp:
			スクラッチレジスタを除くすべてのレジスタをスタックに保存す
			る
			スタックポインタを自タスク（p_runtsk）のTCBに保存する
			ret_int_rを，実行再開番地として自タスクのTCBに保存する

			dispatcherに分岐する

		  ret_int_r:
			スクラッチレジスタを除くすべてのレジスタをスタックから復帰
			する
		}
	}
	割込み処理からのリターン後に，割込み優先度マスクが
								割込み処理前の値に戻るように準備する
	割込み処理からのリターン後に，OS割込み禁止解除状態に戻るように準備
	する
	スクラッチレジスタをスタックから復帰する
	割込み処理からのリターン
}
----------------------------------------

４．TP割込みハンドラの出入口処理

TPのポーティングでは，TP割込みハンドラの出入口処理を新規に実装する必要
がある．
タイムウィンドウ切り替え用タイマの割込み処理，システム周期用タイマの割
込み処理の出入口処理を<TP割込みの出入口処理>で共通で行う．
以下に，その擬似コードを示す．
----------------------------------------

void
<TP割込みの出入口処理>(割込み番号)
{

	if (非特権モードで割込み発生) {
		スタックを実行状態のタスクのシステムスタックに切り換える
	}
	少なくともOS割込みを禁止した状態にする
	スクラッチレジスタをスタックに保存する
	OS割込み禁止状態にする
	割込み優先度マスクを，全解除状態（TIPM_ENAALL）に設定する 
	if (p_runtskがNULLでない) {
		スタックを非タスクコンテキスト用のスタックに切り換え，
			非タスクコンテキストに切り換える
	}
	
	tp_int_handler(intno);

  tp_ret_int:
	if (p_runtsk != NULL) {
		スタックをタスク用のスタックに戻し，タスクコンテキストに切り換

		える
		if (p_runtsk != p_tschedtsk) {
			check_dispatch_ok_from_tpに分岐する
		}
	}
	
	割込み処理からのリターン後に，割込み優先度マスクが
								割込み処理前の値に戻るように準備する
	割込み処理からのリターン後に，OS割込み禁止解除状態に戻るように準備
	する

	スクラッチレジスタをスタックから復帰する
	割込み処理からのリターン
}
----------------------------------------
５．テンプレートファイルのターゲット依存部

ATK2-SC3-TPでは，ATK2-SC1-TPのポーティングガイドでの記載内容に加えて，
下記の関数を実装する必要がある．

TPでは，プロパータスクとアイドルタスクでスタック共有してはいけないため，
下記の関数を実装し，それらを別々で生成すること．

(5-1) SECTION_SHARED_USTACK（OSAPID，共有スタックID）
		プロパータスクかつ基本タスクの共有スタックのセクション名を返す関数
(5-2) SECTION_SHARED_USTACK_IDLE(共有スタックID)
		アイドルタスクかつ基本タスクの共有スタックのセクション名を返す関数

(5-3) ALLOC_SHARED_USTACK(...)
		プロパータスクかつ基本タスクの共有スタックの確保関数
	第1引数 共有スタック名
	第2引数 OSAPID
	第3引数 共有スタックID(タスク優先度)
	第4引数 スタックサイズ(アライメント調整済み)

  
(5-4) ALLOC_SHARED_USTACK_IDLE(...)
		アイドルタスクかつ基本タスクの共有スタックの確保関数
	第1引数 共有スタック名
	第2引数 共有スタックID(タスク優先度)
	第3引数 スタックサイズ(アライメント調整済み)


６．オフセットファイル関係
prc_offset.tfに以下の記述を追加する．
----------------------------------------
$DEFINE("TINIB_p_scdcb", offsetof_TINIB_p_scdcb)$
----------------------------------------

上記にあわせて，prc_def.csvに以下の記述を追加する．
----------------------------------------
offsetof_TINIB_p_scdcb,"offsetof(TINIB,p_scdcb)"
----------------------------------------
７．リファレンス

ATK2-SC1-TPから変更なし．

以上
