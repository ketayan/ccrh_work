/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004-2016 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2004-2016 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: timewindow.c 636 2016-03-22 01:35:30Z witz-itoyo $
 */

/*
 *		timewindow制御モジュール
 */

#include "kernel_impl.h"
#include "osap.h"
#include "task.h"
#include "timewindow.h"
#include "interrupt.h"

LOCAL_INLINE void change_timewindow(void);
LOCAL_INLINE void systemcycle_expire(void);
LOCAL_INLINE void timewindow_expire(void);

#ifdef TOPPERS_timewindow_initialize

/*
 *  現在実行中のタイムウィンドウ
 */
uint32 current_timewindow;

/*
 *  タイムウィンドウ制御モジュール初期化処理
 */
void
timewindow_initialize(void)
{
	target_initialize_systemcycle_timer();
	target_initialize_timewindow_timer();

	current_timewindow = 0U;
	target_set_timewindow_timer(twinib_table[current_timewindow].budget);
}

#endif /* TOPPERS_timewindow_initialize */

/*
 *  TP用タイマ開始処理
 */
#ifdef TOPPERS_start_tp_timer

void
start_tp_timer(void)
{
	target_start_systemcycle_timer();
	target_start_timewindow_timer();
}

#endif /* TOPPERS_start_tp_timer */

/*
 *  TP用タイマ終了処理
 */
#ifdef TOPPERS_stop_tp_timer

void
stop_tp_timer(void)
{
	target_stop_systemcycle_timer();
	target_intclear_systemcycle_timer();
	target_stop_timewindow_timer();
	target_intclear_timewindow_timer();
}

#endif /* TOPPERS_stop_tp_timer */

/* タイムウィンドウ切り替え処理 */
LOCAL_INLINE void
change_timewindow(void)
{
	p_curtwschedom = twinib_table[current_timewindow].p_scdcb;

	target_set_timewindow_timer(twinib_table[current_timewindow].budget);
	target_start_timewindow_timer();
}


/*
 *  システム周期満了処理
 */
LOCAL_INLINE void
systemcycle_expire(void)
{
	ISRType	i;
	ISRCB	*p_isrcb;

	/* p_curtwschedmn がNULLでない場合，実行中のタイムウィンドウがあるということ */
	if (p_curtwschedom != p_idleschedom) {
		/* TPでは，システム周期満了時に現在実行中のタイムウインドウが存在する場合 */
		/* シャットダウンを行うため，pre_protection_supervisedをTRUEとする */
		pre_protection_supervised = TRUE;
		call_protectionhk_main(E_OS_PROTECTION_TIMEWINDOW);
	}

	/* 各C2ISRの割込み発生回数をクリア */
	for (i = 0U; i < tnum_isr2; i++) {
		p_isrcb = &isrcb_table[i];
		p_isrcb->occurrence_count = p_isrcb->overrun_count;
		p_isrcb->overrun_count = 0U;
	}

	/* 割込みクリア */
	target_intclear_systemcycle_timer();

	current_timewindow = 0U;
	change_timewindow();
	update_tschedtsk();
}

/*
 *  タイムウィンドウ切り替え処理
 */
LOCAL_INLINE void
timewindow_expire(void)
{

	/* 割込みクリア */
	target_intclear_timewindow_timer();

	/* タイムウィンドウを更新 */
	current_timewindow++;
	if (current_timewindow < tnum_timewindow) {
		change_timewindow();
	}
	/* タイムウィンドウの総量を超えたらアイドルウィンドウとする */
	else {
		p_curtwschedom = p_idleschedom;
	}
	update_tschedtsk();
}

/*
 *  TP用割込みハンドラtw_interruptから呼び出されるハンドラ
 *  割込み番号によって，タイムウィンドウ満了処理とシステム周期満了処理を呼び出す
 */
#ifdef TOPPERS_tp_int_handler
void
tp_int_handler(uint32 intno)
{
	if (intno == TIMEWINDOWTIMER_INTNO) {
		timewindow_expire();
	}
	/*
	 * 引数intnoはTIMEWINDOWTIMER_INTNOもしくは
	 * SYSTEMCYCLETIMER_INTNOしか受け取らないためelseとする
	 */
	else {
		systemcycle_expire();
	}
}
#endif /* TOPPERS_tp_int_handler */
