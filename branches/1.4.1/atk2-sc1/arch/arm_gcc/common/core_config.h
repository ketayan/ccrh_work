/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2006-2016 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2016 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: core_config.h 636 2016-03-22 01:35:30Z witz-itoyo $
 */

/*
 *		コア依存モジュール（ARM用）
 *
 *  このインクルードファイルは，target_config.h（または，そこからインク
 *  ルードされるファイル）のみからインクルードされる．他のファイルから
 *  直接インクルードしてはならない
 */

#ifndef TOPPERS_CORE_CONFIG_H
#define TOPPERS_CORE_CONFIG_H

/*
 *  ARM依存の定義
 */
#include "arm.h"


/*
 *  エラーチェック方法の指定
 */
#define CHECK_STKSZ_ALIGN	8   /* スタックサイズのアライン単位 */
#define CHECK_FUNC_ALIGN	4   /* 関数のアライン単位 */
#define CHECK_FUNC_NONNULL      /* 関数の非NULLチェック */
#define CHECK_STACK_ALIGN	4   /* スタック領域のアライン単位 */
#define CHECK_STACK_NONNULL     /* スタック領域の非NULLチェック */

/*
 *  全割込み禁止とするCPSRのパターン
 */
#define CPSR_INTLOCK		(CPSR_IRQ_BIT)

/*
 *  CPSRに常にセットするパターン
 */
#ifndef CPSR_ALWAYS_SET
#define CPSR_ALWAYS_SET	0x00
#endif /* CPSR_ALWAYS_SET */

/*
 *  例外の番号
 */
#define EXCH_NO_RESET	0
#define EXCH_NO_UNDEF	1
#define EXCH_NO_SWI		2
#define EXCH_NO_PABORT	3
#define EXCH_NO_DABORT	4
#define EXCH_NO_IRQ		5
#define EXCH_NO_FIQ		6

/*
 *  例外の個数
 */
#define TNUM_EXCH			7

#define call_errorhook(ercd, svcid)	stack_change_and_call_func_2((void *) internal_call_errorhook, (ercd), (svcid))
#define call_shutdownhook(ercd)	stack_change_and_call_func_1((void *) internal_call_shtdwnhk, (ercd))

#ifndef TOPPERS_MACRO_ONLY

/*
 *  非タスクコンテキスト用のスタック初期値
 */
#define TOPPERS_OSTKPT(stk, stksz)	((StackType *) ((sint8 *) (stk) + (stksz)))

/*
 *  プロセッサの特殊命令のインライン関数定義
 */
#include "core_insn.h"

/*
 *  タスクコンテキストブロックの定義
 */
typedef struct task_context_block {
	void			*sp;            /* スタックポインタ */
	FunctionRefType	pc;             /* プログラムカウンタ */
} TSKCTXB;


/*
 *  TOPPERS標準割込み処理モデルの実現
 *
 *  ARM依存部では，OS割込み禁止状態
 *  のみを取り扱う．割込み優先度マスク，割込み要求禁止フラグに関して
 *  は，各ターゲット依存部で取り扱う
 */

/*
 *  コンテキスト参照のための変数
 */
extern uint32			except_nest_cnt; /* 例外（割込み/CPU例外）のネスト回数のカウント */

/*
 * x_nested_lock_os_int()のネスト回数
 * アクセスの順序が変化しないよう，volatile を指定．
 */
extern volatile uint8	nested_lock_os_int_cnt;

extern void x_nested_lock_os_int(void);
extern void x_nested_unlock_os_int(void);
extern void x_lock_all_int(void);
extern void x_unlock_all_int(void);

#define x_suspend_lock_os_int	x_nested_lock_os_int
#define x_resume_unlock_os_int	x_nested_unlock_os_int

/*
 *  タスクディスパッチャ
 */

/*
 *  最高優先順位タスクへのディスパッチ（core_support.S）
 *
 *  dispatchは，タスクコンテキストから呼び出されたサービスコール処理か
 *  ら呼び出すべきもので，タスクコンテキスト・OS割込み禁止状態・ディスパッ
 *  チ許可状態・（モデル上の）割込み優先度マスク全解除状態で呼び出さな
 *  ければならない
 */
extern void dispatch(void);

/*
 *  ディスパッチャの動作開始（core_support.S）
 *
 *  start_dispatchは，カーネル起動時に呼び出すべきもので，すべての割込
 *  みを禁止した状態（全割込み禁止状態と同等の状態）で呼び出さなければ
 *  ならない
 */
extern void start_dispatch(void) NoReturn;

/*
 *  現在のコンテキストを捨ててディスパッチ（core_support.S）
 *
 *  exit_and_dispatchは，TerminateTaskから呼び出すべきもので，タスクコンテキ
 *  スト・OS割込み禁止状態（モデル上の）割込み優先度マスク全解除状態で呼び出
 *  さなければならない
 */
extern void exit_and_dispatch(void) NoReturn;


/*
 *  タスクコンテキストの初期化
 *
 *  タスクが休止状態から実行できる状態に移行する時に呼ばれる．この時点
 *  でスタック領域を使ってはならない
 *
 *  activate_contextを，インライン関数ではなくマクロ定義としているのは，
 *  この時点ではTCBが定義されていないためである
 */
extern void start_r(void);

#define activate_context(p_tcb)											  \
	{																	  \
		(p_tcb)->tskctxb.sp = (void *) ((char8 *) ((p_tcb)->p_tinib->stk) \
										+ (p_tcb)->p_tinib->stksz);		  \
		(p_tcb)->tskctxb.pc = (FunctionRefType) start_r;				  \
	}

/*
 *  プロセッサ依存の初期化
 */
extern void core_initialize(void);

/*
 *  プロセッサ依存の終了時処理
 */
extern void core_terminate(void);

/*
 *  ベクターテーブルの命令から参照されるジャンプ先アドレス
 * （start.S）
 */
extern void	*vector_ref_tbl;

/*
 *  例外ハンドラ
 */

/*
 *  未定義命令 例外ハンドラ（core_support.S）
 */
extern void undef_handler(void);

/*
 *  SWI 例外ハンドラ（core_support.S）
 */
extern void swi_handler(void);

/*
 *  プリフェッチアボード 例外ハンドラ（core_support.S）
 */
extern void prefetch_handler(void);

/*
 *  データアボード 例外ハンドラ（core_support.S）
 */
extern void data_abort_handler(void);

/*
 *  FIQ 例外ハンドラ（core_support.S）
 */
extern void fiq_handler(void);

extern void stack_change_and_call_func_1(void *func, uint32 arg1);
extern void stack_change_and_call_func_2(void *func, uint32 arg1, uint32 arg2);

#endif /* TOPPERS_MACRO_ONLY */
#endif /* TOPPERS_CORE_CONFIG_H */
