/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2016 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2016 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: prc_sil.h 636 2016-03-22 01:35:30Z witz-itoyo $
 */

/*
 *		メモリ及び周辺アドレスアクセスためのプロセッサ依存部（Nios2用）
 */

#ifndef TOPPERS_PRC_SIL_H
#define TOPPERS_PRC_SIL_H

#include "nios2_system.h"

/*
 *  プロセッサのエンディアン
 */
#define SIL_ENDIAN_LITTLE

/*
 *	Mutex関連
 */
#define SIL_MUTEX_MUTEX_OFFSET			UINT_C(0x00)
#define SIL_MUTEX_MUTEX_OWNER_OFFSET	UINT_C(16)
#define SIL_MUTEX_VALUE_DATA			UINT_C(0x1234)
#define SIL_MUTEX_RESET_RESET_MASK		UINT_C(0x01)
#define SIL_MUTEX_RESET_OFFSET			UINT_C(0x04)
#define SIL_MUTEX_MUTEX_OWNER_MASK		UINT_C(0xffff0000)

#ifndef TOPPERS_MACRO_ONLY

/*
 *  NMIを除くすべての割込みの禁止
 */
LOCAL_INLINE uint8
TOPPERS_disint(void)
{
	uint32 TOPPERS_status;

	Asm("rdctl %0, status" : "=r" (TOPPERS_status));
	Asm("wrctl status, %0" :: "r" (TOPPERS_status & (~(uint32) (0x0001U))) : "memory");
	return((uint8) (TOPPERS_status & (0x0001U)));
}

/*
 *  割込み優先度マスク（内部表現）の現在値の設定
 */
LOCAL_INLINE void
TOPPERS_set_pie(uint8 TOPPERS_pie_bit)
{
	uint32 TOPPERS_status;

	Asm("rdctl %0, status" : "=r" (TOPPERS_status));
	TOPPERS_status = TOPPERS_status & (~(0x0001U));
	TOPPERS_status = TOPPERS_status | ((uint32) TOPPERS_pie_bit & (0x0001U));
	Asm("wrctl status, %0" :: "r" (TOPPERS_status) : "memory");
}

/*
 *  I/O空間アクセス関数郡
 *  Nios2はI/O空間とメモリ空間は区別しないが，キャッシュを無効にするため，
 *  I/Oアクセス時はこれらの関数を用いることとする
 */

/*
 *  32ビット単位の読出し／書込み
 */
LOCAL_INLINE uint32
sil_rew_iop(void *mem)
{
	return((uint32) __builtin_ldwio(mem));
}

LOCAL_INLINE void
sil_wrw_iop(void *mem, uint32 data)
{
	__builtin_stwio(mem, data);
}

/*
 *  コアIDの取得
 */
LOCAL_INLINE uint32
sil_get_coreid(void)
{
	uint32 status;

	Asm("rdctl %0, cpuid" : "=r" (status));

	return((status >> 20) & 0x000000fU);
}

/*
 *  割込み禁止を伴うスピンロックの取得
 */
LOCAL_INLINE uint8
TOPPERS_spn_lock(void)
{
	uint32			data;
	uint32			check;
	volatile uint8	status;

	data = (sil_get_coreid() << SIL_MUTEX_MUTEX_OWNER_OFFSET) | SIL_MUTEX_VALUE_DATA;

  retry:
	/* 割込み状態を取得した上，禁止状態へ */
	status = TOPPERS_disint();
	sil_wrw_iop((void *) (SIL_MUTEX_BASE + SIL_MUTEX_MUTEX_OFFSET), data);
	check = sil_rew_iop((void *) (SIL_MUTEX_BASE + SIL_MUTEX_MUTEX_OFFSET));

	if (data != check) {
		/* スピンロックが取得できなかった場合 */
		/* 割込み状態を前の状態へ */
		TOPPERS_set_pie(status);
		goto retry;
	}

	Asm("" ::: "memory");

	return(status);
}

/*
 *  スピンロックの返却
 */
LOCAL_INLINE void
TOPPERS_spn_unlock(uint8 status)
{
	sil_wrw_iop((void *) (SIL_MUTEX_BASE + SIL_MUTEX_MUTEX_OFFSET),
				(sil_get_coreid() << SIL_MUTEX_MUTEX_OWNER_OFFSET));

	/* スピンロックの取得前の割込みの禁止状態へ */
	TOPPERS_set_pie(status);
}

/*
 *  全割込みロック状態の制御
 */
#define SIL_PRE_LOC		uint8 TOPPERS_pie_bit
#define SIL_LOC_INT()	(TOPPERS_pie_bit = TOPPERS_disint())
#define SIL_UNL_INT()	(TOPPERS_set_pie(TOPPERS_pie_bit))
#define SIL_LOC_SPN()	(TOPPERS_pie_bit = TOPPERS_spn_lock())
#define SIL_UNL_SPN()	(TOPPERS_spn_unlock(TOPPERS_pie_bit))

#endif /* TOPPERS_MACRO_ONLY */

#endif /* TOPPERS_PRC_SIL_H */
