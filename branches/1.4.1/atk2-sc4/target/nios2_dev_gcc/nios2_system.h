/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2011-2016 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2016 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: nios2_system.h 636 2016-03-22 01:35:30Z witz-itoyo $
 */

/*
 *		Nios2システムの定義
 *
 *  ペリフェラルのアドレスや割込み番号を定義する
 */

#ifndef TOPPERS_NIOS2_SYSTEM_H
#define TOPPERS_NIOS2_SYSTEM_H

/*
 *  メモリマップ
 *  BG_CODE_START コード先頭アドレス
 *  BG_CODE_END   コード終端アドレス
 *  BG_DATA_START データ先頭アドレス
 *  BG_DATA_END   データ終端アドレス
 */

#define BG_CODE_START	UINT_C(0x00000000)
#define BG_CODE_END		UINT_C(0xffffffff)
#define BG_DATA_START	UINT_C(0x00000000)
#define BG_DATA_END		UINT_C(0xffffffff)

#ifdef TOPPERS_NIOS2_DEV_2S180

/*
 *  NIOS2_DEV_2S180 用の定義
 */

/*
 *  起動メッセージのターゲットシステム名
 */
#define TARGET_NAME	"NIOS2_DEV_2S180(NIOS2)"

/*
 *  タイマ値の内部表現とミリ秒単位との変換
 */
#define TIMER_CLOCK		UINT_C(50000)

/*
 * メモリマップの定義
 */
#define RESET_START_ADDRESS		UINT_C(0x05000000)
#define RESET_SIZE				UINT_C(0x00000020)
#define EXCEPT_START_ADDRESS	UINT_C(0x05000020)
#define EXCEPT_SIZE				UINT_C(0x00001000)
#ifdef PLACE_SDRAM
#define ROM_START_ADDRESS		UINT_C(0x0a000000)
#define ROM_SIZE				UINT_C(0x00800000)
#define RAM_START_ADDRESS		UINT_C(0x0a800000)
#define RAM_SIZE				UINT_C(0x00800000)
#else /* PLACE_SDRAM */
#define ROM_START_ADDRESS		UINT_C(0x05001020)
#define ROM_SIZE				UINT_C(0x0003efe0)
#define RAM_START_ADDRESS		UINT_C(0x05040000)
#define RAM_SIZE				UINT_C(0x00040000)
#endif /* PLACE_SDRAM */

/*
 *  キャッシュサイズ
 */
#define NIOS2_ICACHE_SIZE			UINT_C(0x1000)   /* 4Kbyte */
#define NIOS2_ICACHE_LINE_SIZE		UINT_C(32)
#define NIOS2_DCACHE_SIZE			UINT_C(0)
#define NIOS2_DCACHE_LINE_SIZE		UINT_C(0)

/*
 *  Interval Timer
 */
#define SYS_CLK_TIMER_BASE	UINT_C(0x01001000)
#define SYS_CLK_TIMER_INT	UINT_C(0)

/*
 *  JTAG UART
 */
#define JTAG_UART_PORT1_BASE	UINT_C(0x010008a0)
#define JTAG_UART_PORT1_INT		UINT_C(1)

/*
 *  システムバージョンレジスタのベースアドレス
 */
#define SYSVER_BASE				UINT_C(0x0f000000)

/*
 *  システムバージョン参照値
 */
#define TNUM_HWCORE				UINT_C(1)           /* コア数 */
#define MAJOR_VAR				UINT_C(5)           /* メジャーバージョン */

/*
 *  ベクタ割込みコントローラ
 */
#define VIC_BASE				UINT_C(0x01000c00)  /* ベースアドレス */
#define VIC_INT_NUM				UINT_C(32)

/*
 *  タイマクロック周波数（Hz）50MHz
 */
#define TIMER_CLOCK_HZ		((uint32) 50000000)

/*
 *  NIOS2_DEV_2S180 用の定義割込み番号
 */
#define TIMER_0_IRQ			UINT_C(16)
#define TIMER_1_IRQ			UINT_C(17)
#define TIMER_2_IRQ			UINT_C(18)
#define TIMER_3_IRQ			UINT_C(19)
#define TIMER_4_IRQ			UINT_C(20)
#define TIMER_5_IRQ			UINT_C(21)
#define TIMER_6_IRQ			UINT_C(22)
#define TIMER_7_IRQ			UINT_C(23)
#define TIMER_8_IRQ			UINT_C(24)
#define TIMER_9_IRQ			UINT_C(25)
#define TIMER_10_IRQ		UINT_C(26)
#define TIMER_11_IRQ		UINT_C(27)
#define TIMER_12_IRQ		UINT_C(28)
#define TIMER_13_IRQ		UINT_C(29)
#define TIMER_14_IRQ		UINT_C(30)
#define TIMER_15_IRQ		UINT_C(31)

/*
 *  NIOS2_DEV_2S180 用の定義タイマレジスタベース
 */
#define TIMER_BASE(INTNO)	(UINT_C(0x01001020) + (UINT_C(0x20) * ((INTNO) - UINT_C(16))))

/*
 *  Nios2 MPU 数
 */
#define NIOS2_MPU_INST_MAX_NUM	UINT_C(32)
#define NIOS2_MPU_DATA_MAX_NUM	UINT_C(32)

#elif defined(TOPPERS_NIOS2_DEV_DE2_115)

/*
 *  NIOS2_DEV_DE2_115 用の定義
 */

/*
 *  起動メッセージのターゲットシステム名
 */
#define TARGET_NAME	"NIOS2_DEV_DE2_115(NIOS2)"

/*
 *  タイマ値の内部表現とミリ秒単位との変換
 */
#define TIMER_CLOCK		UINT_C(60000)

/*
 *  メモリマップの定義
 */
#define RESET_START_ADDRESS		UINT_C(0x05000000)
#define RESET_SIZE				UINT_C(0x00000020)
#define EXCEPT_START_ADDRESS	UINT_C(0x00000020)
#define EXCEPT_SIZE				UINT_C(0x00001000)
#ifdef PLACE_SDRAM
#define ROM_START_ADDRESS		UINT_C(0x00001020)
#define ROM_SIZE				UINT_C(0x00ffefe0)
#define RAM_START_ADDRESS		UINT_C(0x01000000)
#define RAM_SIZE				UINT_C(0x01000000)
#else /* PLACE_SDRAM */
#define ROM_START_ADDRESS		UINT_C(0x0A000000)
#define ROM_SIZE				UINT_C(0x00018000)
#define RAM_START_ADDRESS		UINT_C(0x0A018000)
#define RAM_SIZE				UINT_C(0x00008000)
#endif /* PLACE_SDRAM */

/*
 *  キャッシュサイズ
 */
#define NIOS2_ICACHE_SIZE			UINT_C(0x1000)   /* 4Kbyte */
#define NIOS2_ICACHE_LINE_SIZE		UINT_C(32)
#define NIOS2_DCACHE_SIZE			UINT_C(0)
#define NIOS2_DCACHE_LINE_SIZE		UINT_C(0)

/*
 *  Interval Timer
 */
#define SYS_CLK_TIMER_BASE	UINT_C(0x08000160)
#define SYS_CLK_TIMER_INT	UINT_C(0)

/*
 *  JTAG UART
 */
#define JTAG_UART_PORT1_BASE	UINT_C(0x080001a0)
#define JTAG_UART_PORT1_INT		UINT_C(1)

/*
 *  UART
 */
#define UART_PORT1_BASE	UINT_C(0x02000d00)
#define UART_PORT1_INT	UINT_C(3)

/*
 *  システムバージョンレジスタのベースアドレス
 */
#define SYSVER_BASE				UINT_C(0x080001c0)

/*
 *  システムバージョン参照値
 */
#define TNUM_HWCORE				UINT_C(1)           /* コア数 */
#define MAJOR_VAR				UINT_C(7)           /* メジャーバージョン */

/*
 *  ベクタ割込みコントローラ
 */
#define VIC_BASE				UINT_C(0x0f000000)  /* ベースアドレス */
#define VIC_INT_NUM				UINT_C(32)

/*
 *  タイマクロック周波数（Hz）60MHz
 */
#define TIMER_CLOCK_HZ		((uint32) 60000000)

/*
 *  NIOS2_DEV_DE2_115 用の定義割込み番号
 */
#define TIMER_0_IRQ			UINT_C(16)
#define TIMER_1_IRQ			UINT_C(17)
#define TIMER_2_IRQ			UINT_C(18)
#define TIMER_3_IRQ			UINT_C(19)
#define TIMER_4_IRQ			UINT_C(20)
#define TIMER_5_IRQ			UINT_C(21)
#define TIMER_6_IRQ			UINT_C(22)
#define TIMER_7_IRQ			UINT_C(23)
#define TIMER_8_IRQ			UINT_C(24)
#define TIMER_9_IRQ			UINT_C(25)
#define TIMER_10_IRQ		UINT_C(26)
#define TIMER_11_IRQ		UINT_C(27)
#define TIMER_12_IRQ		UINT_C(28)
#define TIMER_13_IRQ		UINT_C(29)
#define TIMER_14_IRQ		UINT_C(30)
#define TIMER_15_IRQ		UINT_C(31)

/*
 *  NIOS2_DEV_DE2_115 用の定義タイマレジスタベース
 */
#define TIMER_BASE(INTNO)	(UINT_C(0x08000000) + (UINT_C(0x00000020) * (INTNO)))

/*
 * Nios2 MPU 数
 */
#define NIOS2_MPU_INST_MAX_NUM	UINT_C(16)
#define NIOS2_MPU_DATA_MAX_NUM	UINT_C(16)

#endif /* TOPPERS_NIOS2_DEV_DE2_115 */

/*
 *  システムバージョンレジスタ(sysver)
 */
#define SYSVER_MAJOR_VAR	(SYSVER_BASE + 0x00U)
#define SYSVER_MINOR_VAR	(SYSVER_BASE + 0x04U)
#define SYSVER_NUM_CORE		(SYSVER_BASE + 0x08U)

/*
 *  カウンタ最大値
 */
#define MAX_TIMER_CNT		((uint32) 0xFFFFFFFF)

/*
 *  カウンタ周期最大値
 */
#define MAX_CNT_CYCLE		((uint32) 0x7FFFFFFF)

/*
 *  タイマstatusレジスタのビット定義
 */
#define TIMER_STATUS_TO			(UINT_C(0x00000001))
#define TIMER_STATUS_RUN		(UINT_C(0x00000002))

/*
 *  タイマcontrolレジスタのビット定義
 */
#define TIMER_CONTROL_ITO		(UINT_C(0x00000001))
#define TIMER_CONTROL_CONT		(UINT_C(0x00000002))
#define TIMER_CONTROL_START		(UINT_C(0x00000004))
#define TIMER_CONTROL_STOP		(UINT_C(0x00000008))

/*
 *  TIMER関連レジスタ定義
 */
#define TIMER_STATUS(INTNO)		(TIMER_BASE(INTNO) + UINT_C(0x00000000))
#define TIMER_CONTROL(INTNO)	(TIMER_BASE(INTNO) + UINT_C(0x00000004))
#define TIMER_PERIODL(INTNO)	(TIMER_BASE(INTNO) + UINT_C(0x00000008))
#define TIMER_PERIODH(INTNO)	(TIMER_BASE(INTNO) + UINT_C(0x0000000C))
#define TIMER_SNAPL(INTNO)		(TIMER_BASE(INTNO) + UINT_C(0x00000010))
#define TIMER_SNAPH(INTNO)		(TIMER_BASE(INTNO) + UINT_C(0x00000014))

#endif /* TOPPERS_NIOS2_SYSTEM_H */
