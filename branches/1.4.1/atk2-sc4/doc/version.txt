
		TOPPERS/ATK2-SC4
        ＜変更履歴＞

このドキュメントは，ATK2-SC4のRelease 1.4.0以降の変更履歴を，新しい方
から順に記述したものである．

----------------------------------------------------------------------
TOPPERS ATK2
    Toyohashi Open Platform for Embedded Real-Time Systems
    Automotive Kernel Version 2

Copyright (C) 2011-2016 by Center for Embedded Computing Systems
            Graduate School of Information Science, Nagoya Univ., JAPAN
Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
Copyright (C) 2011-2013 by Spansion LLC, USA
Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
Copyright (C) 2011-2016 by Witz Corporation
Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION

上記著作権者は，以下の (1)〜(3)の条件を満たす場合に限り，本ドキュメ
ント（本ドキュメントを改変したものを含む．以下同じ）を使用・複製・改
変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
(1) 本ドキュメントを利用する場合には，上記の著作権表示，この利用条件
    および下記の無保証規定が，そのままの形でドキュメント中に含まれて
    いること．
(2) 本ドキュメントを改変する場合には，ドキュメントを改変した旨の記述
    を，改変後のドキュメント中に含めること．ただし，改変後のドキュメ
    ントが，TOPPERSプロジェクト指定の開発成果物である場合には，この限
    りではない．
(3) 本ドキュメントの利用により直接的または間接的に生じるいかなる損害
    からも，上記著作権者およびTOPPERSプロジェクトを免責すること．また，
    本ドキュメントのユーザまたはエンドユーザからのいかなる理由に基づ
    く請求からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

本ドキュメントは，AUTOSAR（AUTomotive Open System ARchitecture）仕様
に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するものではな
い．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利用する
者に対して，AUTOSARパートナーになることを求めている．

本ドキュメントは，無保証で提供されているものである．上記著作権者およ
びTOPPERSプロジェクトは，本ドキュメントに関して，特定の使用目的に対す
る適合性も含めて，いかなる保証も行わない．また，本ドキュメントの利用
により直接的または間接的に生じたいかなる損害に関しても，その責任を負
わない．

$Id: version.txt 640 2016-03-24 03:04:34Z nces-hibino $
----------------------------------------------------------------------
		ATK2-SC4
		Release 1.4.0 から 1.4.1 への変更点
		リリース日 2016-03-24

○変更点のリスト

・GetCurrentApplicationIDの実装
・カーネル起動時にすべてのシステムスタックのマジックナンバー領域を初期化し，
　タスク起動時毎のマジックナンバー初期化を削除

・エラーフックから動作中OSAPを強制終了できるよう修正

・シャットダウンフック実行中のOSAPを判別できるよう修正

・IOCのシステムサービス時のエラーチェック機構(CHECK_IOC_ACCESS)にて
　確認するOSAPIDをGetApplicationIDからGetCurrentApplicationIDに変更

・ioc_xxx_genericに不正なメモリアドレスを渡した場合，
　エラーフックを呼び出し，E_OS_SYS_ILLEGAL_ADDRESSを返すように変更

・アプリケーションの状態がACCESSIBLEでない場合のE_OS_ACCESSを拡張エラーから標準エラーに変更

・IOCのプロパティでMACROが指定された場合，
　関数宣言をターゲット依存部で置きかえれるように修正
----------------------------------------------------------------------
		ATK2-SC4
		Release 1.4.0
		リリース日 2015-12-25

◯Release 1.4.0のリリース

----------------------------------------------------------------------
