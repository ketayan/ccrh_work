
		TOPPERS/ATK2-SC4
		ターゲット依存部 ポーティングガイド

このドキュメントは，TOPPERS/ATK2-SC4を，未サポートのターゲットシステム
にポーティングするために必要となるターゲット依存部の実装方法を説明する
ものである．
ATK2-SC3のポーティングガイドと内容が重複する部分については，記載を省略
する．

----------------------------------------------------------------------
TOPPERS ATK2
    Toyohashi Open Platform for Embedded Real-Time Systems
    Automotive Kernel Version 2

Copyright (C) 2011-2016 by Center for Embedded Computing Systems
            Graduate School of Information Science, Nagoya Univ., JAPAN
Copyright (C) 2011-2016 by FUJI SOFT INCORPORATED, JAPAN
Copyright (C) 2011-2013 by Spansion LLC, USA
Copyright (C) 2011-2016 by NEC Communication Systems, Ltd., JAPAN
Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
Copyright (C) 2011-2016 by TOSHIBA CORPORATION, JAPAN
Copyright (C) 2011-2016 by Witz Corporation
Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
Copyright (C) 2014-2016 by SCSK Corporation, JAPAN
Copyright (C) 2015-2016 by SUZUKI MOTOR CORPORATION

上記著作権者は，以下の (1)〜(3)の条件を満たす場合に限り，本ドキュメ
ント（本ドキュメントを改変したものを含む．以下同じ）を使用・複製・改
変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
(1) 本ドキュメントを利用する場合には，上記の著作権表示，この利用条件
    および下記の無保証規定が，そのままの形でドキュメント中に含まれて
    いること．
(2) 本ドキュメントを改変する場合には，ドキュメントを改変した旨の記述
    を，改変後のドキュメント中に含めること．ただし，改変後のドキュメ
    ントが，TOPPERSプロジェクト指定の開発成果物である場合には，この限
    りではない．
(3) 本ドキュメントの利用により直接的または間接的に生じるいかなる損害
    からも，上記著作権者およびTOPPERSプロジェクトを免責すること．また，
    本ドキュメントのユーザまたはエンドユーザからのいかなる理由に基づ
    く請求からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

本ドキュメントは，AUTOSAR（AUTomotive Open System ARchitecture）仕様
に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するものではな
い．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利用する
者に対して，AUTOSARパートナーになることを求めている．

本ドキュメントは，無保証で提供されているものである．上記著作権者およ
びTOPPERSプロジェクトは，本ドキュメントに関して，特定の使用目的に対す
る適合性も含めて，いかなる保証も行わない．また，本ドキュメントの利用
により直接的または間接的に生じたいかなる損害に関しても，その責任を負
わない．

$Id: porting.txt 636 2016-03-22 01:35:30Z witz-itoyo $
----------------------------------------------------------------------

○目次

０．ATK2-SC3とATK2-SC4の差分
１．タイミング保護用タイマドライバ
	1.1 タイミング保護用タイマに関する定数定義
	1.2 タイミング保護用タイマドライバ
２．プロセッサ依存モジュール
３．ディスパッチャ
４．割込みハンドラの出入口処理
５．SC2割込みハンドラの出入口処理
６．テンプレートファイルのターゲット依存部
７．リファレンス

０．ATK2-SC3とATK2-SC4の差分
SC4のポーティングでは，以下のターゲット依存部を新規で実装する必要がある

１．タイミング保護用タイマドライバ
	
	ATK2-SC2から変更なし．
	
２．プロセッサ依存モジュール

	ATK2-SC2から変更なし．

３．ディスパッチャ

	ATK2-SC2から変更なし．

４．割込みハンドラの出入口処理
	
	ATK2-SC2から変更なし．

５．SC2割込みハンドラの出入口処理
・int_tp_fault_handler
	ATK2-SC2から変更なし．
・int_tp_timer_handler
+	・非特権モードで割込み発生した場合，
			スタックを実行状態のタスクのシステムスタックに切り換える
	・レジスタ保存
	…
	
６．テンプレートファイルのターゲット依存部
6.1 秒からクロック値への変換処理
	ATK2-SC2から変更なし．
6.2 C2ISRの優先度下限の変更
	ATK2-SC2から変更なし．
6.3 ベクタテーブルからSC2割込みハンドラの出入口処理への分岐処理
	ATK2-SC2から変更なし．
７．リファレンス
	ATK2-SC2から変更なし．
以上
