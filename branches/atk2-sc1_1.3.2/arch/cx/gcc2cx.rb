#! ruby -Ke

if ARGV.size != 1 then
    puts "Argment Error!"
    exit
end

#Input from gcc file
begin
    gcc_asm_file = open(ARGV[0])
rescue
    puts "File Open Error!"
    exit
end


cx_asm = ""

#Convert 
while line = gcc_asm_file.gets
    
    line = line.sub('/*', ';/*')
    
    line = line.sub(/^(\s*)\*/, '\1;*')
    
    line = line.sub(/.macro\s+(\w+)/, '\1 .macro')
    
    line = line.sub(/#include\s+[<"](.+)[>"]/, '$include (\1)')
    
    line = line.sub('.global', '.extern')    
    
    line = line.gsub(/AMARG\((\w+)\)/, '\1')
    
    line = line.gsub(/FLABEL\((\w+)\)/, '\1:')
        
    line = line.gsub('~', '!')
    
    line = line.gsub('#ifdef', '$ifdef')
    
    line = line.gsub('#ifndef', '$ifndef')    
    
    line = line.gsub('#endif', '$endif')
    
    line = line.gsub('#else', '$else')    
    
    line = line.gsub('.section', '.cseg')
    
    line = line.gsub(/.endr/i, '.endm')
    
    line = line.gsub(/.text/i, 'text')
        
    cx_asm = cx_asm  + line
end

cx_asm_file_name = File::basename(ARGV[0].sub(".S", ".asm"))
print sprintf("Output %s\n", cx_asm_file_name)

#Output to asm file
File.open(cx_asm_file_name, 'w') {|file|
 file.write cx_asm 
}
