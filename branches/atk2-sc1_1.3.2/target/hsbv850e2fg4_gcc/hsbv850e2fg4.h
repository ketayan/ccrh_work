/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2012-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2012-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2012-2013 by Spansion LLC, USA
 *  Copyright (C) 2012-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2012-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2012-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2012-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: hsbv850e2fg4.h 134 2014-12-18 09:43:43Z ertl-honda $
 */
/*
 *		HSBV850E2FG4ボードの定義
 */

#ifndef TOPPERS_HSBV850E2FG4_H
#define TOPPERS_HSBV850E2FG4_H

#if !(defined(__f4002__) || defined(__f3580__))
#error Target MCU type is not supported!!
#endif

#define USE_BB_SUTDY_IO

#ifdef __f4002__

/*
 *  uPD70F4002 V8502E/FG4
 */

#define V850FG4

/*
 *  起動メッセージのターゲットシステム名
 */
#define TARGET_NAME	"HSBV850E2FG4(V850E2/FG4:uPD70F4002)"

/*
 *  クロック速度
 */
#define PCLOCK			(40000000)

/*
 *  PLL関連の定義
 */
#define MAINOSC_CLOCK	20  /* MainOSC is 20MHz */
#define PLL0_CLOCK		80  /* PLL0 is 80MHz */
#define PLL1_CLOCK		40  /* PLL1 is 40MHz */
#define PLL2_CLOCK		80  /* PLL2 is 80MHz */


/* LED PORT Configuration
     P0_12 : LED1(OUT)
     P0_13 : LED2(OUT)
     P0_14 : LED3(OUT)
     P0_15 : LED4(OUT)
 */
/*
 * LEDが接続されているポート
 */
#define LED_PORT_NO	0

#define LED_PORT_MASK				((uint16) 0xf000)
#define LED_PORT_PMC_INIT			((uint16) 0x0000)
#define LED_PORT_PFCE_INIT			((uint16) 0x0000)
#define LED_PORT_PFC_INIT			((uint16) 0x0000)
#define LED_PORT_PM_INIT			((uint16) 0x0000)
#define LED_PORT_P_INIT				((uint16) 0x0000)

#define LED1_BIT					((uint16) 0x1000)
#define LED2_BIT					((uint16) 0x2000)
#define LED3_BIT					((uint16) 0x4000)
#define LED4_BIT					((uint16) 0x8000)
#define LED_BIT_ALL					((uint16) 0xf000)

/*
 * 使用するUARTEのポート番号
 */
#define TARGET_UARTE_PORT	10

/*
 *  UARTE10が接続されているポート
 */
#define UARTE10_PORT_NO	0

/* Port 0 Configration
     P0_8 : URTET10TX
     P0_9 : URTER10RX
 */
#define UARTE10_P_MASK				((uint16) 0x0300)
#define UARTE10_PMC_INIT			((uint16) 0x0300)
#define UARTE10_PFC_INIT			((uint16) 0x0000)
#define UARTE10_PFCE_INIT			((uint16) 0x0000)
#define UARTE10_PM_INIT				((uint16) 0x0200)

#include "v850_gcc/v850e2_fx4.h"

#endif /* __f4002__ */

#ifdef __f3580__

/*
 *  UPD70F3580GC V8502E/FG4-L
 */

#define V850FG4_L

/*
 *  起動メッセージのターゲットシステム名
 */
#define TARGET_NAME	"HSBV850E2FG4L(V850E2/FG4-L:uPD70F3580)"

/*
 *  クロック速度
 */
#define PCLOCK			(64000000)

/*
 *  PLL関連の定義
 */
#define MAINOSC_CLOCK	20  /* MainOSC is 20MHz */
#define PLL0_CLOCK		64  /* PLL0 is 48MHz */

/*
 * LEDが接続されているポート
 */
#define LED_PORT_NO	0

/* Port 0 Configuration
     P0_12 : LED1(OUT)
     P0_13 : LED2(OUT)
     P4_14 : LED3(OUT)
     P4_15 : LED4(OUT)
 */
#define LED_PORT_MASK				((uint16) 0xf000)
#define LED_PORT_PMC_INIT			((uint16) 0x0000)
#define LED_PORT_PFCE_INIT			((uint16) 0x0000)
#define LED_PORT_PFC_INIT			((uint16) 0x0000)
#define LED_PORT_PM_INIT			((uint16) 0x0000)
#define LED_PORT_P_INIT				((uint16) 0x0000)

#define LED1_BIT					((uint16) 0x1000)
#define LED2_BIT					((uint16) 0x2000)
#define LED3_BIT					((uint16) 0x4000)
#define LED4_BIT					((uint16) 0x8000)
#define LED_BIT_ALL					((uint16) 0xF000)

/*
 * 使用するUARTEのポート番号
 */
#define TARGET_UARTE_PORT	10

/*
 *  UARTE10が接続されているポート
 */
#define UARTE10_PORT_NO	0

/* Port 0 Configration
     P0_8 : URTET10TX
     P0_9 : URTER10RX
 */
#define UARTE10_P_MASK				((uint16) 0x0300)
#define UARTE10_PMC_INIT			((uint16) 0x0300)
#define UARTE10_PFC_INIT			((uint16) 0x0000)
#define UARTE10_PFCE_INIT			((uint16) 0x0000)
#define UARTE10_PM_INIT				((uint16) 0x0200)

#include "v850_gcc/v850e2_fx4l.h"

#endif /* __f3580__ */

#endif /* TOPPERS_HSBV850E2FG4_H */
