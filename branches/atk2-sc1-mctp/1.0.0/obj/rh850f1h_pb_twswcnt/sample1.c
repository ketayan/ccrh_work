#include "Os.h"
#include "t_syslog.h"
#include "t_stdlib.h"
#include "sysmod/serial.h"
#include "sysmod/syslog.h"
#include "sample1.h"
#include "sysmod/uspinlock.h"

#include "sysmod/banner.h"
#include "target_sysmod.h"
#include "target_serial.h"

#define GetAppModeInfo()	(0)

/*
 *  ファイル名，行番号の参照用の変数
 */
extern const char8	*fatal_file_name;   /* ファイル名 */
extern sint32		fatal_line_num;     /* 行番号 */

/*
 *  内部関数プロトタイプ宣言
 */
sint32 main(void);

/*
 *  APIエラーログマクロ
 *
 *  ErrorHookが有効の場合はErrorHookから
 *  エラーログを出力し, ErrorHookが無効の場合は
 *  以下のマクロよりエラーログ出力を行う
 */
#if defined(CFG_USE_ERRORHOOK)
#define error_log(api)	(api)
#else /* !defined( CFG_USE_ERRORHOOK ) */
#define	error_log(api)										   \
	{														   \
		StatusType ercd;									   \
		ercd = api;     /* 各API実行 */						   \
		if (ercd != E_OK) {									   \
			syslog(LOG_INFO, "Error:%d", atk2_strerror(ercd)); \
		}													   \
	}
#endif /* defined( CFG_USE_ERRORHOOK ) */

/*
 *  ユーザメイン関数
 *
 *  アプリケーションモードの判断と，カーネル起動
 */
sint32
main(void)
{
	AppModeType	crt_app_mode;
	StatusType	ercd;
	CoreIdType	i;

	/*
	 *  アプリケーションモードの判断
	 */
	switch (GetAppModeInfo()) {
	case 0:
		crt_app_mode = AppMode1;
		break;
	case 1:
		crt_app_mode = AppMode2;
		break;
	default:
		crt_app_mode = AppMode3;
		break;
	}

	if (GetCoreID() == OS_CORE_ID_MASTER) {
		for (i = 0; i < TNUM_HWCORE; i++) {
			if (i != OS_CORE_ID_MASTER) {
				StartCore(i, &ercd);
			}
		}
				InitSerial();
		/*
		 *  カーネル起動
		 */
		StartOS(crt_app_mode);
	}
	else {
		/*
		 *  カーネル起動
		 */
		StartOS(DONOTCARE);
	}

	while (1) {
	}
}   /* main */

void
busy_loop(void){
	volatile int i;
	for(i = 0; i < 10000000; i++);
}

TASK(Task1_1_1)
{
	syslog(LOG_INFO, "Task1_1_1");
	ActivateTask(Task1_1_2);
	
	while(1){
		
	}
	//error_log(TerminateTask());
}   /* TASK( Task1 ) */

TASK(Task1_1_2)
{
	int i;
	int cnt = 0;

	syslog(LOG_INFO, "Task1_1_2");
	while(1){
		busy_loop();
		syslog(LOG_INFO, "Task1_1_2 : running %d", cnt++);
	}
	//error_log(TerminateTask());
}   /* TASK( Task2 ) */

TASK(Task1_1_3)
{
	syslog(LOG_INFO, "======== Task1_1_3(TimeWindowRelatedCounter) =============");
	error_log(TerminateTask());
}   /* TASK( Task2 ) */


TASK(Task1_2_1)
{
	syslog(LOG_INFO, "Task1_2_1");
	ActivateTask(Task1_2_2);
	
	while(1){

	}
	//error_log(TerminateTask());
}   /* TASK( Task1 ) */

TASK(Task1_2_2)
{
	int i;
	int cnt = 0;
	volatile int tmp;

	syslog(LOG_INFO, "Task1_2_2");
	while(1){
		uGetSpinlock(1);
		busy_loop();
		uReleaseSpinlock(1);
		syslog(LOG_INFO, "Task1_2_2 : running %d", cnt++);
		if((cnt % 4) == 0) {
			RaiseInterCoreInterrupt(Core1_ICI_SYS1);
			RaiseInterCoreInterrupt(Core1_ICI_TW1);
		}
	}
	//error_log(TerminateTask());
}   /* TASK( Task2 ) */


TASK(Task1_3_1)
{
	syslog(LOG_INFO, "Task1_3_1");
	ActivateTask(Task1_3_2);
	
	while(1){
		
	}
	//error_log(TerminateTask1_());
}   /* TASK( Task1_1 ) */

TASK(Task1_3_2)
{
	int i;
	int cnt = 0;

	syslog(LOG_INFO, "Task1_3_2");
	while(1){
		busy_loop();
		syslog(LOG_INFO, "Task1_3_2 : running %d", cnt++);
		if((cnt % 4) == 0) {
			RaiseInterCoreInterrupt(Core1_ICI_SYS1);
			RaiseInterCoreInterrupt(Core1_ICI_TW1);
		}
	}
	//error_log(TerminateTask());
}   /* TASK( Task2 ) */


TASK(Task2_1_1)
{
	syslog(LOG_INFO, "Task2_1_1");
	ActivateTask(Task2_1_2);
	
	while(1){
		
	}
	//error_log(TerminateTask());
}   /* TASK( Task2 ) */

TASK(Task2_1_2)
{
	int i;
	int cnt = 0;

	syslog(LOG_INFO, "Task2_1_2");
	while(1){
		busy_loop();
		syslog(LOG_INFO, "Task2_1_2 : running %d", cnt++);
	}
	//error_log(TerminateTask());
}   /* TASK( Task2 ) */

TASK(Task2_1_3)
{
	syslog(LOG_INFO, "======== Task2_1_3(TimeWindowRelatedCounter) =============");	
	error_log(TerminateTask());
}   /* TASK( Task2 ) */

TASK(Task2_2_1)
{
	syslog(LOG_INFO, "Task2_2_1");
	ActivateTask(Task2_2_2);
	
	while(1){

	}
	//error_log(TerminateTask());
}   /* TASK( Task2 ) */

TASK(Task2_2_2)
{
	int i;
	int cnt = 0;

	syslog(LOG_INFO, "Task2_2_2");
	while(1){
		uGetSpinlock(1);
		busy_loop();
		uReleaseSpinlock(1);
		syslog(LOG_INFO, "Task2_2_2 : running %d", cnt++);
		if((cnt % 4) == 0) {
			RaiseInterCoreInterrupt(Core0_ICI_SYS1);
			RaiseInterCoreInterrupt(Core0_ICI_TW1);
		}
	}
	//error_log(TerminateTask());
}   /* TASK( Task2 ) */


TASK(Task2_3_1)
{
	syslog(LOG_INFO, "Task2_3_1");
	ActivateTask(Task2_3_2);
	
	while(1){
		
	}
	//error_log(TerminateTask2_());
}   /* TASK( Task2_1 ) */

TASK(Task2_3_2)
{
	int i;
	int cnt = 0;

	syslog(LOG_INFO, "Task2_3_2");
	while(1){
		busy_loop();
		syslog(LOG_INFO, "Task2_3_2 : running %d", cnt++);
		if((cnt % 4) == 0) {
			RaiseInterCoreInterrupt(Core0_ICI_SYS1);
			RaiseInterCoreInterrupt(Core0_ICI_TW1);
		}
	}
	//error_log(TerminateTask());
}   /* TASK( Task2 ) */


ICISR(Core0_ICI_SYS1) {
	syslog(LOG_INFO, "/***********************/\nCoer0_ICI_SYS1 run!");
	syslog(LOG_INFO, "Coer0_ICI_1 GetISRID() = %d", GetISRID());
	syslog(LOG_INFO, "Coer0_ICI_1 GetApplicationID() = %d", GetApplicationID());
}

ICISR(Core0_ICI_TW1) {
	syslog(LOG_INFO, "/***********************/\nCoer0_ICI_TW1 run!");
	syslog(LOG_INFO, "Coer0_ICI_1 GetISRID() = %d", GetISRID());
	syslog(LOG_INFO, "Coer0_ICI_1 GetApplicationID() = %d", GetApplicationID());
}

ICISR(Core1_ICI_SYS1) {
	syslog(LOG_INFO, "/***********************/\nCoer1_ICI_SYS1 run!");
	syslog(LOG_INFO, "Coer0_ICI_1 GetISRID() = %d", GetISRID());
	syslog(LOG_INFO, "Coer0_ICI_1 GetApplicationID() = %d", GetApplicationID());
}

ICISR(Core1_ICI_TW1) {
	syslog(LOG_INFO, "/***********************/\nCoer1_ICI_TW1 run!");
	syslog(LOG_INFO, "Coer0_ICI_1 GetISRID() = %d", GetISRID());
	syslog(LOG_INFO, "Coer0_ICI_1 GetApplicationID() = %d", GetApplicationID());
}


/*
 *  エラーフックルーチン
 */
#ifdef CFG_USE_ERRORHOOK
void
ErrorHook(StatusType Error)
{
	/*
	 *  エラー要因ごとのパラメータログ出力
	 */
	switch (OSErrorGetServiceId()) {
	case OSServiceId_ActivateTask:
		syslog(LOG_INFO, "Error:%s=ActivateTask(%d)", atk2_strerror(Error), OSError_ActivateTask_TaskID());
		break;
	case OSServiceId_TerminateTask:
		syslog(LOG_INFO, "Error:%s=TerminateTask()", atk2_strerror(Error));
		break;
	case OSServiceId_ChainTask:
		syslog(LOG_INFO, "Error:%s=ChainTask(%d)", atk2_strerror(Error), OSError_ChainTask_TaskID());
		break;
	case OSServiceId_Schedule:
		syslog(LOG_INFO, "Error:%s=Schedule()", atk2_strerror(Error));
		break;
	case OSServiceId_GetTaskID:
		syslog(LOG_INFO, "Error:%s=GetTaskID(0x%p)", atk2_strerror(Error), OSError_GetTaskID_TaskID());
		break;
	case OSServiceId_GetTaskState:
		syslog(LOG_INFO, "Error:%s=GetTaskState(%d, 0x%p)", atk2_strerror(Error),
			   OSError_GetTaskState_TaskID(), OSError_GetTaskState_State());
		break;
	case OSServiceId_EnableAllInterrupts:
		syslog(LOG_INFO, "Error:%s=EnableAllInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_DisableAllInterrupts:
		syslog(LOG_INFO, "Error:%s=DisableAllInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_ResumeAllInterrupts:
		syslog(LOG_INFO, "Error:%s=ResumeAllInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_SuspendAllInterrupts:
		syslog(LOG_INFO, "Error:%s=SuspendAllInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_ResumeOSInterrupts:
		syslog(LOG_INFO, "Error:%s=ResumeOSInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_SuspendOSInterrupts:
		syslog(LOG_INFO, "Error:%s=SuspendOSInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_GetISRID:
		syslog(LOG_INFO, "Error:%s=GetISRID()", atk2_strerror(Error));
		break;
	case OSServiceId_GetResource:
		syslog(LOG_INFO, "Error:%s=GetResource(%d)", atk2_strerror(Error), OSError_GetResource_ResID());
		break;
	case OSServiceId_ReleaseResource:
		syslog(LOG_INFO, "Error:%s=ReleaseResource(%d)", atk2_strerror(Error), OSError_ReleaseResource_ResID());
		break;
	case OSServiceId_SetEvent:
		syslog(LOG_INFO, "Error:%s=SetEvent(%d, 0x%x)", atk2_strerror(Error),
			   OSError_SetEvent_TaskID(), OSError_SetEvent_Mask());
		break;
	case OSServiceId_ClearEvent:
		syslog(LOG_INFO, "Error:%s=ClearEvent(0x%x)", atk2_strerror(Error), OSError_ClearEvent_Mask());
		break;
	case OSServiceId_GetEvent:
		syslog(LOG_INFO, "Error:%s=GetEvent(%d, 0x%p)", atk2_strerror(Error),
			   OSError_GetEvent_TaskID(), OSError_GetEvent_Event());
		break;
	case OSServiceId_WaitEvent:
		syslog(LOG_INFO, "Error:%s=WaitEvent(0x%x)", atk2_strerror(Error), OSError_WaitEvent_Mask());
		break;
	case OSServiceId_GetAlarmBase:
		syslog(LOG_INFO, "Error:%s=GetAlarmBase(0x%p)", atk2_strerror(Error), OSError_GetAlarmBase_AlarmID());
		break;
	case OSServiceId_GetAlarm:
		syslog(LOG_INFO, "Error:%s=GetAlarm(%d, 0x%p)", atk2_strerror(Error),
			   OSError_GetAlarm_AlarmID(), OSError_GetAlarm_Tick());
		break;
	case OSServiceId_SetRelAlarm:
		syslog(LOG_INFO, "Error:%s=SetRelAlarm(%d, %d, %d)", atk2_strerror(Error),
			   OSError_SetRelAlarm_AlarmID(), OSError_SetRelAlarm_increment(), OSError_SetRelAlarm_cycle());
		break;
	case OSServiceId_SetAbsAlarm:
		syslog(LOG_INFO, "Error:%s=SetAbsAlarm(%d, %d, %d)", atk2_strerror(Error),
			   OSError_SetAbsAlarm_AlarmID(), OSError_SetAbsAlarm_start(), OSError_SetAbsAlarm_cycle());
		break;
	case OSServiceId_CancelAlarm:
		syslog(LOG_INFO, "Error:%s=CancelAlarm(%d)", atk2_strerror(Error), OSError_CancelAlarm_AlarmID());
		break;
	case OSServiceId_StartScheduleTableRel:
		syslog(LOG_INFO, "Error:%s=StartScheduleTableRel(%d, %d)", atk2_strerror(Error),
			   OSError_StartScheduleTableRel_ScheduleTableID(), OSError_StartScheduleTableRel_Offset());
		break;
	case OSServiceId_StartScheduleTableAbs:
		syslog(LOG_INFO, "Error:%s=StartScheduleTableAbs(%d, %d)", atk2_strerror(Error),
			   OSError_StartScheduleTableAbs_ScheduleTableID(), OSError_StartScheduleTableAbs_Start());
		break;
	case OSServiceId_StopScheduleTable:
		syslog(LOG_INFO, "Error:%s=StopScheduleTable(%d)", atk2_strerror(Error), OSError_StopScheduleTable_ScheduleTableID());
		break;
	case OSServiceId_NextScheduleTable:
		syslog(LOG_INFO, "Error:%s=NextScheduleTable(%d, %d)", atk2_strerror(Error),
			   OSError_NextScheduleTable_ScheduleTableID_From(), OSError_NextScheduleTable_ScheduleTableID_To());
		break;
	case OSServiceId_GetScheduleTableStatus:
		syslog(LOG_INFO, "Error:%s=GetScheduleTableStatus(%d, 0x%p)", atk2_strerror(Error),
			   OSError_GetScheduleTableStatus_ScheduleTableID(), OSError_GetScheduleTableStatus_ScheduleStatus());
		break;
	case OSServiceId_GetActiveApplicationMode:
		syslog(LOG_INFO, "Error:%s=GetActiveApplicationMode()", atk2_strerror(Error));
		break;
	case OSServiceId_StartOS:
		syslog(LOG_INFO, "Error:%s=StartOS()", atk2_strerror(Error));
		break;
	case OSServiceId_IncrementCounter:
		syslog(LOG_INFO, "Error:%s=IncrementCounter(%d)", atk2_strerror(Error), OSError_IncrementCounter_CounterID());
		break;
	case OSServiceId_TaskMissingEnd:
		syslog(LOG_INFO, "Error:%s=MissingEnd()", atk2_strerror(Error));
		break;
	default:
		syslog(LOG_INFO, "Error:%s=UnKnownFunc()", atk2_strerror(Error));
		break;
	}

}   /* ErrorHook */
#endif /* CFG_USE_ERRORHOOK */

void
PostTaskHook(void)
{
}   /* PostTaskHook */

void
PreTaskHook(void)
{
}   /* PreTaskHook */

void
StartupHook(void)
{
	CoreIdType coreid = GetCoreID();

#ifdef TOPPERS_ENABLE_SYS_TIMER
	target_timer_initialize();
#endif /* TOPPERS_ENABLE_SYS_TIMER */
	if (coreid == OS_CORE_ID_MASTER) {
		syslog_initialize();
		syslog_msk_log(LOG_UPTO(LOG_INFO));
		InitSerial();
		print_banner();
	}
	else {
		InitSerial();
	}
	syslog(LOG_EMERG, "StartupHook @ core%d", coreid);
}   /* StartupHook */

/*
 *  シャットダウンフックルーチン
 */
#ifdef CFG_USE_SHUTDOWNHOOK
#ifdef TOPPERS_ENABLE_SYS_TIMER
extern void target_timer_terminate(void);
#endif /* TOPPERS_ENABLE_SYS_TIMER */

void
ShutdownHook(StatusType Error)
{
	/* 終了ログ出力 */
	syslog(LOG_INFO, "");
	syslog(LOG_INFO, "Sample System ShutDown");
	syslog(LOG_INFO, "ShutDownCode:%s", atk2_strerror(Error));
	syslog(LOG_INFO, "");

	if (Error == E_OS_SYS_ASSERT_FATAL) {
		syslog(LOG_INFO, "fatal_file_name:%s", fatal_file_name);
		syslog(LOG_INFO, "fatal_line_num:%d", fatal_line_num);
	}
	while(1);
#ifdef TOPPERS_ENABLE_SYS_TIMER
	target_timer_terminate();
#endif /* TOPPERS_ENABLE_SYS_TIMER */
	TermSerial();
}   /* ShutdownHook */
#endif /* CFG_USE_SHUTDOWNHOOK */


/*
 *  プロテクションフックルーチン
 */
#ifdef CFG_USE_PROTECTIONHOOK
ProtectionReturnType
ProtectionHook(StatusType FatalError)
{
	StatusType ercd;

	syslog(LOG_INFO, "");
	syslog(LOG_INFO, "ProtectionHook");

	if (FatalError == E_OS_STACKFAULT) {
		syslog(LOG_INFO, "E_OS_STACKFAULT");
		ercd = PRO_SHUTDOWN;
	}
	else if (FatalError == E_OS_PROTECTION_EXCEPTION) {
		syslog(LOG_INFO, "E_OS_PROTECTION_EXCEPTION");
		ercd = PRO_IGNORE;
	}
	else {
		ercd = PRO_SHUTDOWN;
	}

	return(ercd);
}
#endif /* CFG_USE_PROTECTIONHOOK */
