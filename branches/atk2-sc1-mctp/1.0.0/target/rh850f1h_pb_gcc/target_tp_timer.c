/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2015 by Witz Corporation
 *  Copyright (C) 2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2015 by SCSK Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_tp_timer.c 769 2017-03-03 06:41:08Z ertl-honda $
 */

#include "Os.h"
#include "prc_sil.h"
#include "kernel/timewindow.h"
#include "interrupt.h"
#include "target_tp_timer.h"
#include "target_hw_counter.h"
#include "sysmod/syslog.h"
#include "rh850f1h_pb.h"

/*
 *  TAUJ1を使用．クロックは60Mhz(CPUCLK2)
 *  分周なしの60Mhzで使用する．
 *  (nsec * 60) = cycle 
 */

typedef struct tauj_unit_ch {
	uint32 unit;
	uint32 ch;
}TAUJ_UNIT_CH;

const TAUJ_UNIT_CH twt_table[] = {
	{1, 0},
	{1, 2}
};

const TAUJ_UNIT_CH sct_table[] = {
	{1, 1},
	{1, 3}
};

const uint32 twt_intno_table[] = {
	TIMEWINDOWTIMER_INTNO_CORE0,
	TIMEWINDOWTIMER_INTNO_CORE1
};

const uint32 sct_intno_table[] = {
	SYSTEMCYCLETIMER_INTNO_CORE0,
	SYSTEMCYCLETIMER_INTNO_CORE1
};

typedef struct tauj_pause_cb {
	uint32 current_tw_remain_time;
	uint32 next_tw_reserve_time;
	uint8 tw_timer_start;
}TAUJ_PAUSE_CB;

static TAUJ_PAUSE_CB tauj_pause_cb_table[TotalNumberOfCores];

int tp_count;

void
target_initialize_systemcycle_timer(void)
{
	uint16	wk;
	uint32	coreid = x_core_id();
	uint32	unit = sct_table[coreid].unit;
	uint32	ch   = sct_table[coreid].ch;
	uint32	intno = sct_intno_table[coreid];

	/* assert((tauj_id + 1) < TAUJ_MAX); */

	/* システム周期タイマの停止 */
	SetTimerStopTAUJ(unit, ch);
	/* 割込み禁止 */
	HwcounterDisableInterrupt(intno);
	/* 割込み要求クリア */
	HwcounterClearInterrupt(intno);

	if (coreid == OS_CORE_ID_MASTER) {
		/* 
		 *  プリスケーラを設定 PCLK/32(2^5)
		 */
		wk = sil_reh_mem((void *) TAUJTPS(unit));
		wk &= MCU_TAUJ_MASK_CK0;
		wk |= MCU_TAUJ_CK0_0;
		sil_wrh_mem((void *) TAUJTPS(unit), wk);
	}

	/* 
	 *  システム周期タイマの設定
	 *  クロック選択 CK0
	 *  インターバルタイマ
	 *  立ち上がりエッジ
	 */
	sil_wrh_mem((void *) TAUJCMOR(unit, ch), MCU_TAUJ00_CMOR);
	sil_wrb_mem((void *) TAUJCMUR(unit, ch), MCU_TAUJ00_CMUR);

	/* タイマカウント周期設定 */
	sil_wrw_mem((void *) TAUJCDR(unit, ch), syscyc[coreid]);

	x_config_int(intno, ENABLE, TPRI_TPTIMER, coreid);
}


void
target_start_systemcycle_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	unit = sct_table[coreid].unit;
	uint32	ch   = sct_table[coreid].ch;
	uint32	intno = sct_intno_table[coreid];

	/* システム周期用タイマはタイムアウト後自動で再スタートするように設定 */
	SetTimerStartTAUJ(unit, ch);
}

void
target_intclear_systemcycle_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	intno = sct_intno_table[coreid];

	/* タイマ割込み要求のクリア */
	HwcounterClearInterrupt(intno);
}

void
target_stop_systemcycle_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	unit = sct_table[coreid].unit;
	uint32	ch   = sct_table[coreid].ch;
	uint32	intno = sct_intno_table[coreid];

	/* タイマ停止 */
	SetTimerStopTAUJ(unit, ch);

	/* タイマ割込み要求のクリア */
	HwcounterClearInterrupt(intno);
}

boolean
target_get_timeoutbit_systemcycle_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	intno = sct_intno_table[coreid];

	/* 割込み制御レジスタ */
	uint32 eic_address = EIC_ADDRESS(intno);

	/* 割込み要求ビットの確認 */
	if ((sil_reh_mem((void *) eic_address) & EIRFn) != 0x00U) {
		return(TRUE);
	}
	else {
		return(FALSE);
	}
}

/*
 *  タイムウィンドウ用タイマの初期化
 */
void
target_initialize_timewindow_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	unit = twt_table[coreid].unit;
	uint32	ch   = twt_table[coreid].ch;
	uint32	intno = twt_intno_table[coreid];
	TAUJ_PAUSE_CB *p_tauj_pause_cb = &tauj_pause_cb_table[coreid];
	
	/* assert((tauj_id + 1) < TAUJ_MAX); */

	/* タイムウィンドウタイマの停止 */
	SetTimerStopTAUJ(unit, ch);
	/* 割込み禁止 */
	HwcounterDisableInterrupt(intno);
	/* 割込み要求クリア */
	HwcounterClearInterrupt(intno);

	/* 
	 *  システム周期タイマの設定
	 *  クロック選択 CK0
	 *  ワンショットタイマ
	 *  立ち上がりエッジ
	 */
	sil_wrh_mem((void *) TAUJCMOR(unit, ch), (MCU_TAUJ00_CMOR));
	sil_wrb_mem((void *) TAUJCMUR(unit, ch), MCU_TAUJ00_CMUR);

	p_tauj_pause_cb->current_tw_remain_time = 0U;
	p_tauj_pause_cb->next_tw_reserve_time = 0U;
	p_tauj_pause_cb->tw_timer_start = 0U;

	x_config_int(intno, ENABLE, TPRI_TPTIMER, coreid);
}

/*
 *  タイムウィンドウタイマの開始
 *  タイムウィンドウ用タイマ開始処理とタイムウィンドウ切り替え処理
 *  から呼び出される．
 *  割込み禁止状態で呼び出されることを想定
 */
void
target_start_timewindow_timer(TickType tick)
{
	uint32	coreid = x_core_id();
	uint32	unit = twt_table[coreid].unit;
	uint32	ch   = twt_table[coreid].ch;
	CCB		*my_p_ccb = get_my_p_ccb();
	uint32	intno = twt_intno_table[coreid];

	/* タイマを停止 */
	SetTimerStopTAUJ(unit, ch);

	/*
	 * タイマ割込み要求のクリア
	 */
	HwcounterClearInterrupt(intno);

	/* アイドルウィンドウであればリターン */
	if (my_p_ccb->p_curtwschedom == my_p_ccb->p_idleschedom) {
		return;
	}

	/*
	 * CDRにカウント値を書き込んでからタイマをスタート
	 * スタート後，次のカウントクロックでCDR -> CNTにロードされる
	 * 割込みが許可されるまでに次のカウントクロックが来ることを想定
	 * (カウントクロックを設定するようにする) 
	 */ 
	sil_wrw_mem((void *) TAUJCDR(unit, ch), tick);
	SetTimerStartTAUJ(unit, ch);
}

/*
 *  タイムウィンドウタイマの停止
 *  終了処理から呼び出される． 
 */
void
target_stop_timewindow_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	unit = twt_table[coreid].unit;
	uint32	ch   = twt_table[coreid].ch;
	uint32	intno = sct_intno_table[coreid];

	/* タイマ停止 */
	SetTimerStopTAUJ(unit, ch);

	/* タイマ割込み要求のクリア */
	HwcounterClearInterrupt(intno);
}

/*
 *  タイムウィンドウタイマの一時停止
 *  割込み・例外の入口処理から呼び出される 
 */
void
target_pause_timewindow_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	unit = twt_table[coreid].unit;
	uint32	ch   = twt_table[coreid].ch;
	CCB			*my_p_ccb = get_my_p_ccb();

	/* アイドルウィンドウであれば何も行わない */
	if (my_p_ccb->p_curtwschedom == my_p_ccb->p_idleschedom) {
		return;
	}

	/* タイマ停止 */
	SetTimerStopTAUJ(unit, ch);

	/*
	 * 現在のパーティションの残りタイマカウントを再開時のためにCDRにコピー
	 */
	sil_wrw_mem((void *) TAUJCDR(unit, ch), sil_rew_mem((void *) TAUJCNT(unit, ch)));
}

/*
 *  タイムウィンドウタイマの再開
 *  割込み・例外の出口処理
 *  から呼び出される． 
 */
void
target_resume_timewindow_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	unit = twt_table[coreid].unit;
	uint32	ch   = twt_table[coreid].ch;
	CCB			*my_p_ccb = get_my_p_ccb();

	/* アイドルウィンドウであれば何も行わない */
	if (my_p_ccb->p_curtwschedom == my_p_ccb->p_idleschedom) {
		return;
	}

	/*
	 *  タイマの再開
	 *  CDRはpause処理で設定済み
	 * スタート後，次のカウントクロックでCDR -> CNTにロードされる
	 * 割込みが許可されるまでに次のカウントクロックが来ることを想定
	 * (カウントクロックを設定するようにする) 
	 */
	SetTimerStartTAUJ(unit, ch);
}

void
target_intclear_timewindow_timer(void)
{
	uint32	coreid = x_core_id();
	uint32	intno = twt_intno_table[coreid];

	/* タイマ割込み要求のクリア */
	HwcounterClearInterrupt(intno);
}


/*
 *  タイムウィンドウの同期用
 *  RH850/F1Hは2コアであるため単なるバリア同期でよい 
 */
#if TotalNumberOfCores > 2
#error target_sync_timewindow() only support Dual processor!!
#endif /* TotalNumberOfCores > 2 */

void
target_sync_timewindow(uint16 syncid)
{
	uint32 mev_addr    = MEV_ADDR(TP_SYNC_MEV_NO);
	uint32 mev_bit     = 1 << (syncid -1);
	uint32 mev_bit_no  = syncid - 1;
	uint32	coreid = x_core_id();
	uint32	unit = twt_table[coreid].unit;
	uint32	ch   = twt_table[coreid].ch;

	Asm(
	"    set1 %1, [%0]   \n"
	"    bz   1f         \n"  
	"    clr1 %1, [%0]   \n"  /* bit was 1 */
	"    br   2f         \n"
	"1:  ldl.w [%0], r21 \n"  /* bit was 0 */
	"    and %2, r21     \n"
	"    bnz 1b          \n"  /* loop dualing bit is 1 */
	"2:                  \n"
	: 
	: "r" (mev_addr), "r" (mev_bit_no), "r" (mev_bit)
	: "cc", "r21");

//	syslog(LOG_NOTICE, "Syncd! %d", syncid);
}
