$ 
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$  
$  Copyright (C) 2011-2014 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2014 by FUJI SOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by Spansion LLC, USA
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2014 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2014 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2014 by Witz Corporation, JAPAN
$  
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$  
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$ 
$  $Id: tlv.tf 73 2014-09-30 09:43:51Z ertl-ishikawa $
$

$FILE "kernel.res"$
{$NL$
$TAB$"TimeScale" :"ck",$NL$
$TAB$"TimeRadix" :10,$NL$
$TAB$"ConvertRules"   :["atk2"],$NL$
$TAB$"VisualizeRules" :["atk2"],$NL$
$TAB$"ResourceHeaders":["atk2"],$NL$
$TAB$"Resources":$NL$
$TAB${$NL$
$TAB$$TAB$"CurrentContext":{$NL$
$TAB$$TAB$$TAB$"Type":"Context",$NL$
$TAB$$TAB$$TAB$"Attributes":$NL$
$TAB$$TAB$$TAB${$NL$
$TAB$$TAB$$TAB$$TAB$"name"    : "None"$NL$
$TAB$$TAB$$TAB$}$NL$
$TAB$$TAB$},$NL$
$JOINEACH tskid TSK.ID_LIST ",\n"$
	$TAB$$TAB$"$tskid$":{$NL$
	$TAB$$TAB$$TAB$"Type":"Task",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"id"    :$TSK.ID[tskid]$,$NL$
	$TAB$$TAB$$TAB$$TAB$"atr"   :0,$NL$
	$TAB$$TAB$$TAB$$TAB$"pri"   :$+TSK.PRIORITY[tskid]$,$NL$
	$TAB$$TAB$$TAB$$TAB$"task"  :"$tskid$",$NL$
	$TAB$$TAB$$TAB$$TAB$"stksz" :$+TSK.STKSZ[tskid]$,$NL$
	$TAB$$TAB$$TAB$$TAB$"stk"   :"NULL",$NL$
	$TAB$$TAB$$TAB$$TAB$"state" :
	$autoact=0$
	$FOREACH tskappid TSKAPP.ID_LIST$
		$IF EQ(tskid, TSKAPP.TASK[tskappid])$
			$autoact=1$
		$END$
	$END$
	$IF autoact$
		"READY"$NL$
	$ELSE$
		"SUSPENDED"$NL$
	$END$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$}
$END$,
$NL$
$FOREACH isrid ISR.ID_LIST$
	$TAB$$TAB$"$isrid$":{$NL$
	$TAB$$TAB$$TAB$"Type":"InterruptServiceRoutine",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"id"    :$ISR.ID[isrid]$,$NL$
	$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$},$NL$
$END$
$FOREACH almid ALM.ID_LIST$
	$TAB$$TAB$"$almid$":{$NL$
	$TAB$$TAB$$TAB$"Type":"Alarm",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"id"    :$ALM.ID[almid]$,$NL$
	$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$},$NL$
$END$
$FOREACH schtblid SCHTBL.ID_LIST$
	$TAB$$TAB$"$schtblid$":{$NL$
	$TAB$$TAB$$TAB$"Type":"ScheduleTable",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"id"    :$SCHTBL.ID[schtblid]$,$NL$
	$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$},$NL$
$END$
$IF HOOK.STARTUPHOOK[1]$
	$TAB$$TAB$"StartupHook":{$NL$
	$TAB$$TAB$$TAB$"Type":"StartupHook",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$},$NL$
$END$
$IF HOOK.ERRORHOOK[1]$
	$TAB$$TAB$"ErrorHook":{$NL$
	$TAB$$TAB$$TAB$"Type":"ErrorHook",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$},$NL$
$END$
$IF HOOK.PROTECTIONHOOK[1]$
	$TAB$$TAB$"ProtectionHook":{$NL$
	$TAB$$TAB$$TAB$"Type":"ProtectionHook",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$},$NL$
$END$
$IF HOOK.SHUTDOWNHOOK[1]$
	$TAB$$TAB$"ShutdownHook":{$NL$
	$TAB$$TAB$$TAB$"Type":"ShutdownHook",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$},$NL$
$END$
$FOREACH tfnid TFN.ID_LIST$
	$TAB$$TAB$"$tfnid$":{$NL$
	$TAB$$TAB$$TAB$"Type":"TrustedFunction",$NL$
	$TAB$$TAB$$TAB$"Attributes":$NL$
	$TAB$$TAB$$TAB${$NL$
	$TAB$$TAB$$TAB$$TAB$"id"    :$TFN.ID[tfnid]$,$NL$
	$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
	$TAB$$TAB$$TAB$}$NL$
	$TAB$$TAB$},$NL$
$END$
$FOREACH osapid OSAP.ID_LIST$
	$IF OSAP.STARTUPHOOK[osapid]$
		$TAB$$TAB$"StartupHook_$osapid$":{$NL$
		$TAB$$TAB$$TAB$"Type":"StartupHookOSAP",$NL$
		$TAB$$TAB$$TAB$"Attributes":$NL$
		$TAB$$TAB$$TAB${$NL$
		$TAB$$TAB$$TAB$$TAB$"id"    :$OSAP.ID[osapid]$,$NL$
		$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
		$TAB$$TAB$$TAB$}$NL$
		$TAB$$TAB$},$NL$
	$END$
$END$
$FOREACH osapid OSAP.ID_LIST$
	$IF OSAP.ERRORHOOK[osapid]$
		$TAB$$TAB$"ErrorHook_$osapid$":{$NL$
		$TAB$$TAB$$TAB$"Type":"ErrorHookOSAP",$NL$
		$TAB$$TAB$$TAB$"Attributes":$NL$
		$TAB$$TAB$$TAB${$NL$
		$TAB$$TAB$$TAB$$TAB$"id"    :$OSAP.ID[osapid]$,$NL$
		$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
		$TAB$$TAB$$TAB$}$NL$
		$TAB$$TAB$},$NL$
	$END$
$END$
$FOREACH osapid OSAP.ID_LIST$
	$IF OSAP.SHUTDOWNHOOK[osapid]$
		$TAB$$TAB$"ShutdownHook_$osapid$":{$NL$
		$TAB$$TAB$$TAB$"Type":"ShutdownHookOSAP",$NL$
		$TAB$$TAB$$TAB$"Attributes":$NL$
		$TAB$$TAB$$TAB${$NL$
		$TAB$$TAB$$TAB$$TAB$"id"    :$OSAP.ID[osapid]$,$NL$
		$TAB$$TAB$$TAB$$TAB$"state"    : "SUSPENDED"$NL$
		$TAB$$TAB$$TAB$}$NL$
		$TAB$$TAB$},$NL$
	$END$
$END$
$TAB$}$NL$
}$NL$
$FILE "Os_Lcfg.c"$
