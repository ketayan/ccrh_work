/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2004-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: osap.c 73 2014-09-30 09:43:51Z ertl-ishikawa $
 */

#include "kernel_impl.h"
#include "check.h"
#include "osap.h"
#include "task.h"
#include "interrupt.h"
#include "counter.h"
#include "alarm.h"
#include "resource.h"
#include "scheduletable.h"

/*
 *		OSアプリケーション管理モジュール
 */

/*
 *  トレースマクロのデフォルト定義
 */
#ifndef LOG_GETOSAPID_ENTER
#define LOG_GETOSAPID_ENTER()
#endif /* LOG_GETOSAPID_ENTER */

#ifndef LOG_GETOSAPID_LEAVE
#define LOG_GETOSAPID_LEAVE(id)
#endif /* LOG_GETOSAPID_LEAVE */

#ifndef LOG_GETAST_ENTER
#define LOG_GETAST_ENTER(Application)
#endif /* LOG_GETAST_ENTER */

#ifndef LOG_GETAST_LEAVE
#define LOG_GETAST_LEAVE(ercd, p_value)
#endif /* LOG_GETAST_LEAVE */

#ifndef LOG_CALTFN_ENTER
#define LOG_CALTFN_ENTER(FunctionIndex)
#endif /* LOG_CALTFN_ENTER */

#ifndef LOG_CALTFN_LEAVE
#define LOG_CALTFN_LEAVE(FunctionIndex)
#endif /* LOG_CALTFN_LEAVE */

#ifndef LOG_TFN_ENTER
#define LOG_TFN_ENTER(FunctionIndex)
#endif /* LOG_TFN_ENTER */

#ifndef LOG_TFN_LEAVE
#define LOG_TFN_LEAVE(FunctionIndex, ercd)
#endif /* LOG_TFN_LEAVE */

#ifndef LOG_CHKTSKACS_ENTER
#define LOG_CHKTSKACS_ENTER(ApplID, TaskID)
#endif /* LOG_CHKTSKACS_ENTER */

#ifndef LOG_CHKTSKACS_LEAVE
#define LOG_CHKTSKACS_LEAVE(access)
#endif /* LOG_CHKTSKACS_LEAVE */

#ifndef LOG_CHKISRACS_ENTER
#define LOG_CHKISRACS_ENTER(ApplID, ISRID)
#endif /* LOG_CHKISRACS_ENTER */

#ifndef LOG_CHKISRACS_LEAVE
#define LOG_CHKISRACS_LEAVE(access)
#endif /* LOG_CHKISRACS_LEAVE */

#ifndef LOG_CHKALMACS_ENTER
#define LOG_CHKALMACS_ENTER(ApplID, AlarmID)
#endif /* LOG_CHKALMACS_ENTER */

#ifndef LOG_CHKALMACS_LEAVE
#define LOG_CHKALMACS_LEAVE(access)
#endif /* LOG_CHKALMACS_LEAVE */

#ifndef LOG_CHKRESACS_ENTER
#define LOG_CHKRESACS_ENTER(ApplID, ResID)
#endif /* LOG_CHKRESACS_ENTER */

#ifndef LOG_CHKRESACS_LEAVE
#define LOG_CHKRESACS_LEAVE(access)
#endif /* LOG_CHKRESACS_LEAVE */

#ifndef LOG_CHKCNTACS_ENTER
#define LOG_CHKCNTACS_ENTER(ApplID, CounterID)
#endif /* LOG_CHKCNTACS_ENTER */

#ifndef LOG_CHKCNTACS_LEAVE
#define LOG_CHKCNTACS_LEAVE(access)
#endif /* LOG_CHKCNTACS_LEAVE */

#ifndef LOG_CHKSCHTBLACS_ENTER
#define LOG_CHKSCHTBLACS_ENTER(ApplID, ScheduleTableID)
#endif /* LOG_CHKSCHTBLACS_ENTER */

#ifndef LOG_CHKSCHTBLACS_LEAVE
#define LOG_CHKSCHTBLACS_LEAVE(access)
#endif /* LOG_CHKSCHTBLACS_LEAVE */

#ifndef LOG_CHKTSKOWN_ENTER
#define LOG_CHKTSKOWN_ENTER(TaskID)
#endif /* LOG_CHKTSKOWN_ENTER */

#ifndef LOG_CHKTSKOWN_LEAVE
#define LOG_CHKTSKOWN_LEAVE(owner)
#endif /* LOG_CHKTSKOWN_LEAVE */

#ifndef LOG_CHKISROWN_ENTER
#define LOG_CHKISROWN_ENTER(ISRID)
#endif /* LOG_CHKISROWN_ENTER */

#ifndef LOG_CHKISROWN_LEAVE
#define LOG_CHKISROWN_LEAVE(owner)
#endif /* LOG_CHKISROWN_LEAVE */

#ifndef LOG_CHKALMOWN_ENTER
#define LOG_CHKALMOWN_ENTER(AlarmID)
#endif /* LOG_CHKALMOWN_ENTER */

#ifndef LOG_CHKALMOWN_LEAVE
#define LOG_CHKALMOWN_LEAVE(owner)
#endif /* LOG_CHKALMOWN_LEAVE */

#ifndef LOG_CHKCNTOWN_ENTER
#define LOG_CHKCNTOWN_ENTER(CounterID)
#endif /* LOG_CHKCNTOWN_ENTER */

#ifndef LOG_CHKCNTOWN_LEAVE
#define LOG_CHKCNTOWN_LEAVE(owner)
#endif /* LOG_CHKCNTOWN_LEAVE */

#ifndef LOG_CHKSCHTBLOWN_ENTER
#define LOG_CHKSCHTBLOWN_ENTER(ScheduleTableID)
#endif /* LOG_CHKSCHTBLOWN_ENTER */

#ifndef LOG_CHKSCHTBLOWN_LEAVE
#define LOG_CHKSCHTBLOWN_LEAVE(owner)
#endif /* LOG_CHKSCHTBLOWN_LEAVE */

#ifndef LOG_ALLOWACCESS_ENTER
#define LOG_ALLOWACCESS_ENTER()
#endif /* LOG_ALLOWACCESS_ENTER */

#ifndef LOG_ALLOWACCESS_LEAVE
#define LOG_ALLOWACCESS_LEAVE(ercd)
#endif /* LOG_ALLOWACCESS_LEAVE */

#ifndef LOG_TERMINATEAPPLICATION_ENTER
#define LOG_TERMINATEAPPLICATION_ENTER(Application, RestartOption)
#endif /* LOG_TERMINATEAPPLICATION_ENTER */

#ifndef LOG_TERMINATEAPPLICATION_LEAVE
#define LOG_TERMINATEAPPLICATION_LEAVE(ercd)
#endif /* LOG_TERMINATEAPPLICATION_LEAVE */

#ifdef TOPPERS_osap_initialize
/*
 *  実行中のOSアプリケーション
 */
OSAPCB *p_runosap;

/*
 *  OSアプリケーション管理ブロック初期化
 */
void
osap_initialize(void)
{
	ApplicationType	i;
	OSAPCB			*p_osapcb;

	p_runosap = NULL;
	for (i = 0U; i < tnum_osap; i++) {
		p_osapcb = &osapcb_table[i];
		p_osapcb->p_osapinib = &osapinib_table[i];
		p_osapcb->osap_stat = APPLICATION_ACCESSIBLE;
	}
}

#endif /* TOPPERS_osap_initialize */

/*
 *  実行状態のOSアプリケーション ID取得
 */
#ifdef TOPPERS_GetApplicationID

ApplicationType
GetApplicationID(void)
{
	ApplicationType id;
	StatusType		ercd;

	LOG_GETOSAPID_ENTER();

	CHECK_CALLEVEL(CALLEVEL_GETAPPLICATIONID);

	id = (p_runosap == NULL) ? INVALID_OSAPPLICATION : OSAPID(p_runosap);

  exit_finish:
	LOG_GETOSAPID_LEAVE(id);
	return(id);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
	call_errorhook(ercd, OSServiceId_GetApplicationID);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	id = INVALID_OSAPPLICATION;
	goto exit_finish;
}

#endif /* TOPPERS_GetApplicationID */

/*
 *  アプリケーションを利用可能な状態にするシステムサービス
 */
#ifdef TOPPERS_GetApplicationState

StatusType
GetApplicationState(ApplicationType Application, ApplicationStateRefType Value)
{
	StatusType	ercd = E_OK;
	OSAPCB		*p_osapcb;

	LOG_GETAST_ENTER(Application);

	CHECK_CALLEVEL(CALLEVEL_GETAPPLICATIONSTATE);
	CHECK_PARAM_POINTER(Value);
	CHECK_MEM_WRITE(Value, ApplicationStateType);
	CHECK_ID(Application < tnum_osap);

	p_osapcb = get_osapcb(Application);
	*Value = p_osapcb->osap_stat;

  exit_no_errorhook:
	LOG_GETAST_LEAVE(ercd, Value);
	return(ercd);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.applid = Application;
	_errorhook_par2.p_appstat = Value;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_GetApplicationState);
	x_nested_unlock_os_int();
	goto exit_no_errorhook;
#endif /* CFG_USE_ERRORHOOK */
}

#endif /* TOPPERS_GetApplicationState */

#ifdef TOPPERS_CallTrustedFunction

StatusType
CallTrustedFunction(TrustedFunctionIndexType FunctionIndex,
					TrustedFunctionParameterRefType FunctionParams)
{
	StatusType		ercd = E_OK;
	const TFINIB	*p_tfinib;
	boolean			saved_run_trusted;

	LOG_CALTFN_ENTER(FunctionIndex);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CALLTRUSTEDFUNCTION);

	/* ファンクションIDのチェック */
	CHECK_SERVICEID(FunctionIndex < tnum_tfn);

	/* 使用するスタックのチェック */
	p_tfinib = &(tfinib_table[FunctionIndex]);
	ercd = trustedfunc_stack_check(p_tfinib->tf_stksz);
	CHECK_NO_ERCD(ercd == E_OK);

	saved_run_trusted = run_trusted;
	run_trusted = TRUE;

#ifdef CFG_USE_PROTECTIONHOOK
	/* 信頼関数呼び出し中フラグ設定 */
	if (callevel_stat == TCL_TASK) {
		p_runtsk->calltfn = TRUE;
	}
	else {
		p_runisr->calltfn = TRUE;
	}
#endif /* CFG_USE_PROTECTIONHOOK */

	LOG_TFN_ENTER(FunctionIndex);
	/* 信頼関数実行 */
	ercd = p_tfinib->trs_func(FunctionIndex, FunctionParams);
	LOG_TFN_LEAVE(FunctionIndex, ercd);

#ifdef CFG_USE_STACKMONITORING
	if ((callevel_stat & TCL_ISR2) != TCL_NULL) {
		if ((*TOPPERS_ISTK_MAGIC_REGION(_ostk, _ostksz)) != STACK_MAGIC_NUMBER) {
			x_nested_lock_os_int();
			call_protectionhk_main(E_OS_STACKFAULT);
			ASSERT(0);
		}
	}
	else {
		if ((*TOPPERS_SSTK_MAGIC_REGION(p_runtsk->p_tinib)) != STACK_MAGIC_NUMBER) {
			x_nested_lock_os_int();
			call_protectionhk_main_stkchg(E_OS_STACKFAULT);
			ASSERT(0);
		}
	}
#endif /* CFG_USE_STACKMONITORING */

#ifdef CFG_USE_PROTECTIONHOOK
	/* 信頼関数呼び出し中フラグ戻す */
	if (callevel_stat == TCL_TASK) {
		p_runtsk->calltfn = FALSE;
	}
	else {
		p_runisr->calltfn = FALSE;
	}
#endif /* CFG_USE_PROTECTIONHOOK */

	run_trusted = saved_run_trusted;

	/* 信頼関数がエラーになった場合もエラーフックを呼び出す */
	CHECK_NO_ERCD(ercd == E_OK);

  exit_no_errorhook:
	LOG_CALTFN_LEAVE(ercd);
	return(ercd);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.tfnid = FunctionIndex;
	_errorhook_par2.tfnpr = FunctionParams;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CallTrustedFunction);
	x_nested_unlock_os_int();
	goto exit_no_errorhook;
#endif /* CFG_USE_ERRORHOOK */
}

#endif /* TOPPERS_CallTrustedFunction */

/*
 *  アクセスチェック関数
 */

/*
 *  OSアプリケーションのオブジェクトのアクセス権限チェックシステムサービス
 */

/*
 *  ビットマップチェックマクロ
 *   このファイル内のみで使用するため，ここで定義している
 */

#define	CHECK_OSAP_ACS(p_osapinib, btmp)	(ObjectAccessType) ((((p_osapinib)->osap_trusted) != FALSE) || \
																(((p_osapinib)->btptn & (btmp)) != 0U))

/*
 *  タスク用アクセス権限チェックシステムサービス
 */
#ifdef TOPPERS_CheckTaskAccess

ObjectAccessType
CheckTaskAccess(ApplicationType ApplID, TaskType TaskID)
{
	ObjectAccessType	access;
	StatusType			ercd;
	TCB					*p_tcb;

	LOG_CHKTSKACS_ENTER(ApplID, TaskID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTACCESS);
	CHECK_ID(ApplID < tnum_osap);
	CHECK_ID(TaskID < tnum_task);

	p_tcb = get_tcb(TaskID);

	access = CHECK_OSAP_ACS(get_osapinib(ApplID), p_tcb->p_tinib->acsbtmp);

  exit_finish:
	LOG_CHKTSKACS_LEAVE(access);
	return(access);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.applid = ApplID;
	_errorhook_par2.tskid = TaskID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckTaskAccess);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	access = NO_ACCESS;
	goto exit_finish;
}

#endif /* TOPPERS_CheckTaskAccess */

/*
 *  ISR用アクセス権限チェックシステムサービス
 */
#ifdef TOPPERS_CheckISRAccess

ObjectAccessType
CheckISRAccess(ApplicationType ApplID, ISRType ISRID)
{
	ObjectAccessType	access;
	StatusType			ercd;
	ISRCB				*p_isrcb;

	LOG_CHKISRACS_ENTER(ApplID, ISRID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTACCESS);
	CHECK_ID(ApplID < tnum_osap);
	CHECK_ID(ISRID < tnum_isr2);

	p_isrcb = get_isrcb(ISRID);

	access = CHECK_OSAP_ACS(get_osapinib(ApplID), p_isrcb->p_isrinib->acsbtmp);

  exit_finish:
	LOG_CHKISRACS_LEAVE(access);
	return(access);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.applid = ApplID;
	_errorhook_par2.isrid = ISRID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckISRAccess);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	access = NO_ACCESS;
	goto exit_finish;
}

#endif /* TOPPERS_CheckISRAccess */

/*
 *  アラーム用アクセス権限チェックシステムサービス
 */
#ifdef TOPPERS_CheckAlarmAccess

ObjectAccessType
CheckAlarmAccess(ApplicationType ApplID, AlarmType AlarmID)
{
	ObjectAccessType	access;
	StatusType			ercd;
	ALMCB				*p_almcb;

	LOG_CHKALMACS_ENTER(ApplID, AlarmID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTACCESS);
	CHECK_ID(ApplID < tnum_osap);
	CHECK_ID(AlarmID < tnum_alarm);

	p_almcb = get_almcb(AlarmID);

	access = CHECK_OSAP_ACS(get_osapinib(ApplID), p_almcb->p_alminib->acsbtmp);

  exit_finish:
	LOG_CHKALMACS_LEAVE(access);
	return(access);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.applid = ApplID;
	_errorhook_par2.almid = AlarmID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckAlarmAccess);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	access = NO_ACCESS;
	goto exit_finish;
}

#endif /* TOPPERS_CheckAlarmAccess */

/*
 *  リソース用アクセス権限チェックシステムサービス
 */
#ifdef TOPPERS_CheckResourceAccess

ObjectAccessType
CheckResourceAccess(ApplicationType ApplID, ResourceType ResID)
{
	ObjectAccessType	access;
	StatusType			ercd;
	RESCB				*p_rescb;

	LOG_CHKRESACS_ENTER(ApplID, ResID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTACCESS);
	CHECK_ID(ApplID < tnum_osap);
	CHECK_ID(ResID < tnum_stdresource);

	p_rescb = get_rescb(ResID);
	access = CHECK_OSAP_ACS(get_osapinib(ApplID), p_rescb->p_resinib->acsbtmp);

  exit_finish:
	LOG_CHKRESACS_LEAVE(access);
	return(access);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.applid = ApplID;
	_errorhook_par2.resid = ResID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckResourceAccess);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	access = NO_ACCESS;
	goto exit_finish;
}

#endif /* TOPPERS_CheckResourceAccess */

/*
 *  カウンタ用アクセス権限チェックシステムサービス
 */
#ifdef TOPPERS_CheckCounterAccess

ObjectAccessType
CheckCounterAccess(ApplicationType ApplID, CounterType CounterID)
{
	ObjectAccessType	access;
	StatusType			ercd;
	CNTCB				*p_cntcb;

	LOG_CHKCNTACS_ENTER(ApplID, CounterID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTACCESS);
	CHECK_ID(ApplID < tnum_osap);
	CHECK_ID(CounterID < tnum_counter);

	p_cntcb = get_cntcb(CounterID);

	access = CHECK_OSAP_ACS(get_osapinib(ApplID), p_cntcb->p_cntinib->acsbtmp);

  exit_finish:
	LOG_CHKCNTACS_LEAVE(access);
	return(access);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.applid = ApplID;
	_errorhook_par2.cntid = CounterID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckCounterAccess);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	access = NO_ACCESS;
	goto exit_finish;
}

#endif /* TOPPERS_CheckCounterAccess */

/*
 *  スケジュールテーブル用アクセス権限チェックシステムサービス
 */
#ifdef TOPPERS_CheckScheduleTableAccess

ObjectAccessType
CheckScheduleTableAccess(ApplicationType ApplID, ScheduleTableType ScheduleTableID)
{
	ObjectAccessType	access;
	StatusType			ercd;
	SCHTBLCB			*p_schtblcb;

	LOG_CHKSCHTBLACS_ENTER(ApplID, ScheduleTableID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTACCESS);
	CHECK_ID(ApplID < tnum_osap);
	CHECK_ID(ScheduleTableID < tnum_scheduletable);

	p_schtblcb = get_schtblcb(ScheduleTableID);

	access = CHECK_OSAP_ACS(get_osapinib(ApplID), p_schtblcb->p_schtblinib->acsbtmp);

  exit_finish:
	LOG_CHKSCHTBLACS_LEAVE(access);
	return(access);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.applid = ApplID;
	_errorhook_par2.schtblid = ScheduleTableID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckScheduleTableAccess);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	access = NO_ACCESS;
	goto exit_finish;
}

#endif /* TOPPERS_CheckScheduleTableAccess */

/*
 *  OSアプリケーションのオブジェクトの所有チェックシステムサービス
 */

/*
 *  タスク用オブジェクト所有チェックシステムサービス
 */
#ifdef TOPPERS_CheckTaskOwnership

ApplicationType
CheckTaskOwnership(TaskType TaskID)
{
	ApplicationType	owner;
	StatusType		ercd;
	TCB				*p_tcb;

	LOG_CHKTSKOWN_ENTER(TaskID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTOWNERSHIP);
	CHECK_ID(TaskID < tnum_task);

	p_tcb = get_tcb(TaskID);
	owner = OSAPID(p_tcb->p_tinib->p_osapcb);

  exit_finish:
	LOG_CHKTSKOWN_LEAVE(owner);
	return(owner);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.tskid = TaskID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckTaskOwnership);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	owner = INVALID_OSAPPLICATION;
	goto exit_finish;
}

#endif /* TOPPERS_CheckTaskOwnership */

/*
 *  ISR用オブジェクト所有チェックシステムサービス
 */
#ifdef TOPPERS_CheckISROwnership

ApplicationType
CheckISROwnership(ISRType ISRID)
{
	ApplicationType	owner;
	StatusType		ercd;
	ISRCB			*p_isrcb;

	LOG_CHKISROWN_ENTER(ISRID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTOWNERSHIP);
	CHECK_ID(ISRID < tnum_isr2);

	p_isrcb = get_isrcb(ISRID);
	owner = OSAPID(p_isrcb->p_isrinib->p_osapcb);

  exit_finish:
	LOG_CHKISROWN_LEAVE(owner);
	return(owner);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.isrid = ISRID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckISROwnership);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	owner = INVALID_OSAPPLICATION;
	goto exit_finish;
}

#endif /* TOPPERS_CheckISROwnership */

/*
 *  アラーム用オブジェクト所有チェックシステムサービス
 */
#ifdef TOPPERS_CheckAlarmOwnership

ApplicationType
CheckAlarmOwnership(AlarmType AlarmID)
{
	ApplicationType	owner;
	StatusType		ercd;
	ALMCB			*p_almcb;

	LOG_CHKALMOWN_ENTER(AlarmID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTOWNERSHIP);
	CHECK_ID(AlarmID < tnum_alarm);

	p_almcb = get_almcb(AlarmID);
	owner = OSAPID(p_almcb->p_alminib->p_osapcb);

  exit_finish:
	LOG_CHKALMOWN_LEAVE(owner);
	return(owner);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.almid = AlarmID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckAlarmOwnership);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	owner = INVALID_OSAPPLICATION;
	goto exit_finish;
}

#endif /* TOPPERS_CheckAlarmOwnership */

/*
 *  カウンタ用オブジェクト所有チェックシステムサービス
 */
#ifdef TOPPERS_CheckCounterOwnership

ApplicationType
CheckCounterOwnership(CounterType CounterID)
{
	ApplicationType	owner;
	StatusType		ercd;
	CNTCB			*p_cntcb;

	LOG_CHKCNTOWN_ENTER(CounterID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTOWNERSHIP);
	CHECK_ID(CounterID < tnum_counter);

	p_cntcb = get_cntcb(CounterID);
	owner = OSAPID(p_cntcb->p_cntinib->p_osapcb);

  exit_finish:
	LOG_CHKCNTOWN_LEAVE(owner);
	return(owner);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.cntid = CounterID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckCounterOwnership);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	owner = INVALID_OSAPPLICATION;
	goto exit_finish;
}

#endif /* TOPPERS_CheckCounterOwnership */

/*
 *  スケジュールテーブル用オブジェクト所有チェックシステムサービス
 */
#ifdef TOPPERS_CheckScheduleTableOwnership

ApplicationType
CheckScheduleTableOwnership(ScheduleTableType ScheduleTableID)
{
	ApplicationType	owner;
	StatusType		ercd;
	SCHTBLCB		*p_schtblcb;

	LOG_CHKSCHTBLOWN_ENTER(ScheduleTableID);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_CHECKOBJECTOWNERSHIP);
	CHECK_ID(ScheduleTableID < tnum_scheduletable);

	p_schtblcb = get_schtblcb(ScheduleTableID);
	owner = OSAPID(p_schtblcb->p_schtblinib->p_osapcb);

  exit_finish:
	LOG_CHKSCHTBLOWN_LEAVE(owner);
	return(owner);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.schtblid = ScheduleTableID;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_CheckScheduleTableOwnership);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

  exit_no_errorhook:
	owner = INVALID_OSAPPLICATION;
	goto exit_finish;
}

#endif /* TOPPERS_CheckScheduleTableOwnership */

/*
 *  自OSAPを利用可能な状態にする
 */
#ifdef TOPPERS_AllowAccess

StatusType
AllowAccess(void)
{
	StatusType ercd = E_OK;

	LOG_ALLOWACCESS_ENTER();

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_ALLOWACCESS);
	/* ここに来た場合p_runosap(自OSAP)がNULLではない */
	CHECK_STATE(p_runosap->osap_stat == APPLICATION_RESTARTING);

	x_nested_lock_os_int();

	p_runosap->osap_stat = APPLICATION_ACCESSIBLE;

  d_exit_no_errorhook:
	x_nested_unlock_os_int();
  exit_no_errorhook:
	LOG_ALLOWACCESS_LEAVE(ercd);
	return(ercd);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
	call_errorhook(ercd, OSServiceId_AllowAccess);
	goto d_exit_no_errorhook;
#endif /* CFG_USE_ERRORHOOK */
}

#endif /* TOPPERS_AllowAccess */

/*
 *  指定OSAPを終了/再起動する
 */
#ifdef TOPPERS_TerminateApplication

StatusType
TerminateApplication(ApplicationType Application, RestartType RestartOption)
{
	StatusType	ercd = E_OK;
	OSAPCB		*p_osapcb;

	LOG_TERMINATEAPPLICATION_ENTER(Application, RestartOption);

	CHECK_DISABLEDINT();
	CHECK_CALLEVEL(CALLEVEL_TERMINATEAPPLICATION);
	CHECK_ID((Application < tnum_osap) && (get_osapinib(Application)->osap_trusted == FALSE));
	p_osapcb = get_osapcb(Application);
	CHECK_VALUE((RestartOption == NO_RESTART) || ((RestartOption == RESTART) &&
												  (p_osapcb->p_osapinib->p_restart_tcb != NULL)));

#ifdef CFG_USE_ERRORHOOK
	if ((callevel_stat & TCL_ERROR) == TCL_NULL) {
		/*
		 *  タスクかC2ISRからの呼び出し
		 *  信頼OSAPからか自OSAPからかしか呼び出せない
		 */
		CHECK_ACCESS((run_trusted != FALSE) || (p_osapcb == p_runosap));
	}
	else {
		/*
		 *  自OSAP(非信頼)の固有エラーフック以外のフックなら呼び出せない
		 */
		CHECK_ID((run_trusted == FALSE) && (p_osapcb == p_runosap));
	}
#else /* CFG_USE_ERRORHOOK */
	/*
	 *  タスクかC2ISRからの呼び出し
	 *  信頼OSAPからか自OSAPからかしか呼び出せない
	 */
	CHECK_ACCESS((run_trusted != FALSE) || (p_osapcb == p_runosap));
#endif /* CFG_USE_ERRORHOOK */

	x_nested_lock_os_int();

	S_D_CHECK_STATE((p_osapcb->osap_stat == APPLICATION_ACCESSIBLE) ||
					((p_osapcb->osap_stat == APPLICATION_RESTARTING) &&
					 (p_osapcb == p_runosap) && (RestartOption == NO_RESTART)));

	internal_term_osap(p_osapcb, RestartOption);

  d_exit_no_errorhook:
	x_nested_unlock_os_int();
  exit_no_errorhook:
	LOG_TERMINATEAPPLICATION_LEAVE(ercd);
	return(ercd);

#ifdef CFG_USE_ERRORHOOK
  exit_errorhook:
	x_nested_lock_os_int();
  d_exit_errorhook:
#ifdef CFG_USE_PARAMETERACCESS
	_errorhook_par1.applid = Application;
	_errorhook_par2.restartoption = RestartOption;
#endif /* CFG_USE_PARAMETERACCESS */
	call_errorhook(ercd, OSServiceId_TerminateApplication);
	goto d_exit_no_errorhook;
#endif /* CFG_USE_ERRORHOOK */
}

#endif /* TOPPERS_TerminateApplication */

/*
 *  指定OSAPを終了/再起動する内部関数(プロテクションフックからも呼ばれる)
 */
#ifdef TOPPERS_internal_term_osap

void
internal_term_osap(OSAPCB *p_osapcb, RestartType RestartOption)
{
	TCB		*p_restart_tcb;
	boolean	dspflg = FALSE;

	if (RestartOption == RESTART) {
		p_osapcb->osap_stat = APPLICATION_RESTARTING;
	}
	else {
		p_osapcb->osap_stat = APPLICATION_TERMINATED;
	}

	force_term_osap_counter(p_osapcb);
	force_term_osap_alarm(p_osapcb);
	force_term_osap_schtbl(p_osapcb);
	force_term_osap_task(p_osapcb);

	if (RestartOption == RESTART) {
		p_restart_tcb = p_osapcb->p_osapinib->p_restart_tcb;
		if (p_restart_tcb != NULL) {
			dspflg = make_active(p_restart_tcb);
		}
		else {
			/* プロテクションフックからのリターンのみ実行される */
			callevel_chk_shutdown(E_OS_PROTECTION_FATAL);
		}
	}

	if ((callevel_stat & TCL_OSAP_STARTUP) != TCL_NULL) {
		/*  OSAP固有スタートアップフック中発生したプロテクションフックから
		 *  のリターンのみ実行される
		 *  フックを強制終了し，そのフックが所属するOSAPは終了状態となる
		 *  後続のスタートアップフックは実行され，OSが起動する
		 */
		x_clear_nested_os_int();
		exit_nontrusted_hook();
	}
	else if (p_runosap == p_osapcb) {
		/* 全割込み禁止状態で，タスク強制終了しても，DisableAllInterrupts全割込み禁止状態を解除しない */
		callevel_stat = (callevel_stat & TSYS_DISALLINT) | TCL_TASK;
		/*
		 *  処理単位がネストした場合，終了するOSAP確保した割込み禁止を解除する
		 *  エラーフックは呼ばない
		 *  エラーフックを呼ばないため，引数にOSServiceId_Invalidをする
		 */
		if (sus_all_cnt > 0U) {
			sus_all_cnt = 0U;
			sus_all_cnt_ctx = 0U;
		}
		if (sus_os_cnt > 0U) {
			sus_os_cnt = 0U;
			sus_os_cnt_ctx = 0U;
		}
		x_clear_nested_os_int();
		cancel_nontrusted_hook();
		/* 自OSAPから呼び出した場合は，呼び出し元に戻らない */
		exit_and_dispatch_nohook();
	}
	else if ((dspflg != FALSE) && ((callevel_stat & ~TCL_TASK) == TCL_NULL)) {
		/*他のOSAPのタスクから呼び出し，且つディスパッチが必要な場合 */
		dispatch();
	}
	else {
		/* 上記以外の場合は，リターン */
	}
}

#endif /* TOPPERS_internal_term_osap */
