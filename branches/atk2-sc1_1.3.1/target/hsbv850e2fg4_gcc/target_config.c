/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2012-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2012-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2012-2013 by Spansion LLC, USA
 *  Copyright (C) 2012-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2012-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2012-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2012-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_config.c 134 2014-12-18 09:43:43Z ertl-honda $
 */

/*
 *		ターゲット依存モジュール（HSBV850E2FG4用）
 */

#include "kernel_impl.h"
#include "v850_gcc/uart.h"
#include "v850_gcc/prc_sil.h"
#include "target_sysmod.h"
#ifdef ENABLE_RETURN_MAIN
#include "interrupt.h"
#endif /* ENABLE_RETURN_MAIN */
#ifdef TOPPERS_ENABLE_TRACE
#include "logtrace/trace_config.h"
#endif /* TOPPERS_ENABLE_TRACE */

/*
 *  文字列の出力
 */
void
target_fput_str(const char8 *c)
{
	while (*c != '\0') {
		uart_putc(*c);
		c++;
	}
	uart_putc('\n');
}

/*
 *  LED出力
 */
void
led_output(uint8 pattern)
{
	uint16 port_data = ~0;

	if (pattern & LED1_ON) {
		port_data &= ~LED1_BIT;
	}

	if (pattern & LED2_ON) {
		port_data &= ~LED2_BIT;
	}

	if (pattern & LED3_ON) {
		port_data &= ~LED3_BIT;
	}

	if (pattern & LED4_ON) {
		port_data &= ~LED4_BIT;
	}

	/* Output Port */
	sil_wrh_mem((void *) P(LED_PORT_NO),
				(sil_reh_mem((void *) P(LED_PORT_NO)) & ~LED_PORT_MASK) | port_data);

}

/*
 *  ポートの初期設定
 */
void
target_port_initialize(void)
{
	uint16 wk;

	/*
	 *  Initialize LED1〜4 connected port
	 */
	/* PMC 設定 */
	wk = sil_reh_mem((void *) PMC(LED_PORT_NO));
	wk &= ~LED_PORT_MASK;
	wk |= (LED_PORT_PMC_INIT & LED_PORT_MASK);
	sil_wrh_mem((void *) PMC(LED_PORT_NO), wk);

	/* PFCE 設定 */
	wk = sil_reh_mem((void *) PFCE(LED_PORT_NO));
	wk &= ~LED_PORT_MASK;
	wk |= (LED_PORT_PFCE_INIT & LED_PORT_MASK);
	sil_wrh_mem((void *) PFCE(LED_PORT_NO), wk);

	/* PFC 設定 */
	wk = sil_reh_mem((void *) PFC(LED_PORT_NO));
	wk &= ~LED_PORT_MASK;
	wk |= (LED_PORT_PFC_INIT & LED_PORT_MASK);
	sil_wrh_mem((void *) PFC(LED_PORT_NO), wk);

	/* PM 設定 */
	wk = sil_reh_mem((void *) PM(LED_PORT_NO));
	wk &= ~LED_PORT_MASK;
	wk |= (LED_PORT_PM_INIT & LED_PORT_MASK);
	sil_wrh_mem((void *) PM(LED_PORT_NO), wk);

	/* P 設定 */
	wk = sil_reh_mem((void *) P(LED_PORT_NO));
	wk &= ~LED_PORT_MASK;
	wk |= (LED_PORT_P_INIT & LED_PORT_MASK);
	sil_wrh_mem((void *) P(LED_PORT_NO), wk);

#if TARGET_UARTE_PORT == 10
	/* PMC 設定 */
	wk = sil_reh_mem((void *) PMC(UARTE10_PORT_NO));
	wk &= ~UARTE10_P_MASK;
	wk |= (UARTE10_PMC_INIT & UARTE10_P_MASK);
	sil_wrh_mem((void *) PMC(UARTE10_PORT_NO), wk);

	/* PFC 設定 */
	wk = sil_reh_mem((void *) PFC(UARTE10_PORT_NO));
	wk &= ~UARTE10_P_MASK;
	wk |= (UARTE10_PFC_INIT & UARTE10_P_MASK);
	sil_wrh_mem((void *) PFC(UARTE10_PORT_NO), wk);

	/* PFCE 設定 */
	wk = sil_reh_mem((void *) PFCE(UARTE10_PORT_NO));
	wk &= ~UARTE10_P_MASK;
	wk |= (UARTE10_PFCE_INIT & UARTE10_P_MASK);
	sil_wrh_mem((void *) PFCE(UARTE10_PORT_NO), wk);

	/* PM 設定 */
	wk = sil_reh_mem((void *) PM(UARTE10_PORT_NO));
	wk &= ~UARTE10_P_MASK;
	wk |= (UARTE10_PM_INIT & UARTE10_P_MASK);
	sil_wrh_mem((void *) PM(UARTE10_PORT_NO), wk);

	/* フィルタレジスタのセット */
	sil_wrb_mem((void *) FCLA7CTL0, 0x80);
#else
#error
#endif /* TARGET_UARTE_PORT == 10 */
}

/*
 *  クロック関係の初期化
 */
void
target_clock_initialize(void)
{
	uint32	errcnt = 0;
	uint32	pll0clk, pll1clk, pll2clk;

	/* Init MainClock */
	if (EnableMainOSC(MHz(MAINOSC_CLOCK)) != UC_SUCCESS) {
		errcnt++;
	}

#ifdef PLL0_CLOCK
	/* Init PLL0 */
	if (SetPLL(0, PLL0_CLOCK, &pll0clk) != UC_SUCCESS) {
		errcnt++;
	}
#endif /* PLL0_CLOCK */

#ifdef PLL1_CLOCK
	/* Init PLL1 */
	if (SetPLL(1, PLL1_CLOCK, &pll1clk) != UC_SUCCESS) {
		errcnt++;
	}
#endif /* PLL1_CLOCK */

#ifdef PLL2_CLOCK
	/* Init PLL2 */
	if (SetPLL(2, PLL2_CLOCK, &pll2clk) != UC_SUCCESS) {
		errcnt++;
	}
#endif /* PLL2_CLOCK */

#ifdef __f4002__
	/* CPUCLK PLL0 / 1 = 80MHz */
	set_clock_selection(CKSC_0(0), CSCSTAT_0(0),   PROT_CKSC0, 0x0014);
	/* URTE10  MainOsc / 1  = 20Mhz*/
	set_clock_selection(CKSC_0(11), CSCSTAT_0(11), PROT_CKSC0, 0x000c);
	/* FCNA0/FCNA1  MainOsc / 1 = 20MHz */
	set_clock_selection(CKSC_1(13), CSCSTAT_1(13), PROT_CKSC1, 0x000c);
	/* TAUJ0 IntOSC / 1 = 8MHz */
	set_clock_selection(CKSC_A(3), CSCSTAT_A(3), PROT_CKSCA, 0x0007);
	/* TAUJ1 IntOSC / 1 = 8MHz */
	set_clock_selection(CKSC_A(4), CSCSTAT_A(4), PROT_CKSCA, 0x0007);
#endif /* __f4002__ */

#ifdef __f3580__
	/* ISO0_0  : CPUCLK PLL0 / 1 = 64MHz */
	set_clock_selection(CKSC_0(0), CSCSTAT_0(0),   PROT_CKSC0, 0x0014);
	/* ISO0_11 : URTE10  MainOsc / 1  = 20Mhz*/
	set_clock_selection(CKSC_0(11), CSCSTAT_0(11), PROT_CKSC0, 0x000c);
	/* ISO0_6 : TAUB0 PLL0 / 1 = 48MHz */
	set_clock_selection(CKSC_0(6), CSCSTAT_0(6), PROT_CKSC0, 0x0014);
	/* FCNA0/FCNA1  MainOsc / 1 = 20MHz */
	set_clock_selection(CKSC_1(13), CSCSTAT_1(13), PROT_CKSC0, 0x000c);
	/* TAUJ0 IntOSC / 1 = 8MHz */
	set_clock_selection(CKSC_A(3), CSCSTAT_A(3), PROT_CKSCA, 0x0007);
#endif /* __f3580__ */
}

void
bb_study_io_init(void)
{
	uint16 wk;

	/* LED の初期化 */
	/* PMC 設定 */
	wk = sil_reh_mem((void *) PMC(4));
	wk &= ~0x0f;
	sil_wrh_mem((void *) PMC(4), wk);

	/* PFCE 設定 */
	wk = sil_reh_mem((void *) PFCE(4));
	wk &= ~0x0f;
	sil_wrh_mem((void *) PFCE(4), wk);

	/* PFC 設定 */
	wk = sil_reh_mem((void *) PFC(4));
	wk &= ~0x0f;
	sil_wrh_mem((void *) PFC(4), wk);

	/* PM 設定 */
	wk = sil_reh_mem((void *) PM(4));
	wk &= ~0x0f;
	sil_wrh_mem((void *) PM(4), wk);

	wk = sil_reh_mem((void *) P(4));
	wk &= ~0x0f;
	sil_wrh_mem((void *) P(4), wk);


	/* INT_SW1(P4_7) INTP4*/
	/* PMC 設定 */
	wk = sil_reh_mem((void *) PMC(4));
	wk |= 0x80;
	sil_wrh_mem((void *) PMC(4), wk);

	/* PFCE 設定 */
	wk = sil_reh_mem((void *) PFCE(4));
	wk &= ~0x80;
	sil_wrh_mem((void *) PFCE(4), wk);

	/* PFC 設定 */
	wk = sil_reh_mem((void *) PFC(4));
	wk &= ~0x80;
	sil_wrh_mem((void *) PFC(4), wk);

	/* PM 設定 */
	wk = sil_reh_mem((void *) PM(4));
	wk |= 0x80;
	sil_wrh_mem((void *) PM(4), wk);

	/* 立ち下がりエッジ検出 */
	sil_wrb_mem((void *) FCLA0CTL4, 0x01);


	/* INT_SW1(P4_4) INTP2*/
	/* PMC 設定 */
	wk = sil_reh_mem((void *) PMC(4));
	wk |= 0x10;
	sil_wrh_mem((void *) PMC(4), wk);

	/* PFCE 設定 */
	wk = sil_reh_mem((void *) PFCE(4));
	wk &= ~0x10;
	sil_wrh_mem((void *) PFCE(4), wk);

	/* PFC 設定 */
	wk = sil_reh_mem((void *) PFC(4));
	wk &= ~0x10;
	sil_wrh_mem((void *) PFC(4), wk);

	/* PM 設定 */
	wk = sil_reh_mem((void *) PM(4));
	wk |= 0x10;
	sil_wrh_mem((void *) PM(4), wk);

	/* 立ち下がりエッジ検出 */
	sil_wrb_mem((void *) FCLA0CTL2, 0x01);


	/* buzzer の初期化 */
	/* PMC 設定 */
	wk = sil_reh_mem((void *) PMC(4));
	wk &= ~0x100;
	sil_wrh_mem((void *) PMC(4), wk);

	/* PFCE 設定 */
	wk = sil_reh_mem((void *) PFCE(4));
	wk &= ~0x100;
	sil_wrh_mem((void *) PFCE(4), wk);

	/* PFC 設定 */
	wk = sil_reh_mem((void *) PFC(4));
	wk &= ~0x100;
	sil_wrh_mem((void *) PFC(4), wk);

	/* PM 設定 */
	wk = sil_reh_mem((void *) PM(4));
	wk &= ~0x100;
	sil_wrh_mem((void *) PM(4), wk);

	wk = sil_reh_mem((void *) P(4));
	wk &= ~0x100;
	sil_wrh_mem((void *) P(4), wk);
}

void
bb_study_io_led(uint8 pattern)
{
	uint16 wk;

	pattern &= 0x0f;
	wk = sil_reh_mem((void *) P(4));
	wk &= ~0x0f;
	wk |= pattern;
	sil_wrh_mem((void *) P(4), wk);
}

void
bb_study_io_buzzer(uint8 on_off)
{
	uint16 wk;

	wk = sil_reh_mem((void *) P(4));

	if (on_off == 1) {
		wk &= ~0x0100;
	}
	else if (on_off == 0) {
		wk |= 0x0100;
	}
	sil_wrh_mem((void *) P(4), wk);
}

void
target_hardware_initialize(void)
{
	/* ポートの初期設定 */
	target_port_initialize();

	/* クロックの初期設定 */
	target_clock_initialize();
}

/*
 *  ターゲット依存の初期化
 */
void
target_initialize(void)
{
	/*
	 *  V850E2依存の初期化
	 */
	prc_initialize();

#ifdef TOPPERS_ENABLE_TRACE
	/*
	 *  トレースログ機能の初期化
	 */
	trace_initialize((uintptr) (TRACE_AUTOSTOP));
#endif /* TOPPERS_ENABLE_TRACE */

#ifdef USE_BB_SUTDY_IO
	bb_study_io_init();
	bb_study_io_led(0x0);
#endif /* USE_BB_SUTDY_IO */
}

/*
 *  ターゲット依存の終了処理
 */
void
target_exit(void)
{
#ifdef TOPPERS_ENABLE_TRACE
	/*
	 *  トレースログのダンプ
	 */
	trace_dump(target_fput_log);
#endif /* TOPPERS_ENABLE_TRACE */

#ifndef ENABLE_RETURN_MAIN
	/*
	 *  シャットダウン処理の出力
	 */
	target_fput_str("Kernel Exit...");
#else
	target_fput_str("Kernel Shutdown...");
#endif /* ENABLE_RETURN_MAIN */

	/*
	 *  HSBV850E2FG依存の終了処理
	 */
	prc_terminate();

#ifdef ENABLE_RETURN_MAIN
	kerflg = FALSE;
	except_nest_cnt = 0U;
	nested_lock_os_int_cnt = 0U;
	sus_all_cnt = 0U;
	sus_all_cnt_ctx = 0U;
	sus_os_cnt = 0U;
	sus_os_cnt_ctx = 0U;

	/* スタックポインタの初期化とmain()の呼び出し */
	return_main();
#endif /* ENABLE_RETURN_MAIN */

	infinite_loop();
}

/*
 *  ターゲット依存の文字出力
 */
void
target_fput_log(char8 c)
{
	if (c == '\n') {
		uart_putc('\r');
	}
	uart_putc(c);
}
