/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2012-2013 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2012-2013 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2012-2013 by FUJITSU VLSI LIMITED, JAPAN
 *  Copyright (C) 2012-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2012-2013 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2012-2013 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2012-2013 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2012-2013 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2012-2013 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_test.c 66 2014-09-17 04:50:37Z fujisft-shigihara $
 */

/*
 *		以下ハードウェアカウンタプログラムのターゲット依存定義（V850用）
 *
 *  使用するタイマ：
 *    差分タイマ：目的の時間を設定する時の現在時間(現在値タイマ)
 *              と次の満了時間との相対時間をカウントすることで
 *              目的の絶対時間に満了したこととする
 *              count mode:count down once
 *
 *    現在値タイマ：カウンタ周期分のベースタイマを実現
 *              (絶対時間をカウント)
 *              count mode:continuous count down
 *
 *    また上記のタイマは32bitのダウンカウンタタイマである
 *
 *  制御方針：
 *
 *   現在値タイマはユーザ定義カウンタ最大値2倍+1までカウントし，
 *   周期タイマとして連続カウントダウンして，常に現在時刻を
 *   取得する．割込み発生する必要がないため，割込みなしモード
 *
 *   差分タイマは，満了処理を行うため，割込みありモードで動く
 *   アラームなどの満了点とタイマー1で示した現在時刻の差を
 *   現在値タイマに設定する
 *
 *  ポイント：
 *   満了処理は，現在時刻を影響しないため，現在値タイマを設けている
 *
 */

#include "Os.h"

#include "v850e2_gcc/prc_sil.h"
#include "target_test.h"
#include "sysmod/syslog.h"

/* 内部関数のプロトタイプ宣言 */
static TickType get_curr_time(uint32 irq_no, TickType maxval);
static void init_hwcounter(uint32 irq_no, TickType maxval, TimeType nspertick, TickRefType cycle);
static void start_hwcounter(uint32 irq_no);
static void stop_hwcounter(uint32 irq_no);
static void set_hwcounter(uint32 irq_no, TickType exprtick, TickType maxval);
static TickType get_hwcounter(uint32 irq_no, TickType maxval);
static void cancel_hwcounter(uint32 irq_no);
static void trigger_hwcounter(uint32 irq_no);
static void int_clear_hwcounter(uint32 irq_no);
static void int_cancel_hwcounter(uint32 irq_no);
static void increment_hwcounter(uint32 irq_no);

/*
 *  割込み要求番号をH/Wチャンネルに変換する
 *  無効なチャンネルは0xffffを返す
 */

static uint16
irq_no2taua_ch(uint32 irq_no)
{

	uint16 ch;

	switch (irq_no) {

	case TAUA_CH0:
		ch = 0x0001;
		break;
	case TAUA_CH1:
		ch = 0x0002;
		break;
	case TAUA_CH2:
		ch = 0x0004;
		break;
	case TAUA_CH3:
		ch = 0x0008;
		break;
	case TAUA_CH4:
		ch = 0x0010;
		break;
	case TAUA_CH5:
		ch = 0x0020;
		break;
	case TAUA_CH6:
		ch = 0x0040;
		break;
	case TAUA_CH7:
		ch = 0x0080;
		break;
	case TAUA_CH8:
		ch = 0x0100;
		break;
	case TAUA_CH9:
		ch = 0x0200;
		break;
	case TAUA_CH10:
		ch = 0x0400;
		break;
	case TAUA_CH11:
		ch = 0x0800;
		break;
	case TAUA_CH12:
		ch = 0x1000;
		break;
	case TAUA_CH13:
		ch = 0x2000;
		break;
	case TAUA_CH14:
		ch = 0x4000;
		break;
	case TAUA_CH15:
		ch = 0x8000;
		break;
	default:
		ch = 0xffff;
		break;
	}
	return(ch);

}
/*
 *  TAUAn タイマの動作開始／停止処理
 */
static void
set_timer_start_stop(uint32 irq_no, boolean start)
{
	uint16 ch;

	ch = irq_no2taua_ch(irq_no);

	if (start == 1)	{
		/* タイマ開始処理 */
		sil_wrh_mem((void *) TAUA0TS, ch);                          // カウンタ動作を許可
	}
	else {
		/* タイマ停止処理 */
		sil_wrh_mem((void *) TAUA0TT, ch);                          // カウンタ動作を許可
	}
}
/*
 *  TAUAn 割込み要求のクリア
 */
static void
clear_EIRFn(uint32 irq_no)
{
	uint16 wk;

	wk = sil_reh_mem((void *) ICTAUA0I(irq_no));
	wk &= ~EIRFn;                                           // 割込み要求クリア
	sil_wrh_mem((void *) ICTAUA0I(irq_no), wk);

}
/*
 *  TAUAn 割込み禁止／許可設定
 */
static void
set_EIMKn(uint32 irq_no, boolean set)
{
	uint16 wk;

	wk = sil_reh_mem((void *) ICTAUA0I(irq_no));
	if (set == 0) {
		wk |= EIMKn;                                                    // 割込み禁止
	}
	else {
		wk &= ~EIMKn;                                                   // 割込み許可
	}
	sil_wrh_mem((void *) ICTAUA0I(irq_no), wk);
}

/*
 *  ハードウェアカウンタ現在ティック値取得
 */
static TickType
get_curr_time(uint32 irq_no, TickType maxval)
{
	TickType	count;

	TickType	curr_time = 0U;

	count = sil_reh_mem((void *) (TAUA0CNT(irq_no + 1)));

	/* ダウンカウンタの為，現在チック値に変換 */
	curr_time = maxval - count;

	return(curr_time);
}

/*
 *  ハードウェアカウンタの初期化
 */
static void
init_hwcounter(uint32 irq_no, TickType maxval, TimeType nspertick, TickRefType cycle)
{

	*cycle = maxval;

	/* 差分タイマ停止処理 */
	set_timer_start_stop(irq_no, 0);
	set_EIMKn(irq_no, 0);                                           /* 割込み禁止 */
	clear_EIRFn(irq_no);                                            /* 割込み要求クリア */

	/* 差分タイマをインターバルタイマとして設定 */
	sil_wrh_mem((void *) TAUA0CMOR(irq_no), MCU_TAUA00_CMOR);       // インターバルタイマとして使用
	sil_wrb_mem((void *) TAUA0CMUR(irq_no), MCU_TAUA00_CMUR);

	/* 現在値タイマ停止処理 */
	set_timer_start_stop(irq_no + 1, 0);
	set_EIMKn(irq_no + 1, 0);                                           /* 割込み禁止 */
	clear_EIRFn(irq_no + 1);                                            /* 割込み要求クリア */

	/* 現在値タイマをインターバルタイマとして設定 */
	sil_wrh_mem((void *) TAUA0CMOR(irq_no + 1), MCU_TAUA00_CMOR);     // インターバルタイマとして使用
	sil_wrb_mem((void *) TAUA0CMUR(irq_no + 1), MCU_TAUA00_CMUR);

	sil_wrh_mem((void *) TAUA0CDR(irq_no + 1), maxval);              // タイマカウント周期設定

}
/*
 *  ハードウェアカウンタの開始
 */
static void
start_hwcounter(uint32 irq_no)
{

	/* 現在値タイマ開始 */
	set_timer_start_stop(irq_no + 1, 1);

}

/*
 *  ハードウェアカウンタの停止
 */
static void
stop_hwcounter(uint32 irq_no)
{

	/* 差分タイマの停止 */
	set_timer_start_stop(irq_no, 0);

	/* 差分タイマの割込み要求のクリア */
	set_EIMKn(irq_no, 0);                                           /* 割込み禁止 */
	clear_EIRFn(irq_no);                                            /* 割込み要求クリア */

	/* 現在値タイマの停止 */
	set_timer_start_stop(irq_no + 1, 0);

	/* 現在値タイマの割込み要求のクリア */
	set_EIMKn(irq_no + 1, 0);                                           /* 割込み禁止 */
	clear_EIRFn(irq_no + 1);                                            /* 割込み要求クリア */
}

/*
 *  ハードウェアカウンタへの満了時間の設定
 */
static void
set_hwcounter(uint32 irq_no, TickType exprtick, TickType maxval)
{
	TickType	curr_time;
	TickType	value;

	/* 差分タイマの割込み要求のクリア */
	clear_EIRFn(irq_no);                                            /* 割込み要求クリア */

	/* 現在時刻の取得	*/
	curr_time = get_curr_time(irq_no, maxval);

	/* タイマに設定する値を算出	*/
	if (exprtick >= curr_time) {
		value = exprtick - curr_time;
	}
	else {
		value = (exprtick - curr_time) + (maxval + 1U);
	}

	/*
	 *  タイマに0x00を設定し，割込み発生後，再度0を設定した場合，2回目の
	 *  0x00設定後の割込みは発生しないので，0x00設定値を0x01に直して設定
	 */
	if (value == 0x00U) {
		value = 0x01U;
	}

	/* 差分タイマのタイマ値設定 */
	sil_wrh_mem((void *) TAUA0CDR(irq_no), (value));

	/*
	 * カウント開始
	 */
	set_timer_start_stop(irq_no, 1);

}

/*
 *  ハードウェアカウンタの現在時間の取得
 */
static TickType
get_hwcounter(uint32 irq_no, TickType maxval)
{
	return(get_curr_time(irq_no, maxval));
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
static void
cancel_hwcounter(uint32 irq_no)
{
	set_timer_start_stop(irq_no, 0);
}
/*
 *  ハードウェアカウンタの強制割込み要求
 */
static void
trigger_hwcounter(uint32 irq_no)
{
	/* 差分タイマ停止 */
	set_timer_start_stop(irq_no, 0);                                /* タイマの停止 */

	/* 差分タイマの割込み要求のクリア */
	clear_EIRFn(irq_no);                                            /* 割込み要求クリア */
	set_EIMKn(irq_no, 1);                                           /* 割込み許可 */

	/* 差分タイマカウンターに0x01をセットすることで，すぐ満了 */
	sil_wrh_mem((void *) TAUA0CDR(irq_no), (1));


	/*  差分タイマ開始 */
	set_timer_start_stop(irq_no, 1);

}

/*
 *  割込み要求のクリア
 */
static void
int_clear_hwcounter(uint32 irq_no)
{

	clear_EIRFn(irq_no);                                            /* 割込み要求クリア */

}

/*
 *  割込み要求のキャンセル
 *  ペンディングされている割込み要求をキャンセル
 */
static void
int_cancel_hwcounter(uint32 irq_no)
{

	clear_EIRFn(irq_no);                                            /* 割込み要求クリア */

}

/*
 *  ハードウェアカウンタのインクリメント
 */
static void
increment_hwcounter(uint32 irq_no)
{
	/* 未サポート */
	return;
}

/*
 *  カウンタAの定義
 */
/* カウンタの最大値の2倍+1 */
static TickType SmpHwCntA_maxval;

/*
 *  ハードウェアカウンタの初期化
 */
void
init_hwcounter_SmpHwCntA(TickType maxval, TimeType nspertick)
{
	init_hwcounter(TAUA_CH1, maxval, nspertick, &SmpHwCntA_maxval);
}

/*
 *  ハードウェアカウンタの開始
 */
void
start_hwcounter_SmpHwCntA(void)
{
	start_hwcounter(TAUA_CH1);
}

/*
 *  ハードウェアカウンタの停止
 */
void
stop_hwcounter_SmpHwCntA(void)
{
	stop_hwcounter(TAUA_CH1);
}

/*
 *  ハードウェアカウンタへの満了時間の設定
 */
void
set_hwcounter_SmpHwCntA(TickType exprtick)
{
	set_hwcounter(TAUA_CH1, exprtick, SmpHwCntA_maxval);
}

/*
 *  ハードウェアカウンタの現在時間の取得
 */
TickType
get_hwcounter_SmpHwCntA(void)
{
	return(get_hwcounter(TAUA_CH1, SmpHwCntA_maxval));
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
cancel_hwcounter_SmpHwCntA(void)
{
	cancel_hwcounter(TAUA_CH1);
}

/*
 *  ハードウェアカウンタの強制割込み要求
 */
void
trigger_hwcounter_SmpHwCntA(void)
{
	trigger_hwcounter(TAUA_CH1);
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
int_clear_hwcounter_SmpHwCntA(void)
{
	int_clear_hwcounter(TAUA_CH1);
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
int_cancel_hwcounter_SmpHwCntA(void)
{
	int_cancel_hwcounter(TAUA_CH1);
}

/*
 *  ハードウェアカウンタのインクリメント
 */
void
increment_hwcounter_SmpHwCntA(void)
{
	increment_hwcounter(TAUA_CH1);
}

/*
 *  カウンタBの定義
 */
/* カウンタの最大値の2倍+1 */
static TickType SmpHwCntB_maxval;

/*
 *  ハードウェアカウンタの初期化
 */
void
init_hwcounter_SmpHwCntB(TickType maxval, TimeType nspertick)
{
	init_hwcounter(TAUA_CH3, maxval, nspertick, &SmpHwCntB_maxval);
}

/*
 *  ハードウェアカウンタの開始
 */
void
start_hwcounter_SmpHwCntB(void)
{
	start_hwcounter(TAUA_CH3);
}

/*
 *  ハードウェアカウンタの停止
 */
void
stop_hwcounter_SmpHwCntB(void)
{
	stop_hwcounter(TAUA_CH3);
}

/*
 *  ハードウェアカウンタへの満了時間の設定
 */
void
set_hwcounter_SmpHwCntB(TickType exprtick)
{
	set_hwcounter(TAUA_CH3, exprtick, SmpHwCntB_maxval);
}

/*
 *  ハードウェアカウンタの現在時間の取得
 */
TickType
get_hwcounter_SmpHwCntB(void)
{
	return(get_hwcounter(TAUA_CH3, SmpHwCntB_maxval));
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
cancel_hwcounter_SmpHwCntB(void)
{
	cancel_hwcounter(TAUA_CH3);
}

/*
 *  ハードウェアカウンタの強制割込み要求
 */
void
trigger_hwcounter_SmpHwCntB(void)
{
	trigger_hwcounter(TAUA_CH3);
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
int_clear_hwcounter_SmpHwCntB(void)
{
	int_clear_hwcounter(TAUA_CH3);
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
int_cancel_hwcounter_SmpHwCntB(void)
{
	int_cancel_hwcounter(TAUA_CH3);
}

/*
 *  ハードウェアカウンタのインクリメント
 */
void
increment_hwcounter_SmpHwCntB(void)
{
	increment_hwcounter(TAUA_CH3);
}

/*
 *  カウンタCの定義
 */
/* カウンタの最大値の2倍+1 */
static TickType SmpHwCntC_maxval;

/*
 *  ハードウェアカウンタの初期化
 */
void
init_hwcounter_SmpHwCntC(TickType maxval, TimeType nspertick)
{
	init_hwcounter(TAUA_CH5, maxval, nspertick, &SmpHwCntC_maxval);
}

/*
 *  ハードウェアカウンタの開始
 */
void
start_hwcounter_SmpHwCntC(void)
{
	start_hwcounter(TAUA_CH5);
}

/*
 *  ハードウェアカウンタの停止
 */
void
stop_hwcounter_SmpHwCntC(void)
{
	stop_hwcounter(TAUA_CH5);
}

/*
 *  ハードウェアカウンタへの満了時間の設定
 */
void
set_hwcounter_SmpHwCntC(TickType exprtick)
{
	set_hwcounter(TAUA_CH5, exprtick, SmpHwCntC_maxval);
}

/*
 *  ハードウェアカウンタの現在時間の取得
 */
TickType
get_hwcounter_SmpHwCntC(void)
{
	return(get_hwcounter(TAUA_CH5, SmpHwCntC_maxval));
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
cancel_hwcounter_SmpHwCntC(void)
{
	cancel_hwcounter(TAUA_CH5);
}

/*
 *  ハードウェアカウンタの強制割込み要求
 */
void
trigger_hwcounter_SmpHwCntC(void)
{
	trigger_hwcounter(TAUA_CH5);
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
int_clear_hwcounter_SmpHwCntC(void)
{
	int_clear_hwcounter(TAUA_CH5);
}

/*
 *  ハードウェアカウンタの設定された満了時間の取消
 */
void
int_cancel_hwcounter_SmpHwCntC(void)
{
	int_cancel_hwcounter(TAUA_CH5);
}

/*
 *  ハードウェアカウンタのインクリメント
 */
void
increment_hwcounter_SmpHwCntC(void)
{
	increment_hwcounter(TAUA_CH5);
}

/*
 *  C1ISRのためのタイマ割り込み発生
 */
void
trigger_SmpC1ISR_start(void)
{
	/* 差分タイマの割込み要求のクリア */
	clear_EIRFn(TAUA_CH7);                                          /* 割込み要求クリア */
	set_EIMKn(TAUA_CH7, 1);                                         /* 割込み許可 */
	set_timer_start_stop(TAUA_CH7, 1);                              /* カウントスタート */
}
/*
 *  C1ISRのためのタイマ初期設定
 */
void
trigger_SmpC1ISR_initialize(void)
{
	/* 現在値タイマ停止処理 */
	set_timer_start_stop(TAUA_CH7, 0);
	set_EIMKn(TAUA_CH7, 0);                                         /* 割込み禁止 */
	clear_EIRFn(TAUA_CH7);                                          /* 割込み要求クリア */

	/* 現在値タイマをインターバルタイマとして設定 */
	sil_wrh_mem((void *) TAUA0CMOR(TAUA_CH7), MCU_TAUA00_CMOR);     // インターバルタイマとして使用
	sil_wrb_mem((void *) TAUA0CMUR(TAUA_CH7), MCU_TAUA00_CMUR);

//*	sil_wrh_mem((void *) TAUA0CDR(TAUA_CH7), TAUAn_10ms );			// タイマカウント周期設定
	sil_wrh_mem((void *) TAUA0CDR(TAUA_CH7), TAUAn_700us);          // タイマカウント周期設定

}
/*
 *  C1ISRのサンプルから呼び出すC関数
 */
void
c1isr_syslog(void)
{
	syslog(LOG_INFO, "TAUA7_IRQ");
}
