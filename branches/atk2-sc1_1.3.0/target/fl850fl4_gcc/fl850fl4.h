/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2012-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2012-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2012-2013 by Spansion LLC, USA
 *  Copyright (C) 2012-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2012-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2012-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2012-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: fl850fl4.h 70 2014-09-25 06:48:12Z ertl-honda $
 */
/*
 *		FL-850/FL4ボードの定義
 */

#ifndef TOPPERS_FL850FL4_H
#define TOPPERS_FL850FL4_H

#define V850FL4

#include "v850_gcc/v850e2_fx4.h"

/*
 *  クロック速度
 */
#define PCLOCK			(32000000)

/*
 *  PLL関連の定義
 */
#define MAINOSC_CLOCK	4   /* MainOSC is 4MHz */
#define PLL0_CLOCK		64  /* PLL0 is 64MHz */
#define PLL1_CLOCK		32  /* PLL1 is 32MHz */
#define PLL2_CLOCK		80  /* PLL2 is 80MHz */


/* Port 24 Configuration
     P24_8: LED1(OUT)
     P24_9: LED2(OUT)
     P24_10: LED3(OUT)
     P24_11: LED4(OUT)
 */
#define P24_MASK						((uint16) 0x0F00)
#define PMC24_INIT						((uint16) 0x0000)
#define PFCE24_INIT						((uint16) 0x0000)
#define PFC24_INIT						((uint16) 0x0000)
#define PM24_INIT						((uint16) 0xF0FF)
#define P24_INIT						((uint16) 0x0F00)

/* Port 3 Configration
     P3_7 : URTET3(OUT)
     P3_8 : URTER3(IN)
     P3_10 : URTET5(OUT)
     P3_9  : URTER5(IN)
 */
#define UARTE3_P3_MASK					((uint16) 0x0180)
#define UARTE3_PMC3_INIT				((uint16) 0x0180)
#define UARTE3_PFC3_INIT				((uint16) 0x0180)
#define UARTE3_PFCE3_INIT				((uint16) 0x0180)
#define UARTE3_PM3_INIT					((uint16) 0x0100)

#define UARTE5_P3_MASK					((uint16) 0x0600)
#define UARTE5_PMC3_INIT				((uint16) 0x0600)
#define UARTE5_PFC3_INIT				((uint16) 0x0000)
#define UARTE5_PFCE3_INIT				((uint16) 0x0600)
#define UARTE5_PM3_INIT					((uint16) 0x0200)

/* V850Drv_led_output() */
#define POS_LED1						((uint16) 0x01)
#define POS_LED2						((uint16) 0x02)
#define POS_LED3						((uint16) 0x04)
#define POS_LED4						((uint16) 0x08)
#define POS_LED_ALL						((uint16) 0x0F)

#define MASK_LED_ALLON					((uint16) 0x0F00)
#define MASK_LED_ALLOFF					((uint16) 0x0000)
#define MASK_LED1TO4					((uint16) 0x000F)
#define P24_LED_GAP						((uint8) 8)

#endif /* TOPPERS_FL850FL4_H */
