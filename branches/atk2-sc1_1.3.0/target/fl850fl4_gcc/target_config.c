/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2012-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2012-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2012-2013 by Spansion LLC, USA
 *  Copyright (C) 2012-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2012-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2012-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2012-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_config.c 66 2014-09-17 04:50:37Z fujisft-shigihara $
 */

/*
 *		ターゲット依存モジュール（FL-V850E2/FL4用）
 */

#include "kernel_impl.h"
#include "v850_gcc/uart.h"
#include "v850_gcc/prc_sil.h"
#include "target_sysmod.h"
#ifdef ENABLE_RETURN_MAIN
#include "interrupt.h"
#endif /* ENABLE_RETURN_MAIN */
#ifdef TOPPERS_ENABLE_TRACE
#include "logtrace/trace_config.h"
#endif /* TOPPERS_ENABLE_TRACE */

/*
 *  文字列の出力
 */
void
target_fput_str(const char8 *c)
{
	while (*c != '\0') {
		uart_putc(*c);
		c++;
	}
	uart_putc('\n');
}

/********************************************************************************************/
/*  Function Name : V850Drv_led_output                                                      */
/*  Input         : set LED information, LED ON/OFF information                             */
/*  Output        : none                                                                    */
/*  Description   : Switch ON/OFF LED1-4                                                    */
/********************************************************************************************/
void
V850Drv_led_output(uint16 Data_u16, uint8 LedOnOff_u08) /* FIXME */
{
	uint16	P24_MaskValue_u16;
	uint16	P24_SetValue_u16;

	P24_MaskValue_u16 = Data_u16;
	P24_MaskValue_u16 &= MASK_LED1TO4;
	P24_MaskValue_u16 <<= P24_LED_GAP;

	/* LED ON/OFF check */
	if (LedOnOff_u08 == 1) {
		P24_SetValue_u16 = sil_reh_mem((void *) P(24));
		P24_SetValue_u16 &= ~P24_MaskValue_u16;
		sil_wrh_mem((void *) P(24), P24_SetValue_u16);
		/*    P24 &= ~P24_MaskValue; */
	}
	else if (LedOnOff_u08 == 0) {
		P24_SetValue_u16 = sil_reh_mem((void *) P(24));
		P24_SetValue_u16 |= P24_MaskValue_u16;
		sil_wrh_mem((void *) P(24), P24_SetValue_u16);
		/*    P24 |= P4_MaskValue; */
	}
	else {
		/* Insert error code */
	}
} /* V850Drv_led_output */

/*
 *  ポートの初期設定
 */
void
target_port_initialize(void)
{
	uint16 wk;

	/* PORT24(LED) */
	/* PMC24 設定 */
	wk = sil_reh_mem((void *) PMC(24));
	wk &= ~P24_MASK;
	wk |= (PMC24_INIT & P24_MASK);
	sil_wrh_mem((void *) PMC(24), wk);

	/* PFCE24 設定 */
	wk = sil_reh_mem((void *) PFCE(24));
	wk &= ~P24_MASK;
	wk |= (PFCE24_INIT & P24_MASK);
	sil_wrh_mem((void *) PFCE(24), wk);

	/* PFC24 設定 */
	wk = sil_reh_mem((void *) PFC(24));
	wk &= ~P24_MASK;
	wk |= (PFC24_INIT & P24_MASK);
	sil_wrh_mem((void *) PFC(24), wk);

	/* PM24 設定 */
	wk = sil_reh_mem((void *) PM(24));
	wk &= ~P24_MASK;
	wk |= (PM24_INIT & P24_MASK);
	sil_wrh_mem((void *) PM(24), wk);

	/* P24 設定 */
	wk = sil_reh_mem((void *) P(24));
	wk &= ~P24_MASK;
	wk |= (P24_INIT & P24_MASK);
	sil_wrh_mem((void *) P(24), wk);

#if TARGET_UARTE_PORT == 3
	/* PORT3(UARTE3) */
	/* PMC3 設定 */
	wk = sil_reh_mem((void *) PMC(3));
	wk &= ~UARTE3_P3_MASK;
	wk |= (UARTE3_PMC3_INIT & UARTE3_P3_MASK);
	sil_wrh_mem((void *) PMC(3), wk);

	/* PFC3 設定 */
	wk = sil_reh_mem((void *) PFC(3));
	wk &= ~UARTE3_P3_MASK;
	wk |= (UARTE3_PFC3_INIT & UARTE3_P3_MASK);
	sil_wrh_mem((void *) PFC(3), wk);

	/* PFCE3 設定 */
	wk = sil_reh_mem((void *) PFCE(3));
	wk &= ~UARTE3_P3_MASK;
	wk |= (UARTE3_PFCE3_INIT & UARTE3_P3_MASK);
	sil_wrh_mem((void *) PFCE(3), wk);

	/* PM3 設定 */
	wk = sil_reh_mem((void *) PM(3));
	wk &= ~UARTE3_P3_MASK;
	wk |= (UARTE3_PM3_INIT & UARTE3_P3_MASK);
	sil_wrh_mem((void *) PM(3), wk);

	/* フィルタレジスタのセット */
	sil_wrb_mem((void *) FCLA27CTL1, 0x80);
#elif TARGET_UARTE_PORT == 5

	/* PORT3(UARTE5) */
	/* PMC3 設定 */
	wk = sil_reh_mem((void *) PMC(3));
	wk &= ~UARTE5_P3_MASK;
	wk |= (UARTE5_PMC3_INIT & UARTE5_P3_MASK);
	sil_wrh_mem((void *) PMC(3), wk);

	/* PFC3 設定 */
	wk = sil_reh_mem((void *) PFC(3));
	wk &= ~UARTE5_P3_MASK;
	wk |= (UARTE5_PFC3_INIT & UARTE5_P3_MASK);
	sil_wrh_mem((void *) PFC(3), wk);

	/* PFCE3 設定 */
	wk = sil_reh_mem((void *) PFCE(3));
	wk &= ~UARTE5_P3_MASK;
	wk |= (UARTE5_PFCE3_INIT & UARTE5_P3_MASK);
	sil_wrh_mem((void *) PFCE(3), wk);

	/* PM3 設定 */
	wk = sil_reh_mem((void *) PM(3));
	wk &= ~UARTE5_P3_MASK;
	wk |= (UARTE5_PM3_INIT & UARTE5_P3_MASK);
	sil_wrh_mem((void *) PM(3), wk);

	/* フィルタレジスタのセット */
	sil_wrb_mem((void *) FCLA27CTL3, 0x80);
#else
#error
#endif /* TARGET_UARTE_PORT == 3 */
}

/*
 *  クロック関係の初期化
 */
void
target_clock_initialize(void)
{
	uint32	errcnt = 0;
	uint32	pll0clk, pll1clk, pll2clk;

	/* Init SubClock */
	if (EnableSubOSC() != UC_SUCCESS) {
		errcnt++;
	}

	/* Init MainClock */
	if (EnableMainOSC(MHz(MAINOSC_CLOCK)) != UC_SUCCESS) {
		errcnt++;
	}

	/* Init PLL0 */
	if (SetPLL(0, PLL0_CLOCK, &pll0clk) != UC_SUCCESS) {
		errcnt++;
	}

	/* Init PLL1 */
	if (SetPLL(1, PLL1_CLOCK, &pll1clk) != UC_SUCCESS) {
		errcnt++;
	}

	/* Init PLL2 */
	if (SetPLL(2, PLL2_CLOCK, &pll2clk) != UC_SUCCESS) {
		errcnt++;
	}

	set_clock_selection(CKSC_0(0), CSCSTAT_0(0),   PROT_CKSC0, 0x0014); /* CPUCLK PLL0 / 1 = 64MHz */
	set_clock_selection(CKSC_1(14), CSCSTAT_1(14), PROT_CKSC1, 0x001c); /* URTE2-9 PLL1 / 1 = 32MHz */
#ifdef TOPPERS_ENABLE_SYS_TIMER
	set_clock_selection(CKSC_0(6), CSCSTAT_0(6), PROT_CKSC0, 0x001c);   /* TAUA0 PLL1 / 1 = 32MHz */
#endif /* TOPPERS_ENABLE_SYS_TIMER */
	set_clock_selection(CKSC_A(3), CSCSTAT_A(3),   PROT_CKSCA, 0x001c); /* TAUJ0 PLL1 / 1 = 32MHz */
}

void
target_hardware_initialize(void)
{
	/* ポートの初期設定 */
	target_port_initialize();

	/* クロックの初期設定 */
	target_clock_initialize();
}

/*
 *  ターゲット依存の初期化
 */
void
target_initialize(void)
{
	/*
	 *  V850E2依存の初期化
	 */
	prc_initialize();

#ifdef TOPPERS_ENABLE_TRACE
	/*
	 *  トレースログ機能の初期化
	 */
	trace_initialize((uintptr) (TRACE_AUTOSTOP));
#endif /* TOPPERS_ENABLE_TRACE */
}

/*
 *  ターゲット依存の終了処理
 */
void
target_exit(void)
{
#ifdef TOPPERS_ENABLE_TRACE
	/*
	 *  トレースログのダンプ
	 */
	trace_dump(target_fput_log);
#endif /* TOPPERS_ENABLE_TRACE */

#ifndef ENABLE_RETURN_MAIN
	/*
	 *  シャットダウン処理の出力
	 */
	target_fput_str("Kernel Exit...");
#else
	target_fput_str("Kernel Shutdown...");
#endif /* ENABLE_RETURN_MAIN */

	/*
	 *  FL-850/FL4依存の終了処理
	 */
	prc_terminate();

#ifdef ENABLE_RETURN_MAIN
	kerflg = FALSE;
	except_nest_cnt = 0U;
	nested_lock_os_int_cnt = 0U;
	sus_all_cnt = 0U;
	sus_all_cnt_ctx = 0U;
	sus_os_cnt = 0U;
	sus_os_cnt_ctx = 0U;

	/* スタックポインタの初期化とmain()の呼び出し */
	return_main();
#endif /* ENABLE_RETURN_MAIN */

	infinite_loop();
}

/*
 *  ターゲット依存の文字出力
 */
void
target_fput_log(char8 c)
{
	if (c == '\n') {
		uart_putc('\r');
	}
	uart_putc(c);
}
