/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2006-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_support.S 66 2014-09-17 04:50:37Z fujisft-shigihara $
 */

/*
 *		チップ依存モジュール アセンブリ言語部（AT91SKYEYE用）
 */

#define  TOPPERS_MACRO_ONLY
#define UINT_C(val)		(val)		/* 符号無し整数型の定数を作るマクロ */
#define CAST(type, val)		(val)	/* 型キャストを行うマクロ */
#include "kernel_impl.h"
#include "offset.h"

/*
 *  低レベルのターゲットシステム依存の初期化
 *
 *  スタートアップモジュールの中で，メモリ初期化の前に呼び出される
 */
        .text
        .align 2
        .global hardware_init_hook
hardware_init_hook:
        bx  lr

/*
 * 割込みハンドラ
 *
 * IRQ 例外ベクタから呼び出される
 */
    .text
    .align 2
    .global irq_handler
irq_handler:
    /* 
     * IRQモードで実行される
     */
    /* 
     *  割込み前のモード(スーパーバイザーモード)へ
     *  移行し，コンテキストを保存する
     */
    msr   cpsr, #(CPSR_SVC|CPSR_IRQ_BIT) 
    stmfd sp!, {r0-r3, ip, lr, pc} /* pcはダミー */

    /*
     * spsrと戻り番地を取得するためにIRQモードへ
     */
    msr   cpsr, #(CPSR_IRQ|CPSR_IRQ_BIT)
    sub   r0, lr, #4
    mrs   r1, spsr

    /*
     *  割込みハンドラ実行時のモード（スーパーバイザーモード）に
     */
    msr   cpsr, #(CPSR_SVC|CPSR_IRQ_BIT) 
    str   r0, [sp, #0x18] /* 戻り番地をスタックに */
    stmfd sp!, {r1}       /* spsrをスタックに保存 */
    mov   lr, sp          /* この時点のスタックを復帰のため取得 */

    /*
     *  多重割込みか判定
     */

    ldr   r2, =excpt_nest_count /* 例外・割込みネスト回数を取得 */
    ldr   r3, [r2]
    add   r0, r3, #1            /* 例外・割込みネスト回数を更新 */
    str   r0, [r2]
    cmp   r3, #0    
    bne   irq_handler_1

    /* 
     * 一段目の例外（割込み）ならスタックを変更する
     */
    ldr   r0, =_ostkpt
    ldr   sp, [r0]

irq_handler_1:

    stmfd sp!, {lr}     /* 復帰用のスタックポインタの保存 */

    ldr   r3, =AIC_IVR
    ldr   r1, [r3]

    /*
     *  割込み要因の判定
     */
    ldr   r3, =AIC_ISR
    ldr   r1, [r3]

    /*
     *  割込み要因から割込み管理の添字への変換
     *  割込み要因番号は連続している（1〜18）
     */
    sub   r3, r1, #TMIN_INTNO


	/*
	 *  タスクスタックのオーバフローチェック
	 */
#ifdef CFG_USE_STACKMONITORING
	/*
	 *  C1ISRの場合，スタックモニタリングを実行しない
	 */
    ldr   r0, =isr_callevel_tbl
    ldr   r0, [r0, r3, lsl #1] /* 実行するISRのcallevelを取得 */
    ldr   r1, =TSYS_ISR1
    and   r0, r0, r1
    cmp   r0, #0
    bne   stack_monitoring_exit

	/*
	 *  多重割込みの場合はタスクスタックのスタックモニタリングを行わない
	 */
    ldr   r0, =excpt_nest_count /* 例外・割込みネスト回数を取得 */
    ldr   r1, [r0]
    cmp   r1, #1
    bne   int_nested

	/*
	 *  スタックポインタチェック方式
	 */
    ldr   r0, =p_runtsk					/* p_runtskを読み込む */
    ldr   r1, [r0]
    ldr   r2, [r1, #TCB_p_tinib]		/* タスク初期化ブロック先頭アドレス取得 */
    ldr   r2, [r2, #TINIB_stk]			/* タスクスタック先頭アドレス取得 */
    cmp   lr, r2						/* スタックポインタ(lr) <= スタックの先頭の場合， */
    ble   stack_monitoring_error_isr	/* スタックオーバーフロー発生 */

	/*
	 *  マジックナンバーチェック方式
	 */
    ldr   r1, [r2]						/* タスクスタックの先頭アドレスからマジックナンバー領域の値取得 */
    ldr   r0, =STACK_MAGIC_NUMBER
    cmp   r1, r0						/* マジックナンバーが破壊されていない場合， */
    beq   stack_monitoring_exit			/* 割込み処理に飛ぶ */
    b     stack_monitoring_error_isr	/* マジックナンバーが破壊されていた場合，スタックオーバーフロー発生 */

	/*
	 *  多重割込みの場合
	 */
int_nested:
	/*
	 *  割込みスタックのオーバフローチェック
	 */
	/*
	 *  スタック残量チェック方式
	 */
    ldr   r1,   =isr_p_isrcb_tbl
    ldr   r1,   [r1, r3, lsl #2] 				/* 実行するISRの管理ブロックを取得 */
	ldr   r2,	[r1, #ISRCB_p_intinib]			/* 割込み番号に対応したISRCBのアドレス取得 */
	ldr   r2,   [r2, #INTINIB_remain_stksz]		/* 割込み番号に対応したスタックサイズの初期化アドレス取得 */
	ldr   r1,   =_ostk							/* 割込みスタックの先頭アドレスが格納されているポインタ */
	ldr   r0,   [r1]							/* 割込みスタックの先頭アドレス取得 */
	add   r2,   r2,	 r0							/* 先頭アドレス＋ISRの使用するスタックサイズ */
	cmp   sp,   r2								/* SP <= 先頭アドレス＋ISRの使用するスタックサイズ の場合NG  */
	ble   stack_monitoring_error_isr			/* スタック残量が不足している場合，スタックオーバーフロー検出 */

	/*
	 *  マジックナンバーチェック方式
	 */
    ldr   r2, [r0]						/* タスクスタックの先頭アドレスからマジックナンバー領域の値取得 */
    ldr   r0, =STACK_MAGIC_NUMBER
    cmp   r2, r0						/* マジックナンバーが破壊されていた場合， */
    bne   stack_monitoring_error_isr	/* スタックオーバーフロー発生 */
stack_monitoring_exit:
#endif /* CFG_USE_STACKMONITORING */


    /*
     *  callevel 保存
     */
    ldr   r0, =callevel_stat
    ldrh  r2, [r0]
    stmfd sp!, {r2} 

    /*
     *  callevel 設定
     */
    ldr   r1, =isr_callevel_tbl
    ldr   r1, [r1, r3, lsl #1] /* 実行するISRのcallevelを取得 */
	orr   r1, r1, r2
    strh   r1, [r0]

    /*
     *  割込み要因の割込み優先度を求め(モデル上の)割込み優先度マスクをセット
     *  する．またその際，ハンドラ実行前の(モデル上の)割込み優先度マスクを
     *  保存する
     */
    ldr   r0, =isr_ipm_tbl      /* 割込み優先度を取得 */
    ldr   r1, [r0, r3, lsl #2]  /* r1<-割込み優先度   */
    ldr   r0, =ipm              /* 割込み発生前の割込み優先度マスクをスタックに保存 */
    ldr   r2, [r0]              
    stmfd sp!,{r2}         
    str   r1, [r0]              /* (モデル上の)割込み優先度マスクをセット */

    /*
     * (モデル上の)割込み優先度マスクの設定
     * 
     * 受け付けた割込みの割込み優先度に設定された割込み要求禁止フラグの
     * テーブルの値と(モデル上の)各割込みの割込み要求禁止フラグの状態を
     * 保持した変数の値とのORをIRCの割込み要求禁止フラグにセットする
     */
    rsb   r1, r1,  #0           /* インデックスとなるように反転 */
    ldr   r0, =ipm_mask_tbl     /* (モデル上)の割込み優先度を実現するための */
    ldr   r2, [r0, r1, lsl #2]  /* 割込み要求禁止フラグを取得 */
    ldr   r0, =idf              /* 各割込みの割込み要求禁止フラグの状態を取得 */
    ldr   r1, [r0]
    /*
     * 各割込みの割込み要求禁止フラグの状態と(モデル上)の割込み優先度のを実現する
     * ための割込み要求禁止フラグの状態のORをとり，それの否定を求めることにより，
     * IRCの割込み許可レジスタへの設定値を生成し設定する
     */
    mvn   lr, #0
    ldr   r0, =AIC_IDCR
    str   lr, [r0]
    orr   r1, r1, r2            /* マスク指定されていない割込みの許可 */
    mvn   r1, r1                /* 設定値を生成 */
    ldr   r0, =AIC_IECR
    str   r1, [r0]

    /*
     *  p_runisr 保存
     */
    ldr   r0, =p_runisr
    ldr   r1, [r0]
    stmfd sp!, {r1}

    /*
     *  p_runisr 設定
     */
    ldr   r1, =isr_p_isrcb_tbl
    ldr   r1, [r1, r3, lsl #2] /* 実行するISRの管理ブロックを取得 */
    str   r1, [r0]

    /*
     *  割込みハンドラの起動番地を取得
     */
    ldr   r0, =isr_tbl         /* 割込みハンドラテーブルの読み出し  */
    ldr   r0, [r0, r3, lsl #2] /* r0<-割込みハンドラ            */


    /*
     *  割込み管理の添字から割込み要因への変換
     */
    add   r3, r3, #TMIN_INTNO

    /*
     *  EOIのために保存
     */
    mov   r1, #1
    mov   r1, r1, lsl r3
    stmfd sp!, {r1}

    /* 
     * 割込み許可
     */
    msr   cpsr, #(CPSR_SVC)

    /* 
     * 割込みハンドラの呼び出し
     */
    mov   lr, pc
    bx    r0

	/*
	 *  割込みスタックのオーバフローチェック
	 *  割込みから戻った時，スタックポインタも戻ったはずなので，
	 *  マジックナンバーチェック方式のみ実施
	 */
#ifdef CFG_USE_STACKMONITORING
	/*
	 *  マジックナンバーチェック方式
	 */
    ldr   r1, =_ostk					/* 割込みスタックの先頭アドレス取得 */
    ldr   r2, [r1]
    ldr   r1, [r2]						/* タスクスタックの先頭アドレスからマジックナンバー領域の値取得 */
    ldr   r2, =STACK_MAGIC_NUMBER
    cmp   r1, r2						/* マジックナンバーが破壊されていた場合， */
    bne   stack_monitoring_error_isr	/* スタックオーバーフロー発生 */
#endif /* CFG_USE_STACKMONITORING */

    ldr   r1, =p_runisr
	ldrh  r2, [r1]
	cmp   r2, #0
	beq   skip_exit_isr2

    /*
     * C2ISRの不正終了チェック
     */
	bl    exit_isr2

skip_exit_isr2:

    /*
     * カーネル管理の割込みを禁止する
     */
	bl    x_lock_all_int

    /*
     * 割込みクリア
     */
    ldmfd sp!,{r0}
    ldr   r3, =AIC_EOI
    str   r0, [r3]

    /*
     *  p_runisr 復帰
     */
    ldr   r1, =p_runisr
    ldmfd sp!,{r2}
    str   r2, [r1]
        
target_ret_int:
    /*
     * 割込み優先度マスクを元に戻す
     */
    ldmfd sp!, {r1}             /* 元の割込み優先度マスクを取得 */
    ldr   r0, =ipm              /* 割込み優先度マスクを復帰     */
    str   r1, [r0]
    rsb   r1, r1,  #0           /* インデックスとなるように反転 */
    ldr   r0, =ipm_mask_tbl     /* (モデル上)の割込み優先度のを実現するための */
    ldr   r2, [r0, r1, lsl #2]  /* 割込み要求禁止フラグを取得 */    
    ldr   r0, =idf              /* 各割込みの割込み要求禁止フラグの状態を取得 */
    ldr   r1, [r0]

	/*
	 * OS割込み禁止状態で割込みが入った場合(C1ISR)は
	 * os_int_lock_maskの割込みも禁止する必要がある
	 */
	ldr   r0, =nested_lock_os_int_cnt
	ldr   r3, [r0]
	cmp   r3, #0
	beq   target_ret_int_1

	ldr   r0, =os_int_lock_mask
	ldr   r3, [r0]
	orr   r1, r1, r3

target_ret_int_1:
    /*
     * 各割込みの割込み要求禁止フラグの状態と(モデル上)の割込み優先度のを実現する
     * ための割込み要求禁止フラグの状態のORをとり，それの否定を求めることにより，
     * IRCの割込み許可レジスタへの設定値を生成し設定する
     */
    mvn   lr, #0
    ldr   r0, =AIC_IDCR
    str   lr, [r0]
    orr   r1, r1, r2            /* マスク指定されていない割込みの許可 */
    mvn   r1, r1                /* 設定値を生成 */
    ldr   r0, =AIC_IECR
    str   r1, [r0]

    /*
     *  callevel 復帰
	 *  r3はret_intで参照する
     */
    ldr   r1, =callevel_stat
	ldrh  r3, [r1]
    ldmfd sp!,{r2}
    strh   r2, [r1]

    /*
     * スタックポインタの復帰
     */
    ldmfd sp!, {r2}             /* 元のスタックポインタを取得 */
    mov   sp, r2
    
    /*
     * 後の処理はARM依存部で実行
     */
    b     ret_int

/*
 *
 * ターゲット依存の例外入口処理
 *
 */
    .text
    .global target_exc_handler
target_exc_handler:
    /*
     *  例外実行のモード（スーパーバイザーモード）に
     *  全割込み禁止状態，OS割込み禁止状態はCPU例外発生時の
     *  状態を継承する
     *  この時点のレジスタの内容は以下の通り
     *    r0 : lr(戻り先)
     *    r1 : spsr
     *    r2 : 例外番号
     */
    msr   cpsr, #(CPSR_SVC|CPSR_FIQ_BIT|CPSR_IRQ_BIT)
    str   r0, [sp, #0x18] /* 戻り番地をスタックに */
    stmfd sp!, {r1}       /* spsrをスタックに保存 */
    mov   lr, sp          /* この時点のスタックを復帰のため取得 */

    ldr   r0, =ipm        /* 割込み発生前の割込み優先度マスクをスタックに保存 */
    ldr   r3, [r0]
    stmfd sp!, {r3}

    /* 
     * コンテキスト判定のため，excpt_nest_count をスタックに保存
     * スタックに保存せず，現在のexcpt_nest_countを-1すると取得できるが，
     * スタックに積んでおいた方がデバッグ等が行いやすいので，スタックに
     * 保存する
     */
    ldr   r0, =excpt_nest_count
    ldr   r3, [r0]
    stmfd sp!, {r3}

    mov   r3,  sp         /* 例外フレーム番地を保存 */

    /*
     * カーネル管理外のCPU例外か判定する
     * 
     * カーネル管理外のCPU例外は，カーネル実行中，全割込み禁止状態，
     * OS割込み禁止状態，カーネル管理外の割込みハンドラ実行中に発生した
     * CPU例外である．ARMの場合は，SPSRのI/Fフラグのいずれかが'1'なら，
     * 該当する
     */
    tst   r1, #(CPSR_FIQ_BIT|CPSR_IRQ_BIT) 
    bne   target_kernel_unc_exc_handler /* カーネル管理外のCPU例外の処理へ */

    /*
     * 割込み発生時のコンテキストを判定
     */
    ldr   r0, =excpt_nest_count
    ldr   r1, [r0]
    add   r1, r1, #1
    str   r1, [r0]
    cmp   r1, #1
    bne   target_exc_handler_1

    /* 
     * タスクコンテキストでCPU例外が発生した場合
     * スタックを非タスクコンテキストに切り替える
     */
    ldr  r0, =_ostkpt
    ldr  sp, [r0]

target_exc_handler_1:
    stmfd sp!, {lr}     /* 復帰用のスタックポインタの保存 */

    /* 
     * 割込み発生前の割込み優先度マスクをスタックに保存 
     * 割込みハンドラと出口ルーチンを共有するために保存
     */
    ldr   r0, =ipm
    ldr   r1, [r0]
    stmfd sp!, {r1}

    /* 
     * プロテクションフックはOS割込み禁止状態で実行する
     */
	bl    x_nested_lock_os_int
	bl    x_unlock_all_int

	/*
	 *  カーネル起動していない場合に起きたCPU例外は，無限ループへ
	 */
	ldr   r0, =kerflg  /* kerflgを取得FALSEなら無限ループ */
	ldrb  r0, [r0]
	cmp   r0, #0
	beq   infinity_loop

    /*
     * CPU例外が発生した場合，OSはE_OS_PROTECTION_EXCEPTIONをパラメータとして
     * プロテクションフックを呼び出す
     */
    mov   r0, #E_OS_PROTECTION_EXCEPTION 	/* call_protectionhk_mainの引数設定 */
    bl    call_protectionhk_main		/* プロテクションフックの呼び出し */


    /*
     * 割込み優先度マスクを元に戻す
     */
    ldmfd sp!, {r1}             /* 元の割込み優先度マスクを取得 */
    ldr   r0, =ipm              /* 割込み優先度マスクを復帰     */
    str   r1, [r0]

    /*
     * スタックポインタの復帰
     */
    ldmfd sp!, {r2}             /* 元のスタックポインタを取得 */
    mov   sp, r2

	/*
	 * プロテクションフックでDisableAllInterruptsされた場合に，spsrに反映する
	 */
	mrs   r0, cpsr
	and   r0, r0,#(CPSR_IRQ_BIT)
    ldr   r1, [sp]
	orr   r1, r1, r0
	str   r1, [sp]


	/*
	 * プロテクションフック実行後はOS割込み禁止を解除し，割込み禁止にする
	 */
	stmfd sp!, {r3}
	bl    x_lock_all_int
	bl    x_nested_unlock_os_int
	ldmfd sp!, {r3}

    /*
     * 後の処理はARM依存部で実行
     *  r3はret_intで参照する
     */
    ldr   r0, =callevel_stat
	ldrh  r3, [r0]
    b     ret_int


/*
 *
 * カーネル管理外のCPU例外の出入口処理
 *
 */
target_kernel_unc_exc_handler:
    /*
     * 割込み発生時のコンテキストを判定
     */
    ldr   r0, =excpt_nest_count
    ldr   r1, [r0]
    add   r1, r1, #1
    str   r1, [r0]
    cmp   r1, #1
    bne   target_kernel_unc_exc_handler_1
    
    /* 
     * タスクコンテキストでCPU例外が発生した場合
     * スタックを非タスクコンテキストに切り替える
     */
    ldr   r0, =_ostkpt
    ldr   sp, [r0]

target_kernel_unc_exc_handler_1:
    stmfd sp!, {lr}     /* 復帰用のスタックポインタの保存 */
    
   /*
    * システム状態（コンテキストは除く）を，CPU例外発生時の状態へ
    */
    ldr   r0, [lr]             /* CPU例外発生前のCPSRの取得 */
    and   r0, r0, #(CPSR_IRQ_BIT|CPSR_FIQ_BIT)
    orr   r0, r0, #(CPSR_SVC)
    msr   cpsr, r0

    /* 
     * プロテクションフックはOS割込み禁止状態で実行する
     */
	bl    x_nested_lock_os_int

	/*
	 *  カーネル起動していない場合に起きたCPU例外は，無限ループへ
	 */
	ldr   r0, =kerflg  /* kerflgを取得FALSEなら無限ループ */
	ldrb  r1, [r0]
	cmp   r1, #0
	beq   infinity_loop

    /*
     * CPU例外が発生した場合，OSはE_OS_PROTECTION_EXCEPTIONをパラメータとして
     * プロテクションフックを呼び出す
     */
    mov   r0, #E_OS_PROTECTION_EXCEPTION 	/* call_protectionhk_mainの引数設定 */
    bl    call_protectionhk_main			/* プロテクションフックの呼び出し */

	bl    x_nested_unlock_os_int

    /*
     *  例外・割込みのネストカウント（excpt_nest_count)のデクリメント
     */
    ldr   r0, =excpt_nest_count   /* r0 <-excpt_nest_count */
    ldr   r1, [r0]
    sub   r2, r1, #1
    str   r2, [r0]

    /*
     * スタックポインタの復帰
     */
    ldmfd sp!, {r2}             /* 元のスタックポインタを取得 */
    mov   sp, r2

    /*
     * CPU例外からの復帰
     */
    ldmfd sp!,{r1}              /* CPSRの復帰処理 */
    msr   spsr, r1              /* 戻り先のcpsrをspsrに設定 */
    ldmfd sp!,{r0-r3,ip,lr,pc}^ /* コンテキストの復帰，^付きなので、cpsr <- spsr */


/*
 *  無限ループ処理
 */
infinity_loop:
	b infinity_loop
