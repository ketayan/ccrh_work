$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2014 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2014 by FUJI SOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by Spansion LLC, USA
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2014 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2014 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2014 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: target.tf 66 2014-09-17 04:50:37Z fujisft-shigihara $
$

$ 
$     パス2のターゲット依存テンプレート（AT91SKYEYE用）
$ 


$ 
$  有効な割込み番号，割込みハンドラ番号，CPU例外ハンドラ番号
$ 
$INTNO_VALID = { 1,2,...,18 }$

$
$  割込み優先度の個数
$
$TNUM_INTPRI = 8$

$ 
$  CRE_ISR2で使用できる割込み番号とそれに対応する割込みハンドラ番号
$ 
$INTNO_CREISR2_VALID = INTNO_VALID$

$ 
$  割込み属性中のターゲット依存に用いるビット
$ 
$TARGET_INTATR = TA_HIGHLEVEL | TA_POSEDGE | TA_LOWLEVEL$

$ 
$  コア依存テンプレートのインクルード（ARM用）
$ 
$INCLUDE "arm_gcc/common/core.tf"$

$FOREACH intno INTNO_VALID$
	$FOREACH isrid ISR.ID_LIST$
		$IF intno == ISR.INTNO[isrid]$
			$INT.ISRID[intno] = isrid$
		$END$
	$END$
$END$


$FILE "Os_Lcfg.c"$

$ 
$  割込み優先度テーブル
$
$NL$
const PriorityType isr_ipm_tbl[TNUM_INT] = {$NL$
$JOINEACH intno INTNO_VALID "\n"$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid)$
		$TAB$$-ISR.INTPRI[isrid]$
	$ELSE$
		$TAB$0
	$END$
$	//カンマの出力（最後の要素の後ろに出力しない）
	$IF intno != AT(INTNO_VALID,LENGTH(INTNO_VALID) - 1)$
		,
	$END$
	$TAB$$FORMAT("/* %d */", intno)$
$END$
$NL$};$NL$
$NL$

$ 
$  割込みマスクテーブル
$ 
const uint32 ipm_mask_tbl[$TNUM_INTPRI+1$]={$NL$

$TAB$UINT_C($FORMAT("0x%08x", 0)$),$TAB$/* Priority 0 */$NL$
$os_int_lock_mask = 0$

$FOREACH intpri RANGE(1,TNUM_INTPRI)$
	$intmask = 0$
	$FOREACH isrid ISR.ID_LIST$
		$IF ISR.INTPRI[isrid] <= intpri $
			$intmask = intmask | (1 << (ISR.INTNO[isrid]))$
		$END$
	$END$
	$TAB$UINT_C($FORMAT("0x%08x", intmask)$)
$	//カンマの出力（最後の要素の後ろに出力しない）
	$IF intpri < TNUM_INTPRI$
		,
	$END$
	$TAB$/* Priority -$intpri$ */$NL$
	$IF -intpri == MIN_PRI_ISR2$ 
		$os_int_lock_mask = intmask$
	$END$
$END$
};$NL$


$NL$
const uint32 os_int_lock_mask = UINT_C($FORMAT("0x%08x", os_int_lock_mask)$);$NL$
$NL$

$
$  割込みハンドラテーブル
$
const FunctionRefType isr_tbl[TNUM_INT] = {$NL$
$JOINEACH intno INTNO_VALID "\n"$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid)$
		$TAB$$ISR.INT_ENTRY[isrid]$
	$ELSE$
		$TAB$default_int_handler
	$END$
$	//カンマの出力（最後の要素の後ろに出力しない）
	$IF intno != AT(INTNO_VALID,LENGTH(INTNO_VALID) - 1)$
		,
	$END$
	$TAB$$FORMAT("/* %d */", intno)$
$END$
$NL$};$NL$
$NL$

$
$  コールレベル
$
const uint16 isr_callevel_tbl[TNUM_INT] = {$NL$
$JOINEACH intno INTNO_VALID "\n"$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
		$TAB$TCL_ISR2
	$ELSE$
		$TAB$TSYS_ISR1
	$END$
$	//カンマの出力（最後の要素の後ろに出力しない）
	$IF intno != AT(INTNO_VALID,LENGTH(INTNO_VALID) - 1)$
		,
	$END$
	$TAB$$FORMAT("/* %d */", intno)$
$END$
$NL$};$NL$
$NL$

$ 
$  ISRCBの取得テーブル
$ 
ISRCB * const isr_p_isrcb_tbl[TNUM_INT] = {$NL$
$JOINEACH intno INTNO_VALID "\n"$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
		$TAB$&(isrcb_table[$ISR.ID[isrid]$])
	$ELSE$
		$TAB$NULL
	$END$
$	//カンマの出力（最後の要素の後ろに出力しない）
	$IF intno != AT(INTNO_VALID,LENGTH(INTNO_VALID) - 1)$
		,
	$END$
	$TAB$$FORMAT("/* %d */", intno)$
$END$
$NL$};$NL$
$NL$
