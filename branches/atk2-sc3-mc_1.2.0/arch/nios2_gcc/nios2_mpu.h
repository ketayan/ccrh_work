/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: nios2_mpu.h 69 2014-09-17 04:53:45Z fujisft-shigihara $
 */

/*
 *		Nios2 MPU関連定義
 */

#ifndef TOPPERS_NIOS2_MPU_H
#define TOPPERS_NIOS2_MPU_H

/*
 *  Nios2 MPU リージョンサイズ(Byte)
 */
#define NIOS2_MPU_REGION_SIZE	64

/*
 *  Nios2 MPU config レジスタ
 */

#define NIOS2_MPU_DISABLE	UINT_C(0x000000000)
#define NIOS2_MPU_ENABLE	UINT_C(0x000000001)

/*
 *  Nios2 MPU アクセス権限 ビットパターン
 */
#define NIOS2_MPU_PERM_SYS_NEX_USR_NEX	UINT_C(0x00000000)
#define NIOS2_MPU_PERM_SYS_EXC_USR_NEX	UINT_C(0x00000001)
#define NIOS2_MPU_PERM_SYS_EXC_USR_EXC	UINT_C(0x00000002)

#define NIOS2_MPU_PERM_SYS_NO_USR_NO	UINT_C(0x00000000)
#define NIOS2_MPU_PERM_SYS_RO_USR_NO	UINT_C(0x00000001)
#define NIOS2_MPU_PERM_SYS_RO_USR_RO	UINT_C(0x00000002)
#define NIOS2_MPU_PERM_SYS_RW_USR_NO	UINT_C(0x00000004)
#define NIOS2_MPU_PERM_SYS_RW_USR_RO	UINT_C(0x00000005)
#define NIOS2_MPU_PERM_SYS_RW_USR_RW	UINT_C(0x00000006)

#define NIOS2_MPU_INDEX_SHIFT			1
#define NIOS2_MPU_PERM_SHIFT			2
#define NIOS2_MPU_DATA_REGION			UINT_C(1)
#define NIOS2_MPU_WRITE					UINT_C(1)
#define NIOS2_MPU_READ					UINT_C(2)
#define NIOS2_MPU_CACHEABLE				UINT_C(1)
#define NIOS2_MPU_CACHEABLE_SHIFT		5

/*
 *  MPUレジスタ mpubase値合成マクロ
 */
#define NIOS2_INST_MPUBASE(index, address)	((((uint32) (address)) & (0x7fffffc0U)) | ((((uint32) (index)) & (NIOS2_MPU_INST_MAX_NUM - (1U))) << NIOS2_MPU_INDEX_SHIFT))
#define NIOS2_DATA_MPUBASE(index, address)	((((uint32) (address)) & (0x7fffffc0U)) | ((((uint32) (index)) & (NIOS2_MPU_DATA_MAX_NUM - (1U))) << NIOS2_MPU_INDEX_SHIFT) | NIOS2_MPU_DATA_REGION)

/*
 *  MPUレジスタ mpuacc値合成マクロ
 */
#define NIOS2_MPUACC(perm, address)			(((uint32) (address) & (0xffffffc0U)) | ((uint32) (perm) << NIOS2_MPU_PERM_SHIFT) | NIOS2_MPU_WRITE)
#ifdef NIOS2_USE_DATACACHE
#define NIOS2_MPUACC_DATA(perm, address)	(((uint32) (address) & (0xffffffc0U)) | ((uint32) NIOS2_MPU_CACHEABLE << NIOS2_MPU_CACHEABLE_SHIFT) | (((uint32) (perm)) << NIOS2_MPU_PERM_SHIFT) | NIOS2_MPU_WRITE)
#else
#define NIOS2_MPUACC_DATA(perm, address)	(((uint32) (address) & (0xffffffc0U)) | ((uint32) (perm) << NIOS2_MPU_PERM_SHIFT) | NIOS2_MPU_WRITE)
#endif /* NIOS2_USE_DATACACHE */

/*
 *  MPU index と 使用領域の対比マクロ
 */

/* Instruction MPU */
#define NIOS2_MPU_BKGND_CODE_REGION			((NIOS2_MPU_INST_MAX_NUM) - (1U))

/* Data MPU */
/*
 *  非信頼フックルーチンはタスクと同時に動かないため，
 *  非信頼フックルーチンスタックとタスクスタックと同じMPUを使う
 */
#define NIOS2_MPU_STACK_REGION					UINT_C(0)
#define NIOS2_MPU_PRV_DATA_REGION_START			UINT_C(1)

#define NIOS2_MPU_BKGND_DATA_REGION				((NIOS2_MPU_DATA_MAX_NUM) - (1U))

#ifndef TOPPERS_MACRO_ONLY


/*
 *  Nios2 MPU設定情報ブロック
 *    MPUの設定値を保存する
 *       MPUの設定値はMPUの設定時に使用する
 */
typedef struct mpu_contex_block {
	uint32	mpubase;    /* mpubaseレジスタ設定値 */
	uint32	mpuacc;     /* mpuaccレジスタ設定値 */
} MPUINFOB;

typedef	MPUINFOB STKMPUINFOB;

/*
 *  OSアプリケーション制御ブロックに持つOSアプリケーションのMPU情報
 *     OSアプリケーションに固有
 *     実行時に変更されることが無い
 */

typedef struct osap_mpuinfob {
	const MPUINFOB	*p_mpu_region;
	uint8			tnum_inst_region;
	uint8			tnum_data_region;
} OSAPMPUINFOB;


/*
 *  共有領域  本体はkernel_mem.c に生成
 */
extern const MPUINFOB	shared_region[];
extern const uint8		tnum_shared_region;

extern void prc_set_osap_mpu(OSAPCB *p_osapcb);

/*
 *  MPU 設定マクロ
 */

LOCAL_INLINE void
mpu_enable(void)
{
	uint32 enable = NIOS2_MPU_ENABLE;
	Asm("wrctl config, %0" :: "r" (enable));
}

LOCAL_INLINE void
mpu_disable(void)
{
	Asm("wrctl config, zero");
}

LOCAL_INLINE void
mpu_set_mpubase(uint32 data)
{
	Asm("wrctl mpubase, %0" :: "r" (data));
}

LOCAL_INLINE void
mpu_set_mpuacc(uint32 data)
{
	Asm("wrctl mpuacc, %0" :: "r" (data));
}

/*
 *  MPUの情報設定
 */
LOCAL_INLINE void
prc_set_mpu(const MPUINFOB *mpuinfo)
{
	mpu_set_mpubase(mpuinfo->mpubase);
	mpu_set_mpuacc(mpuinfo->mpuacc);
}

/*
 *  MPU初期化処理
 */
extern void prc_init_mpu(void);

/*
 *  MPU依存メモリチェック
 */
extern AccessType probe_trusted_osap_mem(const MemoryStartAddressType sadr, const MemoryStartAddressType eadr);

#endif /* TOPPERS_MACRO_ONLY */


#endif /* TOPPERS_NIOS2_MPU_H */
