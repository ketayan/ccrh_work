/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: prc_svc.h 69 2014-09-17 04:53:45Z fujisft-shigihara $
 */

#ifndef TOPPERS_PRC_SVC_H
#define TOPPERS_PRC_SVC_H

#ifndef TOPPERS_MACRO_ONLY
typedef sint32 FunctionCodeType;                  /* 機能コード */
#endif /* TOPPERS_MACRO_ONLY */

/*
 *		カーネルのサービスコールのインタフェース
 *
 *  AUTOSAR OS Nios2用インタフェース
 *  スクラッチレジスタ
 *    r1 : GCC Nios2のコーリングコンベンションでat（Assembler Tempolary）と
 *         されるレジスタ
 *
 *  パラメータ渡しレジスタ
 *    Nios2コンベンションでは，パラメータが４つまでであると r4,r5,r6,r7
 *   を用いてレジスタ渡しとしている．パラメータが５つ以上の場合は，５つめ
 *   のパラメータからスタック渡しとなる
 *
 *  AUTOSAR OS のサービスコールのパラメータは最大３つなので，特に考慮しない．
 *  r4などの引数に使用するレジスタが破壊されることがわかったので破壊リスト
 *  に加える
 *
 *  サービスコールのリターン値
 *   Nios2コンベンションでは関数の返戻値はr2/r3に格納される
 *    32bitまでは r2 のみとし，32bit より大きい値は r2/r3 が使用される
 *    サービスコールの範囲ではr2のみ使用される
 *
 *  GCCの拡張アセンブリ構文
 *   asm(アセンブリテンプレート
 *       : 出力オペランド
 *       : 入力オペランド
 *       : 破壊されるレジスタのリスト
 *   )
 *
 */

#define CAL_SVC_0M(TYPE, FNCD)																					   \
	register FunctionCodeType r2 asm ("r2") = (FunctionCodeType) (FNCD);										   \
	Asm (																										   \
		"trap \n\t"																								   \
		: "=r" (r2)																								   \
		: "r" (r2)																								   \
		: "ra", "at", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "memory" \
		);																										   \
	return ((TYPE) r2)


#define CAL_SVC_1M(TYPE, FNCD, TYPE1, PAR1)																	 \
	register FunctionCodeType r2 asm ("r2") = (FunctionCodeType) (FNCD);									 \
	register TYPE1	r4 asm ("r4") = (TYPE1) (PAR1);															 \
	Asm (																									 \
		"trap \n\t"																							 \
		: "=r" (r2), "=r" (r4)																				 \
		: "r" (r2), "r" (r4)																				 \
		: "ra", "at", "r3", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "memory" \
		);																									 \
	return ((TYPE) r2)

#define CAL_SVC_2M(TYPE, FNCD, TYPE1, PAR1, TYPE2, PAR2)											   \
	register FunctionCodeType r2 asm ("r2") = (FunctionCodeType) (FNCD);							   \
	register TYPE1	r4 asm ("r4") = (TYPE1) (PAR1);													   \
	register TYPE2	r5 asm ("r5") = (TYPE2) (PAR2);													   \
	Asm (																							   \
		"trap \n\t"																					   \
		: "=r" (r2), "=r" (r4), "=r" (r5)															   \
		: "r" (r2), "r" (r4), "r" (r5)																   \
		: "ra", "at", "r3", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "memory" \
		);																							   \
	return ((TYPE) r2)

#define CAL_SVC_3M(TYPE, FNCD, TYPE1, PAR1,														 \
				   TYPE2, PAR2, TYPE3, PAR3)													 \
	register FunctionCodeType r2 asm ("r2") = (FunctionCodeType) (FNCD);						 \
	register TYPE1	r4 asm ("r4") = (TYPE1) (PAR1);												 \
	register TYPE2	r5 asm ("r5") = (TYPE2) (PAR2);												 \
	register TYPE3	r6 asm ("r6") = (TYPE3) (PAR3);												 \
	Asm (																						 \
		"trap \n\t"																				 \
		: "=r" (r2), "=r" (r4), "=r" (r5), "=r" (r6)											 \
		: "r" (r2), "r" (r4), "r" (r5), "r" (r6)												 \
		: "ra", "at", "r3", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "memory" \
		);																						 \
	return ((TYPE) r2)


/* 以下 void 対策 */

#define CAL_SVC_0N(TYPE, FNCD)																					   \
	register FunctionCodeType r2 asm ("r2") = (FunctionCodeType) (FNCD);										   \
	Asm (																										   \
		"trap \n\t"																								   \
		: "=r" (r2)																								   \
		: "r" (r2)																								   \
		: "ra", "at", "r3", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "memory" \
		)

#define CAL_SVC_1N(TYPE, FNCD, TYPE1, PAR1)																	 \
	register FunctionCodeType r2 asm ("r2") = (FunctionCodeType) (FNCD);									 \
	register TYPE1	r4 asm ("r4") = (TYPE1) (PAR1);															 \
	Asm (																									 \
		"trap \n\t"																							 \
		: "=r" (r2), "=r" (r4)																				 \
		: "r" (r2), "r" (r4)																				 \
		: "ra", "at", "r3", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "memory" \
		)

#define CAL_SVC_2N(TYPE, FNCD, TYPE1, PAR1, TYPE2, PAR2)											   \
	register FunctionCodeType r2 asm ("r2") = (FunctionCodeType) (FNCD);							   \
	register TYPE1	r4 asm ("r4") = (TYPE1) (PAR1);													   \
	register TYPE2	r5 asm ("r5") = (TYPE2) (PAR2);													   \
	Asm (																							   \
		"trap \n\t"																					   \
		: "=r" (r2), "=r" (r4), "=r" (r5)															   \
		: "r" (r2), "r" (r4), "r" (r5)																   \
		: "ra", "at", "r3", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "memory" \
		)

#define CAL_SVC_3N(TYPE, FNCD, TYPE1, PAR1,														 \
				   TYPE2, PAR2, TYPE3, PAR3)													 \
	register FunctionCodeType r2 asm ("r2") = (FunctionCodeType) (FNCD);						 \
	register TYPE1	r4 asm ("r4") = (TYPE1) (PAR1);												 \
	register TYPE2	r5 asm ("r5") = (TYPE2) (PAR2);												 \
	register TYPE3	r6 asm ("r6") = (TYPE3) (PAR3);												 \
	Asm (																						 \
		"trap \n\t"																				 \
		: "=r" (r2), "=r" (r4), "=r" (r5), "=r" (r6)											 \
		: "r" (r2), "r" (r4), "r" (r5), "r" (r6)												 \
		: "ra", "at", "r3", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "memory" \
		)

#endif /* TOPPERS_PRC_SVC_H */
