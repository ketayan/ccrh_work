/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2014-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2014-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2014-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2014-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2014-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2014-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id$
 */

/*
 *		タイミング保護用タイマ
 */
#include "kernel_impl.h"
#include "prc_sil.h"
#include "target_timingprotection.h"
#include "timingprotection.h"
#include "target_hw_counter.h"

/*
 *  タイマの初期化処理
 */
void
target_tp_initialize(void)
{
	uint16 wk;
	
	/* タイマ停止 */
	SetTimerStopTAUJ(TP_TIMER_UNIT, TP_FAULT_CH);
	
	/* 割込み要求クリア */
	HwcounterClearInterrupt(INTNO_TP_FAULT);

	/* 
     	*  ユニット0のプリスケーラを設定 PCLK/2^0
     	*  ハードウェアカウンタと同じ設定
     	*/
	wk = sil_reh_mem((void *) TAUJTPS(0));
	wk &= MCU_TAUJ_MASK_CK0;
	wk |= MCU_TAUJ_CK0_0;
	sil_wrh_mem((void *) TAUJTPS(0), wk);
	
	/* インターバルタイマ設定 */
	sil_wrh_mem((void *) TAUJCMOR(TP_TIMER_UNIT, TP_FAULT_CH), TP_FAULT_CMOR);
	sil_wrb_mem((void *) TAUJCMUR(TP_TIMER_UNIT, TP_FAULT_CH), TP_FAULT_CMUR);
	
	/* タイマ停止 */
	SetTimerStopTAUJ(TP_TIMER_UNIT, TP_TIMER_CH);
	/* 割込み要求クリア */
	HwcounterClearInterrupt(INTNO_TP_TIMER);
	/* 割込み許可 */
	HwcounterEnableInterrupt(INTNO_TP_TIMER);

	/* インターバルタイマ設定 */
	sil_wrh_mem((void *) TAUJCMOR(TP_TIMER_UNIT, TP_TIMER_CH), TP_TIMER_CMOR);
	sil_wrb_mem((void *) TAUJCMUR(TP_TIMER_UNIT, TP_TIMER_CH), TP_TIMER_CMUR);

	/* タイマカウント周期設定 */
	sil_wrw_mem((void *) TAUJCDR(TP_TIMER_UNIT, TP_TIMER_CH), TP_TIMER_MAX_TICK);

	x_config_int(INTNO_TP_FAULT, ENABLE, intpri_tp_fault);
	x_config_int(INTNO_TP_TIMER, ENABLE, intpri_tp_timer);

	/* タイマ動作開始 */
	SetTimerStartTAUJ(TP_TIMER_UNIT, TP_TIMER_CH);
}

/*
 *  タイマの終了処理
 */
void
target_tp_terminate(void)
{
	/* タイマ停止 */
	SetTimerStopTAUJ(TP_TIMER_UNIT, TP_FAULT_CH);
	/* タイマ停止 */
	SetTimerStopTAUJ(TP_TIMER_UNIT, TP_TIMER_CH);

	/* 割込み要求クリア */
	HwcounterClearInterrupt(INTNO_TP_FAULT);
	/* 割込み要求クリア */
	HwcounterClearInterrupt(INTNO_TP_TIMER);
}

/*
 *  時間監視用タイマの起動処理
 *  - タイマ動作中にも呼び出される
 */
void
target_tp_start_timer(TickType tick)
{
	/* タイマ停止 */
	SetTimerStopTAUJ(TP_TIMER_UNIT, TP_FAULT_CH);
	/* 割込み要求クリア */
	HwcounterClearInterrupt(INTNO_TP_FAULT);
	
	/* 割込み許可 */
	HwcounterEnableInterrupt(INTNO_TP_FAULT);

	/* タイマカウント設定 */
	sil_wrw_mem((void *) TAUJCDR(TP_TIMER_UNIT, TP_FAULT_CH), tick);
	
	/* タイマ動作開始 */
	SetTimerStartTAUJ(TP_TIMER_UNIT, TP_FAULT_CH);
}

/*
 *  時間監視用タイマの残り時間取得
 *  - タイマ動作中にも呼び出される
 */
TickType
target_tp_get_remaining_ticks(void)
{
	TickType	curval;
	uint32 eic_address = EIC_ADDRESS(INTNO_TP_FAULT);

	/* タイマからカウント値を読み込む */
	curval = sil_rew_mem((void *) (TAUJCNT(TP_TIMER_UNIT, TP_FAULT_CH)));

	/* 割込み要求ビットの判定 */
	if ((sil_reh_mem((void *) eic_address) & EIRFn) != 0x00U) {
		/*
		 *  すでにタイムアウトになっている
		 */
		curval = 0x01U;
		/* 割込み要求クリア */
		HwcounterClearInterrupt(INTNO_TP_FAULT);
	}
	else {}

	return(curval);
}

/*
 *  時間監視用タイマの停止処理
 */
TickType
target_tp_stop_timer(void)
{
	TickType curval;

	/* タイマ停止 */
	SetTimerStopTAUJ(TP_TIMER_UNIT, TP_FAULT_CH);

	curval = target_tp_get_remaining_ticks();

	return(curval);
}

/*
 *  到着間隔計測用タイマの経過ティックの読出し
 */
TickType
target_tp_get_elapsed_ticks(void)
{
	TickType curval;

	/* タイマからカウント値を読み込む */
	curval = sil_rew_mem((void *) (TAUJCNT(TP_TIMER_UNIT, TP_TIMER_CH)));

	return(TP_TIMER_MAX_TICK - curval);
}

/*
 *  タイマの割込みチェック
 */
boolean
target_tp_sense_interrupt(void)
{
	uint32 eic_address = EIC_ADDRESS(INTNO_TP_TIMER);

	/* 割込み要求ビットの判定 */
	return((sil_reh_mem((void *) eic_address) & EIRFn) != 0x00U);
}

/*
 *  タイマの停止処理
 */
void
target_tp_stop_fault_timer(void)
{
	/* タイマ停止 */
	SetTimerStopTAUJ(TP_TIMER_UNIT, TP_FAULT_CH);
		
	return;
}
