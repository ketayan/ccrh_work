/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: prc_mpu.c 1801 2015-03-27 06:34:43Z t_ishikawa $
 */

/*
 *		プロセッサ依存モジュール（Nios2用）
 */
#include "kernel_impl.h"
#include "osap.h"
#include "nios2_mpu.h"

/* 内部関数のプロトタイプ宣言 */
static void prc_init_mpu_set(void);
LOCAL_INLINE boolean chk_adr_main(MemoryStartAddressType sadr1, MemoryStartAddressType eadr1, MemoryStartAddressType sadr2, MemoryStartAddressType eadr2);

void
prc_set_osap_mpu(OSAPCB *p_osapcb)
{
	static uint8		tnum_set_inst_region[TotalNumberOfCores];
	static uint8		tnum_set_data_region[TotalNumberOfCores];
	const OSAPMPUINFOB	*mpuinfo;
	MPUINFOB			set_mpu;
	uint8				i;
	CCB					*p_ccb = get_my_p_ccb();

	if (p_osapcb != (OSAPCB *) (p_ccb->target_ccb.p_ctxosap)) {
		p_ccb->target_ccb.p_ctxosap = (void *) p_osapcb;

#ifndef OMIT_OSAPMPUINFOB
		mpuinfo = &(p_osapcb->p_osapinib->osap_mpu);
#endif /* OMIT_OSAPMPUINFOB */

		for (i = 0U; i < (mpuinfo->tnum_inst_region + mpuinfo->tnum_data_region); i++) {
			prc_set_mpu(&(mpuinfo->p_mpu_region[i]));
		}
		for (i = mpuinfo->tnum_inst_region; i < tnum_set_inst_region[p_ccb->coreid]; i++) {
			set_mpu.mpubase = NIOS2_INST_MPUBASE(i, 0U);
			set_mpu.mpuacc = NIOS2_MPUACC(0U, 0U);
			prc_set_mpu(&set_mpu);
		}
		tnum_set_inst_region[p_ccb->coreid] = mpuinfo->tnum_inst_region;

		for (i = mpuinfo->tnum_data_region; i < tnum_set_data_region[p_ccb->coreid]; i++) {
			set_mpu.mpubase = NIOS2_DATA_MPUBASE(NIOS2_MPU_PRV_DATA_REGION_START + (uint32) i, 0U);
			set_mpu.mpuacc = NIOS2_MPUACC_DATA(0U, 0U);
			prc_set_mpu(&set_mpu);
		}
		tnum_set_data_region[p_ccb->coreid] = mpuinfo->tnum_data_region;
	}
}

/*
 *  MPU初期情報設定
 */
static void
prc_init_mpu_set(void)
{
	MPUINFOB	mpuinfo;
	uint32		i;
	uint8		j;

	for (i = 0U; i < (NIOS2_MPU_INST_MAX_NUM - 1U); i++) {
		mpuinfo.mpubase = NIOS2_INST_MPUBASE(i, 0U);
		mpuinfo.mpuacc = NIOS2_MPUACC(0U, 0U);
		prc_set_mpu(&mpuinfo);
	}
	for (i = 0U; i < (NIOS2_MPU_DATA_MAX_NUM - 1U); i++) {
		mpuinfo.mpubase = NIOS2_DATA_MPUBASE(i, 0U);
		mpuinfo.mpuacc = NIOS2_MPUACC_DATA(0U, 0U);
		prc_set_mpu(&mpuinfo);
	}

	/*
	 *  バックグラウンド領域のMPUレジスタ設定
	 */
	mpuinfo.mpubase = NIOS2_INST_MPUBASE(NIOS2_MPU_BKGND_CODE_REGION, BG_CODE_START);
	mpuinfo.mpuacc = NIOS2_MPUACC(NIOS2_MPU_PERM_SYS_EXC_USR_NEX, BG_CODE_END);
	prc_set_mpu(&mpuinfo);

	mpuinfo.mpubase = NIOS2_DATA_MPUBASE(NIOS2_MPU_BKGND_DATA_REGION, BG_DATA_START);
	mpuinfo.mpuacc = NIOS2_MPUACC_DATA(NIOS2_MPU_PERM_SYS_RW_USR_NO, BG_DATA_END);
	prc_set_mpu(&mpuinfo);

	for (j = 0U; j < tnum_shared_region; j++) {
		prc_set_mpu(&(shared_region[j]));
	}
}

/*
 *  MPU初期化処理
 */
void
prc_init_mpu(void)
{
	/* MPU 無効 */
	mpu_disable();

	/*
	 *  MPU初期情報設定
	 */
	prc_init_mpu_set();

	/* MPU 有効 */
	mpu_enable();
}

/*
 *  範囲チェック関数
 *   sadr1 と eadr1 の範囲間に sadr2 と eadr2 が 入るかチェック
 */
LOCAL_INLINE boolean
chk_adr_main(MemoryStartAddressType sadr1, MemoryStartAddressType eadr1, MemoryStartAddressType sadr2, MemoryStartAddressType eadr2)
{
	MemorySizeType	end_adr1 = (MemorySizeType) eadr1;
	MemorySizeType	end_adr2 = (MemorySizeType) eadr2;

	/* eadr には終端アドレス+1 が格納されているので 比較時は -1する */
	return((sadr1 <= sadr2) && (sadr1 <= (MemoryStartAddressType) (end_adr2 - 1U)) &&
		   ((MemoryStartAddressType) (end_adr1 - 1U) >= sadr2) &&
		   ((MemoryStartAddressType) (end_adr1 - 1U) >= (MemoryStartAddressType) (end_adr2 - 1U)));
}

/*
 *  MPU依存のメモリチェック
 *    信頼OSアプリケーションにてバックグランドをチェックする
 */

AccessType
probe_trusted_osap_mem(const MemoryStartAddressType sadr, const MemoryStartAddressType eadr)
{
	AccessType ercd = NO_ACCESS;

	ercd |= (chk_adr_main((MemoryStartAddressType) BG_CODE_START, (MemoryStartAddressType) BG_CODE_END, sadr, eadr) != FALSE) ? AP_Executable : NO_ACCESS;
	ercd |= (chk_adr_main((MemoryStartAddressType) BG_DATA_START, (MemoryStartAddressType) BG_DATA_END, sadr, eadr) != FALSE) ? (AP_Writable | AP_Readable) : NO_ACCESS;

	return(ercd);
}
