$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2008-2015 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by Spansion LLC, USA
$  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2015 by Witz Corporation
$  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
$  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
$  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: prc.tf 1801 2015-03-27 06:34:43Z t_ishikawa $
$

$
$     パス2のアーキテクチャ依存テンプレート（Nios2用）
$

$CFG_PASS3 = 0$
$CFG_PASS4 = 0$

$INCLUDE "prc_common.tf"$

$
$ 割込み優先度の個数
$
$TNUM_INTPRI = 64$

$
$  OsIsrInterruptNumberで使用できる割込み番号
$
$INTNO_CREISR2_VALID = INTNO_VALID$

$
$  メモリ領域アライメントサイズの指定
$    ldscript は マクロ変換されないので直値を設定している
$
$CHECK_MEMORY_ALIGN = MEMORY_ALIGN$
$TARGET_SEC_ALIGN_STR = 0$
$TARGET_PAGE_SIZE_STR = MEMORY_ALIGN$

$
$  ユーザスタック領域のメモリオブジェクト属性
$
$TARGET_MEMATR_USTACK = TA_MEMPRSV$

$
$  標準のセクションのメモリオブジェクト属性の定義
$
$MEMATR_TEXT = TA_NOWRITE | TA_EXEC$
$MEMATR_RODATA = TA_NOWRITE$
$MEMATR_DATA = TA_MEMINI$
$MEMATR_BSS = TA_NULL$
$MEMATR_PRSV = TA_MEMPRSV$
$MEMATR_SDATA = TA_MEMINI | TA_SDATA$
$MEMATR_SBSS = TA_SDATA$

$
$  スタートアップモジュールの定義
$
$START_OBJS = "start.o"$

$
$  ユーザスタック領域のセクション名
$  kernel.tfから呼ばれる
$
$FUNCTION SECTION_USTACK$
	$RESULT = FORMAT(".ustack_%1%", ARGV[1])$
$END$

$
$  ユーザスタック領域の確保
$  第1引数 タスクID
$  第2引数 スタックサイズ
$  kernel.tfから呼ばれる
$
$FUNCTION ALLOC_USTACK$
	$tskid = ARGV[1]$
	$ustksz = ARGV[2]$
	$ustksz = USTACK_ALIGN_SIZE(ustksz)$
	$TSK.TINIB_USTKSZ[tskid] = CONCAT(ustksz, "U")$
	StackType _kernel_ustack_$tskid$[COUNT_STK_T($TSK.TINIB_USTKSZ[tskid]$)]
	$SPC$__attribute__((section(".ustack_$tskid$,\"aw\",@nobits#"), nocommon));$NL$
	$TSK.TINIB_USTK[tskid] = CONCAT("_kernel_ustack_", tskid)$
$END$

$
$  ユーザスタックのアライメント制約に合わせたサイズを返す
$  第1引数 タスクID
$  kernel.tfから呼ばれる
$
$FUNCTION USTACK_ALIGN_SIZE$
	$RESULT = (ARGV[1] + CHECK_USTKSZ_ALIGN - 1) & ~(CHECK_USTKSZ_ALIGN - 1)$
$END$

$
$  基本タスクの共有スタックのセクション名
$  第1引数 共有スタックID(タスク優先度)
$  kernel.tfから呼ばれる
$
$FUNCTION SECTION_SHARED_USTACK$
	$RESULT = FORMAT(".shared_ustack_%1%", ARGV[1])$
$END$

$
$  基本タスクの共有スタックの確保
$  第1引数 共有スタック名
$  第2引数 共有スタックID(タスク優先度)
$  第3引数 スタックサイズ(アライメント調整済み)
$  kernel.tfから呼ばれる
$
$FUNCTION ALLOC_SHARED_USTACK$
	StackType $ARGV[1]$[COUNT_STK_T(ROUND_STK_T($ARGV[3]$))]
	$SPC$__attribute__((section(".shared_ustack_$ARGV[2]$,\"aw\",@nobits#"), nocommon));$NL$
$END$

$
$  非信頼フックスタックサイズのアライメント制約を満たすように拡張したサイズの文字列
$  第1引数 スタックサイズ
$  kernel.tfから呼ばれる
$
$FUNCTION HSTACK_SIZE_STR$
	TOPPERS_ROUND_SZ($ARGV[1]$, CHECK_USTKSZ_ALIGN)
$END$

$
$  標準のセクションの定義
$  kernel.tfから呼ばれる
$  MEMREGの定義値：
$    システム全体の標準ROMメモリリージョン	：1
$    システム全体の標準RAMメモリリージョン	：2
$    コアの標準ROMメモリリージョン			：3
$    コアの標準RAMメモリリージョン			：4
$    OSAPの標準ROMメモリリージョン			：5
$    OSAPの標準RAMメモリリージョン			：6
$
$DSEC.ORDER_LIST = { 1, 2, 3, 4, 5, 6, 7 }$

$DSEC.SECTION[1] = ".text"$
$DSEC.MEMATR[1] = MEMATR_TEXT$
$DSEC.MEMREG[1] = 5$

$DSEC.SECTION[2] = ".rodata"$
$DSEC.MEMATR[2] = MEMATR_RODATA$
$DSEC.MEMREG[2] = 5$

$DSEC.SECTION[3] = ".data"$
$DSEC.MEMATR[3] = MEMATR_DATA$
$DSEC.MEMREG[3] = 6$

$DSEC.SECTION[4] = ".bss"$
$DSEC.MEMATR[4] = MEMATR_BSS$
$DSEC.MEMREG[4] = 6$

$DSEC.SECTION[5] = ".prsv"$
$DSEC.MEMATR[5] = MEMATR_PRSV$
$DSEC.MEMREG[5] = 6$

$DSEC.SECTION[6] = ".sdata"$
$DSEC.MEMATR[6] = MEMATR_SDATA$
$DSEC.MEMREG[6] = 2$

$DSEC.SECTION[7] = ".sbss"$
$DSEC.MEMATR[7] = MEMATR_SBSS$
$DSEC.MEMREG[7] = 2$

$DSEC_SECTION_LIST = { ".rodata.str1.4" }$

$
$  リンカのためのセクション記述の生成
$  ldscript.tfから呼ばれる
$
$FUNCTION SECTION_DESCRIPTION$
	$IF EQ(ARGV[1], ".rodata")$
		$RESULT = ".rodata .rodata.str1.4"$
	$ELIF EQ(ARGV[1], ".bss")$
		$RESULT = ".bss COMMON"$
	$ELIF EQ(ARGV[1], ".text")$
		$RESULT = ".text .text.*"$
	$ELSE$
		$RESULT = ARGV[1]$
	$END$
$END$

$
$  ATT_SECに関するターゲット依存のエラーチェック
$  kernel.tfから呼ばれる
$
$FUNCTION HOOK_ERRORCHECK_SEC$
	$IF (SEC.MEMATR[ARGV[1]] & TA_SDATA) != 0 && !EQ(UNESCSTR(SEC.MEMREG[ARGV[1]]), REG.REGNAME[STANDARD_RAM])$
		$ERROR SEC.TEXT_LINE[ARGV[1]]$
			$_("small data memory is only allowed on STANDARD_RAM")$
		$END$
	$END$
$END$

$
$  ATT_MEMに関するターゲット依存のエラーチェック
$  kernel.tfから呼ばれる
$
$FUNCTION HOOK_ERRORCHECK_MEM$
$	// sizeがメモリ保護境界の制約に合致していない場合
	$IF (MEM.SIZE[ARGV[1]] & (MEMORY_ALIGN - 1)) != 0$
		$ERROR MEM.TEXT_LINE[ARGV[1]]$
			$FORMAT(_("%1% `%2%\' is not aligned to %3%"),
				"size", MEM.SIZE[ARGV[1]], MEMORY_ALIGN)$
		$END$
	$END$
$END$

$
$  gpレジスタの値の設定
$  ldscrip.tfから呼ばれる
$
$FUNCTION GENERATE_GP_LABEL$
	$TAB$$TAB$_gp = ABSOLUTE(. + 0x7ff0);$NL$
$END$

$
$  text領域をNOP命令で埋める
$  ldscrip.tfから呼ばれる
$
$FUNCTION TARGET_CODE_FILLER$
	$moid = ARGV[1]$
	$IF MO.MEMATR[moid] & TA_EXEC$
		$SPC$=0x0001883a
	$END$
$END$

$
$  ターゲット依存のOUTPUT記述の生成
$  ldscrip.tfから呼ばれる
$
$FUNCTION GENERATE_OUTPUT$
	OUTPUT_FORMAT("elf32-littlenios2", "elf32-littlenios2","elf32-littlenios2") $NL$
	OUTPUT_ARCH(nios2)$NL$
	ENTRY(__reset)$NL$
	$NL$
$END$

$
$  ターゲット依存のPROVIDE記述の生成
$  ldscrip.tfから呼ばれる
$
$FUNCTION GENERATE_PROVIDE$
	PROVIDE(hardware_init_hook = 0);$NL$
	PROVIDE(software_init_hook = 0);$NL$
	PROVIDE(StartupHook = 0);$NL$
	PROVIDE(ShutdownHook = 0);$NL$
	PROVIDE(PreTaskHook = 0);$NL$
	PROVIDE(PostTaskHook = 0);$NL$
	PROVIDE(ErrorHook = 0);$NL$
	PROVIDE(ProtectionHook = 0);$NL$
	$FOREACH osapid OSAP.ID_LIST$
		PROVIDE(StartupHook_$osapid$ = 0);$NL$
		PROVIDE(ShutdownHook_$osapid$ = 0);$NL$
		PROVIDE(ErrorHook_$osapid$ = 0);$NL$
		PROVIDE(ProtectionHook_$osapid$ = 0);$NL$
	$END$
$END$

$
$  ターゲット依存のセクション記述の生成
$  ldscrip.tfから呼ばれる
$
$FUNCTION GENERATE_SECTION_FIRST$
	$TAB$.entry : {$NL$
	$TAB$$TAB$ KEEP (*(.entry))$NL$
	$TAB$} > reset=0x0001883a$NL$
	$NL$

	$TAB$.exceptions : {$NL$
	$TAB$$TAB$*(.exceptions);$NL$
	$TAB$} > EXCEPT=0x0001883a$NL$
	$NL$
$END$

$
$	タスクスタックのMPUレジスタ設定値を出力
$
$FUNCTION GENERATE_STKMPUINFOB$
	$TAB$$TAB${ 0U, 0U }$NL$
$END$

$FUNCTION EXTERN_INT_HANDLER$
$	要因ごとの割込み入口処理のextern宣言
	$FOREACH intno INTNO_VALID$
		$isrid = INT.ISRID[intno]$
		$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
			$INT.ENTRY[intno] = CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$
			extern void $INT.ENTRY[intno]$(void) __attribute__((naked));$NL$
		$END$
	$END$
	$NL$

$	コア間割込みハンドラのextern宣言
	$FOREACH coreid RANGE(0, TMAX_COREID)$
		$FOREACH intno INTNO_ICI_LIST$
			$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
				extern void $CONCAT("_kernel_inthdr_", FORMAT("0x%x",+intno))$(void);$NL$
				extern ISR(target_ici_handler$coreid$);$NL$
			$END$
		$END$
	$END$

$	コア間割込み入口処理のextern宣言
	$FOREACH coreid RANGE(0, TMAX_COREID)$
		$FOREACH intno INTNO_ICI_LIST$
			$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
				extern void $CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$(void) __attribute__((naked));$NL$
			$END$
		$END$
	$END$
$END$

$
$  標準テンプレートファイルのインクルード
$
$INCLUDE "kernel/kernel.tf"$

$FOREACH intno INTNO_VALID$
	$FOREACH isrid ISR.ID_LIST$
		$IF intno == ISR.INTNO[isrid]$
			$INT.ISRID[intno] = isrid$
		$END$
	$END$
$END$


$FILE "Os_Lcfg.c"$
$NL$

$
$ コア間割込みハンドラ本体生成
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
	$FOREACH intno INTNO_ICI_LIST$
		$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
			void$NL$
			$CONCAT("_kernel_inthdr_", FORMAT("0x%x",+intno))$(void)$NL$
			{$NL$
			$TAB$i_begin_int($+intno$U);$NL$
			$TAB$ISRNAME(target_ici_handler$coreid$)();$NL$
			$TAB$i_end_int($+intno$U);$NL$
			}$NL$
		$END$
	$END$
$END$

$
$ C2ISRの優先度下限
$
const uint32 tmin_status_il = ((uint32) INT_IPM($MAX_PRI_ISR1+1$) << STATUS_IL_OFFSET);$NL$
$NL$

$
$  割込みハンドラテーブル
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
const FunctionRefType core$coreid$_isr_tbl[TNUM_INT] = {$NL$
$FOREACH intno INTNO_VALID$
$ $JOINEACH intno INTNO_VALID "\n"$
	$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
		$isrid = INT.ISRID[intno]$
		$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
			$TAB$&$ISR.INT_ENTRY[isrid]$
		$ELIF LENGTH(FIND(INTNO_ICI_LIST, intno))$
$			//コア間割割込みハンドラ
			$TAB$&$CONCAT("_kernel_inthdr_", FORMAT("0x%x",+intno))$
		$ELSE$
			$TAB$&default_int_handler
		$END$
$		//カンマの出力（最後の要素の後ろに出力しない）
		$IF (intno & 0xffff) < TMAX_INTNO$
			,
		$END$
		$TAB$$FORMAT("/* 0x%x */", +intno)$$NL$
	$END$
$END$
};$NL$
$NL$
$END$

const uint32 isr_table[TotalNumberOfCores] = {$NL$
$JOINEACH coreid RANGE(0, TMAX_COREID) ",\n"$
	$TAB$(const uint32) core$coreid$_isr_tbl
$END$
$NL$};$NL$
$NL$

$
$  ISRCBの取得テーブル
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
ISRCB * const core$coreid$_isr_p_isrcb_tbl[TNUM_INT] = {$NL$
$FOREACH intno INTNO_VALID$
	$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
		$isrid = INT.ISRID[intno]$
		$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
			$TAB$&_kernel_isrcb_$isrid$
		$ELSE$
			$TAB$NULL
		$END$
$		//カンマの出力（最後の要素の後ろに出力しない）
		$IF (intno & 0xffff) < TMAX_INTNO$
			,
		$END$
		$TAB$$FORMAT("/* 0x%x */", +intno)$$NL$
	$END$
$END$
};$NL$
$NL$
$END$

const uint32 isr_p_isrcb_table[TotalNumberOfCores] = {$NL$
$JOINEACH coreid RANGE(0, TMAX_COREID) ",\n"$
	$TAB$(const uint32) core$coreid$_isr_p_isrcb_tbl
$END$
$NL$};$NL$
$NL$

$
$ 割込みベクタと各割込み入口処理
$

$
$ 要因ごとの割込み入口処理
$
$FOREACH intno INTNO_VALID$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
		$INT.ENTRY[intno] = CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$
		void$NL$
		$INT.ENTRY[intno]$(void)$NL$
		$intno2 = intno & 0xffff$
		{$NL$
			$TAB$Asm("rdctl et, cpuid");$NL$
			$TAB$Asm("stw   r2, %0(et)" :: "i" (CCB_exception_temporary2));$NL$
			$TAB$Asm("movi  et, $intno2$");$NL$
			$TAB$Asm("jmpi  interrupt");$NL$
		}$NL$
		$NL$
	$END$
$END$

$
$ コア間割込み入口処理
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
	$FOREACH intno INTNO_ICI_LIST$
		$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
			void$NL$
			$CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$(void)$NL$
			$intno2 = intno & 0xffff$
			{$NL$
				$TAB$Asm("rdctl et, cpuid");$NL$
				$TAB$Asm("stw   r2, %0(et)" :: "i" (CCB_exception_temporary2));$NL$
				$TAB$Asm("movi  et, $intno2$");$NL$
				$TAB$Asm("jmpi  interrupt");$NL$
			}$NL$
			$NL$
		$END$
	$END$
$END$

$ ベクタテーブル
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
void __attribute__((naked))$NL$
core$coreid$_kernel_vectors(void)$NL$
{$NL$
$FOREACH intno INTNO_VALID$
	$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
		$isrid = INT.ISRID[intno]$
		$IF LENGTH(isrid)$
			$IF EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
				$TAB$Asm("jmpi $INT.ENTRY[intno]$");
			$ELSE$
				$TAB$Asm("jmpi $ISR.INT_ENTRY[isrid]$");
			$END$
		$ELIF LENGTH(FIND(INTNO_ICI_LIST, intno))$
$			// コア間割込みハンドラの登録
			$TAB$Asm("jmpi $CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$");
		$ELSE$
			
$			// 割込みハンドラの登録がない場合
			$TAB$Asm("jmpi default_int_handler");
		$END$
		$FORMAT(" /* 0x%x */", +intno)$$NL$
	$END$
$END$
}$NL$
$END$

const uint32 kernel_vectors_table[TotalNumberOfCores] = {$NL$
$JOINEACH coreid RANGE(0, TMAX_COREID) ",\n"$
	$TAB$(const uint32) &core$coreid$_kernel_vectors
$END$
$NL$};$NL$
$NL$
