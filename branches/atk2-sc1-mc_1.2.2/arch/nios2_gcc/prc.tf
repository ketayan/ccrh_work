$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2008-2015 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by Spansion LLC, USA
$  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2015 by Witz Corporation
$  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
$  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
$  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: prc.tf 1801 2015-03-27 06:34:43Z t_ishikawa $
$

$
$     パス2のアーキテクチャ依存テンプレート（Nios2用）
$

$
$ 割込み優先度の個数
$
$TNUM_INTPRI = 64$

$
$  CRE_ISR2で使用できる割込み番号
$
$INTNO_CREISR2_VALID = INTNO_VALID$

$FUNCTION EXTERN_INT_HANDLER$

$	コア間割込みハンドラ本体生成のextern宣言
	$FOREACH coreid RANGE(0, TMAX_COREID)$
		$FOREACH intno INTNO_ICI_LIST$
			$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
				extern void $CONCAT("_kernel_inthdr_", FORMAT("0x%x",+intno))$(void);$NL$
				extern ISR(target_ici_handler$coreid$);$NL$
			$END$
		$END$
	$END$
$NL$

$	要因ごとの割込み入口処理のextern宣言
	$FOREACH intno INTNO_VALID$
		$isrid = INT.ISRID[intno]$
		$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
			$INT.ENTRY[intno] = CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$
			extern void $INT.ENTRY[intno]$(void) __attribute__((naked));$NL$
		$END$
	$END$

$	コア間割込み入口処理
	$FOREACH coreid RANGE(0, TMAX_COREID)$
		$FOREACH intno INTNO_ICI_LIST$
			$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
				extern void $CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$(void) __attribute__((naked));$NL$
			$END$
		$END$
	$END$
$END$

$
$  標準テンプレートファイルのインクルード
$
$INCLUDE "kernel/kernel.tf"$


$FOREACH intno INTNO_VALID$
	$FOREACH isrid ISR.ID_LIST$
		$IF intno == ISR.INTNO[isrid]$
			$INT.ISRID[intno] = isrid$
		$END$
	$END$
$END$

$FILE "Os_Lcfg.c"$

$
$ コア間割込みハンドラ本体生成
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
	$FOREACH intno INTNO_ICI_LIST$
		$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
			void$NL$
			$CONCAT("_kernel_inthdr_", FORMAT("0x%x",+intno))$(void)$NL$
			{$NL$
			$TAB$i_begin_int($+intno$U);$NL$
			$TAB$ISRNAME(target_ici_handler$coreid$)();$NL$
			$TAB$i_end_int($+intno$U);$NL$
			}$NL$
		$END$
	$END$
$END$

$
$ C2ISRの優先度下限
$
const uint32 tmin_status_il = ((uint32) INT_IPM($MAX_PRI_ISR1+1$) << STATUS_IL_OFFSET);$NL$
$NL$

$
$  割込みハンドラテーブル
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
const FunctionRefType core$coreid$_isr_tbl[TNUM_INT] = {$NL$
$FOREACH intno INTNO_VALID$
$ $JOINEACH intno INTNO_VALID "\n"$
	$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
		$isrid = INT.ISRID[intno]$
		$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
			$TAB$&$ISR.INT_ENTRY[isrid]$
		$ELIF LENGTH(FIND(INTNO_ICI_LIST, intno))$
$			//コア間割割込みハンドラ
			$TAB$&$CONCAT("_kernel_inthdr_", FORMAT("0x%x",+intno))$
		$ELSE$
			$TAB$&default_int_handler
		$END$
$		//カンマの出力（最後の要素の後ろに出力しない）
		$IF (intno & 0xffff) < TMAX_INTNO$
			,
		$END$
		$TAB$$FORMAT("/* 0x%x */", +intno)$$NL$
	$END$
$END$
};$NL$
$NL$
$END$

const uint32 isr_table[TotalNumberOfCores] = {$NL$
$JOINEACH coreid RANGE(0, TMAX_COREID) ",\n"$
	$TAB$(const uint32) core$coreid$_isr_tbl
$END$
$NL$};$NL$
$NL$


$
$  ISRCBの取得テーブル
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
ISRCB * const core$coreid$_isr_p_isrcb_tbl[TNUM_INT] = {$NL$
$FOREACH intno INTNO_VALID$
	$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
		$isrid = INT.ISRID[intno]$
		$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
			$TAB$&_kernel_isrcb_$isrid$
		$ELSE$
			$TAB$NULL
		$END$
$		//カンマの出力（最後の要素の後ろに出力しない）
		$IF (intno & 0xffff) < TMAX_INTNO$
			,
		$END$
		$TAB$$FORMAT("/* 0x%x */", +intno)$$NL$
	$END$
$END$
};$NL$
$NL$
$END$

const uint32 isr_p_isrcb_table[TotalNumberOfCores] = {$NL$
$JOINEACH coreid RANGE(0, TMAX_COREID) ",\n"$
	$TAB$(const uint32) core$coreid$_isr_p_isrcb_tbl
$END$
$NL$};$NL$
$NL$

$
$ 割込みベクタと各割込み入口処理
$

$
$ 要因ごとの割込み入口処理
$
$FOREACH intno INTNO_VALID$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
		$INT.ENTRY[intno] = CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$
		void$NL$
		$INT.ENTRY[intno]$(void)$NL$
		$intno2 = intno & 0xffff$
		{$NL$
			$TAB$Asm("movi  et, $intno2$");$NL$
			$TAB$Asm("jmpi  interrupt");$NL$
		}$NL$
		$NL$
	$END$
$END$

$
$ コア間割込み入口処理
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
	$FOREACH intno INTNO_ICI_LIST$
		$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
			void$NL$
			$CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$(void)$NL$
			$intno2 = intno & 0xffff$
			{$NL$
				$TAB$Asm("movi  et, $intno2$");$NL$
				$TAB$Asm("jmpi  interrupt");$NL$
			}$NL$
			$NL$
		$END$
	$END$
$END$

$
$ ベクタテーブル
$
$FOREACH coreid RANGE(0, TMAX_COREID)$
	void __attribute__((naked))$NL$
	core$coreid$_kernel_vectors(void)$NL$
	{$NL$
	$FOREACH intno INTNO_VALID$
		$IF (intno & 0xffff0000) == ( (coreid+1) << 16)$
			$isrid = INT.ISRID[intno]$
			$IF LENGTH(isrid)$
				$IF EQ(ISR.CATEGORY[isrid], "CATEGORY_2")$
					$TAB$Asm("jmpi $INT.ENTRY[intno]$");
				$ELSE$
					$TAB$Asm("jmpi $ISR.INT_ENTRY[isrid]$");
				$END$
			$ELIF LENGTH(FIND(INTNO_ICI_LIST, intno))$
$				// コア間割込みハンドラの登録
				$TAB$Asm("jmpi $CONCAT("int_handler_entry_", FORMAT("0x%x",+intno))$");
			$ELSE$
$				// 割込みハンドラの登録がない場合
				$TAB$Asm("jmpi default_int_handler");
			$END$
			$FORMAT(" /* 0x%x */", +intno)$$NL$
		$END$
	$END$
	}$NL$
$END$const uint32 kernel_vectors_table[TotalNumberOfCores] = {$NL$
$JOINEACH coreid RANGE(0, TMAX_COREID) ",\n"$
	$TAB$(const uint32) &core$coreid$_kernel_vectors
$END$
$NL$};$NL$
$NL$
