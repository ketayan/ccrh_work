$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2014 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2014 by FUJI SOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by Spansion LLC, USA
$  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2014 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2014 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2014 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: prc_common.tf 74 2014-09-30 09:44:33Z ertl-ishikawa $
$

$
$  prc.tf, prc_mem.tf, prc_mem2.tfで使われる共通関数や共通処理を定義するファイル
$


$ 
$  有効な割込み番号，割込みハンドラ番号
$ 
$INTNO_VALID = { 
	0x10000,0x10001,...,0x1001f;
	0x20000,0x20001,...,0x2001f;
	0x30000,0x30001,...,0x3001f;
	0x40000,0x40001,...,0x4001f
}$

$ 
$  コア間割込み番号の定義
$ 
$	//コア0,コア1,コア2,コア3
$INTNO_ICI_LIST = {0x10002,0x20002,0x30002,0x40002}$

$ 
$  コア間割込み優先度の定義
$ 
$	//コア0,コア1,コア2,コア3
$INTPRI_ICI_LIST = {INTPRI_ICI0,INTPRI_ICI1,INTPRI_ICI2,INTPRI_ICI3}$

$ 
$ 有効な割込み番号の最大値
$ 
$TMAX_INTNO = 0x1f$

$MEMORY_ALIGN = +NIOS2_MPU_REGION_SIZE$

$
$  ショートデータセクションに配置される変数をジェネレータから参
$  照するためにLMAからVMAにコピーするために使うデータ構造の定義
$  コピー処理はkernel_opt.tf及びkernel_mem.tfで行われる
$
$IF !OMIT_IDATA && (CFG_PASS3 || CFG_PASS4)$
	$LMA.ORDER_LIST = { 1 }$
	$LMA.START_DATA[1] = FORMAT("__start_sdata_kernel")$
	$LMA.END_DATA[1] = FORMAT("__end_sdata_kernel")$
	$LMA.START_IDATA[1] = FORMAT("__start_isdata_kernel")$
$END$


$
$  第1引数と，第2引数がメモリ保護のアライメント制約を満たしているかチェックする
$
$FUNCTION CHECK_MP_ALIGN$
	$IF (ARGV[1] & (MEMORY_ALIGN - 1)) != 0
				|| (ARGV[2] & (MEMORY_ALIGN - 1)) != 0$
		$ERROR$
			$FORMAT(_("unaligned memory protection boundary [%x %x]"),
				ARGV[1], ARGV[2])$
		$END$
	$END$
$END$


$
$  OSAPINIBのMPUINFOBを出力する
$  kernel_common.tfから呼ばれる
$
$FUNCTION GENERATE_OSAPINIB_MPUINFOB$
	$osapid = ARGV[1]$
	$TAB$$TAB${$NL$
$	// 信頼の場合はMPUINFOBはない
	$IF OSAP.TRUSTED[osapid]$
		$TAB$$TAB$$TAB$NULL,$NL$
		$TAB$$TAB$$TAB$0U,$NL$
		$TAB$$TAB$$TAB$0U$NL$
	$ELSE$
		$TAB$$TAB$$TAB$mpuinfo_$osapid$,$NL$
		$TAB$$TAB$$TAB$$OSAP.TNUM_INST_REGION[osapid]$U,$NL$
		$TAB$$TAB$$TAB$$OSAP.TNUM_DATA_REGION[osapid]$U$NL$
	$END$
	$TAB$$TAB$}$NL$
$END$

$
$  rodata領域に配置される変数を定義する
$  Nios2ではconstを付けても8バイト以下はsdataに配置されるので，
$  __attribute__を使ってrodataに配置する
$  kernel.tf, kernel_mem.tfから呼ばれる
$
$FUNCTION DEFINE_CONST_VAR$
	$ARGV[1]$ $ARGV[2]$ __attribute__((section(".rodata")))
$END$

$
$  タスクスタックのMPUレジスタ設定値を出力(パス3, パス4)
$
$IF CFG_PASS3 || CFG_PASS4$
	$FUNCTION GENERATE_STKMPUINFOB$
		$tskid = ARGV[1]$
		$IF OSAP.TRUSTED[TSK.OSAPID[tskid]]$
			$TAB$$TAB${ 0U, 0U }$NL$
		$ELSE$
			$moid = TSK.USTACK_MO[tskid]$
			$start = MO.BASEADDR[moid]$
			$limit = MO.LIMITADDR[moid]$

			$IF CFG_PASS4$
				$CHECK_MP_ALIGN(start, limit)$
			$END$

			$TAB$$TAB${$NL$
			$TAB$$TAB$$TAB$NIOS2_DATA_MPUBASE(NIOS2_MPU_STACK_REGION, $FORMAT("0x%08xU", +start)$),$NL$
			$TAB$$TAB$$TAB$NIOS2_MPUACC_DATA(NIOS2_MPU_PERM_SYS_RW_USR_RW,    $FORMAT("0x%08xU", +limit)$)$NL$
			$TAB$$TAB$}$NL$
		$END$
	$END$
$END$

$
$  MPUレジスタ設定値の生成(パス2, パス3, パス4)
$
$FUNCTION GENERATE_TARGET_MPUINFOB$

$	// MPU_REGIONを作成
	$GENERATE_MPU_REGION()$

	$IF CFG_PASS3 || CFG_PASS4$
$		// MPU_REGIONの統合
		$MERGE_MPU_REGION()$
	$END$

$	// 共有領域のMPUレジスタ設定値を生成する
	$GENERATE_SHARED_MPUINFOB()$

$	// OSAP専有領域のMPUレジスタ設定値を生成する
	$GENERATE_OSAP_MPUINFOB()$
$END$

$
$  MPU_REGIONを作成
$
$FUNCTION GENERATE_MPU_REGION$
$	// 共有領域に対するMPU_REGIONの作成
	$GENERATE_MPU_REGION_SUB(TACP_SHARED)$

	$FOREACH osapid OSAP.ID_LIST$
		$IF !OSAP.TRUSTED[osapid]$
$			// 非信頼OSAP専有領域に対するMPU_REGIONの作成
			$GENERATE_MPU_REGION_SUB(DEFAULT_ACPTN[osapid])$
		$END$
	$END$

$	// MPU_REGIONのSTART, LIMIT, BASEADDR, LIMITADDRの設定
	$FOREACH i MPU_REGION.ID_LIST$
		$moid = MPU_REGION.MOID[i]$
		$MPU_REGION.START[i] = MO.START[moid]$
		$MPU_REGION.LIMIT[i] = MO.LIMIT[moid]$
		$IF CFG_PASS3 || CFG_PASS4$
			$MPU_REGION.BASEADDR[i]  = MO.BASEADDR[MPU_REGION.MOID[i]]$
			$MPU_REGION.LIMITADDR[i] = MO.LIMITADDR[MPU_REGION.MOID[i]]$
		$ELSE$
$			// パス2ではBASEADDRとLIMITADDRは分からない
			$MPU_REGION.BASEADDR[i] = 0$
			$MPU_REGION.LIMITADDR[i] = 0$
		$END$
	$END$
$END$

$
$  MPU_REGIONを作成(サブ関数)
$  第1引数 対象アクセスパターン
$
$FUNCTION GENERATE_MPU_REGION_SUB$
	$acptn = ARGV[1]$

	$FOREACH moid MO_SECTION_LIST$

$		// アクセスパターンが第1引数と一致するMOを対象とする(タスクユーザスタックは除外)
		$IF (MO.ACPTN_R[moid] == acptn || MO.ACPTN_W[moid] == acptn || MO.ACPTN_X[moid] == acptn) &&
			MO.TYPE[moid] != TOPPERS_USTACK$

			$use_inst_mpu = 0$
			$use_data_mpu = 0$

			$read  = (MO.ACPTN_R[moid] == TACP_SHARED) || (MO.ACPTN_R[moid] == acptn)$
			$write = (MO.ACPTN_W[moid] == TACP_SHARED) || (MO.ACPTN_W[moid] == acptn)$
			$exec  = (MO.ACPTN_X[moid] == TACP_SHARED) || (MO.ACPTN_X[moid] == acptn)$

			$IF exec$
				$inst_mpu_atr = "NIOS2_MPU_PERM_SYS_EXC_USR_EXC"$
				$use_inst_mpu = 1$
			$END$

			$IF read || write$
				$use_data_mpu = 1$

				$IF !write$
$					// 特権の場合は書き込み禁止領域でも書き込み可能にする
					$data_mpu_atr = "NIOS2_MPU_PERM_SYS_RW_USR_RO"$
				$ELSE$
					$data_mpu_atr = "NIOS2_MPU_PERM_SYS_RW_USR_RW"$
				$END$
			$END$

			$IF use_inst_mpu$
				$i = LENGTH(MPU_REGION.ID_LIST) + 1$
				$MPU_REGION.IS_INST_REGION[i] = 1$
				$MPU_REGION.ATR[i] = inst_mpu_atr$
				$MPU_REGION.ACPTN[i] = acptn$
				$MPU_REGION.MOID[i] = moid$
				$MPU_REGION.ID_LIST = APPEND(MPU_REGION.ID_LIST, i)$
			$END$
			$IF use_data_mpu$
				$i = LENGTH(MPU_REGION.ID_LIST) + 1$
				$MPU_REGION.IS_INST_REGION[i] = 0$
				$MPU_REGION.ATR[i] = data_mpu_atr$
				$MPU_REGION.ACPTN[i] = acptn$
				$MPU_REGION.MOID[i] = moid$
				$MPU_REGION.ID_LIST = APPEND(MPU_REGION.ID_LIST, i)$
			$END$
		$END$
	$END$
$END$

$
$  MPU_REGIONを統合する
$
$FUNCTION MERGE_MPU_REGION$
$	// 共有領域のMPU_REGIONを統合する
	$MERGE_MPU_REGION_SUB(TACP_SHARED)$

	$FOREACH osapid OSAP.ID_LIST$
		$IF !OSAP.TRUSTED[osapid]$
$			// OSAPのMPU_REGIONを統合する
			$MERGE_MPU_REGION_SUB(DEFAULT_ACPTN[osapid])$
		$END$
	$END$

$	// 統合したMPU_REGIONの設定
	$MPU_REGION.ID_LIST = merged_mpu_region$
$END$


$
$  共有領域のMPUレジスタ設定値を出力する
$
$FUNCTION GENERATE_SHARED_MPUINFOB$

$	// 命令領域の開始インデックス
	$mpu_inst_reg_no = NIOS2_MPU_BKGND_CODE_REGION - 1$
$	// データ領域の開始インデックス
	$mpu_data_reg_no = NIOS2_MPU_BKGND_DATA_REGION - 1$

	$IF CFG_PASS4$
		$shared_region_size = PEEK(SYMBOL("tnum_shared_region"), sizeof_uint8)$
	$END$

$	// MPUレジスタ設定値の出力
	$OUTPUT_MPU_REGION(TACP_SHARED, "shared_region", shared_region_size)$

	const uint8 tnum_shared_region = $((NIOS2_MPU_BKGND_DATA_REGION - 1) - mpu_inst_reg_no) +
									  ((NIOS2_MPU_BKGND_CODE_REGION - 1) - mpu_data_reg_no)$U;$NL$

	$shared_mpu_inst_reg_cnt = mpu_inst_reg_no$
	$shared_mpu_data_reg_cnt = mpu_data_reg_no$
$END$

$
$  OSAP専有領域のMPUレジスタ設定値を出力する
$
$FUNCTION GENERATE_OSAP_MPUINFOB$
	$IF !OMIT_OSAPMPUINFOB$
		$offset = 0$
		$FOREACH osapid OSAP.ID_LIST$
			$IF !OSAP.TRUSTED[osapid]$
$				// 命令領域の開始インデックス
				$mpu_inst_reg_no = 0$
$				// データ領域の開始インデックス
				$mpu_data_reg_no = NIOS2_MPU_PRV_DATA_REGION_START$

				$IF CFG_PASS4$
					$mpuinfo_size = PEEK(SYMBOL("osapinib_table") + offset + offsetof_OSAPINIB_tnum_inst_region, sizeof_uint8) +
									PEEK(SYMBOL("osapinib_table") + offset + offsetof_OSAPINIB_tnum_data_region, sizeof_uint8)$
				$END$

$				// MPUレジスタ設定値の出力
				$OUTPUT_MPU_REGION(DEFAULT_ACPTN[osapid], CONCAT("mpuinfo_", osapid), mpuinfo_size)$

$				// 生成した命令領域数
				$OSAP.TNUM_INST_REGION[osapid] = mpu_inst_reg_no$
$				// 生成したデータ領域数
				$OSAP.TNUM_DATA_REGION[osapid] = mpu_data_reg_no - NIOS2_MPU_PRV_DATA_REGION_START$

				$IF CFG_PASS4$
$					// 領域数の上限を超えた場合
					$IF mpu_data_reg_no - 1 >= shared_mpu_data_reg_cnt + 1 ||
						mpu_inst_reg_no - 1 >= shared_mpu_inst_reg_cnt + 1$
						$ERROR$$FORMAT(_("too many memory regions"))$$END$
					$END$
				$END$
			$END$
			$offset = offset + sizeof_OSAPINIB$
		$END$
	$END$
$END$

$
$  MPU_REGIONの統合(サブ関数)
$
$FUNCTION MERGE_MPU_REGION_SUB$
	$acptn = ARGV[1]$

	$FOREACH is_inst_region {1, 0}$
		$merge_reg_id = -1$
		$FOREACH i SORT(MPU_REGION.ID_LIST, "MPU_REGION.BASEADDR")$
			$IF MPU_REGION.ACPTN[i] == acptn &&
				MPU_REGION.BASEADDR[i] != MPU_REGION.LIMITADDR[i] &&
				MPU_REGION.IS_INST_REGION[i] == is_inst_region$
$				// 領域が隣接していて，属性が同じ場合は統合する
				$IF merge_reg_id != -1 &&
$					// パス4はメモリリージョンやリンカの判定は不要
					(CFG_PASS4 ||
					 MO.LINKER[MPU_REGION.MOID[merge_reg_id]] && MO.LINKER[MPU_REGION.MOID[i]] &&
					 MO.MEMREG[MPU_REGION.MOID[merge_reg_id]] == MO.MEMREG[MPU_REGION.MOID[i]]) &&
					MPU_REGION.LIMITADDR[merge_reg_id] == MPU_REGION.BASEADDR[i] &&
					EQ(MPU_REGION.ATR[merge_reg_id], MPU_REGION.ATR[i])$

					$MPU_REGION.LIMITADDR[merge_reg_id] = MPU_REGION.LIMITADDR[i]$
					$MPU_REGION.LIMIT[merge_reg_id] = MPU_REGION.LIMIT[i]$
				$ELSE$
					$merge_reg_id = i$
					$merged_mpu_region = APPEND(merged_mpu_region, i)$
					$MPU_REGION.LIMITADDR[i] = MPU_REGION.LIMITADDR[i]$
					$MPU_REGION.LIMIT[i] = MPU_REGION.LIMIT[i]$
				$END$
			$END$
		$END$
	$END$
$END$

$
$  MPUレジスタ設定値を出力する
$  第1引数 出力対象のアクセスパターン
$  第2引数 出力する変数名
$  第3引数 配列のサイズ
$
$FUNCTION OUTPUT_MPU_REGION$

	$acptn = ARGV[1]$
	$name = ARGV[2]$
	$array_size = ARGV[3]$
	$output = 0$

	$FOREACH is_inst_region {1, 0}$
		$FOREACH i MPU_REGION.ID_LIST$

			$IF MPU_REGION.ACPTN[i] == acptn &&
				MPU_REGION.IS_INST_REGION[i] == is_inst_region$

				$IF !output$
					$output = 1$
					$IF acptn != TACP_SHARED$
						static$SPC$
					$END$
					const MPUINFOB $name$[$array_size$] = {$NL$
				$END$
				$IF CFG_PASS4$
					$CHECK_MP_ALIGN(MPU_REGION.BASEADDR[i], MPU_REGION.LIMITADDR[i])$
				$END$

				$IF is_inst_region$
					$mpubase = "NIOS2_INST_MPUBASE"$
					$mpuacc  = "NIOS2_MPUACC"$
					$reg_no  = mpu_inst_reg_no$
				$ELSE$
					$mpubase = "NIOS2_DATA_MPUBASE"$
					$mpuacc  = "NIOS2_MPUACC_DATA"$
					$reg_no  = mpu_data_reg_no$
				$END$

				$TAB${$NL$
				$TAB$$TAB$$mpubase$($FORMAT("%dU", +reg_no)$, $FORMAT("0x%08xU", +MPU_REGION.BASEADDR[i])$)
				$SPC$/* $MPU_REGION.START[i]$ */,$NL$
				$TAB$$TAB$$mpuacc$($MPU_REGION.ATR[i]$, $FORMAT("0x%08xU", +MPU_REGION.LIMITADDR[i])$)
				$SPC$/* $MPU_REGION.LIMIT[i]$ */$NL$
				$TAB$},$NL$

				$IF acptn == TACP_SHARED$
$					// 共有領域の場合はインデックスを小さくしていく
					$IF is_inst_region$
						$mpu_inst_reg_no = mpu_inst_reg_no - 1$
					$ELSE$
						$mpu_data_reg_no = mpu_data_reg_no - 1$
					$END$
				$ELSE$
$					// OSAP専有領域の場合はインデックスを大きくしていく
					$IF is_inst_region$
						$mpu_inst_reg_no = mpu_inst_reg_no + 1$
					$ELSE$
						$mpu_data_reg_no = mpu_data_reg_no + 1$
					$END$
				$END$
			$END$
		$END$
	$END$

	$IF !output$
		$IF LENGTH(array_size) && array_size > 0$
			$IF acptn != TACP_SHARED$
				static$SPC$
			$END$
			const MPUINFOB $name$[$array_size$] = {{0,0}};$NL$
		$ELSE$
			TOPPERS_EMPTY_LABEL(const MPUINFOB, $name$);$NL$
		$END$
	$ELSE$
		};$NL$
	$END$
	$NL$
$END$

$
$  変数を指定されたセクションに定義
$  第1引数 セクション名
$  第2引数 型
$  第3引数 変数名
$  第4引数 変数の値
$
$FUNCTION DEFINE_VAR_SEC$
	$section = ARGV[1]$
	$type = ARGV[2]$
	$name = ARGV[3]$
	$val = ARGV[4]$
	$type$ $name$ __attribute__((section($ESCSTR(section)$))) = $val$;$NL$
$END$

$
$  CCBの配置先指定
$
$FUNCTION GENERATE_CCB$
	CCB _kernel_core$+ARGV[1]$_ccb$TAB$__attribute__((section("._kernel_core$+ARGV[1]$s_ccb"),nocommon)) = {0, FALSE, 0, 0, 0, 0, 0};$NL$
$END$

$
$  CCBの外部参照宣言
$
$FUNCTION GENERATE_CCB_TARGET_EXTERN$
	extern CCB _kernel_core$+ARGV[1]$_ccb;$NL$
$END$

$
$ コアローカルメモリに配置するためのセクション命名
$
$FUNCTION CORE_LOCAL_MEM_SECTION$
	$IF LENGTH(ARGV[1])$
		$RESULT = FORMAT("\t__attribute__((section(\"._kernel_core%1%_obj\"), nocommon))", +ARGV[1])$
	$ELSE$
		$RESULT = ""$
	$END$
$END$

$
$ OSAPローカルメモリに配置するためのセクション命名
$
$FUNCTION OSAP_LOCAL_MEM_SECTION$
	$IF LENGTH(ARGV[1])$
		$RESULT = FORMAT("\t__attribute__((section(\"._kernel_osap%1%_obj\"), nocommon))", +ARGV[1])$
	$ELSE$
		$RESULT = ""$
	$END$
$END$

$
$  OSSTACKの配置先指定
$
$FUNCTION GENERATE_OSSTACK_DEF$
	static StackType _kernel_core$+ARGV[1]$_ostack[$FORMAT("COUNT_STK_T(0x%xU)", +OSTK.STKSZ[OSTK.COREID[ARGV[1]]])$]$TAB$
		$ARGV[2]$;$NL$
$END$

$
$  TCBの配置先指定
$
$FUNCTION GENERATE_TCB$
	TCB _kernel_tcb_$ARGV[1]$$ARGV[2]$;$NL$
$END$

$
$  RESCBの配置先指定
$
$FUNCTION GENERATE_RESCB$
	RESCB _kernel_rescb_$ARGV[1]$$ARGV[2]$;$NL$
$END$

$
$  ISRCBの配置先指定
$
$FUNCTION GENERATE_ISRCB$
	ISRCB _kernel_isrcb_$ARGV[1]$$ARGV[2]$;$NL$
$END$

$
$  CNTCBの配置先指定
$
$FUNCTION GENERATE_CNTCB$
	CNTCB _kernel_cntcb_$ARGV[1]$$ARGV[2]$;$NL$
$END$

$
$  ALMCBの配置先指定
$
$FUNCTION GENERATE_ALMCB$
	ALMCB _kernel_almcb_$ARGV[1]$$ARGV[2]$;$NL$
$END$

$
$  SCHTBLCBの配置先指定
$
$FUNCTION GENERATE_SCHTBLCB$
	SCHTBLCB _kernel_schtblcb_$ARGV[1]$$ARGV[2]$;$NL$
$END$

$
$  OSAPCBの配置先指定
$
$FUNCTION GENERATE_OSAPCB$
	OSAPCB _kernel_osapcb_$ARGV[1]$$ARGV[2]$;$NL$
$END$

$
$ コアローカルメモリにシステムスタック用セクション命名
$
$FUNCTION CORE_LOCAL_MEM_SSTACK_SECTION$
	$IF LENGTH(ARGV[1])$
		$RESULT = FORMAT("\t__attribute__((section(\"._kernel_core%1%_prsv_kernel,\\\"aw\\\",@nobits#\"), aligned(CHECK_STACK_ALIGN), nocommon))", +ARGV[1])$
	$ELSE$
		$RESULT = FORMAT("\t__attribute__((section(\".prsv_kernel,\\\"aw\\\",@nobits#\"), aligned(CHECK_STACK_ALIGN), nocommon))")$
	$END$
$END$

$
$ コアローカルメモリに非信頼フック用スタック用セクション命名
$
$FUNCTION CORE_LOCAL_MEM_HSTACK_SECTION$
	$IF LENGTH(ARGV[1])$
		$RESULT = FORMAT("\t__attribute__((section(\"._kernel_core%1%_prsv_kernel,\\\"aw\\\",@nobits#\"), aligned(NIOS2_MPU_REGION_SIZE), nocommon))", +ARGV[1])$
	$ELSE$
		$RESULT = FORMAT("\t__attribute__((section(\".prsv_kernel,\\\"aw\\\",@nobits#\"), aligned(NIOS2_MPU_REGION_SIZE), nocommon))")$
	$END$
$END$

$
$  システムスタック領域の確保
$  第1引数 スタック名
$  第2引数 スタックサイズ
$  第3引数 変数定義に付与する指定子
$  kernel.tfから呼ばれる
$
$FUNCTION ALLOC_SSTACK$
	$IF !EQ(ARGV[3], "")$
		$ARGV[3]$$SPC$
	$END$
	StackType $ARGV[1]$[COUNT_STK_T(ROUND_STK_T($ARGV[2]$))]$ARGV[4]$;$NL$
$END$

$
$  非信頼フックスタックの確保
$  第1引数 スタック名
$  第2引数 スタックサイズ
$  kernel.tfから呼ばれる
$
$FUNCTION ALLOC_HSTACK$
	$name = ARGV[1]$
	$size = ARGV[2]$
	StackType $name$[COUNT_STK_T(ROUND_STK_T($HSTACK_SIZE_STR(size)$))]$ARGV[3]$;$NL$
$END$

