/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2014 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2014 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2013 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2014 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2014 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2014 by Witz Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: avalon_timer.c 74 2014-09-30 09:44:33Z ertl-ishikawa $
 */

/*
 *		タイマドライバ（Avalon Timer）
 */

#include "kernel_impl.h"
#include "target_timer.h"
#include "avalon_timer.h"
#include "Os_Lcfg.h"

const uint32
target_sys_clk_timer_base_table[TotalNumberOfCores] = {
	SYS_CLK_TIMER_0_BASE,
#if TotalNumberOfCores > 1
	SYS_CLK_TIMER_1_BASE,
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	SYS_CLK_TIMER_2_BASE,
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	SYS_CLK_TIMER_3_BASE,
#endif /* TotalNumberOfCores > 3 */
};

const uint32
target_sys_clk_timer_intno_table[TotalNumberOfCores] = {
	INTNO_TIMER0,
#if TotalNumberOfCores > 1
	INTNO_TIMER1,
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	INTNO_TIMER2,
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	INTNO_TIMER3,
#endif /* TotalNumberOfCores > 3 */
};


/*
 *  現在のシステム時刻（単位: ミリ秒）
 *
 *  厳密には，前のタイムティックのシステム時刻
 */
static SystemTimeMsType current_time[TotalNumberOfCores];

/*
 *  マイクロ秒単位での時刻を取得
 *  タイマー割込み入り口トレスログの取得時間が1ms少ない値となる
 */
SystemTimeUsType
get_tim_utime(void)
{
	SystemTimeUsType	utime;
	SystemTimeMsType	mtime;
	TickType			clock1, clock2;
	boolean				ireq;
	SIL_PRE_LOC;

	SIL_LOC_INT();
	mtime = current_time[x_core_id()];
	clock1 = target_timer_get_current();
	ireq = target_timer_probe_int();
	clock2 = target_timer_get_current();
	SIL_UNL_INT();
	utime = ((SystemTimeUsType) mtime) * 1000U;

	if ((ireq != FALSE) && (clock2 > clock1)) {
		utime += 1000U;
	}
	utime += TO_USEC(clock1);
	return(utime);
}

/*
 *  100ナノ秒単位での時刻を取得
 */
SystemTime100NsType
get_tim_100ntime(void)
{
	SystemTime100NsType	ntime;
	SystemTimeMsType	mtime;
	TickType			clock1, clock2;
	boolean				ireq;
	SIL_PRE_LOC;

	SIL_LOC_INT();
	mtime = current_time[x_core_id()];
	clock1 = target_timer_get_current();
	ireq = target_timer_probe_int();
	clock2 = target_timer_get_current();
	SIL_UNL_INT();
	ntime = ((SystemTime100NsType) mtime) * 10000U;

	if ((ireq != FALSE) && (clock2 >= clock1)) {
		ntime += 10000U;
	}
	ntime += TO_100NSEC(clock1);
	return(ntime);
}

/*
 *  タイマの起動処理
 */
void
target_timer_initialize(void)
{
	uint32 base = target_sys_clk_timer_base_table[x_core_id()];

	current_time[x_core_id()] = 0U;

	/* タイマーストップ */
	sil_wrw_iop((void *) (base + AVALON_TIM_CONTROL), AVALON_TIM_CONTROL_STOP);
	/* タイムアウトステータスクリア */
	sil_wrw_iop((void *) (base + AVALON_TIM_STATUS), 0x00U);

	sil_wrw_iop((void *) (base + AVALON_TIM_PERIODL), (TIMER_CLOCK & 0xffffU));         /* カウンターセット 下位16bit */
	sil_wrw_iop((void *) (base + AVALON_TIM_PERIODH), ((uint32) TIMER_CLOCK >> 16));    /* カウンターセット 上位16bit */

	/*
	 * タイマースタート，オートリロード，割込み許可
	 */
	sil_wrw_iop((void *) (base + AVALON_TIM_CONTROL), AVALON_TIM_CONTROL_START
				| AVALON_TIM_CONTROL_CONT | AVALON_TIM_CONTROL_ITO);
}

/*
 *  タイマの停止処理
 */
void
target_timer_terminate(void)
{
	uint32 base = target_sys_clk_timer_base_table[x_core_id()];

	/* タイマ停止 */
	sil_wrw_iop((void *) (base + AVALON_TIM_CONTROL), AVALON_TIM_CONTROL_STOP);

	/* 割込み要求のクリア */
	sil_wrw_iop((void *) (base + AVALON_TIM_STATUS), 0x00U);
}

/*
 *  target_timer_core0.arxmlを使用しない場合の対処
 */
#ifndef SysTimerCnt0
#define SysTimerCnt0		UINT_C(0)
#endif /* SysTimerCnt0 */

/*
 *  target_timer_core1.arxmlを使用しない場合の対処
 */
#ifndef SysTimerCnt1
#define SysTimerCnt1		UINT_C(1)
#endif /* SysTimerCnt1 */

/*
 *  タイマ割込みハンドラの共通処理
 */
LOCAL_INLINE void
target_timer_handler(CounterType cntid, CoreIdType coreid)
{
	StatusType	ercd;
	uint32		base = target_sys_clk_timer_base_table[coreid];

	/* TOビットのクリア */
	sil_wrw_iop((void *) (base + AVALON_TIM_STATUS), 0x00U);

	/* current_timeを更新する */
	current_time[coreid]++;

	/*
	 *  カウンタ加算通知処理実行
	 */
	ercd = IncrementCounter(cntid);
	/* エラーリターンの場合はシャットダウン */
	if (ercd != E_OK) {
		ShutdownAllCores(ercd);
	}
}

/*
 *  タイマ割込みハンドラ
 */
ISR(target_timer_hdr0)
{
	target_timer_handler(SysTimerCnt0, 0U);
}

ISR(target_timer_hdr1)
{
	target_timer_handler(SysTimerCnt1, 1U);
}
