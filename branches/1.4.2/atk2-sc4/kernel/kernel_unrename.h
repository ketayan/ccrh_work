/* This file is generated from kernel_rename.def by genrename. */

/* This file is included only when kernel_rename.h has been included. */
#ifdef TOPPERS_KERNEL_RENAME_H
#undef TOPPERS_KERNEL_RENAME_H

/*
 *  alarm.c
 */
#undef alarm_initialize
#undef alarm_expire
#undef force_term_osap_alarm

/*
 *  counter.c
 */
#undef insert_cnt_expr_que
#undef delete_cnt_expr_que
#undef counter_initialize
#undef counter_terminate
#undef get_reltick
#undef get_abstick
#undef expire_process
#undef force_term_osap_counter

/*
 *  counter_manage.c
 */
#undef notify_hardware_counter
#undef incr_counter_process
#undef incr_counter_action

/*
 *  event.c
 */
#undef set_event_action

/*
 *  interrupt.c
 */
#undef interrupt_initialize
#undef release_interrupts
#undef exit_isr2

/*
 *  ioc_manage.c
 */
#undef ioc_initialize

/*
 *  memory.c
 */
#undef check_address_stack
#undef search_meminib
#undef check_osap_memory
#undef check_isr_memory
#undef check_task_memory
#undef probe_memory_access
#undef probe_memory_read
#undef probe_memory_write
#undef probe_memory_read_write
#undef initialize_sections

/*
 *  osap.c
 */
#undef osap_initialize
#undef force_term_osap_main
#undef force_term_osap

/*
 *  osctl.c
 */
#undef internal_call_errorhook
#undef call_posttaskhook
#undef call_pretaskhook
#undef init_stack_magic_region
#undef call_protectionhk_main
#undef internal_shutdownos
#undef internal_call_shtdwnhk

/*
 *  osctl_manage.c
 */
#undef callevel_stat
#undef appmodeid
#undef kerflg
#undef run_trusted
#undef pre_protection_supervised
#undef fatal_file_name
#undef fatal_line_num

/*
 *  resource.c
 */
#undef resource_initialize

/*
 *  scheduletable.c
 */
#undef schtbl_initialize
#undef schtbl_expire
#undef schtbl_expiry_process
#undef schtbl_head
#undef schtbl_exppoint_process
#undef schtbl_tail
#undef force_term_osap_schtbl

/*
 * svc_table.c
 */
#undef svc_table

/*
 *  task.c
 */
#undef p_runtsk
#undef p_schedtsk
#undef nextpri
#undef ready_queue
#undef ready_primap
#undef task_initialize
#undef search_schedtsk
#undef make_runnable
#undef make_non_runnable
#undef make_active
#undef preempt
#undef suspend
#undef release_taskresources
#undef exit_task
#undef remove_task_from_queue
#undef force_terminate_task
#undef force_term_osap_task
#undef move_schedtsk

/*
 *  task_manage.c
 */
#undef activate_task_action

/*
 *  timingprotection.c
 */
#undef is_tp_timer_running
#undef tp_time_count
#undef tp_initialize
#undef tp_terminate
#undef tp_start_timer
#undef tp_stop_timer
#undef tp_stop_task_monitor
#undef tp_get_current_time
#undef tp_check_arrival_time
#undef tp_fault_handler
#undef tp_timer_handler

/*
 *  kernel_mem.c	Os_Lcfg.c
 */
#undef tnum_meminib
#undef memtop_table
#undef meminib_table
#undef tnum_datasec
#undef datasecinib_table
#undef tnum_bsssec
#undef bsssecinib_table
#undef shared_region
#undef tnum_shared_region
#undef tinib_table
#undef osapinib_table
#undef osapcb_table
#undef tnum_alarm
#undef tnum_counter
#undef tnum_hardcounter
#undef tnum_isr2
#undef tnum_stdresource
#undef tnum_task
#undef tnum_task_inc_rt
#undef tnum_exttask
#undef tnum_appmode
#undef tnum_scheduletable
#undef tnum_implscheduletable
#undef tnum_tfn
#undef tnum_osap
#undef intpri_tp
#undef tcb_table
#undef cntinib_table
#undef cntcb_table
#undef hwcntinib_table
#undef alminib_table
#undef almcb_table
#undef schtblinib_table
#undef schtblcb_table
#undef resinib_table
#undef rescb_table
#undef object_initialize
#undef object_terminate
#undef tnum_intno
#undef intinib_table
#undef isrinib_table
#undef isrcb_table
#undef tfinib_table
#undef ostk
#undef ostksz
#undef ostkpt
#undef tnum_ioc
#undef tnum_queueioc
#undef tnum_ioc_wrapper_send
#undef tnum_ioc_wrapper
#undef ioc_inival_table
#undef ioccb_table
#undef iocinib_table
#undef iocwrpinib_table
#undef appid_str
#undef tskid_str
#undef isrid_str
#undef cntid_str
#undef almid_str
#undef resid_str
#undef schtblid_str
#undef evtid_str
#undef osapid_str
#undef iocid_str
#undef tfnid_str



#include "target_unrename.h"

#endif /* TOPPERS_KERNEL_RENAME_H */
