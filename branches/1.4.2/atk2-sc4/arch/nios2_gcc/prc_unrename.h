/* This file is generated from prc_rename.def by genrename. */

/* This file is included only when prc_rename.h has been included. */
#ifdef TOPPERS_PRC_RENAME_H
#undef TOPPERS_PRC_RENAME_H

/*
 *  prc_config.c
 */
#undef prc_hardware_initialize
#undef prc_initialize
#undef prc_terminate
#undef x_config_int
#undef default_int_handler
#undef no_support_service

/*
 *  kernel_mem.c	Os_Lcfg.c
 */
#undef tmin_status_il
#undef isr_tbl
#undef isr_p_isrcb_tbl

/*
 *  prc_support.S
 */
#undef exception_entry
#undef interrupt

#undef dispatch
#undef start_dispatch
#undef exit_and_dispatch_nohook
#undef exit_and_dispatch
#undef start_stask_r
#undef start_utask_r
#undef start_r
#undef stack_change_and_call_func_1
#undef stack_change_and_call_func_2
#undef trustedfunc_stack_check
#undef int_tp_fault_handler
#undef int_tp_timer_handler

/*
 *  prc_mpu.c
 */
#undef prc_set_osap_mpu
#undef prc_init_mpu
#undef probe_trusted_osap_mem


#endif /* TOPPERS_PRC_RENAME_H */
