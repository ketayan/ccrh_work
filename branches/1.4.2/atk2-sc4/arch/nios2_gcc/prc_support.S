/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2017 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2017 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2017 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: prc_support.S 727 2017-01-23 09:27:59Z witz-itoyo $
 */

/*
 *		ターゲット依存情報の定義
 */
#define TOPPERS_MACRO_ONLY
#define UINT_C(val)			(val)		/* 符号無し整数型の定数を作るマクロ */
#define CAST(type, val)		(val)		/* 型キャストを行うマクロ */

#include "kernel_impl.h"
#include "offset.h"

/*
 *  全割込み禁止マクロ
 */
.macro LOCK_ALL_INT sc_reg1, sc_reg2
	rdctl \sc_reg1, status
	movi  \sc_reg2, ~STATUS_PIE
	and   \sc_reg1, \sc_reg1, \sc_reg2
	wrctl status, \sc_reg1
.endm

/*
 *  全割込み禁止解除マクロ
 */
.macro UNLOCK_ALL_INT sc_reg
	rdctl \sc_reg, status
	ori   \sc_reg, \sc_reg, %lo(STATUS_PIE)
	wrctl status, \sc_reg
.endm

/*
 *  OS割込み禁止マクロ
 *  ネストの一番外側で呼び出されることを想定している
 */
.macro OUTER_LOCK_OS_INT sc_reg1, sc_reg2
	rdctl \sc_reg1, status
	movi  \sc_reg2, ~STATUS_IL
	and   \sc_reg1, \sc_reg1, \sc_reg2
	ldw   \sc_reg2, %gprel(tmin_status_il)(gp)
	or    \sc_reg1, \sc_reg1, \sc_reg2
	wrctl status, \sc_reg1
	movi  \sc_reg1, 1
	stb   \sc_reg1, %gprel(nested_lock_os_int_cnt)(gp)
	stw   zero, %gprel(saved_status_il)(gp)
.endm

/*
 *  OS割込み禁止解除マクロ
 *  ネストの一番外側で呼び出されることを想定している
 */
.macro OUTER_UNLOCK_OS_INT sc_reg1, sc_reg2
	stb   zero, %gprel(nested_lock_os_int_cnt)(gp)
	rdctl \sc_reg1, status
	movi  \sc_reg2, ~STATUS_IL
	and   \sc_reg1, \sc_reg1, \sc_reg2
	wrctl status, \sc_reg1
.endm

/*
 *  OS割込み禁止マクロ
 */
.macro NESTED_LOCK_OS_INT sc_reg1, sc_reg2, sc_reg3
	ldbu  \sc_reg1, %gprel(nested_lock_os_int_cnt)(gp)
	bne   \sc_reg1, zero, 1f

	rdctl \sc_reg2, status
	andi  \sc_reg3, \sc_reg2, STATUS_IL
	stw   \sc_reg3, %gprel(saved_status_il)(gp)

	movi  \sc_reg3, ~STATUS_IL
	and   \sc_reg3, \sc_reg2, \sc_reg3
	ldw   \sc_reg2, %gprel(tmin_status_il)(gp)
	or    \sc_reg3, \sc_reg3, \sc_reg2
	wrctl status, \sc_reg3

1:
	addi  \sc_reg1, \sc_reg1, 1
	stb   \sc_reg1, %gprel(nested_lock_os_int_cnt)(gp)
.endm

/*
 *  OS割込み禁止解除マクロ
 */
.macro NESTED_UNLOCK_OS_INT sc_reg1, sc_reg2
	ldbu  \sc_reg1, %gprel(nested_lock_os_int_cnt)(gp)
	subi  \sc_reg1, \sc_reg1, 1
	stb   \sc_reg1, %gprel(nested_lock_os_int_cnt)(gp)

	bne   \sc_reg1, zero, 1f

	rdctl \sc_reg1, status
	movi  \sc_reg2, ~STATUS_IL
	and   \sc_reg1, \sc_reg1, \sc_reg2
	ldw   \sc_reg2, %gprel(saved_status_il)(gp)
	or    \sc_reg1, \sc_reg1, \sc_reg2
	wrctl status, \sc_reg1
1:
.endm

 /*
  *  スタック操作 : プッシュ
  */
.macro PUSH reg
	addi  sp, sp, -4
	stw   \reg, 0(sp)
.endm

 /*
  *  スタック操作 : ポップ
  */
.macro POP reg
	ldw   \reg, 0(sp)
	addi  sp, sp, 4
.endm

/*
 *  例外エントリ
 *  プロセッサの状態は，
 *    ・status.PIEがクリアされ全割り込み禁止
 *    ・status.ILは例外発生前の値
 *    ・estatusに例外発生前のstatusの内容が入っている
 */
	.set noat
	.section .exceptions, "xa"
	.global exception_entry
	.balign  FUNCTION_ALIGN_SIZE
exception_entry:

	/*
	 *  Trapかを判定
	 */
check_trap:
	ldw   et, -4(ea)               /* 例外を出した命令を取得 */
	xorhi et, et, 0x003b           /*  上位16bit             */
	xori  et, et, 0x683a           /*  下位16bit             */
	bne   et, zero, exception   /* trap命令のバイナリ 0x003b683a */
	jmpi  trap_handler

	/*
	 *  例外エントリ
	 */
exception:
	ldw   et, %gprel(except_nest_cnt)(gp)  /* ネスト回数のチェック */
	bne   zero, et, 1f

	/*
	 *  C1ISR実行状態で発生した場合はスタック切り替えしない
	 */
	rdctl   et,   status
	andi    et,   et, STATUS_IL
	stw     at,   %gprel(exception_temporary1)(gp)
	ldw     at,   %gprel(tmin_status_il)(gp)
	/*
	 *  at < etの場合，C1ISR実行状態で発生した例外となり，etに１を入れる
	 */
	cmpltu  et,   at, et
	ldw     at,   %gprel(exception_temporary1)(gp)
	bne     zero, et, 1f

	/*
	 *  スタック切り替え
	 *  切り替え先スタックポインタをetに入れる
	 */
	movia et, ostkpt
	ldw   et, 0(et)
	br    2f

1:
	mov   et, sp

2:
	/*
	 *  コンテキストの保存
	 */
	addi  et, et, -76    /* レジスタの保存 */
	stw   at,   0(et)
	stw   r2,   4(et)
	stw   r3,   8(et)
	stw   r4,  12(et)
	stw   r5,  16(et)
	stw   r6,  20(et)
	stw   r7,  24(et)
	stw   r8,  28(et)
	stw   r9,  32(et)
	stw   r10, 36(et)
	stw   r11, 40(et)
	stw   r12, 44(et)
	stw   r13, 48(et)
	stw   r14, 52(et)
	stw   r15, 56(et)
	stw   ra,  60(et)
	stw   ea,  64(et)
	stw   sp,  72(et)
	mov   sp,  et
	rdctl et,  estatus
	stw   et,  68(sp)

	/*
	 *  プロテクションフックはOS処理レベルで実行するためOS割込み禁止状態にする
	 *  OS割込み禁止状態で例外が発生する可能性があるため，ネスト管理を行う
	 *  C1ISR実行時に発生した場合は，優先度マスクがOS割込み禁止状態より高いため，
	 *  status.ILは変更しない
	 *  例外は次の条件で発生する
	 *   ・C1ISR実行状態で発生した場合
	 *   ・C1ISR実行状態以外で発生した場合
	 *    ・OS割込み解除状態で発生した場合
	 *    ・OS割込み禁止状態で発生した場合
	 */

	/*
	 *  C1ISR実行状態で発生した場合は何も行わない
	 *  OS割込み禁止状態のILの値よりstatus.ILが高優先度ならC1ISRで例外が発生
	 */
	rdctl r4,status
	andi  r7, r4, STATUS_IL
	ldw   r2, %gprel(tmin_status_il)(gp)
	bltu  r2, r7, exception_1

	/*
	 *  C1ISR実行状態以外で発生した場合
	 */
	ldbu  r3, %gprel(nested_lock_os_int_cnt)(gp)
	addi  r1, r3, 1
	stb   r1, %gprel(nested_lock_os_int_cnt)(gp)

	/*
	 *  OS割込み禁止状態で発生した場合はstatusを変更しない
	 */
	bne   zero, r3, exception_1

	/*
	 *  OS割込み解除状態で発生した場合
	 *  OS割込み禁止状態とする
	 *  例外の出口のリターン(eret)によりstatus.ILは例外前に戻るため，
	 *  saved_status_ilには保存しない
	 */
	movi  r3, ~STATUS_IL
	and   r3, r4, r3
	or    r3, r3, r2
	wrctl status, r3

exception_1:
	/*
	 *  CPU例外番号, badaddr
	 *  割込み発生時に変更されるので割込み禁止解除前に取得
	 */
	rdctl r5, exception
	rdctl r8, badaddr

	/*
	 *  例外発生時に全割込み禁止状態でなければ全割込み禁止解除状態とする
	 */
	rdctl r3, estatus
	andi  r3, r3, STATUS_PIE
	beq   r3, zero, exception_2

	UNLOCK_ALL_INT r1

exception_2:
	/*
	 *  カーネル起動していない場合に起きたCPU例外は，無限ループへ
	 */
	ldb   r3, %gprel(kerflg)(gp)  /* kerflgを取得FALSEなら無限ループ */
	beq   zero, r3, infinity_loop

	/*
	 *  割込み・例外のネスト回数のインクリメント
	 */
	ldw   r3, %gprel(except_nest_cnt)(gp)
	addi  r3, r3, 1
	stw   r3, %gprel(except_nest_cnt)(gp)

#ifdef CFG_USE_PROTECTIONHOOK
	/*
	 *  CPU例外がネストして発生した場合に，CPU例外情報を正しく取得できるように
	 *  するため，プロテクションフック実行前にCPU例外情報を退避し，
	 *  プロテクションフックを実行後，退避したCPU例外情報を復帰する．
	 *  CPU例外情報を保存復帰しない場合，CPU例外処理中(※1)にC1ISRが発生し，
	 *  そのC1ISR実行中にCPU例外が発生したとき(※2)，(※2)のCPU例外処理により，
	 *  (※1)のCPU例外情報が上書きされ，(※1)のCPU例外処理に戻ったとき，
	 *  (※1)のプロテクションフックから観測されるCPU例外情報が，(※2)のものと
	 *  なり，正しくない情報が取得されてしまう．
	 */
	/* nios2_cpu_exp_*を退避するスペースを確保 */
	addi  sp, sp, -16

	/* nios2_cpu_exp_noを退避 */
	movia r4, nios2_cpu_exp_no
	ldw   r6, 0(r4)
	stw   r6, 12(sp)

	/*
	 *  exception / 4が例外番号
	 */
	andi  r5, r5, ((1 << 7) - 1)
	srli  r5, r5, 2
	stw   r5, 0(r4)

	/* nios2_cpu_exp_pcを退避 */
	movia r4, nios2_cpu_exp_pc
	ldw   r6, 0(r4)
	stw   r6, 8(sp)

	/*
	 *  例外発生時のPC - 4を取得
	 */
	ldw   r6, 64+16(sp)
	addi  r6, r6, -4
	stw   r6, 0(r4)

	/* nios2_cpu_exp_bad_addrを退避 */
	movia r4, nios2_cpu_exp_bad_addr
	ldw   r6, 0(r4)
	stw   r6, 4(sp)

	stw   r8, 0(r4)

	/* nios2_cpu_exp_bad_spを退避 */
	movia r4, nios2_cpu_exp_sp
	ldw   r6, 0(r4)
	stw   r6, 0(sp)

	addi  r6, sp, 16
	stw   r6, 0(r4)
#else /* CFG_USE_PROTECTIONHOOK */
	/*
	 *  exception / 4が例外番号
	 */
	andi  r5, r5, ((1 << 7) - 1)
	srli  r5, r5, 2
#endif /* CFG_USE_PROTECTIONHOOK */

	movi  r4, E_OS_PROTECTION_EXCEPTION	/* [0..17] より大きい例外要因が渡って来た場合の処理 */
	movi  r3, TNUM_EXCH					/* 例外要因が[0..17] より大きい場合はテーブル参照しない */
	bgeu  r5, r3, call_protectionhook

	movia r4, exc_code_tbl				/* 例外コード変換テーブル取得 */
	add   r4, r4, r5
	ldb   r4, 0(r4)						/* OSエラーコード取得 */

call_protectionhook:

	rdctl r3, estatus			/* プロテクションフック呼び出し時のステータス取得 */
	andi  r3, r3, STATUS_U
	cmpnei r3, r3, STATUS_U
	stb   r3, %gprel(pre_protection_supervised)(gp)
	call  call_protectionhk_main		/* プロテクションフックの呼び出し */


#ifdef CFG_USE_PROTECTIONHOOK
	/* nios2_cpu_exp_noを復帰 */
	movia r4, nios2_cpu_exp_no
	ldw   r6, 12(sp)
	stw   r6, 0(r4)

	/* nios2_cpu_exp_pcを復帰 */
	movia r4, nios2_cpu_exp_pc
	ldw   r6, 8(sp)
	stw   r6, 0(r4)

	/* nios2_cpu_exp_bad_addrを復帰 */
	movia r4, nios2_cpu_exp_bad_addr
	ldw   r6, 4(sp)
	stw   r6, 0(r4)

	/* nios2_cpu_exp_bad_spを復帰 */
	movia r4, nios2_cpu_exp_sp
	ldw   r6, 0(sp)
	stw   r6, 0(r4)

	/* nios2_cpu_exp_*を退避したスペースを戻す */
	addi  sp, sp, 16
#endif /* CFG_USE_PROTECTIONHOOK */

ret_exc:
	/*
	 *  プロテクションフックはOS割込み禁止状態で実行する
	 *  ret_int_1 の eret で esatus -> status で解除される
	 */

	/*
	 *  割込み・例外のネスト回数のデクリメント
	 */
	ldw   r3, %gprel(except_nest_cnt)(gp)
	addi  r3, r3, -1
	stw   r3, %gprel(except_nest_cnt)(gp)

	/*
	 *  OS割込み禁止状態の解除
	 */
	/*
	 *  C1ISR実行状態で発生した場合はOS割込み禁止状態の解除をしない
	 *  OS割込み禁止状態のILの値よりstatus.ILが高優先度ならC1ISRで発生した例外
	 *  r4のstatusを保持してret_exc_2でも使用
	 */
	rdctl r4,status
	andi  r5, r4, STATUS_IL
	ldw   r6, %gprel(tmin_status_il)(gp)
	bltu  r6, r5, ret_exc_2

	/*
	 *  C1ISR実行状態以外で発生した場合，OS割込み禁止状態の解除
	 *  例外からのリターンによりstatus.ILは例外前に戻るため，status.ILの更新はしない
	 */
	ldbu  r3, %gprel(nested_lock_os_int_cnt)(gp)
	addi  r3, r3, -1
	stb   r3, %gprel(nested_lock_os_int_cnt)(gp)

	/*
	 *  例外発生元へ復帰
	 *  ret_int_1 は全割込み禁止状態で呼び出す必要がある
	 */
ret_exc_2:
	andi  r4, r4, STATUS_PIE		/* statusのPIEだけを残す */
	ldw   r1, 68(sp)				/* CPU例外発生時のstatus(estatus) */
	movi  r2, ~STATUS_PIE
	and   r1, r1, r2				/* etatusのPIEをクリア */
	or    r1, r1, r4				/* estatusのPIEをstatusのPIEにする */
	stw   r1, 68(sp)

	LOCK_ALL_INT  r1, r2
	br ret_int_1

	/*
	 *  カーネル起動していない場合に起きたCPU例外の無限ループ
	 */
infinity_loop:
	br infinity_loop

	/*
	 *  割り込みエントリ
	 *  ベクターテーブルから Os_Lcfg.c に生成される各割込みの割込みエントリ
	 *  からジャンプし，etに割込み番号が保存された状態で実行される
	 *  プロセッサの状態は，
	 *    ・status.PIEがクリアされ全割込み禁止
	 *    ・status.ILに受け付けた割込みの割込み優先度がセット
	 *    ・estatusに割込み前のstatusの内容が入っている
	 */
	.global interrupt
	.balign  FUNCTION_ALIGN_SIZE
interrupt:
	addi  ea, ea, -4     /* 戻り番地をデクリメント */

	/*
	 *  特権モードの場合スタック切替えなし
	 */
	stw   et, %gprel(exception_temporary1)(gp)

	rdctl et, estatus
	andi  et, et, STATUS_U
	beq   et, zero, 1f

	/*
	 *  非特権モードで動いていた場合はシステムスタックに切り替える
	 */
	stw   at, %gprel(exception_temporary2)(gp)

	ldw   et, %gprel(p_runtsk)(gp)	/* et に TCBを確保 */
	ldw   et, TCB_p_tinib(et)		/* et に タスク初期化ブロック */
	ldw   at, TINIB_sstk(et)		/* at に システムスタック */
	ldw   et, TINIB_sstksz(et)		/* et に システムスタックサイズ */
	add   et, at, et

	ldw   at, %gprel(exception_temporary2)(gp)
	br    2f

1:
	mov   et, sp

2:
	/* etには切り替えるspが入っている */
	addi  et, et, -76    /* レジスタの保存 */
	stw   at,   0(et)
	stw   r2,   4(et)
	stw   r3,   8(et)
	stw   r4,  12(et)
	stw   r5,  16(et)
	stw   r6,  20(et)
	stw   r7,  24(et)
	stw   r8,  28(et)
	stw   r9,  32(et)
	stw   r10, 36(et)
	stw   r11, 40(et)
	stw   r12, 44(et)
	stw   r13, 48(et)
	stw   r14, 52(et)
	stw   r15, 56(et)
	stw   ra,  60(et)
	stw   ea,  64(et)
	ldw   r4, %gprel(exception_temporary1)(gp)        /* 割込み要因をr4に移動 */
	stw   sp,  72(et)
	mov   sp,  et
	rdctl et, estatus
	stw   et,  68(sp)

	/*
	 *  多重割込みか判定
	 */
	ldw   r3, %gprel(except_nest_cnt)(gp)

	/*
	 *  割込み・例外のネスト回数のインクリメント
	 */
	addi  r5, r3, 1
	stw   r5, %gprel(except_nest_cnt)(gp)

	bltu  zero, r3,  interrupt_1		/* ネストしている場合はスタック切替無し */

	/*
	 *  タスクに割込んだ場合
	 *  スタックポインタの入れ替え
	 */
	movia r2, ostkpt
	ldw   r2, 0(r2)
	addi  r2, r2, -4
	stw   sp, 0(r2)     /* スタックポインタの保存     */
	mov   sp, r2        /* スタックポインタの入れ替え */

	/*
	 *  タイミング保護タイマ停止
	 */
	ldw   r5,  %gprel(is_tp_timer_running)(gp)
	beq   r5, zero, interrupt_1

	PUSH  r3
	PUSH  r4

	call   tp_stop_task_monitor

	POP  r4
	POP  r3

interrupt_1:
	/*
	 *  p_runisr 割り出し
	 */
	movia r2, isr_p_isrcb_tbl
	slli  r10, r4, 2                 /* 割込み番号を4倍してオフセットを生成  */
	add   r2, r2, r10
	ldw   r9,0(r2)

	addi r6, r9, ISRCB_tpacb	/* p_runisr->tpacb */
	ldw r7, ISRCB_p_isrinib(r9)		/* p_runisr->p_intinib */
	ldw r4, ISRINIB_timeframe_count(r7)	/* p_intinib->timeframe.count */
	ldw r5, ISRINIB_timeframe_tick(r7)	/* p_intinib->timeframe.tick */

	movi r7, ARRIVAL_C2ISR			/* context */

	or r1, r4, r5
	beq  zero, r1, 1f

	PUSH  r3
	PUSH  r9
	PUSH  r10

	call  tp_check_arrival_time

	POP  r10
	POP  r9
	POP  r3
1:
	UNLOCK_ALL_INT r1

	/*
	 *  タスクスタックのオーバフローチェック
	 */
#ifdef CFG_USE_STACKMONITORING
	/*
	 *  割込み番号を退避
	 */

	/*
	 *  多重割込みの場合はタスクスタックのスタックモニタリングを行わない
	 */
	bltu  zero, r3,  int_nested

	/*
	 *  スタックポインタチェック方式
	 */
	mov   r4,   zero								/* プロテクションフック実行時にスタック切り替えをしない */
	ldw   r7,   (sp)								/* 保存したタスクスタックポインタを取得 */
	ldw   r6,   %gprel(p_runtsk)(gp)				/* 管理ブロックの先頭アドレス取得 */
	ldw   r6,   TCB_p_tinib(r6)						/* タスク初期化ブロック先頭アドレス取得 */
	ldw   r6,   TINIB_sstk(r6)						/* タスクスタック先頭アドレス取得 */
	bgtu  r7,   r6,  stack_monitoring_magic_number	/* スタックポインタ > スタックの先頭の場合OK  */
	call  stack_monitoring_error
	br    stack_monitoring_exit

stack_monitoring_magic_number:
	/*
	 *  マジックナンバーチェック方式
	 */
	ldw   r8,  0(r6)							/* タスクスタックの先頭アドレスからマジック領域の値取得 */
	movia r6,  STACK_MAGIC_NUMBER				/* マジックナンバーの取得 */
	/*
	 *  マジックナンバー未破壊なら，割込み処理に飛ぶ
	 *  タスクに割込んだ場合，割込みスタックをチェックしない
	 */
	beq   r8,  r6,  stack_monitoring_exit
	call  stack_monitoring_error					/* タスクスタックマジックナンバー破壊した場合NG  */
	br    stack_monitoring_exit

	/*
	 *  多重割込みの場合
	 */
int_nested:
	/*
	 *  割込みスタックのオーバフローチェック
	 */
	/*
	 *  スタック残量チェック方式
	 */								/* 割込み番号を復帰 */

	ldw   r7,	ISRCB_p_isrinib(r9)				/* 割込み番号に対応したISRCBのアドレス取得 */
	ldw   r7,	ISRINIB_p_intinib(r7)			/* 割込み番号に対応したISRINIBのアドレス取得 */
	ldw   r7,   INTINIB_remain_stksz(r7)		/* 割込み番号に対応したスタックサイズの初期化アドレス取得 */
	ldw   r6,   %gprel(ostk)(gp)				/* 割込みスタックの先頭アドレス取得 */
	add   r7,   r7,	 r6							/* 先頭アドレス＋ISRの使用するスタックサイズ */
	movi  r4,	1								/* プロテクションフック実行時にスタック切り替えをする */
	bleu  sp,   r7,  stack_monitoring_error		/* SP <= 先頭アドレス＋ISRの使用するスタックサイズ の場合NG  */

	/*
	 *  マジックナンバーチェック方式
	 */
	ldw   r7,   0(r6)							/* マジック領域の値取得 */
	movia r6,   STACK_MAGIC_NUMBER				/* マジックナンバーの取得 */
	bne   r7,   r6, stack_monitoring_error		/* マジックナンバーと比較 */
stack_monitoring_exit:

#endif /* CFG_USE_STACKMONITORING */

	/*
	 *  callevel_stat 保存
	 */
	ldh   r3, %gprel(callevel_stat)(gp)
	PUSH  r3

	/*
	 *  callevel_stat 設定
	 */
	ori   r6, r3, TCL_ISR2
	sth   r6, %gprel(callevel_stat)(gp)

	/*
	 *  run_trusted 保存
	 */
	ldb   r3, %gprel(run_trusted)(gp)
	PUSH  r3

	/*
	 *  run_trusted 設定
	 */
	movi  r3, TRUE
	stb   r3, %gprel(run_trusted)(gp)

	/*
	 *  p_currentosap 保存
	 */
	ldw   r3, %gprel(p_currentosap)(gp)
	PUSH  r3

	/*
	 *  p_runisr 保存
	 */
	ldw   r3, %gprel(p_runisr)(gp)
	PUSH  r3

	/*
	 *  p_runisr 設定
	 */
	stw   r9, %gprel(p_runisr)(gp)

	/*
	 *  割込みハンドラのアドレスを読み込む
	 */
	movia r2, isr_tbl
	add   r2, r2, r10
	ldw   r6, 0(r2)

	/*
	 *  p_currentosap 設定
	 */
	beq   r9, zero, int_handler_call	/* p_runisr が NULL でなければ設定 */

	ldw   r2, ISRCB_p_isrinib(r9)		/* r2 <- ISR 初期化ブロックのアドレス */
	ldw   r2, ISRINIB_p_osapcb(r2)		/* r2 <- ISR が所属するOSアプリケーション */
	stw   r2, %gprel(p_currentosap)(gp)	/* 実行中のISRが所属するOSアプリケーションを設定*/

int_handler_call:
	/*
	 *  割込みハンドラ呼び出し
	 */
	callr r6

	/*
	 *  割込みスタックのオーバフローチェック
	 *  割込みから戻った時，スタックポインタも戻ったはずなので，
	 *  マジックナンバーチェック方式のみ実施
	 */
#ifdef CFG_USE_STACKMONITORING
	/*
	 *  マジックナンバーチェック方式
	 */
	ldw   r6, %gprel(ostk)(gp)				/* 割込みスタックの先頭アドレス取得 */
	ldw   r7, 0(r6)							/* マジック領域の値取得 */
	movia r6, STACK_MAGIC_NUMBER			/* マジックナンバーの取得 */
	movi  r4, 1								/* プロテクションフック実行時にスタック切り替えをする */
	bne   r7, r6, stack_monitoring_error	/* マジックナンバーと比較 */
#endif /* CFG_USE_STACKMONITORING */

	/*
	 *  C2ISRの不正終了チェック
	 */
	call  exit_isr2

	/*
	 *  p_runisr の復帰
	 */
	POP   r3
	stw   r3, %gprel(p_runisr)(gp)

	/*
	 *  p_currentosap の復帰
	 */
	POP   r3
	stw   r3, %gprel(p_currentosap)(gp)

	/*
	 *  run_trusted の復帰
	 */
	POP   r3
	stb   r3, %gprel(run_trusted)(gp)

	/*
	 *  callevel_stat の復帰
	 */
	POP   r3
	sth   r3, %gprel(callevel_stat)(gp)

/*
 *  割込みハンドラ出口処理
 */
ret_int:
	/*
	 *  except_nest_cntの操作とスタック切り替えの間はOS割込み禁止にする
	 *  必要がある
	 *  この先，割込み先へのリターンか，遅延ディスパッチに分岐する
	 *  割込み先へのリターンには全割込みが必要であり，遅延ディスパッチ時には
	 *  ネスト管理のOS割込み禁止にする必要があるため，ここでは一旦全割込み禁
	 *  止とする
	 */
	LOCK_ALL_INT  r1, r2

	/*
	 *  割込み・例外のネスト回数のデクリメント
	 */
	ldw   r3, %gprel(except_nest_cnt)(gp)
	addi  r3, r3, -1
	stw   r3, %gprel(except_nest_cnt)(gp)

	/*
	 *  ネスト回数が0なら，割込み元がタスク
	 */
	beq  zero, r3, ret_int_task

	/*
	 *  C2ISR割込みからのリターン時は割込み元が
	 *  C2ISRであるためそのままリターン
	 */
	br    ret_int_1

	/*
	 *  割込み元がタスクの場合
	 */
ret_int_task:
	ldw   r2, 0(sp)              /* スタックポインタを戻す    */
	mov   sp, r2

	/*
	 *  戻り先がタスクの場合，ディスパッチの必要があるかチェック
	 */
	ldw   r4, %gprel(p_runtsk)(gp)
	ldw   r5, %gprel(p_schedtsk)(gp)
	ldb   r6, %gprel(context_discard_flg)(gp)
	/*
	 *  実行中タスクなし(C2ISR実行中のタスク強制終了)の場合はディスパッ
	 *  チャに行く
	 */
	beq   r4, zero, ret_int_task_1
	bne   r6, zero, ret_int_task_1 /* OSAP強制終了処理にて，ディスパッチの必要がある場合，ディスパッチャに行く */

	PUSH  r4

	/*
	 *  タイミング保護タイマ設定
	 */
	ldw r4, TCB_remaining_execution(r4)	/* p_runtsk->remaining_execution */
	beq   r4, zero, 1f

	PUSH  r5

	call  tp_start_timer

	POP r5

1:
	POP r4

	beq   r4, r5, ret_int_1      /* ディスパッチの必要がなければ割込み元へ戻る */

	/*
	 *  ここでは，戻り先がタスクであり，スタックは，タスクスタックに
	 *  スクラッチレジスタのみが保存された状態になっている．また，
	 *  全割込みを禁止した状態となっている
	 */

ret_int_task_1:
	/*
	 *  OS割込み禁止状態に移行する
	 *
	 *  この時点でOS割込み禁止状態とするのは，dispatcherを実行する際に
	 *  OS割込み禁止状態になっている必要があるためである
	 *
	 *  (モデル上の)割込み優先度マスクは0であるため，C1ISR以外の割込みは全て禁止する
	 */
	OUTER_LOCK_OS_INT  r1, r2
	/* ret_int で全割込み禁止状態としたため解除 */
	UNLOCK_ALL_INT r1

	/*
	 *  r4にはp_runtskが入っている
	 */
	beq   r4, zero, dispatcher_0
	bne   r6, zero, ret_int_task_2       /* p_runtaskが所属するOSAPを強制終了する場合はコンテキストを保存せず，ディスパッチャに行く */

	/*
	 *  p_currentosap 保存
	 */
	ldw   r3, %gprel(p_currentosap)(gp)
	stw   r3,  TCB_p_lastosapcb(r4)         /* p_currentosapをTCBに保存 */

	addi  sp,   sp, -36                /* 残りのレジスタを保存 */
	stw   r16,  0(sp)
	stw   r17,  4(sp)
	stw   r18,  8(sp)
	stw   r19, 12(sp)
	stw   r20, 16(sp)
	stw   r21, 20(sp)
	stw   r22, 24(sp)
	stw   r23, 28(sp)
	stw   fp,  32(sp)
	stw   sp,  TCB_sp(r4)         /* タスクスタックをTCBに保存 */
	movia r2,  ret_int_r          /* 実行再開番地をTCBに保存   */
	stw   r2,  TCB_pc(r4)
	br    dispatcher

ret_int_task_2:
	stb   zero, %gprel(context_discard_flg)(gp)
	br    dispatcher_0

	/*
	 *  割込み・例外でコンテキスト保存した場合の復帰ルーチン
	 */
ret_int_r:
	ldw   r16,  0(sp)       /* レジスタを復帰 */
	ldw   r17,  4(sp)
	ldw   r18,  8(sp)
	ldw   r19, 12(sp)
	ldw   r20, 16(sp)
	ldw   r21, 20(sp)
	ldw   r22, 24(sp)
	ldw   r23, 28(sp)
	ldw   fp,  32(sp)
	addi  sp, sp, 36

	/*
	 *  プリタスクフックでDisableAllInterrupts()をした場合は，
	 *  estatusにSTATUS_PIEを反映させる
	 */
	rdctl r2, status
	andi  r2, r2, STATUS_PIE
	ldw   r1, 68(sp)
	movi  r3, %lo(~STATUS_PIE)
	and   r1, r1, r3
	or    r1, r1, r2
	stw   r1, 68(sp)

	/*
	 *  esatus を使うため全割込み禁止とする
	 */
	LOCK_ALL_INT  r1, r2

	OUTER_UNLOCK_OS_INT  r1, r2

	/*
	 *  割込みからの復帰ルーチン
	 *  estatusを用いるため全割込み禁止状態で呼び出すこと．
	 *  全割込み禁止状態は，eretにより解除される
	 *  eret で estatus -> status となるため，ILは元の値，PIEは割り込み許可
	 *  に設定される
	 */
ret_int_1:
	/*
	 *  レジスタを復帰
	 */
	ldw   at,   0(sp)
	ldw   r2,   4(sp)
	ldw   r3,   8(sp)
	ldw   r4,  12(sp)
	ldw   r5,  16(sp)
	ldw   r6,  20(sp)
	ldw   r7,  24(sp)
	ldw   r8,  28(sp)
	ldw   r9,  32(sp)
	ldw   r10, 36(sp)
	ldw   r11, 40(sp)
	ldw   r12, 44(sp)
	ldw   r13, 48(sp)
	ldw   r14, 52(sp)
	ldw   r15, 56(sp)
	ldw   ra,  60(sp)
	ldw   ea,  64(sp)
	ldw   et,  68(sp)
	wrctl estatus, et
	ldw   sp,  72(sp)
	eret

/*
 *  システムサービス呼出のソフトウェア割込みのための
 *  Trap Handler
 */
	/*
	 *  Trapハンドラ
	 *   SC3よりシステムサービスのエントリルーチンとして機能する
	 *
	 *  入力：
	 *   r2：機能コード
	 *   r4,r5,r6,r7 ： システムサービスのパラメータ
	 *     ＊ Nios2 GCC の場合,パラメータが5つ以上の場合はスタック渡しとなるが
	 *       AUTOSAR のシステムサービスのパラメータは最大3 のため，問題なし
	 *
	 *  出力：
	 *   r2：システムサービスの返戻値
	 *   Nios2GCCの場合は，r3も関数の返戻値であるが，AUTOSAR システムサービスの
	 *   返戻値が32bit以下のためr2のみが使用される
	 *
	 *  機能：
	 *   1．呼出元コンテキストの保存（リターン時に使用）
	 *
	 *   2．スタックの切替（ユーザスタック⇒システムスタック）
	 *     呼出元が特権モードの場合は切替えない
	 *      ⇒ すでにシステムスタックか割込みスタックを使用しているため
	 *        切り替え不要
	 *
	 *   3．カーネルサービスの特定
	 *     r2に格納されている機能コードを利用して，システムサービステーブルから
	 *     呼出しを行うシステムサービスの先頭アドレスを取得する
	 *
	 *   4．システムサービス本体の呼出
	 *     (1) 戻り番地の設定
	 *     (2) trap呼出し時の全割り込み禁止状態にする
	 *     (3) システムサービス本体（関数）の呼出
	 *    ※ 呼出とstatusレジスタの同時書換のためeretを使用
	 *
	 *   5．サービスルーチン後処理
	 *     (1) 呼出元コンテキストの復帰
	 *     (2) eretによる終了（リターン）
	 *
	 */
trap_handler:

	rdctl et, estatus			/* trap発行時のステータス取得 */
	andi  et, et, STATUS_U		/* trap発行元のCPU動作モードチェック */
	beq   et, zero, 1f			/* 呼出し元が特権モードの場合はスタック切替無し */

	/*
	 *  タスクのスタック切替
	 *   タスク初期化ブロックのシステムスタックに切替える
	 *    カレントタスクの TCB は p_runtsk そのもの
	 *   切替前のスタックは，例外フレームに格納する
	 */
	ldw   et, %gprel(p_runtsk)(gp)	/* et に TCBを確保 */
	ldw   et, TCB_p_tinib(et)		/* et に タスク初期化ブロック */
	ldw   r8, TINIB_sstk(et)		/* r8 に システムスタック */
	ldw   et, TINIB_sstksz(et)		/* et に システムスタックサイズ */
	add   et, r8, et
	br    2f

1:
	mov   et, sp

2:
	/*
	 *  呼出し元コンテキストのセーブ
	 *
	 *      estatus(呼出し元のstatus)も割込み制御システムサービスで変更
	 *      されるが，動作モードの切替えに影響するのでスタックに保存する
	 *      estatusの格納箇所は切替え後スタックの 0(sp)
	 */
	addi  et, et, -12    /* レジスタの保存 */
	stw   ea,  4(et)
	stw   sp,  8(et)
	mov   sp,  et
	rdctl et, estatus
	stw   et,  0(sp)



	/*
	 *  システムサービス呼出
	 *   カーネルシステムサービス，信頼関数 振分
	 *
	 *  入力：
	 *   r2：機能コード
	 */
	/*
	 *  カーネルシステムサービス処理
	 */
	movi r3, TMAX_SVCID			/* カーネルのシステムサービス番号の最大値 */
	bgt  r2, r3, trp_SVC_err	/* 最大値より大きい場合は不正呼出のためエラー処理 */

	movia r3, svc_table			/* カーネルのシステムサービステーブル取得 */

	/*
	 *  エントリアドレス取得
	 *  r2は後で使うので上書きしない
	 */
	slli r8, r2, 2					/* r8 = r2 + r2 元のr2を４倍 */
	add  r3, r3, r8					/* r3 = r3 + r8 ⇒ svc_table[r2] */
	ldw  ea, 0(r3)					/* 例外からの戻り番地にシステムサービスの先頭番地を設定	*/

	br   trp_SVC_call

	/*
	 *  不正な機能コードを指定して，システムサービス発行時の処理
	 */
trp_SVC_err:
	movia ea, no_support_service

trp_SVC_call:
	/*
	 *  呼出処理：
	 *   入力： r2：機能コード
	 *    ea（例外戻りアドレス）にエントリアドレス設定
	 *    estatusのEUを0（スーパバイザモード）に設定
	 */


	/*
	 *  estatus を U = 0 に設定（PIE は trap 発行もとのまま）
	 */
	rdctl et, estatus
	movi  r15, ~STATUS_U
	and   et, et, r15
	wrctl estatus, et

	/*
	 *  ra に戻り番地を設定（システムサービスの戻り番地に見せかける）
	 *   ra ← trp_SVC_ret
	 */
	movia ra, trp_SVC_ret

trp_SVC:
	eret	/* システムサービス実行 */

trp_SVC_ret:

	/*
	 *  以下の処理は割込み禁止
	 *   status PIE ← 0   （LOCK_ALL_INT にて実施）
	 *
	 *  Suspend*Interrupts/Resume*Interrupts対策として戻ってきた時の
	 *  StatusのPIE を 保存している Statusに反映する
	 */
	rdctl r15, status		/* 呼び先で設定された status PIE を保存 */

							/* 割込み禁止解除は，eret で行う */

	ldw    r13, 0(sp)		/* システムサービス呼出元の Status */

	andi  r15, r15, (STATUS_PIE | STATUS_IL)
	movi  r14, ~(STATUS_PIE | STATUS_IL)
	and   r13, r13 ,r14
	or    r13, r13 , r15

	LOCK_ALL_INT    r14, r15				/* 割込み禁止*/

	wrctl  estatus, r13		/* 割込み状態をセーブした estatusに反映 */

#ifdef PERF_TEST
	ldw   et, %gprel(cache_flash_flag)(gp)	/* キャッシュパージチェック */
	beq   et, zero, trp_cache_flash_no
	stw   zero, %gprel(cache_flash_flag)(gp)
	br    trp_SVC_another_path				/* パージパスへ */
trp_cache_flash_no:
#endif /* PERF_TEST */

	ldw   ea,  4(sp)
	ldw   sp,  8(sp)

	eret	/* エントリ終了 */

	/*
	 *  スタックチェック関数
	 *   入力 r4  使用するスタック量
	 *   使用するスタック量が残っていない場合はエラー
	 */
	.global  trustedfunc_stack_check
	.balign  FUNCTION_ALIGN_SIZE
trustedfunc_stack_check:
	ldh   r1, %gprel(callevel_stat)(gp)
	andi  r2, r1, TCL_ISR2
	beq   r2, zero, trustedfunc_stack_check_1	/* TCL_TASK の場合は jump */

	ldw   r1,   %gprel(ostk)(gp)

	br    trustedfunc_stack_check_2

trustedfunc_stack_check_1:
	ldw   r1, %gprel(p_runtsk)(gp)
	ldw   r1, TCB_p_tinib(r1)
	ldw   r1, TINIB_sstk(r1)		/* sstkを使う */

trustedfunc_stack_check_2:
	add   r4, r4, r1		/* r4 <- r4(size) + r1 (stktop) */
	bleu  sp, r4, trustedfunc_stack_check_3	/* if(sp <= r4) error jump	*/
	movi  r2, E_OK
	ret

trustedfunc_stack_check_3:
	movi  r2, E_OS_STACKINSUFFICIENT
	ret

#ifdef PERF_TEST
	/*
	 *  性能測定用 キャッシュパージ処理
	 *
	 *   性能測定専用のため割込み状態の反映などは削除
	 *   セーブしたコンテキストのみリストアしている
	 */
trp_SVC_another_path:

	ldw   ea,   4(sp)
	ldw   sp,   8(sp)    /* レジスタのリストア */

	movui et, 4096
trp_cache_flash:
	initi et
	addi et, et, -32
	bgt et, zero, trp_cache_flash

trp_SVC_another_path00:
	eret
#endif /* PERF_TEST */

/*
 *  タスクディスパッチ
 */
	.global dispatch
	.balign  FUNCTION_ALIGN_SIZE
dispatch:
	/*
	 *  p_currentosap 保存
	 */
	ldw   r6,  %gprel(p_runtsk)(gp) /* r6 <- runtsk              */
	ldw   r3, %gprel(p_currentosap)(gp)
	stw   r3,  TCB_p_lastosapcb(r6)         /* p_currentosapをTCBに保存 */

	/*
	 *  このルーチンは，タスクコンテキスト・OS割込み禁止状態・ディスパッチ
	 *  許可状態・（モデル上の）割込み優先度マスク全解除状態で呼び出さ
	 *  れる
	 */
	addi  sp, sp, -40                /* レジスタを保存 */
	stw   r16,  0(sp)
	stw   r17,  4(sp)
	stw   r18,  8(sp)
	stw   r19, 12(sp)
	stw   r20, 16(sp)
	stw   r21, 20(sp)
	stw   r22, 24(sp)
	stw   r23, 28(sp)
	stw   fp,  32(sp)
	stw   ra,  36(sp)
	ldw   r6,  %gprel(p_runtsk)(gp) /* r4 <- runtsk              */
	stw   sp,  TCB_sp(r6)           /* タスクスタックをTCBに保存 */
	movia r5,  dispatch_r			/* 実行開始番地を保存        */
	stw   r5,  TCB_pc(r6)           /* 実行再開番地をTCBに保存   */

#ifdef CFG_USE_STACKMONITORING
	/*
	 *  実行中タスクのタスクスタックのオーバフローチェック
	 */
	/*
	 *  スタックポインタチェック方式
	 */
	ldw   r6,  TCB_p_tinib(r6)					/* タスク初期化ブロック先頭アドレス取得 */
	ldw   r6,  TINIB_sstk(r6)					/* タスクスタック先頭アドレス取得 */
	movi  r4,  1								/* プロテクションフック実行時にスタック切り替えをする */
	bgtu  sp,  r6,  1f							/* スタックポインタ <= スタックの先頭の場合NG  */
	call  stack_monitoring_error
	br    2f
	/*
	 *  マジックナンバーチェック方式
	 */
1:
	ldw   r7,  0(r6)							/* タスクスタックの先頭アドレスからマジック領域の値取得 */
	movia r6,  STACK_MAGIC_NUMBER				/* マジックナンバーの取得 */
	beq   r7,  r6,  2f							/* マジックナンバーと比較 */
	call  stack_monitoring_error

2:
#endif /* CFG_USE_STACKMONITORING */

	br    dispatcher

dispatch_r:
	ldw   r16,  0(sp)               /* レジスタを復帰 */
	ldw   r17,  4(sp)
	ldw   r18,  8(sp)
	ldw   r19, 12(sp)
	ldw   r20, 16(sp)
	ldw   r21, 20(sp)
	ldw   r22, 24(sp)
	ldw   r23, 28(sp)
	ldw   fp,  32(sp)
	ldw   ra,  36(sp)
	addi  sp, sp, 40
	ret

/*
 *  ディスパッチャの動作開始
 */
	.global start_dispatch
	.balign  FUNCTION_ALIGN_SIZE
start_dispatch:
	/*
	 *  このルーチンは，カーネル起動時に，すべての割込みを禁止した状態
	 *  で呼び出される．また，sp＝非タスクコンテキスト用スタックポイン
	 *  タの状態で呼び出されることを想定している
	 *
	 *  dispatcherは，OS割込み禁止状態で呼び出す
	 */
	/* OS割込み禁止状態 */
	OUTER_LOCK_OS_INT r1, r2
	/* 全割込み禁止解除状態へ */
	UNLOCK_ALL_INT  r1
	br    dispatcher_0

	.global exit_and_dispatch_nohook
	.balign  FUNCTION_ALIGN_SIZE

	/*
	 *  タスク強制終了の場合はPostTaskHookを呼出さない
	 *  その場合のエントリである
	 */
exit_and_dispatch_nohook:
	br    dispatcher_0

	/*
	 *  現在のコンテキストを捨ててディスパッチ
	 */
	.global exit_and_dispatch
	.balign  FUNCTION_ALIGN_SIZE
exit_and_dispatch:
#ifdef CFG_USE_STACKMONITORING
	/*
	 *  実行中タスクのタスクスタックのオーバフローチェック
	 */
	/*
	 *  スタックポインタチェック方式
	 */
	ldw   r6,  %gprel(p_runtsk)(gp)				/* 管理ブロックの先頭アドレス取得 */
	beq   r6,  zero, dispatcher_0
	ldw   r6,  TCB_p_tinib(r6)					/* タスク初期化ブロック先頭アドレス取得 */
	ldw   r6,  TINIB_sstk(r6)					/* タスクスタック先頭アドレス取得 */
	movi  r4,  1								/* プロテクションフック実行時にスタック切り替えをする */
	bgtu  sp,  r6,  1f							/* スタックポインタ <= スタックの先頭の場合NG  */
	call  stack_monitoring_error
	br    2f
	/*
	 *  マジックナンバーチェック方式
	 */
1:
	ldw   r7,  0(r6)							/* タスクスタックの先頭アドレスからマジック領域の値取得 */
	movia r6,  STACK_MAGIC_NUMBER				/* マジックナンバーの取得 */
	beq   r7,  r6,  2f							/* マジックナンバーと比較 */
	call  stack_monitoring_error

2:
#endif /* CFG_USE_STACKMONITORING */
	/* ディスパッチャ本体（dispatcher）へ */

dispatcher:
	ldw   r6,  %gprel(p_runtsk)(gp)				/* 管理ブロックの先頭アドレス取得 */
	beq   r6,  zero, dispatcher_0

	/*
	 *  PostTaskHookの呼び出し
	 */
#ifdef CFG_USE_POSTTASKHOOK
posttaskhook_call:
	movia r4, call_posttaskhook
	call  stack_change_and_call_func_1
#endif /* CFG_USE_POSTTASKHOOK */

tp_stop:
	/*
	 *  タイミング保護タイマ停止
	 */
	/*
	 *  is_tp_timer_runningがFALSEなら
	 *  tp_stop_task_monitorを呼び出さない
	 */
	ldw   r4,  %gprel(is_tp_timer_running)(gp)
	beq   r4, zero, dispatcher_0
	call   tp_stop_task_monitor
dispatcher_0:
	/*
	 *  このルーチンは，タスクコンテキスト・OS割込み禁止状態・ディスパッチ
	 *  許可状態・（モデル上の）割込み優先度マスク全解除状態で呼び出さ
	 *  れる．実行再開番地へもこの状態のまま分岐する
	 */
#ifdef LOG_DSP_ENTER
	ldw   r4,  %gprel(p_runtsk)(gp)
	beq   r4, zero, 1f
	call  log_dsp_enter
1:
#endif /* LOG_DSP_ENTER */

	ldh   r5, %gprel(callevel_stat)(gp)
	andi  r4, r5, TSYS_DISALLINT
	beq   r4, zero, dispatcher_1

	andi  r5, r5, %lo(~TSYS_DISALLINT)
	sth	  r5, %gprel(callevel_stat)(gp)
	UNLOCK_ALL_INT    r2    /* 全割込み禁止解除状態へ */

	/*
	 *  例外（割込み/CPU例外）のネスト回数はstart_dispatchや，dispatcher_3:
	 *  から来た場合は'1'となっているため，タスクスタックに切り換え後，
	 *  '0'に初期化する
	 */

	/*
	 *  SC3ではタスクディスパッチ時にMPU情報も切替える
	 *      Nios2 MPUの場合はオーバーライトする
	 *  対象とする領域は
	 *      ・タスクのスタック領域
	 *      ・OSアプリケーションのプライベート領域
	 *  ただし，OSアプリケーションのプライベート領域は現在のOSアプリケーション
	 *  と切替え先のOSアプリケーションが同じである場合は(情報が同じなので)切替えない
	 *  if(タスクディスパッチする){
	 *       スタック情報の設定
	 *       OSアプリケーション情報の設定
	 *  }
	 */
dispatcher_1:
	ldw   r5,   %gprel(p_schedtsk)(gp) /* r5 <- schedtsk              */
	stw   r5,   %gprel(p_runtsk)(gp)   /* schedtsk を runtskに        */
	beq   r5,   zero, dispatcher_3     /* schedtskがあるか            */
	ldw   sp,   TCB_sp(r5)             /* TCBからタスクスタックを復帰 */
	stw   zero, %gprel(except_nest_cnt)(gp) /* 例外ネスト回数を'0'へ */

	ldw   r3, TCB_p_tinib(r5)		/* r3 <- タスク初期化ブロックのアドレス */
	ldw   r4, TINIB_p_osapcb(r3)	/* r4 <- タスクが所属するOSアプリケーション */

	ldw   r2, OSAPCB_p_osapinib(r4)		/* r6 <- タスクが所属するOSアプリケーションの初期化管理ブロック */
	ldb   r1, OSAPINIB_osap_trusted(r2)	/* r1 <- OSアプリケーションのmode(信頼／非信頼) */

	ldw   r2,  TCB_p_lastosapcb(r5)         /* p_currentosapをTCBから呼び出し */
	stw   r2, %gprel(p_currentosap)(gp)	/* p_currentosap ← 次に実行するタスクのOSアプリケーション */

	ldw   r6, OSAPCB_p_osapinib(r2)		/* r6 <- タスクが所属するOSアプリケーションの初期化管理ブロック */
	ldb   r2, OSAPINIB_osap_trusted(r6)	/* r1 <- OSアプリケーションのmode(信頼／非信頼) */
	stb   r2, %gprel(run_trusted)(gp)
	bne   r1, zero, dispatcher_1_5	/* 信頼OSアプリケーション(mode == 1)の場合 Jump */

	ldw   r2, %gprel(p_ctxstk)(gp)	/* r2 <- 現在MPUに設定されているタスクのTCB */
	beq   r2, r5, dispatcher_1_5	/* p_ctxstk と 次に実行するタスクのTCBが同じならMPU切替え無し */

	stw   r5, %gprel(p_ctxstk)(gp)	/* p_ctxstk <- 次に実行するタスクのTCB */

	ldw   r1, TINIB_mpubase(r3)	/* r1 <- mpubase情報 */
	ldw   r2, TINIB_mpuacc(r3)	/* r2 <- mpuacc情報 */

	wrctl mpubase, r1
	wrctl mpuacc,  r2

	call  prc_set_osap_mpu

dispatcher_1_5:

#ifdef LOG_DSP_LEAVE
	ldw   r4,  %gprel(p_runtsk)(gp)
	call  log_dsp_leave
#endif /* LOG_DSP_LEAVE */

	/*
	 *  タイミング保護タイマ設定
	 */
	ldw   r5, %gprel(p_runtsk)(gp)
	ldw r4, TCB_remaining_execution(r5)	/* p_runtsk->remaining_execution */
	beq   r4, zero, 1f
	call tp_start_timer
1:

#ifdef CFG_USE_PRETASKHOOK
pretaskhook_call:
	movia r4, call_pretaskhook
	call  stack_change_and_call_func_1
#endif /* CFG_USE_PRETASKHOOK */

dispatcher_2:
	ldw   r5, %gprel(p_runtsk)(gp)
	ldw   r5, TCB_pc(r5)			/* TCB から実行再開番地を復帰   */
	jmp   r5

dispatcher_3:
	/*
	 *  OS割込み禁止状態の解除と，非タスクコンテキスト実行状態への
	 *  準備をする
	 *
	 *  ここで非タスクコンテキストに切り替える（sp＝非タスクコンテキス
	 *  ト用スタックポインタ，except_nest_cnt＝1)のは，OS割込み禁止解
	 *  除後に発生する割込み処理にどのスタックを使うかという問題の解決
	 *  と，割込みハンドラ内でのタスクディスパッチの防止という2つの意
	 *  味がある
	 */
	stw   zero, %gprel(p_currentosap)(gp)		/* p_currentosap を NULL にする */
	movia r2, ostkpt
	ldw   r2, 0(r2)
	mov   sp, r2							/* スタックポインタの入れ替え */
	movi  r5, 1								/* except_nest_cnt を1に */
	stw   r5, %gprel(except_nest_cnt)(gp)

	/*
	 *  OS割込み禁止解除状態へ
	 *  割込みを許可し，非タスクコンテキスト実行状態とし割込みを待つ
	 *
	 *  プロセッサを割込み待ちに移行させる処理と，割込み許可とは，不可
	 *  分に行なう必要がある
	 *  これを不可分に行なわない場合，割込みを許可した直後に割込
	 *  みが入り，その中でタスクが実行可能状態になると，実行すべきタス
	 *  クがあるにもかかわらずプロセッサが割込み待ちになってしまう
	 *
	 *  割込み待ちの間は，p_runtskとp_currentosapをNULL（＝0）に設定しなけれ
	 *  ばならない
	 *  このように設定しないと，割込みハンドラからGetTaskID
	 *  とGetApplicationIDを呼び出した際の動作が仕様に合致しなくなる
	 */
	OUTER_UNLOCK_OS_INT r1, r2

	nop
	nop
	nop
	nop

	/*
	 *  OS割込み禁止状態へ
	 *  (モデル上の)割込み優先度マスクは0であるため，C1ISR以外の割込みは全て禁止する
	 *  スタックは非タスクコンテキストのスタックを使用しているため，except_nest_cntは，
	 *  '1'のままとする
	 */
	OUTER_LOCK_OS_INT r1, r2
	br    dispatcher_1

	/*
	 *  スタックオーバフロー時の処理
	 */
#ifdef CFG_USE_STACKMONITORING
	/*
	 *  スタックオーバフロー時プロテクションフックを呼び出し
	 *  スタックを更に壊さないため，割込みスタックの初期値を使用する
	 *  r4 == 1 スタック切り替えをする
	 *  r4 == 0 スタック切り替えをしない
	 */
stack_monitoring_error:
	/*
	 *  r4==0の場合でも，既にostkptを使っているので，
	 *  except_nest_cntをインクリメントしても問題はない
	 */
	ldw   r2, %gprel(except_nest_cnt)(gp)		/* ostkptを使う場合はexcept_nest_cntをインクリメントする */
	addi  r2, r2, 1
	stw   r2, %gprel(except_nest_cnt)(gp)

	beq   r4, zero, stack_monitoring_error_1
	movia r2, ostkpt
	ldw   sp, 0(r2)

stack_monitoring_error_1:
	/*
	 *  プロテクションフックはOS割込み禁止状態で呼び出す
	 */
	NESTED_LOCK_OS_INT r1, r2, r3

	movi  r4, E_OS_STACKFAULT					/* プロテクションフックの引数を設定 */

	movi  r6, TRUE
	stb   r6, %gprel(pre_protection_supervised)(gp)

	jmpi  call_protectionhk_main				/* プロテクションフックを呼び出し */
#endif /* CFG_USE_STACKMONITORING */

/*
 *  タスク開始時処理
 *
 *  dispatcherから呼び出される
 */

	.text
	.global start_stask_r
	.balign  FUNCTION_ALIGN_SIZE
start_stask_r:
	movia ra, exit_task					/* ra(戻り番地） <- exit_task */
	mov   r1, zero						/* 特権モードで動作するよう設定 */

	br    start_r

	.global start_utask_r
	.balign  FUNCTION_ALIGN_SIZE
start_utask_r:
	movia ra, exit_utask				/* ra(戻り番地） <- exit_utask */
	movi  r1, %lo(STATUS_U)				/* 非特権モードで動作するよう設定 */

	/*
	 *  スタックをシステムスタックからユーザスタックに切り替え
	 */
	ldw   r4, %gprel(p_runtsk)(gp)
	ldw   r4, TCB_p_tinib(r4)
	ldw   r5, TINIB_ustk(r4)
	ldw   r4, TINIB_ustksz(r4)
	add   r4, r5, r4
	mov   sp, r4

	br    start_r								/* .balign 対策 0x00000000 で埋められるので jmp */

	.text
	.global start_r
	.balign  FUNCTION_ALIGN_SIZE
start_r:

	/*
	 *  プレタスクフックで割込み禁止されていれば，割込み禁止でタスクを起動させる
	 *   OS割込み禁止解除前に現在優先度(TCB.curpri)を実行優先度(TINIB.exepri)に
	 *   変更(内部リソースを獲得)
	 */
	rdctl r2, status
	andi  r2, r2, STATUS_PIE
	or    r1, r1, r2
	ldw   r4, %gprel(p_runtsk)(gp)
	ldw   r3, TCB_p_tinib(r4)
	ldb   r5, TINIB_exepri(r3)			/* TINIB.exepri -> TCB.curpri */
	stb   r5, TCB_curpri(r4)

	LOCK_ALL_INT  r4, r5

	/*
	 *  OS割込み禁止解除状態へ
	 *  (モデル上の)割込み優先度マスクは0であるため，有効な割込みは全て許可する
	 *  タスクが不正終了した場合は保護処理を行う
	 */
	OUTER_UNLOCK_OS_INT r4, r5

	ldw   ea, TINIB_task(r3)			/* ea <- タスクの実行番地 */
	wrctl estatus, r1					/* estatus <- r1 */
	eret								/* タスクの実行開始 */

	.global stack_change_and_call_func_1
	.global stack_change_and_call_func_2
	.balign  FUNCTION_ALIGN_SIZE
stack_change_and_call_func_1:
stack_change_and_call_func_2:

	PUSH  ra

	ldw   r2, %gprel(except_nest_cnt)(gp)
	addi  r3, r2, 1
	stw   r3, %gprel(except_nest_cnt)(gp)
	bne   zero, r2, 1f

	movia r2, ostkpt
	ldw   r2, 0(r2)
	addi  r2, r2, -4
	stw   sp, 0(r2)     /* スタックポインタの保存     */
	mov   sp, r2        /* スタックポインタの入れ替え */
1:

	mov   r2, r4
	mov   r4, r5
	mov   r5, r6
	callr r2

	ldw   r2, %gprel(except_nest_cnt)(gp)
	subi  r2, r2, 1
	stw   r2, %gprel(except_nest_cnt)(gp)
	bne   zero, r2, 1f

	ldw   sp, 0(sp)     /* スタックポインタの復帰     */
1:
	POP   ra
	ret

	.section .text_kernel
	/*
	 *  割り込みエントリ
	 *  ベクターテーブルから Os_Lcfg.c に生成される各割込みの割込みエントリ
	 *  からジャンプし，etに割込み番号が保存された状態で実行される
	 *  プロセッサの状態は，
	 *    ・status.PIEがクリアされ全割込み禁止
	 *    ・status.ILに受け付けた割込みの割込み優先度がセット
	 *    ・estatusに割込み前のstatusの内容が入っている
	 */
	.global int_tp_fault_handler
	.balign  FUNCTION_ALIGN_SIZE
int_tp_fault_handler:

	/*
	 *  多重割込みか判定
	 */
	ldw   r3, %gprel(except_nest_cnt)(gp)

	/*
	 *  割込み・例外のネスト回数のインクリメント
	 */
	addi  r5, r3, 1
	stw   r5, %gprel(except_nest_cnt)(gp)

	bltu  zero, r3,  1f     /* ネストしている場合はスタック切替無し */

	/*
	 *  タスクに割込んだ場合
	 *  スタックポインタの入れ替え
	 */
	movia r2, ostkpt
	ldw   r2, 0(r2)
	addi  r2, r2, -4
	stw   sp, 0(r2)     /* スタックポインタの保存     */
	mov   sp, r2        /* スタックポインタの入れ替え */

1:
	/* OS割込み禁止状態(割込みレベルを下げる) */
	OUTER_LOCK_OS_INT r1, r2
	
	UNLOCK_ALL_INT r1

	/*
	 *  割込みハンドラ呼び出し
	 */
	jmpi    tp_fault_handler
	/*
	 *  ここには戻ってこない
	 */

	.global int_tp_timer_handler
	.balign  FUNCTION_ALIGN_SIZE
int_tp_timer_handler:
	addi  ea, ea, -4     /* 戻り番地をデクリメント */

	/*
	 *  特権モードの場合スタック切替えなし
	 */
	stw   et, %gprel(exception_temporary1)(gp)

	rdctl et, estatus
	andi  et, et, STATUS_U
	beq   et, zero, 1f

	/*
	 *  非特権モードで動いていた場合はシステムスタックに切り替える
	 */
	stw   at, %gprel(exception_temporary2)(gp)

	ldw   et, %gprel(p_runtsk)(gp)	/* et に TCBを確保 */
	ldw   et, TCB_p_tinib(et)		/* et に タスク初期化ブロック */
	ldw   at, TINIB_sstk(et)		/* at に システムスタック */
	ldw   et, TINIB_sstksz(et)		/* et に システムスタックサイズ */
	add   et, at, et

	ldw   at, %gprel(exception_temporary2)(gp)
	br    2f

1:
	mov   et, sp

2:
	/* etには切り替えるspが入っている */
	addi  et, et, -76    /* レジスタの保存 */
	stw   at,   0(et)
	stw   r2,   4(et)
	stw   r3,   8(et)
	stw   r4,  12(et)
	stw   r5,  16(et)
	stw   r6,  20(et)
	stw   r7,  24(et)
	stw   r8,  28(et)
	stw   r9,  32(et)
	stw   r10, 36(et)
	stw   r11, 40(et)
	stw   r12, 44(et)
	stw   r13, 48(et)
	stw   r14, 52(et)
	stw   r15, 56(et)
	stw   ra,  60(et)
	stw   ea,  64(et)
	ldw   r4, %gprel(exception_temporary1)(gp)        /* 割込み要因をr4に移動 */
	stw   sp,  72(et)
	mov   sp,  et
	rdctl et, estatus
	stw   et,  68(sp)

	/*
	 *  多重割込みか判定
	 */
	ldw   r3, %gprel(except_nest_cnt)(gp)

	/*
	 *  割込み・例外のネスト回数のインクリメント
	 */
	addi  r5, r3, 1
	stw   r5, %gprel(except_nest_cnt)(gp)

	bltu  zero, r3,  int_tp_timer_handler_1		/* ネストしている場合はスタック切替無し */

	/*
	 *  タスクに割込んだ場合
	 *  スタックポインタの入れ替え
	 */
	movia r2, ostkpt
	ldw   r2, 0(r2)
	addi  r2, r2, -4
	stw   sp, 0(r2)     /* スタックポインタの保存     */
	mov   sp, r2        /* スタックポインタの入れ替え */

int_tp_timer_handler_1:
	UNLOCK_ALL_INT r1

	/*
	 *  割込みハンドラ呼び出し
	 */
	call  tp_timer_handler

	/*
	 *  except_nest_cntの操作とスタック切り替えの間はOS割込み禁止にする
	 *  必要がある
	 *  この先，割込み先へのリターンか，遅延ディスパッチに分岐する
	 *  割込み先へのリターンには全割込みが必要であり，遅延ディスパッチ時には
	 *  ネスト管理のOS割込み禁止にする必要があるため，ここでは一旦全割込み禁
	 *  止とする
	 */
	LOCK_ALL_INT  r1, r2

	/*
	 *  割込み・例外のネスト回数のデクリメント
	 */
	ldw   r3, %gprel(except_nest_cnt)(gp)
	addi  r3, r3, -1
	stw   r3, %gprel(except_nest_cnt)(gp)

	/*
	 *  ネスト回数が0なら，割込み元がタスク
	 */
	bne  zero, r3, int_tp_timer_handler_2
	ldw   r2, 0(sp)              /* スタックポインタを戻す    */
	mov   sp, r2
int_tp_timer_handler_2:
	/*
	 *  レジスタを復帰
	 */
	ldw   at,   0(sp)
	ldw   r2,   4(sp)
	ldw   r3,   8(sp)
	ldw   r4,  12(sp)
	ldw   r5,  16(sp)
	ldw   r6,  20(sp)
	ldw   r7,  24(sp)
	ldw   r8,  28(sp)
	ldw   r9,  32(sp)
	ldw   r10, 36(sp)
	ldw   r11, 40(sp)
	ldw   r12, 44(sp)
	ldw   r13, 48(sp)
	ldw   r14, 52(sp)
	ldw   r15, 56(sp)
	ldw   ra,  60(sp)
	ldw   ea,  64(sp)
	ldw   et,  68(sp)
	wrctl estatus, et
	ldw   sp,  72(sp)
	eret

/*
 *  非信頼処理単位（非特権モード）で動作するので，共有コード領域
 *  に配置する（.section .text_shared で配置指示している）
 */
	/*
	 *  非信頼タスクが TerminateTask を呼出さないでタスク関数を終了
	 *  したときに実行するコード（サービスプロテクション対応）
	 *  ソフトウェア割込みにてカーネル本体の exit_task を呼出す
	 */
	.section .text_shared
	.balign  FUNCTION_ALIGN_SIZE
exit_utask:
	movi  r2, TFN_EXIT_TASK
	trap
