#
#  TOPPERS ATK2
#      Toyohashi Open Platform for Embedded Real-Time Systems
#      Automotive Kernel Version 2
#
#  Copyright (C) 2011-2017 by Center for Embedded Computing Systems
#              Graduate School of Information Science, Nagoya Univ., JAPAN
#  Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
#  Copyright (C) 2011-2013 by Spansion LLC, USA
#  Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
#  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
#  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
#  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
#  Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
#  Copyright (C) 2011-2017 by Witz Corporation
#  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
#  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
#  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
#
#  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
#  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
#  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
#  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
#      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
#      スコード中に含まれていること．
#  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
#      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
#      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
#      の無保証規定を掲載すること．
#  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
#      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
#      と．
#    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
#        作権表示，この利用条件および下記の無保証規定を掲載すること．
#    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
#        報告すること．
#  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
#      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
#      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
#      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
#      免責すること．
#
#  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
#  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
#  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
#  用する者に対して，AUTOSARパートナーになることを求めている．
#
#  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
#  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
#  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
#  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
#  の責任を負わない．
#
#  $Id: Makefile.target 727 2017-01-23 09:27:59Z witz-itoyo $
#

#
#		Makefile のターゲット依存部（AT91SKYEYE用）
#

#
#  ボード名，プロセッサ名，開発環境名の定義
#
BOARD = at91skyeye
PRC = arm
TOOL = gcc

#
#  コンパイルオプション
#
INCLUDES := $(INCLUDES) -I$(TARGETDIR)
COPTS := $(COPTS) -mcpu=arm7tdmi -mlittle-endian
LDFLAGS := $(LDFLAGS) -mcpu=arm7tdmi  -N -mlittle-endian

#
#  カーネルに関する定義
#
KERNEL_DIR := $(KERNEL_DIR) $(TARGETDIR)
KERNEL_ASMOBJS := $(KERNEL_ASMOBJS) target_support.o
KERNEL_COBJS := $(KERNEL_COBJS) target_config.o target_hw_counter.o

#
#  システムタイマに関する設定
#
ifeq ($(ENABLE_SYS_TIMER),true)
      CDEFS := $(CDEFS) -DTOPPERS_ENABLE_SYS_TIMER
      KERNEL_COBJS := $(KERNEL_COBJS) target_timer.o
endif

#
#  システムモジュールに関する定義
#
SYSMOD_DIR := $(SYSMOD_DIR)
SYSMOD_COBJS := $(SYSMOD_COBJS) target_serial.o 

#
#  ジェネレータ関係の変数の定義
#
CFG_TABS := $(CFG_TABS) --cfg1-def-table $(TARGETDIR)/target_def.csv

#
#  GNU開発環境のターゲットアーキテクチャの定義
#
GCC_TARGET = arm-none-eabi

#
#  リンカスクリプトの定義
#
LDSCRIPT = $(TARGETDIR)/at91skyeye.ld

#
#  スタートアップモジュールに関する定義
#
#  at91skyeye.ldに「STARTUP(start.o)」を記述したため，スタートアップモジュー
#  ルの名前をHIDDEN_OBJSに定義する．
#
HIDDEN_OBJS = start.o

$(HIDDEN_OBJS): %.o: %.S
	$(CC) -c $(CFLAGS) $(KERNEL_CFLAGS) $<

$(HIDDEN_OBJS:.o=.d): %.d: %.S
	@$(PERL) $(SRCDIR)/utils/makedep -C $(CC) \
		-O "$(CFLAGS) $(KERNEL_CFLAGS)" $< >> Makefile.depend

#
#  依存関係の定義
#
cfg1_out.c: $(TARGETDIR)/target_def.csv
Os_Lcfg.timestamp: $(TARGETDIR)/target.tf
$(OBJFILE): $(TARGETDIR)/target_check.tf
offset.h: $(TARGETDIR)/target_offset.tf

#
#  オフセットファイル生成のための定義
#
OFFSET_TF := $(TARGETDIR)/target_offset.tf

#
#  プロセッサ依存部のインクルード
#
include $(SRCDIR)/arch/$(PRC)_$(TOOL)/common/Makefile.core

#
#  gcovを有効に
#  

#
#  ENABLE_GCOVに設定可能な値
#    full              : 全体取得
#    part              : 一部取得
#    only_kernel_full  : 全体取得(カーネルコードのみ)
#    only_kernel_part  : 一部取得(カーネルコードのみ)
#    false             : 無効
ifdef ENABLE_GCOV
ifneq ($(ENABLE_GCOV),false)
ifeq ($(ENABLE_GCOV),full)
      CDEFS := $(CDEFS) -DTOPPERS_ENABLE_GCOV_FULL
      COPTS := $(COPTS) -fprofile-arcs -ftest-coverage
endif
ifeq ($(ENABLE_GCOV),part)
      CDEFS := $(CDEFS) -DTOPPERS_ENABLE_GCOV_PART
      COPTS := $(COPTS) -fprofile-arcs -ftest-coverage
endif
ifeq ($(ENABLE_GCOV),only_kernel_full)
      CDEFS := $(CDEFS) -DTOPPERS_ENABLE_GCOV_FULL
      KERNEL_CFLAGS := $(KERNEL_CFLAGS) -fprofile-arcs -ftest-coverage
      CFG1_OUT_LDFLAGS := $(CFG1_OUT_LDFLAGS) -fprofile-arcs -ftest-coverage
endif
ifeq ($(ENABLE_GCOV),only_kernel_part)
      CDEFS := $(CDEFS) -DTOPPERS_ENABLE_GCOV_PART
      KERNEL_CFLAGS := $(KERNEL_CFLAGS) -fprofile-arcs -ftest-coverage
      CFG1_OUT_LDFLAGS := $(CFG1_OUT_LDFLAGS) -fprofile-arcs -ftest-coverage
endif
      LIBS := $(LIBS) -lgcov
      CLEAN_FILES := $(CLEAN_FILES) *.gcda *.gcda.bak *.gcno *.gcov coverage.info
lcov:
	rm -f makeoffset.gcno cfg1_out.*
	lcov -c -d . -o coverage.info --gcov-tool $(GCC_TARGET)-gcov.exe
	genhtml coverage.info -o coverage_html
endif
endif

#
#  各種コマンドの実行
#
run: $(OBJFILE)
	skyeye.exe -c $(TARGETDIR)/skyeye.conf -e $(OBJFILE); date

arun: $(OBJFILE)
	@expect -c "set timeout -1; spawn skyeye.exe -c $(TARGETDIR)/skyeye.conf -e $(OBJFILE); expect \"Kernel Exit...\"; send \"\003\""

db: $(OBJFILE)
	cmd /c start skyeye.exe -c $(TARGETDIR)/skyeye.conf -e $(OBJFILE) -d
	arm-gdb.exe $(OBJFILE) -command $(TARGETDIR)/gdb.ini
