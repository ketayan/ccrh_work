/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2006 by GJ Business Division RICOH COMPANY,LTD. JAPAN
 *  Copyright (C) 2007-2017 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2017 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_config.c 727 2017-01-23 09:27:59Z witz-itoyo $
 */

/*
 *		チップ依存モジュール（AT91SKYEYE用）
 */

#include <stdlib.h>
#include "kernel_impl.h"
#include "target_serial.h"
#ifdef ENABLE_RETURN_MAIN
#include "interrupt.h"
#endif /* ENABLE_RETURN_MAIN */
#ifdef TOPPERS_ENABLE_TRACE
#include "target_sysmod.h"
#include "logtrace/trace_config.h"
#endif /* TOPPERS_ENABLE_TRACE */


/*
 * 各割込みの割込み要求禁止フラグの状態
 */
uint32			idf;

/*
 * 現在の割込み優先度マスクの値
 */
PriorityType	ipm;

/*
 *  文字列の出力（バージョンチェック用）
 */
void
target_fput_str(char8 *c)
{
	while (*c != '\0') {
		usart0_putc(*c++);
	}
	usart0_putc('\n');
}

/*
 *  バージョンチェック
 */
static void
version_check(void)
{
	/* SkyEyeのバージョンを確認 */
	if (sil_rew_mem((uint32 *) (SKYEYE_VER_REG)) != SUPPORT_SKYEYE_VER) {
		target_fput_str("SkyEye version is mismatch!!");
		while (1) {
		}
	}

	if ((sil_rew_mem((uint32 *) (DEVM_VER_REG)) & 0xfff0)
		!= (SUPPORT_DEVM_VER & 0xfff0)) {
		target_fput_str("DeviceManager Extension version is mismatch!!");
		while (1) {
		}
	}
}

void
target_hardware_initialize(void)
{
	/*
	 *  バージョンの確認
	 */
	version_check();
}

/*
 *  ターゲット依存の初期化
 */
void
target_initialize(void)
{

	/*
	 *  ARM依存の初期化
	 */
	core_initialize();

	/*
	 * 各割込みの割込み要求禁止フラグ全禁止
	 */
	idf = ~0U;

	/*
	 * 割込み優先度マスクは0
	 */
	ipm = 0U;

	/*
	 * 全ての割込みをマスク
	 */
	at91skyeye_disable_int(~0U);

	/*
	 * 全ての割込み要因をクリア
	 */
	at91skyeye_clear_int(~0U);

#ifdef TOPPERS_ENABLE_TRACE
	/*
	 *  トレースログ機能の初期化
	 */
	trace_initialize((uintptr) (TRACE_AUTOSTOP));
#endif /* TOPPERS_ENABLE_TRACE */
}

/*
 *  ターゲット依存の終了処理
 */
void
target_exit(void)
{
#ifdef TOPPERS_ENABLE_TRACE
	trace_dump(target_fput_log);
#endif /* TOPPERS_ENABLE_TRACE */

#ifndef ENABLE_RETURN_MAIN
	/*
	 *  シャットダウン処理の出力
	 */
	target_fput_str("Kernel Exit...");
#else
	target_fput_str("Kernel Shutdown...");
#endif /* ENABLE_RETURN_MAIN */

	/*
	 *  すべての割込みをマスクする．
	 */
	at91skyeye_disable_int(~0U);

	/*
	 *  ARM依存の終了処理
	 */
	core_terminate();

	/*
	 *  開発環境依存の終了処理
	 */
	at91skyeye_exit();

#ifdef ENABLE_RETURN_MAIN
	kerflg = FALSE;
	nested_lock_os_int_cnt = 0U;
	sus_all_cnt = 0U;
	sus_os_cnt = 0U;

	Asm("ldr  r0,  =_ostkpt");
	Asm("ldr  sp,  [r0]");
	Asm("bl   __cs3_start_c");
#endif /* ENABLE_RETURN_MAIN */

#if defined(TOPPERS_ENABLE_GCOV_PART)
	/*
	 *  一部取得の場合は終了時に .gcda ファイルを出力しないように
	 *  _exit()を呼び出す
	 */
	extern void _exit(int) NoReturn;
	;
	_exit(0);
#elif defined(TOPPERS_ENABLE_GCOV_FULL)
	exit(0);
#else
	while (1) {
	}
#endif /* TOPPERS_ENABLE_GCOV_PART */
}

/*
 *  システムログの低レベル出力のための文字出力
 */
void
target_fput_log(char8 c)
{
	usart0_putc(c);
}

/*
 *  割込み要求ラインの属性の設定
 */
void
x_config_int(InterruptNumberType intno, AttributeType intatr, PriorityType intpri)
{
	ASSERT(VALID_INTNO(intno));

	/*  SC1では起動時は割込み許可
	 *  intno指定された割込みのみ許可
	 */
	idf &= ~INTNO_BITPAT(intno);
	/* 全割込み禁止 */
	at91skyeye_disable_int(~0U);

	/* intno指定された割込み割込みのみ許可 */
	at91skyeye_enable_int(~(ipm_mask_tbl[INDEX_IPM(ipm)] | idf));
}

/*
 *  OS割込み禁止状態への移行
 */
void
x_nested_lock_os_int(void)
{
	nested_lock_os_int_cnt++;

	/* 全割込み禁止 */
	at91skyeye_disable_int(~0U);

	/* マスク指定されていない割込みのみ許可 */
	at91skyeye_enable_int(~(os_int_lock_mask | idf));
}

/*
 *  OS割込み禁止状態の解除
 */
void
x_nested_unlock_os_int(void)
{
	ASSERT(nested_lock_os_int_cnt > 0);
	nested_lock_os_int_cnt--;

	if (nested_lock_os_int_cnt == 0) {
		/* 全割込み禁止 */
		at91skyeye_disable_int(~0U);

		at91skyeye_enable_int(~(ipm_mask_tbl[INDEX_IPM(ipm)] | idf));
	}
}

#ifndef OMIT_DEFAULT_INT_HANDLER
/*
 * 未定義の割込みが入った場合の処理
 */
void
default_int_handler(void)
{
	target_fput_str("Unregistered Interrupt occurs.");
	target_exit();
	ASSERT_NO_REACHED;
}
#endif /* OMIT_DEFAULT_INT_HANDLER */

#ifdef TOPPERS_ENABLE_GCOV_PART
/*
 *  GCOV一部取得用ライブラリ
 */

extern void	__gcov_flush();

/*
 *  GCOV初期化関数
 *   カバレッジを.gcdaに出力し，SkyEyeで*.gcdaファイルを削除する
 */
void
gcov_init(void)
{
	SIL_PRE_LOC;

	SIL_LOC_INT();
	__gcov_flush();
	sil_wrw_mem((uint32 *) 0xFFFE1020, 0);
	SIL_UNL_INT();
}

/*
 *  GCOV中断関数
 *   カバレッジを.gcdaに出力し，SkyEyeで*.gcdaファイルを*.gcda.bakへ
 *   リネームする
 */
void
gcov_pause(void)
{
	SIL_PRE_LOC;

	SIL_LOC_INT();
	__gcov_flush();
	sil_wrw_mem((uint32 *) 0xFFFE1024, 0);
	SIL_UNL_INT();
}

/*
 *  GCOV再開関数
 *   カバレッジを.gcdaに出力し，SkyEyeで*.gcda.bakファイルを*.gcdaへ
 *   リネームする
 */
void
gcov_resume(void)
{
	SIL_PRE_LOC;

	SIL_LOC_INT();
	__gcov_flush();
	sil_wrw_mem((uint32 *) 0xFFFE1028, 0);
	SIL_UNL_INT();
}

/*
 *  GCOV出力関数
 *   カバレッジを.gcdaに出力する
 */
void
gcov_dump(void)
{
	SIL_PRE_LOC;

	SIL_LOC_INT();
	__gcov_flush();
	SIL_UNL_INT();
}
#endif /* TOPPERS_ENABLE_GCOV_PART */
