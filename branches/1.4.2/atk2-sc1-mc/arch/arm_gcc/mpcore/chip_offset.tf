$
$       オフセットファイル生成用テンプレートファイル（MPCore用）
$

$
$  標準テンプレートファイルのインクルード
$
$INCLUDE "kernel/genoffset.tf"$

$
$  コア依存テンプレートのインクルード（ARM用）
$
$INCLUDE"../common/core_offset.tf"$

$
$  オフセット値のマクロ定義の生成
$
$DEFINE("CCB_p_inh_tbl", offsetof_CCB_p_inh_tbl)$
