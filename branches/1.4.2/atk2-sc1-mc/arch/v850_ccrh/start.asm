;/*
 ;*  TOPPERS ATK2
 ;*      Toyohashi Open Platform for Embedded Real-Time Systems
 ;*      Automotive Kernel Version 2
 ;*
 ;*  Copyright (C) 2014 by Center for Embedded Computing Systems
 ;*              Graduate School of Information Science, Nagoya Univ., JAPAN
 ;*
 ;*  上記著作権者は，以下の(1)～(4)の条件を満たす場合に限り，本ソフトウェ
 ;*  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 ;*  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 ;*  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 ;*      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 ;*      スコード中に含まれていること．
 ;*  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 ;*      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 ;*      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 ;*      の無保証規定を掲載すること．
 ;*  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 ;*      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 ;*      と．
 ;*    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 ;*        作権表示，この利用条件および下記の無保証規定を掲載すること．
 ;*    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 ;*        報告すること．
 ;*  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 ;*      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 ;*      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 ;*      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 ;*      免責すること．
 ;*
 ;*  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 ;*  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 ;*  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 ;*  用する者に対して，AUTOSARパートナーになることを求めている．
 ;*
 ;*  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 ;*  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 ;*  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 ;*  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 ;*  の責任を負わない．
 ;* 
 ;*  $Id: start.S 549 2015-12-30 10:06:17Z ertl-honda $
 ;*/

;/*
 ;*		カーネル対応のスタートアップモジュール（V850用）
 ;*/

$include (v850asm.inc)


#define MAGIC_BASE 0xfffeec00
#define MAGIC_START 0x87654321

# *****************************************************************************
# CCRH予約シンボルの外部ラベル宣言(gp、ep用)
# *****************************************************************************
	.extern __gp_data, 4 
	.extern __ep_data, 4 
	.extern  hardware_init_hook
	.extern __bsstbl_start

# *****************************************************************************
# セクション初期化テーブル
# *****************************************************************************
	.section	".INIT_DSEC.const", const
	.align	4
	.dw	#__s.data,	#__e.data,	#__s.data.R

	.section	".INIT_BSEC.const", const
	.align	4
	.dw	#__s.bss,	#__e.bss

# *****************************************************************************
# 関数の外部ラベル宣言
# *****************************************************************************
	.extern _main
	.extern	_kernel_hardware_init_hook
	.extern	_kernel_software_init_hook
	.extern	__OSstart	
	.extern	_kernel_ostkpt_table
	
;-----------------------------------------------------------------------------
; スタートアップルーチン本体
;-----------------------------------------------------------------------------

STACKSIZE .set 0x200
.section ".stack.bss", bss
.align 4
.ds (STACKSIZE)
.align 4
_stacktop:

MEV_BASE	.set	0xfffeec00
MAGIC_START	.set	0x87654321
.section ".text", text
    .align  2     
    .public __OSstart  
__OSstart:
#マスカブル割込みの禁止
	di

;-----------------------------------------------------------------------------
; 各種ポインタの初期化(GP/EP/SP)
;-----------------------------------------------------------------------------


	;/*
	; * 各種ポインタの初期化(SP/TP/EP/GP/CTBP)ghs版
	; */
	GET_CID r4	
	shl     2, r4
	Lea     _kernel_ostkpt_table, r3
	add     r3, r4
	ld.w    0[r4], r3
	Lea     __ep_data, ep
	Lea     __gp_data, gp

	stsr 5, r10, 0 ; r10 <- PSW
	movhi 0x0001, r0, r11
	or r11, r10
	ldsr r10, 5, 0 ; enable FPU
	movhi 0x0002, r0, r11
	ldsr r11, 6, 0 ; initialize FPSR
	ldsr r0, 7, 0 ; initialize FPEPC

	;/* permit local ram access */
	mov 0xfffee600, r20 ;-- PEG #0 base
	mov 0x0001, r21 ;-- PEGG1SP val / All SPID is permit
	mov 0xffffffff, r22 ;-- PEGG1MK val / All Address Masked
	mov 0x00000077, r23 ;-- PEGG1BA val / Enable, Read/Write permit, Non-Lock, Base=0

	st.h    r21, 0x0c[r20]   ;-- set PEGGSP
	st.w    r22, 0x80[r20]   ;-- set PEGG1MK
	st.w    r23, 0x84[r20]   ;-- set PEGG1BA




	;-- Store completion barrier
	ld.w    0x84[r20], r0
	syncp


	;/*
	; * メモリアクセス許可後、SP(r3)再度設定 (Core1で必須)
	; */
	;nop
	;nop
	;GET_CID r4
	;shl     2, r4
	;Lea     __ostkpt_table, r3
	;add     r3, r4
	;ld.w    0[r4], r3

__start_1:
	;/*
	 ;*  各コアのローカルメモリの初期化(ToDo)
	 ;*/

	;/*
	 ;*  マスタコア以外は初期化待ち
	 ;*/
	GET_CID r6 ;値をデバッグで確認
	cmp     r0, r6
	bne     slave_start


;-----------------------------------------------------------------------------
; dataセクションの初期値コピーおよびbssセクションのゼロクリア
;-----------------------------------------------------------------------------
	mov	#__s.INIT_DSEC.const, r6	;ccrh specification
	mov	#__e.INIT_DSEC.const, r7
	mov	#__s.INIT_BSEC.const, r8
	mov	#__e.INIT_BSEC.const, r9
	jarl32	__INITSCT_RH, lp	;  initialize RAM area

	mov	0, r6
	mov	0, r7
	mov	#__s.bss, r8
	mov	#__e.bss, r9
	jarl32	__INITSCT_RH, lp	;  initialize RAM area

	mov     #__s.pe1_lram.bss, ep
	mov     #__e.pe1_lram.bss, r7
pe1_bss_clear_start:
	cmp     ep, r7
	bl      pe1_bss_clear_end
	sst.w   r0, 0[ep]
	add     4, ep
	br      pe1_bss_clear_start
pe1_bss_clear_end:

	mov     #__s.pe2_lram.bss, ep
	mov     #__e.pe2_lram.bss, r7
pe2_bss_clear_start:
	cmp     ep, r7
	bl      pe2_bss_clear_end
	sst.w   r0, 0[ep]
	add     4, ep
	br      pe2_bss_clear_start
pe2_bss_clear_end:


	mov     #__s.data, r6
	mov     #__e.data, r7
	mov 	#__s.data.R, r8
data_init_start:
	cmp     r6, r7
	bl      data_init_end
	ld.w    0[r6], r9
	st.w    r9, 0[r8]
	add     4, r6
	add     4, r8
	br      data_init_start
data_init_end:



	;/*
	 ;*  他のコアは停止とする
	 ;*/
	Lea     _kernel_core_state_table, r6
	st.w    r0, 4[r6]   ;/* for PE2 */

	;/*
	 ;*  マスタコアを起動済みとする
	 ;*/
	mov32  MAGIC_START, r7 ;MAGIC_START:UINT_C(0x87654321)
	st.w    r7, 0[r6]


__start_4:
;-----------------------------------------------------------------------------
; ターゲット依存のハードウェアの初期化コードへ
;-----------------------------------------------------------------------------
	jarl    _kernel_target_hardware_initialize, lp



	;/*
	 ;* メモリ初期化終了の通知
	 ;*/
	mov  MEV_BASE, r20 ;MEV_BASE:0xfffeec00
	mov32  MAGIC_START, r7 ;MAGIC_START:UINT_C(0x87654321) 
	st.w r7, 0x00[r20]

__start_5:
	jarl    _main, r31
	halt
	;/*
	 ;*  マスタコア以外のメモリ初期化待ち及びStartCore待ちルーチン
	 ;*/
slave_start:
	;/*
	 ;*  メモリ初期化待ち
	 ;*/
	mov  MEV_BASE, r20 ;MEV_BASE:0xfffeec00
	ld.w 0x00[r20], r6
	cmp  r0, r6
	be   slave_start

	;/*
	 ;*  StartCore待ち
	 ;*/
	GET_CID r6
	Lea     _kernel_core_state_table, r9
	shl     2, r6
	add     r6, r9
	mov     MAGIC_START, r7 ;mov ->mov32 MAGIC_START:0x87654321
slave_start_1:
	ld.w    0[r9], r8
	cmp     r7, r8
	bne     slave_start_1
	br      __start_5

;-----------------------------------------------------------------------------
; mainから戻ってきた後の処理
;-----------------------------------------------------------------------------
;__exit:
	halt    
__startend:


	.section	".data", data
.L.dummy.data:
	.section	".bss", bss
.L.dummy.bss:
	.section	".const", const
.L.dummy.const:
	.section	".stack.bss", bss
.L.dummy.stack.bss:
	.section	"RESET", text
.L.dummy.RESET:
	.section	"EIINTTBL", const
	.align 512
.L.dummy.EIINTTBL:
