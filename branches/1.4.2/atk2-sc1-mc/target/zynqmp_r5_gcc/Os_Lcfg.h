/* Os_Lcfg.h */
#ifndef TOPPERS_OS_LCFG_H
#define TOPPERS_OS_LCFG_H

#define TNUM_ALARM				UINT_C(7)
#define TNUM_COUNTER			UINT_C(2)
#define TNUM_HARDCOUNTER		UINT_C(0)
#define TNUM_ISR2				UINT_C(6)
#define TNUM_ICI				UINT_C(0)
#define TNUM_STD_RESOURCE		UINT_C(0)
#define TNUM_TASK				UINT_C(6)
#define TNUM_EXTTASK			UINT_C(3)
#define TNUM_APP_MODE			UINT_C(2)
#define TNUM_SCHEDULETABLE		UINT_C(0)
#define TNUM_IMPLSCHEDULETABLE	UINT_C(0)
#define TNUM_OSAP				UINT_C(4)
#define TNUM_SPINLOCK			UINT_C(0)

/*
 *  Default Definitions of Trace Log Macros
 */

#ifndef TOPPERS_ENABLE_TRACE
#ifndef LOG_USER_MARK
#define LOG_USER_MARK(str)
#endif /* LOG_USER_MARK */
#endif /* TOPPERS_ENABLE_TRACE */

/****** Object TASK ******/

#define BlsmControl_Core0_1_0_Task	UINT_C(0)
#define BlsmControl_Core1_1_0_Task	UINT_C(1)
#define BlsmControl_Core1_100_0_Task	UINT_C(2)
#define OperationManagerTask	UINT_C(3)
#define DriveManagerTask	UINT_C(4)
#define BodyControlTask	UINT_C(5)

/****** Object COUNTER ******/

#define SwCnt0	UINT_C(0)
#define SwCnt1	UINT_C(1)

#define OSMAXALLOWEDVALUE_SwCnt0	((TickType) 99999)
#define OSTICKSPERBASE_SwCnt0	((TickType) 1)
#define OSMINCYCLE_SwCnt0	((TickType) 1)
#define OSMAXALLOWEDVALUE_SwCnt1	((TickType) 99999)
#define OSTICKSPERBASE_SwCnt1	((TickType) 1)
#define OSMINCYCLE_SwCnt1	((TickType) 1)

/****** Object ALARM ******/

#define OperationManagerCycAlarm	UINT_C(0)
#define DriveManagerCycAlarm	UINT_C(1)
#define BodyControlCycAlarm	UINT_C(2)
#define BuzzerControlCycAlarm	UINT_C(3)
#define BlsmControl_Core0_1_0_Alarm	UINT_C(4)
#define BlsmControl_Core1_1_0_Alarm	UINT_C(5)
#define BlsmControl_Core1_100_0_Alarm	UINT_C(6)

/****** Object SCHEDULETABLE ******/


/****** Object RESOURCE ******/


/****** Object ISR ******/

#define RxHwSerialInt0	UINT_C(0)
#define RxHwSerialInt1	UINT_C(1)
#define RLIN3x_TX_ISR	UINT_C(2)
#define RLIN3x_RX_ISR	UINT_C(3)
#define SwCntTimerHdr0	UINT_C(4)
#define SwCntTimerHdr1	UINT_C(5)


/****** Object APPMODE ******/

#define BlsmControl_AppMode	UINT_C(0)
#define Test_AppMode	UINT_C(1)

/****** Object EVENT ******/
#define BlsmControl_WakeupEvent	UINT_C(0x00000001)
#define BlsmControl_InputEvent	UINT_C(0x00000002)
#define BlsmControl_OutputEvent	UINT_C(0x00000004)
#define BlsmControl_CompleteEvent	UINT_C(0x00000008)

/****** Object OSApplication ******/
#define BlsmControl_OSAP_CORE0	UINT_C(0)
#define BlsmControl_OSAP_CORE1	UINT_C(1)
#define OSAP_for_RxHwSerialInt0	UINT_C(2)
#define OSAP_for_RxHwSerialInt1	UINT_C(3)

/****** Object SPINLOCK ******/


/****** Object IOC ******/
#define TNUM_IOC			UINT_C(0)
#define TNUM_QUEUEIOC		UINT_C(0)
#define TNUM_IOC_WRAPPER	UINT_C(0)

#ifndef TOPPERS_MACRO_ONLY
extern TASK(BlsmControl_Core0_1_0_Task);
extern TASK(BlsmControl_Core1_1_0_Task);
extern TASK(BlsmControl_Core1_100_0_Task);
extern TASK(OperationManagerTask);
extern TASK(DriveManagerTask);
extern TASK(BodyControlTask);


extern ALARMCALLBACK(BuzzerControlCycAlarmCb);


extern void _kernel_inthdr_0xffff00ee(void);
extern void _kernel_inthdr_0xffff00ed(void);
extern void _kernel_inthdr_0xffff00a5(void);
extern void _kernel_inthdr_0xffff00a6(void);
extern void _kernel_inthdr_0xffff0054(void);
extern void _kernel_inthdr_0xffff013a(void);

extern ISR(RxHwSerialInt0);
extern ISR(RxHwSerialInt1);
extern ISR(RLIN3x_TX_ISR);
extern ISR(RLIN3x_RX_ISR);
extern ISR(SwCntTimerHdr0);
extern ISR(SwCntTimerHdr1);

extern ISR(target_ici_handler0);
extern ISR(target_ici_handler1);

#ifdef TOPPERS_ENABLE_TRACE
extern const char8 *atk2_appid_str(AppModeType id);
extern const char8 *atk2_tskid_str(TaskType id);
extern const char8 *atk2_isrid_str(ISRType id);
extern const char8 *atk2_cntid_str(CounterType id);
extern const char8 *atk2_almid_str(AlarmType id);
extern const char8 *atk2_resid_str(ResourceType id);
extern const char8 *atk2_schtblid_str(ScheduleTableType id);
extern const char8 *atk2_evtid_str(TaskType task, EventMaskType event);
extern const char8 *atk2_osapid_str(ApplicationType id);
extern const char8 *atk2_iocid_str(IocType id);
#endif /* TOPPERS_ENABLE_TRACE */
#endif /* TOPPERS_MACRO_ONLY */
#endif /* TOPPERS_OS_LCFG_H */

