/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#include <stdlib.h>
#include <libipcx.h>
#include "sample.h"

void	sub_func(void);
void	call_handler_PE2(void);
int	call_func_PE2(void);
int	local_counter = 0;
int	call_counter = 0;

void
main(void)
{
	int	count;
	int	time;
	int	error;

	time	= S_getPE();

	F_sync(2);

	while (call_counter != 30) {

		F_wait(ID_F_SEM2);

		S_lock(ID_F_SEM1);

		count = cmn_counter;
		delay();

		time = rand() % 10;
		cmn_counter = count + time;

		S_unlock(ID_F_SEM1);

		F_clear(ID_F_SEM2);

		sub_func();

		local_counter = (int)P_call_id(ID_PE1, ID_FUNC3, local_counter, time);
		while(local_counter == CALL_ERROR) {}
		
		wait();
	}

		error = P_request_id(ID_PE1, 34, 0, 0);
		while(error == CALL_ERROR) {}

		/* 誤った関数を呼び出し、エラーになる */
		error = P_call_id(ID_PE1, 35, 0, 0);
		while(error == CALL_ERROR) {}
}

void
sub_func(void)
{
	int	count;
	int	time;

	S_lock(ID_F_SEM1);

	count = cmn_counter;
	delay();

	time = rand() % 10;
	cmn_counter = count + time;

	S_unlock(ID_F_SEM1);

	local_counter += time;
}

void
call_handler_PE2(void)		//ID_INT1
{
	if (call_counter < 30) {
		call_counter++;
	} else {}
	return;
}

int
call_func_PE2(void)		//ID_FUNC2
{
	return(0);
}
/* eof */