/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *				注意事項
 *
 *	本ソースコードはアセンブラ言語で記述されていますが
 *	アセンブルの前に、CC-RHコンパイラのプリプロセッサ機能を利用して
 *	プリプロセスを行う必要があります。
 *
 *	CC-RHのプリプロセッサに本ファイルを読み込ませるために、
 *	本ファイルは.C形式で提供しています。
 *
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "libipcx_src.h"

m_GLOBL(_ipir_ch1_handler)
m_GLOBL(_ipir_ch1_rtos_handler)


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ ipir_ch1_handler ]
 *
 *	概要
 *		PE間割り込みエントリ処理(RTOSを使用しない)
 *	文法
 *		なし（PE間割り込みに対応する割り込みベクタに配置する）
 *	入力パラメータ
 *		なし
 *	戻り値
 *		なし
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_ipir_ch1_handler:

	addi	(-1) * SIZE_EF, sp , sp
	st.w	r29, u4_IF_r29[sp]
	st.w	ep, u4_IF_ep[sp]
	st.w	lp, u4_IF_lp[sp]

	addi	(-1) * SIZE_IF, sp , sp
	mov	sp, ep

	sst.w	r1, u4_IF_r1[ep]
	sst.w	r2, u4_IF_r2[ep]
	sst.w	r6, u4_IF_r6[ep]
	sst.w	r7, u4_IF_r7[ep]
	sst.w	r8, u4_IF_r8[ep]
	sst.w	r9, u4_IF_r9[ep]
	sst.w	r10, u4_IF_r10[ep]
	sst.w	r11, u4_IF_r11[ep]
	sst.w	r12, u4_IF_r12[ep]
	sst.w	r13, u4_IF_r13[ep]
	sst.w	r14, u4_IF_r14[ep]
	sst.w	r15, u4_IF_r15[ep]
	sst.w	r16, u4_IF_r16[ep]
	sst.w	r17, u4_IF_r17[ep]
	sst.w	r18, u4_IF_r18[ep]
	sst.w	r19, u4_IF_r19[ep]

	stsr	SystemRegister_EIPC, r11, SelID0
	stsr	SystemRegister_EIPSW, r12, SelID0
	sst.w	r11, u4_IF_EIPC[ep]
	sst.w	r12, u4_IF_EIPSW[ep]

	stsr	SystemRegister_CTPC, r11, SelID0
	stsr	SystemRegister_CTPSW, r12, SelID0
	sst.w	r11, u4_IF_CTPC[ep]
	sst.w	r12, u4_IF_CTPSW[ep]

	/* 
	 * FPU関連レジスタの退避が必要な場合、ここで行う
	 * (退避するフレームの定義は libipcx_src.h)
	 * 
	 * 注意点
	 * - FPU関連レジスタ・アクセス時には
	 *   コプロセッサ(FPU)使用権(PSW.CU0ビット)が1である必要がある。
	 
	 * - FPU非搭載デバイスの場合、FPU関連レジスタ・アクセスによって例外が発生するため
	 *   メインCPUとPCUでipir_ch0_handlerを分ける必要があるかもしれない
	 */

	/* 
	 * ループ処理
	 * 起動要求が有効になっている割り込みハンドラをすべて起動する
	 */
.ipir1_handler.callloop:
	mov	mpu4_MEV3, ep
	sld.w	0[ep], r29
	
	sch1r	r29, r11
	bz	.ipir1_handler.done_callloop		/* MEV3が0になっていれば、ループ終了 */

	/* 起動する割り込みハンドラに対応するビットを落とす */

	add	(-1), r11
	andi	MASK_MOD_8, r11, r13			/* バイト(8bit)内オフセット*/

	mov	r11, r14

	shr	SHIFT_DIV_8, r11
	add	r11, ep

	clr1	r13, [ep]				/* 割り込み要求をクリア */

	/*
	 * ここに割り込みハンドラテーブルからアドレスを取得し
	 * 分岐するまでの処理を記載する
	 *
	 * 以下はサンプルの記述例です
	 * 使用する場合はユーザの割り込みハンドラテーブルに書き換えて使用してください
	 */
	mov	#_adr_interrupttable, r12
	ld.w	0[r12], r11				/* r11: 割り込みハンドラテーブルのアドレス */
	shl	SIZE_WORD, r14
	add	r14, r11
	ld.w	0[r11], r10
	jarl	[r10], lp				/* 割り込みハンドラへ */

	br	.ipir1_handler.callloop

.ipir1_handler.done_callloop:

	mov	sp, ep

	/* 
	 * FPU関連レジスタの復帰が必要な場合、ここで行う
	 * (退避するフレームの定義は libipcx_src.h)
	 */

	sld.w	u4_IF_CTPSW[ep], r12
	sld.w	u4_IF_CTPC[ep], r11
	ldsr	r12, SystemRegister_CTPSW, SelID0
	ldsr	r11, SystemRegister_CTPC, SelID0

	sld.w	u4_IF_EIPSW[ep], r12
	sld.w	u4_IF_EIPC[ep], r11
	ldsr	r12, SystemRegister_EIPSW, SelID0
	ldsr	r11, SystemRegister_EIPC, SelID0

	sld.w	u4_IF_r19[ep], r19
	sld.w	u4_IF_r18[ep], r18
	sld.w	u4_IF_r17[ep], r17
	sld.w	u4_IF_r16[ep], r16
	sld.w	u4_IF_r15[ep], r15
	sld.w	u4_IF_r14[ep], r14
	sld.w	u4_IF_r13[ep], r13
	sld.w	u4_IF_r12[ep], r12
	sld.w	u4_IF_r11[ep], r11
	sld.w	u4_IF_r10[ep], r10
	sld.w	u4_IF_r9[ep], r9
	sld.w	u4_IF_r8[ep], r8
	sld.w	u4_IF_r7[ep], r7
	sld.w	u4_IF_r6[ep], r6
	sld.w	u4_IF_r2[ep], r2
	sld.w	u4_IF_r1[ep], r1

	addi	SIZE_IF, sp , sp
	
	ld.w	u4_IF_r29[sp], r29
	ld.w	u4_IF_ep[sp], ep
	ld.w	u4_IF_lp[sp], lp
	
	addi	SIZE_EF, sp , sp

	eiret

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ ipir_ch1_rtos_handler ]
 *
 *	概要
 *		PE間通信用割り込みハンドラ(RTOSを使用する)
 *	文法
 *		なし(RTOS管理内の割り込みハンドラとして登録する)
 *	入力パラメータ
 *		なし
 *	戻り値
 *		なし
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_ipir_ch1_rtos_handler:

	addi	(-1) * SIZE_EF, sp , sp
	st.w	r29, u4_IF_r29[sp]
	st.w	ep, u4_IF_ep[sp]
	st.w	lp, u4_IF_lp[sp]

	/* 
	 * ループ処理
	 * 起動要求が有効になっている割り込みハンドラをすべて起動する
	 */
.ipir1_rtos_handler.callloop:
	mov	mpu4_MEV3, ep
	sld.w	0[ep], r29
	
	sch1r	r29, r11
	bz	.ipir1_rtos_handler.done_callloop	/* MEV3が0になっていれば、ループ終了 */

	/* 起動する割り込みハンドラに対応するビットを落とす */

	add	(-1), r11
	andi	MASK_MOD_8, r11, r13			/* バイト(8bit)内オフセット*/

	mov	r11, r14

	shr	SHIFT_DIV_8, r11
	add	r11, ep

	clr1	r13, [ep]				/* 割り込み要求をクリア */

	/*
	 * ここに割り込みハンドラテーブルからアドレスを取得し
	 * 分岐するまでの処理を記載する
	 *
	 * 以下はサンプルの記述例です
	 * 使用する場合はユーザの割り込みハンドラテーブルに書き換えて使用してください
	 */
	mov	#_adr_interrupttable, r12
	ld.w	0[r12], r11				/* r11: 割り込みハンドラテーブルのアドレス */
	shl	SIZE_WORD, r14
	add	r14, r11
	ld.w	0[r11], r10
	jarl	[r10], lp				/* 割り込みハンドラへ */

	br	.ipir1_rtos_handler.callloop

.ipir1_rtos_handler.done_callloop:

	mov	sp, ep

	addi	SIZE_IF, sp , sp
	
	ld.w	u4_IF_r29[sp], r29
	ld.w	u4_IF_ep[sp], ep
	ld.w	u4_IF_lp[sp], lp
	
	addi	SIZE_EF, sp , sp

	jmp	[lp]

/* eof */
