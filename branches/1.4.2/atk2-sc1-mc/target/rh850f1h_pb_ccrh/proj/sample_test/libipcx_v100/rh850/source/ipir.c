/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *				注意事項
 *
 *	本ソースコードはアセンブラ言語で記述されていますが
 *	アセンブルの前に、CC-RHコンパイラのプリプロセッサ機能を利用して
 *	プリプロセスを行う必要があります。
 *
 *	CC-RHのプリプロセッサに本ファイルを読み込ませるために、
 *	本ファイルは.C形式で提供しています。
 *
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "libipcx_src.h"

m_GLOBL(_P_call)			/* 処理呼び出し */
m_GLOBL(_P_vcall)			/* 処理呼び出し (可変引数) */
m_GLOBL(_P_request)			/* 処理要求 */
m_GLOBL(_P_vrequest)			/* 処理要求 (可変引数) */

m_GLOBL(_P_call_id)			/* 処理呼び出し */
m_GLOBL(_P_vcall_id)			/* 処理呼び出し (可変引数) */
m_GLOBL(_P_request_id)			/* 処理要求 */
m_GLOBL(_P_vrequest_id)			/* 処理要求 (可変引数) */

m_GLOBL(_P_raise_handler)		/* 処理呼び出し (割り込み要求型) */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ P_call ]
 *	[ P_vcall ]
 *	[ P_call_id ]
 *	[ P_vcall_id ]
 *
 *	概要
 *		PE間処理呼び出し
 *	文法
 *		int
 *		P_call ( int PEID, void (* fp)(), void * arg1, void * arg2 );
 *		P_vcall ( int PEID, void (* fp)(), va_list ap );
 *		P_call_id (int PEID, fncid, void * arg1, void * arg2 );
 *		P_vcall_id (int PEID, fncid, va_list ap ); 
 *	入力パラメータ
 *		PEID	: 対象PEID
 *		fp	: 関数アドレス
 *		fncid	: 関数ID
 *		arg1	: 引数1
 *		arg2	: 引数2
 *		ap	: 引数リスト
 *	戻り値
 *		呼び出し関数の戻り値
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	m_ALIGN(4)
_P_call_id:
_P_vcall_id:
#ifdef	__parameter_check__
	cmp	MIN_FNCID, r7
	blt	.p_cmn.error

	cmp	MAX_FNCID, r7
	bgt	.p_cmn.error
#endif	/* __parameter_check__ */

	mov	MEV_BIT_FUNC, r11
	mov	ep, r12
	mov	mpu4_MEV4, ep

.p_call_id.func_loop:
	/* 関数呼び出し方式(関数ポインタ or 関数テーブル)に関わらず，排他はMEV_BIT_FUNCで行う */
	set1	r11, [ep]		/* 排他 */

	bnz	.p_call_id.func_loop

	mov	MEV_BIT_FUNCID, r11

	/*
	 * MEVのFUNCIDビットは排他には使用しない
	 * 関数を呼び出される側で受け付けたPE間割り込み処理内で、
	 * 引数に関数IDが渡されているか、関数ポインタが渡されているか
	 * の区別のために使用する
	 */
	set1	r11, [ep]

	br	.p_call.cmn

	m_ALIGN(4)
_P_call:
_P_vcall:

	mov	MEV_BIT_FUNC, r11
	mov	ep, r12
	mov	mpu4_MEV4, ep

.p_call.func_loop:
	/* 関数呼び出し方式(関数ポインタ or 関数テーブル)に関わらず，排他はMEV_BIT_FUNCで行う */
	set1	r11, [ep]		/* 排他 */

	bnz	.p_call.func_loop

.p_call.cmn:
	sst.w	r7, (mpu4_MEV5 - mpu4_MEV4)[ep]
	sst.w	r8, (mpu4_MEV6 - mpu4_MEV4)[ep]
	sst.w	r9, (mpu4_MEV7 - mpu4_MEV4)[ep]

	mov	MEV_BIT_CALL, r11
	set1	r11, [ep]

	/* 対象PEIDに割り込み要求をセット */
	/* PE1(1)->bit0, PE2(2)->bit1, PCU(3)->bit2 */
	add	(-1), r6
	mov	1, r13
	shl	r6, r13
	st.w	r13, mpu4_IPIR_CH0[r0]

.p_call.return_loop:
	tst1	r11, [ep]		/* 排他 */

	bnz	.p_call.return_loop

	sld.w	(mpu4_MEV5 - mpu4_MEV4)[ep], r10
	mov	r12, ep

	jmp	[lp]


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ P_request ]
 *	[ P_vrequest ]
 *	[ P_request_id ]
 *	[ P_vrequest_id ]
 *
 *	概要
 *		PE間処理要求
 *	文法
 *		int
 *		P_request ( int PEID, void (* fp)(), void * arg1, void * arg2 );
 *		P_vrequest ( int PEID, void (* fp)(), va_list ap );
 *		P_request_id ( int PEID, fncid, void * arg1, void * arg2 );
 *		P_vrequest_id ( int PEID, fncid, va_list ap );
 *	入力パラメータ
 *		PEID	: 対象PEID
 *		fp	: 関数アドレス
 *		fncid	: 関数ID
 *		arg1	: 引数1
 *		arg2	: 引数2
 *		ap	: 引数リスト
 *	戻り値
 *		0	: 正常終了
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_P_request_id:
_P_vrequest_id:
#ifdef	__parameter_check__
	cmp	MIN_FNCID, r7
	blt	.p_cmn.error

	cmp	MAX_FNCID, r7
	bgt	.p_cmn.error
#endif	/* __parameter_check__ */
	mov	MEV_BIT_FUNC, r11
	mov	ep, r12
	mov	mpu4_MEV4, ep

.p_request_id.func_loop:
	/* 関数呼び出し方式(関数ポインタ or 関数テーブル)に関わらず，排他はMEV_BIT_FUNCで行う */
	set1	r11, [ep]		/* 排他 */

	bnz	.p_request_id.func_loop

	mov	MEV_BIT_FUNCID, r11

	/*
	 * MEVのFUNCIDビットは排他には使用しない
	 * 関数を呼び出される側で受け付けたPE間割り込み処理内で、
	 * 引数に関数IDが渡されているか、関数ポインタが渡されているか
	 * の区別のために使用する
	 */
	set1	r11, [ep]

	br	.p_request.cmn

	m_ALIGN(4)
_P_request:
_P_vrequest:

	mov	MEV_BIT_FUNC, r11
	mov	ep, r12
	mov	mpu4_MEV4, ep

.p_request.func_loop:
	/* 関数呼び出し方式(関数ポインタ or 関数テーブル)に関わらず，排他はMEV_BIT_FUNCで行う */
	set1	r11, [ep]		/* 排他 */

	bnz	.p_request.func_loop

.p_request.cmn:

	sst.w	r7, (mpu4_MEV5 - mpu4_MEV4)[ep]
	sst.w	r8, (mpu4_MEV6 - mpu4_MEV4)[ep]
	sst.w	r9, (mpu4_MEV7 - mpu4_MEV4)[ep]

	mov	MEV_BIT_REQUEST, r11
	set1	r11, [ep]

	/* 対象PEIDに割り込み要求をセット */
	/* PE1(1)->bit0, PE2(2)->bit1, PCU(3)->bit2 */
	add	(-1), r6
	mov	1, r13
	shl	r6, r13
	st.w	r13, mpu4_IPIR_CH0[r0]

	mov	0, r10
	mov	r12, ep

	jmp	[lp]


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ P_raise_handler ]
 *
 *	概要
 *		PE間処理呼び出し（割り込みハンドラとして呼び出す）
 *		同時に複数の割り込みハンドラ呼び出しが可能
 *	文法
 *		int
 *		P_raise_handler ( int PEID, intid);
 *	入力パラメータ
 *		PEID	: 対象PEID
 *		intid	: 起動を要求する割り込みハンドラID
 *	戻り値
 *		 1 :intidに指定した割り込みハンドラの起動要求保留中
 *		 0 :正常終了
 *		-1 :指定したIDが範囲外
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	m_ALIGN(4)
_P_raise_handler:

#ifdef	__parameter_check__
	cmp	MIN_INTID, r7
	blt	.p_cmn.error

	cmp	MAX_INTID, r7
	bgt	.p_cmn.error
#endif	/* __parameter_check__ */

	add	(-1), r7
	andi	MASK_MOD_8, r7, r11	/* バイト(8bit)内オフセット*/

	shr	SHIFT_DIV_8, r7
	mov	mpu4_MEV3, r12
	add	r7, r12			/* MEV3アドレス */

	set1	r11, [r12]		/* 割り込み要求をセット */

	bnz	.p_raise_handler.failed

	/* 対象PEIDに割り込み要求をセット */
	/* PE1(1)->bit0, PE2(2)->bit1, PCU(3)->bit2 */
	mov	1, r13
	add	(-1), r6
	shl	r6, r13
	st.w	r13, mpu4_IPIR_CH1[r0]

	mov	0, r10

	jmp	[lp]

.p_raise_handler.failed:
	mov	1, r10

	jmp	[lp]

#ifdef	__parameter_check__
.p_cmn.error:
	mov	(-1), r10
	jmp	[lp]
#endif	/* __parameter_check__ */
/* eof */
