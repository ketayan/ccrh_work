/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#ifndef	__sample_h__
#define	__sample_h__

/*
 *	PE間排他制御用 資源ID
 */
#define	ID_F_SEM1	1
#define	ID_F_SEM2	2

#define	ID_F_ALL	0

/*
 *	PEID
 */
#define	ID_PE1	1
#define	ID_PE2	2
#define	ID_PE3	3

/* PE間通信 関数ID */
#define	ID_FUNC1	1
#define	ID_FUNC2	2
#define	ID_FUNC3	3

#define	ID_INT1		1

/* libipcx API エラー */
#define	CALL_ERROR	-1

typedef	void		(*FP)();

int	cmn_func_add(int a, int b);

extern int	cmn_counter;

extern void	delay(void);
extern void	wait(void);

extern int dummy_func_PE1(void);
extern int dummy_intfunc_PE1(void);
extern int call_handler_PE1(void);
extern int call_func_PE1(void);

extern int dummy_func_PE2(void);
extern int dummy_intfunc_PE2(void);
extern void call_handler_PE2(void);
extern int call_func_PE2(void);

#endif	/* __sample_h__ */

/* eof */
