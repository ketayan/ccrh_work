/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#ifndef	__libipcx_h__
#define	__libipcx_h__

#include <stdarg.h>


/*
 *	処理間排他/同期制御
 */

int	F_init(int ID);
int	F_lock(int ID);
int	F_try(int ID);
int	F_unlock(int ID);
int	F_wait(int ID);
int	F_test(int ID);
int	F_set(int ID);
int	F_clear(int ID);

int	S_lock(int ID);
int	S_try(int ID);
int	S_unlock(int ID);

/*
 *	PE間処理呼び出し
 */

int	P_call(int PEID, void (*fp)(), ...);
int	P_vcall(int PEID, void (*fp)(), va_list AP);
int	P_request(int PEID, void (*fp)(), ...);
int	P_vrequest(int PEID, void (*fp)(), va_list AP);
int	P_call_id(int PEID, int FUNCID, ...);
int	P_vcall_id(int PEID, int FUNCID, va_list AP);
int	P_request_id(int PEID, int FUNCID, ...);
int	P_vrequest_id(int PEID, int FUNCID, va_list AP);

int	P_raise_handler(int PEID, int INTID);

/*
 *	その他システム
 */
int	S_getPE(void);

int	F_sync(int PENUM);

#endif	/* __libipcx_h__ */

/* eof */
