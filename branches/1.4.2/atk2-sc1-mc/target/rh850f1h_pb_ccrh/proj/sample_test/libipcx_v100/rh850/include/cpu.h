/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#ifndef	__mpx_cpu_h__
#define	__mpx_cpu_h__


/* ============================================================ */
/*	システム・レジスタ											*/
/* ============================================================ */

/* ------------------------------------------------------------ */
/*	選択識別子													*/
/* ------------------------------------------------------------ */
#define	SelID0					0
#define	SelID1					1
#define	SelID2					2
#define	SelID3					3
#define	SelID4					4
#define	SelID5					5
#define	SelID6					6
#define	SelID7					7

/* ------------------------------------------------------------ */
/*	基本システム・レジスタ番号									*/
/* ------------------------------------------------------------ */
/* selID:0 */
#define	SystemRegister_EIPC		 0
#define	SystemRegister_EIPSW	 1
#define	SystemRegister_FEPC		 2
#define	SystemRegister_FEPSW	 3
#define	SystemRegister_PSW		 5
#define	SystemRegister_EIIC		13
#define	SystemRegister_FEIC		14
#define	SystemRegister_CTPC		16
#define	SystemRegister_CTPSW	17
#define	SystemRegister_CTBP		20
#define	SystemRegister_EIWR		28
#define	SystemRegister_FEWR		29

/* selID:1 */
#define	SystemRegister_MCFG0	 0
#define	SystemRegister_MCFG1	 1
#define	SystemRegister_RBASE	 2
#define	SystemRegister_EBASE	 3
#define	SystemRegister_INTBP	 4
#define	SystemRegister_MCTL		 5
#define	SystemRegister_PID		 6
#define	SystemRegister_SCCFG	11
#define	SystemRegister_SCBP		12

/* selID:2 */
#define	SystemRegister_HTCFG0	 0
#define	SystemRegister_MEA		 6
#define	SystemRegister_ASID		 7
#define	SystemRegister_MEI		 8

/* ------------------------------------------------------------ */
/*	割り込み機能システム・レジスタ番号							*/
/* ------------------------------------------------------------ */

/* selID:1 */
#define	SystemRegister_FPIPR	 7

/* selID:2 */
#define	SystemRegister_ISPR	 	10
#define	SystemRegister_PMR		11
#define	SystemRegister_ICSR		12
#define	SystemRegister_INTCFG	13

/* ------------------------------------------------------------ */
/*	FPU機能システム・レジスタ									*/
/* ------------------------------------------------------------ */

/* selID:0 */
#define	SystemRegister_FPSR		 6
#define	SystemRegister_FPEPC	 7
#define	SystemRegister_FPST		 8
#define	SystemRegister_FPCC		 9
#define	SystemRegister_FPCFG	10
#define	SystemRegister_FPEC		11

/* ------------------------------------------------------------ */
/*	MPU機能システム・レジスタ									*/
/* ------------------------------------------------------------ */

/* selID:5 */
#define	SystemRegister_MPM		 0
#define	SystemRegister_MPRC		 1
#define	SystemRegister_MPBRGN	 4
#define	SystemRegister_MPTRGN	 5
#define	SystemRegister_MCA		 8
#define	SystemRegister_MCS		 9
#define	SystemRegister_MCC		10
#define	SystemRegister_MCR		11

/* selID:6 */
#define	SystemRegister_MPLA0	 0
#define	SystemRegister_MPUA0	 1
#define	SystemRegister_MPAT0	 2

#define	SystemRegister_MPLA1	 4
#define	SystemRegister_MPUA1	 5
#define	SystemRegister_MPAT1	 6

#define	SystemRegister_MPLA2	 8
#define	SystemRegister_MPUA2	 9
#define	SystemRegister_MPAT2	10

#define	SystemRegister_MPLA3	12
#define	SystemRegister_MPUA3	13
#define	SystemRegister_MPAT3	14

#define	SystemRegister_MPLA4	16
#define	SystemRegister_MPUA4	17
#define	SystemRegister_MPAT4	18

#define	SystemRegister_MPLA5	20
#define	SystemRegister_MPUA5	21
#define	SystemRegister_MPAT5	22

#define	SystemRegister_MPLA6	24
#define	SystemRegister_MPUA6	25
#define	SystemRegister_MPAT6	26

#define	SystemRegister_MPLA7	28
#define	SystemRegister_MPUA7	29
#define	SystemRegister_MPAT7	30

/* selID:7 */
#define	SystemRegister_MPLA8	 0
#define	SystemRegister_MPUA8	 1
#define	SystemRegister_MPAT8	 2

#define	SystemRegister_MPLA9	 4
#define	SystemRegister_MPUA9	 5
#define	SystemRegister_MPAT9	 6

#define	SystemRegister_MPLA10	 8
#define	SystemRegister_MPUA10	 9
#define	SystemRegister_MPAT10	10

#define	SystemRegister_MPLA11	12
#define	SystemRegister_MPUA11	13
#define	SystemRegister_MPAT11	14

/* ============================================================ */
/*	PSWフラグ													*/
/* ============================================================ */

#define	mu4_PSW_Z			0x00000001
#define	mu4_PSW_S			0x00000002
#define	mu4_PSW_OV			0x00000004
#define	mu4_PSW_CY			0x00000008
#define	mu4_PSW_SAT			0x00000010
#define	mu4_PSW_ID			0x00000020
#define	mu4_PSW_EP			0x00000040
#define	mu4_PSW_NP			0x00000080

#define	mu4_PSW_EBV			0x00008000
#define	mu4_PSW_CU0			0x00010000
#define	mu4_PSW_CU1			0x00020000
#define	mu4_PSW_CU2			0x00040000
#define	mu4_PSW_HVC			0x00080000
#define	mu4_PSW_UM			0x40000000


#endif	/* __mpx_cpu_h__ */

/* eof */
