;*******************************************************************************
; DISCLAIMER
; This software is supplied by Renesas Electronics Corporation and is only
; intended for use with Renesas products. No other uses are authorized. This
; software is owned by Renesas Electronics Corporation and is protected under
; all applicable laws, including copyright laws.
; THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
; THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
; LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
; AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
; TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
; ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
; FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
; ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
; BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
; Renesas reserves the right, without notice, to make changes to this software
; and to discontinue the availability of this software. By using this software,
; you agree to the additional terms and conditions found by accessing the
; following link:
; http://www.renesas.com/disclaimer
; Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved
;*******************************************************************************

;   NOTE       : THIS IS A TYPICAL EXAMPLE. (R7F701503)

	; example of using eiint as table reference method
	USE_TABLE_REFERENCE_METHOD .set 1	;[libipcx] table reference method is mandatory

	; offset of processing module setting table element
	.OFFSET_ENTRY	.set	0

;-----------------------------------------------------------------------------
;	startup
;-----------------------------------------------------------------------------
	.section	".text", text
	.public	__start
	.align	2
__start:
	; jump to entry point of each PE
	stsr	0, r10, 2		; get HTCFG0

__start_PE2:
	jarl	_hdwinit_PE2, lp	; initialize hardware
$ifdef USE_TABLE_REFERENCE_METHOD
	jarl	_init_eiint, lp		; initialize exception
$endif
	mov	#_pm2_setting_table, r13
	ld.w	.OFFSET_ENTRY[r13], r10	; r10 <- #__start
	ldsr	r10, 2, 0		; set FEPC

	; apply PSW and jump to the application entry point
	feret

;-----------------------------------------------------------------------------
;	target dependence informations (specify values suitable to your system)
;-----------------------------------------------------------------------------
	; RAM address
	GLOBAL_RAM_ADDR 	.set	0xfef00000
	GLOBAL_RAM_END		.set	0xfef0bfff

	LOCAL_RAM_PE2_ADDR	.set	0xfe9d0000
	LOCAL_RAM_PE2_END	.set	0xfe9fffff

	; mutual exclusion variable
	MEV_ADDR	.set	0xfffeec00

;-----------------------------------------------------------------------------
;	hdwinit_PE2
;-----------------------------------------------------------------------------
	.section	".text", text
	.align	2
_hdwinit_PE2:
	mov	lp, r14			; save return address

	; clear Local RAM PE2
	mov	LOCAL_RAM_PE2_ADDR, r6
	mov	LOCAL_RAM_PE2_END, r7
	jarl	_zeroclr4, lp

	; wait for PE1
	mov	MEV_ADDR, r10
	st.w	r0, [r10]

.L.hdwinit_PE2.1:
	snooze
	set1	1, [r10]
	tst1	0, [r10]
	bz	.L.hdwinit_PE2.1

	mov	r14, lp
	jmp	[lp]

;-----------------------------------------------------------------------------
;	zeroclr4
;-----------------------------------------------------------------------------
	.align	2
_zeroclr4:
	br	.L.zeroclr4.2
.L.zeroclr4.1:
	st.w	r0, [r6]
	add	4, r6
.L.zeroclr4.2:
	cmp	r6, r7
	bh	.L.zeroclr4.1
	jmp	[lp]

$ifdef USE_TABLE_REFERENCE_METHOD
;-----------------------------------------------------------------------------
;	init_eiint
;-----------------------------------------------------------------------------
	; interrupt control register address
	ICBASE	.set	0xfffeea00

	.align	2
_init_eiint:
	; some inetrrupt channels use the table reference method.
	mov	ICBASE, r10		; get interrupt control register address
	set1	6, 0[r10]		; set INT0 as table reference
	set1	6, 2[r10]		; set INT1 as table reference
	set1	6, 4[r10]		; set INT2 as table reference
	
	clr1	7, 0[r10]		; clear INT0 MASK bit
	clr1	7, 2[r10]		; clear INT1 MASK bit
	clr1	7, 4[r10]		; clear INT2 MASK bit
	jmp	[lp]
$endif

;-----------------------------------------------------------------------------
;	processing module setting table
;-----------------------------------------------------------------------------
	.section	".const.pe2", const
	.public	_pm2_setting_table
	.align	4
_pm2_setting_table:
	.dw	#__start_PE2_only	; ENTRY ADDRESS

;-----------------------------------------------------------------------------
;	system stack
;-----------------------------------------------------------------------------
STACKSIZE	.set	0x200
	.section	".stack.bss", bss
	.align	4
	.ds	(STACKSIZE)
	.align	4
_stacktop:

;-----------------------------------------------------------------------------
;	section initialize table
;-----------------------------------------------------------------------------
	.section	".INIT_DSEC.const", const
	.align	4
	.dw	#__s.data,	#__e.data,	#__s.data.R

	.section	".INIT_BSEC.const", const
	.align	4
	.dw	#__s.bss,	#__e.bss

;-----------------------------------------------------------------------------
;	startup
;-----------------------------------------------------------------------------
	.section	".text", text
	.public	__start_PE2_only
	.align	2
__start_PE2_only:
	mov	#_stacktop, sp		;  set sp register
	mov	#__gp_data, gp		;  set gp register
	mov	#__ep_data, ep		;  set ep register

	jarl	_hdwinit, lp		;  initialize hardware

	mov	#__s.INIT_DSEC.const, r6
	mov	#__e.INIT_DSEC.const, r7
	mov	#__s.INIT_BSEC.const, r8
	mov	#__e.INIT_BSEC.const, r9
	jarl32	__INITSCT_RH, lp	;  initialize RAM area

	;[libipcx] INTBP vector including IPI handler of libipcx 
	mov	#__sEIINTTBL, r10
	ldsr	r10, 4, 1		; set INTBP
	
	; [libipcx] Enable MIR0

	ld.h	0xfffeea00[r0], r11	;EIC0
	andi	!(0x80), r11, r11
	st.h	r11, 0xfffeea00[r0]
	
	; set various flags to PSW via FEPSW

	stsr	5, r10, 0		; r10 <- PSW

	movhi	0x0001, r0, r11
	or	r11, r10		; enable FPU

	xori	0x0020, r10, r10	; enable interrupt

	movhi	0x4000, r0, r11
;	or	r11, r10		; supervisor mode -> user mode

	ldsr	r10, 3, 0		; FEPSW <- r10

	mov	#_exit_pe2, lp		; lp <- #_exit_pe2
	mov	#_main, r10
	ldsr	r10, 2, 0		; FEPC <- #_main

	; apply PSW and PC to start user mode
	feret

_exit_pe2:
	br	_exit_pe2			;  end of program
;-----------------------------------------------------------------------------
;	abort
;-----------------------------------------------------------------------------
	.public	_abort
	.align	2
_abort:
	br	_abort

;-----------------------------------------------------------------------------
;	dummy section
;-----------------------------------------------------------------------------
	.section	".data", data
.L.dummy.data:
	.section	".bss", bss
.L.dummy.bss:
	.section	".const", const
.L.dummy.const:
	.section	".text", text
.L.dummy.text:


	.section "RESET", text
	.align	512
	jr32	__start		; 0x00 RESET

	.align	16
	jr32	_Dummy		; 0x10 SYSERR

	.align	16
	jr32	_Dummy		; 0x20 HVTRAP

	.align	16
	jr32	_Dummy		; 0x30 FETRAP

	.align	16
	jr32	_Dummy_EI	; 0x40 TRAP0

	.align	16
	jr32	_Dummy_EI	; 0x50 TRAP1

	.align	16
	jr32	_Dummy		; 0x60 RIE

	.align	16
	jr32	_Dummy_EI	; 0x70 FPP/FPI

	.align	16
	jr32	_Dummy		; 0x80 UCPOP

	.align	16
	jr32	_Dummy		; 0x90 MIP/MDP/ITLBE/DTLBE

	.align	16
	jr32	_Dummy		; 0xa0 PIE

	.align	16
	jr32	_Dummy		; 0xb0 Debug

	.align	16
	jr32	_Dummy		; 0xc0 MAE

	.align	16
	jr32	_Dummy		; 0xd0 (R.F.U)

	.align	16
	jr32	_Dummy		; 0xe0 FENMI

	.align	16
	jr32	_Dummy 		; 0xf0 FEINT

	.align	16
	jr32	_Dummy_EI	; 0x100 INTn(priority0)

	.align	16
	jr32	_Dummy_EI	; 0x110 INTn(priority1)

	.align	16
	jr32	_Dummy_EI	; 0x120 INTn(priority2)

	.align	16
	jr32	_Dummy_EI	; 0x130 INTn(priority3)

	.align	16
	jr32	_Dummy_EI	; 0x140 INTn(priority4)

	.align	16
	jr32	_Dummy_EI	; 0x150 INTn(priority5)

	.align	16
	jr32	_Dummy_EI	; 0x160 INTn(priority6)

	.align	16
	jr32	_Dummy_EI	; 0x170 INTn(priority7)

	.align	16
	jr32	_Dummy_EI	; 0x180 INTn(priority8)

	.align	16
	jr32	_Dummy_EI	; 0x190 INTn(priority9)

	.align	16
	jr32	_Dummy_EI	; 0x1a0 INTn(priority10)

	.align	16
	jr32	_Dummy_EI	; 0x1b0 INTn(priority11)

	.align	16
	jr32	_Dummy_EI	; 0x1c0 INTn(priority12)

	.align	16
	jr32	_Dummy_EI	; 0x1d0 INTn(priority13)

	.align	16
	jr32	_Dummy_EI	; 0x1e0 INTn(priority14)

	.align	16
	jr32	_Dummy_EI	; 0x1f0 INTn(priority15)

	.align	4
_Dummy_EI:
	br	_Dummy_EI
	
	.align	4
_Dummy:
	br	_Dummy
