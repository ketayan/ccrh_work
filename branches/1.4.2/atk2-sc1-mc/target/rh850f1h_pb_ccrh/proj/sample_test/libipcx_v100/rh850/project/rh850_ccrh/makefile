# *****************************************************************************
# DISCLAIMER
# This software is supplied by Renesas Electronics Corporation and is only
# intended for use with Renesas products. No other uses are authorized. This
# software is owned by Renesas Electronics Corporation and is protected under
# all applicable laws, including copyright laws.
# THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
# THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
# LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
# AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
# TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
# ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
# FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
# ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
# BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
# Renesas reserves the right, without notice, to make changes to this software
# and to discontinue the availability of this software. By using this software,
# you agree to the additional terms and conditions found by accessing the
# following link:
# http://www.renesas.com/disclaimer
# Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved
# *****************************************************************************

CC-RHVER	= V1.02.00

TOPDIR		= ../..

CC-RHTOOLS	= /cygdrive/c/Program\ Files/Renesas\ Electronics/CS+/CC/CC-RH/$(CC-RHVER)/bin

CC	= $(CC-RHTOOLS)/ccrh
AS	= $(CC-RHTOOLS)/asrh
LD	= $(CC-RHTOOLS)/rlink
AR	= $(CC-RHTOOLS)/rlink
CPP	= $(CC-RHTOOLS)/ccrh -P
RM	= /bin/rm -f

INCDIR	= $(TOPDIR)/include

SYSDIR		= $(TOPDIR)/source

### CC-RH�p ###
CPUOPT	= -Xcommon=v850e3v5
CFLAGS	=
CPPFLAGS= -I$(INCDIR) \
	   -D__ASM__ \
	   -D__CC_RH__ -D__parameter_check__

### CC-RH�p ###
ASFLAGS = $(CPUOPT) -Xreg_mode=32 -Xcpu=g3m -Xep=callee \
	  -Xreserve_r2

ARFLAGS	= -form=library=s -nologo

SYSLIB		= $(TOPDIR)/library/rh850_ccrh/libipcx.lib

SYSCOREOBJS	= \
	ipir.obj ipir_ch0_handler.obj \
	ipir_ch1_handler.obj mev.obj msys.obj 

# ----

kernel	: $(SYSLIB)

clean	: 
	$(RM) $(SYSCOREOBJS) 
	$(RM) *.s m.r core

cleanall: clean
	$(RM) $(SYSLIB)

# ----

$(SYSLIB)	: $(SYSCOREOBJS) $(SYSLIBOBJS)
	$(AR) $(ARFLAGS) -output=$(SYSLIB) $(SYSCOREOBJS) $(SYSLIBOBJS)

#
#	system core
#

ipir.obj :	$(SYSDIR)/ipir.c
	$(CPP) $(CPPFLAGS) $(SYSDIR)/ipir.c -oipir.s
	$(AS) $(ASFLAGS) ipir.s

ipir_ch0_handler.obj :	$(SYSDIR)/ipir_ch0_handler.c
	$(CPP) $(CPPFLAGS) $(SYSDIR)/ipir_ch0_handler.c -oipir_ch0_handler.s
	$(AS) $(ASFLAGS) ipir_ch0_handler.s

ipir_ch1_handler.obj :	$(SYSDIR)/ipir_ch1_handler.c
	$(CPP) $(CPPFLAGS) $(SYSDIR)/ipir_ch1_handler.c -oipir_ch1_handler.s
	$(AS) $(ASFLAGS) ipir_ch1_handler.s

mev.obj :	$(SYSDIR)/mev.c
	$(CPP) $(CPPFLAGS) $(SYSDIR)/mev.c -omev.s
	$(AS) $(ASFLAGS) mev.s

msys.obj :	$(SYSDIR)/msys.c
	$(CPP) $(CPPFLAGS) $(SYSDIR)/msys.c -omsys.s
	$(AS) $(ASFLAGS) msys.s

# EOF
