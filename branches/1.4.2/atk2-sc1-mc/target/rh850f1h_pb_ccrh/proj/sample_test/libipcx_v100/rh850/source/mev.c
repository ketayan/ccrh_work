/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *				注意事項
 *
 *	本ソースコードはアセンブラ言語で記述されていますが
 *	アセンブルの前に、CC-RHコンパイラのプリプロセッサ機能を利用して
 *	プリプロセスを行う必要があります。
 *
 *	CC-RHのプリプロセッサに本ファイルを読み込ませるために、
 *	本ファイルは.C形式で提供しています。
 *
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

#include "libipcx_src.h"

m_GLOBL(_F_init)				/* 初期化 */
m_GLOBL(_F_lock)				/* 排他開始 */
m_GLOBL(_F_try)					/* 排他開始 (polling) */
m_GLOBL(_F_unlock)				/* 排他終了 */
m_GLOBL(_F_wait)				/* フラグ待ち */
m_GLOBL(_F_test)				/* フラグ確認 */
m_GLOBL(_F_set)					/* フラグ設定 */
m_GLOBL(_F_clear)				/* フラグ解除 */

m_GLOBL(_S_lock)				/* 割り込み処理を含む排他開始 */
m_GLOBL(_S_try)					/* 割り込み処理を含む排他開始 (polling) */
m_GLOBL(_S_unlock)				/* 割り込み処理を含む排他終了 */

m_GLOBL(_F_sync)				/* 複数PE間の同期(バリア同期) */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ F_init ]
 *	[ F_unlock ]
 *
 *	概要
 *		処理間排他/同期制御の初期化 (終了)
 *	文法
 *		int
 *		F_init ( int ID );
 *		F_unlock ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: 正常終了
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_F_init:
_F_unlock:

#ifdef	__parameter_check__
	cmp	0, r6				/* F_init, F_unlockの場合は0まで正常 */
	blt	.f_common.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_common.error
#endif	/* __parameter_check__ */

	cmp	mu4_ALL, r6
	be	.f_init.all

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

	clr1	r11, [r12]			/* 初期化(解放) */

	mov	0, r10
	jmp	[lp]

.f_init.all:

	st.w	r0, mpu4_MEV0[r0]
	st.w	r0, mpu4_MEV1[r0]

	mov	0, r10
	jmp	[lp]

#ifdef	__parameter_check__
.f_common.error:
	mov	(-1), r10
	jmp	[lp]
#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ F_lock ]
 *
 *	概要
 *		処理間排他の開始
 *	文法
 *		int
 *		F_lock ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: 正常終了
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_F_lock:

#ifdef	__parameter_check__
	cmp	MIN_MEV_ID, r6
	blt	.f_common.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_common.error
#endif	/* __parameter_check__ */

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

.f_lock.loop:
	set1	r11, [r12]			/* 排他 */

	bnz	.f_lock.loop

	mov	0, r10
	jmp	[lp]

#ifdef	__parameter_check__

#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ F_try ]
 *
 *	概要
 *		処理間排他の開始 (polling)
 *	文法
 *		int
 *		F_try ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: ロック獲得成功(正常終了)
 *		-1	: 指定したIDが範囲外
 *		-2	: ロック獲得失敗(異常終了)
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_F_try:

#ifdef	__parameter_check__
	cmp	MIN_MEV_ID, r6
	blt	.f_try.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_try.error
#endif	/* __parameter_check__ */

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

	set1	r11, [r12]			/* 排他 */

	bnz	.f_try.failed

	mov	0, r10
	jmp	[lp]

.f_try.failed:
	mov	(-2), r10
	jmp	[lp]

#ifdef	__parameter_check__
.f_try.error:
	mov	(-1), r10
	jmp	[lp]
#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ F_wait ]
 *
 *	概要
 *		同期制御の開始
 *	文法
 *		int
 *		F_wait ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		1
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_F_wait:

#ifdef	__parameter_check__
	cmp	MIN_MEV_ID, r6
	blt	.f_common.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_common.error
#endif	/* __parameter_check__ */

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

.f_wait.loop:
	tst1	r11, [r12]			/* 確認 */

	bz	.f_wait.loop

	mov	1, r10
	jmp	[lp]

#ifdef	__parameter_check__

#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ F_test ]
 *
 *	概要
 *		同期制御の確認
 *	文法
 *		int
 *		F_test ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: フラグ未設定
 *		1	: フラグ設定済み
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_F_test:

#ifdef	__parameter_check__
	cmp	MIN_MEV_ID, r6
	blt	.f_common.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_common.error
#endif	/* __parameter_check__ */

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

	tst1	r11, [r12]			/* 確認 */

	setfnz	r10
	jmp	[lp]

#ifdef	__parameter_check__

#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ F_set ]
 *
 *	概要
 *		同期制御の設定
 *	文法
 *		int
 *		F_set ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: セット前フラグ未設定
 *		1	: セット前フラグ設定済み
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_F_set:

#ifdef	__parameter_check__
	cmp	MIN_MEV_ID, r6
	blt	.f_common.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_common.error
#endif	/* __parameter_check__ */

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

	set1	r11, [r12]			/* 設定 */

	setfnz	r10
	jmp	[lp]

#ifdef	__parameter_check__

#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ F_clear ]
 *
 *	概要
 *		同期制御の設定
 *	文法
 *		int
 *		F_clear ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: クリア前フラグ未設定
 *		1	: クリア前フラグ設定済み
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_F_clear:

#ifdef	__parameter_check__
	cmp	MIN_MEV_ID, r6
	blt	.f_common.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_common.error
#endif	/* __parameter_check__ */

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

	clr1	r11, [r12]			/* 設定 */

	setfnz	r10
	jmp	[lp]

#ifdef	__parameter_check__

#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ S_lock ]
 *
 *	概要
 *		割り込みを含む処理間排他の開始
 *	文法
 *		int
 *		S_lock ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: 正常終了
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_S_lock:

#ifdef	__parameter_check__
	cmp	MIN_MEV_ID, r6
	blt	.f_common.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_common.error
#endif	/* __parameter_check__ */

	di

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

.s_lock.loop:
	set1	r11, [r12]			/* 排他 */

	bz	.s_lock.succeed
	
	ei
	syncp
	di

	br	.s_lock.loop

.s_lock.succeed:
	mov	0, r10
	jmp	[lp]

#ifdef	__parameter_check__

#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ S_try ]
 *
 *	概要
 *		割り込み処理を含む処理間排他の開始 (polling)
 *	文法
 *		int
 *		S_try ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: ロック獲得成功(正常終了)
 *		-1	: 指定したIDが範囲外
 *		-2	: ロック獲得失敗(異常終了)

 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_S_try:

#ifdef	__parameter_check__
	cmp	MIN_MEV_ID, r6
	blt	.f_try.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_try.error
#endif	/* __parameter_check__ */

	di

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

	set1	r11, [r12]			/* 排他 */

	bnz	.s_try.failed

	mov	0, r10
	jmp	[lp]

.s_try.failed:
	ei
	mov	(-2), r10
	jmp	[lp]

#ifdef	__parameter_check__

#endif	/* __parameter_check__ */



/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ S_unlock ]
 *
 *	概要
 *		割り込みを含む処理間排他の開始(ポーリング)
 *	文法
 *		int
 *		S_unlock ( int ID );
 *	入力パラメータ
 *		ID	: 資源番号 (1〜64)
 *	戻り値
 *		0	: 正常終了
 *		-1	: 指定したIDが範囲外
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_S_unlock:

#ifdef	__parameter_check__
	cmp	0, r6				/* S_unlockの場合は0まで正常 */
	blt	.f_common.error

	cmp	MAX_MEV_ID, r6
	bgt	.f_common.error
#endif	/* __parameter_check__ */

	cmp	mu4_ALL, r6
	be	.s_init.all

	add	(-1), r6
	andi	MASK_MOD_8, r6, r11		/* バイト(8bit)内オフセット */

	shr	SHIFT_DIV_8, r6
	mov	mpu4_MEV0, r12
	add	r6, r12				/* MEVアドレス */

	clr1	r11, [r12]			/* 初期化(解放) */

	ei
	mov	0, r10
	jmp	[lp]

.s_init.all:

	st.w	r0, mpu4_MEV0[r0]
	st.w	r0, mpu4_MEV1[r0]

	ei
	mov	0, r10
	jmp	[lp]

#ifdef	__parameter_check__

#endif	/* __parameter_check__ */


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ F_sync ]
 *
 *	概要
 *		複数PE間の同期(バリア同期)
 *	文法
 *		int
 *		F_sync ( PEnumber );
 *	入力パラメータ
 *		なし
 *	戻り値
 *		F_sync関数を呼び出した順番
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

	m_ALIGN(4)
_F_sync:
	mov	MEV2_BIT_GCOUNT, r11
	mov	ep, r12
	mov	mpu4_MEV2, ep

	/* gSenseを反転し、テンポラリ・レジスタにコピーする */
	sld.bu	u1_OFFSET_GSENSE[ep], r14	/* r14: mySense */
	mov	MEV2_UPDATE_GSENSE, r10
	xor	r10, r14			/* r14: mySense(反転後) */

.f_sync.gcountlock:
	set1	r11, [ep]			/* gCountの排他開始 */

	bnz	.f_sync.gcountlock		/* 既にgCountの操作が開始されている場合、無限ループ */

	sld.bu	u1_OFFSET_GCOUNT[ep], r10	/* r10: gCount */
	add	1, r10
	sst.b	r10, u1_OFFSET_GCOUNT[ep]

	clr1	r11, [ep]			/* gCountのunlock */

	cmp	r6, r10				/* gCountが引数のPE数と一致しているか */
	be	.f_sync.unlock_barrier

.f_sync.wait_loop:
	sld.bu	u1_OFFSET_GSENSE[ep], r13	/* r13: gSense */
	cmp	r14, r13
	bne	.f_sync.wait_loop

	br	.f_sync.finish_barrier

.f_sync.unlock_barrier:
	sst.b	r0, u1_OFFSET_GCOUNT[ep]
	sst.b	r14, u1_OFFSET_GSENSE[ep]	/* gSenseの反転 */

.f_sync.finish_barrier:
	mov	r12, ep
	jmp	[lp]

/* eof */
