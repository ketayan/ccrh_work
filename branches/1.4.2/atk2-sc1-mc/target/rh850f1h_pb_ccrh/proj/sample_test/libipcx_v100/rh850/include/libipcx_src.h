/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/


#ifndef	__libipcx_h__
#define	__libipcx_h__

#include "cpu.h"
#include "pfss.h"


/*
 *	アセンブリ言語命令定義
 */

#ifdef	__ASM__
#ifdef	__GHS__

#define	m_GLOBL(SYMBOL)			.globl	SYMBOL;	..lxtdaG.##SYMBOL##=:0
#define	m_ALIGN(NUM)			.align	NUM

#define	m_HI(ADR)			hi(ADR)	
#define	m_LO(ADR)			lo(ADR)
#define	m_NOT(BITS)			~(BITS)
#define	WORD(VALUE)			.word	VALUE

#endif	/* __GHS__ */

#ifdef	__CC_RH__

#define	m_GLOBL(SYMBOL)			.##public	SYMBOL
#define	m_ALIGN(NUM)			.##align	NUM

#define	m_HI(ADR)			highw1(ADR)	
#define	m_LO(ADR)			loww(ADR)
#define	m_NOT(BITS)			!(BITS)
#define	WORD(VALUE)			.dw	!VALUE

#endif	/* __CC_RH__ */

#endif	/* __ASM__ */


/*
 *	汎用マクロ
 */

#define	mu4_ALL				0

/*
 *	処理間排他/同期制御
 */

#define	MIN_MEV_ID	1
#define	MAX_MEV_ID	64

#define	MIN_FNCID	1
#define	MAX_FNCID	64		/* MEVのビット幅に依存するわけではないため、MAX_FNCIDは容易に変更可能 */

#define	MIN_INTID	1
#define	MAX_INTID	32

#define	MASK_MOD_8	0x0007
#define	SHIFT_DIV_8	3
#define	SIZE_WORD	2

/*
 *	PE間処理呼び出し
 */

#define	MEV_BIT_FUNC	0
#define	MEV_BIT_CALL	1
#define	MEV_BIT_REQUEST	2
#define	MEV_BIT_FUNCID	3

#define	MEV_FUNC	(1 << MEV_BIT_FUNC)
#define	MEV_CALL	(1 << MEV_BIT_CALL)
#define	MEV_REQUEST	(1 << MEV_BIT_REQUEST)
#define	MEV_FUNCID	(1 << MEV_BIT_FUNCID)


/*
 *	複数PE間のバリア同期
 */
#define	MEV2_BIT_GCOUNT		0
#define	MEV2_UPDATE_GSENSE	0x01

/* 以下はMEV2にアクセスする際に使用するオフセットマクロ */
#define	u1_OFFSET_GSENSE	0x01
#define	u1_OFFSET_GCOUNT	0x02

/*
 *	PE間割り込み処理
 */
#define	u4_IF_r29	0x00
#define	u4_IF_ep	0x04
#define	u4_IF_lp	0x08

#define	SIZE_EF		0x0c


#define	u4_IF_r1	0x00
#define	u4_IF_r2	0x04
#define	u4_IF_r6	0x08
#define	u4_IF_r7	0x0c
#define	u4_IF_r8	0x10
#define	u4_IF_r9	0x14
#define	u4_IF_r10	0x18
#define	u4_IF_r11	0x1c
#define	u4_IF_r12	0x20
#define	u4_IF_r13	0x24
#define	u4_IF_r14	0x28
#define	u4_IF_r15	0x2c
#define	u4_IF_r16	0x30
#define	u4_IF_r17	0x34
#define	u4_IF_r18	0x38
#define	u4_IF_r19	0x3c
#define	u4_IF_EIPC	0x40
#define	u4_IF_EIPSW	0x44
#define	u4_IF_CTPC	0x48
#define	u4_IF_CTPSW	0x4c

#define	SIZE_IF		0x50
#define	NUM_IF		0x14

/*
 *	PE番号取得
 */
#define	mu4_SHIFT_HALF		16
#define	mu2_HTCFG0H_PEID	0x0003	/* PEID: HTCFG0 bit16-18 */


#endif	/* __libipcx_h__ */

/* eof */
