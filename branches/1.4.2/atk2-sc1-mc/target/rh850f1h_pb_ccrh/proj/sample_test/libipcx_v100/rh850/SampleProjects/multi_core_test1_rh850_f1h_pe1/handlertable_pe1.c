/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#include <libipcx.h>
#include "sample.h"

#pragma section .const.pe1

const FP functiontable_PE1[64];
const FP interrupttable_PE1[32];

const FP * const adr_functiontable = functiontable_PE1;
const FP * const adr_interrupttable = interrupttable_PE1;

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ functiontable_PE1 ]
 *	P_call_id, またはP_request_idにより他PEから呼び出し可能な
 *	関数テーブル
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

const FP
functiontable_PE1[64] =
{
	(FP)&call_func_PE1,		/* funcid = 1  */
	(FP)&dummy_func_PE1,		/* funcid = 2  */
	(FP)&cmn_func_add,		/* funcid = 3  */
	(FP)&dummy_func_PE1,		/* funcid = 4  */
	(FP)&dummy_func_PE1,		/* funcid = 5  */
	(FP)&dummy_func_PE1,		/* funcid = 6  */
	(FP)&dummy_func_PE1,		/* funcid = 7  */
	(FP)&dummy_func_PE1,		/* funcid = 8  */
	(FP)&dummy_func_PE1,		/* funcid = 9  */
	(FP)&dummy_func_PE1,		/* funcid = 10 */
	(FP)&dummy_func_PE1,		/* funcid = 11 */
	(FP)&dummy_func_PE1,		/* funcid = 12 */
	(FP)&dummy_func_PE1,		/* funcid = 13 */
	(FP)&dummy_func_PE1,		/* funcid = 14 */
	(FP)&dummy_func_PE1,		/* funcid = 15 */
	(FP)&dummy_func_PE1,		/* funcid = 16 */
	(FP)&dummy_func_PE1,		/* funcid = 17 */
	(FP)&dummy_func_PE1,		/* funcid = 18 */
	(FP)&dummy_func_PE1,		/* funcid = 19 */
	(FP)&dummy_func_PE1,		/* funcid = 20 */
	(FP)&dummy_func_PE1,		/* funcid = 21 */
	(FP)&dummy_func_PE1,		/* funcid = 22 */
	(FP)&dummy_func_PE1,		/* funcid = 23 */
	(FP)&dummy_func_PE1,		/* funcid = 24 */
	(FP)&dummy_func_PE1,		/* funcid = 25 */
	(FP)&dummy_func_PE1,		/* funcid = 26 */
	(FP)&dummy_func_PE1,		/* funcid = 27 */
	(FP)&dummy_func_PE1,		/* funcid = 28 */
	(FP)&dummy_func_PE1,		/* funcid = 29 */
	(FP)&dummy_func_PE1,		/* funcid = 30 */
	(FP)&dummy_func_PE1,		/* funcid = 31 */
	(FP)&dummy_func_PE1,		/* funcid = 32 */
	(FP)&dummy_func_PE1,		/* funcid = 33 */
	(FP)&dummy_func_PE1,		/* funcid = 34 */
	(FP)&dummy_func_PE1,		/* funcid = 35 */
	(FP)&dummy_func_PE1,		/* funcid = 36 */
	(FP)&dummy_func_PE1,		/* funcid = 37 */
	(FP)&dummy_func_PE1,		/* funcid = 38 */
	(FP)&dummy_func_PE1,		/* funcid = 39 */
	(FP)&dummy_func_PE1,		/* funcid = 40 */
	(FP)&dummy_func_PE1,		/* funcid = 41 */
	(FP)&dummy_func_PE1,		/* funcid = 42 */
	(FP)&dummy_func_PE1,		/* funcid = 43 */
	(FP)&dummy_func_PE1,		/* funcid = 44 */
	(FP)&dummy_func_PE1,		/* funcid = 45 */
	(FP)&dummy_func_PE1,		/* funcid = 46 */
	(FP)&dummy_func_PE1,		/* funcid = 47 */
	(FP)&dummy_func_PE1,		/* funcid = 48 */
	(FP)&dummy_func_PE1,		/* funcid = 49 */
	(FP)&dummy_func_PE1,		/* funcid = 50 */
	(FP)&dummy_func_PE1,		/* funcid = 51 */
	(FP)&dummy_func_PE1,		/* funcid = 52 */
	(FP)&dummy_func_PE1,		/* funcid = 53 */
	(FP)&dummy_func_PE1,		/* funcid = 54 */
	(FP)&dummy_func_PE1,		/* funcid = 55 */
	(FP)&dummy_func_PE1,		/* funcid = 56 */
	(FP)&dummy_func_PE1,		/* funcid = 57 */
	(FP)&dummy_func_PE1,		/* funcid = 58 */
	(FP)&dummy_func_PE1,		/* funcid = 59 */
	(FP)&dummy_func_PE1,		/* funcid = 60 */
	(FP)&dummy_func_PE1,		/* funcid = 61 */
	(FP)&dummy_func_PE1,		/* funcid = 62 */
	(FP)&dummy_func_PE1,		/* funcid = 63 */
	(FP)&dummy_func_PE1		/* funcid = 64 */
};

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *
 *
 *	[ interrupttable_PE1 ]
 *	P_raise_handlerにより他PEから呼び出し可能な
 *	割り込みハンドラのテーブル
 *
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

const FP
interrupttable_PE1[32] =
{
	(FP)&dummy_intfunc_PE1,		/* intid = 1  */
	(FP)&dummy_intfunc_PE1,		/* intid = 2  */
	(FP)&dummy_intfunc_PE1,		/* intid = 3  */
	(FP)&dummy_intfunc_PE1,		/* intid = 4  */
	(FP)&dummy_intfunc_PE1,		/* intid = 5  */
	(FP)&dummy_intfunc_PE1,		/* intid = 6  */
	(FP)&dummy_intfunc_PE1,		/* intid = 7  */
	(FP)&dummy_intfunc_PE1,		/* intid = 8  */
	(FP)&dummy_intfunc_PE1,		/* intid = 9  */
	(FP)&dummy_intfunc_PE1,		/* intid = 10 */
	(FP)&dummy_intfunc_PE1,		/* intid = 11 */
	(FP)&dummy_intfunc_PE1,		/* intid = 12 */
	(FP)&dummy_intfunc_PE1,		/* intid = 13 */
	(FP)&dummy_intfunc_PE1,		/* intid = 14 */
	(FP)&dummy_intfunc_PE1,		/* intid = 15 */
	(FP)&dummy_intfunc_PE1,		/* intid = 16 */
	(FP)&dummy_intfunc_PE1,		/* intid = 17 */
	(FP)&dummy_intfunc_PE1,		/* intid = 18 */
	(FP)&dummy_intfunc_PE1,		/* intid = 19 */
	(FP)&dummy_intfunc_PE1,		/* intid = 20 */
	(FP)&dummy_intfunc_PE1,		/* intid = 21 */
	(FP)&dummy_intfunc_PE1,		/* intid = 22 */
	(FP)&dummy_intfunc_PE1,		/* intid = 23 */
	(FP)&dummy_intfunc_PE1,		/* intid = 24 */
	(FP)&dummy_intfunc_PE1,		/* intid = 25 */
	(FP)&dummy_intfunc_PE1,		/* intid = 26 */
	(FP)&dummy_intfunc_PE1,		/* intid = 27 */
	(FP)&dummy_intfunc_PE1,		/* intid = 28 */
	(FP)&dummy_intfunc_PE1,		/* intid = 29 */
	(FP)&dummy_intfunc_PE1,		/* intid = 30 */
	(FP)&dummy_intfunc_PE1,		/* intid = 31 */
	(FP)&dummy_intfunc_PE1		/* intid = 32 */
};

/* eof */
