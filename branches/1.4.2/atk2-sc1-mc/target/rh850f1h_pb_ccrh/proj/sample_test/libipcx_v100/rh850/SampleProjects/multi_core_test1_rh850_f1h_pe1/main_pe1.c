/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#include <stdlib.h>
#include <libipcx.h>
#include "sample.h"

int	local_counter = 0;
int	call_counter = 0;

void
main(void)
{
	int	count;
	int	time;

	time	= S_getPE();
	
	F_init(ID_F_ALL);
	F_sync(2);
	
	while (1) {

		F_lock(ID_F_SEM1);

		count = cmn_counter;
		delay();

		time = rand() % 10;
		cmn_counter = count + time;

		F_unlock(ID_F_SEM1);

		F_set(ID_F_SEM2);

		local_counter = (int)P_call_id(ID_PE2, ID_FUNC3, local_counter, time);
		while(local_counter == CALL_ERROR) {}

		delay();

		P_raise_handler(ID_PE2, ID_INT1);

	}
}

int
call_func_PE1(void){		// ID_FUNC1
	call_counter++;
	return(call_counter);
}

/* eof */