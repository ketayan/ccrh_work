/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2013, 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

#ifndef	__mpx_pfss_h__
#define	__mpx_pfss_h__


/*
 *	PE間割り込み要求レジスタ
 */

#define	mpu4_IPIR_CH0	0xfffeec80
#define	mpu2_IPIR_CH0L	0xfffeec80
#define	mpu2_IPIR_CH0LL	0xfffeec80
#define	mpu2_IPIR_CH0LH	0xfffeec81
#define	mpu2_IPIR_CH0H	0xfffeec82
#define	mpu2_IPIR_CH0HL	0xfffeec82
#define	mpu2_IPIR_CH0HH	0xfffeec83

#define	mpu4_IPIR_CH1	0xfffeec84
#define	mpu2_IPIR_CH1L	0xfffeec84
#define	mpu2_IPIR_CH1LL	0xfffeec84
#define	mpu2_IPIR_CH1LH	0xfffeec85
#define	mpu2_IPIR_CH1H	0xfffeec86
#define	mpu2_IPIR_CH1HL	0xfffeec86
#define	mpu2_IPIR_CH1HH	0xfffeec87

#define	mpu4_IPIR_CH2	0xfffeec88
#define	mpu2_IPIR_CH2L	0xfffeec88
#define	mpu2_IPIR_CH2LL	0xfffeec88
#define	mpu2_IPIR_CH2LH	0xfffeec89
#define	mpu2_IPIR_CH2H	0xfffeec8a
#define	mpu2_IPIR_CH2HL	0xfffeec8a
#define	mpu2_IPIR_CH2HH	0xfffeec8b

#define	mpu4_IPIR_CH3	0xfffeec8c
#define	mpu2_IPIR_CH3L	0xfffeec8c
#define	mpu2_IPIR_CH3LL	0xfffeec8c
#define	mpu2_IPIR_CH3LH	0xfffeec8d
#define	mpu2_IPIR_CH3H	0xfffeec8e
#define	mpu2_IPIR_CH3HL	0xfffeec8e
#define	mpu2_IPIR_CH3HH	0xfffeec8f


/*
 *	相互排除変数レジスタ
 */

#define	mpu4_MEV0	0xfffeec00
#define	mpu2_MEV0L	0xfffeec00
#define	mpu1_MEV0LL	0xfffeec00
#define	mpu1_MEV0LH	0xfffeec01
#define	mpu2_MEV0H	0xfffeec02
#define	mpu1_MEV0HL	0xfffeec02
#define	mpu1_MEV0HH	0xfffeec03

#define	mpu4_MEV1	0xfffeec04
#define	mpu2_MEV1L	0xfffeec04
#define	mpu1_MEV1LL	0xfffeec04
#define	mpu1_MEV1LH	0xfffeec05
#define	mpu2_MEV1H	0xfffeec06
#define	mpu1_MEV1HL	0xfffeec06
#define	mpu1_MEV1HH	0xfffeec07

#define	mpu4_MEV2	0xfffeec08
#define	mpu2_MEV2L	0xfffeec08
#define	mpu1_MEV2LL	0xfffeec08
#define	mpu1_MEV2LH	0xfffeec09
#define	mpu2_MEV2H	0xfffeec0a
#define	mpu1_MEV2HL	0xfffeec0a
#define	mpu1_MEV2HH	0xfffeec0b

#define	mpu4_MEV3	0xfffeec0c
#define	mpu2_MEV3L	0xfffeec0c
#define	mpu1_MEV3LL	0xfffeec0c
#define	mpu1_MEV3LH	0xfffeec0d
#define	mpu2_MEV3H	0xfffeec0e
#define	mpu1_MEV3HL	0xfffeec0e
#define	mpu1_MEV3HH	0xfffeec0f

#define	mpu4_MEV4	0xfffeec10
#define	mpu2_MEV4L	0xfffeec10
#define	mpu1_MEV4LL	0xfffeec10
#define	mpu1_MEV4LH	0xfffeec11
#define	mpu2_MEV4H	0xfffeec12
#define	mpu1_MEV4HL	0xfffeec12
#define	mpu1_MEV4HH	0xfffeec13

#define	mpu4_MEV5	0xfffeec14
#define	mpu2_MEV5L	0xfffeec14
#define	mpu1_MEV5LL	0xfffeec14
#define	mpu1_MEV5LH	0xfffeec15
#define	mpu2_MEV5H	0xfffeec16
#define	mpu1_MEV5HL	0xfffeec16
#define	mpu1_MEV5HH	0xfffeec17

#define	mpu4_MEV6	0xfffeec18
#define	mpu2_MEV6L	0xfffeec18
#define	mpu1_MEV6LL	0xfffeec18
#define	mpu1_MEV6LH	0xfffeec19
#define	mpu2_MEV6H	0xfffeec1a
#define	mpu1_MEV6HL	0xfffeec1a
#define	mpu1_MEV6HH	0xfffeec1b

#define	mpu4_MEV7	0xfffeec1c
#define	mpu2_MEV7L	0xfffeec1c
#define	mpu1_MEV7LL	0xfffeec1c
#define	mpu1_MEV7LH	0xfffeec1d
#define	mpu2_MEV7H	0xfffeec1e
#define	mpu1_MEV7HL	0xfffeec1e
#define	mpu1_MEV7HH	0xfffeec1f


#endif	/* __mpx_pfss_h__ */

/* eof */
