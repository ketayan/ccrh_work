

#ifndef PARALLEL_BLSMCONTROLLER_TRACE_H
#define PARALLEL_BLSMCONTROLLER_TRACE_H

#include "logtrace/trace_config.h"

const char step_start_str[]="task %d becomes RUNNABLE.";
const char step_end_str[]="task %d becomes DORMANT.";

#define LOG_START_COMMENT(num)  trace_2(LOG_TYPE_COMMENT , (const uintptr) step_start_str, num);
#define LOG_END_COMMENT(num)  trace_2(LOG_TYPE_COMMENT , (const uintptr) step_end_str, num);


#define     TASK_BlsmControl_Core0_1_0_Task_LOOP_TOP LOG_START_COMMENT(10)
#define     TASK_BlsmControl_Core0_1_0_Task_BEFORE_SLEEP LOG_END_COMMENT(10)

#define     TASK_BlsmControl_Core1_1_0_Task_LOOP_TOP LOG_START_COMMENT(11)
#define     TASK_BlsmControl_Core1_1_0_Task_BEFORE_SLEEP LOG_END_COMMENT(11)

#define     TASK_BlsmControl_Core1_100_0_Task_LOOP_TOP LOG_START_COMMENT(12)
#define     TASK_BlsmControl_Core1_100_0_Task_BEFORE_SLEEP LOG_END_COMMENT(12)

#endif /* PARALLEL_BLSMCONTROLLER_TRACE_H */