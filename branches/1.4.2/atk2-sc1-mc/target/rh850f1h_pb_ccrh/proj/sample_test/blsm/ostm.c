/*****************************************************/
/*                                                   */
/*  FILE        :counter.c                              */
/*                                                   */
/*****************************************************/
#include "Os.h"
#include "t_syslog.h"
#include "t_stdlib.h"
#include "sysmod/serial.h"
#include "sysmod/syslog.h"
#include "sysmod/banner.h"
#include "target_sysmod.h"
#include "target_serial.h"

#include "rh850_f1h.h"

#include "ostm.h"

void ostm0_init(void);
void ostm0_start(void);
void ostm0_stop(void);
unsigned int ostm0_getcnt(void);

void ostm5_init(void);
void ostm5_start(void);
void ostm5_stop(void);
unsigned int ostm5_getcnt(void);


void ostm1_init(void);
void ostm1_start(void);
void ostm1_stop(void);
unsigned int ostm1_getcnt(void);


/*
 ソフトウェアカウンタ:OSTM0を使用
*/
void swcnt_init(void)
{
  ostm0_init(); //周期:TIMER_CLOCK
  ostm5_init();
}

void swcnt_start(void)
{
  ostm0_start();
  ostm5_start();
}

void swcnt_stop(void)
{
  ostm0_stop();
  ostm5_stop();
}

ISR (SwCntTimerHdr0)
{
  StatusType  ercd;

  /*
   *  カウンタ加算通知処理実行
   */
  ercd = IncrementCounter(SwCnt0);

  /* エラーリターンの場合はシャットダウン */
  if (ercd != E_OK)
    ShutdownAllCores(ercd);
}

ISR (SwCntTimerHdr1)
{
  StatusType  ercd;

  /*
   *  カウンタ加算通知処理実行
   */
  ercd = IncrementCounter(SwCnt1);

  /* エラーリターンの場合はシャットダウン */
  if (ercd != E_OK)
    ShutdownAllCores(ercd);
}


/*
 時間評価用カウンタ:OSTM1を使用
*/
void mesurecnt_init(void)
{
  ostm1_init(); //周波数:60[MHz]
  ostm1_start();
}

unsigned int get_tim_utime(void)
{
  return( ostm1_getcnt()/6 );
}




void ostm0_init(void)
{
  sil_wrw_mem((void *) (OSTM0_BASE + OSTM_CMP_W), (uint32) SwCnt0_TIMER_CLOCK);
  sil_wrb_mem((void *) (OSTM0_BASE + OSTM_CTL_B), 0x01);
}

void ostm0_start(void)
{
  sil_wrb_mem((void *) (OSTM0_BASE + OSTM_TS_B), 0x01);
}

void ostm0_stop(void)
{
  sil_wrb_mem((void *) (OSTM0_BASE + OSTM_TT_B), 0x01);
}

unsigned int ostm0_getcnt(void)
{
  return( sil_rew_mem((void *) (OSTM0_BASE + OSTM_CNT_W)) );
}


void ostm5_init(void)
{
  sil_wrw_mem((void *) (OSTM5_BASE + OSTM_CMP_W), (uint32) SwCnt0_TIMER_CLOCK);
  sil_wrb_mem((void *) (OSTM5_BASE + OSTM_CTL_B), 0x01);
}

void ostm5_start(void)
{
  sil_wrb_mem((void *) (OSTM5_BASE + OSTM_TS_B), 0x01);
}

void ostm5_stop(void)
{
  sil_wrb_mem((void *) (OSTM5_BASE + OSTM_TT_B), 0x01);
}

unsigned int ostm5_getcnt(void)
{
  return( sil_rew_mem((void *) (OSTM5_BASE + OSTM_CNT_W)) );
}




void ostm1_init(void)
{
  sil_wrw_mem((void *) (OSTM1_BASE + OSTM_CMP_W), 0x00000000);
  sil_wrb_mem((void *) (OSTM1_BASE + OSTM_CTL_B), 0x02);
}

void ostm1_start(void)
{
  sil_wrb_mem((void *) (OSTM1_BASE + OSTM_TS_B), 0x01);
}

void ostm1_stop(void)
{
  sil_wrb_mem((void *) (OSTM1_BASE + OSTM_TT_B), 0x01);
}

unsigned int ostm1_getcnt(void)
{
  return( sil_rew_mem((void *) (OSTM1_BASE + OSTM_CNT_W)) );
}
