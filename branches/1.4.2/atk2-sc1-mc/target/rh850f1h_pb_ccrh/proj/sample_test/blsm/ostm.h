/*****************************************************/
/*                                                   */
/*  FILE        :ostm.h                              */
/*                                                   */
/*****************************************************/
#ifndef _OSTM_H_
#define _OSTM_H_

/*
 *  タイマのカウント周期
 */
#define SwCnt0_TIMER_CLOCK UINT_C(6000)  //100usec
#define SwCnt0_TIMER_CLOCK_HZ   60000000/SwCnt0_TIMER_CLOCK

#define SwCnt0_TICK_FOR_10MS SwCnt0_TIMER_CLOCK_HZ/100

/*
 ソフトウェアカウンタ:OSTM0を使用
*/
void swcnt_init(void);
void swcnt_start(void);
void swcnt_stop(void);

/*
 時間評価用カウンタ:OSTM1を使用
*/
void mesurecnt_init(void);
unsigned int get_tim_utime(void);

#endif /* _OSTM_H_ */