/* Os_Lcfg.c */
#include "kernel/kernel_int.h"
#include "Os_Lcfg.h"

#ifndef TOPPERS_EMPTY_LABEL
#define TOPPERS_EMPTY_LABEL(x, y) x y[0]
#endif

/*
 *  Default Definitions of Trace Log Macros
 */

#ifndef LOG_ISR_ENTER
#define LOG_ISR_ENTER(isrid)
#endif /* LOG_ISR_ENTER */

#ifndef LOG_ISR_LEAVE
#define LOG_ISR_LEAVE(isrid)
#endif /* LOG_ISR_LEAVE */

/*
 *  Include Directives (#include)
 */

#include "BlsmControl-app.h"
#include "rlin3x.h"
#include "ostm.h"

const AlarmType					kernel_tnum_alarm				= TNUM_ALARM;
const CounterType				kernel_tnum_counter			= TNUM_COUNTER;
const CounterType				kernel_tnum_hardcounter		= TNUM_HARDCOUNTER;
const ISRType					kernel_tnum_isr2				= TNUM_ISR2;
const ISRType					kernel_tnum_ici				= TNUM_ICI;
const ResourceType				kernel_tnum_stdresource		= TNUM_STD_RESOURCE;
const TaskType					kernel_tnum_task				= TNUM_TASK;
const TaskType					kernel_tnum_exttask			= TNUM_EXTTASK;
const AppModeType				kernel_tnum_appmode			= TNUM_APP_MODE;
const ScheduleTableType			kernel_tnum_scheduletable		= TNUM_SCHEDULETABLE;
const ScheduleTableType			kernel_tnum_implscheduletable	= TNUM_IMPLSCHEDULETABLE;
const ApplicationType			kernel_tnum_osap				= TNUM_OSAP;
const SpinlockIdType			kernel_tnum_spinlock			= TNUM_SPINLOCK;


/****** CCB ******/

extern CCB kernel_core0_ccb __attribute__((section(".pe1_lram")));
CCB kernel_core0_ccb = {0U, FALSE, 0U, 0U, 0U, 0U, 0U};
extern CCB kernel_core1_ccb __attribute__((section(".pe2_lram")));
CCB kernel_core1_ccb = {0U, FALSE, 0U, 0U, 0U, 0U, 0U};


CCB * const kernel_p_ccb_table[TotalNumberOfCores] = {
	&kernel_core0_ccb,
	&kernel_core1_ccb
};


/****** Object TASK ******/

static StackType kernel_stack_BlsmControl_Core0_1_0_Task[COUNT_STK_T(5000U)] __attribute__((section(".pe1_lram")));
static StackType kernel_stack_BlsmControl_Core1_1_0_Task[COUNT_STK_T(5000U)] __attribute__((section(".pe2_lram")));
static StackType kernel_stack_BlsmControl_Core1_100_0_Task[COUNT_STK_T(5000U)] __attribute__((section(".pe2_lram")));
static StackType kernel_shared_stack_2[COUNT_STK_T(2048U)] __attribute__((section(".pe1_lram")));
static StackType kernel_shared_stack_3[COUNT_STK_T(2048U)] __attribute__((section(".pe1_lram")));

const TINIB kernel_tinib_table[TNUM_TASK] = {
	{
		&TASKNAME(BlsmControl_Core0_1_0_Task),
		ROUND_STK_T(5000U),
		kernel_stack_BlsmControl_Core0_1_0_Task,
		&(kernel_osapinib_table[BlsmControl_OSAP_CORE0]),
		0,
		0,
		(1U) - 1U,
		0x00000001U,
		&kernel_core0_ccb
	},
	{
		&TASKNAME(BlsmControl_Core1_1_0_Task),
		ROUND_STK_T(5000U),
		kernel_stack_BlsmControl_Core1_1_0_Task,
		&(kernel_osapinib_table[BlsmControl_OSAP_CORE1]),
		0,
		0,
		(1U) - 1U,
		0x00000001U,
		&kernel_core1_ccb
	},
	{
		&TASKNAME(BlsmControl_Core1_100_0_Task),
		ROUND_STK_T(5000U),
		kernel_stack_BlsmControl_Core1_100_0_Task,
		&(kernel_osapinib_table[BlsmControl_OSAP_CORE1]),
		1,
		1,
		(1U) - 1U,
		0x00000001U,
		&kernel_core1_ccb
	},
	{
		&TASKNAME(OperationManagerTask),
		ROUND_STK_T(2048U),
		kernel_shared_stack_2,
		&(kernel_osapinib_table[BlsmControl_OSAP_CORE0]),
		13,
		13,
		(1U) - 1U,
		0x00000000U,
		&kernel_core0_ccb
	},
	{
		&TASKNAME(DriveManagerTask),
		ROUND_STK_T(2048U),
		kernel_shared_stack_3,
		&(kernel_osapinib_table[BlsmControl_OSAP_CORE0]),
		12,
		12,
		(1U) - 1U,
		0x00000000U,
		&kernel_core0_ccb
	},
	{
		&TASKNAME(BodyControlTask),
		ROUND_STK_T(2048U),
		kernel_shared_stack_2,
		&(kernel_osapinib_table[BlsmControl_OSAP_CORE0]),
		13,
		13,
		(1U) - 1U,
		0x00000000U,
		&kernel_core0_ccb
	}
};

TCB kernel_tcb_BlsmControl_Core0_1_0_Task __attribute__((section(".pe1_lram")));
TCB kernel_tcb_BlsmControl_Core1_1_0_Task __attribute__((section(".pe2_lram")));
TCB kernel_tcb_BlsmControl_Core1_100_0_Task __attribute__((section(".pe2_lram")));
TCB kernel_tcb_OperationManagerTask __attribute__((section(".pe1_lram")));
TCB kernel_tcb_DriveManagerTask __attribute__((section(".pe1_lram")));
TCB kernel_tcb_BodyControlTask __attribute__((section(".pe1_lram")));

TCB * const kernel_p_tcb_table[TNUM_TASK] ={
	&kernel_tcb_BlsmControl_Core0_1_0_Task,
	&kernel_tcb_BlsmControl_Core1_1_0_Task,
	&kernel_tcb_BlsmControl_Core1_100_0_Task,
	&kernel_tcb_OperationManagerTask,
	&kernel_tcb_DriveManagerTask,
	&kernel_tcb_BodyControlTask
};

/****** Object COUNTER ******/

const CNTINIB kernel_cntinib_table[TNUM_COUNTER] = {
	{ 99999U, (99999U * 2U) + 1U, 1U, 1U, 0, &(kernel_osapinib_table[BlsmControl_OSAP_CORE0]) },
	{ 99999U, (99999U * 2U) + 1U, 1U, 1U, 1, &(kernel_osapinib_table[BlsmControl_OSAP_CORE1]) }
};

CNTCB kernel_cntcb_SwCnt0 __attribute__((section(".pe1_lram")));
CNTCB kernel_cntcb_SwCnt1 __attribute__((section(".pe2_lram")));

CNTCB * const kernel_p_cntcb_table[TNUM_COUNTER] = {
	&kernel_cntcb_SwCnt0,
	&kernel_cntcb_SwCnt1
};
TOPPERS_EMPTY_LABEL(const HWCNTINIB, kernel_hwcntinib_table);


/****** Object ALARM ******/

static void
activate_alarm_1(void);
static void
activate_alarm_1(void)
{
	(void) kernel_activate_task_action(OperationManagerTask);
}

static void
activate_alarm_2(void);
static void
activate_alarm_2(void)
{
	(void) kernel_activate_task_action(DriveManagerTask);
}

static void
activate_alarm_3(void);
static void
activate_alarm_3(void)
{
	(void) kernel_activate_task_action(BodyControlTask);
}


static void
setevent_alarm_5(void);
static void
setevent_alarm_5(void)
{
	(void) kernel_set_event_action(BlsmControl_Core0_1_0_Task, BlsmControl_WakeupEvent);
}

static void
setevent_alarm_6(void);
static void
setevent_alarm_6(void)
{
	(void) kernel_set_event_action(BlsmControl_Core1_1_0_Task, BlsmControl_WakeupEvent);
}

static void
setevent_alarm_7(void);
static void
setevent_alarm_7(void)
{
	(void) kernel_set_event_action(BlsmControl_Core1_100_0_Task, BlsmControl_WakeupEvent);
}

const ALMINIB kernel_alminib_table[TNUM_ALARM] = {
	{ &kernel_cntcb_SwCnt0, &activate_alarm_1, 0x00000003U, 100U, 100U, (ACTIVATETASK & CALLBACK) | ABSOLUTE, &(kernel_osapinib_table[BlsmControl_OSAP_CORE0]) },
	{ &kernel_cntcb_SwCnt0, &activate_alarm_2, 0x00000003U, 200U, 200U, (ACTIVATETASK & CALLBACK) | ABSOLUTE, &(kernel_osapinib_table[BlsmControl_OSAP_CORE0]) },
	{ &kernel_cntcb_SwCnt0, &activate_alarm_3, 0x00000001U, 1000U, 1000U, (ACTIVATETASK & CALLBACK) | ABSOLUTE, &(kernel_osapinib_table[BlsmControl_OSAP_CORE0]) },
	{ &kernel_cntcb_SwCnt0, ALARMCALLBACKNAME(BuzzerControlCycAlarmCb), 0x00000001U, 1U, 1U, (CALLBACK & CALLBACK) | ABSOLUTE, &(kernel_osapinib_table[BlsmControl_OSAP_CORE0]) },
	{ &kernel_cntcb_SwCnt0, &setevent_alarm_5, 0x00000000U, 0U, 0U, SETEVENT & CALLBACK, &(kernel_osapinib_table[BlsmControl_OSAP_CORE0]) },
	{ &kernel_cntcb_SwCnt1, &setevent_alarm_6, 0x00000000U, 0U, 0U, SETEVENT & CALLBACK, &(kernel_osapinib_table[BlsmControl_OSAP_CORE1]) },
	{ &kernel_cntcb_SwCnt1, &setevent_alarm_7, 0x00000000U, 0U, 0U, SETEVENT & CALLBACK, &(kernel_osapinib_table[BlsmControl_OSAP_CORE1]) }
};

ALMCB kernel_almcb_OperationManagerCycAlarm __attribute__((section(".pe1_lram")));
ALMCB kernel_almcb_DriveManagerCycAlarm __attribute__((section(".pe1_lram")));
ALMCB kernel_almcb_BodyControlCycAlarm __attribute__((section(".pe1_lram")));
ALMCB kernel_almcb_BuzzerControlCycAlarm __attribute__((section(".pe1_lram")));
ALMCB kernel_almcb_BlsmControl_Core0_1_0_Alarm __attribute__((section(".pe1_lram")));
ALMCB kernel_almcb_BlsmControl_Core1_1_0_Alarm __attribute__((section(".pe2_lram")));
ALMCB kernel_almcb_BlsmControl_Core1_100_0_Alarm __attribute__((section(".pe2_lram")));

ALMCB * const kernel_p_almcb_table[TNUM_ALARM] = {
	&kernel_almcb_OperationManagerCycAlarm,
	&kernel_almcb_DriveManagerCycAlarm,
	&kernel_almcb_BodyControlCycAlarm,
	&kernel_almcb_BuzzerControlCycAlarm,
	&kernel_almcb_BlsmControl_Core0_1_0_Alarm,
	&kernel_almcb_BlsmControl_Core1_1_0_Alarm,
	&kernel_almcb_BlsmControl_Core1_100_0_Alarm
};

/****** Object SCHEDULETABLE ******/


TOPPERS_EMPTY_LABEL(const SCHTBLINIB, kernel_schtblinib_table);
TOPPERS_EMPTY_LABEL(SCHTBLCB * const, kernel_p_schtblcb_table);


/****** Object RESOURCE ******/

TOPPERS_EMPTY_LABEL(const RESINIB, kernel_resinib_table);
TOPPERS_EMPTY_LABEL(RESCB * const, kernel_p_rescb_table);


/****** Object ISR(include InterCoreInterrupts) ******/


const ISRINIB kernel_isrinib_table[TNUM_ISR2] = {
	{
		&(kernel_intinib_table[RLIN3x_TX_ISR]), &(kernel_osapinib_table[BlsmControl_OSAP_CORE0])
	},
	{
		&(kernel_intinib_table[RLIN3x_RX_ISR]), &(kernel_osapinib_table[BlsmControl_OSAP_CORE0])
	},
	{
		&(kernel_intinib_table[SwCntTimerHdr0]), &(kernel_osapinib_table[BlsmControl_OSAP_CORE0])
	},
	{
		&(kernel_intinib_table[SwCntTimerHdr1]), &(kernel_osapinib_table[BlsmControl_OSAP_CORE1])
	}
};

ISRCB kernel_isrcb_RLIN3x_TX_ISR __attribute__((section(".pe1_lram")));
ISRCB kernel_isrcb_RLIN3x_RX_ISR __attribute__((section(".pe1_lram")));
ISRCB kernel_isrcb_SwCntTimerHdr0 __attribute__((section(".pe1_lram")));
ISRCB kernel_isrcb_SwCntTimerHdr1 __attribute__((section(".pe2_lram")));


ISRCB * const kernel_p_isrcb_table[TNUM_ISR2] = {
	&kernel_isrcb_RLIN3x_TX_ISR,
	&kernel_isrcb_RLIN3x_RX_ISR,
	&kernel_isrcb_SwCntTimerHdr0,
	&kernel_isrcb_SwCntTimerHdr1
};


/****** Object InterCoreInterrupts ******/

TOPPERS_EMPTY_LABEL(const ICIINIB, kernel_iciinib_table);
const ICIINIB * kernel_p_iciinb_table[TotalNumberOfCores] = {0};
const uint8 kernel_tnum_ici_of_core[TotalNumberOfCores] = {0};


const OSAPINIB kernel_osapinib_table[TNUM_OSAP] = {
	{
		0
	},
	{
		1
	}
};

/****** Object SPINLOCK ******/
TOPPERS_EMPTY_LABEL(const SPNINIB, kernel_spninib_table);
TOPPERS_EMPTY_LABEL(SPNCB, kernel_spncb_table);

void
kernel_object_initialize(void)
{
	kernel_interrupt_initialize();
	kernel_task_initialize();
	kernel_counter_initialize();
	kernel_alarm_initialize();
}


void
kernel_object_terminate(void)
{
	kernel_counter_terminate();
}


/*
 *  Interrupt Management Functions
 */

void
kernel_inthdr_0xffff00a5(void)
{
	i_begin_int(4294901925U);
	LOG_ISR_ENTER(RLIN3x_TX_ISR);
	ISRNAME(RLIN3x_TX_ISR)();
	LOG_ISR_LEAVE(RLIN3x_TX_ISR);
	i_end_int(4294901925U);
}
void
kernel_inthdr_0xffff00a6(void)
{
	i_begin_int(4294901926U);
	LOG_ISR_ENTER(RLIN3x_RX_ISR);
	ISRNAME(RLIN3x_RX_ISR)();
	LOG_ISR_LEAVE(RLIN3x_RX_ISR);
	i_end_int(4294901926U);
}
void
kernel_inthdr_0xffff0054(void)
{
	i_begin_int(4294901844U);
	LOG_ISR_ENTER(SwCntTimerHdr0);
	ISRNAME(SwCntTimerHdr0)();
	LOG_ISR_LEAVE(SwCntTimerHdr0);
	i_end_int(4294901844U);
}
void
kernel_inthdr_0xffff013a(void)
{
	i_begin_int(4294902074U);
	LOG_ISR_ENTER(SwCntTimerHdr1);
	ISRNAME(SwCntTimerHdr1)();
	LOG_ISR_LEAVE(SwCntTimerHdr1);
	i_end_int(4294902074U);
}

/* HardWare Counter Interrupt Handler(C2ISR) */

/*
 *  Stack Area for Non-task Context
 */

/* calculate stack size for MC */
#define TNUM_INTNO	UINT_C(4)
const InterruptNumberType kernel_tnum_intno = TNUM_INTNO;

const INTINIB kernel_intinib_table[TNUM_INTNO] = {
	{ (4294901925U), ENABLE, (-4), 0, 0x6a0U},
	{ (4294901926U), ENABLE, (-4), 0, 0x6a0U},
	{ (4294901844U), ENABLE, (-5), 0, 0x450U},
	{ (4294902074U), ENABLE, (-5), 1, 0x650U}
};

const MemorySizeType kernel_ici_remain_stksz[TotalNumberOfCores] = {
	 0x6a0U,
	 0x650U
};
static StackType kernel_core0_ostack[COUNT_STK_T(2976)] __attribute__((section(".pe1_lram")));
#define TOPPERS_CORE0_OSTKSZ		ROUND_STK_T(2976)
#define TOPPERS_CORE0_OSTK		kernel_core0_ostack

static StackType kernel_core1_ostack[COUNT_STK_T(0x750U)] __attribute__((section(".pe2_lram")));
#define TOPPERS_CORE1_OSTKSZ		ROUND_STK_T(0x750U)
#define TOPPERS_CORE1_OSTK		kernel_core1_ostack


const MemorySizeType	kernel_ostksz_table[TotalNumberOfCores] = {
	TOPPERS_CORE0_OSTKSZ,
	TOPPERS_CORE1_OSTKSZ
};

StackType * const		kernel_ostk_table[TotalNumberOfCores] = {
	(StackType *) TOPPERS_CORE0_OSTK,
	(StackType *) TOPPERS_CORE1_OSTK
};

#ifdef TOPPERS_OSTKPT
StackType * const	kernel_ostkpt_table[TotalNumberOfCores] = {
	TOPPERS_OSTKPT(TOPPERS_CORE0_OSTK, TOPPERS_CORE0_OSTKSZ),
	TOPPERS_OSTKPT(TOPPERS_CORE1_OSTK, TOPPERS_CORE1_OSTKSZ)
};
#endif /* TOPPERS_OSTKPT */


/****** Object IOC ******/
const IocType	kernel_tnum_ioc = TNUM_IOC;
const IocType	kernel_tnum_queueioc = TNUM_QUEUEIOC;

TOPPERS_EMPTY_LABEL(void * const, kernel_ioc_inival_table);
TOPPERS_EMPTY_LABEL(IOCCB, kernel_ioccb_table);
TOPPERS_EMPTY_LABEL(const IOCINIB, kernel_iocinib_table);
TOPPERS_EMPTY_LABEL(const IOCWRPINIB, kernel_iocwrpinib_table);
const IocType kernel_tnum_ioc_wrapper_send = TNUM_IOC_WRAPPER_SEND;
const IocType kernel_tnum_ioc_wrapper= TNUM_IOC_WRAPPER;

#ifdef TOPPERS_ENABLE_TRACE
const char8 *
kernel_appid_str(AppModeType id)
{
	const char8	*appid_str;
	switch (id) {
	case BlsmControl_AppMode:
		appid_str = "BlsmControl_AppMode";
		break;
	case Test_AppMode:
		appid_str = "Test_AppMode";
		break;
	default:
		appid_str = "";
		break;
	}
	return(appid_str);
}
const char8 *
kernel_tskid_str(TaskType id)
{
	const char8	*tskid_str;
	switch (id) {
	case BlsmControl_Core0_1_0_Task:
		tskid_str = "BlsmControl_Core0_1_0_Task";
		break;
	case BlsmControl_Core1_1_0_Task:
		tskid_str = "BlsmControl_Core1_1_0_Task";
		break;
	case BlsmControl_Core1_100_0_Task:
		tskid_str = "BlsmControl_Core1_100_0_Task";
		break;
	case OperationManagerTask:
		tskid_str = "OperationManagerTask";
		break;
	case DriveManagerTask:
		tskid_str = "DriveManagerTask";
		break;
	case BodyControlTask:
		tskid_str = "BodyControlTask";
		break;
	case INVALID_TASK:
		tskid_str = "INVALID_TASK";
		break;
	default:
		tskid_str = "";
		break;
	}
	return(tskid_str);
}

const char8 *
kernel_isrid_str(ISRType id)
{
	const char8	*isrid_str;
	switch (id) {
	case RLIN3x_TX_ISR:
		isrid_str = "RLIN3x_TX_ISR";
		break;
	case RLIN3x_RX_ISR:
		isrid_str = "RLIN3x_RX_ISR";
		break;
	case SwCntTimerHdr0:
		isrid_str = "SwCntTimerHdr0";
		break;
	case SwCntTimerHdr1:
		isrid_str = "SwCntTimerHdr1";
		break;
	case INVALID_ISR:
		isrid_str = "INVALID_ISR";
		break;
	default:
		isrid_str = "";
		break;
	}
	return(isrid_str);
}

const char8 *
kernel_cntid_str(CounterType id)
{
	const char8	*cntid_str;
	switch (id) {
	case SwCnt0:
		cntid_str = "SwCnt0";
		break;
	case SwCnt1:
		cntid_str = "SwCnt1";
		break;
	default:
		cntid_str = "";
		break;
	}
	return(cntid_str);
}

const char8 *
kernel_almid_str(AlarmType id)
{
	const char8	*almid_str;
	switch (id) {
	case OperationManagerCycAlarm:
		almid_str = "OperationManagerCycAlarm";
		break;
	case DriveManagerCycAlarm:
		almid_str = "DriveManagerCycAlarm";
		break;
	case BodyControlCycAlarm:
		almid_str = "BodyControlCycAlarm";
		break;
	case BuzzerControlCycAlarm:
		almid_str = "BuzzerControlCycAlarm";
		break;
	case BlsmControl_Core0_1_0_Alarm:
		almid_str = "BlsmControl_Core0_1_0_Alarm";
		break;
	case BlsmControl_Core1_1_0_Alarm:
		almid_str = "BlsmControl_Core1_1_0_Alarm";
		break;
	case BlsmControl_Core1_100_0_Alarm:
		almid_str = "BlsmControl_Core1_100_0_Alarm";
		break;
	default:
		almid_str = "";
		break;
	}
	return(almid_str);
}

const char8 *
kernel_resid_str(ResourceType id)
{
	return("");
}

const char8 *
kernel_schtblid_str(ScheduleTableType id)
{
	return("");
}

const char8 *
kernel_evtid_str(TaskType task, EventMaskType event)
{
	const char8	*evtid_str;
	switch (task) {
	case BlsmControl_Core0_1_0_Task:
		switch (event) {
		case BlsmControl_WakeupEvent:
			evtid_str = "BlsmControl_WakeupEvent";
			break;
		case BlsmControl_InputEvent:
			evtid_str = "BlsmControl_InputEvent";
			break;
		case BlsmControl_OutputEvent:
			evtid_str = "BlsmControl_OutputEvent";
			break;
		case BlsmControl_CompleteEvent:
			evtid_str = "BlsmControl_CompleteEvent";
			break;
		default:
			evtid_str = NULL;
			break;
		}
		break;
	case BlsmControl_Core1_1_0_Task:
		switch (event) {
		case BlsmControl_WakeupEvent:
			evtid_str = "BlsmControl_WakeupEvent";
			break;
		case BlsmControl_InputEvent:
			evtid_str = "BlsmControl_InputEvent";
			break;
		case BlsmControl_OutputEvent:
			evtid_str = "BlsmControl_OutputEvent";
			break;
		case BlsmControl_CompleteEvent:
			evtid_str = "BlsmControl_CompleteEvent";
			break;
		default:
			evtid_str = NULL;
			break;
		}
		break;
	case BlsmControl_Core1_100_0_Task:
		switch (event) {
		case BlsmControl_WakeupEvent:
			evtid_str = "BlsmControl_WakeupEvent";
			break;
		case BlsmControl_InputEvent:
			evtid_str = "BlsmControl_InputEvent";
			break;
		case BlsmControl_OutputEvent:
			evtid_str = "BlsmControl_OutputEvent";
			break;
		case BlsmControl_CompleteEvent:
			evtid_str = "BlsmControl_CompleteEvent";
			break;
		default:
			evtid_str = NULL;
			break;
		}
		break;
	case OperationManagerTask:
		evtid_str = NULL;
		break;
	case DriveManagerTask:
		evtid_str = NULL;
		break;
	case BodyControlTask:
		evtid_str = NULL;
		break;
	default:
		evtid_str = NULL;
		break;
	}
	if (evtid_str == NULL) {
		if (event == BlsmControl_WakeupEvent) {
			evtid_str = "BlsmControl_WakeupEvent";
		}
		if (event == BlsmControl_InputEvent) {
			evtid_str = "BlsmControl_InputEvent";
		}
		if (event == BlsmControl_OutputEvent) {
			evtid_str = "BlsmControl_OutputEvent";
		}
		if (event == BlsmControl_CompleteEvent) {
			evtid_str = "BlsmControl_CompleteEvent";
		}
	}
	return(evtid_str);
}

const char8 *
kernel_osapid_str(ApplicationType id)
{
	const char8	*osapid_str;
	switch (id) {
	case BlsmControl_OSAP_CORE0:
		osapid_str = "BlsmControl_OSAP_CORE0";
		break;
	case BlsmControl_OSAP_CORE1:
		osapid_str = "BlsmControl_OSAP_CORE1";
		break;
	case INVALID_OSAPPLICATION:
		osapid_str = "INVALID_OSAPPLICATION";
		break;
	default:
		osapid_str = "";
		break;
	}
	return(osapid_str);
}

const char8 *
kernel_iocid_str(IocType id)
{
	const char8	*iocid_str;
	switch (id) {
	default:
		iocid_str = "";
		break;
	}
	return(iocid_str);
}
#endif /* TOPPERS_ENABLE_TRACE */

void
kernel_inthdr_0x10000(void)
{
	i_begin_int(65536U);
	ISRNAME(target_ici_handler0)();
	i_end_int(65536U);
}
void
kernel_inthdr_0x20000(void)
{
	i_begin_int(131072U);
	ISRNAME(target_ici_handler1)();
	i_end_int(131072U);
}
const uint16 kernel_pmr_isr2_mask = 0xf800;
const uint16 kernel_pmr_isr1_mask = 0x7ff;

const FunctionRefType kernel_core0_isr_tbl[TNUM_INT] = {
	&kernel_inthdr_0x10000,	/* 0x10000 */
	&kernel_default_int_handler,	/* 0x10001 */
	&kernel_default_int_handler,	/* 0x10002 */
	&kernel_default_int_handler,	/* 0x10003 */
	&kernel_default_int_handler,	/* 0x10004 */
	&kernel_default_int_handler,	/* 0x10005 */
	&kernel_default_int_handler,	/* 0x10006 */
	&kernel_default_int_handler,	/* 0x10007 */
	&kernel_default_int_handler,	/* 0x10008 */
	&kernel_default_int_handler,	/* 0x10009 */
	&kernel_default_int_handler,	/* 0x1000a */
	&kernel_default_int_handler,	/* 0x1000b */
	&kernel_default_int_handler,	/* 0x1000c */
	&kernel_default_int_handler,	/* 0x1000d */
	&kernel_default_int_handler,	/* 0x1000e */
	&kernel_default_int_handler,	/* 0x1000f */
	&kernel_default_int_handler,	/* 0x10010 */
	&kernel_default_int_handler,	/* 0x10011 */
	&kernel_default_int_handler,	/* 0x10012 */
	&kernel_default_int_handler,	/* 0x10013 */
	&kernel_default_int_handler,	/* 0x10014 */
	&kernel_default_int_handler,	/* 0x10015 */
	&kernel_default_int_handler,	/* 0x10016 */
	&kernel_default_int_handler,	/* 0x10017 */
	&kernel_default_int_handler,	/* 0x10018 */
	&kernel_default_int_handler,	/* 0x10019 */
	&kernel_default_int_handler,	/* 0x1001a */
	&kernel_default_int_handler,	/* 0x1001b */
	&kernel_default_int_handler,	/* 0x1001c */
	&kernel_default_int_handler,	/* 0x1001d */
	&kernel_default_int_handler,	/* 0x1001e */
	&kernel_default_int_handler,	/* 0x1001f */
	&kernel_default_int_handler,	/* 0xffff0020 */
	&kernel_default_int_handler,	/* 0xffff0021 */
	&kernel_default_int_handler,	/* 0xffff0022 */
	&kernel_default_int_handler,	/* 0xffff0023 */
	&kernel_default_int_handler,	/* 0xffff0024 */
	&kernel_default_int_handler,	/* 0xffff0025 */
	&kernel_default_int_handler,	/* 0xffff0026 */
	&kernel_default_int_handler,	/* 0xffff0027 */
	&kernel_default_int_handler,	/* 0xffff0028 */
	&kernel_default_int_handler,	/* 0xffff0029 */
	&kernel_default_int_handler,	/* 0xffff002a */
	&kernel_default_int_handler,	/* 0xffff002b */
	&kernel_default_int_handler,	/* 0xffff002c */
	&kernel_default_int_handler,	/* 0xffff002d */
	&kernel_default_int_handler,	/* 0xffff002e */
	&kernel_default_int_handler,	/* 0xffff002f */
	&kernel_default_int_handler,	/* 0xffff0030 */
	&kernel_default_int_handler,	/* 0xffff0031 */
	&kernel_default_int_handler,	/* 0xffff0032 */
	&kernel_default_int_handler,	/* 0xffff0033 */
	&kernel_default_int_handler,	/* 0xffff0034 */
	&kernel_default_int_handler,	/* 0xffff0035 */
	&kernel_default_int_handler,	/* 0xffff0036 */
	&kernel_default_int_handler,	/* 0xffff0037 */
	&kernel_default_int_handler,	/* 0xffff0038 */
	&kernel_default_int_handler,	/* 0xffff0039 */
	&kernel_default_int_handler,	/* 0xffff003a */
	&kernel_default_int_handler,	/* 0xffff003b */
	&kernel_default_int_handler,	/* 0xffff003c */
	&kernel_default_int_handler,	/* 0xffff003d */
	&kernel_default_int_handler,	/* 0xffff003e */
	&kernel_default_int_handler,	/* 0xffff003f */
	&kernel_default_int_handler,	/* 0xffff0040 */
	&kernel_default_int_handler,	/* 0xffff0041 */
	&kernel_default_int_handler,	/* 0xffff0042 */
	&kernel_default_int_handler,	/* 0xffff0043 */
	&kernel_default_int_handler,	/* 0xffff0044 */
	&kernel_default_int_handler,	/* 0xffff0045 */
	&kernel_default_int_handler,	/* 0xffff0046 */
	&kernel_default_int_handler,	/* 0xffff0047 */
	&kernel_default_int_handler,	/* 0xffff0048 */
	&kernel_default_int_handler,	/* 0xffff0049 */
	&kernel_default_int_handler,	/* 0xffff004a */
	&kernel_default_int_handler,	/* 0xffff004b */
	&kernel_default_int_handler,	/* 0xffff004c */
	&kernel_default_int_handler,	/* 0xffff004d */
	&kernel_default_int_handler,	/* 0xffff004e */
	&kernel_default_int_handler,	/* 0xffff004f */
	&kernel_default_int_handler,	/* 0xffff0050 */
	&kernel_default_int_handler,	/* 0xffff0051 */
	&kernel_default_int_handler,	/* 0xffff0052 */
	&kernel_default_int_handler,	/* 0xffff0053 */
	&kernel_inthdr_0xffff0054,	/* 0xffff0054 */
	&kernel_default_int_handler,	/* 0xffff0055 */
	&kernel_default_int_handler,	/* 0xffff0056 */
	&kernel_default_int_handler,	/* 0xffff0057 */
	&kernel_default_int_handler,	/* 0xffff0058 */
	&kernel_default_int_handler,	/* 0xffff0059 */
	&kernel_default_int_handler,	/* 0xffff005a */
	&kernel_default_int_handler,	/* 0xffff005b */
	&kernel_default_int_handler,	/* 0xffff005c */
	&kernel_default_int_handler,	/* 0xffff005d */
	&kernel_default_int_handler,	/* 0xffff005e */
	&kernel_default_int_handler,	/* 0xffff005f */
	&kernel_default_int_handler,	/* 0xffff0060 */
	&kernel_default_int_handler,	/* 0xffff0061 */
	&kernel_default_int_handler,	/* 0xffff0062 */
	&kernel_default_int_handler,	/* 0xffff0063 */
	&kernel_default_int_handler,	/* 0xffff0064 */
	&kernel_default_int_handler,	/* 0xffff0065 */
	&kernel_default_int_handler,	/* 0xffff0066 */
	&kernel_default_int_handler,	/* 0xffff0067 */
	&kernel_default_int_handler,	/* 0xffff0068 */
	&kernel_default_int_handler,	/* 0xffff0069 */
	&kernel_default_int_handler,	/* 0xffff006a */
	&kernel_default_int_handler,	/* 0xffff006b */
	&kernel_default_int_handler,	/* 0xffff006c */
	&kernel_default_int_handler,	/* 0xffff006d */
	&kernel_default_int_handler,	/* 0xffff006e */
	&kernel_default_int_handler,	/* 0xffff006f */
	&kernel_default_int_handler,	/* 0xffff0070 */
	&kernel_default_int_handler,	/* 0xffff0071 */
	&kernel_default_int_handler,	/* 0xffff0072 */
	&kernel_default_int_handler,	/* 0xffff0073 */
	&kernel_default_int_handler,	/* 0xffff0074 */
	&kernel_default_int_handler,	/* 0xffff0075 */
	&kernel_default_int_handler,	/* 0xffff0076 */
	&kernel_default_int_handler,	/* 0xffff0077 */
	&kernel_default_int_handler,	/* 0xffff0078 */
	&kernel_default_int_handler,	/* 0xffff0079 */
	&kernel_default_int_handler,	/* 0xffff007a */
	&kernel_default_int_handler,	/* 0xffff007b */
	&kernel_default_int_handler,	/* 0xffff007c */
	&kernel_default_int_handler,	/* 0xffff007d */
	&kernel_default_int_handler,	/* 0xffff007e */
	&kernel_default_int_handler,	/* 0xffff007f */
	&kernel_default_int_handler,	/* 0xffff0080 */
	&kernel_default_int_handler,	/* 0xffff0081 */
	&kernel_default_int_handler,	/* 0xffff0082 */
	&kernel_default_int_handler,	/* 0xffff0083 */
	&kernel_default_int_handler,	/* 0xffff0084 */
	&kernel_default_int_handler,	/* 0xffff0085 */
	&kernel_default_int_handler,	/* 0xffff0086 */
	&kernel_default_int_handler,	/* 0xffff0087 */
	&kernel_default_int_handler,	/* 0xffff0088 */
	&kernel_default_int_handler,	/* 0xffff0089 */
	&kernel_default_int_handler,	/* 0xffff008a */
	&kernel_default_int_handler,	/* 0xffff008b */
	&kernel_default_int_handler,	/* 0xffff008c */
	&kernel_default_int_handler,	/* 0xffff008d */
	&kernel_default_int_handler,	/* 0xffff008e */
	&kernel_default_int_handler,	/* 0xffff008f */
	&kernel_default_int_handler,	/* 0xffff0090 */
	&kernel_default_int_handler,	/* 0xffff0091 */
	&kernel_default_int_handler,	/* 0xffff0092 */
	&kernel_default_int_handler,	/* 0xffff0093 */
	&kernel_default_int_handler,	/* 0xffff0094 */
	&kernel_default_int_handler,	/* 0xffff0095 */
	&kernel_default_int_handler,	/* 0xffff0096 */
	&kernel_default_int_handler,	/* 0xffff0097 */
	&kernel_default_int_handler,	/* 0xffff0098 */
	&kernel_default_int_handler,	/* 0xffff0099 */
	&kernel_default_int_handler,	/* 0xffff009a */
	&kernel_default_int_handler,	/* 0xffff009b */
	&kernel_default_int_handler,	/* 0xffff009c */
	&kernel_default_int_handler,	/* 0xffff009d */
	&kernel_default_int_handler,	/* 0xffff009e */
	&kernel_default_int_handler,	/* 0xffff009f */
	&kernel_default_int_handler,	/* 0xffff00a0 */
	&kernel_default_int_handler,	/* 0xffff00a1 */
	&kernel_default_int_handler,	/* 0xffff00a2 */
	&kernel_default_int_handler,	/* 0xffff00a3 */
	&kernel_default_int_handler,	/* 0xffff00a4 */
	&kernel_inthdr_0xffff00a5,	/* 0xffff00a5 */
	&kernel_inthdr_0xffff00a6,	/* 0xffff00a6 */
	&kernel_default_int_handler,	/* 0xffff00a7 */
	&kernel_default_int_handler,	/* 0xffff00a8 */
	&kernel_default_int_handler,	/* 0xffff00a9 */
	&kernel_default_int_handler,	/* 0xffff00aa */
	&kernel_default_int_handler,	/* 0xffff00ab */
	&kernel_default_int_handler,	/* 0xffff00ac */
	&kernel_default_int_handler,	/* 0xffff00ad */
	&kernel_default_int_handler,	/* 0xffff00ae */
	&kernel_default_int_handler,	/* 0xffff00af */
	&kernel_default_int_handler,	/* 0xffff00b0 */
	&kernel_default_int_handler,	/* 0xffff00b1 */
	&kernel_default_int_handler,	/* 0xffff00b2 */
	&kernel_default_int_handler,	/* 0xffff00b3 */
	&kernel_default_int_handler,	/* 0xffff00b4 */
	&kernel_default_int_handler,	/* 0xffff00b5 */
	&kernel_default_int_handler,	/* 0xffff00b6 */
	&kernel_default_int_handler,	/* 0xffff00b7 */
	&kernel_default_int_handler,	/* 0xffff00b8 */
	&kernel_default_int_handler,	/* 0xffff00b9 */
	&kernel_default_int_handler,	/* 0xffff00ba */
	&kernel_default_int_handler,	/* 0xffff00bb */
	&kernel_default_int_handler,	/* 0xffff00bc */
	&kernel_default_int_handler,	/* 0xffff00bd */
	&kernel_default_int_handler,	/* 0xffff00be */
	&kernel_default_int_handler,	/* 0xffff00bf */
	&kernel_default_int_handler,	/* 0xffff00c0 */
	&kernel_default_int_handler,	/* 0xffff00c1 */
	&kernel_default_int_handler,	/* 0xffff00c2 */
	&kernel_default_int_handler,	/* 0xffff00c3 */
	&kernel_default_int_handler,	/* 0xffff00c4 */
	&kernel_default_int_handler,	/* 0xffff00c5 */
	&kernel_default_int_handler,	/* 0xffff00c6 */
	&kernel_default_int_handler,	/* 0xffff00c7 */
	&kernel_default_int_handler,	/* 0xffff00c8 */
	&kernel_default_int_handler,	/* 0xffff00c9 */
	&kernel_default_int_handler,	/* 0xffff00ca */
	&kernel_default_int_handler,	/* 0xffff00cb */
	&kernel_default_int_handler,	/* 0xffff00cc */
	&kernel_default_int_handler,	/* 0xffff00cd */
	&kernel_default_int_handler,	/* 0xffff00ce */
	&kernel_default_int_handler,	/* 0xffff00cf */
	&kernel_default_int_handler,	/* 0xffff00d0 */
	&kernel_default_int_handler,	/* 0xffff00d1 */
	&kernel_default_int_handler,	/* 0xffff00d2 */
	&kernel_default_int_handler,	/* 0xffff00d3 */
	&kernel_default_int_handler,	/* 0xffff00d4 */
	&kernel_default_int_handler,	/* 0xffff00d5 */
	&kernel_default_int_handler,	/* 0xffff00d6 */
	&kernel_default_int_handler,	/* 0xffff00d7 */
	&kernel_default_int_handler,	/* 0xffff00d8 */
	&kernel_default_int_handler,	/* 0xffff00d9 */
	&kernel_default_int_handler,	/* 0xffff00da */
	&kernel_default_int_handler,	/* 0xffff00db */
	&kernel_default_int_handler,	/* 0xffff00dc */
	&kernel_default_int_handler,	/* 0xffff00dd */
	&kernel_default_int_handler,	/* 0xffff00de */
	&kernel_default_int_handler,	/* 0xffff00df */
	&kernel_default_int_handler,	/* 0xffff00e0 */
	&kernel_default_int_handler,	/* 0xffff00e1 */
	&kernel_default_int_handler,	/* 0xffff00e2 */
	&kernel_default_int_handler,	/* 0xffff00e3 */
	&kernel_default_int_handler,	/* 0xffff00e4 */
	&kernel_default_int_handler,	/* 0xffff00e5 */
	&kernel_default_int_handler,	/* 0xffff00e6 */
	&kernel_default_int_handler,	/* 0xffff00e7 */
	&kernel_default_int_handler,	/* 0xffff00e8 */
	&kernel_default_int_handler,	/* 0xffff00e9 */
	&kernel_default_int_handler,	/* 0xffff00ea */
	&kernel_default_int_handler,	/* 0xffff00eb */
	&kernel_default_int_handler,	/* 0xffff00ec */
	&kernel_default_int_handler,	/* 0xffff00ed */
	&kernel_default_int_handler,	/* 0xffff00ee */
	&kernel_default_int_handler,	/* 0xffff00ef */
	&kernel_default_int_handler,	/* 0xffff00f0 */
	&kernel_default_int_handler,	/* 0xffff00f1 */
	&kernel_default_int_handler,	/* 0xffff00f2 */
	&kernel_default_int_handler,	/* 0xffff00f3 */
	&kernel_default_int_handler,	/* 0xffff00f4 */
	&kernel_default_int_handler,	/* 0xffff00f5 */
	&kernel_default_int_handler,	/* 0xffff00f6 */
	&kernel_default_int_handler,	/* 0xffff00f7 */
	&kernel_default_int_handler,	/* 0xffff00f8 */
	&kernel_default_int_handler,	/* 0xffff00f9 */
	&kernel_default_int_handler,	/* 0xffff00fa */
	&kernel_default_int_handler,	/* 0xffff00fb */
	&kernel_default_int_handler,	/* 0xffff00fc */
	&kernel_default_int_handler,	/* 0xffff00fd */
	&kernel_default_int_handler,	/* 0xffff00fe */
	&kernel_default_int_handler,	/* 0xffff00ff */
	&kernel_default_int_handler,	/* 0xffff0100 */
	&kernel_default_int_handler,	/* 0xffff0101 */
	&kernel_default_int_handler,	/* 0xffff0102 */
	&kernel_default_int_handler,	/* 0xffff0103 */
	&kernel_default_int_handler,	/* 0xffff0104 */
	&kernel_default_int_handler,	/* 0xffff0105 */
	&kernel_default_int_handler,	/* 0xffff0106 */
	&kernel_default_int_handler,	/* 0xffff0107 */
	&kernel_default_int_handler,	/* 0xffff0108 */
	&kernel_default_int_handler,	/* 0xffff0109 */
	&kernel_default_int_handler,	/* 0xffff010a */
	&kernel_default_int_handler,	/* 0xffff010b */
	&kernel_default_int_handler,	/* 0xffff010c */
	&kernel_default_int_handler,	/* 0xffff010d */
	&kernel_default_int_handler,	/* 0xffff010e */
	&kernel_default_int_handler,	/* 0xffff010f */
	&kernel_default_int_handler,	/* 0xffff0110 */
	&kernel_default_int_handler,	/* 0xffff0111 */
	&kernel_default_int_handler,	/* 0xffff0112 */
	&kernel_default_int_handler,	/* 0xffff0113 */
	&kernel_default_int_handler,	/* 0xffff0114 */
	&kernel_default_int_handler,	/* 0xffff0115 */
	&kernel_default_int_handler,	/* 0xffff0116 */
	&kernel_default_int_handler,	/* 0xffff0117 */
	&kernel_default_int_handler,	/* 0xffff0118 */
	&kernel_default_int_handler,	/* 0xffff0119 */
	&kernel_default_int_handler,	/* 0xffff011a */
	&kernel_default_int_handler,	/* 0xffff011b */
	&kernel_default_int_handler,	/* 0xffff011c */
	&kernel_default_int_handler,	/* 0xffff011d */
	&kernel_default_int_handler,	/* 0xffff011e */
	&kernel_default_int_handler,	/* 0xffff011f */
	&kernel_default_int_handler,	/* 0xffff0120 */
	&kernel_default_int_handler,	/* 0xffff0121 */
	&kernel_default_int_handler,	/* 0xffff0122 */
	&kernel_default_int_handler,	/* 0xffff0123 */
	&kernel_default_int_handler,	/* 0xffff0124 */
	&kernel_default_int_handler,	/* 0xffff0125 */
	&kernel_default_int_handler,	/* 0xffff0126 */
	&kernel_default_int_handler,	/* 0xffff0127 */
	&kernel_default_int_handler,	/* 0xffff0128 */
	&kernel_default_int_handler,	/* 0xffff0129 */
	&kernel_default_int_handler,	/* 0xffff012a */
	&kernel_default_int_handler,	/* 0xffff012b */
	&kernel_default_int_handler,	/* 0xffff012c */
	&kernel_default_int_handler,	/* 0xffff012d */
	&kernel_default_int_handler,	/* 0xffff012e */
	&kernel_default_int_handler,	/* 0xffff012f */
	&kernel_default_int_handler,	/* 0xffff0130 */
	&kernel_default_int_handler,	/* 0xffff0131 */
	&kernel_default_int_handler,	/* 0xffff0132 */
	&kernel_default_int_handler,	/* 0xffff0133 */
	&kernel_default_int_handler,	/* 0xffff0134 */
	&kernel_default_int_handler,	/* 0xffff0135 */
	&kernel_default_int_handler,	/* 0xffff0136 */
	&kernel_default_int_handler,	/* 0xffff0137 */
	&kernel_default_int_handler,	/* 0xffff0138 */
	&kernel_default_int_handler,	/* 0xffff0139 */
	&kernel_default_int_handler,	/* 0xffff013a */
	&kernel_default_int_handler,	/* 0xffff013b */
	&kernel_default_int_handler,	/* 0xffff013c */
	&kernel_default_int_handler,	/* 0xffff013d */
	&kernel_default_int_handler,	/* 0xffff013e */
	&kernel_default_int_handler,	/* 0xffff013f */
	&kernel_default_int_handler,	/* 0xffff0140 */
	&kernel_default_int_handler,	/* 0xffff0141 */
	&kernel_default_int_handler,	/* 0xffff0142 */
	&kernel_default_int_handler,	/* 0xffff0143 */
	&kernel_default_int_handler,	/* 0xffff0144 */
	&kernel_default_int_handler,	/* 0xffff0145 */
	&kernel_default_int_handler,	/* 0xffff0146 */
	&kernel_default_int_handler,	/* 0xffff0147 */
	&kernel_default_int_handler,	/* 0xffff0148 */
	&kernel_default_int_handler,	/* 0xffff0149 */
	&kernel_default_int_handler,	/* 0xffff014a */
	&kernel_default_int_handler,	/* 0xffff014b */
	&kernel_default_int_handler,	/* 0xffff014c */
	&kernel_default_int_handler,	/* 0xffff014d */
	&kernel_default_int_handler,	/* 0xffff014e */
	&kernel_default_int_handler,	/* 0xffff014f */
	&kernel_default_int_handler,	/* 0xffff0150 */
	&kernel_default_int_handler,	/* 0xffff0151 */
	&kernel_default_int_handler,	/* 0xffff0152 */
	&kernel_default_int_handler,	/* 0xffff0153 */
	&kernel_default_int_handler,	/* 0xffff0154 */
	&kernel_default_int_handler,	/* 0xffff0155 */
	&kernel_default_int_handler,	/* 0xffff0156 */
	&kernel_default_int_handler,	/* 0xffff0157 */
	&kernel_default_int_handler,	/* 0xffff0158 */
	&kernel_default_int_handler,	/* 0xffff0159 */
	&kernel_default_int_handler,	/* 0xffff015a */
	&kernel_default_int_handler,	/* 0xffff015b */
	&kernel_default_int_handler,	/* 0xffff015c */
	&kernel_default_int_handler,	/* 0xffff015d */
	&kernel_default_int_handler	/* 0xffff015e */
};

const FunctionRefType kernel_core1_isr_tbl[TNUM_INT] = {
	&kernel_inthdr_0x20000,	/* 0x20000 */
	&kernel_default_int_handler,	/* 0x20001 */
	&kernel_default_int_handler,	/* 0x20002 */
	&kernel_default_int_handler,	/* 0x20003 */
	&kernel_default_int_handler,	/* 0x20004 */
	&kernel_default_int_handler,	/* 0x20005 */
	&kernel_default_int_handler,	/* 0x20006 */
	&kernel_default_int_handler,	/* 0x20007 */
	&kernel_default_int_handler,	/* 0x20008 */
	&kernel_default_int_handler,	/* 0x20009 */
	&kernel_default_int_handler,	/* 0x2000a */
	&kernel_default_int_handler,	/* 0x2000b */
	&kernel_default_int_handler,	/* 0x2000c */
	&kernel_default_int_handler,	/* 0x2000d */
	&kernel_default_int_handler,	/* 0x2000e */
	&kernel_default_int_handler,	/* 0x2000f */
	&kernel_default_int_handler,	/* 0x20010 */
	&kernel_default_int_handler,	/* 0x20011 */
	&kernel_default_int_handler,	/* 0x20012 */
	&kernel_default_int_handler,	/* 0x20013 */
	&kernel_default_int_handler,	/* 0x20014 */
	&kernel_default_int_handler,	/* 0x20015 */
	&kernel_default_int_handler,	/* 0x20016 */
	&kernel_default_int_handler,	/* 0x20017 */
	&kernel_default_int_handler,	/* 0x20018 */
	&kernel_default_int_handler,	/* 0x20019 */
	&kernel_default_int_handler,	/* 0x2001a */
	&kernel_default_int_handler,	/* 0x2001b */
	&kernel_default_int_handler,	/* 0x2001c */
	&kernel_default_int_handler,	/* 0x2001d */
	&kernel_default_int_handler,	/* 0x2001e */
	&kernel_default_int_handler,	/* 0x2001f */
	&kernel_default_int_handler,	/* 0xffff0020 */
	&kernel_default_int_handler,	/* 0xffff0021 */
	&kernel_default_int_handler,	/* 0xffff0022 */
	&kernel_default_int_handler,	/* 0xffff0023 */
	&kernel_default_int_handler,	/* 0xffff0024 */
	&kernel_default_int_handler,	/* 0xffff0025 */
	&kernel_default_int_handler,	/* 0xffff0026 */
	&kernel_default_int_handler,	/* 0xffff0027 */
	&kernel_default_int_handler,	/* 0xffff0028 */
	&kernel_default_int_handler,	/* 0xffff0029 */
	&kernel_default_int_handler,	/* 0xffff002a */
	&kernel_default_int_handler,	/* 0xffff002b */
	&kernel_default_int_handler,	/* 0xffff002c */
	&kernel_default_int_handler,	/* 0xffff002d */
	&kernel_default_int_handler,	/* 0xffff002e */
	&kernel_default_int_handler,	/* 0xffff002f */
	&kernel_default_int_handler,	/* 0xffff0030 */
	&kernel_default_int_handler,	/* 0xffff0031 */
	&kernel_default_int_handler,	/* 0xffff0032 */
	&kernel_default_int_handler,	/* 0xffff0033 */
	&kernel_default_int_handler,	/* 0xffff0034 */
	&kernel_default_int_handler,	/* 0xffff0035 */
	&kernel_default_int_handler,	/* 0xffff0036 */
	&kernel_default_int_handler,	/* 0xffff0037 */
	&kernel_default_int_handler,	/* 0xffff0038 */
	&kernel_default_int_handler,	/* 0xffff0039 */
	&kernel_default_int_handler,	/* 0xffff003a */
	&kernel_default_int_handler,	/* 0xffff003b */
	&kernel_default_int_handler,	/* 0xffff003c */
	&kernel_default_int_handler,	/* 0xffff003d */
	&kernel_default_int_handler,	/* 0xffff003e */
	&kernel_default_int_handler,	/* 0xffff003f */
	&kernel_default_int_handler,	/* 0xffff0040 */
	&kernel_default_int_handler,	/* 0xffff0041 */
	&kernel_default_int_handler,	/* 0xffff0042 */
	&kernel_default_int_handler,	/* 0xffff0043 */
	&kernel_default_int_handler,	/* 0xffff0044 */
	&kernel_default_int_handler,	/* 0xffff0045 */
	&kernel_default_int_handler,	/* 0xffff0046 */
	&kernel_default_int_handler,	/* 0xffff0047 */
	&kernel_default_int_handler,	/* 0xffff0048 */
	&kernel_default_int_handler,	/* 0xffff0049 */
	&kernel_default_int_handler,	/* 0xffff004a */
	&kernel_default_int_handler,	/* 0xffff004b */
	&kernel_default_int_handler,	/* 0xffff004c */
	&kernel_default_int_handler,	/* 0xffff004d */
	&kernel_default_int_handler,	/* 0xffff004e */
	&kernel_default_int_handler,	/* 0xffff004f */
	&kernel_default_int_handler,	/* 0xffff0050 */
	&kernel_default_int_handler,	/* 0xffff0051 */
	&kernel_default_int_handler,	/* 0xffff0052 */
	&kernel_default_int_handler,	/* 0xffff0053 */
	&kernel_default_int_handler,	/* 0xffff0054 */
	&kernel_default_int_handler,	/* 0xffff0055 */
	&kernel_default_int_handler,	/* 0xffff0056 */
	&kernel_default_int_handler,	/* 0xffff0057 */
	&kernel_default_int_handler,	/* 0xffff0058 */
	&kernel_default_int_handler,	/* 0xffff0059 */
	&kernel_default_int_handler,	/* 0xffff005a */
	&kernel_default_int_handler,	/* 0xffff005b */
	&kernel_default_int_handler,	/* 0xffff005c */
	&kernel_default_int_handler,	/* 0xffff005d */
	&kernel_default_int_handler,	/* 0xffff005e */
	&kernel_default_int_handler,	/* 0xffff005f */
	&kernel_default_int_handler,	/* 0xffff0060 */
	&kernel_default_int_handler,	/* 0xffff0061 */
	&kernel_default_int_handler,	/* 0xffff0062 */
	&kernel_default_int_handler,	/* 0xffff0063 */
	&kernel_default_int_handler,	/* 0xffff0064 */
	&kernel_default_int_handler,	/* 0xffff0065 */
	&kernel_default_int_handler,	/* 0xffff0066 */
	&kernel_default_int_handler,	/* 0xffff0067 */
	&kernel_default_int_handler,	/* 0xffff0068 */
	&kernel_default_int_handler,	/* 0xffff0069 */
	&kernel_default_int_handler,	/* 0xffff006a */
	&kernel_default_int_handler,	/* 0xffff006b */
	&kernel_default_int_handler,	/* 0xffff006c */
	&kernel_default_int_handler,	/* 0xffff006d */
	&kernel_default_int_handler,	/* 0xffff006e */
	&kernel_default_int_handler,	/* 0xffff006f */
	&kernel_default_int_handler,	/* 0xffff0070 */
	&kernel_default_int_handler,	/* 0xffff0071 */
	&kernel_default_int_handler,	/* 0xffff0072 */
	&kernel_default_int_handler,	/* 0xffff0073 */
	&kernel_default_int_handler,	/* 0xffff0074 */
	&kernel_default_int_handler,	/* 0xffff0075 */
	&kernel_default_int_handler,	/* 0xffff0076 */
	&kernel_default_int_handler,	/* 0xffff0077 */
	&kernel_default_int_handler,	/* 0xffff0078 */
	&kernel_default_int_handler,	/* 0xffff0079 */
	&kernel_default_int_handler,	/* 0xffff007a */
	&kernel_default_int_handler,	/* 0xffff007b */
	&kernel_default_int_handler,	/* 0xffff007c */
	&kernel_default_int_handler,	/* 0xffff007d */
	&kernel_default_int_handler,	/* 0xffff007e */
	&kernel_default_int_handler,	/* 0xffff007f */
	&kernel_default_int_handler,	/* 0xffff0080 */
	&kernel_default_int_handler,	/* 0xffff0081 */
	&kernel_default_int_handler,	/* 0xffff0082 */
	&kernel_default_int_handler,	/* 0xffff0083 */
	&kernel_default_int_handler,	/* 0xffff0084 */
	&kernel_default_int_handler,	/* 0xffff0085 */
	&kernel_default_int_handler,	/* 0xffff0086 */
	&kernel_default_int_handler,	/* 0xffff0087 */
	&kernel_default_int_handler,	/* 0xffff0088 */
	&kernel_default_int_handler,	/* 0xffff0089 */
	&kernel_default_int_handler,	/* 0xffff008a */
	&kernel_default_int_handler,	/* 0xffff008b */
	&kernel_default_int_handler,	/* 0xffff008c */
	&kernel_default_int_handler,	/* 0xffff008d */
	&kernel_default_int_handler,	/* 0xffff008e */
	&kernel_default_int_handler,	/* 0xffff008f */
	&kernel_default_int_handler,	/* 0xffff0090 */
	&kernel_default_int_handler,	/* 0xffff0091 */
	&kernel_default_int_handler,	/* 0xffff0092 */
	&kernel_default_int_handler,	/* 0xffff0093 */
	&kernel_default_int_handler,	/* 0xffff0094 */
	&kernel_default_int_handler,	/* 0xffff0095 */
	&kernel_default_int_handler,	/* 0xffff0096 */
	&kernel_default_int_handler,	/* 0xffff0097 */
	&kernel_default_int_handler,	/* 0xffff0098 */
	&kernel_default_int_handler,	/* 0xffff0099 */
	&kernel_default_int_handler,	/* 0xffff009a */
	&kernel_default_int_handler,	/* 0xffff009b */
	&kernel_default_int_handler,	/* 0xffff009c */
	&kernel_default_int_handler,	/* 0xffff009d */
	&kernel_default_int_handler,	/* 0xffff009e */
	&kernel_default_int_handler,	/* 0xffff009f */
	&kernel_default_int_handler,	/* 0xffff00a0 */
	&kernel_default_int_handler,	/* 0xffff00a1 */
	&kernel_default_int_handler,	/* 0xffff00a2 */
	&kernel_default_int_handler,	/* 0xffff00a3 */
	&kernel_default_int_handler,	/* 0xffff00a4 */
	&kernel_default_int_handler,	/* 0xffff00a5 */
	&kernel_default_int_handler,	/* 0xffff00a6 */
	&kernel_default_int_handler,	/* 0xffff00a7 */
	&kernel_default_int_handler,	/* 0xffff00a8 */
	&kernel_default_int_handler,	/* 0xffff00a9 */
	&kernel_default_int_handler,	/* 0xffff00aa */
	&kernel_default_int_handler,	/* 0xffff00ab */
	&kernel_default_int_handler,	/* 0xffff00ac */
	&kernel_default_int_handler,	/* 0xffff00ad */
	&kernel_default_int_handler,	/* 0xffff00ae */
	&kernel_default_int_handler,	/* 0xffff00af */
	&kernel_default_int_handler,	/* 0xffff00b0 */
	&kernel_default_int_handler,	/* 0xffff00b1 */
	&kernel_default_int_handler,	/* 0xffff00b2 */
	&kernel_default_int_handler,	/* 0xffff00b3 */
	&kernel_default_int_handler,	/* 0xffff00b4 */
	&kernel_default_int_handler,	/* 0xffff00b5 */
	&kernel_default_int_handler,	/* 0xffff00b6 */
	&kernel_default_int_handler,	/* 0xffff00b7 */
	&kernel_default_int_handler,	/* 0xffff00b8 */
	&kernel_default_int_handler,	/* 0xffff00b9 */
	&kernel_default_int_handler,	/* 0xffff00ba */
	&kernel_default_int_handler,	/* 0xffff00bb */
	&kernel_default_int_handler,	/* 0xffff00bc */
	&kernel_default_int_handler,	/* 0xffff00bd */
	&kernel_default_int_handler,	/* 0xffff00be */
	&kernel_default_int_handler,	/* 0xffff00bf */
	&kernel_default_int_handler,	/* 0xffff00c0 */
	&kernel_default_int_handler,	/* 0xffff00c1 */
	&kernel_default_int_handler,	/* 0xffff00c2 */
	&kernel_default_int_handler,	/* 0xffff00c3 */
	&kernel_default_int_handler,	/* 0xffff00c4 */
	&kernel_default_int_handler,	/* 0xffff00c5 */
	&kernel_default_int_handler,	/* 0xffff00c6 */
	&kernel_default_int_handler,	/* 0xffff00c7 */
	&kernel_default_int_handler,	/* 0xffff00c8 */
	&kernel_default_int_handler,	/* 0xffff00c9 */
	&kernel_default_int_handler,	/* 0xffff00ca */
	&kernel_default_int_handler,	/* 0xffff00cb */
	&kernel_default_int_handler,	/* 0xffff00cc */
	&kernel_default_int_handler,	/* 0xffff00cd */
	&kernel_default_int_handler,	/* 0xffff00ce */
	&kernel_default_int_handler,	/* 0xffff00cf */
	&kernel_default_int_handler,	/* 0xffff00d0 */
	&kernel_default_int_handler,	/* 0xffff00d1 */
	&kernel_default_int_handler,	/* 0xffff00d2 */
	&kernel_default_int_handler,	/* 0xffff00d3 */
	&kernel_default_int_handler,	/* 0xffff00d4 */
	&kernel_default_int_handler,	/* 0xffff00d5 */
	&kernel_default_int_handler,	/* 0xffff00d6 */
	&kernel_default_int_handler,	/* 0xffff00d7 */
	&kernel_default_int_handler,	/* 0xffff00d8 */
	&kernel_default_int_handler,	/* 0xffff00d9 */
	&kernel_default_int_handler,	/* 0xffff00da */
	&kernel_default_int_handler,	/* 0xffff00db */
	&kernel_default_int_handler,	/* 0xffff00dc */
	&kernel_default_int_handler,	/* 0xffff00dd */
	&kernel_default_int_handler,	/* 0xffff00de */
	&kernel_default_int_handler,	/* 0xffff00df */
	&kernel_default_int_handler,	/* 0xffff00e0 */
	&kernel_default_int_handler,	/* 0xffff00e1 */
	&kernel_default_int_handler,	/* 0xffff00e2 */
	&kernel_default_int_handler,	/* 0xffff00e3 */
	&kernel_default_int_handler,	/* 0xffff00e4 */
	&kernel_default_int_handler,	/* 0xffff00e5 */
	&kernel_default_int_handler,	/* 0xffff00e6 */
	&kernel_default_int_handler,	/* 0xffff00e7 */
	&kernel_default_int_handler,	/* 0xffff00e8 */
	&kernel_default_int_handler,	/* 0xffff00e9 */
	&kernel_default_int_handler,	/* 0xffff00ea */
	&kernel_default_int_handler,	/* 0xffff00eb */
	&kernel_default_int_handler,	/* 0xffff00ec */
	&kernel_default_int_handler,	/* 0xffff00ed */
	&kernel_default_int_handler,	/* 0xffff00ee */
	&kernel_default_int_handler,	/* 0xffff00ef */
	&kernel_default_int_handler,	/* 0xffff00f0 */
	&kernel_default_int_handler,	/* 0xffff00f1 */
	&kernel_default_int_handler,	/* 0xffff00f2 */
	&kernel_default_int_handler,	/* 0xffff00f3 */
	&kernel_default_int_handler,	/* 0xffff00f4 */
	&kernel_default_int_handler,	/* 0xffff00f5 */
	&kernel_default_int_handler,	/* 0xffff00f6 */
	&kernel_default_int_handler,	/* 0xffff00f7 */
	&kernel_default_int_handler,	/* 0xffff00f8 */
	&kernel_default_int_handler,	/* 0xffff00f9 */
	&kernel_default_int_handler,	/* 0xffff00fa */
	&kernel_default_int_handler,	/* 0xffff00fb */
	&kernel_default_int_handler,	/* 0xffff00fc */
	&kernel_default_int_handler,	/* 0xffff00fd */
	&kernel_default_int_handler,	/* 0xffff00fe */
	&kernel_default_int_handler,	/* 0xffff00ff */
	&kernel_default_int_handler,	/* 0xffff0100 */
	&kernel_default_int_handler,	/* 0xffff0101 */
	&kernel_default_int_handler,	/* 0xffff0102 */
	&kernel_default_int_handler,	/* 0xffff0103 */
	&kernel_default_int_handler,	/* 0xffff0104 */
	&kernel_default_int_handler,	/* 0xffff0105 */
	&kernel_default_int_handler,	/* 0xffff0106 */
	&kernel_default_int_handler,	/* 0xffff0107 */
	&kernel_default_int_handler,	/* 0xffff0108 */
	&kernel_default_int_handler,	/* 0xffff0109 */
	&kernel_default_int_handler,	/* 0xffff010a */
	&kernel_default_int_handler,	/* 0xffff010b */
	&kernel_default_int_handler,	/* 0xffff010c */
	&kernel_default_int_handler,	/* 0xffff010d */
	&kernel_default_int_handler,	/* 0xffff010e */
	&kernel_default_int_handler,	/* 0xffff010f */
	&kernel_default_int_handler,	/* 0xffff0110 */
	&kernel_default_int_handler,	/* 0xffff0111 */
	&kernel_default_int_handler,	/* 0xffff0112 */
	&kernel_default_int_handler,	/* 0xffff0113 */
	&kernel_default_int_handler,	/* 0xffff0114 */
	&kernel_default_int_handler,	/* 0xffff0115 */
	&kernel_default_int_handler,	/* 0xffff0116 */
	&kernel_default_int_handler,	/* 0xffff0117 */
	&kernel_default_int_handler,	/* 0xffff0118 */
	&kernel_default_int_handler,	/* 0xffff0119 */
	&kernel_default_int_handler,	/* 0xffff011a */
	&kernel_default_int_handler,	/* 0xffff011b */
	&kernel_default_int_handler,	/* 0xffff011c */
	&kernel_default_int_handler,	/* 0xffff011d */
	&kernel_default_int_handler,	/* 0xffff011e */
	&kernel_default_int_handler,	/* 0xffff011f */
	&kernel_default_int_handler,	/* 0xffff0120 */
	&kernel_default_int_handler,	/* 0xffff0121 */
	&kernel_default_int_handler,	/* 0xffff0122 */
	&kernel_default_int_handler,	/* 0xffff0123 */
	&kernel_default_int_handler,	/* 0xffff0124 */
	&kernel_default_int_handler,	/* 0xffff0125 */
	&kernel_default_int_handler,	/* 0xffff0126 */
	&kernel_default_int_handler,	/* 0xffff0127 */
	&kernel_default_int_handler,	/* 0xffff0128 */
	&kernel_default_int_handler,	/* 0xffff0129 */
	&kernel_default_int_handler,	/* 0xffff012a */
	&kernel_default_int_handler,	/* 0xffff012b */
	&kernel_default_int_handler,	/* 0xffff012c */
	&kernel_default_int_handler,	/* 0xffff012d */
	&kernel_default_int_handler,	/* 0xffff012e */
	&kernel_default_int_handler,	/* 0xffff012f */
	&kernel_default_int_handler,	/* 0xffff0130 */
	&kernel_default_int_handler,	/* 0xffff0131 */
	&kernel_default_int_handler,	/* 0xffff0132 */
	&kernel_default_int_handler,	/* 0xffff0133 */
	&kernel_default_int_handler,	/* 0xffff0134 */
	&kernel_default_int_handler,	/* 0xffff0135 */
	&kernel_default_int_handler,	/* 0xffff0136 */
	&kernel_default_int_handler,	/* 0xffff0137 */
	&kernel_default_int_handler,	/* 0xffff0138 */
	&kernel_default_int_handler,	/* 0xffff0139 */
	&kernel_inthdr_0xffff013a,	/* 0xffff013a */
	&kernel_default_int_handler,	/* 0xffff013b */
	&kernel_default_int_handler,	/* 0xffff013c */
	&kernel_default_int_handler,	/* 0xffff013d */
	&kernel_default_int_handler,	/* 0xffff013e */
	&kernel_default_int_handler,	/* 0xffff013f */
	&kernel_default_int_handler,	/* 0xffff0140 */
	&kernel_default_int_handler,	/* 0xffff0141 */
	&kernel_default_int_handler,	/* 0xffff0142 */
	&kernel_default_int_handler,	/* 0xffff0143 */
	&kernel_default_int_handler,	/* 0xffff0144 */
	&kernel_default_int_handler,	/* 0xffff0145 */
	&kernel_default_int_handler,	/* 0xffff0146 */
	&kernel_default_int_handler,	/* 0xffff0147 */
	&kernel_default_int_handler,	/* 0xffff0148 */
	&kernel_default_int_handler,	/* 0xffff0149 */
	&kernel_default_int_handler,	/* 0xffff014a */
	&kernel_default_int_handler,	/* 0xffff014b */
	&kernel_default_int_handler,	/* 0xffff014c */
	&kernel_default_int_handler,	/* 0xffff014d */
	&kernel_default_int_handler,	/* 0xffff014e */
	&kernel_default_int_handler,	/* 0xffff014f */
	&kernel_default_int_handler,	/* 0xffff0150 */
	&kernel_default_int_handler,	/* 0xffff0151 */
	&kernel_default_int_handler,	/* 0xffff0152 */
	&kernel_default_int_handler,	/* 0xffff0153 */
	&kernel_default_int_handler,	/* 0xffff0154 */
	&kernel_default_int_handler,	/* 0xffff0155 */
	&kernel_default_int_handler,	/* 0xffff0156 */
	&kernel_default_int_handler,	/* 0xffff0157 */
	&kernel_default_int_handler,	/* 0xffff0158 */
	&kernel_default_int_handler,	/* 0xffff0159 */
	&kernel_default_int_handler,	/* 0xffff015a */
	&kernel_default_int_handler,	/* 0xffff015b */
	&kernel_default_int_handler,	/* 0xffff015c */
	&kernel_default_int_handler,	/* 0xffff015d */
	&kernel_default_int_handler	/* 0xffff015e */
};

const uint32 kernel_isr_table[TotalNumberOfCores] = {
	(const uint32) kernel_core0_isr_tbl,
	(const uint32) kernel_core1_isr_tbl
};

ISRCB * const kernel_core0_isr_p_isrcb_tbl[TNUM_INT] = {
	NULL,	/* 0x10000 */
	NULL,	/* 0x10001 */
	NULL,	/* 0x10002 */
	NULL,	/* 0x10003 */
	NULL,	/* 0x10004 */
	NULL,	/* 0x10005 */
	NULL,	/* 0x10006 */
	NULL,	/* 0x10007 */
	NULL,	/* 0x10008 */
	NULL,	/* 0x10009 */
	NULL,	/* 0x1000a */
	NULL,	/* 0x1000b */
	NULL,	/* 0x1000c */
	NULL,	/* 0x1000d */
	NULL,	/* 0x1000e */
	NULL,	/* 0x1000f */
	NULL,	/* 0x10010 */
	NULL,	/* 0x10011 */
	NULL,	/* 0x10012 */
	NULL,	/* 0x10013 */
	NULL,	/* 0x10014 */
	NULL,	/* 0x10015 */
	NULL,	/* 0x10016 */
	NULL,	/* 0x10017 */
	NULL,	/* 0x10018 */
	NULL,	/* 0x10019 */
	NULL,	/* 0x1001a */
	NULL,	/* 0x1001b */
	NULL,	/* 0x1001c */
	NULL,	/* 0x1001d */
	NULL,	/* 0x1001e */
	NULL,	/* 0x1001f */
	NULL,	/* 0xffff0020 */
	NULL,	/* 0xffff0021 */
	NULL,	/* 0xffff0022 */
	NULL,	/* 0xffff0023 */
	NULL,	/* 0xffff0024 */
	NULL,	/* 0xffff0025 */
	NULL,	/* 0xffff0026 */
	NULL,	/* 0xffff0027 */
	NULL,	/* 0xffff0028 */
	NULL,	/* 0xffff0029 */
	NULL,	/* 0xffff002a */
	NULL,	/* 0xffff002b */
	NULL,	/* 0xffff002c */
	NULL,	/* 0xffff002d */
	NULL,	/* 0xffff002e */
	NULL,	/* 0xffff002f */
	NULL,	/* 0xffff0030 */
	NULL,	/* 0xffff0031 */
	NULL,	/* 0xffff0032 */
	NULL,	/* 0xffff0033 */
	NULL,	/* 0xffff0034 */
	NULL,	/* 0xffff0035 */
	NULL,	/* 0xffff0036 */
	NULL,	/* 0xffff0037 */
	NULL,	/* 0xffff0038 */
	NULL,	/* 0xffff0039 */
	NULL,	/* 0xffff003a */
	NULL,	/* 0xffff003b */
	NULL,	/* 0xffff003c */
	NULL,	/* 0xffff003d */
	NULL,	/* 0xffff003e */
	NULL,	/* 0xffff003f */
	NULL,	/* 0xffff0040 */
	NULL,	/* 0xffff0041 */
	NULL,	/* 0xffff0042 */
	NULL,	/* 0xffff0043 */
	NULL,	/* 0xffff0044 */
	NULL,	/* 0xffff0045 */
	NULL,	/* 0xffff0046 */
	NULL,	/* 0xffff0047 */
	NULL,	/* 0xffff0048 */
	NULL,	/* 0xffff0049 */
	NULL,	/* 0xffff004a */
	NULL,	/* 0xffff004b */
	NULL,	/* 0xffff004c */
	NULL,	/* 0xffff004d */
	NULL,	/* 0xffff004e */
	NULL,	/* 0xffff004f */
	NULL,	/* 0xffff0050 */
	NULL,	/* 0xffff0051 */
	NULL,	/* 0xffff0052 */
	NULL,	/* 0xffff0053 */
	&kernel_isrcb_SwCntTimerHdr0,	/* 0xffff0054 */
	NULL,	/* 0xffff0055 */
	NULL,	/* 0xffff0056 */
	NULL,	/* 0xffff0057 */
	NULL,	/* 0xffff0058 */
	NULL,	/* 0xffff0059 */
	NULL,	/* 0xffff005a */
	NULL,	/* 0xffff005b */
	NULL,	/* 0xffff005c */
	NULL,	/* 0xffff005d */
	NULL,	/* 0xffff005e */
	NULL,	/* 0xffff005f */
	NULL,	/* 0xffff0060 */
	NULL,	/* 0xffff0061 */
	NULL,	/* 0xffff0062 */
	NULL,	/* 0xffff0063 */
	NULL,	/* 0xffff0064 */
	NULL,	/* 0xffff0065 */
	NULL,	/* 0xffff0066 */
	NULL,	/* 0xffff0067 */
	NULL,	/* 0xffff0068 */
	NULL,	/* 0xffff0069 */
	NULL,	/* 0xffff006a */
	NULL,	/* 0xffff006b */
	NULL,	/* 0xffff006c */
	NULL,	/* 0xffff006d */
	NULL,	/* 0xffff006e */
	NULL,	/* 0xffff006f */
	NULL,	/* 0xffff0070 */
	NULL,	/* 0xffff0071 */
	NULL,	/* 0xffff0072 */
	NULL,	/* 0xffff0073 */
	NULL,	/* 0xffff0074 */
	NULL,	/* 0xffff0075 */
	NULL,	/* 0xffff0076 */
	NULL,	/* 0xffff0077 */
	NULL,	/* 0xffff0078 */
	NULL,	/* 0xffff0079 */
	NULL,	/* 0xffff007a */
	NULL,	/* 0xffff007b */
	NULL,	/* 0xffff007c */
	NULL,	/* 0xffff007d */
	NULL,	/* 0xffff007e */
	NULL,	/* 0xffff007f */
	NULL,	/* 0xffff0080 */
	NULL,	/* 0xffff0081 */
	NULL,	/* 0xffff0082 */
	NULL,	/* 0xffff0083 */
	NULL,	/* 0xffff0084 */
	NULL,	/* 0xffff0085 */
	NULL,	/* 0xffff0086 */
	NULL,	/* 0xffff0087 */
	NULL,	/* 0xffff0088 */
	NULL,	/* 0xffff0089 */
	NULL,	/* 0xffff008a */
	NULL,	/* 0xffff008b */
	NULL,	/* 0xffff008c */
	NULL,	/* 0xffff008d */
	NULL,	/* 0xffff008e */
	NULL,	/* 0xffff008f */
	NULL,	/* 0xffff0090 */
	NULL,	/* 0xffff0091 */
	NULL,	/* 0xffff0092 */
	NULL,	/* 0xffff0093 */
	NULL,	/* 0xffff0094 */
	NULL,	/* 0xffff0095 */
	NULL,	/* 0xffff0096 */
	NULL,	/* 0xffff0097 */
	NULL,	/* 0xffff0098 */
	NULL,	/* 0xffff0099 */
	NULL,	/* 0xffff009a */
	NULL,	/* 0xffff009b */
	NULL,	/* 0xffff009c */
	NULL,	/* 0xffff009d */
	NULL,	/* 0xffff009e */
	NULL,	/* 0xffff009f */
	NULL,	/* 0xffff00a0 */
	NULL,	/* 0xffff00a1 */
	NULL,	/* 0xffff00a2 */
	NULL,	/* 0xffff00a3 */
	NULL,	/* 0xffff00a4 */
	&kernel_isrcb_RLIN3x_TX_ISR,	/* 0xffff00a5 */
	&kernel_isrcb_RLIN3x_RX_ISR,	/* 0xffff00a6 */
	NULL,	/* 0xffff00a7 */
	NULL,	/* 0xffff00a8 */
	NULL,	/* 0xffff00a9 */
	NULL,	/* 0xffff00aa */
	NULL,	/* 0xffff00ab */
	NULL,	/* 0xffff00ac */
	NULL,	/* 0xffff00ad */
	NULL,	/* 0xffff00ae */
	NULL,	/* 0xffff00af */
	NULL,	/* 0xffff00b0 */
	NULL,	/* 0xffff00b1 */
	NULL,	/* 0xffff00b2 */
	NULL,	/* 0xffff00b3 */
	NULL,	/* 0xffff00b4 */
	NULL,	/* 0xffff00b5 */
	NULL,	/* 0xffff00b6 */
	NULL,	/* 0xffff00b7 */
	NULL,	/* 0xffff00b8 */
	NULL,	/* 0xffff00b9 */
	NULL,	/* 0xffff00ba */
	NULL,	/* 0xffff00bb */
	NULL,	/* 0xffff00bc */
	NULL,	/* 0xffff00bd */
	NULL,	/* 0xffff00be */
	NULL,	/* 0xffff00bf */
	NULL,	/* 0xffff00c0 */
	NULL,	/* 0xffff00c1 */
	NULL,	/* 0xffff00c2 */
	NULL,	/* 0xffff00c3 */
	NULL,	/* 0xffff00c4 */
	NULL,	/* 0xffff00c5 */
	NULL,	/* 0xffff00c6 */
	NULL,	/* 0xffff00c7 */
	NULL,	/* 0xffff00c8 */
	NULL,	/* 0xffff00c9 */
	NULL,	/* 0xffff00ca */
	NULL,	/* 0xffff00cb */
	NULL,	/* 0xffff00cc */
	NULL,	/* 0xffff00cd */
	NULL,	/* 0xffff00ce */
	NULL,	/* 0xffff00cf */
	NULL,	/* 0xffff00d0 */
	NULL,	/* 0xffff00d1 */
	NULL,	/* 0xffff00d2 */
	NULL,	/* 0xffff00d3 */
	NULL,	/* 0xffff00d4 */
	NULL,	/* 0xffff00d5 */
	NULL,	/* 0xffff00d6 */
	NULL,	/* 0xffff00d7 */
	NULL,	/* 0xffff00d8 */
	NULL,	/* 0xffff00d9 */
	NULL,	/* 0xffff00da */
	NULL,	/* 0xffff00db */
	NULL,	/* 0xffff00dc */
	NULL,	/* 0xffff00dd */
	NULL,	/* 0xffff00de */
	NULL,	/* 0xffff00df */
	NULL,	/* 0xffff00e0 */
	NULL,	/* 0xffff00e1 */
	NULL,	/* 0xffff00e2 */
	NULL,	/* 0xffff00e3 */
	NULL,	/* 0xffff00e4 */
	NULL,	/* 0xffff00e5 */
	NULL,	/* 0xffff00e6 */
	NULL,	/* 0xffff00e7 */
	NULL,	/* 0xffff00e8 */
	NULL,	/* 0xffff00e9 */
	NULL,	/* 0xffff00ea */
	NULL,	/* 0xffff00eb */
	NULL,	/* 0xffff00ec */
	NULL,	/* 0xffff00ed */
	NULL,	/* 0xffff00ee */
	NULL,	/* 0xffff00ef */
	NULL,	/* 0xffff00f0 */
	NULL,	/* 0xffff00f1 */
	NULL,	/* 0xffff00f2 */
	NULL,	/* 0xffff00f3 */
	NULL,	/* 0xffff00f4 */
	NULL,	/* 0xffff00f5 */
	NULL,	/* 0xffff00f6 */
	NULL,	/* 0xffff00f7 */
	NULL,	/* 0xffff00f8 */
	NULL,	/* 0xffff00f9 */
	NULL,	/* 0xffff00fa */
	NULL,	/* 0xffff00fb */
	NULL,	/* 0xffff00fc */
	NULL,	/* 0xffff00fd */
	NULL,	/* 0xffff00fe */
	NULL,	/* 0xffff00ff */
	NULL,	/* 0xffff0100 */
	NULL,	/* 0xffff0101 */
	NULL,	/* 0xffff0102 */
	NULL,	/* 0xffff0103 */
	NULL,	/* 0xffff0104 */
	NULL,	/* 0xffff0105 */
	NULL,	/* 0xffff0106 */
	NULL,	/* 0xffff0107 */
	NULL,	/* 0xffff0108 */
	NULL,	/* 0xffff0109 */
	NULL,	/* 0xffff010a */
	NULL,	/* 0xffff010b */
	NULL,	/* 0xffff010c */
	NULL,	/* 0xffff010d */
	NULL,	/* 0xffff010e */
	NULL,	/* 0xffff010f */
	NULL,	/* 0xffff0110 */
	NULL,	/* 0xffff0111 */
	NULL,	/* 0xffff0112 */
	NULL,	/* 0xffff0113 */
	NULL,	/* 0xffff0114 */
	NULL,	/* 0xffff0115 */
	NULL,	/* 0xffff0116 */
	NULL,	/* 0xffff0117 */
	NULL,	/* 0xffff0118 */
	NULL,	/* 0xffff0119 */
	NULL,	/* 0xffff011a */
	NULL,	/* 0xffff011b */
	NULL,	/* 0xffff011c */
	NULL,	/* 0xffff011d */
	NULL,	/* 0xffff011e */
	NULL,	/* 0xffff011f */
	NULL,	/* 0xffff0120 */
	NULL,	/* 0xffff0121 */
	NULL,	/* 0xffff0122 */
	NULL,	/* 0xffff0123 */
	NULL,	/* 0xffff0124 */
	NULL,	/* 0xffff0125 */
	NULL,	/* 0xffff0126 */
	NULL,	/* 0xffff0127 */
	NULL,	/* 0xffff0128 */
	NULL,	/* 0xffff0129 */
	NULL,	/* 0xffff012a */
	NULL,	/* 0xffff012b */
	NULL,	/* 0xffff012c */
	NULL,	/* 0xffff012d */
	NULL,	/* 0xffff012e */
	NULL,	/* 0xffff012f */
	NULL,	/* 0xffff0130 */
	NULL,	/* 0xffff0131 */
	NULL,	/* 0xffff0132 */
	NULL,	/* 0xffff0133 */
	NULL,	/* 0xffff0134 */
	NULL,	/* 0xffff0135 */
	NULL,	/* 0xffff0136 */
	NULL,	/* 0xffff0137 */
	NULL,	/* 0xffff0138 */
	NULL,	/* 0xffff0139 */
	NULL,	/* 0xffff013a */
	NULL,	/* 0xffff013b */
	NULL,	/* 0xffff013c */
	NULL,	/* 0xffff013d */
	NULL,	/* 0xffff013e */
	NULL,	/* 0xffff013f */
	NULL,	/* 0xffff0140 */
	NULL,	/* 0xffff0141 */
	NULL,	/* 0xffff0142 */
	NULL,	/* 0xffff0143 */
	NULL,	/* 0xffff0144 */
	NULL,	/* 0xffff0145 */
	NULL,	/* 0xffff0146 */
	NULL,	/* 0xffff0147 */
	NULL,	/* 0xffff0148 */
	NULL,	/* 0xffff0149 */
	NULL,	/* 0xffff014a */
	NULL,	/* 0xffff014b */
	NULL,	/* 0xffff014c */
	NULL,	/* 0xffff014d */
	NULL,	/* 0xffff014e */
	NULL,	/* 0xffff014f */
	NULL,	/* 0xffff0150 */
	NULL,	/* 0xffff0151 */
	NULL,	/* 0xffff0152 */
	NULL,	/* 0xffff0153 */
	NULL,	/* 0xffff0154 */
	NULL,	/* 0xffff0155 */
	NULL,	/* 0xffff0156 */
	NULL,	/* 0xffff0157 */
	NULL,	/* 0xffff0158 */
	NULL,	/* 0xffff0159 */
	NULL,	/* 0xffff015a */
	NULL,	/* 0xffff015b */
	NULL,	/* 0xffff015c */
	NULL,	/* 0xffff015d */
	NULL	/* 0xffff015e */
};

ISRCB * const kernel_core1_isr_p_isrcb_tbl[TNUM_INT] = {
	NULL,	/* 0x20000 */
	NULL,	/* 0x20001 */
	NULL,	/* 0x20002 */
	NULL,	/* 0x20003 */
	NULL,	/* 0x20004 */
	NULL,	/* 0x20005 */
	NULL,	/* 0x20006 */
	NULL,	/* 0x20007 */
	NULL,	/* 0x20008 */
	NULL,	/* 0x20009 */
	NULL,	/* 0x2000a */
	NULL,	/* 0x2000b */
	NULL,	/* 0x2000c */
	NULL,	/* 0x2000d */
	NULL,	/* 0x2000e */
	NULL,	/* 0x2000f */
	NULL,	/* 0x20010 */
	NULL,	/* 0x20011 */
	NULL,	/* 0x20012 */
	NULL,	/* 0x20013 */
	NULL,	/* 0x20014 */
	NULL,	/* 0x20015 */
	NULL,	/* 0x20016 */
	NULL,	/* 0x20017 */
	NULL,	/* 0x20018 */
	NULL,	/* 0x20019 */
	NULL,	/* 0x2001a */
	NULL,	/* 0x2001b */
	NULL,	/* 0x2001c */
	NULL,	/* 0x2001d */
	NULL,	/* 0x2001e */
	NULL,	/* 0x2001f */
	NULL,	/* 0xffff0020 */
	NULL,	/* 0xffff0021 */
	NULL,	/* 0xffff0022 */
	NULL,	/* 0xffff0023 */
	NULL,	/* 0xffff0024 */
	NULL,	/* 0xffff0025 */
	NULL,	/* 0xffff0026 */
	NULL,	/* 0xffff0027 */
	NULL,	/* 0xffff0028 */
	NULL,	/* 0xffff0029 */
	NULL,	/* 0xffff002a */
	NULL,	/* 0xffff002b */
	NULL,	/* 0xffff002c */
	NULL,	/* 0xffff002d */
	NULL,	/* 0xffff002e */
	NULL,	/* 0xffff002f */
	NULL,	/* 0xffff0030 */
	NULL,	/* 0xffff0031 */
	NULL,	/* 0xffff0032 */
	NULL,	/* 0xffff0033 */
	NULL,	/* 0xffff0034 */
	NULL,	/* 0xffff0035 */
	NULL,	/* 0xffff0036 */
	NULL,	/* 0xffff0037 */
	NULL,	/* 0xffff0038 */
	NULL,	/* 0xffff0039 */
	NULL,	/* 0xffff003a */
	NULL,	/* 0xffff003b */
	NULL,	/* 0xffff003c */
	NULL,	/* 0xffff003d */
	NULL,	/* 0xffff003e */
	NULL,	/* 0xffff003f */
	NULL,	/* 0xffff0040 */
	NULL,	/* 0xffff0041 */
	NULL,	/* 0xffff0042 */
	NULL,	/* 0xffff0043 */
	NULL,	/* 0xffff0044 */
	NULL,	/* 0xffff0045 */
	NULL,	/* 0xffff0046 */
	NULL,	/* 0xffff0047 */
	NULL,	/* 0xffff0048 */
	NULL,	/* 0xffff0049 */
	NULL,	/* 0xffff004a */
	NULL,	/* 0xffff004b */
	NULL,	/* 0xffff004c */
	NULL,	/* 0xffff004d */
	NULL,	/* 0xffff004e */
	NULL,	/* 0xffff004f */
	NULL,	/* 0xffff0050 */
	NULL,	/* 0xffff0051 */
	NULL,	/* 0xffff0052 */
	NULL,	/* 0xffff0053 */
	NULL,	/* 0xffff0054 */
	NULL,	/* 0xffff0055 */
	NULL,	/* 0xffff0056 */
	NULL,	/* 0xffff0057 */
	NULL,	/* 0xffff0058 */
	NULL,	/* 0xffff0059 */
	NULL,	/* 0xffff005a */
	NULL,	/* 0xffff005b */
	NULL,	/* 0xffff005c */
	NULL,	/* 0xffff005d */
	NULL,	/* 0xffff005e */
	NULL,	/* 0xffff005f */
	NULL,	/* 0xffff0060 */
	NULL,	/* 0xffff0061 */
	NULL,	/* 0xffff0062 */
	NULL,	/* 0xffff0063 */
	NULL,	/* 0xffff0064 */
	NULL,	/* 0xffff0065 */
	NULL,	/* 0xffff0066 */
	NULL,	/* 0xffff0067 */
	NULL,	/* 0xffff0068 */
	NULL,	/* 0xffff0069 */
	NULL,	/* 0xffff006a */
	NULL,	/* 0xffff006b */
	NULL,	/* 0xffff006c */
	NULL,	/* 0xffff006d */
	NULL,	/* 0xffff006e */
	NULL,	/* 0xffff006f */
	NULL,	/* 0xffff0070 */
	NULL,	/* 0xffff0071 */
	NULL,	/* 0xffff0072 */
	NULL,	/* 0xffff0073 */
	NULL,	/* 0xffff0074 */
	NULL,	/* 0xffff0075 */
	NULL,	/* 0xffff0076 */
	NULL,	/* 0xffff0077 */
	NULL,	/* 0xffff0078 */
	NULL,	/* 0xffff0079 */
	NULL,	/* 0xffff007a */
	NULL,	/* 0xffff007b */
	NULL,	/* 0xffff007c */
	NULL,	/* 0xffff007d */
	NULL,	/* 0xffff007e */
	NULL,	/* 0xffff007f */
	NULL,	/* 0xffff0080 */
	NULL,	/* 0xffff0081 */
	NULL,	/* 0xffff0082 */
	NULL,	/* 0xffff0083 */
	NULL,	/* 0xffff0084 */
	NULL,	/* 0xffff0085 */
	NULL,	/* 0xffff0086 */
	NULL,	/* 0xffff0087 */
	NULL,	/* 0xffff0088 */
	NULL,	/* 0xffff0089 */
	NULL,	/* 0xffff008a */
	NULL,	/* 0xffff008b */
	NULL,	/* 0xffff008c */
	NULL,	/* 0xffff008d */
	NULL,	/* 0xffff008e */
	NULL,	/* 0xffff008f */
	NULL,	/* 0xffff0090 */
	NULL,	/* 0xffff0091 */
	NULL,	/* 0xffff0092 */
	NULL,	/* 0xffff0093 */
	NULL,	/* 0xffff0094 */
	NULL,	/* 0xffff0095 */
	NULL,	/* 0xffff0096 */
	NULL,	/* 0xffff0097 */
	NULL,	/* 0xffff0098 */
	NULL,	/* 0xffff0099 */
	NULL,	/* 0xffff009a */
	NULL,	/* 0xffff009b */
	NULL,	/* 0xffff009c */
	NULL,	/* 0xffff009d */
	NULL,	/* 0xffff009e */
	NULL,	/* 0xffff009f */
	NULL,	/* 0xffff00a0 */
	NULL,	/* 0xffff00a1 */
	NULL,	/* 0xffff00a2 */
	NULL,	/* 0xffff00a3 */
	NULL,	/* 0xffff00a4 */
	NULL,	/* 0xffff00a5 */
	NULL,	/* 0xffff00a6 */
	NULL,	/* 0xffff00a7 */
	NULL,	/* 0xffff00a8 */
	NULL,	/* 0xffff00a9 */
	NULL,	/* 0xffff00aa */
	NULL,	/* 0xffff00ab */
	NULL,	/* 0xffff00ac */
	NULL,	/* 0xffff00ad */
	NULL,	/* 0xffff00ae */
	NULL,	/* 0xffff00af */
	NULL,	/* 0xffff00b0 */
	NULL,	/* 0xffff00b1 */
	NULL,	/* 0xffff00b2 */
	NULL,	/* 0xffff00b3 */
	NULL,	/* 0xffff00b4 */
	NULL,	/* 0xffff00b5 */
	NULL,	/* 0xffff00b6 */
	NULL,	/* 0xffff00b7 */
	NULL,	/* 0xffff00b8 */
	NULL,	/* 0xffff00b9 */
	NULL,	/* 0xffff00ba */
	NULL,	/* 0xffff00bb */
	NULL,	/* 0xffff00bc */
	NULL,	/* 0xffff00bd */
	NULL,	/* 0xffff00be */
	NULL,	/* 0xffff00bf */
	NULL,	/* 0xffff00c0 */
	NULL,	/* 0xffff00c1 */
	NULL,	/* 0xffff00c2 */
	NULL,	/* 0xffff00c3 */
	NULL,	/* 0xffff00c4 */
	NULL,	/* 0xffff00c5 */
	NULL,	/* 0xffff00c6 */
	NULL,	/* 0xffff00c7 */
	NULL,	/* 0xffff00c8 */
	NULL,	/* 0xffff00c9 */
	NULL,	/* 0xffff00ca */
	NULL,	/* 0xffff00cb */
	NULL,	/* 0xffff00cc */
	NULL,	/* 0xffff00cd */
	NULL,	/* 0xffff00ce */
	NULL,	/* 0xffff00cf */
	NULL,	/* 0xffff00d0 */
	NULL,	/* 0xffff00d1 */
	NULL,	/* 0xffff00d2 */
	NULL,	/* 0xffff00d3 */
	NULL,	/* 0xffff00d4 */
	NULL,	/* 0xffff00d5 */
	NULL,	/* 0xffff00d6 */
	NULL,	/* 0xffff00d7 */
	NULL,	/* 0xffff00d8 */
	NULL,	/* 0xffff00d9 */
	NULL,	/* 0xffff00da */
	NULL,	/* 0xffff00db */
	NULL,	/* 0xffff00dc */
	NULL,	/* 0xffff00dd */
	NULL,	/* 0xffff00de */
	NULL,	/* 0xffff00df */
	NULL,	/* 0xffff00e0 */
	NULL,	/* 0xffff00e1 */
	NULL,	/* 0xffff00e2 */
	NULL,	/* 0xffff00e3 */
	NULL,	/* 0xffff00e4 */
	NULL,	/* 0xffff00e5 */
	NULL,	/* 0xffff00e6 */
	NULL,	/* 0xffff00e7 */
	NULL,	/* 0xffff00e8 */
	NULL,	/* 0xffff00e9 */
	NULL,	/* 0xffff00ea */
	NULL,	/* 0xffff00eb */
	NULL,	/* 0xffff00ec */
	NULL,	/* 0xffff00ed */
	NULL,	/* 0xffff00ee */
	NULL,	/* 0xffff00ef */
	NULL,	/* 0xffff00f0 */
	NULL,	/* 0xffff00f1 */
	NULL,	/* 0xffff00f2 */
	NULL,	/* 0xffff00f3 */
	NULL,	/* 0xffff00f4 */
	NULL,	/* 0xffff00f5 */
	NULL,	/* 0xffff00f6 */
	NULL,	/* 0xffff00f7 */
	NULL,	/* 0xffff00f8 */
	NULL,	/* 0xffff00f9 */
	NULL,	/* 0xffff00fa */
	NULL,	/* 0xffff00fb */
	NULL,	/* 0xffff00fc */
	NULL,	/* 0xffff00fd */
	NULL,	/* 0xffff00fe */
	NULL,	/* 0xffff00ff */
	NULL,	/* 0xffff0100 */
	NULL,	/* 0xffff0101 */
	NULL,	/* 0xffff0102 */
	NULL,	/* 0xffff0103 */
	NULL,	/* 0xffff0104 */
	NULL,	/* 0xffff0105 */
	NULL,	/* 0xffff0106 */
	NULL,	/* 0xffff0107 */
	NULL,	/* 0xffff0108 */
	NULL,	/* 0xffff0109 */
	NULL,	/* 0xffff010a */
	NULL,	/* 0xffff010b */
	NULL,	/* 0xffff010c */
	NULL,	/* 0xffff010d */
	NULL,	/* 0xffff010e */
	NULL,	/* 0xffff010f */
	NULL,	/* 0xffff0110 */
	NULL,	/* 0xffff0111 */
	NULL,	/* 0xffff0112 */
	NULL,	/* 0xffff0113 */
	NULL,	/* 0xffff0114 */
	NULL,	/* 0xffff0115 */
	NULL,	/* 0xffff0116 */
	NULL,	/* 0xffff0117 */
	NULL,	/* 0xffff0118 */
	NULL,	/* 0xffff0119 */
	NULL,	/* 0xffff011a */
	NULL,	/* 0xffff011b */
	NULL,	/* 0xffff011c */
	NULL,	/* 0xffff011d */
	NULL,	/* 0xffff011e */
	NULL,	/* 0xffff011f */
	NULL,	/* 0xffff0120 */
	NULL,	/* 0xffff0121 */
	NULL,	/* 0xffff0122 */
	NULL,	/* 0xffff0123 */
	NULL,	/* 0xffff0124 */
	NULL,	/* 0xffff0125 */
	NULL,	/* 0xffff0126 */
	NULL,	/* 0xffff0127 */
	NULL,	/* 0xffff0128 */
	NULL,	/* 0xffff0129 */
	NULL,	/* 0xffff012a */
	NULL,	/* 0xffff012b */
	NULL,	/* 0xffff012c */
	NULL,	/* 0xffff012d */
	NULL,	/* 0xffff012e */
	NULL,	/* 0xffff012f */
	NULL,	/* 0xffff0130 */
	NULL,	/* 0xffff0131 */
	NULL,	/* 0xffff0132 */
	NULL,	/* 0xffff0133 */
	NULL,	/* 0xffff0134 */
	NULL,	/* 0xffff0135 */
	NULL,	/* 0xffff0136 */
	NULL,	/* 0xffff0137 */
	NULL,	/* 0xffff0138 */
	NULL,	/* 0xffff0139 */
	&kernel_isrcb_SwCntTimerHdr1,	/* 0xffff013a */
	NULL,	/* 0xffff013b */
	NULL,	/* 0xffff013c */
	NULL,	/* 0xffff013d */
	NULL,	/* 0xffff013e */
	NULL,	/* 0xffff013f */
	NULL,	/* 0xffff0140 */
	NULL,	/* 0xffff0141 */
	NULL,	/* 0xffff0142 */
	NULL,	/* 0xffff0143 */
	NULL,	/* 0xffff0144 */
	NULL,	/* 0xffff0145 */
	NULL,	/* 0xffff0146 */
	NULL,	/* 0xffff0147 */
	NULL,	/* 0xffff0148 */
	NULL,	/* 0xffff0149 */
	NULL,	/* 0xffff014a */
	NULL,	/* 0xffff014b */
	NULL,	/* 0xffff014c */
	NULL,	/* 0xffff014d */
	NULL,	/* 0xffff014e */
	NULL,	/* 0xffff014f */
	NULL,	/* 0xffff0150 */
	NULL,	/* 0xffff0151 */
	NULL,	/* 0xffff0152 */
	NULL,	/* 0xffff0153 */
	NULL,	/* 0xffff0154 */
	NULL,	/* 0xffff0155 */
	NULL,	/* 0xffff0156 */
	NULL,	/* 0xffff0157 */
	NULL,	/* 0xffff0158 */
	NULL,	/* 0xffff0159 */
	NULL,	/* 0xffff015a */
	NULL,	/* 0xffff015b */
	NULL,	/* 0xffff015c */
	NULL,	/* 0xffff015d */
	NULL	/* 0xffff015e */
};

const uint32 kernel_isr_p_isrcb_table[TotalNumberOfCores] = {
	(const uint32) kernel_core0_isr_p_isrcb_tbl,
	(const uint32) kernel_core1_isr_p_isrcb_tbl
};

extern void kernel_interrupt(void);
const uint32 __attribute__((aligned(512))) kernel_core0_intbp_tbl[TNUM_INT] = {
	(uint32)&kernel_interrupt,	/* 0x10000 */
	(uint32)&kernel_interrupt,	/* 0x10001 */
	(uint32)&kernel_interrupt,	/* 0x10002 */
	(uint32)&kernel_interrupt,	/* 0x10003 */
	(uint32)&kernel_interrupt,	/* 0x10004 */
	(uint32)&kernel_interrupt,	/* 0x10005 */
	(uint32)&kernel_interrupt,	/* 0x10006 */
	(uint32)&kernel_interrupt,	/* 0x10007 */
	(uint32)&kernel_interrupt,	/* 0x10008 */
	(uint32)&kernel_interrupt,	/* 0x10009 */
	(uint32)&kernel_interrupt,	/* 0x1000a */
	(uint32)&kernel_interrupt,	/* 0x1000b */
	(uint32)&kernel_interrupt,	/* 0x1000c */
	(uint32)&kernel_interrupt,	/* 0x1000d */
	(uint32)&kernel_interrupt,	/* 0x1000e */
	(uint32)&kernel_interrupt,	/* 0x1000f */
	(uint32)&kernel_interrupt,	/* 0x10010 */
	(uint32)&kernel_interrupt,	/* 0x10011 */
	(uint32)&kernel_interrupt,	/* 0x10012 */
	(uint32)&kernel_interrupt,	/* 0x10013 */
	(uint32)&kernel_interrupt,	/* 0x10014 */
	(uint32)&kernel_interrupt,	/* 0x10015 */
	(uint32)&kernel_interrupt,	/* 0x10016 */
	(uint32)&kernel_interrupt,	/* 0x10017 */
	(uint32)&kernel_interrupt,	/* 0x10018 */
	(uint32)&kernel_interrupt,	/* 0x10019 */
	(uint32)&kernel_interrupt,	/* 0x1001a */
	(uint32)&kernel_interrupt,	/* 0x1001b */
	(uint32)&kernel_interrupt,	/* 0x1001c */
	(uint32)&kernel_interrupt,	/* 0x1001d */
	(uint32)&kernel_interrupt,	/* 0x1001e */
	(uint32)&kernel_interrupt,	/* 0x1001f */
	(uint32)&kernel_interrupt,	/* 0xffff0020 */
	(uint32)&kernel_interrupt,	/* 0xffff0021 */
	(uint32)&kernel_interrupt,	/* 0xffff0022 */
	(uint32)&kernel_interrupt,	/* 0xffff0023 */
	(uint32)&kernel_interrupt,	/* 0xffff0024 */
	(uint32)&kernel_interrupt,	/* 0xffff0025 */
	(uint32)&kernel_interrupt,	/* 0xffff0026 */
	(uint32)&kernel_interrupt,	/* 0xffff0027 */
	(uint32)&kernel_interrupt,	/* 0xffff0028 */
	(uint32)&kernel_interrupt,	/* 0xffff0029 */
	(uint32)&kernel_interrupt,	/* 0xffff002a */
	(uint32)&kernel_interrupt,	/* 0xffff002b */
	(uint32)&kernel_interrupt,	/* 0xffff002c */
	(uint32)&kernel_interrupt,	/* 0xffff002d */
	(uint32)&kernel_interrupt,	/* 0xffff002e */
	(uint32)&kernel_interrupt,	/* 0xffff002f */
	(uint32)&kernel_interrupt,	/* 0xffff0030 */
	(uint32)&kernel_interrupt,	/* 0xffff0031 */
	(uint32)&kernel_interrupt,	/* 0xffff0032 */
	(uint32)&kernel_interrupt,	/* 0xffff0033 */
	(uint32)&kernel_interrupt,	/* 0xffff0034 */
	(uint32)&kernel_interrupt,	/* 0xffff0035 */
	(uint32)&kernel_interrupt,	/* 0xffff0036 */
	(uint32)&kernel_interrupt,	/* 0xffff0037 */
	(uint32)&kernel_interrupt,	/* 0xffff0038 */
	(uint32)&kernel_interrupt,	/* 0xffff0039 */
	(uint32)&kernel_interrupt,	/* 0xffff003a */
	(uint32)&kernel_interrupt,	/* 0xffff003b */
	(uint32)&kernel_interrupt,	/* 0xffff003c */
	(uint32)&kernel_interrupt,	/* 0xffff003d */
	(uint32)&kernel_interrupt,	/* 0xffff003e */
	(uint32)&kernel_interrupt,	/* 0xffff003f */
	(uint32)&kernel_interrupt,	/* 0xffff0040 */
	(uint32)&kernel_interrupt,	/* 0xffff0041 */
	(uint32)&kernel_interrupt,	/* 0xffff0042 */
	(uint32)&kernel_interrupt,	/* 0xffff0043 */
	(uint32)&kernel_interrupt,	/* 0xffff0044 */
	(uint32)&kernel_interrupt,	/* 0xffff0045 */
	(uint32)&kernel_interrupt,	/* 0xffff0046 */
	(uint32)&kernel_interrupt,	/* 0xffff0047 */
	(uint32)&kernel_interrupt,	/* 0xffff0048 */
	(uint32)&kernel_interrupt,	/* 0xffff0049 */
	(uint32)&kernel_interrupt,	/* 0xffff004a */
	(uint32)&kernel_interrupt,	/* 0xffff004b */
	(uint32)&kernel_interrupt,	/* 0xffff004c */
	(uint32)&kernel_interrupt,	/* 0xffff004d */
	(uint32)&kernel_interrupt,	/* 0xffff004e */
	(uint32)&kernel_interrupt,	/* 0xffff004f */
	(uint32)&kernel_interrupt,	/* 0xffff0050 */
	(uint32)&kernel_interrupt,	/* 0xffff0051 */
	(uint32)&kernel_interrupt,	/* 0xffff0052 */
	(uint32)&kernel_interrupt,	/* 0xffff0053 */
	(uint32)&kernel_interrupt,	/* 0xffff0054 */
	(uint32)&kernel_interrupt,	/* 0xffff0055 */
	(uint32)&kernel_interrupt,	/* 0xffff0056 */
	(uint32)&kernel_interrupt,	/* 0xffff0057 */
	(uint32)&kernel_interrupt,	/* 0xffff0058 */
	(uint32)&kernel_interrupt,	/* 0xffff0059 */
	(uint32)&kernel_interrupt,	/* 0xffff005a */
	(uint32)&kernel_interrupt,	/* 0xffff005b */
	(uint32)&kernel_interrupt,	/* 0xffff005c */
	(uint32)&kernel_interrupt,	/* 0xffff005d */
	(uint32)&kernel_interrupt,	/* 0xffff005e */
	(uint32)&kernel_interrupt,	/* 0xffff005f */
	(uint32)&kernel_interrupt,	/* 0xffff0060 */
	(uint32)&kernel_interrupt,	/* 0xffff0061 */
	(uint32)&kernel_interrupt,	/* 0xffff0062 */
	(uint32)&kernel_interrupt,	/* 0xffff0063 */
	(uint32)&kernel_interrupt,	/* 0xffff0064 */
	(uint32)&kernel_interrupt,	/* 0xffff0065 */
	(uint32)&kernel_interrupt,	/* 0xffff0066 */
	(uint32)&kernel_interrupt,	/* 0xffff0067 */
	(uint32)&kernel_interrupt,	/* 0xffff0068 */
	(uint32)&kernel_interrupt,	/* 0xffff0069 */
	(uint32)&kernel_interrupt,	/* 0xffff006a */
	(uint32)&kernel_interrupt,	/* 0xffff006b */
	(uint32)&kernel_interrupt,	/* 0xffff006c */
	(uint32)&kernel_interrupt,	/* 0xffff006d */
	(uint32)&kernel_interrupt,	/* 0xffff006e */
	(uint32)&kernel_interrupt,	/* 0xffff006f */
	(uint32)&kernel_interrupt,	/* 0xffff0070 */
	(uint32)&kernel_interrupt,	/* 0xffff0071 */
	(uint32)&kernel_interrupt,	/* 0xffff0072 */
	(uint32)&kernel_interrupt,	/* 0xffff0073 */
	(uint32)&kernel_interrupt,	/* 0xffff0074 */
	(uint32)&kernel_interrupt,	/* 0xffff0075 */
	(uint32)&kernel_interrupt,	/* 0xffff0076 */
	(uint32)&kernel_interrupt,	/* 0xffff0077 */
	(uint32)&kernel_interrupt,	/* 0xffff0078 */
	(uint32)&kernel_interrupt,	/* 0xffff0079 */
	(uint32)&kernel_interrupt,	/* 0xffff007a */
	(uint32)&kernel_interrupt,	/* 0xffff007b */
	(uint32)&kernel_interrupt,	/* 0xffff007c */
	(uint32)&kernel_interrupt,	/* 0xffff007d */
	(uint32)&kernel_interrupt,	/* 0xffff007e */
	(uint32)&kernel_interrupt,	/* 0xffff007f */
	(uint32)&kernel_interrupt,	/* 0xffff0080 */
	(uint32)&kernel_interrupt,	/* 0xffff0081 */
	(uint32)&kernel_interrupt,	/* 0xffff0082 */
	(uint32)&kernel_interrupt,	/* 0xffff0083 */
	(uint32)&kernel_interrupt,	/* 0xffff0084 */
	(uint32)&kernel_interrupt,	/* 0xffff0085 */
	(uint32)&kernel_interrupt,	/* 0xffff0086 */
	(uint32)&kernel_interrupt,	/* 0xffff0087 */
	(uint32)&kernel_interrupt,	/* 0xffff0088 */
	(uint32)&kernel_interrupt,	/* 0xffff0089 */
	(uint32)&kernel_interrupt,	/* 0xffff008a */
	(uint32)&kernel_interrupt,	/* 0xffff008b */
	(uint32)&kernel_interrupt,	/* 0xffff008c */
	(uint32)&kernel_interrupt,	/* 0xffff008d */
	(uint32)&kernel_interrupt,	/* 0xffff008e */
	(uint32)&kernel_interrupt,	/* 0xffff008f */
	(uint32)&kernel_interrupt,	/* 0xffff0090 */
	(uint32)&kernel_interrupt,	/* 0xffff0091 */
	(uint32)&kernel_interrupt,	/* 0xffff0092 */
	(uint32)&kernel_interrupt,	/* 0xffff0093 */
	(uint32)&kernel_interrupt,	/* 0xffff0094 */
	(uint32)&kernel_interrupt,	/* 0xffff0095 */
	(uint32)&kernel_interrupt,	/* 0xffff0096 */
	(uint32)&kernel_interrupt,	/* 0xffff0097 */
	(uint32)&kernel_interrupt,	/* 0xffff0098 */
	(uint32)&kernel_interrupt,	/* 0xffff0099 */
	(uint32)&kernel_interrupt,	/* 0xffff009a */
	(uint32)&kernel_interrupt,	/* 0xffff009b */
	(uint32)&kernel_interrupt,	/* 0xffff009c */
	(uint32)&kernel_interrupt,	/* 0xffff009d */
	(uint32)&kernel_interrupt,	/* 0xffff009e */
	(uint32)&kernel_interrupt,	/* 0xffff009f */
	(uint32)&kernel_interrupt,	/* 0xffff00a0 */
	(uint32)&kernel_interrupt,	/* 0xffff00a1 */
	(uint32)&kernel_interrupt,	/* 0xffff00a2 */
	(uint32)&kernel_interrupt,	/* 0xffff00a3 */
	(uint32)&kernel_interrupt,	/* 0xffff00a4 */
	(uint32)&kernel_interrupt,	/* 0xffff00a5 */
	(uint32)&kernel_interrupt,	/* 0xffff00a6 */
	(uint32)&kernel_interrupt,	/* 0xffff00a7 */
	(uint32)&kernel_interrupt,	/* 0xffff00a8 */
	(uint32)&kernel_interrupt,	/* 0xffff00a9 */
	(uint32)&kernel_interrupt,	/* 0xffff00aa */
	(uint32)&kernel_interrupt,	/* 0xffff00ab */
	(uint32)&kernel_interrupt,	/* 0xffff00ac */
	(uint32)&kernel_interrupt,	/* 0xffff00ad */
	(uint32)&kernel_interrupt,	/* 0xffff00ae */
	(uint32)&kernel_interrupt,	/* 0xffff00af */
	(uint32)&kernel_interrupt,	/* 0xffff00b0 */
	(uint32)&kernel_interrupt,	/* 0xffff00b1 */
	(uint32)&kernel_interrupt,	/* 0xffff00b2 */
	(uint32)&kernel_interrupt,	/* 0xffff00b3 */
	(uint32)&kernel_interrupt,	/* 0xffff00b4 */
	(uint32)&kernel_interrupt,	/* 0xffff00b5 */
	(uint32)&kernel_interrupt,	/* 0xffff00b6 */
	(uint32)&kernel_interrupt,	/* 0xffff00b7 */
	(uint32)&kernel_interrupt,	/* 0xffff00b8 */
	(uint32)&kernel_interrupt,	/* 0xffff00b9 */
	(uint32)&kernel_interrupt,	/* 0xffff00ba */
	(uint32)&kernel_interrupt,	/* 0xffff00bb */
	(uint32)&kernel_interrupt,	/* 0xffff00bc */
	(uint32)&kernel_interrupt,	/* 0xffff00bd */
	(uint32)&kernel_interrupt,	/* 0xffff00be */
	(uint32)&kernel_interrupt,	/* 0xffff00bf */
	(uint32)&kernel_interrupt,	/* 0xffff00c0 */
	(uint32)&kernel_interrupt,	/* 0xffff00c1 */
	(uint32)&kernel_interrupt,	/* 0xffff00c2 */
	(uint32)&kernel_interrupt,	/* 0xffff00c3 */
	(uint32)&kernel_interrupt,	/* 0xffff00c4 */
	(uint32)&kernel_interrupt,	/* 0xffff00c5 */
	(uint32)&kernel_interrupt,	/* 0xffff00c6 */
	(uint32)&kernel_interrupt,	/* 0xffff00c7 */
	(uint32)&kernel_interrupt,	/* 0xffff00c8 */
	(uint32)&kernel_interrupt,	/* 0xffff00c9 */
	(uint32)&kernel_interrupt,	/* 0xffff00ca */
	(uint32)&kernel_interrupt,	/* 0xffff00cb */
	(uint32)&kernel_interrupt,	/* 0xffff00cc */
	(uint32)&kernel_interrupt,	/* 0xffff00cd */
	(uint32)&kernel_interrupt,	/* 0xffff00ce */
	(uint32)&kernel_interrupt,	/* 0xffff00cf */
	(uint32)&kernel_interrupt,	/* 0xffff00d0 */
	(uint32)&kernel_interrupt,	/* 0xffff00d1 */
	(uint32)&kernel_interrupt,	/* 0xffff00d2 */
	(uint32)&kernel_interrupt,	/* 0xffff00d3 */
	(uint32)&kernel_interrupt,	/* 0xffff00d4 */
	(uint32)&kernel_interrupt,	/* 0xffff00d5 */
	(uint32)&kernel_interrupt,	/* 0xffff00d6 */
	(uint32)&kernel_interrupt,	/* 0xffff00d7 */
	(uint32)&kernel_interrupt,	/* 0xffff00d8 */
	(uint32)&kernel_interrupt,	/* 0xffff00d9 */
	(uint32)&kernel_interrupt,	/* 0xffff00da */
	(uint32)&kernel_interrupt,	/* 0xffff00db */
	(uint32)&kernel_interrupt,	/* 0xffff00dc */
	(uint32)&kernel_interrupt,	/* 0xffff00dd */
	(uint32)&kernel_interrupt,	/* 0xffff00de */
	(uint32)&kernel_interrupt,	/* 0xffff00df */
	(uint32)&kernel_interrupt,	/* 0xffff00e0 */
	(uint32)&kernel_interrupt,	/* 0xffff00e1 */
	(uint32)&kernel_interrupt,	/* 0xffff00e2 */
	(uint32)&kernel_interrupt,	/* 0xffff00e3 */
	(uint32)&kernel_interrupt,	/* 0xffff00e4 */
	(uint32)&kernel_interrupt,	/* 0xffff00e5 */
	(uint32)&kernel_interrupt,	/* 0xffff00e6 */
	(uint32)&kernel_interrupt,	/* 0xffff00e7 */
	(uint32)&kernel_interrupt,	/* 0xffff00e8 */
	(uint32)&kernel_interrupt,	/* 0xffff00e9 */
	(uint32)&kernel_interrupt,	/* 0xffff00ea */
	(uint32)&kernel_interrupt,	/* 0xffff00eb */
	(uint32)&kernel_interrupt,	/* 0xffff00ec */
	(uint32)&kernel_interrupt,	/* 0xffff00ed */
	(uint32)&kernel_interrupt,	/* 0xffff00ee */
	(uint32)&kernel_interrupt,	/* 0xffff00ef */
	(uint32)&kernel_interrupt,	/* 0xffff00f0 */
	(uint32)&kernel_interrupt,	/* 0xffff00f1 */
	(uint32)&kernel_interrupt,	/* 0xffff00f2 */
	(uint32)&kernel_interrupt,	/* 0xffff00f3 */
	(uint32)&kernel_interrupt,	/* 0xffff00f4 */
	(uint32)&kernel_interrupt,	/* 0xffff00f5 */
	(uint32)&kernel_interrupt,	/* 0xffff00f6 */
	(uint32)&kernel_interrupt,	/* 0xffff00f7 */
	(uint32)&kernel_interrupt,	/* 0xffff00f8 */
	(uint32)&kernel_interrupt,	/* 0xffff00f9 */
	(uint32)&kernel_interrupt,	/* 0xffff00fa */
	(uint32)&kernel_interrupt,	/* 0xffff00fb */
	(uint32)&kernel_interrupt,	/* 0xffff00fc */
	(uint32)&kernel_interrupt,	/* 0xffff00fd */
	(uint32)&kernel_interrupt,	/* 0xffff00fe */
	(uint32)&kernel_interrupt,	/* 0xffff00ff */
	(uint32)&kernel_interrupt,	/* 0xffff0100 */
	(uint32)&kernel_interrupt,	/* 0xffff0101 */
	(uint32)&kernel_interrupt,	/* 0xffff0102 */
	(uint32)&kernel_interrupt,	/* 0xffff0103 */
	(uint32)&kernel_interrupt,	/* 0xffff0104 */
	(uint32)&kernel_interrupt,	/* 0xffff0105 */
	(uint32)&kernel_interrupt,	/* 0xffff0106 */
	(uint32)&kernel_interrupt,	/* 0xffff0107 */
	(uint32)&kernel_interrupt,	/* 0xffff0108 */
	(uint32)&kernel_interrupt,	/* 0xffff0109 */
	(uint32)&kernel_interrupt,	/* 0xffff010a */
	(uint32)&kernel_interrupt,	/* 0xffff010b */
	(uint32)&kernel_interrupt,	/* 0xffff010c */
	(uint32)&kernel_interrupt,	/* 0xffff010d */
	(uint32)&kernel_interrupt,	/* 0xffff010e */
	(uint32)&kernel_interrupt,	/* 0xffff010f */
	(uint32)&kernel_interrupt,	/* 0xffff0110 */
	(uint32)&kernel_interrupt,	/* 0xffff0111 */
	(uint32)&kernel_interrupt,	/* 0xffff0112 */
	(uint32)&kernel_interrupt,	/* 0xffff0113 */
	(uint32)&kernel_interrupt,	/* 0xffff0114 */
	(uint32)&kernel_interrupt,	/* 0xffff0115 */
	(uint32)&kernel_interrupt,	/* 0xffff0116 */
	(uint32)&kernel_interrupt,	/* 0xffff0117 */
	(uint32)&kernel_interrupt,	/* 0xffff0118 */
	(uint32)&kernel_interrupt,	/* 0xffff0119 */
	(uint32)&kernel_interrupt,	/* 0xffff011a */
	(uint32)&kernel_interrupt,	/* 0xffff011b */
	(uint32)&kernel_interrupt,	/* 0xffff011c */
	(uint32)&kernel_interrupt,	/* 0xffff011d */
	(uint32)&kernel_interrupt,	/* 0xffff011e */
	(uint32)&kernel_interrupt,	/* 0xffff011f */
	(uint32)&kernel_interrupt,	/* 0xffff0120 */
	(uint32)&kernel_interrupt,	/* 0xffff0121 */
	(uint32)&kernel_interrupt,	/* 0xffff0122 */
	(uint32)&kernel_interrupt,	/* 0xffff0123 */
	(uint32)&kernel_interrupt,	/* 0xffff0124 */
	(uint32)&kernel_interrupt,	/* 0xffff0125 */
	(uint32)&kernel_interrupt,	/* 0xffff0126 */
	(uint32)&kernel_interrupt,	/* 0xffff0127 */
	(uint32)&kernel_interrupt,	/* 0xffff0128 */
	(uint32)&kernel_interrupt,	/* 0xffff0129 */
	(uint32)&kernel_interrupt,	/* 0xffff012a */
	(uint32)&kernel_interrupt,	/* 0xffff012b */
	(uint32)&kernel_interrupt,	/* 0xffff012c */
	(uint32)&kernel_interrupt,	/* 0xffff012d */
	(uint32)&kernel_interrupt,	/* 0xffff012e */
	(uint32)&kernel_interrupt,	/* 0xffff012f */
	(uint32)&kernel_interrupt,	/* 0xffff0130 */
	(uint32)&kernel_interrupt,	/* 0xffff0131 */
	(uint32)&kernel_interrupt,	/* 0xffff0132 */
	(uint32)&kernel_interrupt,	/* 0xffff0133 */
	(uint32)&kernel_interrupt,	/* 0xffff0134 */
	(uint32)&kernel_interrupt,	/* 0xffff0135 */
	(uint32)&kernel_interrupt,	/* 0xffff0136 */
	(uint32)&kernel_interrupt,	/* 0xffff0137 */
	(uint32)&kernel_interrupt,	/* 0xffff0138 */
	(uint32)&kernel_interrupt,	/* 0xffff0139 */
	(uint32)&kernel_interrupt,	/* 0xffff013a */
	(uint32)&kernel_interrupt,	/* 0xffff013b */
	(uint32)&kernel_interrupt,	/* 0xffff013c */
	(uint32)&kernel_interrupt,	/* 0xffff013d */
	(uint32)&kernel_interrupt,	/* 0xffff013e */
	(uint32)&kernel_interrupt,	/* 0xffff013f */
	(uint32)&kernel_interrupt,	/* 0xffff0140 */
	(uint32)&kernel_interrupt,	/* 0xffff0141 */
	(uint32)&kernel_interrupt,	/* 0xffff0142 */
	(uint32)&kernel_interrupt,	/* 0xffff0143 */
	(uint32)&kernel_interrupt,	/* 0xffff0144 */
	(uint32)&kernel_interrupt,	/* 0xffff0145 */
	(uint32)&kernel_interrupt,	/* 0xffff0146 */
	(uint32)&kernel_interrupt,	/* 0xffff0147 */
	(uint32)&kernel_interrupt,	/* 0xffff0148 */
	(uint32)&kernel_interrupt,	/* 0xffff0149 */
	(uint32)&kernel_interrupt,	/* 0xffff014a */
	(uint32)&kernel_interrupt,	/* 0xffff014b */
	(uint32)&kernel_interrupt,	/* 0xffff014c */
	(uint32)&kernel_interrupt,	/* 0xffff014d */
	(uint32)&kernel_interrupt,	/* 0xffff014e */
	(uint32)&kernel_interrupt,	/* 0xffff014f */
	(uint32)&kernel_interrupt,	/* 0xffff0150 */
	(uint32)&kernel_interrupt,	/* 0xffff0151 */
	(uint32)&kernel_interrupt,	/* 0xffff0152 */
	(uint32)&kernel_interrupt,	/* 0xffff0153 */
	(uint32)&kernel_interrupt,	/* 0xffff0154 */
	(uint32)&kernel_interrupt,	/* 0xffff0155 */
	(uint32)&kernel_interrupt,	/* 0xffff0156 */
	(uint32)&kernel_interrupt,	/* 0xffff0157 */
	(uint32)&kernel_interrupt,	/* 0xffff0158 */
	(uint32)&kernel_interrupt,	/* 0xffff0159 */
	(uint32)&kernel_interrupt,	/* 0xffff015a */
	(uint32)&kernel_interrupt,	/* 0xffff015b */
	(uint32)&kernel_interrupt,	/* 0xffff015c */
	(uint32)&kernel_interrupt,	/* 0xffff015d */
	(uint32)&kernel_interrupt	/* 0xffff015e */
};
extern void kernel_interrupt(void);
const uint32 __attribute__((aligned(512))) kernel_core1_intbp_tbl[TNUM_INT] = {
	(uint32)&kernel_interrupt,	/* 0x20000 */
	(uint32)&kernel_interrupt,	/* 0x20001 */
	(uint32)&kernel_interrupt,	/* 0x20002 */
	(uint32)&kernel_interrupt,	/* 0x20003 */
	(uint32)&kernel_interrupt,	/* 0x20004 */
	(uint32)&kernel_interrupt,	/* 0x20005 */
	(uint32)&kernel_interrupt,	/* 0x20006 */
	(uint32)&kernel_interrupt,	/* 0x20007 */
	(uint32)&kernel_interrupt,	/* 0x20008 */
	(uint32)&kernel_interrupt,	/* 0x20009 */
	(uint32)&kernel_interrupt,	/* 0x2000a */
	(uint32)&kernel_interrupt,	/* 0x2000b */
	(uint32)&kernel_interrupt,	/* 0x2000c */
	(uint32)&kernel_interrupt,	/* 0x2000d */
	(uint32)&kernel_interrupt,	/* 0x2000e */
	(uint32)&kernel_interrupt,	/* 0x2000f */
	(uint32)&kernel_interrupt,	/* 0x20010 */
	(uint32)&kernel_interrupt,	/* 0x20011 */
	(uint32)&kernel_interrupt,	/* 0x20012 */
	(uint32)&kernel_interrupt,	/* 0x20013 */
	(uint32)&kernel_interrupt,	/* 0x20014 */
	(uint32)&kernel_interrupt,	/* 0x20015 */
	(uint32)&kernel_interrupt,	/* 0x20016 */
	(uint32)&kernel_interrupt,	/* 0x20017 */
	(uint32)&kernel_interrupt,	/* 0x20018 */
	(uint32)&kernel_interrupt,	/* 0x20019 */
	(uint32)&kernel_interrupt,	/* 0x2001a */
	(uint32)&kernel_interrupt,	/* 0x2001b */
	(uint32)&kernel_interrupt,	/* 0x2001c */
	(uint32)&kernel_interrupt,	/* 0x2001d */
	(uint32)&kernel_interrupt,	/* 0x2001e */
	(uint32)&kernel_interrupt,	/* 0x2001f */
	(uint32)&kernel_interrupt,	/* 0xffff0020 */
	(uint32)&kernel_interrupt,	/* 0xffff0021 */
	(uint32)&kernel_interrupt,	/* 0xffff0022 */
	(uint32)&kernel_interrupt,	/* 0xffff0023 */
	(uint32)&kernel_interrupt,	/* 0xffff0024 */
	(uint32)&kernel_interrupt,	/* 0xffff0025 */
	(uint32)&kernel_interrupt,	/* 0xffff0026 */
	(uint32)&kernel_interrupt,	/* 0xffff0027 */
	(uint32)&kernel_interrupt,	/* 0xffff0028 */
	(uint32)&kernel_interrupt,	/* 0xffff0029 */
	(uint32)&kernel_interrupt,	/* 0xffff002a */
	(uint32)&kernel_interrupt,	/* 0xffff002b */
	(uint32)&kernel_interrupt,	/* 0xffff002c */
	(uint32)&kernel_interrupt,	/* 0xffff002d */
	(uint32)&kernel_interrupt,	/* 0xffff002e */
	(uint32)&kernel_interrupt,	/* 0xffff002f */
	(uint32)&kernel_interrupt,	/* 0xffff0030 */
	(uint32)&kernel_interrupt,	/* 0xffff0031 */
	(uint32)&kernel_interrupt,	/* 0xffff0032 */
	(uint32)&kernel_interrupt,	/* 0xffff0033 */
	(uint32)&kernel_interrupt,	/* 0xffff0034 */
	(uint32)&kernel_interrupt,	/* 0xffff0035 */
	(uint32)&kernel_interrupt,	/* 0xffff0036 */
	(uint32)&kernel_interrupt,	/* 0xffff0037 */
	(uint32)&kernel_interrupt,	/* 0xffff0038 */
	(uint32)&kernel_interrupt,	/* 0xffff0039 */
	(uint32)&kernel_interrupt,	/* 0xffff003a */
	(uint32)&kernel_interrupt,	/* 0xffff003b */
	(uint32)&kernel_interrupt,	/* 0xffff003c */
	(uint32)&kernel_interrupt,	/* 0xffff003d */
	(uint32)&kernel_interrupt,	/* 0xffff003e */
	(uint32)&kernel_interrupt,	/* 0xffff003f */
	(uint32)&kernel_interrupt,	/* 0xffff0040 */
	(uint32)&kernel_interrupt,	/* 0xffff0041 */
	(uint32)&kernel_interrupt,	/* 0xffff0042 */
	(uint32)&kernel_interrupt,	/* 0xffff0043 */
	(uint32)&kernel_interrupt,	/* 0xffff0044 */
	(uint32)&kernel_interrupt,	/* 0xffff0045 */
	(uint32)&kernel_interrupt,	/* 0xffff0046 */
	(uint32)&kernel_interrupt,	/* 0xffff0047 */
	(uint32)&kernel_interrupt,	/* 0xffff0048 */
	(uint32)&kernel_interrupt,	/* 0xffff0049 */
	(uint32)&kernel_interrupt,	/* 0xffff004a */
	(uint32)&kernel_interrupt,	/* 0xffff004b */
	(uint32)&kernel_interrupt,	/* 0xffff004c */
	(uint32)&kernel_interrupt,	/* 0xffff004d */
	(uint32)&kernel_interrupt,	/* 0xffff004e */
	(uint32)&kernel_interrupt,	/* 0xffff004f */
	(uint32)&kernel_interrupt,	/* 0xffff0050 */
	(uint32)&kernel_interrupt,	/* 0xffff0051 */
	(uint32)&kernel_interrupt,	/* 0xffff0052 */
	(uint32)&kernel_interrupt,	/* 0xffff0053 */
	(uint32)&kernel_interrupt,	/* 0xffff0054 */
	(uint32)&kernel_interrupt,	/* 0xffff0055 */
	(uint32)&kernel_interrupt,	/* 0xffff0056 */
	(uint32)&kernel_interrupt,	/* 0xffff0057 */
	(uint32)&kernel_interrupt,	/* 0xffff0058 */
	(uint32)&kernel_interrupt,	/* 0xffff0059 */
	(uint32)&kernel_interrupt,	/* 0xffff005a */
	(uint32)&kernel_interrupt,	/* 0xffff005b */
	(uint32)&kernel_interrupt,	/* 0xffff005c */
	(uint32)&kernel_interrupt,	/* 0xffff005d */
	(uint32)&kernel_interrupt,	/* 0xffff005e */
	(uint32)&kernel_interrupt,	/* 0xffff005f */
	(uint32)&kernel_interrupt,	/* 0xffff0060 */
	(uint32)&kernel_interrupt,	/* 0xffff0061 */
	(uint32)&kernel_interrupt,	/* 0xffff0062 */
	(uint32)&kernel_interrupt,	/* 0xffff0063 */
	(uint32)&kernel_interrupt,	/* 0xffff0064 */
	(uint32)&kernel_interrupt,	/* 0xffff0065 */
	(uint32)&kernel_interrupt,	/* 0xffff0066 */
	(uint32)&kernel_interrupt,	/* 0xffff0067 */
	(uint32)&kernel_interrupt,	/* 0xffff0068 */
	(uint32)&kernel_interrupt,	/* 0xffff0069 */
	(uint32)&kernel_interrupt,	/* 0xffff006a */
	(uint32)&kernel_interrupt,	/* 0xffff006b */
	(uint32)&kernel_interrupt,	/* 0xffff006c */
	(uint32)&kernel_interrupt,	/* 0xffff006d */
	(uint32)&kernel_interrupt,	/* 0xffff006e */
	(uint32)&kernel_interrupt,	/* 0xffff006f */
	(uint32)&kernel_interrupt,	/* 0xffff0070 */
	(uint32)&kernel_interrupt,	/* 0xffff0071 */
	(uint32)&kernel_interrupt,	/* 0xffff0072 */
	(uint32)&kernel_interrupt,	/* 0xffff0073 */
	(uint32)&kernel_interrupt,	/* 0xffff0074 */
	(uint32)&kernel_interrupt,	/* 0xffff0075 */
	(uint32)&kernel_interrupt,	/* 0xffff0076 */
	(uint32)&kernel_interrupt,	/* 0xffff0077 */
	(uint32)&kernel_interrupt,	/* 0xffff0078 */
	(uint32)&kernel_interrupt,	/* 0xffff0079 */
	(uint32)&kernel_interrupt,	/* 0xffff007a */
	(uint32)&kernel_interrupt,	/* 0xffff007b */
	(uint32)&kernel_interrupt,	/* 0xffff007c */
	(uint32)&kernel_interrupt,	/* 0xffff007d */
	(uint32)&kernel_interrupt,	/* 0xffff007e */
	(uint32)&kernel_interrupt,	/* 0xffff007f */
	(uint32)&kernel_interrupt,	/* 0xffff0080 */
	(uint32)&kernel_interrupt,	/* 0xffff0081 */
	(uint32)&kernel_interrupt,	/* 0xffff0082 */
	(uint32)&kernel_interrupt,	/* 0xffff0083 */
	(uint32)&kernel_interrupt,	/* 0xffff0084 */
	(uint32)&kernel_interrupt,	/* 0xffff0085 */
	(uint32)&kernel_interrupt,	/* 0xffff0086 */
	(uint32)&kernel_interrupt,	/* 0xffff0087 */
	(uint32)&kernel_interrupt,	/* 0xffff0088 */
	(uint32)&kernel_interrupt,	/* 0xffff0089 */
	(uint32)&kernel_interrupt,	/* 0xffff008a */
	(uint32)&kernel_interrupt,	/* 0xffff008b */
	(uint32)&kernel_interrupt,	/* 0xffff008c */
	(uint32)&kernel_interrupt,	/* 0xffff008d */
	(uint32)&kernel_interrupt,	/* 0xffff008e */
	(uint32)&kernel_interrupt,	/* 0xffff008f */
	(uint32)&kernel_interrupt,	/* 0xffff0090 */
	(uint32)&kernel_interrupt,	/* 0xffff0091 */
	(uint32)&kernel_interrupt,	/* 0xffff0092 */
	(uint32)&kernel_interrupt,	/* 0xffff0093 */
	(uint32)&kernel_interrupt,	/* 0xffff0094 */
	(uint32)&kernel_interrupt,	/* 0xffff0095 */
	(uint32)&kernel_interrupt,	/* 0xffff0096 */
	(uint32)&kernel_interrupt,	/* 0xffff0097 */
	(uint32)&kernel_interrupt,	/* 0xffff0098 */
	(uint32)&kernel_interrupt,	/* 0xffff0099 */
	(uint32)&kernel_interrupt,	/* 0xffff009a */
	(uint32)&kernel_interrupt,	/* 0xffff009b */
	(uint32)&kernel_interrupt,	/* 0xffff009c */
	(uint32)&kernel_interrupt,	/* 0xffff009d */
	(uint32)&kernel_interrupt,	/* 0xffff009e */
	(uint32)&kernel_interrupt,	/* 0xffff009f */
	(uint32)&kernel_interrupt,	/* 0xffff00a0 */
	(uint32)&kernel_interrupt,	/* 0xffff00a1 */
	(uint32)&kernel_interrupt,	/* 0xffff00a2 */
	(uint32)&kernel_interrupt,	/* 0xffff00a3 */
	(uint32)&kernel_interrupt,	/* 0xffff00a4 */
	(uint32)&kernel_interrupt,	/* 0xffff00a5 */
	(uint32)&kernel_interrupt,	/* 0xffff00a6 */
	(uint32)&kernel_interrupt,	/* 0xffff00a7 */
	(uint32)&kernel_interrupt,	/* 0xffff00a8 */
	(uint32)&kernel_interrupt,	/* 0xffff00a9 */
	(uint32)&kernel_interrupt,	/* 0xffff00aa */
	(uint32)&kernel_interrupt,	/* 0xffff00ab */
	(uint32)&kernel_interrupt,	/* 0xffff00ac */
	(uint32)&kernel_interrupt,	/* 0xffff00ad */
	(uint32)&kernel_interrupt,	/* 0xffff00ae */
	(uint32)&kernel_interrupt,	/* 0xffff00af */
	(uint32)&kernel_interrupt,	/* 0xffff00b0 */
	(uint32)&kernel_interrupt,	/* 0xffff00b1 */
	(uint32)&kernel_interrupt,	/* 0xffff00b2 */
	(uint32)&kernel_interrupt,	/* 0xffff00b3 */
	(uint32)&kernel_interrupt,	/* 0xffff00b4 */
	(uint32)&kernel_interrupt,	/* 0xffff00b5 */
	(uint32)&kernel_interrupt,	/* 0xffff00b6 */
	(uint32)&kernel_interrupt,	/* 0xffff00b7 */
	(uint32)&kernel_interrupt,	/* 0xffff00b8 */
	(uint32)&kernel_interrupt,	/* 0xffff00b9 */
	(uint32)&kernel_interrupt,	/* 0xffff00ba */
	(uint32)&kernel_interrupt,	/* 0xffff00bb */
	(uint32)&kernel_interrupt,	/* 0xffff00bc */
	(uint32)&kernel_interrupt,	/* 0xffff00bd */
	(uint32)&kernel_interrupt,	/* 0xffff00be */
	(uint32)&kernel_interrupt,	/* 0xffff00bf */
	(uint32)&kernel_interrupt,	/* 0xffff00c0 */
	(uint32)&kernel_interrupt,	/* 0xffff00c1 */
	(uint32)&kernel_interrupt,	/* 0xffff00c2 */
	(uint32)&kernel_interrupt,	/* 0xffff00c3 */
	(uint32)&kernel_interrupt,	/* 0xffff00c4 */
	(uint32)&kernel_interrupt,	/* 0xffff00c5 */
	(uint32)&kernel_interrupt,	/* 0xffff00c6 */
	(uint32)&kernel_interrupt,	/* 0xffff00c7 */
	(uint32)&kernel_interrupt,	/* 0xffff00c8 */
	(uint32)&kernel_interrupt,	/* 0xffff00c9 */
	(uint32)&kernel_interrupt,	/* 0xffff00ca */
	(uint32)&kernel_interrupt,	/* 0xffff00cb */
	(uint32)&kernel_interrupt,	/* 0xffff00cc */
	(uint32)&kernel_interrupt,	/* 0xffff00cd */
	(uint32)&kernel_interrupt,	/* 0xffff00ce */
	(uint32)&kernel_interrupt,	/* 0xffff00cf */
	(uint32)&kernel_interrupt,	/* 0xffff00d0 */
	(uint32)&kernel_interrupt,	/* 0xffff00d1 */
	(uint32)&kernel_interrupt,	/* 0xffff00d2 */
	(uint32)&kernel_interrupt,	/* 0xffff00d3 */
	(uint32)&kernel_interrupt,	/* 0xffff00d4 */
	(uint32)&kernel_interrupt,	/* 0xffff00d5 */
	(uint32)&kernel_interrupt,	/* 0xffff00d6 */
	(uint32)&kernel_interrupt,	/* 0xffff00d7 */
	(uint32)&kernel_interrupt,	/* 0xffff00d8 */
	(uint32)&kernel_interrupt,	/* 0xffff00d9 */
	(uint32)&kernel_interrupt,	/* 0xffff00da */
	(uint32)&kernel_interrupt,	/* 0xffff00db */
	(uint32)&kernel_interrupt,	/* 0xffff00dc */
	(uint32)&kernel_interrupt,	/* 0xffff00dd */
	(uint32)&kernel_interrupt,	/* 0xffff00de */
	(uint32)&kernel_interrupt,	/* 0xffff00df */
	(uint32)&kernel_interrupt,	/* 0xffff00e0 */
	(uint32)&kernel_interrupt,	/* 0xffff00e1 */
	(uint32)&kernel_interrupt,	/* 0xffff00e2 */
	(uint32)&kernel_interrupt,	/* 0xffff00e3 */
	(uint32)&kernel_interrupt,	/* 0xffff00e4 */
	(uint32)&kernel_interrupt,	/* 0xffff00e5 */
	(uint32)&kernel_interrupt,	/* 0xffff00e6 */
	(uint32)&kernel_interrupt,	/* 0xffff00e7 */
	(uint32)&kernel_interrupt,	/* 0xffff00e8 */
	(uint32)&kernel_interrupt,	/* 0xffff00e9 */
	(uint32)&kernel_interrupt,	/* 0xffff00ea */
	(uint32)&kernel_interrupt,	/* 0xffff00eb */
	(uint32)&kernel_interrupt,	/* 0xffff00ec */
	(uint32)&kernel_interrupt,	/* 0xffff00ed */
	(uint32)&kernel_interrupt,	/* 0xffff00ee */
	(uint32)&kernel_interrupt,	/* 0xffff00ef */
	(uint32)&kernel_interrupt,	/* 0xffff00f0 */
	(uint32)&kernel_interrupt,	/* 0xffff00f1 */
	(uint32)&kernel_interrupt,	/* 0xffff00f2 */
	(uint32)&kernel_interrupt,	/* 0xffff00f3 */
	(uint32)&kernel_interrupt,	/* 0xffff00f4 */
	(uint32)&kernel_interrupt,	/* 0xffff00f5 */
	(uint32)&kernel_interrupt,	/* 0xffff00f6 */
	(uint32)&kernel_interrupt,	/* 0xffff00f7 */
	(uint32)&kernel_interrupt,	/* 0xffff00f8 */
	(uint32)&kernel_interrupt,	/* 0xffff00f9 */
	(uint32)&kernel_interrupt,	/* 0xffff00fa */
	(uint32)&kernel_interrupt,	/* 0xffff00fb */
	(uint32)&kernel_interrupt,	/* 0xffff00fc */
	(uint32)&kernel_interrupt,	/* 0xffff00fd */
	(uint32)&kernel_interrupt,	/* 0xffff00fe */
	(uint32)&kernel_interrupt,	/* 0xffff00ff */
	(uint32)&kernel_interrupt,	/* 0xffff0100 */
	(uint32)&kernel_interrupt,	/* 0xffff0101 */
	(uint32)&kernel_interrupt,	/* 0xffff0102 */
	(uint32)&kernel_interrupt,	/* 0xffff0103 */
	(uint32)&kernel_interrupt,	/* 0xffff0104 */
	(uint32)&kernel_interrupt,	/* 0xffff0105 */
	(uint32)&kernel_interrupt,	/* 0xffff0106 */
	(uint32)&kernel_interrupt,	/* 0xffff0107 */
	(uint32)&kernel_interrupt,	/* 0xffff0108 */
	(uint32)&kernel_interrupt,	/* 0xffff0109 */
	(uint32)&kernel_interrupt,	/* 0xffff010a */
	(uint32)&kernel_interrupt,	/* 0xffff010b */
	(uint32)&kernel_interrupt,	/* 0xffff010c */
	(uint32)&kernel_interrupt,	/* 0xffff010d */
	(uint32)&kernel_interrupt,	/* 0xffff010e */
	(uint32)&kernel_interrupt,	/* 0xffff010f */
	(uint32)&kernel_interrupt,	/* 0xffff0110 */
	(uint32)&kernel_interrupt,	/* 0xffff0111 */
	(uint32)&kernel_interrupt,	/* 0xffff0112 */
	(uint32)&kernel_interrupt,	/* 0xffff0113 */
	(uint32)&kernel_interrupt,	/* 0xffff0114 */
	(uint32)&kernel_interrupt,	/* 0xffff0115 */
	(uint32)&kernel_interrupt,	/* 0xffff0116 */
	(uint32)&kernel_interrupt,	/* 0xffff0117 */
	(uint32)&kernel_interrupt,	/* 0xffff0118 */
	(uint32)&kernel_interrupt,	/* 0xffff0119 */
	(uint32)&kernel_interrupt,	/* 0xffff011a */
	(uint32)&kernel_interrupt,	/* 0xffff011b */
	(uint32)&kernel_interrupt,	/* 0xffff011c */
	(uint32)&kernel_interrupt,	/* 0xffff011d */
	(uint32)&kernel_interrupt,	/* 0xffff011e */
	(uint32)&kernel_interrupt,	/* 0xffff011f */
	(uint32)&kernel_interrupt,	/* 0xffff0120 */
	(uint32)&kernel_interrupt,	/* 0xffff0121 */
	(uint32)&kernel_interrupt,	/* 0xffff0122 */
	(uint32)&kernel_interrupt,	/* 0xffff0123 */
	(uint32)&kernel_interrupt,	/* 0xffff0124 */
	(uint32)&kernel_interrupt,	/* 0xffff0125 */
	(uint32)&kernel_interrupt,	/* 0xffff0126 */
	(uint32)&kernel_interrupt,	/* 0xffff0127 */
	(uint32)&kernel_interrupt,	/* 0xffff0128 */
	(uint32)&kernel_interrupt,	/* 0xffff0129 */
	(uint32)&kernel_interrupt,	/* 0xffff012a */
	(uint32)&kernel_interrupt,	/* 0xffff012b */
	(uint32)&kernel_interrupt,	/* 0xffff012c */
	(uint32)&kernel_interrupt,	/* 0xffff012d */
	(uint32)&kernel_interrupt,	/* 0xffff012e */
	(uint32)&kernel_interrupt,	/* 0xffff012f */
	(uint32)&kernel_interrupt,	/* 0xffff0130 */
	(uint32)&kernel_interrupt,	/* 0xffff0131 */
	(uint32)&kernel_interrupt,	/* 0xffff0132 */
	(uint32)&kernel_interrupt,	/* 0xffff0133 */
	(uint32)&kernel_interrupt,	/* 0xffff0134 */
	(uint32)&kernel_interrupt,	/* 0xffff0135 */
	(uint32)&kernel_interrupt,	/* 0xffff0136 */
	(uint32)&kernel_interrupt,	/* 0xffff0137 */
	(uint32)&kernel_interrupt,	/* 0xffff0138 */
	(uint32)&kernel_interrupt,	/* 0xffff0139 */
	(uint32)&kernel_interrupt,	/* 0xffff013a */
	(uint32)&kernel_interrupt,	/* 0xffff013b */
	(uint32)&kernel_interrupt,	/* 0xffff013c */
	(uint32)&kernel_interrupt,	/* 0xffff013d */
	(uint32)&kernel_interrupt,	/* 0xffff013e */
	(uint32)&kernel_interrupt,	/* 0xffff013f */
	(uint32)&kernel_interrupt,	/* 0xffff0140 */
	(uint32)&kernel_interrupt,	/* 0xffff0141 */
	(uint32)&kernel_interrupt,	/* 0xffff0142 */
	(uint32)&kernel_interrupt,	/* 0xffff0143 */
	(uint32)&kernel_interrupt,	/* 0xffff0144 */
	(uint32)&kernel_interrupt,	/* 0xffff0145 */
	(uint32)&kernel_interrupt,	/* 0xffff0146 */
	(uint32)&kernel_interrupt,	/* 0xffff0147 */
	(uint32)&kernel_interrupt,	/* 0xffff0148 */
	(uint32)&kernel_interrupt,	/* 0xffff0149 */
	(uint32)&kernel_interrupt,	/* 0xffff014a */
	(uint32)&kernel_interrupt,	/* 0xffff014b */
	(uint32)&kernel_interrupt,	/* 0xffff014c */
	(uint32)&kernel_interrupt,	/* 0xffff014d */
	(uint32)&kernel_interrupt,	/* 0xffff014e */
	(uint32)&kernel_interrupt,	/* 0xffff014f */
	(uint32)&kernel_interrupt,	/* 0xffff0150 */
	(uint32)&kernel_interrupt,	/* 0xffff0151 */
	(uint32)&kernel_interrupt,	/* 0xffff0152 */
	(uint32)&kernel_interrupt,	/* 0xffff0153 */
	(uint32)&kernel_interrupt,	/* 0xffff0154 */
	(uint32)&kernel_interrupt,	/* 0xffff0155 */
	(uint32)&kernel_interrupt,	/* 0xffff0156 */
	(uint32)&kernel_interrupt,	/* 0xffff0157 */
	(uint32)&kernel_interrupt,	/* 0xffff0158 */
	(uint32)&kernel_interrupt,	/* 0xffff0159 */
	(uint32)&kernel_interrupt,	/* 0xffff015a */
	(uint32)&kernel_interrupt,	/* 0xffff015b */
	(uint32)&kernel_interrupt,	/* 0xffff015c */
	(uint32)&kernel_interrupt,	/* 0xffff015d */
	(uint32)&kernel_interrupt	/* 0xffff015e */
};

const uint32 kernel_intbp_table[TotalNumberOfCores] = {
	(const uint32) kernel_core0_intbp_tbl,
	(const uint32) kernel_core1_intbp_tbl
};
