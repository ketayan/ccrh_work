#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\arch\v850_gcc\uart_rlin.c"

	$reg_mode 32

	.extern _rlin3x_base_table
	.public _uart_putc
	.public _InitHwSerial
	.public _TermHwSerial
	.public _ISRMainRxHwSerialInt0
	.extern _int_uart30_rcv
	.public _ISRMainRxHwSerialInt1
	.extern _int_uart31_rcv

	.section .text, text
	.align 4
_uart_putc:
	.stack _uart_putc = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/uart_rlin.c", 79
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x0000000E, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	mov #_rlin3x_base_table, r7
	andi 0x0000000C, r5, r5
	add r7, r5
	ld.w 0xFFFFFFFC[r5], r5
.BB.LABEL.1_1:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 87
	tst1 0x00000004, 0x00000012[r5]
	bnz9 .BB.LABEL.1_1
.BB.LABEL.1_2:	; bb14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 92
	st.b r6, 0x00000024[r5]
	jmp [r31]
	.align 4
_InitHwSerial:
	.stack _InitHwSerial = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x0000000E, r5
	mov 0xFFFFFFF0, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x0000000C, r5, r5
	mov 0x00000008, r8
	mov #_rlin3x_base_table, r6
	add r6, r5
	mov 0x00000001, r6
	ld.w 0xFFFFFFFC[r5], r5
	mov 0x0000000D, r9
	mov 0x00000002, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 92
	st.b r6, 0x00000008[r5]
	st.b r7, 0x00000001[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r8, 0x00000002[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 92
	st.b r9, 0x0000000D[r5]
	st.b r0, 0x00000009[r5]
	st.b r6, 0x0000000E[r5]
	st.b r10, 0x0000000C[r5]
.BB.LABEL.2_1:	; bb26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 85
	ld.bu 0x00000011[r5], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 87
	cmp 0x00000000, r6
	bz9 .BB.LABEL.2_1
.BB.LABEL.2_2:	; bb34
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/uart_rlin.c", 0
	mov 0x00000003, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 92
	st.b r6, 0x00000020[r5]
	jmp [r31]
	.align 4
_TermHwSerial:
	.stack _TermHwSerial = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x0000000E, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	mov #_rlin3x_base_table, r6
	andi 0x0000000C, r5, r5
	add r6, r5
	ld.w 0xFFFFFFFC[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 92
	st.b r0, 0x0000000C[r5]
	jmp [r31]
	.align 4
_ISRMainRxHwSerialInt0:
	.stack _ISRMainRxHwSerialInt0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/uart_rlin.c", 152
	jr _int_uart30_rcv
	.align 4
_ISRMainRxHwSerialInt1:
	.stack _ISRMainRxHwSerialInt1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/uart_rlin.c", 165
	jr _int_uart31_rcv
