/* This file is generated from prc_rename.def by genrename. */

#ifndef TOPPERS_PRC_RENAME_H
#define TOPPERS_PRC_RENAME_H

/*
 *  prc_config.c
 */
#define prc_hardware_initialize		kernel_prc_hardware_initialize
#define prc_initialize				kernel_prc_initialize
#define prc_terminate				kernel_prc_terminate
#define x_config_int				kernel_x_config_int
#define default_int_handler			kernel_default_int_handler
#define no_support_service			kernel_no_support_service

/*
 *  kernel_mem.c	Os_Lcfg.c
 */
#define tmin_status_il				kernel_tmin_status_il
#define isr_tbl						kernel_isr_tbl
#define isr_p_isrcb_tbl				kernel_isr_p_isrcb_tbl

/*
 *  prc_support.S
 */
#define exception_entry				kernel_exception_entry
#define interrupt					kernel_interrupt

#define dispatch					kernel_dispatch
#define start_dispatch				kernel_start_dispatch
#define exit_and_dispatch_nohook	kernel_exit_and_dispatch_nohook
#define exit_and_dispatch			kernel_exit_and_dispatch
#define start_stask_r				kernel_start_stask_r
#define start_utask_r				kernel_start_utask_r
#define start_r						kernel_start_r
#define stack_change_and_call_func_1	kernel_stack_change_and_call_func_1
#define stack_change_and_call_func_2	kernel_stack_change_and_call_func_2
#define trustedfunc_stack_check		kernel_trustedfunc_stack_check

/*
 *  prc_mpu.c
 */
#define prc_set_osap_mpu			kernel_prc_set_osap_mpu
#define prc_init_mpu				kernel_prc_init_mpu
#define probe_trusted_osap_mem		kernel_probe_trusted_osap_mem


#endif /* TOPPERS_PRC_RENAME_H */
