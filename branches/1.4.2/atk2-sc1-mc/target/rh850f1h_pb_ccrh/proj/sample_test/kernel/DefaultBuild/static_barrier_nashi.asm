#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\static_barrier.c -oDefaultBuild\static_barrier.obj -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_r3wmmljj.ezw
#@	compiled at Wed Aug 08 17:06:30 2018

	.file "..\..\..\static_barrier.c"

	$reg_mode 32

	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _activated_cores
	.extern _shutdown_reqflg
	.extern _v850_memory_changed
	.public _barrier_sync
	.extern _busy_wait
	.extern _internal_shutdownallcores
	.public _main

	.section .text, text
	.align 4
_current_peid.1:
	.stack _current_peid.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	jmp [r31]
	.align 4
_x_core_id.1:
	.stack _x_core_id.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 112
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 115
	jarl _current_peid.1, r31
	add 0xFFFFFFFF, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	zxb r10
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_get_my_p_ccb.1:
	.stack _get_my_p_ccb.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 244
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 246
	jarl _x_core_id.1, r31
	shl 0x00000002, r10
	mov #_p_ccb_table, r5
	add r10, r5
	ld.w 0x00000000[r5], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_x_sense_mcore.1:
	.stack _x_sense_mcore.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 489
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 491
	jarl _x_core_id.1, r31
	cmp 0x00000000, r10
	setf 0x00000002, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 492
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_barrier_sync:
	.stack _barrier_sync = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 7
	prepare 0x00000071, 0x00000000
	mov r6, r20
	mov r7, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 15
	jarl _x_core_id.1, r31
	mov #_core_init.1.barrier_sync, r5
	add r10, r5
	st.b r20, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 17
	jarl _x_sense_mcore.1, r31
	cmp 0x00000000, r10
	bz9 .BB.LABEL.5_16
.BB.LABEL.5_1:	; entry.bb28_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 0
	mov 0x00000000, r22
	mov r22, r5
	br9 .BB.LABEL.5_3
.BB.LABEL.5_2:	; bb12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 21
	andi 0x000000FF, r5, r6
	mov #_core_init.1.barrier_sync, r7
	add r6, r7
	ld.bu 0x00000000[r7], r6
	cmp r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 22
	adf 0x00000002, r0, r22, r22
	add 0x00000001, r5
.BB.LABEL.5_3:	; bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 20
	andi 0x000000FF, r5, r6
	cmp 0x00000002, r6
	blt9 .BB.LABEL.5_2
.BB.LABEL.5_4:	; bb34
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 25
	jarl _busy_wait, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 26
	cmp 0x00000000, r21
	bz9 .BB.LABEL.5_9
.BB.LABEL.5_5:	; bb40
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 0
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.5_9
.BB.LABEL.5_6:	; if_then_bb52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 177
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r10], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.5_8
.BB.LABEL.5_7:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.5_8:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r10], r5
	add 0x00000001, r5
	st.b r5, 0x000000D5[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 33
	jarl _internal_shutdownallcores, r31
.BB.LABEL.5_9:	; if_break_bb53
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 18
	movhi HIGHW1(#_activated_cores), r0, r5
	ld.w LOWW(#_activated_cores)[r5], r5
	zxb r22
	cmp r5, r22
	bl9 .BB.LABEL.5_1
.BB.LABEL.5_10:	; bb60
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 36
	movhi HIGHW1(#_sys_start.2.barrier_sync), r0, r21
	st.b r20, LOWW(#_sys_start.2.barrier_sync)[r21]
	dispose 0x00000000, 0x00000071, [r31]
.BB.LABEL.5_11:	; bb62
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 40
	jarl _busy_wait, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 41
	cmp 0x00000000, r21
	bz9 .BB.LABEL.5_16
.BB.LABEL.5_12:	; bb68
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 0
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.5_16
.BB.LABEL.5_13:	; if_then_bb81
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 177
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r10], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.5_15
.BB.LABEL.5_14:	; if_then_bb.i8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.5_15:	; x_nested_lock_os_int.1.exit11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r10], r5
	add 0x00000001, r5
	st.b r5, 0x000000D5[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 48
	jarl _internal_shutdownallcores, r31
.BB.LABEL.5_16:	; bb83
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 39
	movhi HIGHW1(#_sys_start.2.barrier_sync), r0, r5
	ld.bu LOWW(#_sys_start.2.barrier_sync)[r5], r5
	cmp r5, r20
	bnz9 .BB.LABEL.5_11
.BB.LABEL.5_17:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 52
	dispose 0x00000000, 0x00000071, [r31]
	.align 4
_main:
	.stack _main = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 54
	prepare 0x00000041, 0x00000000
	mov 0x00000000, r20
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 55
	mov r20, r7
	jarl _barrier_sync, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 56
	mov r20, r10
	dispose 0x00000000, 0x00000041, [r31]
	.section .bss, bss
_sys_start.2.barrier_sync:
	.ds (1)
	.section .data, data
_core_init.1.barrier_sync:
	.ds (2)
