#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\kernel\mc_manage.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _pmr_setting_tbl
	.extern _core_state_table
	.extern _activated_cores
	.extern _tnum_ici
	.extern _iciinib_table
	.extern _tnum_isr2
	.extern _p_isrcb_table
	.extern _v850_memory_changed
	.extern _internal_shutdownallcores
	.public _RaiseInterCoreInterrupt
	.extern _acquire_tsk_lock
	.extern _release_tsk_lock
	.public _GetCoreID
	.public _StartCore
	.public _StartNonAutosarCore
	.public _GetNumberOfActivatedCores
	.public _ShutdownAllCores

	.section .text, text
	.align 4
_x_core_id.1:
	.stack _x_core_id.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r5
	add 0xFFFFFFFF, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r10
	jmp [r31]
	.align 4
_RaiseInterCoreInterrupt:
	.stack _RaiseInterCoreInterrupt = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 126
	prepare 0x00000079, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r6, r21
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x0000704F, r5, r5
	addi 0xFFFF8FB1, r5, r0
	cmov 0x0000000A, 0x00000002, r20, r20
	bnz9 .BB.LABEL.2_4
.BB.LABEL.2_1:	; bb19
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 135
	movhi HIGHW1(#_tnum_isr2), r0, r5
	movhi HIGHW1(#_tnum_ici), r0, r6
	ld.w LOWW(#_tnum_isr2)[r5], r5
	ld.w LOWW(#_tnum_ici)[r6], r6
	subr r5, r6
	cmp r21, r5
	bnh9 .BB.LABEL.2_3
.BB.LABEL.2_2:	; bb19
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	cmp r21, r6
	bnh9 .BB.LABEL.2_5
.BB.LABEL.2_3:	; bb19.exit_no_errorhook_crit_edge
	mov 0x00000003, r20
.BB.LABEL.2_4:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 166
	andi 0x000000FF, r20, r10
	dispose 0x00000000, 0x00000079, [r31]
.BB.LABEL.2_5:	; bb48
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 136
	mov r21, r5
	shl 0x00000002, r5
	mov #_p_isrcb_table, r6
	add r5, r6
	ld.w 0x00000000[r6], r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 137
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_7
.BB.LABEL.2_6:	; bb48.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	movea 0x0000001D, r0, r20
	br9 .BB.LABEL.2_4
.BB.LABEL.2_7:	; bb68
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 138
	ld.w 0x00000004[r5], r5
	ld.bu 0x00000000[r5], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 139
	mov r22, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.2_9
.BB.LABEL.2_8:	; if_then_bb.i18
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.2_9:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 142
	mov r23, r6
	jarl _acquire_tsk_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 145
	movhi HIGHW1(#_tnum_ici), r0, r7
	ld.w LOWW(#_tnum_ici)[r7], r7
	movhi HIGHW1(#_tnum_isr2), r0, r8
	ld.w LOWW(#_tnum_isr2)[r8], r8
	mov #_iciinib_table, r9
	add r21, r7
	ld.w 0x000000C0[r23], r5
	sub r8, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 146
	ld.w 0x000000C4[r23], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 145
	mov r7, r8
	mul 0x0000000C, r8, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 146
	mul 0x0000000C, r7, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 145
	add r9, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 146
	add r7, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 145
	ld.w 0x00000004[r8], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 146
	ld.w 0x00000004[r9], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 145
	or r8, r5
	st.w r5, 0x000000C0[r23]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 146
	tst r6, r7
	bz9 .BB.LABEL.2_13
.BB.LABEL.2_10:	; if_then_bb107
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ici.h", 69
	cmp 0x00000000, r22
	mov 0x00000001, r5
	bz9 .BB.LABEL.2_12
.BB.LABEL.2_11:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	mov 0x00000002, r5
.BB.LABEL.2_12:	; if_else_bb.i
	st23.w r5, 0xFFFEEC80[r0]
.BB.LABEL.2_13:	; if_break_bb109
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 149
	mov r23, r6
	jarl _release_tsk_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_15
.BB.LABEL.2_14:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.2_15:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.2_17
.BB.LABEL.2_16:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r20
	mov #_pmr_setting_tbl, r5
	add r20, r20
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r20, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.2_17:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 166
	dispose 0x00000000, 0x00000079, [r31]
	.align 4
_GetCoreID:
	.stack _GetCoreID = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 181
	jr _x_core_id.1
	.align 4
_StartCore:
	.stack _StartCore = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 195
	movhi HIGHW1(#_p_ccb_table), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 208
	ld.w LOWW(#_p_ccb_table)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r8, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r8
	andi 0x00000003, r8, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r8, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 211
	andi 0x000000FF, r8, r8
	cmp 0x00000002, r8
.BB.LABEL.4_1:	; if_else_bb57
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	cmp 0x00000000, r7
	bz9 .BB.LABEL.4_4
.BB.LABEL.4_2:	; if_else_bb63
	ld.bu 0x00000001[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.4_7
.BB.LABEL.4_3:	; if_then_bb71
	mov 0x00000001, r5
	st.b r5, 0x00000000[r7]
.BB.LABEL.4_4:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 310
	jmp [r31]
.BB.LABEL.4_5:	; if_then_bb97
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	mov 0x00000003, r5
	st.b r5, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 310
	jmp [r31]
.BB.LABEL.4_6:	; if_then_bb88
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	mov 0x00000007, r5
	st.b r5, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 310
	jmp [r31]
.BB.LABEL.4_7:	; if_else_bb73
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	cmp 0x00000002, r6
	bge9 .BB.LABEL.4_5
.BB.LABEL.4_8:	; if_else_bb81
	mov r6, r5
	shl 0x00000002, r5
	mov #_core_state_table, r8
	add r8, r5
	ld.w 0x00000000[r5], r8
	cmp 0x00000000, r8
	bnz9 .BB.LABEL.4_6
.BB.LABEL.4_9:	; if_else_bb90
	cmp 0x00000001, r6
	bh9 .BB.LABEL.4_5
.BB.LABEL.4_10:	; if_then_bb.i
	mov 0x87654321, r6
	st.w r6, 0x00000000[r5]
	movhi HIGHW1(#_activated_cores), r0, r5
	ld.w LOWW(#_activated_cores)[r5], r6
	add 0x00000001, r6
	st.w r6, LOWW(#_activated_cores)[r5]
	st.b r0, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 310
	jmp [r31]
	.align 4
_StartNonAutosarCore:
	.stack _StartNonAutosarCore = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 320
	movhi HIGHW1(#_p_ccb_table), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 333
	ld.w LOWW(#_p_ccb_table)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r8, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r8
	andi 0x00000003, r8, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r8, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 336
	andi 0x000000FF, r8, r8
	cmp 0x00000002, r8
.BB.LABEL.5_1:	; if_else_bb57
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	cmp 0x00000000, r7
	bz9 .BB.LABEL.5_7
.BB.LABEL.5_2:	; if_else_bb63
	mov r6, r8
	shl 0x00000002, r8
	mov #_core_state_table, r9
	add r9, r8
	ld.w 0x00000000[r8], r9
	cmp 0x00000000, r9
	bz9 .BB.LABEL.5_8
.BB.LABEL.5_3:	; if_then_bb71
	ld.bu 0x00000001[r5], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.5_6
.BB.LABEL.5_4:	; if_else_bb81
	mov 0x00000007, r5
	st.b r5, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 433
	jmp [r31]
.BB.LABEL.5_5:	; if_then_bb91
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	ld.bu 0x00000001[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.5_10
.BB.LABEL.5_6:	; if_then_bb79
	mov 0x00000001, r5
	st.b r5, 0x00000000[r7]
.BB.LABEL.5_7:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 433
	jmp [r31]
.BB.LABEL.5_8:	; if_else_bb84
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	cmp 0x00000001, r6
	bh9 .BB.LABEL.5_5
.BB.LABEL.5_9:	; if_then_bb.i
	mov 0x87654321, r5
	st.w r5, 0x00000000[r8]
	st.b r0, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 433
	jmp [r31]
.BB.LABEL.5_10:	; if_else_bb101
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 0
	mov 0x00000003, r5
	st.b r5, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 433
	jmp [r31]
	.align 4
_GetNumberOfActivatedCores:
	.stack _GetNumberOfActivatedCores = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 448
	movhi HIGHW1(#_activated_cores), r0, r5
	ld.w LOWW(#_activated_cores)[r5], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 449
	jmp [r31]
	.align 4
_ShutdownAllCores:
	.stack _ShutdownAllCores = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 459
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	movea 0x00000024, r0, r8
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r7
	ld.w 0x00000000[r7], r5
	movea 0x00000019, r0, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.h 0x00000004[r5], r5
	ori 0x00000053, r5, r5
	andi 0x00000FFF, r5, r5
	addi 0xFFFFFFAD, r5, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 469
	cmov 0x00000002, r6, r7, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 476
	cmp r8, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 477
	cmov 0x0000000B, r7, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc_manage.c", 480
	jr _internal_shutdownallcores
	.section .const, const
.STR.1:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x61,0x72,0x63,0x68
	.db 0x5C,0x76,0x38,0x35,0x30,0x5F,0x67,0x63,0x63,0x2F,0x70,0x72,0x63,0x5F,0x63,0x6F
	.db 0x6E,0x66,0x69,0x67,0x2E,0x68
	.ds (1)
