#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\arch\v850_gcc\tauj_hw_counter.c"

	$reg_mode 32

	.public _init_hwcounter_tauj
	.public _start_hwcounter_tauj
	.public _stop_hwcounter_tauj
	.public _set_hwcounter_tauj
	.public _get_hwcounter_tauj
	.public _cancel_hwcounter_tauj
	.public _trigger_hwcounter_tauj
	.public _int_clear_hwcounter_tauj
	.public _int_cancel_hwcounter_tauj
	.public _increment_hwcounter_tauj

	.section .text, text
	.align 4
_HwcounterClearInterrupt.1:
	.stack _HwcounterClearInterrupt.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 113
	movea 0x0000001F, r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 116
	cmp r5, r6
	bh9 .BB.LABEL.1_2
.BB.LABEL.1_1:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r6, r6
	mov 0xFFFEEA00, r5
	br9 .BB.LABEL.1_3
.BB.LABEL.1_2:	; bb8
	mov 0x7FFFFFE0, r5
	add r5, r6
	mov r6, r5
	add r5, r5
	movea 0xFFFFB040, r0, r6
.BB.LABEL.1_3:	; bb13
	add r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.hu 0x00000000[r5], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	andi 0x0000EFFF, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r6, 0x00000000[r5]
	jmp [r31]
	.align 4
_SetTimerStartTAUJ.1:
	.stack _SetTimerStartTAUJ.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 152
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 155
	shl r7, r5
	shl 0x0000000C, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFE50054[r6]
	jmp [r31]
	.align 4
_SetTimerStopTAUJ.1:
	.stack _SetTimerStopTAUJ.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 159
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 162
	shl r7, r5
	shl 0x0000000C, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFE50058[r6]
	jmp [r31]
	.align 4
_init_hwcounter_tauj:
	.stack _init_hwcounter_tauj = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 100
	ld.w 0x00000008[r3], r5
	mov 0x00000001, r11
	ld.w 0x00000000[r3], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 162
	mov r6, r13
	shl r7, r11, r12
	shl 0x0000000C, r13
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 110
	cmp 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 103
	st.w r10, 0x00000000[r5]
	movea 0x00000050, r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r12, 0xFFE50058[r13]
	mov r7, r12
	bz9 .BB.LABEL.4_2
.BB.LABEL.4_1:	; bb19
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r12
	mov r7, r5
.BB.LABEL.4_2:	; bb23
	add r12, r5
	movea 0x0000001F, r0, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 130
	cmp r12, r5
	bh9 .BB.LABEL.4_4
.BB.LABEL.4_3:	; bb.i55
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r5, r5
	mov 0xFFFEEA00, r12
	br9 .BB.LABEL.4_5
.BB.LABEL.4_4:	; bb8.i58
	mov 0x7FFFFFE0, r12
	add r12, r5
	mov r5, r12
	add r12, r12
	movea 0xFFFFB040, r0, r5
.BB.LABEL.4_5:	; HwcounterDisableInterrupt.1.exit68
	add r5, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.h 0x00000000[r12], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	ori 0x00000080, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 112
	cmp 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r12]
	movea 0x00000050, r0, r5
	mov r7, r6
	bz9 .BB.LABEL.4_7
.BB.LABEL.4_6:	; bb33
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r6
	mov r7, r5
.BB.LABEL.4_7:	; bb38
	add r6, r5
	movea 0x0000001F, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 116
	cmp r6, r5
	bh9 .BB.LABEL.4_9
.BB.LABEL.4_8:	; bb.i39
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.4_10
.BB.LABEL.4_9:	; bb8.i42
	mov 0x7FFFFFE0, r6
	add r6, r5
	mov r5, r6
	add r6, r6
	movea 0xFFFFB040, r0, r5
.BB.LABEL.4_10:	; HwcounterClearInterrupt.1.exit52
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 121
	mov r8, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 121
	shl 0x0000000C, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 127
	shl 0x00000002, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 162
	shl r9, r11, r14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 128
	add r13, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	andi 0x0000EFFF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	mov r9, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.hu 0xFFE50090[r13], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 116
	andi 0x0000FFF0, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFE50090[r13]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.hu 0xFFE50090[r12], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 122
	andi 0x0000FFF0, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 133
	cmp 0x00000000, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFE50090[r12]
	st23.h r0, 0xFFE50080[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 92
	st23.b r11, 0xFFE50020[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r14, 0xFFE50058[r12]
	movea 0x00000050, r0, r5
	bz9 .BB.LABEL.4_12
.BB.LABEL.4_11:	; bb115
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r6
	mov r9, r5
.BB.LABEL.4_12:	; bb120
	add r6, r5
	movea 0x0000001F, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 130
	cmp r6, r5
	bh9 .BB.LABEL.4_14
.BB.LABEL.4_13:	; bb.i17
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.4_15
.BB.LABEL.4_14:	; bb8.i20
	mov 0x7FFFFFE0, r6
	add r6, r5
	mov r5, r6
	add r6, r6
	movea 0xFFFFB040, r0, r5
.BB.LABEL.4_15:	; HwcounterDisableInterrupt.1.exit
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.h 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	ori 0x00000080, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 135
	cmp 0x00000000, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	movea 0x00000050, r0, r5
	mov r9, r6
	bz9 .BB.LABEL.4_17
.BB.LABEL.4_16:	; bb131
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r6
	mov r9, r5
.BB.LABEL.4_17:	; bb136
	add r6, r5
	movea 0x0000001F, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 116
	cmp r6, r5
	bh9 .BB.LABEL.4_19
.BB.LABEL.4_18:	; bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.4_20
.BB.LABEL.4_19:	; bb8.i
	mov 0x7FFFFFE0, r6
	add r6, r5
	mov r5, r6
	add r6, r6
	movea 0xFFFFB040, r0, r5
.BB.LABEL.4_20:	; HwcounterClearInterrupt.1.exit
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 138
	shl 0x00000002, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 142
	add r9, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	andi 0x0000EFFF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	st23.h r0, 0xFFE50080[r12]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 92
	st23.b r11, 0xFFE50020[r12]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r10, 0xFFE50000[r12]
	jmp [r31]
	.align 4
_start_hwcounter_tauj:
	.stack _start_hwcounter_tauj = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 149
	jr _SetTimerStartTAUJ.1
	.align 4
_stop_hwcounter_tauj:
	.stack _stop_hwcounter_tauj = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 163
	prepare 0x00000079, 0x00000000
	mov r6, r20
	mov r7, r21
	mov r8, r22
	mov r9, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 166
	jarl _SetTimerStopTAUJ.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 169
	cmp 0x00000000, r20
	movea 0x00000050, r0, r5
	mov r21, r6
	bz9 .BB.LABEL.6_2
.BB.LABEL.6_1:	; bb13
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r6
	mov r21, r5
.BB.LABEL.6_2:	; bb17
	add r6, r5
	movea 0x0000001F, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 130
	cmp r6, r5
	bh9 .BB.LABEL.6_4
.BB.LABEL.6_3:	; bb.i8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.6_5
.BB.LABEL.6_4:	; bb8.i11
	mov 0x7FFFFFE0, r6
	add r6, r5
	mov r5, r6
	add r6, r6
	movea 0xFFFFB040, r0, r5
.BB.LABEL.6_5:	; HwcounterDisableInterrupt.1.exit
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.h 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	ori 0x00000080, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 171
	cmp 0x00000000, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	movea 0x00000050, r0, r6
	mov r21, r5
	bz9 .BB.LABEL.6_7
.BB.LABEL.6_6:	; bb27
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r5
	mov r21, r6
.BB.LABEL.6_7:	; bb32
	add r5, r6
	jarl _HwcounterClearInterrupt.1, r31
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 162
	mov r22, r6
	shl r23, r5
	shl 0x0000000C, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 179
	cmp 0x00000000, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFE50058[r6]
	movea 0x00000050, r0, r5
	mov r23, r6
	bz9 .BB.LABEL.6_9
.BB.LABEL.6_8:	; bb45
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r6
	mov r23, r5
.BB.LABEL.6_9:	; bb50
	add r6, r5
	movea 0x0000001F, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 116
	cmp r6, r5
	bh9 .BB.LABEL.6_11
.BB.LABEL.6_10:	; bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.6_12
.BB.LABEL.6_11:	; bb8.i
	mov 0x7FFFFFE0, r6
	add r6, r5
	mov r5, r6
	add r6, r6
	movea 0xFFFFB040, r0, r5
.BB.LABEL.6_12:	; HwcounterClearInterrupt.1.exit
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	andi 0x0000EFFF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	dispose 0x00000000, 0x00000079, [r31]
	.align 4
_set_hwcounter_tauj:
	.stack _set_hwcounter_tauj = 28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 190
	prepare 0x00000679, 0x00000000
	ld.w 0x00000020[r3], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 196
	mov r7, r22
	ld.w 0x0000001C[r3], r21
	mov r9, r23
	mov r8, r24
	addi 0x00000000, r6, r25
	bnz9 .BB.LABEL.7_2
.BB.LABEL.7_1:	; bb19.bb34_crit_edge.critedge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x00000050, r22, r6
	jarl _HwcounterClearInterrupt.1, r31
	movea 0x00000050, r0, r5
	mov r22, r6
	br9 .BB.LABEL.7_3
.BB.LABEL.7_2:	; bb15
	movea 0x000000A8, r22, r6
	jarl _HwcounterClearInterrupt.1, r31
	movea 0x000000A8, r0, r6
	mov r22, r5
.BB.LABEL.7_3:	; bb34
	add r6, r5
	movea 0x0000001F, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 141
	cmp r6, r5
	bh9 .BB.LABEL.7_5
.BB.LABEL.7_4:	; bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.7_6
.BB.LABEL.7_5:	; bb8.i
	mov 0x7FFFFFE0, r6
	add r6, r5
	mov r5, r6
	add r6, r6
	movea 0xFFFFB040, r0, r5
.BB.LABEL.7_6:	; HwcounterEnableInterrupt.1.exit
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 174
	shl 0x0000000C, r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 174
	shl 0x00000002, r23
	or r23, r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	andi 0x0000FF7F, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 204
	mov r21, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld23.w 0xFFE50010[r24], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 177
	subr r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 178
	divqu r20, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 204
	sub r5, r6
	cmp r21, r5
	bnh9 .BB.LABEL.7_8
.BB.LABEL.7_7:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 208
	add r6, r20
	add 0x00000001, r20
	mov r20, r6
.BB.LABEL.7_8:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 220
	mov r22, r5
	shl 0x0000000C, r25
	shl 0x00000002, r5
	or r25, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 215
	cmp 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 216
	cmov 0x00000002, 0x00000001, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r6, 0xFFE50000[r5]
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 155
	shl r22, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFE50054[r25]
	dispose 0x00000000, 0x00000679, [r31]
	.align 4
_get_hwcounter_tauj:
	.stack _get_hwcounter_tauj = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 232
	shl 0x0000000C, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 174
	shl 0x00000002, r7
	or r7, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld23.w 0xFFE50010[r6], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 177
	subr r8, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 178
	divqu r8, r10, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 235
	jmp [r31]
	.align 4
_cancel_hwcounter_tauj:
	.stack _cancel_hwcounter_tauj = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 241
	jr _SetTimerStopTAUJ.1
	.align 4
_trigger_hwcounter_tauj:
	.stack _trigger_hwcounter_tauj = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 251
	prepare 0x00000079, 0x00000000
	mov 0x00000001, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 162
	mov r6, r22
	shl r7, r20, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 257
	mov r7, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 162
	shl 0x0000000C, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 257
	cmp 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r21, 0xFFE50058[r22]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 257
	bnz9 .BB.LABEL.10_2
.BB.LABEL.10_1:	; bb15.bb30_crit_edge.critedge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x00000050, r23, r6
	jarl _HwcounterClearInterrupt.1, r31
	movea 0x00000050, r0, r5
	mov r23, r6
	br9 .BB.LABEL.10_3
.BB.LABEL.10_2:	; bb11
	movea 0x000000A8, r23, r6
	jarl _HwcounterClearInterrupt.1, r31
	movea 0x000000A8, r0, r6
	mov r23, r5
.BB.LABEL.10_3:	; bb30
	add r6, r5
	movea 0x0000001F, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.h", 141
	cmp r6, r5
	bh9 .BB.LABEL.10_5
.BB.LABEL.10_4:	; bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.10_6
.BB.LABEL.10_5:	; bb8.i
	mov 0x7FFFFFE0, r6
	add r6, r5
	mov r5, r6
	add r6, r6
	movea 0xFFFFB040, r0, r5
.BB.LABEL.10_6:	; HwcounterEnableInterrupt.1.exit
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 262
	shl 0x00000002, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 262
	add r22, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	andi 0x0000FF7F, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r20, 0xFFE50000[r23]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r21, 0xFFE50054[r22]
	dispose 0x00000000, 0x00000079, [r31]
	.align 4
_int_clear_hwcounter_tauj:
	.stack _int_clear_hwcounter_tauj = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 272
	cmp 0x00000000, r6
	movea 0x00000050, r0, r6
	mov r7, r5
	bz9 .BB.LABEL.11_2
.BB.LABEL.11_1:	; bb9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r5
	mov r7, r6
.BB.LABEL.11_2:	; bb13
	add r5, r6
	jr _HwcounterClearInterrupt.1
	.align 4
_int_cancel_hwcounter_tauj:
	.stack _int_cancel_hwcounter_tauj = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 283
	cmp 0x00000000, r6
	movea 0x00000050, r0, r6
	mov r7, r5
	bz9 .BB.LABEL.12_2
.BB.LABEL.12_1:	; bb9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 0
	movea 0x000000A8, r0, r5
	mov r7, r6
.BB.LABEL.12_2:	; bb13
	add r5, r6
	jr _HwcounterClearInterrupt.1
	.align 4
_increment_hwcounter_tauj:
	.stack _increment_hwcounter_tauj = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/tauj_hw_counter.c", 293
	jmp [r31]
