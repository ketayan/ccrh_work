#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\kernel\task.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _appmodeid
	.extern _tnum_task
	.extern _tnum_exttask
	.extern _tinib_table
	.extern _p_tcb_table
	.extern _internal_shutdownallcores
	.extern _v850_memory_changed
	.public _task_initialize
	.public _search_schedtsk
	.public _make_runnable
	.public _make_non_runnable
	.public _make_active
	.extern _start_r
	.public _preempt
	.public _suspend
	.public _exit_task
	.extern _force_release_spinlocks
	.extern _release_interrupts
	.extern _acquire_tsk_lock
	.extern _release_tsk_lock
	.extern _exit_and_dispatch

	.section .text, text
	.align 4
_task_initialize:
	.stack _task_initialize = 32
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 88
	prepare 0x00000779, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 95
	movhi HIGHW1(#_tnum_task), r0, r5
	ld.w LOWW(#_tnum_task)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	ld.w 0x00000000[r6], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 95
	cmp 0x00000000, r5
	bz9 .BB.LABEL.1_6
.BB.LABEL.1_1:	; entry.bb_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 0
	mov 0x00000000, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 95
	movhi HIGHW1(#_tnum_task), r0, r24
	mov #_p_tcb_table, r22
	mov #_tinib_table, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 101
	movhi HIGHW1(#_appmodeid), r0, r25
	mov 0x00000001, r26
.BB.LABEL.1_2:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 96
	ld.w 0x00000020[r23], r5
	cmp r5, r20
	bnz9 .BB.LABEL.1_5
.BB.LABEL.1_3:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 97
	ld.w 0x00000000[r22], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 98
	st.w r23, 0x00000008[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 99
	st.b r0, 0x00000011[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 100
	st.b r0, 0x00000010[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 101
	ld.w LOWW(#_appmodeid)[r25], r5
	ld.w 0x0000001C[r23], r7
	shl r5, r26, r5
	tst r7, r5
	bz9 .BB.LABEL.1_5
.BB.LABEL.1_4:	; if_then_bb33
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 102
	jarl _make_active, r31
.BB.LABEL.1_5:	; bb39
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 95
	ld.w LOWW(#_tnum_task)[r24], r5
	add 0x00000001, r21
	add 0x00000004, r22
	movea 0x00000024, r23, r23
	cmp r5, r21
	bl9 .BB.LABEL.1_2
.BB.LABEL.1_6:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 106
	dispose 0x00000000, 0x00000779, [r31]
	.align 4
_search_schedtsk:
	.stack _search_schedtsk = 28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 152
	prepare 0x00000061, 0x00000010
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 116
	ld.hu 0x0000009C[r6], r5
	mov r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 117
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_2
.BB.LABEL.2_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 155
	st.w r0, 0x00000014[r20]
	dispose 0x00000010, 0x00000061, [r31]
.BB.LABEL.2_2:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 158
	ld.w 0x00000018[r20], r5
	shl 0x00000003, r5
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 134
	ld.w 0x0000001C[r5], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 158
	movea 0x0000001C, r5, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 134
	cmp r6, r21
	bnz9 .BB.LABEL.2_4
.BB.LABEL.2_3:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x00000086, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.2_4:	; queue_delete_next.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 135
	ld.w 0x00000000[r21], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 136
	ld.w 0x00000000[r5], r6
	st.w r6, 0x00000000[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 137
	ld.w 0x00000000[r5], r6
	st.w r21, 0x00000004[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 139
	st.w r5, 0x00000014[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 159
	ld.w 0x00000018[r20], r5
	mov r5, r6
	shl 0x00000003, r6
	add r20, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 147
	ld.w 0x0000001C[r6], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 159
	movea 0x0000001C, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 148
	cmp r7, r6
	bnz9 .BB.LABEL.2_12
.BB.LABEL.2_5:	; if_then_bb32
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 143
	ld.h 0x0000009C[r20], r6
	mov 0x00000001, r7
	shl r5, r7, r5
	not r5, r5
	and r5, r6
	st.h r6, 0x0000009C[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 117
	andi 0x0000FFFF, r6, r5
	cmov 0x00000002, 0x0000000F, r5, r5
	bz9 .BB.LABEL.2_11
.BB.LABEL.2_6:	; bb44
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000001[r3]
	mov 0x00000002, r8
	st.b r7, 0x00000002[r3]
	mov 0x00000003, r9
	st.b r0, 0x00000003[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 405
	andi 0x000000FF, r6, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000005[r3]
	st.b r7, 0x00000006[r3]
	st.b r0, 0x00000007[r3]
	st.b r0, 0x00000009[r3]
	st.b r7, 0x0000000A[r3]
	st.b r0, 0x0000000B[r3]
	st.b r0, 0x0000000D[r3]
	st.b r7, 0x0000000E[r3]
	mov 0x00000000, r7
	st.b r0, 0x0000000F[r3]
	st.b r8, 0x00000004[r3]
	st.b r9, 0x00000008[r3]
	st.b r8, 0x0000000C[r3]
	bnz9 .BB.LABEL.2_8
.BB.LABEL.2_7:	; if_then_bb34.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 406
	shr 0x00000008, r5
	mov 0x00000008, r7
	mov r5, r6
.BB.LABEL.2_8:	; if_break_bb43.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 409
	andi 0x0000000F, r6, r0
	bnz9 .BB.LABEL.2_10
.BB.LABEL.2_9:	; if_then_bb50.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	andi 0x0000FFFF, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 411
	add 0x00000004, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	shr 0x00000004, r6
.BB.LABEL.2_10:	; primap_search.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 413
	andi 0x0000000F, r6, r5
	movea 0x00000001, r3, r6
	add r6, r5
	ld.bu 0xFFFFFFFF[r5], r5
	add r7, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 414
	andi 0x0000FFFF, r5, r5
.BB.LABEL.2_11:	; bb47
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 0
	st.w r5, 0x00000018[r20]
.BB.LABEL.2_12:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 165
	dispose 0x00000010, 0x00000061, [r31]
	.align 4
_make_runnable:
	.stack _make_runnable = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 175
	ld.w 0x00000008[r6], r5
	mov 0x00000002, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 181
	ld.w 0x00000020[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 182
	st.b r7, 0x00000010[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 184
	ld.w 0x00000014[r5], r7
	cmp 0x00000000, r7
	bz9 .BB.LABEL.3_3
.BB.LABEL.3_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 185
	ld.w 0x0000000C[r6], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 186
	ld.w 0x0000000C[r7], r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 187
	cmp r9, r8
	bge9 .BB.LABEL.3_4
.BB.LABEL.3_2:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 204
	mov r9, r8
	shl 0x00000003, r8
	add r5, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 110
	ld.w 0x0000001C[r8], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 204
	movea 0x0000001C, r8, r11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 111
	st.w r11, 0x00000004[r7]
	mov 0x00000001, r11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	shl r9, r11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 110
	st.w r10, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 112
	ld.w 0x0000001C[r8], r10
	st.w r7, 0x00000004[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 113
	st.w r7, 0x0000001C[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	ld.h 0x0000009C[r5], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 206
	st.w r9, 0x00000018[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	or r11, r8
	st.h r8, 0x0000009C[r5]
.BB.LABEL.3_3:	; if_then_bb72
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 211
	st.w r6, 0x00000014[r5]
	mov 0x00000001, r10
	jmp [r31]
.BB.LABEL.3_4:	; if_then_bb30
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 192
	mov r8, r7
	shl 0x00000003, r7
	add r5, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 98
	ld.w 0x00000020[r7], r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 192
	movea 0x0000001C, r7, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 99
	st.w r10, 0x00000000[r6]
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	shl r8, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 98
	st.w r9, 0x00000004[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 100
	ld.w 0x00000020[r7], r9
	st.w r6, 0x00000000[r9]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 101
	st.w r6, 0x00000020[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	ld.h 0x0000009C[r5], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 194
	ld.w 0x00000018[r5], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	or r10, r6
	st.h r6, 0x0000009C[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 194
	cmp r7, r8
	bge9 .BB.LABEL.3_6
.BB.LABEL.3_5:	; if_then_bb47
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 195
	st.w r8, 0x00000018[r5]
.BB.LABEL.3_6:	; if_break_bb66
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 0
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 214
	jmp [r31]
	.align 4
_make_non_runnable:
	.stack _make_non_runnable = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 224
	prepare 0x00000001, 0x00000010
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 228
	ld.w 0x00000010[r6], r5
	ld.w 0x00000014[r6], r7
	cmp r7, r5
	bnz9 .BB.LABEL.4_2
.BB.LABEL.4_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 229
	jarl _search_schedtsk, r31
	dispose 0x00000010, 0x00000001, [r31]
.BB.LABEL.4_2:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	ld.w 0x00000000[r5], r7
	ld.w 0x00000004[r5], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 236
	ld.w 0x0000000C[r5], r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	st.w r7, 0x00000000[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 123
	ld.w 0x00000004[r5], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 238
	mov r9, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 123
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 238
	shl 0x00000003, r8
	add r6, r8
	movea 0x0000001C, r8, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 123
	st.w r7, 0x00000004[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 147
	ld.w 0x0000001C[r8], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 148
	cmp r5, r10
	bnz9 .BB.LABEL.4_10
.BB.LABEL.4_3:	; if_then_bb30
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 143
	ld.h 0x0000009C[r6], r5
	mov 0x00000001, r7
	shl r9, r7, r8
	not r8, r8
	and r8, r5
	st.h r5, 0x0000009C[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 117
	andi 0x0000FFFF, r5, r8
	cmov 0x00000002, 0x0000000F, r5, r5
	bz9 .BB.LABEL.4_9
.BB.LABEL.4_4:	; bb40
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000001[r3]
	mov 0x00000002, r9
	st.b r7, 0x00000002[r3]
	mov 0x00000003, r10
	st.b r0, 0x00000003[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 405
	andi 0x000000FF, r5, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000005[r3]
	st.b r7, 0x00000006[r3]
	st.b r0, 0x00000007[r3]
	st.b r0, 0x00000009[r3]
	st.b r7, 0x0000000A[r3]
	st.b r0, 0x0000000B[r3]
	st.b r0, 0x0000000D[r3]
	st.b r7, 0x0000000E[r3]
	mov 0x00000000, r7
	st.b r0, 0x0000000F[r3]
	st.b r9, 0x00000004[r3]
	st.b r10, 0x00000008[r3]
	st.b r9, 0x0000000C[r3]
	bnz9 .BB.LABEL.4_6
.BB.LABEL.4_5:	; if_then_bb34.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 406
	shr 0x00000008, r8
	mov 0x00000008, r7
	mov r8, r5
.BB.LABEL.4_6:	; if_break_bb43.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 409
	andi 0x0000000F, r5, r0
	bnz9 .BB.LABEL.4_8
.BB.LABEL.4_7:	; if_then_bb50.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	andi 0x0000FFFF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 411
	add 0x00000004, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	shr 0x00000004, r5
.BB.LABEL.4_8:	; primap_search.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 413
	andi 0x0000000F, r5, r5
	movea 0x00000001, r3, r8
	add r8, r5
	ld.bu 0xFFFFFFFF[r5], r5
	add r7, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 414
	andi 0x0000FFFF, r5, r5
.BB.LABEL.4_9:	; bb43
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 0
	st.w r5, 0x00000018[r6]
.BB.LABEL.4_10:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 245
	dispose 0x00000010, 0x00000001, [r31]
	.align 4
_make_active:
	.stack _make_active = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 258
	ld.w 0x00000008[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 261
	movhi HIGHW1(#_tnum_exttask), r0, r9
	mov #_tinib_table, r7
	mov 0x38E38E39, r8
	subr r5, r7
	ld.w LOWW(#_tnum_exttask)[r9], r9
	mul r8, r7, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 260
	ld.w 0x00000010[r5], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 261
	mov r7, r8
	sar 0x00000003, r8
	shr 0x0000001F, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 260
	st.w r10, 0x0000000C[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 261
	add r7, r8
	cmp r9, r8
	bnc9 .BB.LABEL.5_2
.BB.LABEL.5_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 262
	st.w r0, 0x00000014[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 263
	st.w r0, 0x00000018[r6]
.BB.LABEL.5_2:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 265
	st.w r0, 0x0000001C[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 266
	st.w r0, 0x00000020[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 267
	ld.w 0x00000008[r5], r7
	ld.w 0x00000004[r5], r5
	mov #_start_r, r8
	st.w r8, 0x00000028[r6]
	add r5, r7
	st.w r7, 0x00000024[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 268
	jr _make_runnable
	.align 4
_preempt:
	.stack _preempt = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 279
	prepare 0x00000001, 0x00000010
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 283
	ld.w 0x00000010[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 284
	ld.w 0x00000014[r6], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 283
	ld.w 0x0000000C[r5], r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 284
	cmp r8, r5
	bnz9 .BB.LABEL.6_2
.BB.LABEL.6_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 285
	mov r9, r7
	shl 0x00000003, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 110
	add r6, r7
	ld.w 0x0000001C[r7], r8
	movea 0x0000001C, r7, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 111
	st.w r10, 0x00000004[r5]
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	shl r9, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 110
	st.w r8, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 112
	ld.w 0x0000001C[r7], r9
	st.w r5, 0x00000004[r9]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 113
	st.w r5, 0x0000001C[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	ld.h 0x0000009C[r6], r5
	or r10, r5
	st.h r5, 0x0000009C[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 287
	jarl _search_schedtsk, r31
	dispose 0x00000010, 0x00000001, [r31]
.BB.LABEL.6_2:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	ld.w 0x00000000[r5], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 297
	mov r7, r11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	ld.w 0x00000004[r5], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 297
	shl 0x00000003, r11
	add r6, r11
	movea 0x0000001C, r11, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	st.w r8, 0x00000000[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 123
	ld.w 0x00000004[r5], r8
	ld.w 0x00000000[r5], r5
	st.w r8, 0x00000004[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 147
	ld.w 0x0000001C[r11], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 148
	cmp r5, r12
	bnz9 .BB.LABEL.6_10
.BB.LABEL.6_3:	; if_then_bb42
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 143
	ld.h 0x0000009C[r6], r5
	mov 0x00000001, r8
	shl r7, r8, r7
	not r7, r7
	and r7, r5
	st.h r5, 0x0000009C[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 117
	andi 0x0000FFFF, r5, r7
	cmov 0x00000002, 0x0000000F, r5, r5
	bz9 .BB.LABEL.6_9
.BB.LABEL.6_4:	; bb52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000001[r3]
	mov 0x00000002, r10
	st.b r8, 0x00000002[r3]
	mov 0x00000003, r11
	st.b r0, 0x00000003[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 405
	andi 0x000000FF, r5, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000005[r3]
	st.b r8, 0x00000006[r3]
	st.b r0, 0x00000007[r3]
	st.b r0, 0x00000009[r3]
	st.b r8, 0x0000000A[r3]
	st.b r0, 0x0000000B[r3]
	st.b r0, 0x0000000D[r3]
	st.b r8, 0x0000000E[r3]
	mov 0x00000000, r8
	st.b r0, 0x0000000F[r3]
	st.b r10, 0x00000004[r3]
	st.b r11, 0x00000008[r3]
	st.b r10, 0x0000000C[r3]
	bnz9 .BB.LABEL.6_6
.BB.LABEL.6_5:	; if_then_bb34.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 406
	shr 0x00000008, r7
	mov 0x00000008, r8
	mov r7, r5
.BB.LABEL.6_6:	; if_break_bb43.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 409
	andi 0x0000000F, r5, r0
	bnz9 .BB.LABEL.6_8
.BB.LABEL.6_7:	; if_then_bb50.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	andi 0x0000FFFF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 411
	add 0x00000004, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	shr 0x00000004, r5
.BB.LABEL.6_8:	; primap_search.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 413
	andi 0x0000000F, r5, r5
	movea 0x00000001, r3, r7
	add r7, r5
	ld.bu 0xFFFFFFFF[r5], r5
	add r8, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 414
	andi 0x0000FFFF, r5, r5
.BB.LABEL.6_9:	; bb55
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 0
	st.w r5, 0x00000018[r6]
.BB.LABEL.6_10:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 303
	mov r9, r7
	shl 0x00000003, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 110
	add r6, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 303
	ld.w 0x00000010[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 110
	ld.w 0x0000001C[r7], r8
	movea 0x0000001C, r7, r10
	mov 0x00000001, r11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	shl r9, r11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 111
	st.w r10, 0x00000004[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 110
	st.w r8, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 112
	ld.w 0x0000001C[r7], r8
	st.w r5, 0x00000004[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 113
	st.w r5, 0x0000001C[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 134
	ld.h 0x0000009C[r6], r5
	or r11, r5
	st.h r5, 0x0000009C[r6]
	dispose 0x00000010, 0x00000001, [r31]
	.align 4
_suspend:
	.stack _suspend = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 316
	prepare 0x00000041, 0x00000000
	mov r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 318
	ld.w 0x00000010[r20], r5
	st.b r0, 0x00000010[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 320
	jarl _make_non_runnable, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 321
	ld.w 0x00000010[r20], r5
	ld.bu 0x00000011[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.7_2
.BB.LABEL.7_1:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 325
	dispose 0x00000000, 0x00000041, [r31]
.BB.LABEL.7_2:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 322
	add 0xFFFFFFFF, r6
	st.b r6, 0x00000011[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 323
	ld.w 0x00000010[r20], r6
	dispose 0x00000000, 0x00000041
	jr _make_active
	.align 4
_exit_task:
	.stack _exit_task = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 359
	prepare 0x00000071, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.8_2
.BB.LABEL.8_1:	; if_then_bb.i10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.8_2:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 366
	ld.w 0x00000010[r21], r5
	ld.w 0x00000020[r5], r6
	cmp 0x00000000, r6
	bz9 .BB.LABEL.8_4
.BB.LABEL.8_3:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 367
	jarl _force_release_spinlocks, r31
.BB.LABEL.8_4:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 0
	movea 0x000000FF, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 371
	jarl _release_interrupts, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 374
	ld.w 0x00000010[r21], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 342
	ld.w 0x0000001C[r22], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.8_11
.BB.LABEL.8_5:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 343
	ld.w 0x0000000C[r22], r5
	cmp 0x00000000, r5
	bp9 .BB.LABEL.8_9
.BB.LABEL.8_6:	; if_then_bb12.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 283
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.8_8
.BB.LABEL.8_7:	; if_then_bb.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1616, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x0000011B, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.8_8:	; x_set_ipm.1.exit.i
	movea 0x00000010, r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 285
	st.b r5, 0x000000D4[r20]
.BB.LABEL.8_9:	; if_break_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 348
	ld.w 0x00000008[r22], r5
	ld.w 0x00000014[r5], r5
	st.w r5, 0x0000000C[r22]
.BB.LABEL.8_10:	; bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 352
	ld.w 0x0000001C[r22], r5
	st.b r0, 0x0000000C[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 353
	ld.w 0x0000001C[r22], r5
	ld.w 0x00000008[r5], r5
	st.w r5, 0x0000001C[r22]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 351
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.8_10
.BB.LABEL.8_11:	; release_taskresources.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 381
	mov r21, r6
	jarl _acquire_tsk_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 382
	mov r21, r6
	jarl _suspend, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 383
	mov r21, r6
	jarl _release_tsk_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/task.c", 386
	dispose 0x00000000, 0x00000071
	jr _exit_and_dispatch
	.section .const, const
.STR.1:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x69,0x6E,0x63,0x6C
	.db 0x75,0x64,0x65,0x5C,0x71,0x75,0x65,0x75,0x65,0x2E,0x68
	.ds (1)
.STR.1616:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x61,0x72,0x63,0x68
	.db 0x5C,0x76,0x38,0x35,0x30,0x5F,0x67,0x63,0x63,0x2F,0x70,0x72,0x63,0x5F,0x63,0x6F
	.db 0x6E,0x66,0x69,0x67,0x2E,0x68
	.ds (1)
