#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\arch\v850_ccrh\rh850_f1h.c"

	$reg_mode 32

	.public _EnableSubOSC
	.public _EnableMainOSC
	.public _EnablePLL0
	.public _EnablePLL1
	.public _SetClockSelection
	.public _raise_ipir

	.section .text, text
	.align 4
_write_protected_reg.1:
	.stack _write_protected_reg.1 = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 75
	add 0xFFFFFFF8, r3
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 81
	addi 0xFFFFFFE7, r8, r0
	blt9 .BB.LABEL.1_2
.BB.LABEL.1_1:	; bb47
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000002, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 97
	dispose 0x00000008, 0x00000000, [r31]
.BB.LABEL.1_2:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 98
	st.w r5, 0x00000004[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 139
	ld.w 0x00000004[r3], r5
	ori 0x00000020, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 141
	ld.w 0x00000004[r3], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 86
	shl 0x00000002, r8
	mov #_cmd_reg.1, r9
	add r8, r9
	movea 0x000000A5, r0, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 142
	st.w r5, 0x00000000[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 89
	not r7, r11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 86
	ld.w 0x00000000[r9], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st.w r10, 0x00000000[r5]
	st.w r7, 0x00000000[r6]
	st.w r11, 0x00000000[r6]
	st.w r7, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 91
	ld.w 0x00000000[r3], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 150
	andi 0x00000020, r5, r0
	bnz9 .BB.LABEL.1_4
.BB.LABEL.1_3:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	movea 0xFFFFFFDF, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 98
	and r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.1_4:	; TOPPERS_enaint.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 93
	mov #_s_reg.2, r5
	add r5, r8
	ld.w 0x00000000[r8], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 96
	andi 0x00000001, r5, r5
	cmov 0x0000000A, 0x00000003, r5, r10
	dispose 0x00000008, 0x00000000, [r31]
	.align 4
_EnableSubOSC:
	.stack _EnableSubOSC = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 108
	prepare 0x00000041, 0x00000000
	mov 0x00000000, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 111
	mov r7, r8
	mov 0xFFF81200, r6
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bz9 .BB.LABEL.2_2
.BB.LABEL.2_1:	; entry.bb28_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000001, r20
	br9 .BB.LABEL.2_5
.BB.LABEL.2_2:	; bb5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld23.w 0xFFF81204[r0], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 129
	andi 0x00000004, r20, r0
	bnz9 .BB.LABEL.2_2
.BB.LABEL.2_3:	; bb11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000000, r20
	mov 0x00000001, r7
	mov 0xFFF81200, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 118
	mov r20, r8
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.2_1
.BB.LABEL.2_4:	; bb20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld23.w 0xFFF81204[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 129
	andi 0x00000004, r5, r0
	bz9 .BB.LABEL.2_4
.BB.LABEL.2_5:	; bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov r20, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 125
	dispose 0x00000000, 0x00000041, [r31]
	.align 4
_EnableMainOSC:
	.stack _EnableMainOSC = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 135
	prepare 0x00000071, 0x00000000
	mov r6, r20
	mov 0x00000000, r21
	mov 0x00000002, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 140
	mov r22, r7
	mov 0xFFF81100, r6
	mov r21, r8
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bz9 .BB.LABEL.3_2
.BB.LABEL.3_1:	; entry.bb93_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000001, r20
	br9 .BB.LABEL.3_10
.BB.LABEL.3_2:	; if_break_bb
	mov 0x007A1200, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 142
	cmp r5, r20
	cmov 0x00000002, 0x00000003, r22, r22
	bz9 .BB.LABEL.3_7
.BB.LABEL.3_3:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0xFF85EDFF, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 145
	add r20, r5
	mov 0x007A11FF, r6
	cmp r6, r5
	bnh9 .BB.LABEL.3_7
.BB.LABEL.3_4:	; if_else_bb26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0xFF0BDBFF, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 148
	add r20, r22
	mov 0x003D08FF, r5
	cmp r5, r22
	mov 0x00000001, r22
	bnh9 .BB.LABEL.3_7
.BB.LABEL.3_5:	; if_else_bb43
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0xFECED2FF, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 151
	add r22, r20
	mov 0x003D0900, r22
	cmp r22, r20
	mov 0x00000002, r20
	bnc9 .BB.LABEL.3_10
.BB.LABEL.3_6:	; if_else_bb43.bb67.preheader_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov r21, r22
.BB.LABEL.3_7:	; bb67
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld23.w 0xFFF81104[r0], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 129
	andi 0x00000004, r20, r0
	bnz9 .BB.LABEL.3_7
.BB.LABEL.3_8:	; bb73
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 162
	andi 0x000000FF, r22, r20
	mov 0x00000001, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r20, 0xFFF81108[r0]
	movea 0x000000FF, r0, r20
	st23.w r20, 0xFFF8110C[r0]
	mov 0x00000000, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 166
	mov r20, r8
	mov 0xFFF81100, r6
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.3_1
.BB.LABEL.3_9:	; bb85
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld23.w 0xFFF81104[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 129
	andi 0x00000004, r5, r0
	bz9 .BB.LABEL.3_9
.BB.LABEL.3_10:	; bb93
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov r20, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 173
	dispose 0x00000000, 0x00000071, [r31]
	.align 4
_EnablePLL0:
	.stack _EnablePLL0 = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 183
	prepare 0x00000041, 0x00000000
	mov 0x00000001, r20
	mov 0x00000002, r7
	mov 0xFFF89000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 186
	mov r20, r8
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.4_4
.BB.LABEL.4_1:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x40000A3B, r5
	mov 0x00000001, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 194
	mov r7, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r5, 0xFFF89008[r0]
	mov 0xFFF89000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 194
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.4_4
.BB.LABEL.4_2:	; bb12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld23.w 0xFFF89004[r0], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 129
	andi 0x00000004, r20, r0
	bz9 .BB.LABEL.4_2
.BB.LABEL.4_3:	; bb12.bb20_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000000, r20
.BB.LABEL.4_4:	; bb20
	mov r20, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 201
	dispose 0x00000000, 0x00000041, [r31]
	.align 4
_EnablePLL1:
	.stack _EnablePLL1 = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 212
	prepare 0x00000041, 0x00000000
	mov 0x00000001, r20
	mov 0x00000002, r7
	mov 0xFFF89100, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 215
	mov r20, r8
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.5_4
.BB.LABEL.5_1:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	movea 0x00000A27, r0, r5
	mov 0x00000001, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r5, 0xFFF89108[r0]
	mov 0xFFF89100, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 220
	mov r7, r8
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.5_4
.BB.LABEL.5_2:	; bb12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld23.w 0xFFF89104[r0], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 129
	andi 0x00000004, r20, r0
	bz9 .BB.LABEL.5_2
.BB.LABEL.5_3:	; bb12.bb20_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000000, r20
.BB.LABEL.5_4:	; bb20
	mov r20, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 227
	dispose 0x00000000, 0x00000041, [r31]
	.align 4
_SetClockSelection:
	.stack _SetClockSelection = 24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 243
	prepare 0x00000479, 0x00000000
	mov r7, r21
	mov r6, r20
	mov r8, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 247
	mov r9, r7
	jarl _write_protected_reg.1, r31
	ld.w 0x00000020[r3], r7
	cmp 0x00000000, r10
	ld.w 0x0000001C[r3], r23
	ld.w 0x00000018[r3], r24
	bz9 .BB.LABEL.6_3
.BB.LABEL.6_1:	; entry.bb63_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000001, r10
.BB.LABEL.6_2:	; bb63
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 269
	dispose 0x00000000, 0x00000479, [r31]
.BB.LABEL.6_3:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 252
	cmp 0x00000000, r24
	bz9 .BB.LABEL.6_5
.BB.LABEL.6_4:	; if_then_bb19
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 253
	mov r24, r6
	mov r22, r8
	jarl _write_protected_reg.1, r31
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.6_1
.BB.LABEL.6_5:	; bb33
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld.w 0x00000000[r20], r5
	ld.w 0x00000000[r21], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 129
	cmp r6, r5
	bnz9 .BB.LABEL.6_5
.BB.LABEL.6_6:	; bb43
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 262
	cmp 0x00000000, r24
	bnz9 .BB.LABEL.6_8
.BB.LABEL.6_7:	; bb43.bb63_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000000, r10
	br9 .BB.LABEL.6_2
.BB.LABEL.6_8:	; bb50
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 127
	ld.w 0x00000000[r24], r5
	ld.w 0x00000000[r23], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 129
	cmp r6, r5
	bnz9 .BB.LABEL.6_8
	br9 .BB.LABEL.6_7
	.align 4
_raise_ipir:
	.stack _raise_ipir = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 272
	shl 0x00000002, r6
	mov 0xFFFEEC80, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	cmp 0x00000001, r5
	bnz9 .BB.LABEL.7_2
.BB.LABEL.7_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000002, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st.w r5, 0x00000000[r6]
	jmp [r31]
.BB.LABEL.7_2:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/rh850_f1h.c", 0
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st.w r5, 0x00000000[r6]
	jmp [r31]
	.section .const, const
	.align 4
_cmd_reg.1:
	.dw 0xFFF80000,0xFFF88000,0xFFF8C010,0xFFF8D010,0xFFF8E010,0xFFF8C200,0xFFC204C0,0xFFC14C00
	.dw 0xFFC14C04,0xFFC14C08,0xFFC14C0C,0xFFC14C20,0xFFC14C24,0xFFC14C28,0xFFC14C2C,0xFFC14C30
	.dw 0xFFC14C34,0xFFC14C48,0xFFC14C4C,0xFFC14C50,0xFFC14C54,0xFFC14C58,0xFFF83200,0xFFA00004
	.align 4
_s_reg.2:
	.dw 0xFFF80004,0xFFF88004,0xFFF8C014,0xFFF8D014,0xFFF8E014,0xFFF8C204,0xFFC204B0,0xFFC14B00
	.dw 0xFFC14B04,0xFFC14B08,0xFFC14B0C,0xFFC14B20,0xFFC14B24,0xFFC14B28,0xFFC14B2C,0xFFC14B30
	.dw 0xFFC14B34,0xFFC14B48,0xFFC14B4C,0xFFC14B50,0xFFC14B54,0xFFC14B58,0xFFF83204,0xFFA00008
