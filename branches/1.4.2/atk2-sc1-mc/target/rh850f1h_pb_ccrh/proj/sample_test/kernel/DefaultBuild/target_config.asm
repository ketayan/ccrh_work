#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\target\rh850f1h_pb_ccrh\target_config.c"

	$reg_mode 32

	.extern _tnum_intno
	.extern _intinib_table
	.public _rlin3x_base_table, 8
	.public _target_ici_intno_table, 8
	.public _target_ici_intpri_table, 8
	.public _target_fput_str
	.extern _uart_putc
	.public _target_port_initialize
	.public _target_clock_initialize
	.extern _EnableMainOSC
	.extern _EnablePLL0
	.extern _EnablePLL1
	.extern _SetClockSelection
	.extern _infinite_loop
	.public _target_hardware_initialize
	.public _hardware_init_hook
	.public _target_initialize
	.extern _prc_initialize
	.extern _x_config_int
	.public _target_exit
	.extern _prc_terminate
	.public _target_fput_log

	.section .text, text
	.align 4
_target_fput_str:
	.stack _target_fput_str = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 102
	prepare 0x00000041, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 104
	ld.bu 0x00000000[r6], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.1_3
.BB.LABEL.1_1:	; bb.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 0
	add 0x00000001, r6
	mov r6, r20
	mov r5, r6
.BB.LABEL.1_2:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 105
	sxb r6
	jarl _uart_putc, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 104
	ld.bu 0x00000000[r20], r6
	add 0x00000001, r20
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.1_2
.BB.LABEL.1_3:	; bb11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 108
	dispose 0x00000000, 0x00000041
	mov 0x0000000A, r6
	jr _uart_putc
	.align 4
_target_port_initialize:
	.stack _target_port_initialize = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.h 0xFFC10528[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 126
	ori 0x00000600, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10528[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.hu 0xFFC10628[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 131
	andi 0x0000F9FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10628[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.hu 0xFFC10A28[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 137
	andi 0x0000F9FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10A28[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.h 0xFFC10328[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 144
	ori 0x00000200, r5, r5
	andi 0x0000FBFF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10328[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.h 0xFFC10428[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 150
	ori 0x00000600, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10428[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.h 0xFFC14028[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 156
	ori 0x00000200, r5, r5
	andi 0x0000FBFF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC14028[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.hu 0xFFC10500[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 164
	andi 0x0000FFCF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10500[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.hu 0xFFC10600[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 170
	andi 0x0000FFCF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10600[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.hu 0xFFC10A00[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 176
	andi 0x0000FFCF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10A00[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.h 0xFFC10300[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 183
	ori 0x00000010, r5, r5
	andi 0x0000FFDF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10300[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.h 0xFFC10400[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 189
	ori 0x00000030, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC10400[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld23.h 0xFFC14000[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 195
	ori 0x00000010, r5, r5
	andi 0x0000FFDF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st23.h r5, 0xFFC14000[r0]
	jmp [r31]
	.align 4
_target_clock_initialize:
	.stack _target_clock_initialize = 44
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 203
	prepare 0x00000779, 0x0000000C
	mov 0x00F42400, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 208
	jarl _EnableMainOSC, r31
	mov r10, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 213
	jarl _EnablePLL0, r31
	mov r10, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 218
	jarl _EnablePLL1, r31
	mov 0xFFF8A808, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 223
	st.w r5, 0x00000004[r3]
	mov 0x00000001, r23
	mov 0xFFF8A800, r5
	mov 0x00000002, r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 218
	mov r10, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 223
	st.w r23, 0x00000008[r3]
	mov r23, r8
	st.w r5, 0x00000000[r3]
	mov r24, r9
	mov 0xFFF8A408, r7
	mov 0xFFF8A400, r6
	jarl _SetClockSelection, r31
	mov 0xFFF82208, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 231
	st.w r5, 0x00000004[r3]
	mov 0x00000000, r26
	mov 0xFFF82200, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 223
	mov r10, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 231
	st.w r24, 0x00000008[r3]
	st.w r5, 0x00000000[r3]
	mov r26, r8
	mov 0xFFF82108, r7
	mov 0xFFF82100, r6
	mov r24, r9
	jarl _SetClockSelection, r31
	mov 0xFFF8A108, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 242
	st.w r5, 0x00000004[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 231
	mov r10, r24
	mov 0xFFF8A100, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 242
	st.w r23, 0x00000008[r3]
	mov 0x00000003, r9
	st.w r5, 0x00000000[r3]
	mov r23, r8
	mov 0xFFF8A008, r7
	mov 0xFFF8A000, r6
	jarl _SetClockSelection, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 227
	cmp 0x00000000, r25
	setf 0x0000000A, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 209
	cmp 0x00000000, r20
	setf 0x0000000A, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 214
	cmp 0x00000000, r21
	setf 0x0000000A, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 219
	cmp 0x00000000, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 236
	adf 0x0000000A, r6, r7, r6
	cmp 0x00000000, r24
	adf 0x0000000A, r5, r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 246
	cmp 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 249
	sbf 0x0000000A, r0, r26, r6
	cmp r6, r5
	bnz9 .BB.LABEL.3_2
.BB.LABEL.3_1:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 252
	dispose 0x0000000C, 0x00000779, [r31]
.BB.LABEL.3_2:	; if_then_bb50
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 250
	dispose 0x0000000C, 0x00000779
	jr _infinite_loop
	.align 4
_target_hardware_initialize:
	.stack _target_hardware_initialize = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 259
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 262
	jarl _target_clock_initialize, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 265
	dispose 0x00000000, 0x00000001
	jr _target_port_initialize
	.align 4
_hardware_init_hook:
	.stack _hardware_init_hook = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 272
	jmp [r31]
	.align 4
_target_initialize:
	.stack _target_initialize = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 281
	prepare 0x00000041, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 289
	jarl _prc_initialize, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov 0x00000001, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 300
	mov #_target_ici_intpri_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	add 0xFFFFFFFF, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 300
	andi 0x000000FF, r5, r20
	mov r20, r5
	shl 0x00000002, r5
	add r5, r6
	mov r20, r9
	ld.w 0x00000000[r6], r8
	mov #_target_ici_intno_table, r6
	add r6, r5
	ld.w 0x00000000[r5], r6
	jarl _x_config_int, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 303
	cmp 0x00000000, r20
	bnz9 .BB.LABEL.6_9
.BB.LABEL.6_1:	; bb54.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 304
	movhi HIGHW1(#_tnum_intno), r0, r5
	ld.w LOWW(#_tnum_intno)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.6_9
.BB.LABEL.6_2:	; bb54.preheader.bb_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 0
	mov 0x00000000, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 304
	movhi HIGHW1(#_tnum_intno), r0, r7
	mov #_intinib_table, r6
	movea 0x0000001F, r0, r8
	mov 0x3FFFFFE0, r9
	mov 0x00010002, r10
.BB.LABEL.6_3:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 305
	ld.bu 0x0000000C[r6], r11
	cmp 0x00000001, r11
	bnz9 .BB.LABEL.6_8
.BB.LABEL.6_4:	; if_then_bb23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 306
	ld.hu 0x00000000[r6], r11
	cmp r8, r11
	bh9 .BB.LABEL.6_6
.BB.LABEL.6_5:	; bb31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 0
	shl 0x00000002, r11
	mov 0xFFFEEB00, r12
	br9 .BB.LABEL.6_7
.BB.LABEL.6_6:	; bb39
	add r9, r11
	mov r11, r12
	shl 0x00000002, r12
	movea 0xFFFFB880, r0, r11
.BB.LABEL.6_7:	; bb48
	add r11, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st.w r10, 0x00000000[r12]
.BB.LABEL.6_8:	; bb54
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 304
	ld.w LOWW(#_tnum_intno)[r7], r11
	add 0x00000001, r5
	movea 0x00000010, r6, r6
	cmp r11, r5
	bl9 .BB.LABEL.6_3
.BB.LABEL.6_9:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 310
	dispose 0x00000000, 0x00000041, [r31]
	.align 4
_target_exit:
	.stack _target_exit = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 316
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 329
	mov #.STR.1, r6
	jarl _target_fput_str, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 337
	jarl _prc_terminate, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 352
	dispose 0x00000000, 0x00000001
	jr _infinite_loop
	.align 4
_target_fput_log:
	.stack _target_fput_log = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 359
	prepare 0x00000041, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 361
	cmp 0x0000000A, r6
	mov r6, r20
	bnz9 .BB.LABEL.8_2
.BB.LABEL.8_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 0
	mov 0x0000000D, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 362
	jarl _uart_putc, r31
.BB.LABEL.8_2:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_config.c", 364
	mov r20, r6
	dispose 0x00000000, 0x00000041
	jr _uart_putc
	.section .const, const
	.align 4
_rlin3x_base_table:
	.dw 0xFFCE2000,0xFFCE2040
	.align 4
_target_ici_intno_table:
	.dw 0x00010000,0x00020000
	.align 4
_target_ici_intpri_table:
	.dw 0xFFFFFFFF,0xFFFFFFFF
.STR.1:
	.db 0x4B,0x65,0x72,0x6E,0x65,0x6C,0x20,0x45,0x78,0x69,0x74,0x2E,0x2E,0x2E
	.ds (1)
