#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c"

	$reg_mode 32

	.public _init_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _init_hwcounter_tauj
	.public _start_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _start_hwcounter_tauj
	.public _stop_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _stop_hwcounter_tauj
	.public _set_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _set_hwcounter_tauj
	.public _get_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _get_hwcounter_tauj
	.public _cancel_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _cancel_hwcounter_tauj
	.public _trigger_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _trigger_hwcounter_tauj
	.public _int_clear_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _int_clear_hwcounter_tauj
	.public _int_cancel_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _int_cancel_hwcounter_tauj
	.public _increment_hwcounter_MAIN_HW_COUNTER_CORE0
	.extern _increment_hwcounter_tauj
	.public _init_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _start_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _stop_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _set_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _get_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _cancel_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _trigger_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _int_clear_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _int_cancel_hwcounter_MAIN_HW_COUNTER_CORE1
	.public _increment_hwcounter_MAIN_HW_COUNTER_CORE1

	.section .text, text
	.align 4
_init_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _init_hwcounter_MAIN_HW_COUNTER_CORE0 = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 65
	prepare 0x00000001, 0x0000000C
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 67
	st.w r6, 0x00000000[r3]
	mov 0x00000000, r6
	mov #_MAIN_HW_COUNTER_maxval.1, r5
	st.w r7, 0x00000004[r3]
	mov 0x00000001, r9
	st.w r5, 0x00000008[r3]
	mov r6, r7
	mov r6, r8
	jarl _init_hwcounter_tauj, r31
	dispose 0x0000000C, 0x00000001, [r31]
	.align 4
_start_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _start_hwcounter_MAIN_HW_COUNTER_CORE0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 76
	mov 0x00000001, r7
	mov 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 78
	jr _start_hwcounter_tauj
	.align 4
_stop_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _stop_hwcounter_MAIN_HW_COUNTER_CORE0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 85
	mov 0x00000000, r6
	mov 0x00000001, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 87
	mov r6, r7
	mov r6, r8
	jr _stop_hwcounter_tauj
	.align 4
_set_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _set_hwcounter_MAIN_HW_COUNTER_CORE0 = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 95
	prepare 0x00000001, 0x00000008
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 97
	movhi HIGHW1(#_MAIN_HW_COUNTER_maxval.1), r0, r5
	ld.w LOWW(#_MAIN_HW_COUNTER_maxval.1)[r5], r5
	st.w r6, 0x00000000[r3]
	mov 0x00000000, r6
	mov 0x00000001, r9
	mov r6, r7
	st.w r5, 0x00000004[r3]
	mov r6, r8
	jarl _set_hwcounter_tauj, r31
	dispose 0x00000008, 0x00000001, [r31]
	.align 4
_get_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _get_hwcounter_MAIN_HW_COUNTER_CORE0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 108
	movhi HIGHW1(#_MAIN_HW_COUNTER_maxval.1), r0, r5
	ld.w LOWW(#_MAIN_HW_COUNTER_maxval.1)[r5], r8
	mov 0x00000001, r7
	mov 0x00000000, r6
	jr _get_hwcounter_tauj
	.align 4
_cancel_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _cancel_hwcounter_MAIN_HW_COUNTER_CORE0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 116
	mov 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 118
	mov r6, r7
	jr _cancel_hwcounter_tauj
	.align 4
_trigger_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _trigger_hwcounter_MAIN_HW_COUNTER_CORE0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 125
	mov 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 127
	mov r6, r7
	jr _trigger_hwcounter_tauj
	.align 4
_int_clear_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _int_clear_hwcounter_MAIN_HW_COUNTER_CORE0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 134
	mov 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 136
	mov r6, r7
	jr _int_clear_hwcounter_tauj
	.align 4
_int_cancel_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _int_cancel_hwcounter_MAIN_HW_COUNTER_CORE0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 143
	mov 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 145
	mov r6, r7
	jr _int_cancel_hwcounter_tauj
	.align 4
_increment_hwcounter_MAIN_HW_COUNTER_CORE0:
	.stack _increment_hwcounter_MAIN_HW_COUNTER_CORE0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 152
	mov 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 154
	mov r6, r7
	jr _increment_hwcounter_tauj
	.align 4
_init_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _init_hwcounter_MAIN_HW_COUNTER_CORE1 = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 170
	prepare 0x00000001, 0x0000000C
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 172
	st.w r6, 0x00000000[r3]
	mov 0x00000001, r6
	mov #_MAIN_HW_COUNTER_maxval.1, r5
	st.w r7, 0x00000004[r3]
	mov 0x00000000, r7
	st.w r5, 0x00000008[r3]
	mov r6, r8
	mov r6, r9
	jarl _init_hwcounter_tauj, r31
	dispose 0x0000000C, 0x00000001, [r31]
	.align 4
_start_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _start_hwcounter_MAIN_HW_COUNTER_CORE1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 181
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 183
	mov r6, r7
	jr _start_hwcounter_tauj
	.align 4
_stop_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _stop_hwcounter_MAIN_HW_COUNTER_CORE1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 190
	mov 0x00000001, r6
	mov 0x00000000, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 192
	mov r6, r8
	mov r6, r9
	jr _stop_hwcounter_tauj
	.align 4
_set_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _set_hwcounter_MAIN_HW_COUNTER_CORE1 = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 200
	prepare 0x00000001, 0x00000008
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 202
	movhi HIGHW1(#_MAIN_HW_COUNTER_maxval.1), r0, r5
	ld.w LOWW(#_MAIN_HW_COUNTER_maxval.1)[r5], r5
	st.w r6, 0x00000000[r3]
	mov 0x00000001, r6
	mov 0x00000000, r7
	mov r6, r8
	st.w r5, 0x00000004[r3]
	mov r6, r9
	jarl _set_hwcounter_tauj, r31
	dispose 0x00000008, 0x00000001, [r31]
	.align 4
_get_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _get_hwcounter_MAIN_HW_COUNTER_CORE1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 213
	movhi HIGHW1(#_MAIN_HW_COUNTER_maxval.1), r0, r5
	ld.w LOWW(#_MAIN_HW_COUNTER_maxval.1)[r5], r8
	mov 0x00000001, r6
	mov r6, r7
	jr _get_hwcounter_tauj
	.align 4
_cancel_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _cancel_hwcounter_MAIN_HW_COUNTER_CORE1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 221
	mov 0x00000000, r7
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 223
	jr _cancel_hwcounter_tauj
	.align 4
_trigger_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _trigger_hwcounter_MAIN_HW_COUNTER_CORE1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 230
	mov 0x00000000, r7
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 232
	jr _trigger_hwcounter_tauj
	.align 4
_int_clear_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _int_clear_hwcounter_MAIN_HW_COUNTER_CORE1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 239
	mov 0x00000000, r7
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 241
	jr _int_clear_hwcounter_tauj
	.align 4
_int_cancel_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _int_cancel_hwcounter_MAIN_HW_COUNTER_CORE1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 248
	mov 0x00000000, r7
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 250
	jr _int_cancel_hwcounter_tauj
	.align 4
_increment_hwcounter_MAIN_HW_COUNTER_CORE1:
	.stack _increment_hwcounter_MAIN_HW_COUNTER_CORE1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 257
	mov 0x00000000, r7
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/target/rh850f1h_pb_ccrh/target_hw_counter.c", 259
	jr _increment_hwcounter_tauj
	.section .bss, bss
	.align 4
_MAIN_HW_COUNTER_maxval.1:
	.ds (4)
