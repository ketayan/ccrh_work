#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\kernel\scheduletable.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _pmr_setting_tbl
	.extern _appmodeid
	.extern _tnum_scheduletable
	.extern _tnum_implscheduletable
	.extern _schtblinib_table
	.extern _p_schtblcb_table
	.extern _v850_memory_changed
	.extern _internal_shutdownallcores
	.public _schtbl_initialize
	.extern _get_abstick
	.extern _get_reltick
	.extern _insert_cnt_expr_que
	.public _StartScheduleTableRel
	.extern _acquire_cnt_lock
	.extern _release_cnt_lock
	.public _StartScheduleTableAbs
	.public _StopScheduleTable
	.extern _delete_cnt_expr_que
	.public _NextScheduleTable
	.public _GetScheduleTableStatus
	.public _schtbl_expire
	.public _schtbl_expiry_process
	.public _schtbl_head
	.public _schtbl_exppoint_process
	.public _schtbl_tail

	.section .text, text
	.align 4
_schtbl_initialize:
	.stack _schtbl_initialize = 48
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 124
	prepare 0x00000FFF, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 132
	movhi HIGHW1(#_tnum_scheduletable), r0, r5
	ld.w LOWW(#_tnum_scheduletable)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	add 0xFFFFFFFF, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 132
	cmp 0x00000000, r5
	bz9 .BB.LABEL.1_11
.BB.LABEL.1_1:	; entry.bb_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000000, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 146
	movhi HIGHW1(#_appmodeid), r0, r24
	mov #_p_schtblcb_table, r22
	mov #_schtblinib_table, r23
	mov 0x00000001, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 147
	movhi HIGHW1(#_tnum_implscheduletable), r0, r26
	movea 0x00000010, r0, r27
	mov 0xFFFFFFFF, r28
.BB.LABEL.1_2:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 133
	ld.w 0x00000000[r23], r5
	ld.w 0x00000008[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.1_10
.BB.LABEL.1_3:	; bb14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	ld.bu 0x00000010[r5], r5
	andi 0x000000FF, r20, r6
	cmp r5, r6
	bnz9 .BB.LABEL.1_10
.BB.LABEL.1_4:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 135
	ld.w 0x00000000[r22], r29
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 144
	mov #_schtbl_expire, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 136
	st.w r23, 0x00000010[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 142
	st.w r0, 0x00000018[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 143
	st.w r0, 0x00000014[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 144
	st.w r5, 0x0000000C[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 146
	ld.w LOWW(#_appmodeid)[r24], r5
	ld.w 0x00000008[r23], r6
	shl r5, r25, r5
	tst r6, r5
	bnz9 .BB.LABEL.1_6
.BB.LABEL.1_5:	; if_else_bb108
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 173
	st.b r25, 0x0000001C[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r29, 0x00000004[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r29, 0x00000000[r29]
	br9 .BB.LABEL.1_10
.BB.LABEL.1_6:	; if_then_bb59
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 147
	ld.w LOWW(#_tnum_implscheduletable)[r26], r5
	cmp r5, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 151
	cmov 0x00000009, 0x00000008, r27, r5
	st.b r5, 0x0000001C[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 153
	st.b r28, 0x0000001D[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 157
	ld.w 0x0000000C[r23], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 156
	ld.w 0x00000010[r23], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 155
	ld.w 0x00000000[r23], r30
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 157
	add 0xFFFFFFF0, r5
	bnz9 .BB.LABEL.1_8
.BB.LABEL.1_7:	; if_then_bb90
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 163
	mov r30, r6
	jarl _get_abstick, r31
	br9 .BB.LABEL.1_9
.BB.LABEL.1_8:	; if_else_bb97
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 167
	mov r30, r6
	jarl _get_reltick, r31
.BB.LABEL.1_9:	; if_break_bb104
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	st.w r10, 0x00000008[r29]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 170
	mov r29, r6
	mov r30, r7
	jarl _insert_cnt_expr_que, r31
.BB.LABEL.1_10:	; bb118
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 132
	movhi HIGHW1(#_tnum_scheduletable), r0, r5
	ld.w LOWW(#_tnum_scheduletable)[r5], r5
	add 0x00000001, r21
	add 0x00000004, r22
	movea 0x00000020, r23, r23
	cmp r5, r21
	bl9 .BB.LABEL.1_2
.BB.LABEL.1_11:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 179
	dispose 0x00000000, 0x00000FFF, [r31]
	.align 4
_StartScheduleTableRel:
	.stack _StartScheduleTableRel = 24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 189
	prepare 0x00000479, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r7, r21
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	cmov 0x0000000A, 0x0000000C, r20, r20
	bnz9 .BB.LABEL.2_9
.BB.LABEL.2_1:	; bb20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x00000003, r5, r5
	cmp 0x00000003, r5
	cmov 0x0000000A, 0x00000002, r20, r20
	bnz9 .BB.LABEL.2_9
.BB.LABEL.2_2:	; bb39
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 199
	movhi HIGHW1(#_tnum_scheduletable), r0, r5
	ld.w LOWW(#_tnum_scheduletable)[r5], r5
	cmp r5, r6
	bl9 .BB.LABEL.2_4
.BB.LABEL.2_3:	; bb39.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000003, r20
	br9 .BB.LABEL.2_9
.BB.LABEL.2_4:	; bb55
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 200
	movhi HIGHW1(#_tnum_implscheduletable), r0, r5
	ld.w LOWW(#_tnum_implscheduletable)[r5], r5
	cmp r5, r6
	bl9 .BB.LABEL.2_3
.BB.LABEL.2_5:	; bb70
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 202
	shl 0x00000002, r6
	mov #_p_schtblcb_table, r5
	add r6, r5
	ld.w 0x00000000[r5], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 203
	ld.w 0x00000010[r22], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_7
.BB.LABEL.2_6:	; bb70.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movea 0x0000001D, r0, r20
	br9 .BB.LABEL.2_9
.BB.LABEL.2_7:	; bb90
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 204
	ld.w 0x00000000[r5], r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 206
	cmp 0x00000000, r21
	bnz9 .BB.LABEL.2_10
.BB.LABEL.2_8:	; bb90.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000008, r20
.BB.LABEL.2_9:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 244
	andi 0x000000FF, r20, r10
	dispose 0x00000000, 0x00000479, [r31]
.BB.LABEL.2_10:	; bb100
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	ld.w 0x00000008[r23], r6
	ld.w 0x00000014[r5], r5
	ld.w 0x00000000[r6], r7
	ld.w 0x00000000[r5], r5
	sub r5, r7
	cmp r21, r7
	bl9 .BB.LABEL.2_8
.BB.LABEL.2_11:	; bb134
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 209
	ld.bu 0x00000010[r6], r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.2_13
.BB.LABEL.2_12:	; if_then_bb.i24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.2_13:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 211
	mov r24, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 213
	ld.bu 0x0000001C[r22], r5
	cmp 0x00000001, r5
	cmov 0x0000000A, 0x00000007, r21, r21
	bnz9 .BB.LABEL.2_15
.BB.LABEL.2_14:	; bb161
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000008, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 215
	st.b r5, 0x0000001C[r22]
	mov 0xFFFFFFFF, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 216
	st.b r5, 0x0000001D[r22]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 217
	mov r23, r6
	mov r21, r7
	jarl _get_reltick, r31
	st.w r10, 0x00000008[r22]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 219
	mov r22, r6
	mov r23, r7
	jarl _insert_cnt_expr_que, r31
	mov 0x00000000, r21
.BB.LABEL.2_15:	; d_exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 222
	mov r24, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_17
.BB.LABEL.2_16:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.2_17:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.2_19
.BB.LABEL.2_18:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r20
	mov #_pmr_setting_tbl, r5
	add r20, r20
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r20, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.2_19:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	andi 0x000000FF, r21, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 244
	dispose 0x00000000, 0x00000479, [r31]
	.align 4
_StartScheduleTableAbs:
	.stack _StartScheduleTableAbs = 28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 254
	prepare 0x00000679, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r7, r21
	andi 0x00000003, r5, r5
	mov r6, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	cmov 0x0000000A, 0x0000000C, r20, r20
	bnz9 .BB.LABEL.3_5
.BB.LABEL.3_1:	; bb20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x00000003, r5, r5
	cmp 0x00000003, r5
	cmov 0x0000000A, 0x00000002, r20, r20
	bnz9 .BB.LABEL.3_5
.BB.LABEL.3_2:	; bb39
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 264
	movhi HIGHW1(#_tnum_scheduletable), r0, r5
	ld.w LOWW(#_tnum_scheduletable)[r5], r5
	cmp r5, r22
	cmov 0x00000009, 0x00000003, r20, r20
	bnc9 .BB.LABEL.3_5
.BB.LABEL.3_3:	; bb54
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 266
	mov r22, r5
	shl 0x00000002, r5
	mov #_p_schtblcb_table, r6
	add r5, r6
	ld.w 0x00000000[r6], r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 267
	ld.w 0x00000010[r23], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.3_6
.BB.LABEL.3_4:	; bb54.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movea 0x0000001D, r0, r20
.BB.LABEL.3_5:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 313
	andi 0x000000FF, r20, r10
	dispose 0x00000000, 0x00000679, [r31]
.BB.LABEL.3_6:	; bb74
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 268
	ld.w 0x00000000[r5], r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 270
	ld.w 0x00000008[r24], r5
	ld.w 0x00000000[r5], r6
	cmp r6, r21
	cmov 0x0000000B, 0x00000008, r20, r20
	bh9 .BB.LABEL.3_5
.BB.LABEL.3_7:	; bb99
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 272
	ld.bu 0x00000010[r5], r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.3_9
.BB.LABEL.3_8:	; if_then_bb.i24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.3_9:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 274
	mov r25, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 276
	ld.bu 0x0000001C[r23], r5
	cmp 0x00000001, r5
	cmov 0x0000000A, 0x00000007, r21, r21
	bnz9 .BB.LABEL.3_11
.BB.LABEL.3_10:	; bb126
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 279
	movhi HIGHW1(#_tnum_implscheduletable), r0, r5
	ld.w LOWW(#_tnum_implscheduletable)[r5], r5
	movea 0x00000010, r0, r6
	mov 0xFFFFFFFF, r7
	cmp r5, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 283
	cmov 0x00000009, 0x00000008, r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 286
	mov r24, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 283
	st.b r5, 0x0000001C[r23]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 285
	st.b r7, 0x0000001D[r23]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 286
	mov r21, r7
	jarl _get_abstick, r31
	st.w r10, 0x00000008[r23]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 288
	mov r23, r6
	mov r24, r7
	jarl _insert_cnt_expr_que, r31
	mov 0x00000000, r21
.BB.LABEL.3_11:	; d_exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 291
	mov r25, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.3_13
.BB.LABEL.3_12:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.3_13:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.3_15
.BB.LABEL.3_14:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r20
	mov #_pmr_setting_tbl, r5
	add r20, r20
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r20, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.3_15:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	andi 0x000000FF, r21, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 313
	dispose 0x00000000, 0x00000679, [r31]
	.align 4
_StopScheduleTable:
	.stack _StopScheduleTable = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 323
	prepare 0x00000071, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	cmov 0x0000000A, 0x0000000C, r20, r20
	bnz9 .BB.LABEL.4_5
.BB.LABEL.4_1:	; bb19
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x00000003, r5, r5
	cmp 0x00000003, r5
	cmov 0x0000000A, 0x00000002, r20, r20
	bnz9 .BB.LABEL.4_5
.BB.LABEL.4_2:	; bb38
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 332
	movhi HIGHW1(#_tnum_scheduletable), r0, r5
	ld.w LOWW(#_tnum_scheduletable)[r5], r5
	cmp r5, r6
	cmov 0x00000009, 0x00000003, r20, r20
	bnc9 .BB.LABEL.4_5
.BB.LABEL.4_3:	; bb53
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 334
	shl 0x00000002, r6
	mov #_p_schtblcb_table, r5
	add r6, r5
	ld.w 0x00000000[r5], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 335
	ld.w 0x00000010[r21], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.4_6
.BB.LABEL.4_4:	; bb53.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movea 0x0000001D, r0, r20
.BB.LABEL.4_5:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 392
	andi 0x000000FF, r20, r10
	dispose 0x00000000, 0x00000071, [r31]
.BB.LABEL.4_6:	; bb73
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 337
	ld.w 0x00000000[r5], r5
	ld.w 0x00000008[r5], r5
	ld.bu 0x00000010[r5], r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.4_8
.BB.LABEL.4_7:	; if_then_bb.i24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.4_8:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 339
	mov r22, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 341
	ld.bu 0x0000001C[r21], r5
	cmp 0x00000001, r5
	cmov 0x00000002, 0x00000005, r21, r21
	bz9 .BB.LABEL.4_15
.BB.LABEL.4_9:	; bb105
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 347
	cmp 0x00000002, r5
	bnz9 .BB.LABEL.4_11
.BB.LABEL.4_10:	; if_then_bb113
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 348
	ld.w 0x00000014[r21], r5
	st.w r0, 0x00000018[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 349
	st.w r0, 0x00000014[r21]
	br9 .BB.LABEL.4_14
.BB.LABEL.4_11:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 356
	ld.w 0x00000018[r21], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 357
	cmp 0x00000000, r5
	bz9 .BB.LABEL.4_13
.BB.LABEL.4_12:	; if_then_bb127
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 358
	st.b r6, 0x0000001C[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 359
	st.w r0, 0x00000014[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 360
	st.w r0, 0x00000018[r21]
.BB.LABEL.4_13:	; if_break_bb134
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 364
	ld.w 0x00000010[r21], r5
	mov r21, r6
	ld.w 0x00000000[r5], r7
	jarl _delete_cnt_expr_que, r31
.BB.LABEL.4_14:	; if_break_bb142
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 368
	st.b r5, 0x0000001C[r21]
	mov 0x00000000, r21
.BB.LABEL.4_15:	; d_exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 371
	mov r22, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.4_17
.BB.LABEL.4_16:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.4_17:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.4_19
.BB.LABEL.4_18:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r20
	mov #_pmr_setting_tbl, r5
	add r20, r20
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r20, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.4_19:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	andi 0x000000FF, r21, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 392
	dispose 0x00000000, 0x00000071, [r31]
	.align 4
_NextScheduleTable:
	.stack _NextScheduleTable = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 401
	prepare 0x00000079, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	cmov 0x0000000A, 0x0000000C, r20, r20
	bnz9 .BB.LABEL.5_8
.BB.LABEL.5_1:	; bb20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x00000003, r5, r5
	cmp 0x00000003, r5
	cmov 0x0000000A, 0x00000002, r20, r20
	bnz9 .BB.LABEL.5_8
.BB.LABEL.5_2:	; bb39
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 411
	movhi HIGHW1(#_tnum_scheduletable), r0, r5
	ld.w LOWW(#_tnum_scheduletable)[r5], r5
	cmp r6, r5
	bnh9 .BB.LABEL.5_4
.BB.LABEL.5_3:	; bb39
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	cmp r7, r5
	bh9 .BB.LABEL.5_5
.BB.LABEL.5_4:	; bb39.exit_no_errorhook_crit_edge
	mov 0x00000003, r20
	br9 .BB.LABEL.5_8
.BB.LABEL.5_5:	; bb71
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 413
	movhi HIGHW1(#_tnum_implscheduletable), r0, r5
	ld.w LOWW(#_tnum_implscheduletable)[r5], r5
	cmp r7, r5
	setf 0x0000000B, r8
	cmp r6, r5
	setf 0x0000000B, r5
	xor r5, r8
	bnz9 .BB.LABEL.5_4
.BB.LABEL.5_6:	; bb92
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 417
	shl 0x00000002, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 416
	mov #_p_schtblcb_table, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 417
	add r5, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 416
	shl 0x00000002, r6
	add r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 417
	ld.w 0x00000000[r7], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 416
	ld.w 0x00000000[r5], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 419
	ld.w 0x00000010[r22], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.5_9
.BB.LABEL.5_7:	; bb92.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movea 0x0000001D, r0, r20
.BB.LABEL.5_8:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 469
	andi 0x000000FF, r20, r10
	dispose 0x00000000, 0x00000079, [r31]
.BB.LABEL.5_9:	; bb116
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 420
	ld.w 0x00000010[r21], r6
	cmp 0x00000000, r6
	bz9 .BB.LABEL.5_7
.BB.LABEL.5_10:	; bb133
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 421
	ld.w 0x00000000[r5], r5
	ld.w 0x00000000[r6], r6
	cmp r6, r5
	bnz9 .BB.LABEL.5_4
.BB.LABEL.5_11:	; bb156
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 423
	ld.w 0x00000008[r5], r5
	ld.bu 0x00000010[r5], r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.5_13
.BB.LABEL.5_12:	; if_then_bb.i24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.5_13:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 425
	mov r23, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 428
	ld.bu 0x0000001C[r22], r5
	andi 0x00000003, r5, r0
	cmov 0x0000000A, 0x00000005, r21, r21
	bnz9 .BB.LABEL.5_18
.BB.LABEL.5_14:	; bb189
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 431
	ld.bu 0x0000001C[r21], r5
	cmp 0x00000001, r5
	cmov 0x0000000A, 0x00000007, r21, r21
	bnz9 .BB.LABEL.5_18
.BB.LABEL.5_15:	; bb206
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 437
	ld.w 0x00000018[r22], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.5_17
.BB.LABEL.5_16:	; if_then_bb213
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 438
	st.b r6, 0x0000001C[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 439
	ld.w 0x00000018[r22], r5
	st.w r0, 0x00000014[r5]
.BB.LABEL.5_17:	; if_break_bb222
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 442
	st.w r21, 0x00000018[r22]
	mov 0x00000002, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 443
	st.b r5, 0x0000001C[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 444
	st.w r22, 0x00000014[r21]
	mov 0x00000000, r21
.BB.LABEL.5_18:	; d_exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 447
	mov r23, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.5_20
.BB.LABEL.5_19:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.5_20:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.5_22
.BB.LABEL.5_21:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r20
	mov #_pmr_setting_tbl, r5
	add r20, r20
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r20, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.5_22:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	andi 0x000000FF, r21, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 469
	dispose 0x00000000, 0x00000079, [r31]
	.align 4
_GetScheduleTableStatus:
	.stack _GetScheduleTableStatus = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 479
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r8, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	cmov 0x0000000A, 0x0000000C, r8, r8
	bnz9 .BB.LABEL.6_6
.BB.LABEL.6_1:	; bb18
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r8
	ld.w 0x00000000[r8], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r8], r8
	ori 0x00000003, r8, r8
	cmp 0x00000003, r8
	mov 0x00000002, r8
	bnz9 .BB.LABEL.6_6
.BB.LABEL.6_2:	; bb37
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 488
	movhi HIGHW1(#_tnum_scheduletable), r0, r8
	ld.w LOWW(#_tnum_scheduletable)[r8], r8
	cmp r8, r6
	mov 0x00000003, r8
	bnc9 .BB.LABEL.6_6
.BB.LABEL.6_3:	; bb53
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 489
	cmp 0x00000000, r7
	movea 0x0000001A, r0, r8
	bz9 .BB.LABEL.6_6
.BB.LABEL.6_4:	; bb67
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 491
	shl 0x00000002, r6
	mov #_p_schtblcb_table, r8
	add r6, r8
	ld.w 0x00000000[r8], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 492
	ld.w 0x00000010[r8], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.6_7
.BB.LABEL.6_5:	; bb67.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	movea 0x0000001D, r0, r8
.BB.LABEL.6_6:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 511
	andi 0x000000FF, r8, r10
	jmp [r31]
.BB.LABEL.6_7:	; bb87
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 494
	ld.b 0x0000001C[r8], r8
	mov 0x00000000, r10
	st.b r8, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 496
	jmp [r31]
	.align 4
_schtbl_expire:
	.stack _schtbl_expire = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 521
	prepare 0x00000041, 0x00000004
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 525
	st.w r6, 0x00000000[r3]
	mov r7, r20
	mov r3, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 527
	jarl _schtbl_expiry_process, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 529
	ld.w 0x00000000[r3], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.7_2
.BB.LABEL.7_1:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 532
	dispose 0x00000004, 0x00000041, [r31]
.BB.LABEL.7_2:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 530
	mov r20, r7
	jarl _insert_cnt_expr_que, r31
	dispose 0x00000004, 0x00000041, [r31]
	.align 4
_schtbl_expiry_process:
	.stack _schtbl_expiry_process = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 542
	prepare 0x00000061, 0x00000000
	mov r7, r20
	mov r6, r21
.BB.LABEL.8_1:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 552
	ld.w 0x00000000[r21], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 554
	ld.w 0x00000010[r6], r7
	ld.bu 0x0000001D[r6], r5
	ld.bu 0x00000019[r7], r7
	cmp r7, r5
	bnc9 .BB.LABEL.8_3
.BB.LABEL.8_2:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 556
	mov r20, r7
	jarl _schtbl_exppoint_process, r31
	br9 .BB.LABEL.8_6
.BB.LABEL.8_3:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 558
	cmp r7, r5
	bnz9 .BB.LABEL.8_5
.BB.LABEL.8_4:	; if_then_bb35
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 560
	mov r21, r7
	mov r20, r8
	jarl _schtbl_tail, r31
	br9 .BB.LABEL.8_6
.BB.LABEL.8_5:	; if_else_bb40
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 567
	mov r20, r7
	jarl _schtbl_head, r31
.BB.LABEL.8_6:	; if_break_bb44
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 551
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.8_1
.BB.LABEL.8_7:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 570
	dispose 0x00000000, 0x00000061, [r31]
	.align 4
_schtbl_head:
	.stack _schtbl_head = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 580
	ld.w 0x00000010[r6], r5
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 585
	ld.w 0x00000014[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 587
	ld.w 0x00000000[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.9_4
.BB.LABEL.9_1:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 594
	ld.w 0x00000008[r7], r7
	ld.w 0x00000008[r6], r8
	ld.w 0x00000004[r7], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 217
	mov r7, r9
	sub r8, r9
	add r5, r8
	cmp r5, r9
	bnc9 .BB.LABEL.9_3
.BB.LABEL.9_2:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 226
	add 0xFFFFFFFF, r8
	sub r7, r8
.BB.LABEL.9_3:	; add_tick.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	st.w r8, 0x00000008[r6]
	mov 0x00000000, r10
.BB.LABEL.9_4:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 598
	st.b r0, 0x0000001D[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 601
	jmp [r31]
	.align 4
_schtbl_exppoint_process:
	.stack _schtbl_exppoint_process = 32
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 611
	prepare 0x00000779, 0x00000000
	mov r6, r20
	mov r7, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 621
	ld.w 0x00000010[r20], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 622
	ld.bu 0x0000001D[r20], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 621
	ld.w 0x00000014[r6], r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	shl 0x00000002, r5
	mov #_p_ccb_table, r6
	add r5, r6
	ld.w 0x00000000[r6], r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 626
	mov r24, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 629
	ld.w 0x00000010[r20], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 631
	mov r22, r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 628
	ld.w 0x000000CC[r24], r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 631
	shl 0x00000003, r26
	add r23, r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 629
	ld.w 0x0000001C[r5], r5
	st.w r5, 0x000000CC[r24]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 631
	ld.w 0x00000004[r26], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 633
	st.w r25, 0x000000CC[r24]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 636
	mov r24, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 644
	ld.w 0x00000010[r20], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 641
	add 0x00000001, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 639
	ld.w 0x00000000[r26], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 644
	andi 0x000000FF, r22, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 642
	st.b r22, 0x0000001D[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 644
	ld.bu 0x00000019[r6], r8
	cmp r8, r7
	bnz9 .BB.LABEL.10_6
.BB.LABEL.10_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 646
	ld.w 0x00000004[r6], r7
	cmp r5, r7
	bnz9 .BB.LABEL.10_3
.BB.LABEL.10_2:	; if_break_bb113
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 667
	dispose 0x00000000, 0x00000779, [r31]
.BB.LABEL.10_3:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 655
	ld.w 0x00000008[r21], r21
	sub r5, r7
	ld.w 0x00000008[r20], r23
	ld.w 0x00000004[r21], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 217
	mov r5, r21
	sub r23, r21
	add r7, r23
	cmp r7, r21
	bnc9 .BB.LABEL.10_5
.BB.LABEL.10_4:	; if_else_bb.i7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 226
	add 0xFFFFFFFF, r23
	sub r5, r23
.BB.LABEL.10_5:	; add_tick.1.exit9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	st.w r23, 0x00000008[r20]
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 667
	dispose 0x00000000, 0x00000779, [r31]
.BB.LABEL.10_6:	; if_else_bb90
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 662
	shl 0x00000003, r7
	ld.w 0x00000008[r21], r8
	add r7, r23
	ld.w 0x00000008[r20], r6
	ld.w 0x00000000[r23], r7
	ld.w 0x00000004[r8], r8
	sub r5, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 217
	mov r7, r5
	add r6, r5
	subr r8, r6
	cmp r7, r6
	bnc9 .BB.LABEL.10_8
.BB.LABEL.10_7:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 226
	add 0xFFFFFFFF, r5
	sub r8, r5
.BB.LABEL.10_8:	; add_tick.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	st.w r5, 0x00000008[r20]
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 667
	dispose 0x00000000, 0x00000779, [r31]
	.align 4
_schtbl_tail:
	.stack _schtbl_tail = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 677
	ld.w 0x00000018[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 684
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.11_8
.BB.LABEL.11_1:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 715
	ld.w 0x00000010[r6], r5
	ld.bu 0x00000018[r5], r9
	cmp 0x00000000, r9
	bnz9 .BB.LABEL.11_3
.BB.LABEL.11_2:	; if_else_bb79
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 732
	st.b r5, 0x0000001C[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 733
	st.w r0, 0x00000000[r7]
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 734
	st.w r0, 0x00000014[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 735
	st.w r0, 0x00000018[r6]
	jmp [r31]
.BB.LABEL.11_3:	; if_then_bb46
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 717
	st.b r0, 0x0000001D[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 718
	ld.w 0x00000014[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 720
	ld.w 0x00000000[r5], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.11_5
.BB.LABEL.11_4:	; if_break_bb89
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 740
	jmp [r31]
.BB.LABEL.11_5:	; if_else_bb62
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 726
	ld.w 0x00000008[r8], r8
	ld.w 0x00000008[r6], r7
	ld.w 0x00000004[r8], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 217
	mov r8, r9
	sub r7, r9
	add r5, r7
	cmp r5, r9
	bnc9 .BB.LABEL.11_7
.BB.LABEL.11_6:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 226
	add 0xFFFFFFFF, r7
	sub r8, r7
.BB.LABEL.11_7:	; add_tick.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 0
	st.w r7, 0x00000008[r6]
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 740
	jmp [r31]
.BB.LABEL.11_8:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 694
	ld.b 0x0000001C[r6], r8
	mov 0xFFFFFFFF, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 695
	st.b r9, 0x0000001D[r5]
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 694
	st.b r8, 0x0000001C[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 698
	ld.w 0x00000008[r6], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 699
	st.w r0, 0x00000014[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 698
	st.w r8, 0x00000008[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 702
	st.b r10, 0x0000001C[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 703
	st.w r0, 0x00000018[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 709
	st.w r5, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/scheduletable.c", 711
	jmp [r31]
	.section .const, const
.STR.1:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x61,0x72,0x63,0x68
	.db 0x5C,0x76,0x38,0x35,0x30,0x5F,0x67,0x63,0x63,0x2F,0x70,0x72,0x63,0x5F,0x63,0x6F
	.db 0x6E,0x66,0x69,0x67,0x2E,0x68
	.ds (1)
