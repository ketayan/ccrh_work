#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.04.00 [10 Jun 2016]
#@	Command : ..\prc_insn.c -oDefaultBuild\prc_insn.obj -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -c -g -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\tmp85E0.tmp
#@	compiled at Wed Dec 27 15:23:12 2017

	.file "..\prc_insn.c"

	$reg_mode 32

	.public _func1
	.public _func2
	.public _main

	.section .text, text
	.align 4
_acquire_lock_ldlstc.1:
	.stack _acquire_lock_ldlstc.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/arch/v850_ccrh/prc_insn.h", 216
	._line_top inline_asm
.local aa,bb,cc
aa:
  addi    -8,sp,sp
  mov     sp, ep
  sst.w    r21,4[ep]
  sst.w    zero,0[ep]
  movea    0,ep,r6
  ldl.w [r6], r21
  cmp   r0, r21
  bnz   bb       
  mov   1, r21 
  stc.w r21, [r6]
  cmp   r0, r21 
  be    bb
  mov   1, r10
  br    cc
bb:     
  mov   0, r10
  sld.w    4[ep],r21
  addi    8,ep,sp
cc:
  sld.w    4[ep],r21
  addi    8,ep,sp

	._line_end inline_asm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/arch/v850_ccrh/prc_insn.h", 228
	cmp 0x00000001, r10
	setf 0x00000002, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/arch/v850_ccrh/prc_insn.h", 229
	jmp [r31]
	.align 4
_func1:
	.stack _func1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 3
	mov r6, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 4
	add 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 5
	jmp [r31]
	.align 4
_func2:
	.stack _func2 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 6
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 8
	add 0x00000001, r6
	jarl _func1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 10
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_main:
	.stack _main = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 11
	prepare 0x00000041, 0x00000004
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 13
	st.w r0, 0x00000000[r3]
	mov 0x00000000, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 14
	mov r20, r6
	jarl _func2, r31
	mov r3, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 15
	jarl _acquire_lock_ldlstc.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/arch/v850_ccrh/prc_insn.h", 88
	ei
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/HSBRH850F1H176_BLSM_atk2_rccar_ccrh/OBJ_BLSM/configure/prc_insn.c", 18
	mov r20, r10
	dispose 0x00000004, 0x00000041, [r31]
