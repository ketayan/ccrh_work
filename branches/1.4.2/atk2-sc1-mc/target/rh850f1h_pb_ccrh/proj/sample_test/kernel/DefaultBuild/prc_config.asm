#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\arch\v850_gcc\prc_config.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.public _core_state_table, 8
	.public _TOPPERS_spn_var, 4
	.public _pmr_setting_tbl, 34
	.extern _intbp_table
	.public _prc_hardware_initialize
	.public _prc_initialize
	.public _prc_terminate
	.public _x_config_int
	.extern _internal_shutdownallcores
	.public _default_int_handler
	.extern _target_fput_str
	.public _infinite_loop
	.public _ISRMaintarget_ici_handler0
	.extern _ici_handler_main
	.public _ISRMaintarget_ici_handler1
	.public _target_is_int_controllable

	.section .text, text
	.align 4
_x_core_id.1:
	.stack _x_core_id.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r5
	add 0xFFFFFFFF, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r10
	jmp [r31]
	.align 4
_prc_hardware_initialize:
	.stack _prc_hardware_initialize = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 124
	movhi HIGHW1(#_TOPPERS_spn_var), r0, r5
	st.w r0, LOWW(#_TOPPERS_spn_var)[r5]
	jmp [r31]
	.align 4
_prc_initialize:
	.stack _prc_initialize = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 140
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 246
	jarl _x_core_id.1, r31
	shl 0x00000002, r10
	mov 0x00000001, r6
	mov #_p_ccb_table, r5
	add r10, r5
	movea 0x00000010, r0, r7
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 147
	st.w r6, 0x000000D0[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 152
	st.b r7, 0x000000D4[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 154
	st.w r0, 0x000000D8[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_intbp_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r6
	ld.w 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 162
	ldsr r5, 0x00000004, 0x00000001
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_prc_terminate:
	.stack _prc_terminate = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 138
	ldsr r6, 0x0000000D, 0x00000002
	mov 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 139
	ldsr r6, 0x0000000A, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 140
	ldsr r6, 0x0000000D, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 141
	syncp
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
	movea 0xFFFFFF3F, r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 98
	and r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_core_state_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r6
	st.w r0, 0x00000000[r6]
	jmp [r31]
	.align 4
_x_config_int:
	.stack _x_config_int = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 180
	prepare 0x00000071, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 184
	andi 0x0000FFFF, r6, r20
	movea 0x0000015F, r0, r5
	cmp r5, r20
	mov r8, r21
	mov r7, r22
	bl9 .BB.LABEL.5_2
.BB.LABEL.5_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000B8, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.5_2:	; bb14
	movea 0x0000001F, r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 186
	cmp r5, r20
	bh9 .BB.LABEL.5_4
.BB.LABEL.5_3:	; bb19
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 0
	mov r20, r5
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.5_5
.BB.LABEL.5_4:	; bb24
	mov 0x7FFFFFE0, r6
	add r20, r6
	movea 0xFFFFB040, r0, r5
	add r6, r6
.BB.LABEL.5_5:	; bb31
	add r5, r6
	movea 0x0000001F, r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 312
	cmp r5, r20
	bh9 .BB.LABEL.5_7
.BB.LABEL.5_6:	; bb.i9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 0
	mov r20, r5
	add r5, r5
	mov 0xFFFEEA00, r7
	br9 .BB.LABEL.5_8
.BB.LABEL.5_7:	; bb11.i12
	mov 0x7FFFFFE0, r7
	add r20, r7
	movea 0xFFFFB040, r0, r5
	add r7, r7
.BB.LABEL.5_8:	; bb17.i18
	movea 0x0000015E, r0, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 314
	cmp r8, r20
	bh9 .BB.LABEL.5_10
.BB.LABEL.5_9:	; if_break_bb.i25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 0
	add r5, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.h 0x00000000[r7], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	ori 0x00000080, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r7]
.BB.LABEL.5_10:	; x_disable_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	ld.hu 0x00000000[r6], r5
	movea 0x00000010, r21, r7
	andi 0x0000FFF0, r5, r5
	or r7, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.h 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	ori 0x00000040, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 207
	andi 0x00000001, r22, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 207
	bnz9 .BB.LABEL.5_12
.BB.LABEL.5_11:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 213
	dispose 0x00000000, 0x00000071, [r31]
.BB.LABEL.5_12:	; if_then_bb65
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 0
	movea 0x0000001F, r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 334
	cmp r5, r20
	bh9 .BB.LABEL.5_14
.BB.LABEL.5_13:	; bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 0
	mov r20, r5
	add r5, r5
	mov 0xFFFEEA00, r6
	br9 .BB.LABEL.5_15
.BB.LABEL.5_14:	; bb11.i
	mov 0x7FFFFFE0, r6
	add r20, r6
	movea 0xFFFFB040, r0, r5
	add r6, r6
.BB.LABEL.5_15:	; bb17.i
	movea 0x0000015E, r0, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 336
	cmp r7, r20
	bh9 .BB.LABEL.5_17
.BB.LABEL.5_16:	; if_break_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 0
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 103
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 105
	andi 0x0000FF7F, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 110
	st.h r5, 0x00000000[r6]
.BB.LABEL.5_17:	; x_enable_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 213
	dispose 0x00000000, 0x00000071, [r31]
	.align 4
_default_int_handler:
	.stack _default_int_handler = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 220
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 222
	mov #.STR.1548, r6
	jarl _target_fput_str, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 223
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1549, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000DF, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	dispose 0x00000000, 0x00000001
	movea 0x0000001B, r0, r6
	jr _internal_shutdownallcores
	.align 4
_infinite_loop:
	.stack _infinite_loop = 0
.BB.LABEL.7_1:	; bb1
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 234
	br9 .BB.LABEL.7_1
	.align 4
_ISRMaintarget_ici_handler0:
	.stack _ISRMaintarget_ici_handler0 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 243
	jr _ici_handler_main
	.align 4
_ISRMaintarget_ici_handler1:
	.stack _ISRMaintarget_ici_handler1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 248
	jr _ici_handler_main
	.align 4
_target_is_int_controllable:
	.stack _target_is_int_controllable = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 268
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.c", 275
	jmp [r31]
	.section .bss, bss
	.align 4
_core_state_table:
	.ds (8)
	.align 4
_TOPPERS_spn_var:
	.ds (4)
	.section .const, const
	.align 2
_pmr_setting_tbl:
	.dhw 0xFFFF,0xFFFE,0xFFFC,0xFFF8,0xFFF0,0xFFE0,0xFFC0,0xFF80,0xFF00,0xFE00,0xFC00,0xF800
	.dhw 0xF000,0xE000,0xC000,0x8000
	.ds (2)
.STR.1:
	.db 0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x61,0x72,0x63,0x68,0x5C,0x76,0x38
	.db 0x35,0x30,0x5F,0x67,0x63,0x63,0x5C,0x70,0x72,0x63,0x5F,0x63,0x6F,0x6E,0x66,0x69
	.db 0x67,0x2E,0x63
	.ds (1)
.STR.1548:
	.db 0x55,0x6E,0x72,0x65,0x67,0x69,0x73,0x74,0x65,0x72,0x65,0x64,0x20,0x49,0x6E,0x74
	.db 0x65,0x72,0x72,0x75,0x70,0x74,0x20,0x6F,0x63,0x63,0x75,0x72,0x73,0x2E
	.ds (1)
.STR.1549:
	.db 0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x61,0x72,0x63,0x68,0x5C,0x76,0x38
	.db 0x35,0x30,0x5F,0x67,0x63,0x63,0x5C,0x70,0x72,0x63,0x5F,0x63,0x6F,0x6E,0x66,0x69
	.db 0x67,0x2E,0x63
	.ds (1)
