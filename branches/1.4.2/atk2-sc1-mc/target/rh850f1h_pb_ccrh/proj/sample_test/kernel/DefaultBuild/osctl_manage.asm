#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\kernel\osctl_manage.c"

	$reg_mode 32

	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _pmr_setting_tbl
	.extern _tnum_appmode
	.public _appmodeid, 4
	.public _fatal_file_name, 4
	.public _fatal_line_num, 4
	.extern _v850_memory_changed
	.extern _internal_shutdownallcores
	.public _StartOS
	.extern _p_inib_initialize
	.extern _barrier_sync
	.extern _ccb_initialize
	.extern _target_initialize
	.extern _object_initialize
	.extern _StartupHook
	.extern _start_dispatch
	.public _GetActiveApplicationMode
	.public _GetFaultyContext

	.section .text, text
	.align 4
_x_core_id.1:
	.stack _x_core_id.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r5
	add 0xFFFFFFFF, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r10
	jmp [r31]
	.align 4
_StartOS:
	.stack _StartOS = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 115
	prepare 0x00000079, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r5
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 133
	andi 0x000000FF, r5, r5
	cmp 0x00000001, r5
	bgt17 .BB.LABEL.2_26
.BB.LABEL.2_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 134
	ld.bu 0x00000001[r21], r7
	cmp 0x00000000, r7
	bnz17 .BB.LABEL.2_26
.BB.LABEL.2_2:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 163
	mov #_os_controlled.2.StartOS, r7
	add r5, r7
	mov 0x00000001, r8
	st.b r8, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 166
	shl 0x00000002, r5
	mov #_appmodeid_table.1.StartOS, r7
	add r5, r7
	st.w r6, 0x00000000[r7]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 491
	jarl _x_core_id.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 492
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.2_4
.BB.LABEL.2_3:	; if_then_bb31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 169
	jarl _p_inib_initialize, r31
.BB.LABEL.2_4:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 0
	mov 0x00000001, r22
	mov 0x00000000, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 171
	mov r22, r6
	jarl _barrier_sync, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 175
	movhi HIGHW1(#_os_controlled.2.StartOS), r0, r5
	ld.bu LOWW(#_os_controlled.2.StartOS)[r5], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_6
.BB.LABEL.2_5:	; if_break_bb.bb.split.ul_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 0
	mov 0xFFFFFFFF, r5
	br9 .BB.LABEL.2_7
.BB.LABEL.2_6:	; bb39
	movhi HIGHW1(#_appmodeid_table.1.StartOS), r0, r5
	ld.w LOWW(#_appmodeid_table.1.StartOS)[r5], r5
	cmp 0xFFFFFFFF, r5
	bz9 .BB.LABEL.2_5
.BB.LABEL.2_7:	; bb.split.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 175
	movhi HIGHW1(#_os_controlled.2.StartOS+0x00000001), r0, r6
	ld.bu LOWW(#_os_controlled.2.StartOS+0x00000001)[r6], r6
	cmp 0x00000000, r6
	bz9 .BB.LABEL.2_12
.BB.LABEL.2_8:	; bb39.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 0
	movhi HIGHW1(#_appmodeid_table.1.StartOS+0x00000004), r0, r6
	ld.w LOWW(#_appmodeid_table.1.StartOS+0x00000004)[r6], r6
	cmp 0xFFFFFFFF, r6
	bz9 .BB.LABEL.2_12
.BB.LABEL.2_9:	; if_then_bb53.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 177
	cmp r6, r5
	bz9 .BB.LABEL.2_11
.BB.LABEL.2_10:	; if_then_bb53.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 0
	cmp 0xFFFFFFFF, r5
	bnz9 .BB.LABEL.2_14
.BB.LABEL.2_11:	; if_then_bb53.ul.bb92_crit_edge
	mov r6, r5
.BB.LABEL.2_12:	; bb92
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 185
	cmp 0xFFFFFFFF, r5
	bnz9 .BB.LABEL.2_15
.BB.LABEL.2_13:	; bb99
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 186
	br9 .BB.LABEL.2_13
.BB.LABEL.2_14:	; bb75
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 178
	br9 .BB.LABEL.2_14
.BB.LABEL.2_15:	; if_break_bb101
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 189
	movhi HIGHW1(#_appmodeid), r0, r23
	st.w r5, LOWW(#_appmodeid)[r23]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 194
	jarl _ccb_initialize, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 197
	jarl _target_initialize, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 200
	jarl _object_initialize, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 202
	st.h r0, 0x00000004[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 211
	movhi HIGHW1(#_tnum_appmode), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 205
	st.b r22, 0x00000001[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 211
	ld.w LOWW(#_appmodeid)[r23], r5
	ld.w LOWW(#_tnum_appmode)[r6], r6
	cmp r6, r5
	bl9 .BB.LABEL.2_19
.BB.LABEL.2_16:	; if_then_bb112
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.2_18
.BB.LABEL.2_17:	; if_then_bb.i78
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.2_18:	; x_nested_lock_os_int.1.exit81
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000018, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 218
	jarl _internal_shutdownallcores, r31
.BB.LABEL.2_19:	; if_break_bb113
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 0
	mov 0x00000000, r7
	mov 0x00000002, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 221
	jarl _barrier_sync, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.2_21
.BB.LABEL.2_20:	; if_then_bb.i63
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.2_21:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	set1 0x00000004, 0x00000004[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 233
	jarl _StartupHook, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	clr1 0x00000004, 0x00000004[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r21], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_23
.BB.LABEL.2_22:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.2_23:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r21], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r21]
	bnz9 .BB.LABEL.2_25
.BB.LABEL.2_24:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r21], r5
	mov #_pmr_setting_tbl, r6
	add r5, r5
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.2_25:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov 0x00000001, r7
	andi 0x00000003, r5, r5
	mov 0x00000003, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	set1 0x00000000, 0x00000004[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 250
	jarl _barrier_sync, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 252
	jarl _start_dispatch, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 253
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1521, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000FD, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	dispose 0x00000000, 0x00000079
	movea 0x0000001B, r0, r6
	jr _internal_shutdownallcores
.BB.LABEL.2_26:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 258
	dispose 0x00000000, 0x00000079, [r31]
	.align 4
_GetActiveApplicationMode:
	.stack _GetActiveApplicationMode = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r6, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	bnz9 .BB.LABEL.3_3
.BB.LABEL.3_1:	; bb16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r6
	ld.w 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x0000007B, r5, r5
	addi 0xFFFFFF85, r5, r0
	bnz9 .BB.LABEL.3_3
.BB.LABEL.3_2:	; bb34
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 279
	movhi HIGHW1(#_appmodeid), r0, r5
	ld.w LOWW(#_appmodeid)[r5], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 281
	jmp [r31]
.BB.LABEL.3_3:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 0
	mov 0xFFFFFFFF, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 298
	jmp [r31]
	.align 4
_GetFaultyContext:
	.stack _GetFaultyContext = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 309
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl_manage.c", 344
	jmp [r31]
	.section .bss, bss
	.align 4
_appmodeid:
	.ds (4)
	.align 4
_appmodeid_table.1.StartOS:
	.ds (8)
	.section .data, data
	.align 4
_fatal_file_name:
	.ds (4)
	.align 4
_fatal_line_num:
	.ds (4)
_os_controlled.2.StartOS:
	.ds (2)
	.section .const, const
.STR.1:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x61,0x72,0x63,0x68
	.db 0x5C,0x76,0x38,0x35,0x30,0x5F,0x67,0x63,0x63,0x2F,0x70,0x72,0x63,0x5F,0x63,0x6F
	.db 0x6E,0x66,0x69,0x67,0x2E,0x68
	.ds (1)
.STR.1521:
	.db 0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x6B,0x65,0x72,0x6E,0x65,0x6C,0x5C
	.db 0x6F,0x73,0x63,0x74,0x6C,0x5F,0x6D,0x61,0x6E,0x61,0x67,0x65,0x2E,0x63
	.ds (1)
