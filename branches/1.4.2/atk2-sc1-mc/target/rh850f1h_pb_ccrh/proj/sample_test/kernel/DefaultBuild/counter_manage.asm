#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\kernel\counter_manage.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _pmr_setting_tbl
	.extern _tnum_hardcounter
	.extern _tnum_counter
	.extern _p_cntcb_table
	.extern _hwcntinib_table
	.extern _v850_memory_changed
	.extern _internal_shutdownallcores
	.public _IncrementCounter
	.public _GetCounterValue
	.extern _acquire_cnt_lock
	.extern _release_cnt_lock
	.public _GetElapsedValue
	.public _notify_hardware_counter
	.public _incr_counter_action
	.extern _expire_process

	.section .text, text
	.align 4
_IncrementCounter:
	.stack _IncrementCounter = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 103
	prepare 0x00000061, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r6, r21
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	cmov 0x0000000A, 0x0000000C, r20, r20
	bnz9 .BB.LABEL.1_7
.BB.LABEL.1_1:	; bb17
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x00000003, r5, r5
	cmp 0x00000003, r5
	cmov 0x0000000A, 0x00000002, r20, r20
	bnz9 .BB.LABEL.1_7
.BB.LABEL.1_2:	; bb36
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 110
	movhi HIGHW1(#_tnum_counter), r0, r5
	ld.w LOWW(#_tnum_counter)[r5], r5
	cmp r5, r21
	bl9 .BB.LABEL.1_4
.BB.LABEL.1_3:	; bb36.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	mov 0x00000003, r20
	br9 .BB.LABEL.1_7
.BB.LABEL.1_4:	; bb52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 111
	movhi HIGHW1(#_tnum_hardcounter), r0, r5
	ld.w LOWW(#_tnum_hardcounter)[r5], r5
	cmp r5, r21
	bl9 .BB.LABEL.1_3
.BB.LABEL.1_5:	; bb68
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 112
	mov r21, r5
	shl 0x00000002, r5
	mov #_p_cntcb_table, r6
	add r5, r6
	ld.w 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 113
	ld.w 0x00000008[r5], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.1_8
.BB.LABEL.1_6:	; bb68.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	movea 0x0000001D, r0, r20
.BB.LABEL.1_7:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 138
	andi 0x000000FF, r20, r10
	dispose 0x00000000, 0x00000061, [r31]
.BB.LABEL.1_8:	; bb89
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 114
	ld.bu 0x00000010[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r6, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r6, r6
	add 0xFFFFFFFF, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r6, r6
	cmp r5, r6
	bnz9 .BB.LABEL.1_6
.BB.LABEL.1_9:	; bb110
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.1_11
.BB.LABEL.1_10:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.1_11:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 122
	mov r21, r6
	jarl _incr_counter_action, r31
	mov r10, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.1_13
.BB.LABEL.1_12:	; if_then_bb.i24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.1_13:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.1_15
.BB.LABEL.1_14:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r20
	mov #_pmr_setting_tbl, r5
	add r20, r20
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r20, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.1_15:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	mov r21, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 138
	dispose 0x00000000, 0x00000061, [r31]
	.align 4
_GetCounterValue:
	.stack _GetCounterValue = 24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 148
	prepare 0x00000479, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r7, r21
	andi 0x00000003, r5, r5
	mov r6, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	cmov 0x0000000A, 0x0000000C, r20, r20
	bnz9 .BB.LABEL.2_7
.BB.LABEL.2_1:	; bb20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x00000003, r5, r5
	cmp 0x00000003, r5
	cmov 0x0000000A, 0x00000002, r20, r20
	bnz9 .BB.LABEL.2_7
.BB.LABEL.2_2:	; bb39
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 159
	movhi HIGHW1(#_tnum_counter), r0, r5
	ld.w LOWW(#_tnum_counter)[r5], r5
	cmp r5, r22
	cmov 0x00000009, 0x00000003, r20, r20
	bnc9 .BB.LABEL.2_7
.BB.LABEL.2_3:	; bb55
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 160
	cmp 0x00000000, r21
	bnz9 .BB.LABEL.2_5
.BB.LABEL.2_4:	; bb55.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	movea 0x0000001A, r0, r20
	br9 .BB.LABEL.2_7
.BB.LABEL.2_5:	; bb69
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 161
	mov r22, r5
	shl 0x00000002, r5
	mov #_p_cntcb_table, r6
	add r5, r6
	ld.w 0x00000000[r6], r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 162
	ld.w 0x00000008[r23], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_8
.BB.LABEL.2_6:	; bb69.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	movea 0x0000001D, r0, r20
.BB.LABEL.2_7:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 198
	andi 0x000000FF, r20, r10
	dispose 0x00000000, 0x00000479, [r31]
.BB.LABEL.2_8:	; bb89
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 163
	ld.bu 0x00000010[r5], r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.2_10
.BB.LABEL.2_9:	; if_then_bb.i29
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.2_10:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 173
	mov r24, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	movhi HIGHW1(#_tnum_hardcounter), r0, r5
	ld.w LOWW(#_tnum_hardcounter)[r5], r5
	cmp r5, r22
	bnc9 .BB.LABEL.2_12
.BB.LABEL.2_11:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 265
	mul 0x00000030, r22, r0
	mov #_hwcntinib_table, r5
	add r22, r5
	ld.w 0x00000010[r5], r5
	jarl [r5], r31
	br9 .BB.LABEL.2_13
.BB.LABEL.2_12:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 268
	ld.w 0x0000000C[r23], r10
.BB.LABEL.2_13:	; get_curval.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	mov r10, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 175
	mov r24, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.2_15
.BB.LABEL.2_14:	; if_then_bb.i13
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.2_15:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.2_17
.BB.LABEL.2_16:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r20
	mov #_pmr_setting_tbl, r5
	add r20, r20
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r20, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.2_17:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 178
	ld.w 0x00000008[r23], r20
	ld.w 0x00000000[r20], r20
	cmp r20, r22
	bnh9 .BB.LABEL.2_19
.BB.LABEL.2_18:	; if_then_bb112
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 179
	add 0xFFFFFFFF, r22
	sub r20, r22
.BB.LABEL.2_19:	; if_break_bb121
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 181
	st.w r22, 0x00000000[r21]
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 183
	dispose 0x00000000, 0x00000479, [r31]
	.align 4
_GetElapsedValue:
	.stack _GetElapsedValue = 28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 208
	prepare 0x00000679, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r8, r21
	andi 0x00000003, r5, r5
	mov r7, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	mov r6, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	mov #_p_ccb_table, r20
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	andi 0x00007000, r5, r0
	cmov 0x0000000A, 0x0000000C, r20, r20
	bnz9 .BB.LABEL.3_8
.BB.LABEL.3_1:	; bb21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	ld.hu 0x00000004[r5], r5
	ori 0x00000003, r5, r5
	cmp 0x00000003, r5
	cmov 0x0000000A, 0x00000002, r20, r20
	bnz9 .BB.LABEL.3_8
.BB.LABEL.3_2:	; bb40
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 218
	movhi HIGHW1(#_tnum_counter), r0, r5
	ld.w LOWW(#_tnum_counter)[r5], r5
	cmp r5, r23
	cmov 0x00000009, 0x00000003, r20, r20
	bnc9 .BB.LABEL.3_8
.BB.LABEL.3_3:	; bb56
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 219
	cmp 0x00000000, r21
	bz9 .BB.LABEL.3_5
.BB.LABEL.3_4:	; bb56
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	cmp 0x00000000, r22
	bnz9 .BB.LABEL.3_6
.BB.LABEL.3_5:	; bb56.exit_no_errorhook_crit_edge
	movea 0x0000001A, r0, r20
	br9 .BB.LABEL.3_8
.BB.LABEL.3_6:	; bb85
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 221
	mov r23, r5
	shl 0x00000002, r5
	mov #_p_cntcb_table, r6
	add r5, r6
	ld.w 0x00000000[r6], r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 222
	ld.w 0x00000008[r24], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.3_9
.BB.LABEL.3_7:	; bb85.exit_no_errorhook_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	movea 0x0000001D, r0, r20
.BB.LABEL.3_8:	; exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 259
	andi 0x000000FF, r20, r10
	dispose 0x00000000, 0x00000679, [r31]
.BB.LABEL.3_9:	; bb106
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 224
	ld.w 0x00000000[r22], r6
	ld.w 0x00000000[r5], r7
	cmp r7, r6
	cmov 0x0000000B, 0x00000008, r20, r20
	bh9 .BB.LABEL.3_8
.BB.LABEL.3_10:	; bb126
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 225
	ld.bu 0x00000010[r5], r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.3_12
.BB.LABEL.3_11:	; if_then_bb.i32
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.3_12:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 232
	mov r25, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	movhi HIGHW1(#_tnum_hardcounter), r0, r5
	ld.w LOWW(#_tnum_hardcounter)[r5], r5
	cmp r5, r23
	bnc9 .BB.LABEL.3_14
.BB.LABEL.3_13:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 265
	mul 0x00000030, r23, r0
	mov #_hwcntinib_table, r5
	add r23, r5
	ld.w 0x00000010[r5], r5
	jarl [r5], r31
	br9 .BB.LABEL.3_15
.BB.LABEL.3_14:	; if_else_bb.i11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 268
	ld.w 0x0000000C[r24], r10
.BB.LABEL.3_15:	; get_curval.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	mov r10, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 234
	mov r25, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.3_17
.BB.LABEL.3_16:	; if_then_bb.i16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.3_17:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.3_19
.BB.LABEL.3_18:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r20
	mov #_pmr_setting_tbl, r5
	add r20, r20
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r20, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.3_19:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 237
	ld.w 0x00000008[r24], r20
	ld.w 0x00000000[r20], r20
	cmp r20, r23
	bnh9 .BB.LABEL.3_21
.BB.LABEL.3_20:	; if_then_bb149
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 238
	add 0xFFFFFFFF, r23
	sub r20, r23
.BB.LABEL.3_21:	; if_break_bb158
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 240
	ld.w 0x00000000[r22], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 239
	mov r23, r6
	sub r5, r6
	cmp r5, r23
	bnc9 .BB.LABEL.3_23
.BB.LABEL.3_22:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 248
	add r20, r6
	add 0x00000001, r6
.BB.LABEL.3_23:	; diff_tick.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	st.w r6, 0x00000000[r21]
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 241
	st.w r23, 0x00000000[r22]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 243
	dispose 0x00000000, 0x00000679, [r31]
	.align 4
_notify_hardware_counter:
	.stack _notify_hardware_counter = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 271
	mov r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 276
	shl 0x00000002, r5
	mov #_p_cntcb_table, r7
	prepare 0x00000079, 0x00000000
	add r5, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 277
	mov #_p_ccb_table, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 276
	ld.w 0x00000000[r7], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 277
	ld.w 0x00000008[r20], r5
	ld.bu 0x00000010[r5], r5
	shl 0x00000002, r5
	add r21, r5
	ld.w 0x00000000[r5], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r6, r23
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r21, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.4_2
.BB.LABEL.4_1:	; if_then_bb.i14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.4_2:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 286
	mov r23, r5
	mul 0x00000030, r5, r0
	mov #_hwcntinib_table, r6
	add r5, r6
	ld.w 0x0000001C[r6], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 288
	mov r22, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 289
	mov r20, r6
	mov r23, r7
	jarl _expire_process, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 290
	mov r22, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r21
	ld.w 0x00000000[r21], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.4_4
.BB.LABEL.4_3:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.4_4:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.4_6
.BB.LABEL.4_5:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r5
	mov #_pmr_setting_tbl, r6
	add r5, r5
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.4_6:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 293
	dispose 0x00000000, 0x00000079, [r31]
	.align 4
_incr_counter_action:
	.stack _incr_counter_action = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 305
	mov r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 313
	shl 0x00000002, r5
	mov #_p_cntcb_table, r7
	prepare 0x00000071, 0x00000000
	add r5, r7
	mov r6, r21
	ld.w 0x00000000[r7], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 314
	mov #_p_ccb_table, r5
	ld.w 0x00000008[r20], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 320
	ld.bu 0x00000010[r20], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 314
	ld.bu 0x00000010[r6], r6
	shl 0x00000002, r6
	add r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 320
	cmp 0x00000000, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 314
	ld.w 0x00000000[r5], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 320
	bnz9 .BB.LABEL.5_2
.BB.LABEL.5_1:	; bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 321
	mov r22, r6
	jarl _acquire_cnt_lock, r31
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 323
	st.b r5, 0x00000010[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 325
	ld.w 0x00000008[r20], r5
	ld.w 0x0000000C[r20], r6
	ld.w 0x00000004[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 226
	mov r5, r7
	subr r0, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 217
	cmp r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 226
	cmov 0x0000000A, 0x00000001, r7, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 329
	mov r21, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 226
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 327
	st.w r6, 0x0000000C[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 329
	mov r20, r6
	jarl _expire_process, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 331
	st.b r0, 0x00000010[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 332
	mov r22, r6
	jarl _release_cnt_lock, r31
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 334
	dispose 0x00000000, 0x00000071, [r31]
.BB.LABEL.5_2:	; d_exit_no_errorhook
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 0
	mov 0x00000007, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter_manage.c", 346
	dispose 0x00000000, 0x00000071, [r31]
	.section .const, const
.STR.1:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x61,0x72,0x63,0x68
	.db 0x5C,0x76,0x38,0x35,0x30,0x5F,0x67,0x63,0x63,0x2F,0x70,0x72,0x63,0x5F,0x63,0x6F
	.db 0x6E,0x66,0x69,0x67,0x2E,0x68
	.ds (1)
