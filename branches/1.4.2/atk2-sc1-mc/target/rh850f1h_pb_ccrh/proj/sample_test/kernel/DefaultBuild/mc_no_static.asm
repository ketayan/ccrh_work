#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\mc.c -oDefaultBuild\mc.obj -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_xbzdmyis.oid
#@	compiled at Wed Aug 08 10:26:40 2018

	.file "..\..\..\kernel\mc.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _pmr_setting_tbl
	.extern _tnum_ici_of_core
	.extern _p_iciinb_table
	.extern _p_isrcb_table
	.public _ioc_lock, 4
	.public _shutdown_reqflg, 1
	.public _activated_cores, 4
	.extern _v850_memory_changed
	.public _ccb_initialize
	.public _ici_handler_main
	.extern _exit_isr2
	.public _dispatch_request
	.public _shutdown_request
	.public _barrier_sync
	.public _internal_shutdownallcores
	.public _acquire_tsk_lock
	.public _release_tsk_lock
	.extern _stack_change_and_call_func_1
	.extern _internal_call_shtdwnhk
	.extern _object_terminate
	.extern _target_exit
	.public _release_tsk_lock_and_dispatch
	.extern _dispatch
	.public _acquire_cnt_lock
	.public _release_cnt_lock
	.public _acquire_spn_lock
	.public _release_spn_lock
	.public _try_spn_lock
	.public _acquire_ioc_lock
	.public _release_ioc_lock

	.section .text, text
	.align 4
_x_acquire_lock.1:
	.stack _x_acquire_lock.1 = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 600
	prepare 0x00000071, 0x00000000
	mov r6, r20
.BB.LABEL.1_1:	; entry
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	._line_top inline_asm
.local aa,bb,cc
aa:
  addi    -8,sp,sp
  mov     sp, ep
  sst.w    r21,4[ep]
  sst.w    zero,0[ep]
  movea    0,ep,r6
  ldl.w [r6], r21
  cmp   r0, r21
  bnz   bb       
  mov   1, r21 
  stc.w r21, [r6]
  cmp   r0, r21 
  be    bb
  mov   1, r10
  br    cc
bb:     
  mov   0, r10
  sld.w    4[ep],r21
  addi    8,ep,sp
cc:
  sld.w    4[ep],r21
  addi    8,ep,sp

	._line_end inline_asm
	cmp 0x00000001, r10
	bz9 .BB.LABEL.1_9
.BB.LABEL.1_2:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r21, r5
	ld.w 0x00000000[r5], r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r22], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.1_4
.BB.LABEL.1_3:	; if_then_bb.i13
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.1_4:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r22], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r22]
	bnz9 .BB.LABEL.1_6
.BB.LABEL.1_5:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r22], r5
	mov #_pmr_setting_tbl, r6
	add r5, r5
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.1_6:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r21
	ld.w 0x00000000[r21], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.1_8
.BB.LABEL.1_7:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.1_8:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 226
	mov r20, r6
	jr .BB.LABEL.1_1
.BB.LABEL.1_9:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 605
	dispose 0x00000000, 0x00000071
	jr _v850_memory_changed
	.align 4
_x_release_lock.1:
	.stack _x_release_lock.1 = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 619
	prepare 0x00000041, 0x00000000
	mov r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 621
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 241
	st.w r0, 0x00000000[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 243
	._line_top inline_asm
  snooze
	._line_end inline_asm
	dispose 0x00000000, 0x00000041, [r31]
	.align 4
_ccb_initialize:
	.stack _ccb_initialize = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r6
	ld.w 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r6, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 559
	st.w r0, 0x00000008[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 568
	st.w r0, 0x0000000C[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	add 0xFFFFFFFF, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 132
	st.b r6, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r6, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x00000003, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 492
	cmp 0x00000001, r6
	bnz9 .BB.LABEL.3_2
.BB.LABEL.3_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 586
	movhi HIGHW1(#_ioc_lock), r0, r6
	st.w r0, LOWW(#_ioc_lock)[r6]
.BB.LABEL.3_2:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x0000000F, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 170
	st.w r6, 0x00000018[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 169
	st.w r0, 0x00000014[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 172
	movea 0x00000030, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 169
	st.w r0, 0x00000010[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 172
	movea 0x0000001C, r5, r7
	mov 0x00000004, r8
.BB.LABEL.3_3:	; bb.split.clone
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r7, 0xFFFFFFF0[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 173
	addi 0x00000008, r7, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r7, 0xFFFFFFEC[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r9, 0xFFFFFFF8[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r9, 0xFFFFFFF4[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 173
	movea 0x00000010, r7, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r9, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r9, 0xFFFFFFFC[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 173
	movea 0x00000018, r7, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r9, 0x00000008[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 172
	movea 0x00000020, r7, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r9, 0x00000004[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 172
	movea 0x00000020, r6, r6
	loop r8, .BB.LABEL.3_3
.BB.LABEL.3_4:	; bb35
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 176
	st.h r0, 0x0000009C[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 181
	st.w r0, 0x000000A0[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 183
	st.b r0, 0x00000002[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 184
	st.b r0, 0x000000A4[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 185
	st.b r0, 0x00000003[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 186
	st.b r0, 0x000000A5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r6, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x00000003, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 492
	cmp 0x00000001, r6
	bnz9 .BB.LABEL.3_6
.BB.LABEL.3_5:	; if_then_bb53
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 190
	movhi HIGHW1(#_shutdown_reqflg), r0, r6
	st.b r0, LOWW(#_shutdown_reqflg)[r6]
.BB.LABEL.3_6:	; if_break_bb54
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 197
	st.w r0, 0x000000C0[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 202
	st.b r0, 0x000000C8[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 207
	st.w r0, 0x000000CC[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 212
	st.w r0, 0x000000DC[r5]
	jmp [r31]
	.align 4
_ici_handler_main:
	.stack _ici_handler_main = 80
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 239
	prepare 0x00000FFF, 0x00000020
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 257
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.4_2
.BB.LABEL.4_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 258
	jarl _internal_shutdownallcores, r31
.BB.LABEL.4_2:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.4_4
.BB.LABEL.4_3:	; if_then_bb.i133
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.4_4:	; x_nested_lock_os_int.1.exit136
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 520
	addi 0x00000008, r21, r22
	mov r22, r6
	jarl _x_acquire_lock.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 265
	ld.w 0x000000C4[r21], r5
	ld.w 0x000000C0[r21], r6
	mov r5, r7
	and r6, r7
	bz17 .BB.LABEL.4_33
.BB.LABEL.4_5:	; bb.nph
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000001, r23
	mov 0x00000002, r24
	mov 0x00000003, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 279
	mov #_p_iciinb_table, r26
.BB.LABEL.4_6:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 269
	andi 0x0000FFFF, r7, r8
	bnz9 .BB.LABEL.4_17
.BB.LABEL.4_7:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000011[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 273
	mov r7, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r23, 0x00000012[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 273
	shr 0x00000010, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000013[r3]
	st.b r24, 0x00000014[r3]
	st.b r0, 0x00000015[r3]
	st.b r23, 0x00000016[r3]
	st.b r0, 0x00000017[r3]
	st.b r25, 0x00000018[r3]
	st.b r0, 0x00000019[r3]
	st.b r23, 0x0000001A[r3]
	st.b r0, 0x0000001B[r3]
	st.b r24, 0x0000001C[r3]
	st.b r0, 0x0000001D[r3]
	st.b r23, 0x0000001E[r3]
	st.b r0, 0x0000001F[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 404
	bz9 .BB.LABEL.4_13
.BB.LABEL.4_8:	; bb27.i42
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 405
	andi 0x000000FF, r8, r0
	mov 0x00000000, r9
	bnz9 .BB.LABEL.4_10
.BB.LABEL.4_9:	; if_then_bb34.i44
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 406
	shr 0x00000018, r7
	mov 0x00000008, r9
	mov r7, r8
.BB.LABEL.4_10:	; if_break_bb43.i50
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 409
	andi 0x0000000F, r8, r0
	bnz9 .BB.LABEL.4_12
.BB.LABEL.4_11:	; if_then_bb50.i53
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 411
	add 0x00000004, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	shr 0x00000004, r8
.BB.LABEL.4_12:	; bitmap_search.1.exit63
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 413
	andi 0x0000000F, r8, r7
	movea 0x00000011, r3, r8
	add r8, r7
	ld.bu 0xFFFFFFFF[r7], r7
	add r9, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 414
	andi 0x0000FFFF, r7, r7
	movea 0x00000010, r7, r27
	br9 .BB.LABEL.4_22
.BB.LABEL.4_13:	; if_then_bb.i30
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1726, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x00000194, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.4_15
.BB.LABEL.4_14:	; if_then_bb.i.i.i34
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.4_15:	; x_nested_lock_os_int.1.exit.i.i37
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 477
	jarl _shutdown_request, r31
	movea 0x0000001B, r0, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 480
	mov #_internal_call_shtdwnhk, r6
	jarl _stack_change_and_call_func_1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 484
	jarl _object_terminate, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 490
	jarl _target_exit, r31
.BB.LABEL.4_16:	; bb3.i.i38
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 496
	br9 .BB.LABEL.4_16
.BB.LABEL.4_17:	; if_then_bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r0, 0x00000002[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 405
	andi 0x000000FF, r7, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	st.b r23, 0x00000003[r3]
	mov 0x00000000, r27
	st.b r0, 0x00000004[r3]
	st.b r24, 0x00000005[r3]
	st.b r0, 0x00000006[r3]
	st.b r23, 0x00000007[r3]
	st.b r0, 0x00000008[r3]
	st.b r25, 0x00000009[r3]
	st.b r0, 0x0000000A[r3]
	st.b r23, 0x0000000B[r3]
	st.b r0, 0x0000000C[r3]
	st.b r24, 0x0000000D[r3]
	st.b r0, 0x0000000E[r3]
	st.b r23, 0x0000000F[r3]
	st.b r0, 0x00000010[r3]
	bnz9 .BB.LABEL.4_19
.BB.LABEL.4_18:	; if_then_bb34.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 406
	shr 0x00000008, r8
	mov 0x00000008, r27
	mov r8, r7
.BB.LABEL.4_19:	; if_break_bb43.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 409
	andi 0x0000000F, r7, r0
	bnz9 .BB.LABEL.4_21
.BB.LABEL.4_20:	; if_then_bb50.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	andi 0x0000FFFF, r7, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 411
	add 0x00000004, r27
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	shr 0x00000004, r7
.BB.LABEL.4_21:	; bitmap_search.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 413
	andi 0x0000000F, r7, r7
	movea 0x00000002, r3, r8
	add r8, r7
	ld.bu 0xFFFFFFFF[r7], r7
	add r27, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 414
	andi 0x0000FFFF, r7, r27
.BB.LABEL.4_22:	; if_break_bb40
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 277
	ld.bu 0x00000000[r21], r7
	mov #_tnum_ici_of_core, r8
	add r7, r8
	ld.bu 0x00000000[r8], r8
	cmp r8, r27
	bnc17 .BB.LABEL.4_31
.BB.LABEL.4_23:	; if_then_bb52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 279
	shl 0x00000002, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 280
	mov r27, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 279
	add r26, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 280
	mul 0x0000000C, r8, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 279
	ld.w 0x00000000[r7], r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 280
	add r8, r9
	ld.w 0x00000004[r9], r8
	tst r5, r8
	bz17 .BB.LABEL.4_32
.BB.LABEL.4_24:	; if_then_bb71
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 282
	not r8, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 289
	mov r27, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 282
	and r8, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 289
	mul 0x0000000C, r5, r0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 282
	st.w r6, 0x000000C0[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 289
	ld.w 0x00000000[r7], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 288
	ld.w 0x000000A0[r21], r28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 289
	add r5, r6
	ld.w 0x00000008[r6], r5
	mov #_p_isrcb_table, r6
	shl 0x00000002, r5
	add r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 533
	mov r22, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 289
	ld.w 0x00000000[r5], r5
	st.w r5, 0x000000A0[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 291
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 290
	ld.w 0x000000CC[r21], r29
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 291
	ld.w 0x00000004[r5], r5
	st.w r5, 0x000000CC[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 533
	jarl _x_release_lock.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r30
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r30], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.4_26
.BB.LABEL.4_25:	; if_then_bb.i102
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov #.STR.1, r6
	movhi HIGHW1(#_fatal_file_name), r0, r5
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movea 0x000000CA, r0, r6
	movhi HIGHW1(#_fatal_line_num), r0, r5
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r5
	mov r5, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.4_26:	; bb14.i106
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r30], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r30]
	bnz9 .BB.LABEL.4_28
.BB.LABEL.4_27:	; if_then_bb28.i114
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r30], r5
	mov #_pmr_setting_tbl, r6
	add r5, r5
	add r6, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.4_28:	; x_nested_unlock_os_int.1.exit116
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 295
	ld.bu 0x00000000[r21], r5
	mul 0x0000000C, r27, r0
	shl 0x00000002, r5
	add r26, r5
	ld.w 0x00000000[r5], r5
	add r27, r5
	ld.w 0x00000000[r5], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 303
	jarl _exit_isr2, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r20, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.4_30
.BB.LABEL.4_29:	; if_then_bb.i88
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.4_30:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 520
	mov r22, r6
	jarl _x_acquire_lock.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 307
	st.w r29, 0x000000CC[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 308
	st.w r28, 0x000000A0[r21]
	br9 .BB.LABEL.4_32
.BB.LABEL.4_31:	; if_else_bb129
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 317
	shl r27, r23, r5
	not r5, r5
	and r5, r6
	st.w r6, 0x000000C0[r21]
.BB.LABEL.4_32:	; bb144
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	ld.w 0x000000C4[r21], r5
	ld.w 0x000000C0[r21], r6
	mov r5, r7
	and r6, r7
	bnz17 .BB.LABEL.4_6
.BB.LABEL.4_33:	; bb149
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 325
	st.b r0, 0x000000C8[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 533
	mov r22, r6
	jarl _x_release_lock.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r20
	ld.w 0x00000000[r20], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.4_35
.BB.LABEL.4_34:	; if_then_bb.i69
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.4_35:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r20]
	bnz9 .BB.LABEL.4_37
.BB.LABEL.4_36:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r5
	mov #_pmr_setting_tbl, r6
	add r5, r5
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.4_37:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 329
	dispose 0x00000020, 0x00000FFF, [r31]
	.align 4
_dispatch_request:
	.stack _dispatch_request = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 342
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov 0x00000001, r10
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r7
	ld.w 0x00000000[r7], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 348
	cmp r5, r6
	bz9 .BB.LABEL.5_5
.BB.LABEL.5_1:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 353
	ld.bu 0x000000C8[r6], r5
	mov 0x00000000, r10
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.5_5
.BB.LABEL.5_2:	; if_then_bb15
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 354
	st.b r5, 0x000000C8[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ici.h", 80
	ld.bu 0x00000000[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ici.h", 69
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.5_4
.BB.LABEL.5_3:	; if_then_bb.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	st23.w r5, 0xFFFEEC80[r0]
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 359
	jmp [r31]
.BB.LABEL.5_4:	; if_else_bb.i.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000002, r5
	br9 .BB.LABEL.5_3
.BB.LABEL.5_5:	; bb21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 359
	jmp [r31]
	.align 4
_shutdown_request:
	.stack _shutdown_request = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 377
	movhi HIGHW1(#_shutdown_reqflg), r0, r6
	ld.bu LOWW(#_shutdown_reqflg)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r5
	movea 0x000000FF, r5, r5
	andi 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 377
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.6_7
.BB.LABEL.6_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 378
	movhi HIGHW1(#_shutdown_reqflg), r0, r6
	mov 0x00000001, r7
	st.b r7, LOWW(#_shutdown_reqflg)[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 380
	movhi HIGHW1(#_p_ccb_table), r0, r6
	ld.w LOWW(#_p_ccb_table)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 381
	ld.bu 0x00000001[r6], r6
	cmp 0x00000000, r6
	bz9 .BB.LABEL.6_4
.BB.LABEL.6_2:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	cmp 0x00000000, r5
	bz9 .BB.LABEL.6_4
.BB.LABEL.6_3:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r7, 0xFFFEEC80[r0]
.BB.LABEL.6_4:	; bb.split.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 380
	movhi HIGHW1(#_p_ccb_table+0x00000004), r0, r6
	ld.w LOWW(#_p_ccb_table+0x00000004)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 381
	ld.bu 0x00000001[r6], r6
	cmp 0x00000000, r6
	bz9 .BB.LABEL.6_7
.BB.LABEL.6_5:	; bb.split.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	cmp 0x00000001, r5
	bz9 .BB.LABEL.6_7
.BB.LABEL.6_6:	; if_else_bb.i.ul
	mov 0x00000002, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r5, 0xFFFEEC80[r0]
.BB.LABEL.6_7:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 386
	jmp [r31]
	.align 4
_busy_wait.1:
	.stack _busy_wait.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 402
	add 0xFFFFFFFC, r3
	movea 0x00000063, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 405
	st.b r0, 0x00000003[r3]
	ld.bu 0x00000003[r3], r5
	cmp r6, r5
	bh9 .BB.LABEL.7_2
.BB.LABEL.7_1:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movea 0x00000003, r3, r5
	movea 0x00000064, r0, r6
	ld.b 0x00000000[r5], r5
	add 0x00000001, r5
	st.b r5, 0x00000003[r3]
	ld.bu 0x00000003[r3], r5
	cmp r6, r5
	bl9 .BB.LABEL.7_1
.BB.LABEL.7_2:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 407
	dispose 0x00000004, 0x00000000, [r31]
	.align 4
_barrier_sync:
	.stack _barrier_sync = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 415
	prepare 0x00000071, 0x00000004
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_core_init.1.barrier_sync, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	add r20, r5
	st.b r6, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r6, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 492
	cmp 0x00000001, r5
	bnz9 .BB.LABEL.8_15
.BB.LABEL.8_1:	; bb29.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	cmp 0x00000000, r7
	bz9 .BB.LABEL.8_6
.BB.LABEL.8_2:	; bb29.preheader.bb29_crit_edge
	mov 0x00000000, r22
	mov r22, r5
.BB.LABEL.8_3:	; bb29
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 429
	andi 0x000000FF, r5, r6
	cmp 0x00000001, r6
	bgt9 .BB.LABEL.8_9
.BB.LABEL.8_4:	; bb13
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 430
	add r20, r6
	ld.bu 0x00000000[r6], r6
	cmp r6, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 431
	adf 0x00000002, r0, r22, r22
	add 0x00000001, r5
	br9 .BB.LABEL.8_3
.BB.LABEL.8_5:	; bb35.us
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 434
	jarl _busy_wait.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 427
	movhi HIGHW1(#_activated_cores), r0, r5
	ld.w LOWW(#_activated_cores)[r5], r5
	andi 0x000000FF, r22, r22
	cmp r5, r22
	bnc9 .BB.LABEL.8_14
.BB.LABEL.8_6:	; bb29.preheader.bb29.us_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000000, r22
	mov r22, r5
.BB.LABEL.8_7:	; bb29.us
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 429
	andi 0x000000FF, r5, r6
	cmp 0x00000001, r6
	bgt9 .BB.LABEL.8_5
.BB.LABEL.8_8:	; bb13.us
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 430
	add r20, r6
	ld.bu 0x00000000[r6], r6
	cmp r6, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 431
	adf 0x00000002, r0, r22, r22
	add 0x00000001, r5
	br9 .BB.LABEL.8_7
.BB.LABEL.8_9:	; bb35
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 434
	jarl _busy_wait.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 435
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.8_13
.BB.LABEL.8_10:	; if_then_bb52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r6
	ld.w 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.8_12
.BB.LABEL.8_11:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.8_12:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 442
	jarl _internal_shutdownallcores, r31
.BB.LABEL.8_13:	; if_break_bb53
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 427
	movhi HIGHW1(#_activated_cores), r0, r5
	ld.w LOWW(#_activated_cores)[r5], r5
	andi 0x000000FF, r22, r6
	cmp r5, r6
	bl9 .BB.LABEL.8_2
.BB.LABEL.8_14:	; bb60.split
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 445
	st.b r21, 0x00000003[r3]
	dispose 0x00000004, 0x00000071, [r31]
.BB.LABEL.8_15:	; bb82.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 448
	ld.bu 0x00000003[r3], r5
	cmp r5, r21
	bz9 .BB.LABEL.8_23
.BB.LABEL.8_16:	; bb.nph
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	cmp 0x00000000, r7
	bnz9 .BB.LABEL.8_18
.BB.LABEL.8_17:	; bb62.us
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 449
	jarl _busy_wait.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 448
	ld.bu 0x00000003[r3], r5
	cmp r5, r21
	bnz9 .BB.LABEL.8_17
	br9 .BB.LABEL.8_23
.BB.LABEL.8_18:	; bb62
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 449
	jarl _busy_wait.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 450
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.8_22
.BB.LABEL.8_19:	; if_then_bb80
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r6
	ld.w 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.8_21
.BB.LABEL.8_20:	; if_then_bb.i19
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.8_21:	; x_nested_lock_os_int.1.exit22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 457
	jarl _internal_shutdownallcores, r31
.BB.LABEL.8_22:	; bb82
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 448
	ld.bu 0x00000003[r3], r5
	cmp r5, r21
	bnz9 .BB.LABEL.8_18
.BB.LABEL.8_23:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 461
	dispose 0x00000004, 0x00000071, [r31]
	.align 4
_internal_shutdownallcores:
	.stack _internal_shutdownallcores = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 472
	prepare 0x00000041, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	mov r6, r20
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r7
	ld.w 0x00000000[r7], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.9_2
.BB.LABEL.9_1:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.9_2:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 477
	jarl _shutdown_request, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 480
	mov #_internal_call_shtdwnhk, r6
	mov r20, r7
	jarl _stack_change_and_call_func_1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 484
	jarl _object_terminate, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 490
	jarl _target_exit, r31
.BB.LABEL.9_3:	; bb3
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 496
	br9 .BB.LABEL.9_3
	.align 4
_acquire_tsk_lock:
	.stack _acquire_tsk_lock = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 518
	add 0x00000008, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 520
	jr _x_acquire_lock.1
	.align 4
_release_tsk_lock:
	.stack _release_tsk_lock = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 531
	add 0x00000008, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 533
	jr _x_release_lock.1
	.align 4
_release_tsk_lock_and_dispatch:
	.stack _release_tsk_lock_and_dispatch = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 544
	prepare 0x00000061, 0x00000000
	mov r6, r20
	mov r7, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 621
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 241
	st.w r0, 0x00000008[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 243
	._line_top inline_asm
  snooze
	._line_end inline_asm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 547
	cmp 0x00000000, r21
	bnz9 .BB.LABEL.12_2
.BB.LABEL.12_1:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 550
	dispose 0x00000000, 0x00000061, [r31]
.BB.LABEL.12_2:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 548
	dispose 0x00000000, 0x00000061
	jr _dispatch
	.align 4
_acquire_cnt_lock:
	.stack _acquire_cnt_lock = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 560
	add 0x0000000C, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 562
	jr _x_acquire_lock.1
	.align 4
_release_cnt_lock:
	.stack _release_cnt_lock = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 573
	add 0x0000000C, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 575
	jr _x_release_lock.1
	.align 4
_acquire_spn_lock:
	.stack _acquire_spn_lock = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 586
	jr _x_acquire_lock.1
	.align 4
_release_spn_lock:
	.stack _release_spn_lock = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 599
	jr _x_release_lock.1
	.align 4
_try_spn_lock:
	.stack _try_spn_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 612
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 226
	._line_top inline_asm
.local aa,bb,cc
aa:
  addi    -8,sp,sp
  mov     sp, ep
  sst.w    r21,4[ep]
  sst.w    zero,0[ep]
  movea    0,ep,r6
  ldl.w [r6], r21
  cmp   r0, r21
  bnz   bb       
  mov   1, r21 
  stc.w r21, [r6]
  cmp   r0, r21 
  be    bb
  mov   1, r10
  br    cc
bb:     
  mov   0, r10
  sld.w    4[ep],r21
  addi    8,ep,sp
cc:
  sld.w    4[ep],r21
  addi    8,ep,sp

	._line_end inline_asm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 229
	cmp 0x00000001, r10
	bnz9 .BB.LABEL.17_2
.BB.LABEL.17_1:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 633
	jarl _v850_memory_changed, r31
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 634
	dispose 0x00000000, 0x00000001, [r31]
.BB.LABEL.17_2:	; x_try_lock.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 615
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_acquire_ioc_lock:
	.stack _acquire_ioc_lock = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 624
	jr _x_acquire_lock.1
	.align 4
_release_ioc_lock:
	.stack _release_ioc_lock = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 636
	jr _x_release_lock.1
	.section .bss, bss
	.align 4
_ioc_lock:
	.ds (4)
_shutdown_reqflg:
	.ds (1)
	.section .data, data
	.align 4
_activated_cores:
	.dw 0x00000001
_core_init.1.barrier_sync:
	.ds (2)
	.section .const, const
.STR.1:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x61,0x72,0x63,0x68
	.db 0x5C,0x76,0x38,0x35,0x30,0x5F,0x67,0x63,0x63,0x2F,0x70,0x72,0x63,0x5F,0x63,0x6F
	.db 0x6E,0x66,0x69,0x67,0x2E,0x68
	.ds (1)
.STR.1726:
	.db 0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x6B,0x65,0x72,0x6E,0x65,0x6C,0x5C
	.db 0x6B,0x65,0x72,0x6E,0x65,0x6C,0x5F,0x69,0x6D,0x70,0x6C,0x2E,0x68
	.ds (1)
