#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\mc.c -oDefaultBuild\mc.obj -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_ub0igrnd.4d2
#@	compiled at Tue Jul 24 09:58:48 2018

	.file "..\..\..\kernel\mc.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _pmr_setting_tbl
	.extern _tnum_ici_of_core
	.extern _p_iciinb_table
	.extern _p_isrcb_table
	.public _ioc_lock, 4
	.public _shutdown_reqflg, 1
	.public _activated_cores, 4
	.extern _v850_memory_changed
	.public _ccb_initialize
	.public _ici_handler_main
	.extern _exit_isr2
	.public _dispatch_request
	.public _shutdown_request
	.public _barrier_sync
	.public _internal_shutdownallcores
	.public _acquire_tsk_lock
	.public _release_tsk_lock
	.extern _stack_change_and_call_func_1
	.extern _internal_call_shtdwnhk
	.extern _object_terminate
	.extern _target_exit
	.public _release_tsk_lock_and_dispatch
	.extern _dispatch
	.public _acquire_cnt_lock
	.public _release_cnt_lock
	.public _acquire_spn_lock
	.public _release_spn_lock
	.public _try_spn_lock
	.public _acquire_ioc_lock
	.public _release_ioc_lock

	.section .text, text
	.align 4
_current_peid.1:
	.stack _current_peid.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	jmp [r31]
	.align 4
_acquire_lock_ldlstc.1:
	.stack _acquire_lock_ldlstc.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 216
	._line_top inline_asm
.local aa,bb,cc
aa:
  addi    -8,sp,sp
  mov     sp, ep
  sst.w    r21,4[ep]
  sst.w    zero,0[ep]
  movea    0,ep,r6
  ldl.w [r6], r21
  cmp   r0, r21
  bnz   bb       
  mov   1, r21 
  stc.w r21, [r6]
  cmp   r0, r21 
  be    bb
  mov   1, r10
  br    cc
bb:     
  mov   0, r10
  sld.w    4[ep],r21
  addi    8,ep,sp
cc:
  sld.w    4[ep],r21
  addi    8,ep,sp

	._line_end inline_asm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 228
	cmp 0x00000001, r10
	setf 0x00000002, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 229
	jmp [r31]
	.align 4
_release_lock_ldlstc.1:
	.stack _release_lock_ldlstc.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 239
	st.w r0, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 243
	._line_top inline_asm
  snooze
	._line_end inline_asm
	jmp [r31]
	.align 4
_x_core_id.1:
	.stack _x_core_id.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 112
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 115
	jarl _current_peid.1, r31
	add 0xFFFFFFFF, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	zxb r10
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_get_my_p_ccb.1:
	.stack _get_my_p_ccb.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 244
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 246
	jarl _x_core_id.1, r31
	shl 0x00000002, r10
	mov #_p_ccb_table, r5
	add r10, r5
	ld.w 0x00000000[r5], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_target_ici_raise.1:
	.stack _target_ici_raise.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ici.h", 67
	cmp 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ici.h", 69
	bnz9 .BB.LABEL.6_2
.BB.LABEL.6_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r5, 0xFFFEEC80[r0]
	jmp [r31]
.BB.LABEL.6_2:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000002, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_sil.h", 119
	st23.w r5, 0xFFFEEC80[r0]
	jmp [r31]
	.align 4
_target_ici_dispreq.1:
	.stack _target_ici_dispreq.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ici.h", 78
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ici.h", 80
	ld.bu 0x00000000[r6], r6
	jarl _target_ici_raise.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_target_ici_clear.1:
	.stack _target_ici_clear.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ici.h", 88
	jmp [r31]
	.align 4
_x_sense_mcore.1:
	.stack _x_sense_mcore.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 489
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 491
	jarl _x_core_id.1, r31
	cmp 0x00000000, r10
	setf 0x00000002, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 492
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_x_initialize_tsk_lock.1:
	.stack _x_initialize_tsk_lock.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 557
	st.w r0, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 559
	jmp [r31]
	.align 4
_x_initialize_cnt_lock.1:
	.stack _x_initialize_cnt_lock.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 566
	st.w r0, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 568
	jmp [r31]
	.align 4
_x_initialize_ioc_lock.1:
	.stack _x_initialize_ioc_lock.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 584
	st.w r0, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 586
	jmp [r31]
	.align 4
_x_acquire_lock.1:
	.stack _x_acquire_lock.1 = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 600
	prepare 0x00000061, 0x00000000
	mov r6, r20
	br9 .BB.LABEL.13_8
.BB.LABEL.13_1:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 199
	jarl _get_my_p_ccb.1, r31
	mov r10, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r21], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.13_3
.BB.LABEL.13_2:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.13_3:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r21], r5
	addi 0xFFFFFFFF, r5, r6
	st.b r6, 0x000000D5[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	bnz9 .BB.LABEL.13_5
.BB.LABEL.13_4:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r21], r5
	add r5, r5
	mov #_pmr_setting_tbl, r6
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.13_5:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 177
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r10], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.13_7
.BB.LABEL.13_6:	; if_then_bb.i7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.13_7:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r10], r5
	add 0x00000001, r5
	st.b r5, 0x000000D5[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
.BB.LABEL.13_8:	; bb7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 603
	mov r20, r6
	jarl _acquire_lock_ldlstc.1, r31
	cmp 0x00000000, r10
	bz9 .BB.LABEL.13_1
.BB.LABEL.13_9:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 605
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 606
	dispose 0x00000000, 0x00000061, [r31]
	.align 4
_x_release_lock.1:
	.stack _x_release_lock.1 = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 619
	prepare 0x00000041, 0x00000000
	mov r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 621
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 622
	mov r20, r6
	jarl _release_lock_ldlstc.1, r31
	dispose 0x00000000, 0x00000041, [r31]
	.align 4
_x_try_lock.1:
	.stack _x_try_lock.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 629
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 631
	jarl _acquire_lock_ldlstc.1, r31
	cmp 0x00000000, r10
	bz9 .BB.LABEL.15_2
.BB.LABEL.15_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 633
	jarl _v850_memory_changed, r31
	mov 0x00000001, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 634
	dispose 0x00000000, 0x00000001, [r31]
.BB.LABEL.15_2:	; bb8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 637
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_ccb_initialize:
	.stack _ccb_initialize = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 123
	prepare 0x00000041, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 126
	jarl _get_my_p_ccb.1, r31
	mov r10, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 127
	jarl _x_core_id.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 132
	st.b r10, 0x00000000[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 143
	addi 0x00000008, r20, r6
	jarl _x_initialize_tsk_lock.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 148
	addi 0x0000000C, r20, r6
	jarl _x_initialize_cnt_lock.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 150
	jarl _x_sense_mcore.1, r31
	cmp 0x00000000, r10
	bz9 .BB.LABEL.16_2
.BB.LABEL.16_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 161
	mov #_ioc_lock, r6
	jarl _x_initialize_ioc_lock.1, r31
.BB.LABEL.16_2:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 169
	st.w r0, 0x00000014[r20]
	st.w r0, 0x00000010[r20]
	mov 0x0000000F, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 170
	st.w r5, 0x00000018[r20]
	mov 0x00000000, r5
	br9 .BB.LABEL.16_4
.BB.LABEL.16_3:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 173
	mov r5, r6
	shl 0x00000003, r6
	add r20, r6
	movea 0x0000001C, r6, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r7, 0x00000020[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r7, 0x0000001C[r6]
	add 0x00000001, r5
.BB.LABEL.16_4:	; bb30
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 172
	cmp 0x0000000F, r5
	bnh9 .BB.LABEL.16_3
.BB.LABEL.16_5:	; bb35
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 176
	st.h r0, 0x0000009C[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 181
	st.w r0, 0x000000A0[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 183
	st.b r0, 0x00000002[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 184
	st.b r0, 0x000000A4[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 185
	st.b r0, 0x00000003[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 186
	st.b r0, 0x000000A5[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 189
	jarl _x_sense_mcore.1, r31
	cmp 0x00000000, r10
	bz9 .BB.LABEL.16_7
.BB.LABEL.16_6:	; if_then_bb53
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 190
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	st.b r0, LOWW(#_shutdown_reqflg)[r5]
.BB.LABEL.16_7:	; if_break_bb54
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 197
	st.w r0, 0x000000C0[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 202
	st.b r0, 0x000000C8[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 207
	st.w r0, 0x000000CC[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 212
	st.w r0, 0x000000DC[r20]
	dispose 0x00000000, 0x00000041, [r31]
	.align 4
_ici_handler_main:
	.stack _ici_handler_main = 56
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 239
	prepare 0x00000479, 0x00000020
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 249
	jarl _get_my_p_ccb.1, r31
	mov r10, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 251
	jarl _target_ici_clear.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 257
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.17_2
.BB.LABEL.17_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 258
	jarl _internal_shutdownallcores, r31
.BB.LABEL.17_2:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 177
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r10], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.17_4
.BB.LABEL.17_3:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.17_4:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r10], r5
	add 0x00000001, r5
	st.b r5, 0x000000D5[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 263
	mov r20, r6
	jarl _acquire_tsk_lock, r31
	jr .BB.LABEL.17_36
.BB.LABEL.17_5:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 269
	andi 0x0000FFFF, r21, r0
	bz9 .BB.LABEL.17_15
.BB.LABEL.17_6:	; if_then_bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x0000000F, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	mov #.STR.1726, r6
	movea 0x00000002, r3, r7
	add r7, r5
	br9 .BB.LABEL.17_8
.BB.LABEL.17_7:	; if_then_bb28
	ld.b 0x00000000[r6], r8
	st.b r8, 0x00000000[r7]
	add 0x00000001, r6
	add 0x00000001, r7
.BB.LABEL.17_8:	; if_then_bb28
	cmp r7, r5
	bnz9 .BB.LABEL.17_7
.BB.LABEL.17_9:	; if_then_bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 405
	andi 0x000000FF, r21, r0
	bz9 .BB.LABEL.17_11
.BB.LABEL.17_10:	; if_then_bb28.if_break_bb29.i_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000000, r22
	br9 .BB.LABEL.17_12
.BB.LABEL.17_11:	; if_then_bb20.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 406
	zxh r21
	shr 0x00000008, r21
	mov 0x00000008, r22
.BB.LABEL.17_12:	; if_break_bb29.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 409
	andi 0x0000000F, r21, r0
	bnz9 .BB.LABEL.17_14
.BB.LABEL.17_13:	; if_then_bb36.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	zxh r21
	shr 0x00000004, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 411
	add 0x00000004, r22
.BB.LABEL.17_14:	; bitmap_search.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 413
	andi 0x0000000F, r21, r21
	movea 0x00000002, r3, r5
	add r5, r21
	ld.bu 0xFFFFFFFF[r21], r21
	add r21, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 414
	zxh r22
	br9 .BB.LABEL.17_26
.BB.LABEL.17_15:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x0000000F, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 397
	mov #.STR.1726, r5
	movea 0x00000011, r3, r6
	add r6, r22
	br9 .BB.LABEL.17_17
.BB.LABEL.17_16:	; if_else_bb
	ld.b 0x00000000[r5], r7
	st.b r7, 0x00000000[r6]
	add 0x00000001, r5
	add 0x00000001, r6
.BB.LABEL.17_17:	; if_else_bb
	cmp r6, r22
	bnz9 .BB.LABEL.17_16
.BB.LABEL.17_18:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 273
	mov r21, r22
	shr 0x00000010, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 404
	cmp 0x00000000, r22
	bnz9 .BB.LABEL.17_20
.BB.LABEL.17_19:	; if_then_bb.i13
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1727, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x00000194, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.17_20:	; bb13.i17
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 405
	andi 0x000000FF, r22, r0
	bz9 .BB.LABEL.17_22
.BB.LABEL.17_21:	; bb13.i17.if_break_bb29.i25_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000000, r5
	br9 .BB.LABEL.17_23
.BB.LABEL.17_22:	; if_then_bb20.i19
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 406
	shr 0x00000018, r21
	mov 0x00000008, r5
	mov r21, r22
.BB.LABEL.17_23:	; if_break_bb29.i25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 409
	andi 0x0000000F, r22, r0
	bnz9 .BB.LABEL.17_25
.BB.LABEL.17_24:	; if_then_bb36.i28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 411
	add 0x00000004, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 410
	shr 0x00000004, r22
.BB.LABEL.17_25:	; bitmap_search.1.exit38
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 413
	andi 0x0000000F, r22, r21
	movea 0x00000011, r3, r22
	add r22, r21
	ld.bu 0xFFFFFFFF[r21], r21
	add r21, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/kernel_impl.h", 414
	zxh r5
	movea 0x00000010, r5, r22
.BB.LABEL.17_26:	; if_break_bb40
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 277
	ld.bu 0x00000000[r20], r21
	mov #_tnum_ici_of_core, r5
	add r21, r5
	ld.bu 0x00000000[r5], r5
	cmp r5, r22
	bnc17 .BB.LABEL.17_35
.BB.LABEL.17_27:	; if_then_bb52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 279
	shl 0x00000002, r21
	mov #_p_iciinb_table, r5
	add r5, r21
	ld.w 0x00000000[r21], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 280
	mov r22, r6
	mul 0x0000000C, r6, r0
	add r6, r5
	ld.w 0x00000004[r5], r5
	ld.w 0x000000C4[r20], r6
	tst r5, r6
	bz17 .BB.LABEL.17_36
.BB.LABEL.17_28:	; if_then_bb71
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 282
	ld.w 0x000000C0[r20], r6
	not r5, r5
	and r5, r6
	st.w r6, 0x000000C0[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 289
	mov r22, r5
	mul 0x0000000C, r5, r0
	ld.w 0x00000000[r21], r21
	add r5, r21
	ld.w 0x00000008[r21], r21
	shl 0x00000002, r21
	mov #_p_isrcb_table, r5
	add r21, r5
	ld.w 0x00000000[r5], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 288
	ld.w 0x000000A0[r20], r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 289
	st.w r21, 0x000000A0[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 291
	ld.w 0x00000000[r21], r21
	ld.w 0x00000004[r21], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 290
	ld.w 0x000000CC[r20], r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 291
	st.w r21, 0x000000CC[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 292
	mov r20, r6
	jarl _release_tsk_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 199
	jarl _get_my_p_ccb.1, r31
	mov r10, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r21], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.17_30
.BB.LABEL.17_29:	; if_then_bb.i54
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.17_30:	; bb14.i58
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r21], r5
	addi 0xFFFFFFFF, r5, r6
	st.b r6, 0x000000D5[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	bnz9 .BB.LABEL.17_32
.BB.LABEL.17_31:	; if_then_bb28.i66
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r21], r21
	add r21, r21
	mov #_pmr_setting_tbl, r5
	add r21, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r5], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r21, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.17_32:	; x_nested_unlock_os_int.1.exit68
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 295
	ld.bu 0x00000000[r20], r21
	shl 0x00000002, r21
	mov #_p_iciinb_table, r5
	add r21, r5
	ld.w 0x00000000[r5], r21
	mul 0x0000000C, r22, r0
	add r22, r21
	ld.w 0x00000000[r21], r21
	jarl [r21], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 303
	jarl _exit_isr2, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 177
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r10], r21
	cmp 0x00000000, r21
	bnz9 .BB.LABEL.17_34
.BB.LABEL.17_33:	; if_then_bb.i46
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r21], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r22, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r21, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r22, 5
.BB.LABEL.17_34:	; x_nested_lock_os_int.1.exit49
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r10], r21
	add 0x00000001, r21
	st.b r21, 0x000000D5[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 306
	mov r20, r6
	jarl _acquire_tsk_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 307
	st.w r24, 0x000000CC[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 308
	st.w r23, 0x000000A0[r20]
	br9 .BB.LABEL.17_36
.BB.LABEL.17_35:	; if_else_bb129
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000001, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 317
	shl r22, r21
	not r21, r21
	ld.w 0x000000C0[r20], r5
	and r21, r5
	st.w r5, 0x000000C0[r20]
.BB.LABEL.17_36:	; bb144
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	ld.w 0x000000C0[r20], r21
	ld.w 0x000000C4[r20], r5
	and r5, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 266
	cmp 0x00000000, r21
	bnz17 .BB.LABEL.17_5
.BB.LABEL.17_37:	; bb149
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 325
	st.b r0, 0x000000C8[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 327
	mov r20, r6
	jarl _release_tsk_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 199
	jarl _get_my_p_ccb.1, r31
	mov r10, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r20], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.17_39
.BB.LABEL.17_38:	; if_then_bb.i5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x000000CA, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	movea 0x0000001B, r0, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.17_39:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r20], r5
	addi 0xFFFFFFFF, r5, r6
	st.b r6, 0x000000D5[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	bnz9 .BB.LABEL.17_41
.BB.LABEL.17_40:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r20], r5
	add r5, r5
	mov #_pmr_setting_tbl, r6
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.17_41:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 329
	dispose 0x00000020, 0x00000479, [r31]
	.align 4
_dispatch_request:
	.stack _dispatch_request = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 342
	prepare 0x00000041, 0x00000000
	mov r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 346
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 348
	cmp r20, r10
	bnz9 .BB.LABEL.18_2
.BB.LABEL.18_1:	; entry.bb21_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000001, r10
	br9 .BB.LABEL.18_4
.BB.LABEL.18_2:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 353
	ld.bu 0x000000C8[r20], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.18_5
.BB.LABEL.18_3:	; if_else_bb.bb21_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000000, r10
.BB.LABEL.18_4:	; bb21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 359
	dispose 0x00000000, 0x00000041, [r31]
.BB.LABEL.18_5:	; if_then_bb15
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 354
	st.b r5, 0x000000C8[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 355
	mov r20, r6
	jarl _target_ici_dispreq.1, r31
	br9 .BB.LABEL.18_3
	.align 4
_shutdown_request:
	.stack _shutdown_request = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 370
	prepare 0x00000061, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 373
	jarl _x_core_id.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 377
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	mov r10, r20
	bnz9 .BB.LABEL.19_7
.BB.LABEL.19_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 378
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	mov 0x00000001, r6
	st.b r6, LOWW(#_shutdown_reqflg)[r5]
	mov 0x00000000, r21
	br9 .BB.LABEL.19_6
.BB.LABEL.19_2:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 380
	andi 0x000000FF, r21, r5
	shl 0x00000002, r5
	mov #_p_ccb_table, r6
	add r5, r6
	ld.w 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 381
	ld.bu 0x00000001[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.19_5
.BB.LABEL.19_3:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	andi 0x000000FF, r21, r5
	cmp r20, r5
	bz9 .BB.LABEL.19_5
.BB.LABEL.19_4:	; if_then_bb32
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 382
	andi 0x000000FF, r21, r6
	jarl _target_ici_raise.1, r31
.BB.LABEL.19_5:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	add 0x00000001, r21
.BB.LABEL.19_6:	; bb36
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 379
	andi 0x000000FF, r21, r5
	cmp 0x00000002, r5
	blt9 .BB.LABEL.19_2
.BB.LABEL.19_7:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 386
	dispose 0x00000000, 0x00000061, [r31]
	.align 4
_busy_wait.1:
	.stack _busy_wait.1 = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 402
	add 0xFFFFFFFC, r3
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 405
	st.b r0, 0x00000003[r3]
	br9 .BB.LABEL.20_2
.BB.LABEL.20_1:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movea 0x00000003, r3, r5
	ld.b 0x00000000[r5], r5
	add 0x00000001, r5
	st.b r5, 0x00000003[r3]
.BB.LABEL.20_2:	; bb3
	ld.bu 0x00000003[r3], r5
	movea 0x00000064, r0, r6
	cmp r6, r5
	bl9 .BB.LABEL.20_1
.BB.LABEL.20_3:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 407
	dispose 0x00000004, 0x00000000, [r31]
	.align 4
_barrier_sync:
	.stack _barrier_sync = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 415
	prepare 0x00000071, 0x00000000
	mov r6, r20
	mov r7, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 423
	jarl _x_core_id.1, r31
	mov #_core_init.1.barrier_sync, r5
	add r10, r5
	st.b r20, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 425
	jarl _x_sense_mcore.1, r31
	cmp 0x00000000, r10
	bz9 .BB.LABEL.21_16
.BB.LABEL.21_1:	; entry.bb28_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	mov 0x00000000, r22
	mov r22, r5
	br9 .BB.LABEL.21_3
.BB.LABEL.21_2:	; bb12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 429
	andi 0x000000FF, r5, r6
	mov #_core_init.1.barrier_sync, r7
	add r6, r7
	ld.bu 0x00000000[r7], r6
	cmp r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 430
	adf 0x00000002, r0, r22, r22
	add 0x00000001, r5
.BB.LABEL.21_3:	; bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 428
	andi 0x000000FF, r5, r6
	cmp 0x00000002, r6
	blt9 .BB.LABEL.21_2
.BB.LABEL.21_4:	; bb34
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 433
	jarl _busy_wait.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 434
	cmp 0x00000000, r21
	bz9 .BB.LABEL.21_9
.BB.LABEL.21_5:	; bb39
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.21_9
.BB.LABEL.21_6:	; if_then_bb51
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 177
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r10], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.21_8
.BB.LABEL.21_7:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.21_8:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r10], r5
	add 0x00000001, r5
	st.b r5, 0x000000D5[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 441
	jarl _internal_shutdownallcores, r31
.BB.LABEL.21_9:	; if_break_bb52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 426
	movhi HIGHW1(#_activated_cores), r0, r5
	ld.w LOWW(#_activated_cores)[r5], r5
	zxb r22
	cmp r5, r22
	bl9 .BB.LABEL.21_1
.BB.LABEL.21_10:	; bb59
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 444
	movhi HIGHW1(#_sys_start.2.barrier_sync), r0, r21
	st.b r20, LOWW(#_sys_start.2.barrier_sync)[r21]
	dispose 0x00000000, 0x00000071, [r31]
.BB.LABEL.21_11:	; bb61
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 448
	jarl _busy_wait.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 449
	cmp 0x00000000, r21
	bz9 .BB.LABEL.21_16
.BB.LABEL.21_12:	; bb66
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 0
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.21_16
.BB.LABEL.21_13:	; if_then_bb79
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 177
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r10], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.21_15
.BB.LABEL.21_14:	; if_then_bb.i8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.21_15:	; x_nested_lock_os_int.1.exit11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r10], r5
	add 0x00000001, r5
	st.b r5, 0x000000D5[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 456
	jarl _internal_shutdownallcores, r31
.BB.LABEL.21_16:	; bb81
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 447
	movhi HIGHW1(#_sys_start.2.barrier_sync), r0, r5
	ld.bu LOWW(#_sys_start.2.barrier_sync)[r5], r5
	cmp r5, r20
	bnz9 .BB.LABEL.21_11
.BB.LABEL.21_17:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 460
	dispose 0x00000000, 0x00000071, [r31]
	.align 4
_internal_shutdownallcores:
	.stack _internal_shutdownallcores = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 471
	prepare 0x00000041, 0x00000000
	mov r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 177
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r10], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.22_2
.BB.LABEL.22_1:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.22_2:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r10], r5
	add 0x00000001, r5
	st.b r5, 0x000000D5[r10]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 476
	jarl _shutdown_request, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 479
	mov #_internal_call_shtdwnhk, r6
	mov r20, r7
	jarl _stack_change_and_call_func_1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 483
	jarl _object_terminate, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 489
	jarl _target_exit, r31
.BB.LABEL.22_3:	; bb3
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 495
	br9 .BB.LABEL.22_3
	.align 4
_acquire_tsk_lock:
	.stack _acquire_tsk_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 517
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 519
	add 0x00000008, r6
	jarl _x_acquire_lock.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_release_tsk_lock:
	.stack _release_tsk_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 530
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 532
	add 0x00000008, r6
	jarl _x_release_lock.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_release_tsk_lock_and_dispatch:
	.stack _release_tsk_lock_and_dispatch = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 543
	prepare 0x00000041, 0x00000000
	mov r7, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 545
	add 0x00000008, r6
	jarl _x_release_lock.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 546
	cmp 0x00000000, r20
	bz9 .BB.LABEL.25_2
.BB.LABEL.25_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 547
	jarl _dispatch, r31
.BB.LABEL.25_2:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 549
	dispose 0x00000000, 0x00000041, [r31]
	.align 4
_acquire_cnt_lock:
	.stack _acquire_cnt_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 559
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 561
	add 0x0000000C, r6
	jarl _x_acquire_lock.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_release_cnt_lock:
	.stack _release_cnt_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 572
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 574
	add 0x0000000C, r6
	jarl _x_release_lock.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_acquire_spn_lock:
	.stack _acquire_spn_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 585
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 587
	jarl _x_acquire_lock.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_release_spn_lock:
	.stack _release_spn_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 598
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 600
	jarl _x_release_lock.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_try_spn_lock:
	.stack _try_spn_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 611
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 613
	jarl _x_try_lock.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 614
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_acquire_ioc_lock:
	.stack _acquire_ioc_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 623
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 625
	jarl _x_acquire_lock.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.align 4
_release_ioc_lock:
	.stack _release_ioc_lock = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 635
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/mc.c", 637
	jarl _x_release_lock.1, r31
	dispose 0x00000000, 0x00000001, [r31]
	.section .bss, bss
	.align 4
_ioc_lock:
	.ds (4)
_shutdown_reqflg:
	.ds (1)
_sys_start.2.barrier_sync:
	.ds (1)
	.section .data, data
	.align 4
_activated_cores:
	.dw 0x00000001
_core_init.1.barrier_sync:
	.ds (2)
	.section .const, const
.STR.1:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x61,0x72,0x63,0x68
	.db 0x5C,0x76,0x38,0x35,0x30,0x5F,0x67,0x63,0x63,0x2F,0x70,0x72,0x63,0x5F,0x63,0x6F
	.db 0x6E,0x66,0x69,0x67,0x2E,0x68
	.ds (1)
.STR.1726:
	.ds (1)
	.db 0x01
	.ds (1)
	.db 0x02
	.ds (1)
	.db 0x01
	.ds (1)
	.db 0x03
	.ds (1)
	.db 0x01
	.ds (1)
	.db 0x02
	.ds (1)
	.db 0x01
	.ds (1)
.STR.1727:
	.db 0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x6B,0x65,0x72,0x6E,0x65,0x6C,0x5C
	.db 0x6B,0x65,0x72,0x6E,0x65,0x6C,0x5F,0x69,0x6D,0x70,0x6C,0x2E,0x68
	.ds (1)
