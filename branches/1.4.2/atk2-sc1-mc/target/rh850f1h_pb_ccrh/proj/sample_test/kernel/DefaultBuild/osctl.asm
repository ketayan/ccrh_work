#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\kernel\osctl.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.extern _tnum_stdresource
	.extern _p_rescb_table
	.extern _tnum_task
	.extern _p_tcb_table
	.extern _tnum_counter
	.extern _p_cntcb_table
	.extern _tnum_alarm
	.extern _p_almcb_table
	.extern _tnum_scheduletable
	.extern _p_schtblcb_table
	.public _p_inib_initialize
	.public _cancel_shutdown_hook
	.extern _exit_trusted_shutdown_hook
	.extern _internal_shutdownallcores
	.public _callevel_chk_shutdown
	.public _call_protectionhk_main
	.public _internal_call_shtdwnhk
	.extern _barrier_sync
	.extern _call_trusted_hook
	.extern _ShutdownHook

	.section .text, text
	.align 4
_get_my_p_ccb.1:
	.stack _get_my_p_ccb.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r6
	ld.w 0x00000000[r6], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	jmp [r31]
	.align 4
_p_inib_initialize:
	.stack _p_inib_initialize = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 114
	movhi HIGHW1(#_tnum_task), r0, r5
	ld.w LOWW(#_tnum_task)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.2_3
.BB.LABEL.2_1:	; entry.bb_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 0
	mov 0x00000000, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 114
	movhi HIGHW1(#_tnum_task), r0, r7
	mov #_p_tcb_table, r6
.BB.LABEL.2_2:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 115
	ld.w 0x00000000[r6], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 114
	add 0x00000001, r5
	ld.w LOWW(#_tnum_task)[r7], r9
	add 0x00000004, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 115
	st.w r0, 0x00000008[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 114
	cmp r9, r5
	bl9 .BB.LABEL.2_2
.BB.LABEL.2_3:	; bb24.loopexit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 117
	movhi HIGHW1(#_tnum_alarm), r0, r5
	ld.w LOWW(#_tnum_alarm)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.2_6
.BB.LABEL.2_4:	; bb24.loopexit.bb17_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 0
	mov 0x00000000, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 117
	movhi HIGHW1(#_tnum_alarm), r0, r7
	mov #_p_almcb_table, r6
.BB.LABEL.2_5:	; bb17
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 118
	ld.w 0x00000000[r6], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 117
	add 0x00000001, r5
	ld.w LOWW(#_tnum_alarm)[r7], r9
	add 0x00000004, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 118
	st.w r0, 0x00000010[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 117
	cmp r9, r5
	bl9 .BB.LABEL.2_5
.BB.LABEL.2_6:	; bb38.loopexit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 120
	movhi HIGHW1(#_tnum_scheduletable), r0, r5
	ld.w LOWW(#_tnum_scheduletable)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.2_9
.BB.LABEL.2_7:	; bb38.loopexit.bb31_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 0
	mov 0x00000000, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 120
	movhi HIGHW1(#_tnum_scheduletable), r0, r7
	mov #_p_schtblcb_table, r6
.BB.LABEL.2_8:	; bb31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 121
	ld.w 0x00000000[r6], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 120
	add 0x00000001, r5
	ld.w LOWW(#_tnum_scheduletable)[r7], r9
	add 0x00000004, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 121
	st.w r0, 0x00000010[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 120
	cmp r9, r5
	bl9 .BB.LABEL.2_8
.BB.LABEL.2_9:	; bb52.loopexit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 123
	movhi HIGHW1(#_tnum_counter), r0, r5
	ld.w LOWW(#_tnum_counter)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.2_12
.BB.LABEL.2_10:	; bb52.loopexit.bb45_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 0
	mov 0x00000000, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 123
	movhi HIGHW1(#_tnum_counter), r0, r7
	mov #_p_cntcb_table, r6
.BB.LABEL.2_11:	; bb45
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 124
	ld.w 0x00000000[r6], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 123
	add 0x00000001, r5
	ld.w LOWW(#_tnum_counter)[r7], r9
	add 0x00000004, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 124
	st.w r0, 0x00000008[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 123
	cmp r9, r5
	bl9 .BB.LABEL.2_11
.BB.LABEL.2_12:	; bb66.loopexit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 126
	movhi HIGHW1(#_tnum_stdresource), r0, r5
	ld.w LOWW(#_tnum_stdresource)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.2_15
.BB.LABEL.2_13:	; bb66.loopexit.bb59_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 0
	mov 0x00000000, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 126
	movhi HIGHW1(#_tnum_stdresource), r0, r7
	mov #_p_rescb_table, r6
.BB.LABEL.2_14:	; bb59
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 127
	ld.w 0x00000000[r6], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 126
	add 0x00000001, r5
	ld.w LOWW(#_tnum_stdresource)[r7], r9
	add 0x00000004, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 127
	st.w r0, 0x00000000[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 126
	cmp r9, r5
	bl9 .BB.LABEL.2_14
.BB.LABEL.2_15:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 129
	jmp [r31]
	.align 4
_cancel_shutdown_hook:
	.stack _cancel_shutdown_hook = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 257
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 260
	jarl _exit_trusted_shutdown_hook, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 261
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x00000105, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	dispose 0x00000000, 0x00000001
	movea 0x0000001B, r0, r6
	jr _internal_shutdownallcores
	.align 4
_callevel_chk_shutdown:
	.stack _callevel_chk_shutdown = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 276
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 279
	ld.hu 0x00000004[r6], r5
	andi 0x00000020, r5, r0
	bnz9 .BB.LABEL.4_2
.BB.LABEL.4_1:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 283
	dispose 0x00000000, 0x00000001
	mov r7, r6
	jr _internal_shutdownallcores
.BB.LABEL.4_2:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 260
	jarl _exit_trusted_shutdown_hook, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 261
	movhi HIGHW1(#_fatal_file_name), r0, r5
	mov #.STR.1, r6
	st.w r6, LOWW(#_fatal_file_name)[r5]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	movea 0x00000105, r0, r6
	st.w r6, LOWW(#_fatal_line_num)[r5]
	dispose 0x00000000, 0x00000001
	movea 0x0000001B, r0, r6
	jr _internal_shutdownallcores
	.align 4
_call_protectionhk_main:
	.stack _call_protectionhk_main = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 294
	prepare 0x00000041, 0x00000000
	mov r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 296
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 339
	mov r20, r7
	mov r10, r6
	dispose 0x00000000, 0x00000041
	br9 _callevel_chk_shutdown
	.align 4
_internal_call_shtdwnhk:
	.stack _internal_call_shtdwnhk = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 351
	prepare 0x00000061, 0x00000000
	mov r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 354
	jarl _get_my_p_ccb.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 360
	ld.hu 0x00000004[r10], r5
	mov r10, r21
	andi 0x00000020, r5, r0
	bnz9 .BB.LABEL.6_2
.BB.LABEL.6_1:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 0
	mov 0x00000000, r7
	mov 0x00000004, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 362
	jarl _barrier_sync, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 364
	st.w r0, 0x000000CC[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 371
	mov r20, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_p_ccb_table, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 371
	mov #_ShutdownHook, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r21, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	set1 0x00000005, 0x00000004[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 371
	jarl _call_trusted_hook, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	shl 0x00000002, r5
	add r5, r21
	ld.w 0x00000000[r21], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 247
	clr1 0x00000005, 0x00000004[r5]
.BB.LABEL.6_2:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/osctl.c", 375
	dispose 0x00000000, 0x00000061, [r31]
	.section .const, const
.STR.1:
	.db 0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x2E,0x2E,0x5C,0x6B,0x65,0x72,0x6E,0x65,0x6C,0x5C
	.db 0x6F,0x73,0x63,0x74,0x6C,0x2E,0x63
	.ds (1)
