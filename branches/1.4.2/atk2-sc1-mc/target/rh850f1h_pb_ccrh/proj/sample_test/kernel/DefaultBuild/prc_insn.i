
























































 










 




 























































 













 




 























































 













 





















































 



 




 







 



 



 












































 



 




typedef unsigned char boolean;

typedef char				char8;
typedef unsigned char		uint8;
typedef signed char			sint8;
typedef unsigned short		uint16;
typedef signed short		sint16;
typedef unsigned int		uint32;
typedef signed int			sint32;
typedef unsigned long long	uint64;
typedef signed long long	sint64;

typedef unsigned long	uint8_least;
typedef unsigned long	uint16_least;
typedef unsigned long	uint32_least;
typedef signed long		sint8_least;
typedef signed long		sint16_least;
typedef signed long		sint32_least;

typedef float	float32;
typedef double	float64;

typedef uint32	uintptr;                 
typedef sint32	sintptr;                 













































 



 





















































 




 



 









 

 











































 



 













































 



 













































 



 




 

 
#pragma inline queue_initialize
#pragma inline queue_insert_prev
#pragma inline queue_insert_next
#pragma inline queue_delete
#pragma inline queue_delete_next
#pragma inline queue_empty



 

 
#pragma inline add_tick
#pragma inline diff_tick
#pragma inline get_curval

 
#pragma inline bitmap_search
#pragma inline primap_empty
#pragma inline primap_search
#pragma inline primap_set
#pragma inline primap_clear

 
#pragma inline syslog
#pragma inline _syslog_0
#pragma inline _syslog_1
#pragma inline _syslog_2
#pragma inline _syslog_3
#pragma inline _syslog_4
#pragma inline _syslog_5
#pragma inline _syslog_6


 
#pragma inline set_pmr
#pragma inline get_ispr
#pragma inline clear_ispr

 
#pragma inline x_lock_all_int
#pragma inline x_unlock_all_int
#pragma inline x_nested_lock_os_int
#pragma inline x_nested_unlock_os_int
#pragma inline x_suspend_lock_os_int
#pragma inline x_resume_unlock_os_int
#pragma inline x_set_ipm
#pragma inline x_get_ipm
#pragma inline x_disable_int
#pragma inline x_enable_int
#pragma inline x_clear_int
#pragma inline x_probe_int
#pragma inline i_begin_int
#pragma inline i_end_int

 
#pragma inline disable_int
#pragma inline enable_int
#pragma inline current_psw
#pragma inline set_psw
#pragma inline current_cpuid  

 
#pragma inline sil_reb_mem
#pragma inline sil_wrb_mem
#pragma inline sil_reh_mem
#pragma inline sil_wrh_mem
#pragma inline sil_rew_mem
#pragma inline sil_wrw_mem
#pragma inline TOPPERS_disint
#pragma inline TOPPERS_enaint

 
#pragma inline target_timer_get_current
#pragma inline target_timer_probe_int

 
#pragma inline uart_putc

 
#pragma inline uart_getready
#pragma inline uart_getchar









 









 


 



 

















 





 

typedef uint8 Std_ReturnType;

typedef struct {
	uint16	vendorID;
	uint16	moduleID;
	uint8	sw_major_version;
	uint8	sw_minor_version;
	uint8	sw_patch_version;
} Std_VersionInfoType;




 

typedef unsigned char StatusType;    





























































 


























































 



 






















































 



 





 

 


 


 


 






 



















































 






 













































 


 













































 



 






 



 



 



 



 

 

 






 








 
 

 

 






 


 



 



 



 


 






 





 




extern uint32 EnableSubOSC(void);

extern uint32 EnableMainOSC(uint32 clk_in);

extern uint32 EnablePLL0(void);

extern uint32 EnablePLL1(void);

extern uint32	SetClockSelection(uint32 s_control, uint32 s_status, uint8 regno, uint16 sel,
								  uint32 d_control, uint32 d_status, uint8 divider);

extern void raise_ipir(uint8 ch);






















































 



 










 


 


 





 





 




 

 

 

 

 

 



 



















































 






 



typedef uint32	SystemTimeMsType;
typedef uint32	SystemTimeUsType;
typedef uint32	SystemTime100NsType;






 







 



 
typedef uint8	TaskStateType;               
typedef uint32	EventMaskType;               
typedef uint32	TickType;                    
typedef uint32	AppModeType;                 
typedef uint8	OSServiceIdType;             
typedef uint8	ScheduleTableStatusType;     
typedef uint8	ProtectionReturnType;        
typedef uintptr	MemorySizeType;              
typedef uint8	ApplicationType;             
typedef uint8	ObjectTypeType;              

typedef struct {
	TickType	maxallowedvalue;             
	TickType	ticksperbase;                
	TickType	mincycle;                    
} AlarmBaseType;

typedef enum {
	TRYTOGETSPINLOCK_SUCCESS,
	TRYTOGETSPINLOCK_NOSUCCESS
} TryToGetSpinlockType;



 
typedef uint32	TimeType;                    
typedef uint32	AlarmType;                   
typedef uint32	ResourceType;                
typedef uint32	TaskType;                    
typedef uint32	ISRType;                     
typedef uint32	CounterType;                 
typedef uint32	ScheduleTableType;           
typedef float32	PhysicalTimeType;            
typedef uint8	CoreIdType;                  
typedef uint32	SpinlockIdType;              
typedef uint32	IocType;                     
typedef uint8	SenderIdType;                

typedef AlarmBaseType *				AlarmBaseRefType;
typedef TaskType *					TaskRefType;
typedef TaskStateType *				TaskStateRefType;
typedef EventMaskType *				EventMaskRefType;
typedef TickType *					TickRefType;
typedef ScheduleTableStatusType *	ScheduleTableStatusRefType;



 
typedef uint8 FaultyContextType;



 



 



 
typedef	sintptr StackType;     



 
typedef union {
	TaskType					tskid;
	TaskRefType					p_tskid;
	TaskStateRefType			p_stat;
	ResourceType				resid;
	EventMaskType				mask;
	EventMaskRefType			p_mask;
	AlarmType					almid;
	AlarmBaseRefType			p_info;
	TickRefType					p_tick;
	TickRefType					p_val;
	TickRefType					p_eval;
	TickType					incr;
	TickType					cycle;
	TickType					start;
	AppModeType					mode;
	CounterType					cntid;
	ScheduleTableType			schtblid;
	TickType					offset;
	ScheduleTableType			schtblid_from;
	ScheduleTableType			schtblid_to;
	ScheduleTableStatusRefType	p_schtblstate;
	ISRType						isrid;
	CoreIdType					coreid;
	StatusType					*status;
	SpinlockIdType				spnid;
	TryToGetSpinlockType		*tryspntype;
	IocType						iocid;
	SenderIdType				senderid;
} _ErrorHook_Par;






 




 



 
extern AppModeType GetActiveApplicationMode(void);
extern void StartOS(AppModeType Mode);
extern void ShutdownAllCores(StatusType Error);
extern FaultyContextType GetFaultyContext(void);



 
extern StatusType ActivateTask(TaskType TaskID);
extern StatusType TerminateTask(void);
extern StatusType ChainTask(TaskType TaskID);
extern StatusType Schedule(void);
extern StatusType GetTaskID(TaskRefType TaskID);
extern StatusType GetTaskState(TaskType TaskID, TaskStateRefType State);



 
extern void EnableAllInterrupts(void);
extern void DisableAllInterrupts(void);
extern void ResumeAllInterrupts(void);
extern void SuspendAllInterrupts(void);
extern void ResumeOSInterrupts(void);
extern void SuspendOSInterrupts(void);
extern ISRType GetISRID(void);
extern StatusType DisableInterruptSource(ISRType DisableISR);
extern StatusType EnableInterruptSource(ISRType EnableISR);



 
extern StatusType SetEvent(TaskType TaskID, EventMaskType Mask);
extern StatusType ClearEvent(EventMaskType Mask);
extern StatusType GetEvent(TaskType TaskID, EventMaskRefType Event);
extern StatusType WaitEvent(EventMaskType Mask);



 
extern StatusType GetResource(ResourceType ResID);
extern StatusType ReleaseResource(ResourceType ResID);



 
extern StatusType IncrementCounter(CounterType CounterID);



 
extern StatusType GetCounterValue(CounterType CounterID, TickRefType Value);
extern StatusType GetElapsedValue(CounterType CounterID, TickRefType Value, TickRefType ElapsedValue);



 
extern StatusType GetAlarmBase(AlarmType AlarmID, AlarmBaseRefType Info);
extern StatusType GetAlarm(AlarmType AlarmID, TickRefType Tick);
extern StatusType SetRelAlarm(AlarmType AlarmID, TickType increment, TickType cycle);
extern StatusType SetAbsAlarm(AlarmType AlarmID, TickType start, TickType cycle);
extern StatusType CancelAlarm(AlarmType AlarmID);



 
extern StatusType StartScheduleTableRel(ScheduleTableType ScheduleTableID, TickType Offset);
extern StatusType StartScheduleTableAbs(ScheduleTableType ScheduleTableID, TickType Start);
extern StatusType StopScheduleTable(ScheduleTableType ScheduleTableID);
extern StatusType NextScheduleTable(ScheduleTableType ScheduleTableID_From, ScheduleTableType ScheduleTableID_To);
extern StatusType GetScheduleTableStatus(ScheduleTableType ScheduleTableID, ScheduleTableStatusRefType ScheduleStatus);



 
extern ApplicationType GetApplicationID(void);
extern ApplicationType CheckTaskOwnership(TaskType TaskID);
extern ApplicationType CheckISROwnership(ISRType ISRID);
extern ApplicationType CheckAlarmOwnership(AlarmType AlarmID);
extern ApplicationType CheckCounterOwnership(CounterType CounterID);
extern ApplicationType CheckScheduleTableOwnership(ScheduleTableType ScheduleTableID);



 
extern uint32 GetNumberOfActivatedCores(void);
extern CoreIdType GetCoreID(void);
extern void StartCore(CoreIdType CoreID, StatusType *Status);
extern void StartNonAutosarCore(CoreIdType CoreID, StatusType *Status);
extern StatusType GetSpinlock(SpinlockIdType SpinlockId);
extern StatusType ReleaseSpinlock(SpinlockIdType SpinlockId);
extern StatusType TryToGetSpinlock(SpinlockIdType SpinlockId, TryToGetSpinlockType *Success);
extern StatusType RaiseInterCoreInterrupt(ISRType ISRID);



 
extern Std_ReturnType ioc_send_generic(IocType IocWrapperId, const void *in);
extern Std_ReturnType ioc_write_generic(IocType IocWrapperId, const void *in);
extern Std_ReturnType ioc_receive_generic(IocType IocId, void *out);
extern Std_ReturnType ioc_read_generic(IocType IocId, void *out);
extern Std_ReturnType ioc_empty_queue_generic(IocType IocId);



 



extern void StartupHook(void);





 
extern ApplicationType CheckObjectOwnership(ObjectTypeType ObjectType, ...);



 
extern const char8	*fatal_file_name;    
extern sint32		fatal_line_num;      





 

 




 



 




 



 



 



 





 



 



 



 



 



 






 

 



 



 


 


 
typedef uint32	InterruptNumberType;             
typedef uint32	AttributeType;                   
typedef sint32	PriorityType;                    

typedef void (*FunctionRefType)(void);           



 
extern void internal_shutdownallcores(StatusType ercd);





 





 


 























































 



 




















































 



 



 



 



 



 



 



















































 



 




















































 



 











































 



 




 




 




 
extern void v850_memory_changed(void);



 









 




 

static void
disable_int(void)
{
	__DI();
}

static void
enable_int(void)
{
	__EI();
}




static uint32
current_psw(void)
{
	return(__stsr_rh(5, 0));
}

static void
set_psw(uint32 psw)
{
	__ldsr_rh(5, 0, psw);
}



 
static void
set_pmr(uint16 pmr)
{
	uint32 psw;

	 
	psw = current_psw();
	disable_int();

	__ldsr_rh(11, 2, pmr);

	set_psw(psw);
}

static uint16
get_ispr(void)
{
	return(__stsr_rh(10, 2));
}

static void
clear_ispr(void)
{
	uint32 psw;

	 
	psw = current_psw();
	disable_int();

	__ldsr_rh(13, 2, 1);     
	__ldsr_rh(10, 2, 0);     
	__ldsr_rh(13, 2, 0);     
	__syncp();
	set_psw(psw);
}

static uint32
current_cpuid(void)
{
	uint32 htcfg0_val;
	htcfg0_val = __stsr_rh(0,2);
	return(((htcfg0_val >> 16) & 0x03));
}

static void
set_intbp(uint32 intbp)
{
	uint32 psw;

	 
	psw = current_psw();
	disable_int();

	__ldsr_rh(4, 1, intbp);

	set_psw(psw);
}



static uint32
current_peid(void)
{
	uint32 htcfg0_val;
	htcfg0_val = __stsr_rh(0, 2);
	return(((htcfg0_val >> 16) & 0x03));
}


static uint32
get_eiic(void)
{
	uint32 eiic;
	eiic = __stsr_rh(13, 0);
	return(eiic);
}


#pragma inline_asm (inline_asm_acquire_lock_ldlstc)
static int inline_asm_acquire_lock_ldlstc(uint32 *p_lock){
.local aa,bb,cc
aa:
  addi    -8,sp,sp
  mov     sp, ep
  sst.w    r21,4[ep]
  sst.w    zero,0[ep]
  movea    0,ep,r6
  ldl.w [r6], r21
  cmp   r0, r21
  bnz   bb       
  mov   1, r21 
  stc.w r21, [r6]
  cmp   r0, r21 
  be    bb
  mov   1, r10
  br    cc
bb:     
  mov   0, r10
  sld.w    4[ep],r21
  addi    8,ep,sp
cc:
  sld.w    4[ep],r21
  addi    8,ep,sp

}

static boolean
acquire_lock_ldlstc(uint32 *p_lock)
{
    uint32 locked;
    




 
    
    locked = inline_asm_acquire_lock_ldlstc(p_lock);
    
    return(locked == 1);
}



#pragma inline_asm inline_asm_snooze
static void inline_asm_snooze(void){
  snooze
}

static void
release_lock_ldlstc(uint32 *p_lock)
{
	*p_lock = 0;
	 
	inline_asm_snooze();
}


static void
set_rbase(uint32 addr)
{
	__ldsr_rh(2, 1, (unsigned int) addr);
}







 


 
typedef uint32 LockType;



 
typedef struct core_control_block CCB;



 
typedef struct target_core_control_block {
	


 
	uint32 except_nest_cnt;

	

 
	uint8 current_iintpri;

	

 
	uint8 nested_lock_os_int_cnt;

	

 
	uint16 current_intpri;

	uint32 trusted_hook_savedsp;
} TCCB;





 
static CoreIdType
x_core_id(void)
{
	 
	return((CoreIdType) (current_peid() - 1));
}





























































 










 




 
typedef struct queue {
	struct queue	*p_next;     
	struct queue	*p_prev;     
} QUEUE;



 
static void
queue_initialize(QUEUE *p_queue)
{
	p_queue->p_prev = p_queue;
	p_queue->p_next = p_queue;
}



 
static void
queue_insert_prev(QUEUE *p_queue, QUEUE *p_entry)
{
	p_entry->p_prev = p_queue->p_prev;
	p_entry->p_next = p_queue;
	p_queue->p_prev->p_next = p_entry;
	p_queue->p_prev = p_entry;
}



 
static void
queue_insert_next(QUEUE *p_queue, QUEUE *p_entry)
{
	p_entry->p_next = p_queue->p_next;
	p_entry->p_prev = p_queue;
	p_queue->p_next->p_prev = p_entry;
	p_queue->p_next = p_entry;
}



 
static void
queue_delete(QUEUE *p_entry)
{
	p_entry->p_prev->p_next = p_entry->p_next;
	p_entry->p_next->p_prev = p_entry->p_prev;
}



 
static QUEUE *
queue_delete_next(QUEUE *p_queue)
{
	QUEUE *p_entry;

	do { if (!(p_queue ->p_next != p_queue)) { fatal_file_name = "C:\\Users\\Sasuga\\Documents\\multicore\203v\203\215\203W\203F\203N\203g\\HSBRH850F1H176_BLSM_atk2_rccar_ccrh\\include\\queue.h"; fatal_line_num = 134; internal_shutdownallcores((27U)); } } while (0);
	p_entry = p_queue->p_next;
	p_queue->p_next = p_entry->p_next;
	p_entry->p_next->p_prev = p_queue;
	return(p_entry);
}



 
static boolean
queue_empty(const QUEUE *p_queue)
{
	return(p_queue->p_next == p_queue);
}
























































 



 





 




 



 
typedef struct spinlock_initialization_block {
	StatusType (*susint)(void);                                  
	FunctionRefType								resint;          
	const struct spinlock_initialization_block	*p_nextspn;      
} SPNINIB;



 
typedef struct spinlock_control_block {
	const SPNINIB					*p_spninib;      
	CCB								*p_ccb;          
	void							*p_holdcb;       
	struct spinlock_control_block	*p_prevspncb;    
	LockType spn_lock;
} SPNCB;



 
extern const SPNINIB		spninib_table[];



 
extern SPNCB				spncb_table[];



 
extern void spinlock_initialize(void);



 
extern const SpinlockIdType	tnum_spinlock;   



 
extern StatusType wrap_sus_all_int(void);



 
extern void wrap_res_all_int(void);



 
extern StatusType wrap_sus_os_int(void);



 
extern void wrap_res_os_int(void);



 
extern void force_release_spinlocks(SPNCB *p_lastspncb);




 
typedef struct task_control_block					TCB;
typedef struct isr_control_block					ISRCB;
typedef struct os_application_initialization_block	OSAPINIB;



 
struct core_control_block {

	CoreIdType coreid;

	

 
	boolean kerflg;

	

 
	uint8 sus_all_cnt;

	

 
	uint8 sus_os_cnt;

	

 
	uint16 callevel_stat;        

	

 
	LockType	tsk_lock;
	LockType	cnt_lock;

	

 

	








 
	TCB *p_runtsk;

	





 
	TCB *p_schedtsk;

	




 
	PriorityType nextpri;

	









 
	QUEUE ready_queue[(((15U) - (0U)) + 1U)];

	








 
	uint16 ready_primap;

	

 
	



 
	ISRCB *p_runisr;

	

 
	uint8 wrap_sus_all_cnt;

	

 
	uint8 wrap_sus_os_cnt;

	

 
	OSServiceIdType	_errorhook_svcid;
	_ErrorHook_Par	_errorhook_par1;
	_ErrorHook_Par	_errorhook_par2;
	_ErrorHook_Par	_errorhook_par3;
	_ErrorHook_Par	errorhook_par1;
	_ErrorHook_Par	errorhook_par2;
	_ErrorHook_Par	errorhook_par3;

	

 
	uint32 ici_request_map;

	

 
	uint32 ici_bit_mask;

	

 
	boolean ici_disreqflg;

	

 
	const OSAPINIB *p_runosap;

	

 
	TCCB target_ccb;

	

 
	SPNCB *p_protectspncb;
};



 
extern CCB * const p_ccb_table[];




 
static CCB *
get_my_p_ccb(void)
{
	return(p_ccb_table[x_core_id()]);
}





 



















































 






 





















































 



 




 



 




 



 
extern const InterruptNumberType	target_ici_intno_table[];



 
extern const PriorityType			target_ici_intpri_table[];



 











































 



 





















































 



 






 




 



 
static uint8
sil_reb_mem(void *mem)
{
	uint8 data;
	data = *((volatile uint8 *) mem);
	return(data);
}

static void
sil_wrb_mem(void *mem, uint8 data)
{
	*((volatile uint8 *) mem) = data;
}



 
static uint16
sil_reh_mem(void *mem)
{
	uint16 data;

	data = *((volatile uint16 *) mem);
	return(data);
}

static void
sil_wrh_mem(void *mem, uint16 data)
{
	*((volatile uint16 *) mem) = data;
}



 
static void
sil_wrw_mem(void *mem, uint32 data)
{
	*((volatile uint32 *) mem) = data;
}

static uint32
sil_rew_mem(void *mem)
{
	uint32 data;

	data = *((volatile uint32 *) mem);
	return(data);
}



 
static uint32
TOPPERS_disint(void)
{
	 
	volatile uint32 psw = current_psw();
	set_psw(psw | (1U << 5));

	return(psw);
}



 
static void
TOPPERS_enaint(uint32 TOPPERS_psw)
{
	if ((TOPPERS_psw & (1U << 5)) == 0u) {
		set_psw(current_psw() & ~(1U << 5));
	}
}


 
static uint32
sil_get_coreid(void)
{
	uint32 htcfg0_val;
  

    
    
    htcfg0_val = __stsr_rh(0, 2);
	return(((htcfg0_val >> 16) & 0x03) - 1);

}



 
extern uint32 TOPPERS_spn_var;



  




 

  



 
  














 
#pragma inline_asm func_toppers_spn_lock1
static uint32 func_toppers_spn_lock1(uint32 toppers_spn_var){
	.local aa,bb,cc
aa: ldl.w [r6], r21
    cmp   r0, r21   
    bnz   bb        
    mov   1, r21    
    stc.w r21, [r6] 
    cmp   r0, r21   
    be    bb        
    mov   1, r10    
    br    cc        
bb: mov   0, r10
cc:     
  }





 
static uint32
TOPPERS_spn_lock(void)
{
	volatile uint32 psw;
	uint32			locked;

  retry:
	 
	psw = TOPPERS_disint();

	




 


  locked = func_toppers_spn_lock1(TOPPERS_spn_var);
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

    if (locked == 0) {
		 
		 
		TOPPERS_enaint(psw);
		goto retry;
	}

	v850_memory_changed();
	
    
    __syncp(); 
	return(psw);
}



 
static void
TOPPERS_spn_unlock(uint32 psw)
{
	TOPPERS_spn_var = 0;
	 
	TOPPERS_enaint(psw);
  
  


 
  v850_memory_changed();
}



 






 



 
static void
target_ici_raise(CoreIdType coreid)
{
	if (coreid == 0) {
		sil_wrw_mem((void *) (0xfffeec80 + 0 * 4), 1);
	}
	else {
		sil_wrw_mem((void *) (0xfffeec80 + 0 * 4), 2);
	}
}

static void
target_ici_dispreq(const CCB *p_ccb)
{
	target_ici_raise(p_ccb->coreid);
}




 
static void
target_ici_clear(void)
{

}





 


extern void target_hardware_initialize(void);



 
extern void target_initialize(void);




 
extern void target_exit(void) ;



 
extern void target_fput_str(const char8 *c);



 
extern void fl850_led_output(uint8 pattern);




 



















































 







 





















































 



 




 








 



 




 



 



 



 
extern const uint16	pmr_isr2_mask;



 
extern const uint16	pmr_isr1_mask;



 
extern const uint16	pmr_setting_tbl[];



 
extern void infinite_loop(void) ;



 



 
static void
x_lock_all_int(void)
{
	disable_int();
}



 
static void
x_unlock_all_int(void)
{
	enable_int();
}






 
static void
x_nested_lock_os_int(void)
{
	CCB	*p_ccb = get_my_p_ccb();

	

 
	if (p_ccb->target_ccb.nested_lock_os_int_cnt == 0U) {
		set_pmr(pmr_isr2_mask);    
	}
	p_ccb->target_ccb.nested_lock_os_int_cnt++;

	v850_memory_changed();
}






 
static void
x_nested_unlock_os_int(void)
{
	CCB	*p_ccb = get_my_p_ccb();

	v850_memory_changed();
	do { if (!(p_ccb->target_ccb . nested_lock_os_int_cnt > 0U)) { fatal_file_name = "C:\\Users\\Sasuga\\Documents\\multicore\203v\203\215\203W\203F\203N\203g\\HSBRH850F1H176_BLSM_atk2_rccar_ccrh\\arch\\v850_gcc/prc_config.h"; fatal_line_num = 202; internal_shutdownallcores((27U)); } } while (0);
	p_ccb->target_ccb.nested_lock_os_int_cnt--;

	

 
	if (p_ccb->target_ccb.nested_lock_os_int_cnt == 0U) {
		set_pmr(pmr_setting_tbl[p_ccb->target_ccb.current_iintpri]);
	}
}






 
static void
x_suspend_lock_os_int(void)
{
	uint16	ispr;
	CCB		*p_ccb = get_my_p_ccb();

	ispr = get_ispr();

	if ((ispr & pmr_isr1_mask) == 0U) {
		

 
		if (p_ccb->target_ccb.nested_lock_os_int_cnt == 0U) {
			set_pmr(pmr_isr2_mask);    
		}
		p_ccb->target_ccb.nested_lock_os_int_cnt++;
		v850_memory_changed();
	}
}






 
static void
x_resume_unlock_os_int(void)
{
	uint16	ispr;
	CCB		*p_ccb = get_my_p_ccb();

	v850_memory_changed();
	ispr = get_ispr();

	if ((ispr & pmr_isr1_mask) == 0U) {
		

 
		do { if (!(p_ccb->target_ccb . nested_lock_os_int_cnt > 0U)) { fatal_file_name = "C:\\Users\\Sasuga\\Documents\\multicore\203v\203\215\203W\203F\203N\203g\\HSBRH850F1H176_BLSM_atk2_rccar_ccrh\\arch\\v850_gcc/prc_config.h"; fatal_line_num = 258; internal_shutdownallcores((27U)); } } while (0);
		p_ccb->target_ccb.nested_lock_os_int_cnt--;

		

 
		if (p_ccb->target_ccb.nested_lock_os_int_cnt == 0U) {
			set_pmr(pmr_setting_tbl[p_ccb->target_ccb.current_iintpri]);
		}
	}
}








 
static void
x_set_ipm(PriorityType intpri)
{
	CCB	*p_ccb = get_my_p_ccb();

	do { if (!(p_ccb->target_ccb . nested_lock_os_int_cnt > 0U)) { fatal_file_name = "C:\\Users\\Sasuga\\Documents\\multicore\203v\203\215\203W\203F\203N\203g\\HSBRH850F1H176_BLSM_atk2_rccar_ccrh\\arch\\v850_gcc/prc_config.h"; fatal_line_num = 283; internal_shutdownallcores((27U)); } } while (0);

	p_ccb->target_ccb.current_iintpri = ((uint32) (intpri + (16)));
}






 
static PriorityType
x_get_ipm(void)
{
	CCB	*p_ccb = get_my_p_ccb();

	do { if (!(p_ccb->target_ccb . nested_lock_os_int_cnt > 0U)) { fatal_file_name = "C:\\Users\\Sasuga\\Documents\\multicore\203v\203\215\203W\203F\203N\203g\\HSBRH850F1H176_BLSM_atk2_rccar_ccrh\\arch\\v850_gcc/prc_config.h"; fatal_line_num = 299; internal_shutdownallcores((27U)); } } while (0);
	return(((PriorityType) (p_ccb->target_ccb . current_iintpri - (16))));
}






 
static boolean
x_disable_int(InterruptNumberType intno)
{
	uint32 eic_address = (((intno) & 0xffffU) <= 31) ? (0xFFFEEA00 + (((intno) & 0xffffU) * 2)) : (0xFFFFB000 + 0x040 + ((((intno) & 0xffffU) - 32) * 2));

	if (!(((intno) & 0xffffU) <= (InterruptNumberType) ((350U)))) {
		return((0U));
	}

	 
	sil_wrh_mem((uint16 *) eic_address,
				sil_reh_mem((uint16 *) eic_address) | (0x01U << 7));

	return((1U));
}





 

static boolean
x_enable_int(InterruptNumberType intno)
{
	uint32 eic_address = (((intno) & 0xffffU) <= 31) ? (0xFFFEEA00 + (((intno) & 0xffffU) * 2)) : (0xFFFFB000 + 0x040 + ((((intno) & 0xffffU) - 32) * 2));

	if (!(((intno) & 0xffffU) <= (InterruptNumberType) ((350U)))) {
		return((0U));
	}

	 
	sil_wrh_mem((uint16 *) eic_address,
				sil_reh_mem((uint16 *) eic_address) & ~(0x01U << 7));

	return((1U));
}



 
static boolean
x_clear_int(InterruptNumberType intno)
{
	uint32 eic_address = (((intno) & 0xffffU) <= 31) ? (0xFFFEEA00 + (((intno) & 0xffffU) * 2)) : (0xFFFFB000 + 0x040 + ((((intno) & 0xffffU) - 32) * 2));

	if (!(((intno) & 0xffffU) <= (InterruptNumberType) ((350U)))) {
		return((0U));
	}

	 
	sil_wrh_mem((void *) eic_address,
				sil_reh_mem((void *) eic_address) & ~(0x01U << 12));

	return((1U));
}



 
static boolean
x_probe_int(InterruptNumberType intno)
{
	uint32	eic_address = (((intno) & 0xffffU) <= 31) ? (0xFFFEEA00 + (((intno) & 0xffffU) * 2)) : (0xFFFFB000 + 0x040 + ((((intno) & 0xffffU) - 32) * 2));
	uint16	tmp;

	if (!(((intno) & 0xffffU) <= (InterruptNumberType) ((350U)))) {
		return((0U));
	}

	tmp = sil_reh_mem((void *) (eic_address));
	return((tmp & 0x1000) != 0);
}



 
extern void x_config_int(InterruptNumberType intno, AttributeType intatr, PriorityType intpri, CoreIdType coreid);



 
static void
i_begin_int(InterruptNumberType intno)
{

}



 
static void
i_end_int(InterruptNumberType intno)
{

}



 
extern void default_int_handler(void);
extern void prc_hardware_initialize(void);



 
extern void prc_initialize(void);



 
extern void prc_terminate(void);



 






 
extern void dispatch(void);







 
extern void start_dispatch(void) ;





 
extern void exit_and_dispatch(void) ;



 
typedef struct task_context_block {
	void			*sp;         
	FunctionRefType	pc;          
} TSKCTXB;









 
extern void start_r(void);


extern void call_trusted_hook(void *hook, StatusType arg);
extern void exit_trusted_shutdown_hook(void) ;

 
extern void stack_change_and_call_func_1(void (*func)(StatusType ercd), uint32 arg1);
extern void stack_change_and_call_func_2(void (*func)(StatusType ercd, OSServiceIdType svcid), uint8 arg1, uint8 arg2);

 
extern void stack_monitoring_error_isr(void);



 
static boolean
x_sense_mcore(void)
{
	return(x_core_id() == 0);
}



 
extern volatile uint32 core_state_table[];




 
static boolean
x_start_core(CoreIdType coreid)
{
	boolean valid_core;

	if (coreid < (2U)) {
		core_state_table[coreid] = (0x87654321U);
		valid_core = (1U);
	}
	else {
		valid_core = (0U);
	}
	return(valid_core);
}



 
static boolean
is_halt(CoreIdType coreid)
{
	return(core_state_table[coreid] == 0U);
}





 




 
static void
x_initialize_tsk_lock(LockType *p_tsk_lock)
{
	*p_tsk_lock = 0;
}



 
static void
x_initialize_cnt_lock(LockType *p_cnt_lock)
{
	*p_cnt_lock = 0;
}



 
static void
x_initialize_spn_lock(LockType *p_spn_lock)
{
	*p_spn_lock = 0;
}



 
static void
x_initialize_ioc_lock(LockType *p_ioc_lock)
{
	*p_ioc_lock = 0;
}





 



 
static void
x_acquire_lock(LockType *p_lock)
{
	while (1) {
		if (acquire_lock_ldlstc(p_lock) != (0U)) {
			 
			v850_memory_changed();
			return;
		}
		 
		x_nested_unlock_os_int();
		x_nested_lock_os_int();
	}
}




 
static void
x_release_lock(LockType *p_lock)
{
	v850_memory_changed();
	release_lock_ldlstc(p_lock);
}



 
static boolean
x_try_lock(LockType *p_lock)
{
	if (acquire_lock_ldlstc(p_lock) != (0U)) {
		 
		v850_memory_changed();
		return((1U));
	}
	return((0U));
}





 
static void
x_initialize_spin(SpinlockIdType i, LockType *p_spn_lock)
{
	*p_spn_lock = 0;
}




 
extern boolean target_is_int_controllable(InterruptNumberType intno);






 























































 



 


 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 




 




 



 






 

 

 




 




 



 



 



 



 



 
extern const AppModeType tnum_appmode;


extern void p_inib_initialize(void);



 




 


extern void init_stack_magic_region(void);

extern void cancel_shutdown_hook(void);
extern void callevel_chk_shutdown(const CCB *p_ccb, StatusType ercd);

extern void call_protectionhk_main(StatusType ercd);



 
extern void object_initialize(void);



 
extern void object_terminate(void);



 
extern AppModeType			appmodeid;  



 
extern const MemorySizeType	_ostksz_table[];         
extern StackType * const	_ostk_table[];           
extern StackType * const	_ostkpt_table[];         




















 


static uint16
bitmap_search(uint16 bitmap)
{
	

 
	const uint8	bitmap_search_table[15] = {
		0U, 1U, 0U, 2U, 0U, 1U, 0U,
		3U, 0U, 1U, 0U, 2U, 0U, 1U, 0U
	};

	uint16		n = 0U;

	do { if (!(bitmap != 0U)) { fatal_file_name = "C:\\Users\\Sasuga\\Documents\\multicore\203v\203\215\203W\203F\203N\203g\\HSBRH850F1H176_BLSM_atk2_rccar_ccrh\\OBJ_BLSM\\configure\\kernel_impl.h"; fatal_line_num = 404; internal_shutdownallcores((27U)); } } while (0);
	if ((bitmap & 0x00ffU) == 0U) {
		bitmap >>= 8U;
		n += 8U;
	}
	if ((bitmap & 0x0fU) == 0U) {
		bitmap >>= 4U;
		n += 4U;
	}
	return(n + bitmap_search_table[(bitmap & 0x000fU) - 1U]);
}



int func1(int aaa){
	return 1+aaa;
}
int func2(int aaa){
	int temp;
	temp = func1(aaa+1);
	return temp;
}
int main()
{
    uint32 test=0;
    func2(test);
    acquire_lock_ldlstc(&test);
    enable_int();
    return 0;
}

