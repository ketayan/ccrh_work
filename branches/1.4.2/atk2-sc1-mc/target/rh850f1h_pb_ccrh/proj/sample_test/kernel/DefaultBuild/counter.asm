#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\kernel\alarm.c ..\..\..\kernel\counter.c ..\..\..\kernel\counter_manage.c ..\..\..\kernel\interrupt.c ..\..\..\kernel\interrupt_manage.c ..\..\..\kernel\osctl.c ..\..\..\kernel\osctl_manage.c ..\..\..\kernel\resource.c ..\..\..\kernel\scheduletable.c ..\..\..\kernel\task.c ..\..\..\kernel\task_manage.c ..\..\..\kernel\mc.c ..\..\..\kernel\mc_manage.c ..\..\..\kernel\spinlock.c ..\..\..\kernel\osap.c ..\..\..\arch\v850_gcc\prc_config.c ..\..\..\arch\v850_ccrh\prc_tool.c ..\..\..\target\rh850f1h_pb_ccrh\target_config.c ..\..\..\arch\v850_gcc\tauj_hw_counter.c ..\..\..\arch\v850_gcc\uart_rlin.c ..\..\..\arch\v850_ccrh\rh850_f1h.c ..\cfg\Ioc.c ..\..\..\target\rh850f1h_pb_ccrh\target_hw_counter.c -Xobj_path=DefaultBuild -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_z1a2dfrw.4pw
#@	compiled at Mon Jul 09 17:58:05 2018

	.file "..\..\..\kernel\counter.c"

	$reg_mode 32

	.extern _fatal_file_name
	.extern _fatal_line_num
	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _pmr_setting_tbl
	.extern _tnum_hardcounter
	.extern _tnum_counter
	.extern _cntinib_table
	.extern _p_cntcb_table
	.extern _hwcntinib_table
	.extern _v850_memory_changed
	.extern _internal_shutdownallcores
	.public _insert_cnt_expr_que
	.public _delete_cnt_expr_que
	.public _counter_initialize
	.public _counter_terminate
	.public _get_reltick
	.public _get_abstick
	.public _expire_process
	.extern _release_cnt_lock
	.extern _acquire_tsk_lock
	.extern _release_tsk_lock_and_dispatch
	.extern _acquire_cnt_lock

	.section .text, text
	.align 4
_insert_cnt_expr_que:
	.stack _insert_cnt_expr_que = 24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 101
	prepare 0x00000479, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 117
	ld.w 0x0000000C[r7], r5
	mov r7, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 107
	ld.w 0x00000008[r6], r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 120
	ld.w 0x00000000[r7], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 122
	cmp r20, r5
	bnc9 .BB.LABEL.1_5
.BB.LABEL.1_1:	; bb23.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	cmp r21, r8
	bz9 .BB.LABEL.1_9
.BB.LABEL.1_2:	; bb29
	ld.w 0x00000008[r8], r7
	cmp r7, r5
	bh9 .BB.LABEL.1_9
.BB.LABEL.1_3:	; bb29
	cmp r20, r7
	bh9 .BB.LABEL.1_9
.BB.LABEL.1_4:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 127
	ld.w 0x00000000[r8], r8
	br9 .BB.LABEL.1_1
.BB.LABEL.1_5:	; bb64.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	cmp r21, r8
	bz9 .BB.LABEL.1_9
.BB.LABEL.1_6:	; bb70
	ld.w 0x00000008[r8], r7
	cmp r7, r5
	bnh9 .BB.LABEL.1_8
.BB.LABEL.1_7:	; bb70
	cmp r20, r7
	bh9 .BB.LABEL.1_9
.BB.LABEL.1_8:	; bb60
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 135
	ld.w 0x00000000[r8], r8
	br9 .BB.LABEL.1_5
.BB.LABEL.1_9:	; if_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 98
	ld.w 0x00000004[r8], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 146
	movhi HIGHW1(#_tnum_hardcounter), r0, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 99
	st.w r8, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 145
	mov #_cntinib_table, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 98
	st.w r5, 0x00000004[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 100
	ld.w 0x00000004[r8], r5
	mov 0x2AAAAAAB, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 146
	ld.w LOWW(#_tnum_hardcounter)[r10], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 100
	st.w r6, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 101
	st.w r6, 0x00000004[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 145
	ld.w 0x00000008[r21], r5
	sub r7, r5
	mul r9, r5, r5
	mov r5, r22
	sar 0x00000002, r22
	shr 0x0000001F, r5
	add r5, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 146
	cmp r10, r22
	bnc9 .BB.LABEL.1_15
.BB.LABEL.1_10:	; bb115
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000000[r21], r5
	cmp r5, r6
	bnz9 .BB.LABEL.1_15
.BB.LABEL.1_11:	; if_then_bb132
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 149
	mov r22, r23
	mul 0x00000030, r23, r0
	mov #_hwcntinib_table, r24
	add r24, r23
	ld.w 0x00000014[r23], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 152
	ld.w 0x0000000C[r23], r5
	mov r20, r6
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 164
	ld.w 0x00000010[r23], r5
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 153
	st.b r6, 0x00000011[r21]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 164
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 239
	mov r10, r5
	sub r20, r5
	cmp r20, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 149
	movea 0x00000014, r23, r6
	bnc9 .BB.LABEL.1_13
.BB.LABEL.1_12:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000008[r21], r7
	ld.w 0x00000004[r7], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 248
	add r7, r5
	add 0x00000001, r5
.BB.LABEL.1_13:	; diff_tick.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000008[r21], r7
	ld.w 0x00000000[r7], r7
	cmp r7, r5
	bh9 .BB.LABEL.1_15
.BB.LABEL.1_14:	; if_then_bb164
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 167
	ld.w 0x00000000[r6], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 169
	mul 0x00000030, r22, r0
	add r22, r24
	ld.w 0x00000018[r24], r5
	jarl [r5], r31
.BB.LABEL.1_15:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 172
	dispose 0x00000000, 0x00000479, [r31]
	.align 4
_delete_cnt_expr_que:
	.stack _delete_cnt_expr_que = 24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 182
	prepare 0x00000479, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	ld.w 0x00000000[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 198
	movhi HIGHW1(#_tnum_hardcounter), r0, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	ld.w 0x00000004[r6], r8
	mov r7, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 188
	ld.w 0x00000000[r7], r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 197
	mov #_cntinib_table, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	st.w r5, 0x00000000[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 123
	ld.w 0x00000004[r6], r5
	ld.w 0x00000000[r6], r8
	mov 0x2AAAAAAB, r11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 198
	ld.w LOWW(#_tnum_hardcounter)[r12], r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 123
	st.w r5, 0x00000004[r8]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r6, 0x00000004[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r6, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 197
	ld.w 0x00000008[r7], r5
	sub r10, r5
	mul r11, r5, r5
	mov r5, r21
	sar 0x00000002, r21
	shr 0x0000001F, r5
	add r5, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 198
	cmp r12, r21
	bnc9 .BB.LABEL.2_7
.BB.LABEL.2_1:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000000[r20], r5
	cmp r5, r9
	bz9 .BB.LABEL.2_7
.BB.LABEL.2_2:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 201
	mov r21, r22
	mul 0x00000030, r22, r0
	mov #_hwcntinib_table, r23
	add r23, r22
	ld.w 0x00000014[r22], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 204
	ld.w 0x00000020[r22], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 147
	ld.w 0x00000000[r20], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 201
	movea 0x00000014, r22, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 147
	cmp r20, r5
	bz9 .BB.LABEL.2_7
.BB.LABEL.2_3:	; if_then_bb50
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 213
	mov r21, r24
	mul 0x00000030, r24, r0
	ld.w 0x00000008[r5], r6
	add r23, r24
	ld.w 0x0000000C[r24], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 224
	ld.w 0x00000010[r24], r5
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 214
	st.b r6, 0x00000011[r20]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 224
	jarl [r5], r31
	ld.w 0x00000000[r20], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 239
	mov r10, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 224
	ld.w 0x00000008[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 239
	sub r5, r6
	cmp r5, r10
	bnc9 .BB.LABEL.2_5
.BB.LABEL.2_4:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000008[r20], r5
	ld.w 0x00000004[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 248
	add r5, r6
	add 0x00000001, r6
.BB.LABEL.2_5:	; diff_tick.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000008[r20], r5
	ld.w 0x00000000[r5], r5
	cmp r5, r6
	bh9 .BB.LABEL.2_7
.BB.LABEL.2_6:	; if_then_bb90
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 228
	ld.w 0x00000000[r22], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 230
	mul 0x00000030, r21, r0
	add r21, r23
	ld.w 0x00000018[r23], r5
	jarl [r5], r31
.BB.LABEL.2_7:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 235
	dispose 0x00000000, 0x00000479, [r31]
	.align 4
_counter_initialize:
	.stack _counter_initialize = 20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 245
	prepare 0x00000079, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 251
	movhi HIGHW1(#_tnum_counter), r0, r5
	ld.w LOWW(#_tnum_counter)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	add 0xFFFFFFFF, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 251
	cmp 0x00000000, r5
	bz9 .BB.LABEL.3_5
.BB.LABEL.3_1:	; entry.bb_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	mov #_cntinib_table, r5
	movea 0x00000010, r5, r5
	mov 0x00000000, r6
	mov #_p_cntcb_table, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 251
	movhi HIGHW1(#_tnum_counter), r0, r8
.BB.LABEL.3_2:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 252
	ld.bu 0x00000000[r5], r9
	andi 0x000000FF, r20, r10
	cmp r9, r10
	bnz9 .BB.LABEL.3_4
.BB.LABEL.3_3:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 253
	ld.w 0x00000000[r7], r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 252
	addi 0xFFFFFFF0, r5, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 254
	st.w r10, 0x00000008[r9]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 255
	st.w r0, 0x0000000C[r9]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r9, 0x00000004[r9]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r9, 0x00000000[r9]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 257
	st.b r0, 0x00000010[r9]
.BB.LABEL.3_4:	; bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 251
	ld.w LOWW(#_tnum_counter)[r8], r9
	add 0x00000001, r6
	add 0x00000004, r7
	movea 0x00000018, r5, r5
	cmp r9, r6
	bl9 .BB.LABEL.3_2
.BB.LABEL.3_5:	; bb66.loopexit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 261
	movhi HIGHW1(#_tnum_hardcounter), r0, r5
	ld.w LOWW(#_tnum_hardcounter)[r5], r21
	cmp 0x00000000, r21
	bz9 .BB.LABEL.3_10
.BB.LABEL.3_6:	; bb66.loopexit.bb35_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	mov #_cntinib_table, r22
	add 0x00000004, r22
	mov #_hwcntinib_table, r23
.BB.LABEL.3_7:	; bb35
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 262
	ld.bu 0x0000002C[r23], r5
	andi 0x000000FF, r20, r6
	cmp r5, r6
	bnz9 .BB.LABEL.3_9
.BB.LABEL.3_8:	; if_then_bb46
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 263
	ld.w 0x00000028[r23], r7
	ld.w 0x00000000[r22], r6
	ld.w 0x00000000[r23], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 265
	ld.w 0x00000004[r23], r5
	jarl [r5], r31
.BB.LABEL.3_9:	; bb66
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 261
	movea 0x00000030, r23, r23
	movea 0x00000018, r22, r22
	loop r21, .BB.LABEL.3_7
.BB.LABEL.3_10:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 269
	dispose 0x00000000, 0x00000079, [r31]
	.align 4
_counter_terminate:
	.stack _counter_terminate = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 279
	prepare 0x00000061, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 283
	movhi HIGHW1(#_tnum_hardcounter), r0, r5
	ld.w LOWW(#_tnum_hardcounter)[r5], r20
	cmp 0x00000000, r20
	bz9 .BB.LABEL.4_3
.BB.LABEL.4_1:	; entry.bb_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	mov #_hwcntinib_table, r21
	add 0x00000008, r21
.BB.LABEL.4_2:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 284
	ld.w 0x00000000[r21], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 283
	movea 0x00000030, r21, r21
	loop r20, .BB.LABEL.4_2
.BB.LABEL.4_3:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 286
	dispose 0x00000000, 0x00000061, [r31]
	.align 4
_get_reltick:
	.stack _get_reltick = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 299
	prepare 0x00000061, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 305
	ld.w 0x00000008[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	movhi HIGHW1(#_tnum_hardcounter), r0, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 305
	mov #_cntinib_table, r8
	mov 0x2AAAAAAB, r9
	sub r8, r5
	mov r6, r21
	mul r9, r5, r5
	mov r7, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	ld.w LOWW(#_tnum_hardcounter)[r10], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 305
	mov r5, r6
	sar 0x00000002, r6
	shr 0x0000001F, r5
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	cmp r10, r6
	bnc9 .BB.LABEL.5_2
.BB.LABEL.5_1:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 265
	mul 0x00000030, r6, r0
	mov #_hwcntinib_table, r5
	add r6, r5
	ld.w 0x00000010[r5], r5
	jarl [r5], r31
	br9 .BB.LABEL.5_3
.BB.LABEL.5_2:	; if_else_bb.i2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 268
	ld.w 0x0000000C[r21], r10
.BB.LABEL.5_3:	; get_curval.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 310
	ld.w 0x00000008[r21], r5
	ld.w 0x00000004[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 217
	mov r5, r6
	sub r10, r6
	add r20, r10
	cmp r20, r6
	bnc9 .BB.LABEL.5_5
.BB.LABEL.5_4:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 226
	add 0xFFFFFFFF, r10
	sub r5, r10
.BB.LABEL.5_5:	; add_tick.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 313
	dispose 0x00000000, 0x00000061, [r31]
	.align 4
_get_abstick:
	.stack _get_abstick = 12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 326
	prepare 0x00000061, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 333
	ld.w 0x00000008[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	movhi HIGHW1(#_tnum_hardcounter), r0, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 333
	mov #_cntinib_table, r8
	mov 0x2AAAAAAB, r9
	sub r8, r5
	mov r6, r21
	mul r9, r5, r5
	mov r7, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	ld.w LOWW(#_tnum_hardcounter)[r10], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 333
	mov r5, r6
	sar 0x00000002, r6
	shr 0x0000001F, r5
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	cmp r10, r6
	bnc9 .BB.LABEL.6_2
.BB.LABEL.6_1:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 265
	mul 0x00000030, r6, r0
	mov #_hwcntinib_table, r5
	add r6, r5
	ld.w 0x00000010[r5], r5
	jarl [r5], r31
	br9 .BB.LABEL.6_3
.BB.LABEL.6_2:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 268
	ld.w 0x0000000C[r21], r10
.BB.LABEL.6_3:	; get_curval.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 338
	ld.w 0x00000008[r21], r5
	mov r20, r6
	ld.w 0x00000000[r5], r5
	add r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 340
	add 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 338
	add 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 340
	cmp r5, r10
	bnc9 .BB.LABEL.6_5
.BB.LABEL.6_4:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 345
	cmp r20, r10
	cmov 0x00000001, r20, r6, r10
	dispose 0x00000000, 0x00000061, [r31]
.BB.LABEL.6_5:	; if_else_bb40
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 357
	cmp r10, r6
	cmov 0x0000000B, r6, r20, r10
	dispose 0x00000000, 0x00000061, [r31]
	.align 4
_expire_process:
	.stack _expire_process = 48
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 376
	prepare 0x00000FFF, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 381
	ld.w 0x00000008[r6], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	movhi HIGHW1(#_tnum_hardcounter), r0, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 381
	mov #_p_ccb_table, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	ld.w LOWW(#_tnum_hardcounter)[r8], r21
	mov r7, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 381
	ld.bu 0x00000010[r5], r5
	mov r6, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 383
	st.b r0, 0x00000011[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 381
	shl 0x00000002, r5
	add r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	cmp r7, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 381
	ld.w 0x00000000[r5], r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	bnh9 .BB.LABEL.7_2
.BB.LABEL.7_1:	; if_then_bb.i18
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 265
	mov r22, r5
	mul 0x00000030, r5, r0
	mov #_hwcntinib_table, r6
	add r5, r6
	ld.w 0x00000010[r6], r5
	jarl [r5], r31
	br9 .BB.LABEL.7_3
.BB.LABEL.7_2:	; if_else_bb.i21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 268
	ld.w 0x0000000C[r23], r10
.BB.LABEL.7_3:	; bb129.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 147
	ld.w 0x00000000[r23], r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 148
	cmp r23, r25
	bz17 .BB.LABEL.7_27
.BB.LABEL.7_4:	; bb.nph
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	mov r22, r5
	mul 0x00000030, r5, r0
	mov #_hwcntinib_table, r26
	add r5, r26
	movea 0x000000CA, r0, r28
	movea 0x00000010, r26, r27
	add 0x0000000C, r26
	movea 0x0000001B, r0, r29
	mov 0x00000001, r30
.BB.LABEL.7_5:	; bb136
	ld.w 0x00000008[r25], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 239
	mov r10, r6
	sub r5, r6
	cmp r5, r10
	bnc9 .BB.LABEL.7_7
.BB.LABEL.7_6:	; if_else_bb.i9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000008[r23], r5
	ld.w 0x00000004[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 248
	add r5, r6
	add 0x00000001, r6
.BB.LABEL.7_7:	; diff_tick.1.exit11
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000008[r23], r5
	ld.w 0x00000000[r5], r5
	cmp r5, r6
	bh17 .BB.LABEL.7_27
.BB.LABEL.7_8:	; bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	ld.w 0x00000000[r25], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 402
	movhi HIGHW1(#_tnum_hardcounter), r0, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	ld.w 0x00000004[r25], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 402
	ld.w LOWW(#_tnum_hardcounter)[r21], r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 122
	st.w r5, 0x00000000[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 123
	ld.w 0x00000004[r25], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 402
	cmp r21, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 123
	ld.w 0x00000000[r25], r6
	st.w r5, 0x00000004[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 88
	st.w r25, 0x00000004[r25]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 89
	st.w r25, 0x00000000[r25]
	bnc9 .BB.LABEL.7_14
.BB.LABEL.7_9:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 407
	ld.w 0x00000008[r25], r5
	st.w r5, 0x0000000C[r23]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 147
	ld.w 0x00000000[r23], r5
	cmp r5, r23
	bz9 .BB.LABEL.7_14
.BB.LABEL.7_10:	; bb43
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000000[r27], r5
	jarl [r5], r31
	ld.w 0x00000000[r23], r5
	ld.w 0x00000008[r5], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 239
	mov r10, r5
	sub r6, r5
	cmp r6, r10
	bnc9 .BB.LABEL.7_12
.BB.LABEL.7_11:	; if_else_bb.i3
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000008[r23], r7
	ld.w 0x00000004[r7], r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 248
	add r7, r5
	add 0x00000001, r5
.BB.LABEL.7_12:	; diff_tick.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.w 0x00000008[r23], r7
	ld.w 0x00000000[r7], r7
	cmp r7, r5
	bnh9 .BB.LABEL.7_14
.BB.LABEL.7_13:	; if_then_bb76
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 416
	ld.w 0x00000000[r26], r5
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 417
	st.b r30, 0x00000011[r23]
.BB.LABEL.7_14:	; if_break_bb90
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 421
	ld.w 0x0000000C[r25], r5
	mov r25, r6
	mov r23, r7
	jarl [r5], r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 427
	mov r24, r6
	jarl _release_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 428
	mov r24, r6
	jarl _acquire_tsk_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 429
	ld.w 0x00000010[r24], r25
	ld.w 0x00000014[r24], r5
	cmp r5, r25
	bz9 .BB.LABEL.7_16
.BB.LABEL.7_15:	; bb106
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	ld.hu 0x00000004[r24], r25
	mov r30, r7
	cmp 0x00000001, r25
	bz9 .BB.LABEL.7_17
.BB.LABEL.7_16:	; if_else_bb
	mov 0x00000000, r7
.BB.LABEL.7_17:	; if_break_bb122
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 435
	mov r24, r6
	jarl _release_tsk_lock_and_dispatch, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r25, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r25
	andi 0x00000003, r25, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r25, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r25, r25
	shl 0x00000002, r25
	add r20, r25
	ld.w 0x00000000[r25], r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 201
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 202
	ld.bu 0x000000D5[r25], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.7_19
.BB.LABEL.7_18:	; if_then_bb.i51
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	mov #.STR.1, r5
	movhi HIGHW1(#_fatal_file_name), r0, r6
	st.w r5, LOWW(#_fatal_file_name)[r6]
	movhi HIGHW1(#_fatal_line_num), r0, r5
	st.w r28, LOWW(#_fatal_line_num)[r5]
	mov r29, r6
	jarl _internal_shutdownallcores, r31
.BB.LABEL.7_19:	; bb14.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	ld.bu 0x000000D5[r25], r5
	addi 0xFFFFFFFF, r5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 208
	cmp 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 203
	st.b r6, 0x000000D5[r25]
	bnz9 .BB.LABEL.7_21
.BB.LABEL.7_20:	; if_then_bb28.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 209
	ld.bu 0x000000D4[r25], r25
	mov #_pmr_setting_tbl, r5
	add r25, r25
	add r5, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu 0x00000000[r25], r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r5, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r25, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r5, 5
.BB.LABEL.7_21:	; x_nested_unlock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r25, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r25
	andi 0x00000003, r25, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r25, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r25, r25
	shl 0x00000002, r25
	add r20, r25
	ld.w 0x00000000[r25], r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r25], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.7_23
.BB.LABEL.7_22:	; if_then_bb.i36
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r6, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r5, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r6, 5
.BB.LABEL.7_23:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r25], r5
	add 0x00000001, r5
	st.b r5, 0x000000D5[r25]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 443
	mov r24, r6
	jarl _acquire_cnt_lock, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 264
	cmp r22, r21
	bnh9 .BB.LABEL.7_25
.BB.LABEL.7_24:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 265
	ld.w 0x00000000[r27], r25
	jarl [r25], r31
	br9 .BB.LABEL.7_26
.BB.LABEL.7_25:	; if_else_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.h", 268
	ld.w 0x0000000C[r23], r10
.BB.LABEL.7_26:	; bb129
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/include/queue.h", 147
	ld.w 0x00000000[r23], r25
	cmp r23, r25
	bnz17 .BB.LABEL.7_5
.BB.LABEL.7_27:	; bb166
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 448
	cmp r22, r21
	bnh9 .BB.LABEL.7_31
.BB.LABEL.7_28:	; bb166
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 0
	cmp r23, r25
	bz9 .BB.LABEL.7_31
.BB.LABEL.7_29:	; bb185
	ld.bu 0x00000011[r23], r5
	cmp 0x00000000, r5
	bnz9 .BB.LABEL.7_31
.BB.LABEL.7_30:	; if_then_bb200
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 449
	mul 0x00000030, r22, r0
	mov #_hwcntinib_table, r5
	ld.w 0x00000008[r25], r6
	add r22, r5
	ld.w 0x0000000C[r5], r5
	jarl [r5], r31
.BB.LABEL.7_31:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/counter.c", 451
	dispose 0x00000000, 0x00000FFF, [r31]
	.section .const, const
.STR.1:
	.db 0x43,0x3A,0x5C,0x55,0x73,0x65,0x72,0x73,0x5C,0x53,0x61,0x73,0x75,0x67,0x61,0x5C
	.db 0x44,0x6F,0x63,0x75,0x6D,0x65,0x6E,0x74,0x73,0x5C,0x6D,0x75,0x6C,0x74,0x69,0x63
	.db 0x6F,0x72,0x65,0x83,0x76,0x83,0x8D,0x83,0x57,0x83,0x46,0x83,0x4E,0x83,0x67,0x5C
	.db 0x52,0x43,0x43,0x41,0x52,0x5F,0x53,0x48,0x4F,0x52,0x54,0x5C,0x61,0x72,0x63,0x68
	.db 0x5C,0x76,0x38,0x35,0x30,0x5F,0x67,0x63,0x63,0x2F,0x70,0x72,0x63,0x5F,0x63,0x6F
	.db 0x6E,0x66,0x69,0x67,0x2E,0x68
	.ds (1)
