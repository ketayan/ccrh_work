#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\static_barrier.c -oDefaultBuild\static_barrier.obj -Xcommon=rh850 -Xcpu=g3m -Xreserve_r2 -g -g_line -Ospeed -Oinline_size -I.. -I..\cfg -I..\..\.. -I..\..\..\arch -I..\..\..\include -I..\..\..\kernel -I..\..\..\arch -I..\..\..\sysmod -I..\..\..\library -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\target\rh850f1h_pb_gcc -I..\..\..\arch\v850_ccrh -I..\..\..\arch\ccrh -I.. -I..\..\..\arch\logtrace -I..\..\..\include -I..\blsm -DALLFUNC,TOPPERS_LABEL_ASM -Xalign4 -Xasm_path=DefaultBuild -Xno_warning=20177 -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_1ilyimrq.b1x
#@	compiled at Wed Aug 08 17:05:38 2018

	.file "..\..\..\static_barrier.c"

	$reg_mode 32

	.extern _p_ccb_table
	.extern _pmr_isr2_mask
	.extern _activated_cores
	.extern _shutdown_reqflg
	.extern _v850_memory_changed
	.public _barrier_sync
	.extern _busy_wait
	.extern _internal_shutdownallcores
	.public _main

	.section .text, text
	.align 4
_x_core_id.1:
	.stack _x_core_id.1 = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	andi 0x00000003, r5, r5
	add 0xFFFFFFFF, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r10
	jmp [r31]
	.align 4
_barrier_sync:
	.stack _barrier_sync = 16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 7
	prepare 0x00000071, 0x00000000
	mov r6, r20
	mov r7, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 173
	stsr 0x00000000, r5, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	shr 0x00000010, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	mov #_core_init.1.barrier_sync, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 174
	andi 0x00000003, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 175
	movea 0x000000FF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_ccb.h", 116
	andi 0x000000FF, r5, r5
	add r22, r5
	st.b r20, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 491
	jarl _x_core_id.1, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 492
	cmp 0x00000000, r10
	bnz9 .BB.LABEL.2_15
.BB.LABEL.2_1:	; bb28.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 0
	cmp 0x00000000, r21
	bz9 .BB.LABEL.2_6
.BB.LABEL.2_2:	; bb28.preheader.bb28_crit_edge
	mov 0x00000000, r21
	mov r21, r5
.BB.LABEL.2_3:	; bb28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 20
	andi 0x000000FF, r5, r6
	cmp 0x00000001, r6
	bgt9 .BB.LABEL.2_9
.BB.LABEL.2_4:	; bb12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 21
	add r22, r6
	ld.bu 0x00000000[r6], r6
	cmp r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 22
	adf 0x00000002, r0, r21, r21
	add 0x00000001, r5
	br9 .BB.LABEL.2_3
.BB.LABEL.2_5:	; bb34.us
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 25
	jarl _busy_wait, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 18
	movhi HIGHW1(#_activated_cores), r0, r5
	ld.w LOWW(#_activated_cores)[r5], r5
	andi 0x000000FF, r21, r21
	cmp r5, r21
	bnc9 .BB.LABEL.2_14
.BB.LABEL.2_6:	; bb28.preheader.bb28.us_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 0
	mov 0x00000000, r21
	mov r21, r5
.BB.LABEL.2_7:	; bb28.us
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 20
	andi 0x000000FF, r5, r6
	cmp 0x00000001, r6
	bgt9 .BB.LABEL.2_5
.BB.LABEL.2_8:	; bb12.us
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 21
	add r22, r6
	ld.bu 0x00000000[r6], r6
	cmp r6, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 22
	adf 0x00000002, r0, r21, r21
	add 0x00000001, r5
	br9 .BB.LABEL.2_7
.BB.LABEL.2_9:	; bb34
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 25
	jarl _busy_wait, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 26
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.2_13
.BB.LABEL.2_10:	; if_then_bb52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 246
	jarl _x_core_id.1, r31
	shl 0x00000002, r10
	mov #_p_ccb_table, r5
	add r10, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.2_12
.BB.LABEL.2_11:	; if_then_bb.i14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.2_12:	; x_nested_lock_os_int.1.exit17
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 33
	jarl _internal_shutdownallcores, r31
.BB.LABEL.2_13:	; if_break_bb53
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 18
	movhi HIGHW1(#_activated_cores), r0, r5
	ld.w LOWW(#_activated_cores)[r5], r5
	andi 0x000000FF, r21, r21
	cmp r5, r21
	bl9 .BB.LABEL.2_2
.BB.LABEL.2_14:	; bb60.split
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 36
	movhi HIGHW1(#_sys_start.2.barrier_sync), r0, r21
	st.b r20, LOWW(#_sys_start.2.barrier_sync)[r21]
	dispose 0x00000000, 0x00000071, [r31]
.BB.LABEL.2_15:	; bb83.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 39
	movhi HIGHW1(#_sys_start.2.barrier_sync), r0, r5
	ld.bu LOWW(#_sys_start.2.barrier_sync)[r5], r5
	cmp r5, r20
	bz9 .BB.LABEL.2_23
.BB.LABEL.2_16:	; bb.nph
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 0
	cmp 0x00000000, r21
	bnz9 .BB.LABEL.2_18
.BB.LABEL.2_17:	; bb62.us
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 40
	jarl _busy_wait, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 39
	movhi HIGHW1(#_sys_start.2.barrier_sync), r0, r5
	ld.bu LOWW(#_sys_start.2.barrier_sync)[r5], r5
	cmp r5, r20
	bnz9 .BB.LABEL.2_17
	br9 .BB.LABEL.2_23
.BB.LABEL.2_18:	; bb62
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 40
	jarl _busy_wait, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 41
	movhi HIGHW1(#_shutdown_reqflg), r0, r5
	ld.bu LOWW(#_shutdown_reqflg)[r5], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.2_22
.BB.LABEL.2_19:	; if_then_bb81
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/kernel/ccb.h", 246
	jarl _x_core_id.1, r31
	shl 0x00000002, r10
	mov #_p_ccb_table, r5
	add r10, r5
	ld.w 0x00000000[r5], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 182
	ld.bu 0x000000D5[r5], r6
	cmp 0x00000000, r6
	bnz9 .BB.LABEL.2_21
.BB.LABEL.2_20:	; if_then_bb.i
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 183
	movhi HIGHW1(#_pmr_isr2_mask), r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ld.hu LOWW(#_pmr_isr2_mask)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 97
	stsr 0x00000005, r7, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 82
	di
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 118
	ldsr r6, 0x0000000B, 0x00000002
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 103
	ldsr r7, 5
.BB.LABEL.2_21:	; x_nested_lock_os_int.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 185
	ld.b 0x000000D5[r5], r6
	add 0x00000001, r6
	st.b r6, 0x000000D5[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_gcc/prc_config.h", 187
	jarl _v850_memory_changed, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_insn.h", 88
	ei
	movea 0x00000021, r0, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 48
	jarl _internal_shutdownallcores, r31
.BB.LABEL.2_22:	; bb83
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 39
	movhi HIGHW1(#_sys_start.2.barrier_sync), r0, r5
	ld.bu LOWW(#_sys_start.2.barrier_sync)[r5], r5
	cmp r5, r20
	bnz9 .BB.LABEL.2_18
.BB.LABEL.2_23:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 52
	dispose 0x00000000, 0x00000071, [r31]
	.align 4
_main:
	.stack _main = 8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 54
	prepare 0x00000041, 0x00000000
	mov 0x00000000, r20
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 55
	mov r20, r7
	jarl _barrier_sync, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static_barrier.c", 56
	mov r20, r10
	dispose 0x00000000, 0x00000041, [r31]
	.section .bss, bss
_sys_start.2.barrier_sync:
	.ds (1)
	.section .data, data
_core_init.1.barrier_sync:
	.ds (2)
