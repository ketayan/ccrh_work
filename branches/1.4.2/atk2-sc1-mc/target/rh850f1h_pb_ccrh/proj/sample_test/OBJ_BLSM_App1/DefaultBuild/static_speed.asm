#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\static.c -oDefaultBuild\static.obj -Xcommon=rh850 -Xcpu=g3m -g -g_line -Ospeed -Oinline_size -I..\cfg -I..\..\..\arch\v850_ccrh -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\arch\ccrh -I..\..\..\arch\v850_gcc -I..\..\..\include -I..\..\..\arch -I..\..\..\arch\ccrh -I..\..\..\arch\gcc -I..\..\..\arch\logtrace -I..\..\..\arch\v850_ccrh -I..\..\..\arch\v850_gcc -I..\..\..\arch\v850_ghs -I..\..\..\arch\ccrh\configure -I..\..\..\sysmod -I..\..\.. -I..\..\..\cfg\cfg -I.. -I..\blsm -I..\sci -I..\cfg -Xalign4 -Xasm_path=DefaultBuild -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_jojaedst.bss
#@	compiled at Wed Aug 08 10:20:56 2018

	.file "..\..\..\static.c"

	$reg_mode 32

	.public _func
	.public _main

	.section .text, text
	.align 4
_func:
	.stack _func = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static.c", 4
	movhi HIGHW1(#_a.1.func), r0, r2
.BB.LABEL.1_1:	; bb1
	ld.w LOWW(#_a.1.func)[r2], r5
	cmp 0x00000000, r5
	bz9 .BB.LABEL.1_1
.BB.LABEL.1_2:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static.c", 6
	jmp [r31]
	.align 4
_main:
	.stack _main = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static.c", 8
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static.c", 9
	jarl _func, r31
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/static.c", 10
	dispose 0x00000000, 0x00000001, [r31]
	.section .data, data
	.align 4
_a.1.func:
	.ds (4)
