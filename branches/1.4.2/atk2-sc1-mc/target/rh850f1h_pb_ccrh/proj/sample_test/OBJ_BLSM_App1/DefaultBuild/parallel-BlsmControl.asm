#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\blsm\parallel-BlsmControl.c -oDefaultBuild\parallel-BlsmControl.obj -Xcommon=rh850 -Xcpu=g3m -g -g_line -Ospeed -Oinline_size -I..\cfg -I..\..\..\arch\v850_ccrh -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\arch\ccrh -I..\..\..\arch\v850_gcc -I..\..\..\include -I..\..\..\arch -I..\..\..\arch\ccrh -I..\..\..\arch\gcc -I..\..\..\arch\logtrace -I..\..\..\arch\v850_ccrh -I..\..\..\arch\v850_gcc -I..\..\..\arch\v850_ghs -I..\..\..\arch\ccrh\configure -I..\..\..\sysmod -I..\..\.. -I..\..\..\cfg\cfg -I.. -I..\blsm -I..\sci -I..\cfg -Xalign4 -Xasm_path=DefaultBuild -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_tfj34zgi.kiw
#@	compiled at Wed Jul 11 18:05:58 2018

	.file "..\blsm\parallel-BlsmControl.c"

	$reg_mode 32

	.public _blsm_speed, 2
	.public _CH_BlsmControl_0002_0088, 12
	.public _CH_BlsmControl_0015_0076, 16
	.public _CH_BlsmControl_0036_0038, 16
	.public _CH_BlsmControl_0037_0072, 12
	.public _CH_BlsmControl_0038_0035, 12
	.public _CH_BlsmControl_0039_0037, 16
	.public _CH_BlsmControl_0043_0077, 16
	.public _TaskMainBlsmControl_Core0_1_0_Task
	.extern _SetRelAlarm
	.extern _SetEvent
	.extern _WaitEvent
	.extern _ClearEvent
	.public _TaskMainBlsmControl_Core1_1_0_Task
	.extern _fabs
	.public _TaskMainBlsmControl_Core1_100_0_Task

	.section .text, text
	.align 4
_TaskMainBlsmControl_Core0_1_0_Task:
	.stack _TaskMainBlsmControl_Core0_1_0_Task = 628
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 1777
	prepare 0x000007FF, 0x0000007C
	movea 0xFFFFFE34, r3, r3
	mov 0x3EE4F8B5, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2083
	st.w r6, 0x000000E4[r3]
	mov 0x88E368F1, r6
	st.w r6, 0x000000E0[r3]
	movhi 0x00003FF0, r0, r6
	st.w r6, 0x000000EC[r3]
	mov 0x40B38800, r5
	st.w r5, 0x000000D4[r3]
	st.w r5, 0x000000F4[r3]
	movhi 0x00004059, r0, r5
	mov 0x40F86A00, r7
	st.w r7, 0x0000010C[r3]
	st.w r5, 0x00000104[r3]
	mov 0x40B38900, r2
	mov 0x407F4000, r8
	st.w r7, 0x0000011C[r3]
	st.w r2, 0x000000C4[r3]
	mov 0x408F4000, r9
	st.w r0, 0x000000C0[r3]
	st.w r2, 0x000000CC[r3]
	st.w r7, 0x0000012C[r3]
	st.w r0, 0x000000C8[r3]
	st.w r0, 0x000000D0[r3]
	st.w r2, 0x000000DC[r3]
	movhi 0x00004034, r0, r7
	st.w r7, 0x0000013C[r3]
	st.w r0, 0x000000D8[r3]
	st.w r0, 0x000000E8[r3]
	st.w r0, 0x000000F0[r3]
	movhi 0x00004030, r0, r7
	st.w r7, 0x0000014C[r3]
	st.w r0, 0x000000FC[r3]
	mov 0x40D38800, r7
	st.w r7, 0x00000154[r3]
	st.w r0, 0x000000F8[r3]
	st.w r6, 0x0000015C[r3]
	st.w r0, 0x00000100[r3]
	st.w r0, 0x00000108[r3]
	st.w r8, 0x00000114[r3]
	mov 0x3FC99999, r7
	st.w r7, 0x0000016C[r3]
	mov 0x9999999A, r7
	st.w r7, 0x00000168[r3]
	st.w r6, 0x00000174[r3]
	st.w r0, 0x00000110[r3]
	st.w r0, 0x00000118[r3]
	st.w r9, 0x00000124[r3]
	st.w r6, 0x00000184[r3]
	st.w r0, 0x00000120[r3]
	mov 0x3FC5C28F, r7
	st.w r7, 0x0000018C[r3]
	mov 0x5C28F5C3, r7
	st.w r0, 0x00000128[r3]
	st.w r5, 0x00000194[r3]
	st.w r9, 0x00000134[r3]
	st.w r6, 0x0000019C[r3]
	st.w r0, 0x00000130[r3]
	mov 0x3FC33333, r5
	st.w r5, 0x000001A4[r3]
	mov 0x33333333, r5
	st.w r5, 0x000001A0[r3]
	st.w r6, 0x000001AC[r3]
	st.w r0, 0x00000138[r3]
	st.w r9, 0x00000144[r3]
	st.w r0, 0x00000140[r3]
	movea 0x00000024, r0, r5
	movea 0x000001A0, r3, r6
	st.w r0, 0x00000148[r3]
	st.w r6, 0x000000BC[r3]
	movea 0x00000198, r3, r6
	st.w r6, 0x000000B8[r3]
	movea 0x00000188, r3, r6
	st.w r6, 0x000000B4[r3]
	movea 0x00000180, r3, r6
	st.w r6, 0x000000B0[r3]
	movea 0x00000170, r3, r6
	st.w r6, 0x000000AC[r3]
	movea 0x00000168, r3, r6
	st.w r6, 0x000000A8[r3]
	movea 0x000001B8, r3, r6
	st.w r0, 0x00000150[r3]
	st.w r0, 0x00000158[r3]
	st.w r9, 0x00000164[r3]
	st.w r0, 0x00000160[r3]
	st.w r0, 0x00000170[r3]
	st.w r8, 0x0000017C[r3]
	st.w r0, 0x00000178[r3]
	st.w r0, 0x00000180[r3]
	st.w r7, 0x00000188[r3]
	st.w r0, 0x00000190[r3]
	st.w r0, 0x00000198[r3]
	st.w r0, 0x000001A8[r3]
	st.w r0, 0x000001B4[r3]
	st.w r0, 0x000001B0[r3]
.BB.LABEL.1_1:	; bb.i.split.clone
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 47
	st.b r0, 0x00000000[r6]
	st.b r0, 0x00000001[r6]
	st.b r0, 0x00000002[r6]
	st.b r0, 0x00000003[r6]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 46
	add 0x00000004, r6
	loop r5, .BB.LABEL.1_1
.BB.LABEL.1_2:	; memset.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2455
	st.w r2, 0x000001BC[r3]
	st.w r0, 0x000001B8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2469
	st.w r2, 0x000001C4[r3]
	st.w r0, 0x000001C0[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2505
	st.w r2, 0x000001CC[r3]
	st.w r0, 0x000001C8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2571
	st.w r9, 0x000001D4[r3]
	st.w r0, 0x000001D0[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2585
	st.w r9, 0x000001DC[r3]
	st.w r0, 0x000001D8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2599
	st.w r9, 0x000001E4[r3]
	st.w r0, 0x000001E0[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2613
	st.w r9, 0x000001EC[r3]
	st.w r0, 0x000001E8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2627
	st.w r9, 0x000001F4[r3]
	st.w r0, 0x000001F0[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2641
	st.w r9, 0x000001FC[r3]
	st.w r0, 0x000001F8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2655
	st.w r9, 0x00000204[r3]
	st.w r0, 0x00000200[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2669
	st.w r9, 0x0000020C[r3]
	mov 0x00000004, r6
	mov 0x00000001, r20
	st.w r0, 0x00000208[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2683
	st.w r9, 0x00000214[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3034
	mov r20, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2683
	st.w r0, 0x00000210[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3034
	mov r20, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2697
	st.w r9, 0x0000021C[r3]
	st.w r0, 0x00000218[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2711
	st.w r9, 0x00000224[r3]
	st.w r0, 0x00000220[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2725
	st.w r9, 0x0000022C[r3]
	st.w r0, 0x00000228[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2739
	st.w r9, 0x00000234[r3]
	st.w r0, 0x00000230[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2753
	st.w r9, 0x0000023C[r3]
	st.w r0, 0x00000238[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 2781
	st.w r9, 0x00000244[r3]
	st.w r0, 0x00000240[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3034
	jarl _SetRelAlarm, r31
	mov 0x00000002, r21
	mov 0x00000004, r22
.BB.LABEL.1_3:	; bb653
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_sil.h", 103
	ld23.h 0xFFC10250[r0], r2
	ld23.hu 0xFFC10224[r0], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3090
	movhi HIGHW1(#_CH_BlsmControl_0002_0088), r0, r6
	ld.w LOWW(#_CH_BlsmControl_0002_0088)[r6], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3075
	mov r2, r7
	shr 0x00000003, r7
	andi 0x00000002, r7, r7
	andi 0x00000004, r5, r5
	shr 0x00000001, r2
	or r5, r7
	andi 0x00000001, r2, r2
	or r2, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3090
	cmp 0x00000000, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3077
	cvtf.uwd r7, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3090
	st23.dw r8, 0x000000A0[r3]
	bz9 .BB.LABEL.1_5
.BB.LABEL.1_4:	; bb235
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3103
	mov r22, r6
	jarl _WaitEvent, r31
	mov r22, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3105
	syncm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3090
	movhi HIGHW1(#_CH_BlsmControl_0002_0088), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0002_0088)[r2], r2
	cmp 0x00000000, r2
	bnz9 .BB.LABEL.1_4
.BB.LABEL.1_5:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld23.dw 0x000000A0[r3], r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3091
	mov #_CH_BlsmControl_0002_0088, r2
	st23.dw r24, 0x00000004[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3092
	syncm
	mov 0x00000001, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3093
	st.w r5, 0x00000000[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3094
	syncm
	mov r5, r6
	mov r21, r7
	jarl _SetEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3145
	ld23.dw 0x000001C0[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3123
	ld23.dw 0x000001B8[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3216
	cmpf.d 0x00000002, r24, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3123
	st23.dw r6, 0x00000098[r3]
	st23.dw r8, 0x00000090[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3216
	trfsr 0
	bz9 .BB.LABEL.1_7
.BB.LABEL.1_6:	; if_then_bb.bb268_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	mov 0x00000000, r23
	br9 .BB.LABEL.1_9
.BB.LABEL.1_7:	; bb261
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3193
	ld23.dw 0x000000D0[r3], r6
	ld23.dw 0x00000090[r3], r8
	cmpf.d 0x00000004, r6, r8
	trfsr 0
	bz9 .BB.LABEL.1_6
.BB.LABEL.1_8:	; bb267
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	mov 0x00000001, r23
.BB.LABEL.1_9:	; bb268
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3239
	ld23.dw 0x000001C8[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3252
	ld23.dw 0x00000090[r3], r14
	cmp 0x00000000, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3276
	ld23.dw 0x000000E0[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3252
	cmov 0x0000000A, r6, r14, r6
	cmov 0x0000000A, r7, r15, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3276
	st23.dw r6, 0x00000088[r3]
	mulf.d r6, r8, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3300
	ld23.dw 0x000000E8[r3], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3324
	ld23.dw 0x000000F0[r3], r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3300
	divf.d r6, r10, r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3324
	cmpf.d 0x00000004, r12, r14
	trfsr 0
	bnz9 .BB.LABEL.1_11
.BB.LABEL.1_10:	; if_then_bb298
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3326
	ld23.dw 0x000000F8[r3], r24
.BB.LABEL.1_11:	; if_break_bb303
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3375
	ld23.dw 0x00000100[r3], r6
	cmpf.d 0x00000007, r24, r6
	cmovf.d 0, r24, r24, r6
	trfsr 0
	bnz9 .BB.LABEL.1_13
.BB.LABEL.1_12:	; if_else_bb313
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3380
	ld23.dw 0x00000108[r3], r6
.BB.LABEL.1_13:	; if_else_bb313
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	st23.dw r6, 0x00000080[r3]
.BB.LABEL.1_14:	; if_break_bb316
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3405
	ld23.dw 0x00000110[r3], r6
	ld23.dw 0x00000080[r3], r8
	cmpf.d 0x00000007, r8, r6
	trfsr 0
	bz9 .BB.LABEL.1_16
.BB.LABEL.1_15:	; if_break_bb316.if_break_bb328_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld23.dw 0x00000080[r3], r6
	cmovf.d 0, r6, r6, r26
	br9 .BB.LABEL.1_17
.BB.LABEL.1_16:	; if_else_bb325
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3410
	ld23.dw 0x00000118[r3], r26
.BB.LABEL.1_17:	; if_break_bb328
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3435
	ld23.dw 0x00000120[r3], r6
	cmpf.d 0x00000007, r26, r6
	cmovf.d 0, r26, r26, r6
	trfsr 0
	bnz9 .BB.LABEL.1_19
.BB.LABEL.1_18:	; if_else_bb337
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3440
	ld23.dw 0x00000128[r3], r6
.BB.LABEL.1_19:	; if_else_bb337
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	st23.dw r6, 0x00000078[r3]
.BB.LABEL.1_20:	; if_break_bb340
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3773
	ld23.dw 0x00000138[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3749
	ld23.dw 0x00000238[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3773
	cmpf.d 0x00000007, r24, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3639
	ld23.dw 0x00000210[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3727
	st23.dw r8, 0x00000070[r3]
	ld23.dw 0x00000230[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3617
	st23.dw r6, 0x00000048[r3]
	ld23.dw 0x00000208[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3705
	st23.dw r8, 0x00000068[r3]
	ld23.dw 0x00000228[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3595
	st23.dw r6, 0x00000040[r3]
	ld23.dw 0x00000200[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3683
	st23.dw r8, 0x00000060[r3]
	ld23.dw 0x00000220[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3573
	st23.dw r6, 0x00000038[r3]
	ld23.dw 0x000001F8[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3661
	st23.dw r8, 0x00000058[r3]
	ld23.dw 0x00000218[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3551
	st23.dw r6, 0x00000030[r3]
	ld23.dw 0x000001F0[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3773
	st23.dw r8, 0x00000050[r3]
	trfsr 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3529
	st23.dw r6, 0x00000028[r3]
	ld23.dw 0x000001E8[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3507
	st23.dw r6, 0x00000020[r3]
	ld23.dw 0x000001E0[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3485
	st23.dw r6, 0x00000018[r3]
	ld23.dw 0x000001D8[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3463
	st23.dw r6, 0x00000010[r3]
	ld23.dw 0x000001D0[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3773
	st23.dw r6, 0x00000008[r3]
	cmovf.d 0, r24, r24, r6
	bnz9 .BB.LABEL.1_22
.BB.LABEL.1_21:	; if_else_bb377
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3777
	ld23.dw 0x00000140[r3], r6
.BB.LABEL.1_22:	; if_else_bb377
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	st23.dw r6, 0x00000000[r3]
.BB.LABEL.1_23:	; if_break_bb380
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3822
	ld23.dw 0x00000240[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3831
	ld23.dw 0x00000008[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3885
	movhi HIGHW1(#_CH_BlsmControl_0043_0077+0x00000004), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0043_0077+0x00000004)[r2], r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3831
	addf.d r8, r6, r6
	ld23.dw 0x00000010[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3885
	subr r20, r2
	cmp 0x00000000, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3831
	addf.d r8, r6, r6
	ld23.dw 0x00000018[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000020[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000028[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000030[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000038[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000040[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000048[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000050[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000058[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000060[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000068[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000070[r3], r8
	addf.d r8, r6, r6
	ld23.dw 0x00000000[r3], r8
	addf.d r8, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3870
	ld23.dw 0x00000148[r3], r8
	divf.d r8, r6, r28
	ble9 .BB.LABEL.1_27
.BB.LABEL.1_24:	; if_else_bb431
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3889
	movhi HIGHW1(#_CH_BlsmControl_0043_0077), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0043_0077)[r2], r2
	cmp 0x00000000, r2
	bz9 .BB.LABEL.1_26
.BB.LABEL.1_25:	; bb448
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3899
	mov r22, r6
	jarl _WaitEvent, r31
	mov r22, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3901
	syncm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3885
	movhi HIGHW1(#_CH_BlsmControl_0043_0077+0x00000004), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0043_0077+0x00000004)[r2], r2
	subr r20, r2
	cmp 0x00000000, r2
	bgt9 .BB.LABEL.1_24
	br9 .BB.LABEL.1_27
.BB.LABEL.1_26:	; if_then_bb436
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3890
	mov #_CH_BlsmControl_0043_0077, r2
	st23.dw r28, 0x00000008[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3891
	syncm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3893
	ld.w 0x00000004[r2], r5
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3892
	st.w r6, 0x00000000[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3893
	movea 0x00000064, r5, r5
	st.w r5, 0x00000004[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3894
	syncm
	mov r21, r6
	mov r21, r7
	jarl _SetEvent, r31
.BB.LABEL.1_27:	; bb483.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	movhi HIGHW1(#_CH_BlsmControl_0039_0037), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0039_0037)[r2], r2
	cmp 0x00000000, r2
	bgt9 .BB.LABEL.1_29
.BB.LABEL.1_28:	; bb479
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3959
	mov r21, r6
	jarl _WaitEvent, r31
	mov r21, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3961
	syncm
	br9 .BB.LABEL.1_27
.BB.LABEL.1_29:	; if_then_bb460
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3947
	mov #_CH_BlsmControl_0039_0037, r5
	ld23.dw 0x00000008[r5], r28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3948
	addi 0xFFFFFFFF, r2, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3950
	cmp 0x00000001, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3949
	st.w r6, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3950
	bnz9 .BB.LABEL.1_31
.BB.LABEL.1_30:	; bb470
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3951
	syncm
	mov r21, r6
	mov r22, r7
	jarl _SetEvent, r31
	br9 .BB.LABEL.1_32
.BB.LABEL.1_31:	; if_else_bb473
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3954
	syncm
.BB.LABEL.1_32:	; bb485
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3975
	ld23.dw 0x00000150[r3], r6
	ld23.dw 0x00000078[r3], r8
	cmpf.d 0x00000004, r6, r8
	trfsr 0
	bnz9 .BB.LABEL.1_34
.BB.LABEL.1_33:	; if_then_bb492
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 3977
	ld23.dw 0x00000158[r3], r28
.BB.LABEL.1_34:	; if_break_bb497
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4036
	ld23.dw 0x00000178[r3], r6
	ld23.dw 0x00000080[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4040
	ld.w 0x000000B0[r3], r2
	ld.w 0x000000B4[r3], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4036
	cmpf.d 0x00000004, r6, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4006
	ld23.dw 0x00000160[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4040
	cmovf.s 0, r2, r5, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4010
	ld.w 0x000000A8[r3], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4006
	cmpf.d 0x00000004, r6, r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4066
	ld23.dw 0x00000190[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4040
	ld23.dw 0x00000000[r2], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4010
	ld.w 0x000000AC[r3], r2
	cmovf.s 0, r2, r5, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4070
	ld.w 0x000000BC[r3], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4066
	cmpf.d 0x00000004, r6, r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4010
	ld23.dw 0x00000000[r2], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4112
	movhi HIGHW1(#_CH_BlsmControl_0036_0038+0x00000004), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0036_0038+0x00000004)[r2], r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4093
	mulf.d r10, r28, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4112
	subr r20, r2
	cmp 0x00000000, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4093
	mulf.d r8, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4070
	ld.w 0x000000B8[r3], r8
	cmovf.s 0, r8, r5, r5
	ld23.dw 0x00000000[r5], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4093
	mulf.d r8, r6, r24
	ble9 .BB.LABEL.1_38
.BB.LABEL.1_35:	; if_else_bb556
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4116
	movhi HIGHW1(#_CH_BlsmControl_0036_0038), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0036_0038)[r2], r2
	cmp 0x00000000, r2
	bz9 .BB.LABEL.1_37
.BB.LABEL.1_36:	; bb573
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4128
	mov r22, r6
	jarl _WaitEvent, r31
	mov r22, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4130
	syncm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4112
	movhi HIGHW1(#_CH_BlsmControl_0036_0038+0x00000004), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0036_0038+0x00000004)[r2], r2
	subr r20, r2
	cmp 0x00000000, r2
	bgt9 .BB.LABEL.1_35
	br9 .BB.LABEL.1_38
.BB.LABEL.1_37:	; if_then_bb561
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4117
	mov #_CH_BlsmControl_0036_0038, r2
	st23.dw r24, 0x00000008[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4118
	syncm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4120
	ld.w 0x00000004[r2], r5
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4119
	st.w r6, 0x00000000[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4120
	movea 0x00000064, r5, r5
	st.w r5, 0x00000004[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4121
	syncm
	mov r21, r6
	mov r21, r7
	jarl _SetEvent, r31
.BB.LABEL.1_38:	; bb579
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4149
	ld23.dw 0x000001A8[r3], r6
	ld23.dw 0x00000090[r3], r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4173
	xori 0x00000001, r23, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4149
	addf.d r8, r6, r6
	bz9 .BB.LABEL.1_40
.BB.LABEL.1_39:	; if_then_bb589
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4174
	ld23.dw 0x000001B0[r3], r6
.BB.LABEL.1_40:	; if_break_bb594
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4206
	ld23.dw 0x00000098[r3], r8
	ld23.dw 0x000000A0[r3], r10
	cmp 0x00000000, r23
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_sil.h", 103
	ld23.hu 0xFFE30000[r0], r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4206
	cmov 0x0000000A, r8, r10, r8
	cmov 0x0000000A, r9, r11, r9
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_sil.h", 105
	cvtf.uwd r2, r10
	mulf.d r24, r10, r10
	trncf.duw r10, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_sil.h", 110
	st23.h r2, 0xFFE30008[r0]
	movea 0x00000015, r0, r2
	st23.h r2, 0xFFE30044[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4267
	st23.dw r6, 0x000001B8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4310
	ld23.dw 0x00000088[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4288
	st23.dw r8, 0x000001C0[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4310
	st23.dw r6, 0x000001C8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4332
	ld23.dw 0x00000010[r3], r6
	st23.dw r6, 0x000001D0[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4354
	ld23.dw 0x00000018[r3], r6
	st23.dw r6, 0x000001D8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4376
	ld23.dw 0x00000020[r3], r6
	st23.dw r6, 0x000001E0[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4398
	ld23.dw 0x00000028[r3], r6
	st23.dw r6, 0x000001E8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4420
	ld23.dw 0x00000030[r3], r6
	st23.dw r6, 0x000001F0[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4442
	ld23.dw 0x00000038[r3], r6
	st23.dw r6, 0x000001F8[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4464
	ld23.dw 0x00000040[r3], r6
	st23.dw r6, 0x00000200[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4486
	ld23.dw 0x00000048[r3], r6
	mov 0x00000001, r23
	st23.dw r6, 0x00000208[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4508
	ld23.dw 0x00000050[r3], r6
	st23.dw r6, 0x00000210[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4530
	ld23.dw 0x00000058[r3], r6
	st23.dw r6, 0x00000218[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4552
	ld23.dw 0x00000060[r3], r6
	st23.dw r6, 0x00000220[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4574
	ld23.dw 0x00000068[r3], r6
	st23.dw r6, 0x00000228[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4596
	ld23.dw 0x00000070[r3], r6
	st23.dw r6, 0x00000230[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4618
	ld23.dw 0x00000000[r3], r6
	st23.dw r6, 0x00000238[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4639
	ld23.dw 0x00000008[r3], r6
	st23.dw r6, 0x00000240[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4664
	mov r23, r6
	jarl _WaitEvent, r31
	mov r23, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4667
	add 0x00000001, r20
	jr .BB.LABEL.1_3
	.align 4
_TaskMainBlsmControl_Core1_1_0_Task:
	.stack _TaskMainBlsmControl_Core1_1_0_Task = 1152
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4678
	prepare 0x00000FFF, 0x0000007C
	movea 0xFFFFFC2C, r3, r3
	mov 0xC0568000, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4912
	st.w r2, 0x0000016C[r3]
	movhi 0x0000403E, r0, r2
	st.w r2, 0x00000174[r3]
	movhi 0x0000C03E, r0, r2
	st.w r2, 0x0000017C[r3]
	mov 0x4062C000, r2
	st.w r2, 0x00000184[r3]
	mov 0xC062C000, r2
	st.w r2, 0x0000018C[r3]
	mov 0x40568000, r2
	st.w r2, 0x00000194[r3]
	mov 0x400921FB, r2
	st.w r2, 0x0000019C[r3]
	st.w r0, 0x00000170[r3]
	mov 0x54442D18, r2
	st.w r2, 0x00000198[r3]
	st.w r0, 0x00000178[r3]
	mov 0xC0668000, r2
	st.w r0, 0x00000180[r3]
	st.w r0, 0x00000188[r3]
	st.w r0, 0x00000190[r3]
	st.w r0, 0x000001A0[r3]
	st.w r0, 0x000001A8[r3]
	mov 0x408F4000, r5
	st.w r5, 0x000001FC[r3]
	st.w r5, 0x000001EC[r3]
	st.w r5, 0x000001DC[r3]
	st.w r5, 0x000001CC[r3]
	st.w r5, 0x000001BC[r3]
	st.w r5, 0x000001AC[r3]
	movhi 0x0000C05E, r0, r5
	st.w r0, 0x000001B0[r3]
	movhi 0x0000C04E, r0, r6
	st.w r0, 0x000001B8[r3]
	movhi 0x0000404E, r0, r7
	st.w r0, 0x000001C0[r3]
	movhi 0x0000405E, r0, r8
	st.w r0, 0x000001C8[r3]
	st.w r0, 0x000001D4[r3]
	movhi 0x0000BFF0, r0, r9
	st.w r0, 0x000001D0[r3]
	st.w r0, 0x000001D8[r3]
	st.w r0, 0x000001E0[r3]
	st.w r0, 0x000001E8[r3]
	movhi 0x00003FF0, r0, r10
	st.w r2, 0x000001A4[r3]
	st.w r0, 0x000001F0[r3]
	st.w r0, 0x000001F8[r3]
	st.w r0, 0x00000204[r3]
	st.w r0, 0x00000200[r3]
	st.w r5, 0x000001B4[r3]
	st.w r6, 0x000001C4[r3]
	st.w r7, 0x000001E4[r3]
	st.w r8, 0x000001F4[r3]
	st.w r0, 0x00000168[r3]
	st.w r0, 0x00000208[r3]
	st.w r9, 0x0000020C[r3]
	st.w r0, 0x00000214[r3]
	st.w r0, 0x00000210[r3]
	st.w r0, 0x00000218[r3]
	st.w r10, 0x0000021C[r3]
	st.w r10, 0x00000224[r3]
	st.w r0, 0x00000220[r3]
	st.w r9, 0x0000022C[r3]
	st.w r0, 0x00000228[r3]
	st.w r0, 0x00000234[r3]
	st.w r0, 0x00000230[r3]
	mov 0x40668000, r11
	st.w r0, 0x00000238[r3]
	st.w r11, 0x0000023C[r3]
	st.w r10, 0x00000244[r3]
	st.w r0, 0x00000240[r3]
	st.w r10, 0x0000024C[r3]
	st.w r0, 0x00000248[r3]
	st.w r10, 0x00000254[r3]
	st.w r0, 0x00000250[r3]
	st.w r0, 0x0000025C[r3]
	st.w r0, 0x00000258[r3]
	st.w r9, 0x00000264[r3]
	st.w r0, 0x00000260[r3]
	st.w r10, 0x0000026C[r3]
	st.w r0, 0x00000268[r3]
	st.w r0, 0x00000274[r3]
	st.w r0, 0x00000270[r3]
	st.w r0, 0x0000027C[r3]
	st.w r0, 0x00000278[r3]
	st.w r9, 0x00000284[r3]
	st.w r0, 0x00000280[r3]
	st.w r10, 0x0000028C[r3]
	st.w r0, 0x00000288[r3]
	st.w r8, 0x00000294[r3]
	st.w r0, 0x00000290[r3]
	st.w r10, 0x0000029C[r3]
	st.w r0, 0x00000298[r3]
	st.w r10, 0x000002A4[r3]
	st.w r0, 0x000002A0[r3]
	st.w r10, 0x000002AC[r3]
	st.w r0, 0x000002A8[r3]
	st.w r0, 0x000002B4[r3]
	st.w r0, 0x000002B0[r3]
	st.w r0, 0x000002BC[r3]
	st.w r0, 0x000002B8[r3]
	st.w r10, 0x000002C4[r3]
	st.w r0, 0x000002C0[r3]
	st.w r9, 0x000002CC[r3]
	st.w r0, 0x000002C8[r3]
	st.w r9, 0x000002D4[r3]
	st.w r0, 0x000002D0[r3]
	st.w r0, 0x000002DC[r3]
	st.w r0, 0x000002D8[r3]
	st.w r10, 0x000002E4[r3]
	st.w r0, 0x000002E0[r3]
	st.w r7, 0x000002EC[r3]
	st.w r0, 0x000002E8[r3]
	st.w r10, 0x000002F4[r3]
	st.w r0, 0x000002F0[r3]
	st.w r10, 0x000002FC[r3]
	st.w r0, 0x000002F8[r3]
	st.w r10, 0x00000304[r3]
	st.w r0, 0x00000300[r3]
	st.w r0, 0x0000030C[r3]
	st.w r0, 0x00000308[r3]
	st.w r9, 0x00000314[r3]
	st.w r0, 0x00000310[r3]
	st.w r10, 0x0000031C[r3]
	st.w r0, 0x00000318[r3]
	st.w r0, 0x00000324[r3]
	st.w r0, 0x00000320[r3]
	st.w r10, 0x0000032C[r3]
	st.w r0, 0x00000328[r3]
	st.w r0, 0x00000334[r3]
	st.w r0, 0x00000330[r3]
	st.w r9, 0x0000033C[r3]
	st.w r0, 0x00000338[r3]
	st.w r0, 0x00000344[r3]
	st.w r0, 0x00000340[r3]
	st.w r10, 0x0000034C[r3]
	st.w r0, 0x00000348[r3]
	st.w r10, 0x00000354[r3]
	st.w r0, 0x00000350[r3]
	st.w r10, 0x0000035C[r3]
	st.w r0, 0x00000358[r3]
	st.w r0, 0x00000364[r3]
	st.w r0, 0x00000360[r3]
	st.w r10, 0x0000036C[r3]
	st.w r0, 0x00000368[r3]
	st.w r9, 0x00000374[r3]
	st.w r0, 0x00000370[r3]
	st.w r0, 0x0000037C[r3]
	st.w r0, 0x00000378[r3]
	st.w r0, 0x00000384[r3]
	st.w r0, 0x00000380[r3]
	st.w r10, 0x0000038C[r3]
	st.w r0, 0x00000388[r3]
	st.w r9, 0x00000394[r3]
	st.w r0, 0x00000390[r3]
	st.w r6, 0x0000039C[r3]
	st.w r0, 0x00000398[r3]
	st.w r10, 0x000003A4[r3]
	st.w r0, 0x000003A0[r3]
	st.w r10, 0x000003AC[r3]
	st.w r0, 0x000003A8[r3]
	st.w r10, 0x000003B4[r3]
	st.w r0, 0x000003B0[r3]
	st.w r0, 0x000003BC[r3]
	st.w r0, 0x000003B8[r3]
	st.w r0, 0x000003C4[r3]
	st.w r0, 0x000003C0[r3]
	st.w r9, 0x000003CC[r3]
	st.w r0, 0x000003C8[r3]
	st.w r10, 0x000003D4[r3]
	st.w r0, 0x000003D0[r3]
	st.w r10, 0x000003DC[r3]
	st.w r0, 0x000003D8[r3]
	st.w r9, 0x000003EC[r3]
	st.w r0, 0x000003E4[r3]
	st.w r0, 0x000003E0[r3]
	st.w r0, 0x000003E8[r3]
	st.w r5, 0x000003F4[r3]
	st.w r0, 0x000003F0[r3]
	st.w r10, 0x000003FC[r3]
	st.w r0, 0x000003F8[r3]
	st.w r10, 0x00000404[r3]
	st.w r0, 0x00000400[r3]
	st.w r10, 0x0000040C[r3]
	st.w r0, 0x00000408[r3]
	st.w r2, 0x00000414[r3]
	st.w r0, 0x00000410[r3]
	st.w r10, 0x0000041C[r3]
	st.w r0, 0x00000418[r3]
	st.w r10, 0x0000042C[r3]
	st.w r10, 0x00000424[r3]
	st.w r0, 0x00000420[r3]
	mov 0x00000005, r6
	st.w r0, 0x00000428[r3]
	mov 0x00000001, r20
	st.w r0, 0x00000434[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5647
	mov r20, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4912
	st.w r0, 0x00000430[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5647
	mov r20, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4912
	st.w r0, 0x0000043C[r3]
	st.w r0, 0x00000438[r3]
	st.w r0, 0x00000444[r3]
	st.w r0, 0x00000440[r3]
	st.w r0, 0x0000044C[r3]
	st.w r0, 0x00000448[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5515
	st.w r0, 0x00000154[r3]
	st.w r0, 0x00000150[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5516
	st.w r0, 0x0000015C[r3]
	st.w r0, 0x00000158[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5517
	st.w r0, 0x00000164[r3]
	st.w r0, 0x00000160[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5647
	jarl _SetRelAlarm, r31
	movea 0x00000064, r0, r7
	mov 0x00000006, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5652
	mov r7, r8
	jarl _SetRelAlarm, r31
	mov 0x00000002, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5655
	mov r20, r7
	jarl _SetEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 4912
	movea 0x00000230, r3, r2
	st.w r2, 0x0000014C[r3]
	movea 0x00000228, r3, r2
	st.w r2, 0x00000148[r3]
	movea 0x00000280, r3, r2
	st.w r2, 0x00000144[r3]
	movea 0x000002E0, r3, r2
	st.w r2, 0x00000140[r3]
	movea 0x000002D8, r3, r2
	st.w r2, 0x0000013C[r3]
	movea 0x00000320, r3, r2
	st.w r2, 0x00000138[r3]
	movea 0x00000318, r3, r2
	st.w r2, 0x00000134[r3]
	movea 0x00000390, r3, r2
	st.w r2, 0x00000130[r3]
	movea 0x00000388, r3, r2
	st.w r2, 0x0000012C[r3]
	movea 0x000003E8, r3, r2
	st.w r2, 0x00000128[r3]
	movea 0x000003E0, r3, r2
	st.w r2, 0x00000124[r3]
	movea 0x00000440, r3, r2
	st.w r2, 0x00000120[r3]
	movea 0x00000438, r3, r2
	st.w r2, 0x0000011C[r3]
	movea 0x00000428, r3, r2
	st.w r2, 0x00000118[r3]
	movea 0x00000420, r3, r2
	st.w r2, 0x00000114[r3]
	movea 0x00000408, r3, r2
	st.w r2, 0x00000110[r3]
	movea 0x00000400, r3, r2
	st.w r2, 0x0000010C[r3]
	movea 0x000003D0, r3, r2
	st.w r2, 0x00000108[r3]
	movea 0x000003C8, r3, r2
	st.w r2, 0x00000104[r3]
	movea 0x000003B0, r3, r2
	st.w r2, 0x00000100[r3]
	movea 0x000003A8, r3, r2
	st.w r2, 0x000000FC[r3]
	movea 0x00000378, r3, r2
	st.w r2, 0x000000F8[r3]
	movea 0x00000370, r3, r2
	st.w r2, 0x000000F4[r3]
	movea 0x00000358, r3, r2
	st.w r2, 0x000000F0[r3]
	movea 0x00000350, r3, r2
	st.w r2, 0x000000EC[r3]
	movea 0x00000338, r3, r2
	st.w r2, 0x000000E8[r3]
	movea 0x00000330, r3, r2
	st.w r2, 0x000000E4[r3]
	movea 0x00000300, r3, r2
	st.w r2, 0x000000E0[r3]
	movea 0x000002F8, r3, r2
	st.w r2, 0x000000DC[r3]
	movea 0x000002C8, r3, r2
	st.w r2, 0x000000D8[r3]
	movea 0x000002C0, r3, r2
	st.w r2, 0x000000D4[r3]
	movea 0x000002A8, r3, r2
	st.w r2, 0x000000D0[r3]
	movea 0x000002A0, r3, r2
	st.w r2, 0x000000CC[r3]
	movea 0x00000270, r3, r2
	st.w r2, 0x000000C8[r3]
	movea 0x00000268, r3, r2
	st.w r2, 0x000000C4[r3]
	movea 0x00000250, r3, r2
	st.w r2, 0x000000C0[r3]
	movea 0x00000248, r3, r2
	st.w r2, 0x000000BC[r3]
	movea 0x00000218, r3, r2
	st.w r2, 0x000000B8[r3]
	movea 0x00000210, r3, r2
	st.w r2, 0x000000B4[r3]
	movea 0x00000430, r3, r2
	st.w r2, 0x000000B0[r3]
	movea 0x00000418, r3, r2
	st.w r2, 0x000000AC[r3]
	movea 0x000003F8, r3, r2
	st.w r2, 0x000000A8[r3]
	movea 0x000003D8, r3, r2
	st.w r2, 0x000000A4[r3]
	movea 0x000003C0, r3, r2
	st.w r2, 0x000000A0[r3]
	movea 0x000003A0, r3, r2
	st.w r2, 0x0000009C[r3]
	movea 0x00000380, r3, r2
	st.w r2, 0x00000098[r3]
	movea 0x00000368, r3, r2
	st.w r2, 0x00000094[r3]
	movea 0x00000348, r3, r2
	st.w r2, 0x00000090[r3]
	movea 0x00000328, r3, r2
	st.w r2, 0x0000008C[r3]
	movea 0x00000310, r3, r2
	st.w r2, 0x00000088[r3]
	movea 0x000002F0, r3, r2
	st.w r2, 0x00000084[r3]
	movea 0x000002D0, r3, r2
	st.w r2, 0x00000080[r3]
	movea 0x000002B8, r3, r2
	st.w r2, 0x0000007C[r3]
	movea 0x00000298, r3, r2
	st.w r2, 0x00000078[r3]
	movea 0x00000288, r3, r2
	st.w r2, 0x00000074[r3]
	movea 0x00000278, r3, r2
	st.w r2, 0x00000070[r3]
	movea 0x00000260, r3, r2
	st.w r2, 0x0000006C[r3]
	movea 0x00000240, r3, r2
	st.w r2, 0x00000060[r3]
	movea 0x00000220, r3, r2
	st.w r2, 0x00000068[r3]
	movea 0x00000208, r3, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5718
	st.w r2, 0x00000064[r3]
	movhi HIGHW1(#_CH_BlsmControl_0015_0076+0x00000004), r0, r21
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5821
	movhi HIGHW1(#_CH_BlsmControl_0002_0088), r0, r22
	mov 0x00000002, r23
	mov 0x00000004, r24
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5722
	movhi HIGHW1(#_CH_BlsmControl_0015_0076), r0, r25
.BB.LABEL.2_1:	; bb1213
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5681
	movhi HIGHW1(#_blsm_speed), r0, r2
	ld.h LOWW(#_blsm_speed)[r2], r2
	cvtf.wd r2, r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5705
	cmovf.d 0, r26, r26, r6
	jarl _fabs, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5718
	cmovf.d 0, r10, r10, r28
	ld.w LOWW(#_CH_BlsmControl_0015_0076+0x00000004)[r21], r2
.BB.LABEL.2_2:	; bb1213
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	subr r20, r2
	cmp 0x00000000, r2
	ble9 .BB.LABEL.2_6
.BB.LABEL.2_3:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5722
	ld.w LOWW(#_CH_BlsmControl_0015_0076)[r25], r2
	cmp 0x00000000, r2
	bz9 .BB.LABEL.2_5
.BB.LABEL.2_4:	; bb306
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5732
	mov r24, r6
	jarl _WaitEvent, r31
	mov r24, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5734
	syncm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5718
	ld.w LOWW(#_CH_BlsmControl_0015_0076+0x00000004)[r21], r2
	br9 .BB.LABEL.2_2
.BB.LABEL.2_5:	; if_then_bb295
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5723
	mov #_CH_BlsmControl_0015_0076, r2
	st23.dw r28, 0x00000008[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5724
	syncm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5726
	ld.w 0x00000004[r2], r5
	mov 0x00000001, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5725
	st.w r6, 0x00000000[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5726
	movea 0x00000064, r5, r5
	st.w r5, 0x00000004[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5727
	syncm
	mov r23, r6
	mov r23, r7
	jarl _SetEvent, r31
.BB.LABEL.2_6:	; bb341.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w LOWW(#_CH_BlsmControl_0002_0088)[r22], r2
	cmp 0x00000000, r2
	bgt9 .BB.LABEL.2_8
.BB.LABEL.2_7:	; bb337
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5835
	mov r23, r6
	jarl _WaitEvent, r31
	mov r23, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5837
	syncm
	br9 .BB.LABEL.2_6
.BB.LABEL.2_8:	; if_then_bb318
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5823
	mov #_CH_BlsmControl_0002_0088, r5
	ld23.dw 0x00000004[r5], r28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5824
	addi 0xFFFFFFFF, r2, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5826
	cmp 0x00000001, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5825
	st.w r6, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5826
	bnz9 .BB.LABEL.2_10
.BB.LABEL.2_9:	; bb328
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5827
	syncm
	mov 0x00000000, r2
	mov r2, r6
	mov r24, r7
	jarl _SetEvent, r31
	br9 .BB.LABEL.2_11
.BB.LABEL.2_10:	; if_else_bb331
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5830
	syncm
.BB.LABEL.2_11:	; bb343
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5857
	trncf.dw r28, r2
	add 0xFFFFFFFF, r2
	cmp 0x00000004, r2
	bh9 .BB.LABEL.2_14
.BB.LABEL.2_12:	; bb343
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	shl 0x00000001, r2
	jmp #.SWITCH.LABEL.2_87[r2]
.SWITCH.LABEL.2_87:
	br9 .BB.LABEL.2_13
	br9 .BB.LABEL.2_15
	br9 .BB.LABEL.2_16
	br9 .BB.LABEL.2_17
	br9 .BB.LABEL.2_18
.SWITCH.LABEL.2_87.END:
.BB.LABEL.2_13:	; switch_clause_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5859
	ld23.dw 0x00000168[r3], r6
	br9 .BB.LABEL.2_19
.BB.LABEL.2_14:	; switch_clause_bb360
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5884
	ld23.dw 0x00000190[r3], r6
	br9 .BB.LABEL.2_19
.BB.LABEL.2_15:	; switch_clause_bb348
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5864
	ld23.dw 0x00000170[r3], r6
	br9 .BB.LABEL.2_19
.BB.LABEL.2_16:	; switch_clause_bb351
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5869
	ld23.dw 0x00000178[r3], r6
	br9 .BB.LABEL.2_19
.BB.LABEL.2_17:	; switch_clause_bb354
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5874
	ld23.dw 0x00000180[r3], r6
	br9 .BB.LABEL.2_19
.BB.LABEL.2_18:	; switch_clause_bb357
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5879
	ld23.dw 0x00000188[r3], r6
.BB.LABEL.2_19:	; switch_break_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5909
	ld23.dw 0x00000198[r3], r8
	mov 0x00000000, r12
	mov 0x40668000, r13
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5956
	divf.d r8, r12, r14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5910
	divf.d r12, r8, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5981
	ld23.dw 0x000001A0[r3], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5911
	mulf.d r6, r8, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5957
	mulf.d r6, r14, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5981
	cmpf.d 0x00000005, r6, r10
	st23.dw r6, 0x00000028[r3]
	trfsr 0
	bz9 .BB.LABEL.2_21
.BB.LABEL.2_20:	; switch_break_bb.if_break_bb389_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld23.dw 0x00000028[r3], r6
	br9 .BB.LABEL.2_22
.BB.LABEL.2_21:	; if_else_bb386
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 5986
	ld23.dw 0x000001A8[r3], r6
.BB.LABEL.2_22:	; if_break_bb389
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6011
	ld23.dw 0x000001B0[r3], r8
	cmpf.d 0x00000005, r6, r8
	cmovf.d 0, r6, r6, r8
	trfsr 0
	bnz9 .BB.LABEL.2_24
.BB.LABEL.2_23:	; if_else_bb398
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6016
	ld23.dw 0x000001B8[r3], r8
.BB.LABEL.2_24:	; if_break_bb401
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6041
	ld23.dw 0x000001C0[r3], r10
	cmpf.d 0x00000005, r8, r10
	cmovf.d 0, r8, r8, r10
	trfsr 0
	bnz9 .BB.LABEL.2_26
.BB.LABEL.2_25:	; if_else_bb410
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6046
	ld23.dw 0x000001C8[r3], r10
.BB.LABEL.2_26:	; if_break_bb413
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6071
	ld23.dw 0x000001D0[r3], r12
	cmpf.d 0x00000005, r10, r12
	cmovf.d 0, r10, r10, r12
	trfsr 0
	bnz9 .BB.LABEL.2_28
.BB.LABEL.2_27:	; if_else_bb422
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6076
	ld23.dw 0x000001D8[r3], r12
.BB.LABEL.2_28:	; if_break_bb425
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6101
	ld23.dw 0x000001E0[r3], r14
	cmpf.d 0x00000005, r12, r14
	cmovf.d 0, r12, r12, r14
	trfsr 0
	bnz9 .BB.LABEL.2_30
.BB.LABEL.2_29:	; if_else_bb434
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6106
	ld23.dw 0x000001E8[r3], r14
.BB.LABEL.2_30:	; if_break_bb437
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6131
	ld23.dw 0x000001F0[r3], r16
	cmpf.d 0x00000005, r14, r16
	cmovf.d 0, r14, r14, r16
	trfsr 0
	bnz9 .BB.LABEL.2_32
.BB.LABEL.2_31:	; if_else_bb446
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6136
	ld23.dw 0x000001F8[r3], r16
.BB.LABEL.2_32:	; if_break_bb449
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6163
	ld23.dw 0x00000200[r3], r18
	cmpf.d 0x00000005, r26, r18
	trfsr 0
	bz9 .BB.LABEL.2_34
.BB.LABEL.2_33:	; if_break_bb449.if_break_bb494_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000B8[r3], r2
	ld.w 0x000000B4[r3], r5
	ld.w 0x00000064[r3], r18
	br9 .BB.LABEL.2_35
.BB.LABEL.2_34:	; if_else_bb475
	ld.w 0x0000014C[r3], r2
	ld.w 0x00000148[r3], r5
	ld.w 0x00000068[r3], r18
.BB.LABEL.2_35:	; if_break_bb494
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6201
	ld23.dw 0x00000238[r3], r28
	cmpf.d 0x00000006, r28, r16
	trfsr 0
	bnz9 .BB.LABEL.2_37
.BB.LABEL.2_36:	; if_then_bb501
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000C0[r3], r2
	ld.w 0x000000BC[r3], r5
	ld.w 0x00000060[r3], r18
.BB.LABEL.2_37:	; if_break_bb536
	ld23.dw 0x00000000[r18], r18
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6242
	ld23.dw 0x00000258[r3], r16
	st23.dw r18, 0x00000058[r3]
	ld23.dw 0x00000000[r5], r18
	cmpf.d 0x00000005, r26, r16
	st23.dw r18, 0x00000050[r3]
	ld23.dw 0x00000000[r2], r18
	trfsr 0
	st23.dw r18, 0x00000048[r3]
	bz9 .BB.LABEL.2_39
.BB.LABEL.2_38:	; if_break_bb536.if_break_bb581_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000C8[r3], r2
	ld.w 0x000000C4[r3], r5
	ld.w 0x0000006C[r3], r16
	br9 .BB.LABEL.2_40
.BB.LABEL.2_39:	; if_else_bb562
	ld.w 0x00000074[r3], r2
	ld.w 0x00000144[r3], r5
	ld.w 0x00000070[r3], r16
.BB.LABEL.2_40:	; if_break_bb581
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6280
	ld23.dw 0x00000290[r3], r18
	cmpf.d 0x00000006, r18, r14
	trfsr 0
	bnz9 .BB.LABEL.2_42
.BB.LABEL.2_41:	; if_then_bb588
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000D0[r3], r2
	ld.w 0x000000CC[r3], r5
	ld.w 0x00000078[r3], r16
.BB.LABEL.2_42:	; if_break_bb623
	ld23.dw 0x00000000[r16], r16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6321
	ld23.dw 0x000002B0[r3], r14
	st23.dw r16, 0x00000040[r3]
	ld23.dw 0x00000000[r5], r16
	cmpf.d 0x00000005, r26, r14
	st23.dw r16, 0x00000038[r3]
	ld23.dw 0x00000000[r2], r16
	trfsr 0
	st23.dw r16, 0x00000030[r3]
	bz9 .BB.LABEL.2_44
.BB.LABEL.2_43:	; if_break_bb623.if_break_bb668_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000D8[r3], r2
	ld.w 0x000000D4[r3], r5
	ld.w 0x0000007C[r3], r14
	br9 .BB.LABEL.2_45
.BB.LABEL.2_44:	; if_else_bb649
	ld.w 0x00000140[r3], r2
	ld.w 0x0000013C[r3], r5
	ld.w 0x00000080[r3], r14
.BB.LABEL.2_45:	; if_break_bb668
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6359
	ld23.dw 0x000002E8[r3], r16
	cmpf.d 0x00000006, r16, r12
	trfsr 0
	bnz9 .BB.LABEL.2_47
.BB.LABEL.2_46:	; if_then_bb675
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000E0[r3], r2
	ld.w 0x000000DC[r3], r5
	ld.w 0x00000084[r3], r14
.BB.LABEL.2_47:	; if_break_bb710
	ld23.dw 0x00000000[r14], r14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6400
	ld23.dw 0x00000308[r3], r12
	st23.dw r14, 0x00000020[r3]
	ld23.dw 0x00000000[r5], r14
	cmpf.d 0x00000005, r26, r12
	st23.dw r14, 0x00000018[r3]
	ld23.dw 0x00000000[r2], r14
	trfsr 0
	st23.dw r14, 0x00000010[r3]
	bz9 .BB.LABEL.2_49
.BB.LABEL.2_48:	; if_break_bb710.if_break_bb755_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000E8[r3], r2
	ld.w 0x000000E4[r3], r5
	ld.w 0x0000008C[r3], r12
	br9 .BB.LABEL.2_50
.BB.LABEL.2_49:	; if_else_bb736
	ld.w 0x00000138[r3], r2
	ld.w 0x00000134[r3], r5
	ld.w 0x00000088[r3], r12
.BB.LABEL.2_50:	; if_break_bb755
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6438
	ld23.dw 0x00000340[r3], r14
	cmpf.d 0x00000006, r14, r10
	trfsr 0
	bnz9 .BB.LABEL.2_52
.BB.LABEL.2_51:	; if_then_bb762
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000F0[r3], r2
	ld.w 0x000000EC[r3], r5
	ld.w 0x00000090[r3], r12
.BB.LABEL.2_52:	; if_break_bb797
	ld23.dw 0x00000000[r12], r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6479
	ld23.dw 0x00000360[r3], r10
	st23.dw r12, 0x00000008[r3]
	ld23.dw 0x00000000[r5], r12
	cmpf.d 0x00000005, r26, r10
	st23.dw r12, 0x00000000[r3]
	ld23.dw 0x00000000[r2], r12
	trfsr 0
	bz9 .BB.LABEL.2_54
.BB.LABEL.2_53:	; if_break_bb797.if_break_bb842_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x000000F8[r3], r2
	ld.w 0x000000F4[r3], r5
	ld.w 0x00000094[r3], r10
	br9 .BB.LABEL.2_55
.BB.LABEL.2_54:	; if_else_bb823
	ld.w 0x00000130[r3], r2
	ld.w 0x0000012C[r3], r5
	ld.w 0x00000098[r3], r10
.BB.LABEL.2_55:	; if_break_bb842
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6517
	ld23.dw 0x00000398[r3], r14
	cmpf.d 0x00000006, r14, r8
	trfsr 0
	bnz9 .BB.LABEL.2_57
.BB.LABEL.2_56:	; if_then_bb849
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x00000100[r3], r2
	ld.w 0x000000FC[r3], r5
	ld.w 0x0000009C[r3], r10
.BB.LABEL.2_57:	; if_break_bb884
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6558
	ld23.dw 0x000003B8[r3], r8
	ld23.dw 0x00000000[r10], r10
	ld23.dw 0x00000000[r5], r14
	cmpf.d 0x00000005, r26, r8
	ld23.dw 0x00000000[r2], r16
	trfsr 0
	bz9 .BB.LABEL.2_59
.BB.LABEL.2_58:	; if_break_bb884.if_break_bb929_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x00000108[r3], r2
	ld.w 0x00000104[r3], r5
	ld.w 0x000000A0[r3], r8
	br9 .BB.LABEL.2_60
.BB.LABEL.2_59:	; if_else_bb910
	ld.w 0x00000128[r3], r2
	ld.w 0x00000124[r3], r5
	ld.w 0x000000A4[r3], r8
.BB.LABEL.2_60:	; if_break_bb929
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6596
	ld23.dw 0x000003F0[r3], r18
	cmpf.d 0x00000006, r18, r6
	trfsr 0
	bnz9 .BB.LABEL.2_62
.BB.LABEL.2_61:	; if_then_bb936
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x00000110[r3], r2
	ld.w 0x0000010C[r3], r5
	ld.w 0x000000A8[r3], r8
.BB.LABEL.2_62:	; if_break_bb971
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6636
	ld23.dw 0x00000410[r3], r6
	ld23.dw 0x00000028[r3], r30
	ld23.dw 0x00000000[r8], r8
	ld23.dw 0x00000000[r5], r18
	cmpf.d 0x00000005, r30, r6
	ld23.dw 0x00000000[r2], r28
	trfsr 0
	bz9 .BB.LABEL.2_64
.BB.LABEL.2_63:	; if_break_bb971.if_break_bb1016_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w 0x00000118[r3], r2
	ld.w 0x00000114[r3], r5
	ld.w 0x000000AC[r3], r6
	br9 .BB.LABEL.2_65
.BB.LABEL.2_64:	; if_else_bb997
	ld.w 0x00000120[r3], r2
	ld.w 0x0000011C[r3], r5
	ld.w 0x000000B0[r3], r6
.BB.LABEL.2_65:	; if_break_bb1016
	mov 0x00000000, r30
	mov 0x00000000, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6720
	cmpf.d 0x00000002, r26, r30
	trfsr 0
	bz9 .BB.LABEL.2_67
.BB.LABEL.2_66:	; if_then_bb1108
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6689
	ld23.dw 0x00000030[r3], r26
	ld23.dw 0x00000048[r3], r30
	mulf.d r26, r30, r26
	ld23.dw 0x00000010[r3], r30
	mulf.d r30, r26, r26
	mulf.d r12, r26, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6681
	ld23.dw 0x00000050[r3], r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6689
	mulf.d r16, r12, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6681
	ld23.dw 0x00000038[r3], r16
	mulf.d r16, r26, r16
	ld23.dw 0x00000018[r3], r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6689
	mulf.d r28, r12, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6681
	mulf.d r26, r16, r16
	ld23.dw 0x00000000[r3], r26
	ld23.dw 0x00000000[r6], r6
	mulf.d r26, r16, r16
	mulf.d r14, r16, r14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6673
	ld23.dw 0x00000040[r3], r16
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6681
	mulf.d r18, r14, r14
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6673
	ld23.dw 0x00000058[r3], r18
	mulf.d r16, r18, r16
	ld23.dw 0x00000020[r3], r18
	mulf.d r18, r16, r16
	ld23.dw 0x00000008[r3], r18
	mulf.d r18, r16, r16
	mulf.d r10, r16, r10
	ld23.dw 0x00000000[r5], r16
	mulf.d r8, r10, r8
	ld23.dw 0x00000000[r2], r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6689
	mulf.d r10, r12, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6681
	mulf.d r16, r14, r12
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6673
	mulf.d r6, r8, r6
	br9 .BB.LABEL.2_68
.BB.LABEL.2_67:	; if_else_bb1124
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6728
	ld23.dw 0x00000448[r3], r6
	cmovf.d 0, r6, r6, r12
	cmovf.d 0, r6, r6, r10
.BB.LABEL.2_68:	; if_break_bb1137
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	st23.dw r6, 0x00000150[r3]
	st23.dw r12, 0x00000158[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6787
	trncf.dw r6, r6
	st23.dw r10, 0x00000160[r3]
	ld23.hu 0xFFC10004[r0], r2
	ld23.hu 0xFFC10008[r0], r5
	cmp 0xFFFFFFFF, r6
	bz9 .BB.LABEL.2_72
.BB.LABEL.2_69:	; if_break_bb1137
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	cmp 0x00000001, r6
	bz9 .BB.LABEL.2_71
.BB.LABEL.2_70:	; switch_clause_bb1183
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6799
	andi 0x0000FFD3, r5, r5
	br9 .BB.LABEL.2_73
.BB.LABEL.2_71:	; switch_clause_bb1157
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6789
	ori 0x00000020, r2, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6790
	andi 0x0000FFD3, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6789
	andi 0x0000BFBF, r2, r2
	br9 .BB.LABEL.2_74
.BB.LABEL.2_72:	; switch_clause_bb1170
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6794
	ori 0x00000004, r5, r5
	andi 0x0000FFD7, r5, r5
.BB.LABEL.2_73:	; switch_clause_bb1170
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	andi 0x0000BF9F, r2, r2
.BB.LABEL.2_74:	; bb1146.split.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6787
	trncf.dw r12, r6
	cmp 0xFFFFFFFF, r6
	bz9 .BB.LABEL.2_78
.BB.LABEL.2_75:	; bb1146.split.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	cmp 0x00000001, r6
	bz9 .BB.LABEL.2_77
.BB.LABEL.2_76:	; switch_clause_bb1183.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6799
	andi 0x0000FFF7, r5, r5
	br9 .BB.LABEL.2_79
.BB.LABEL.2_77:	; switch_clause_bb1157.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6790
	andi 0x0000FFF7, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6789
	ori 0x00000040, r2, r2
	br9 .BB.LABEL.2_80
.BB.LABEL.2_78:	; switch_clause_bb1170.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6794
	ori 0x00000008, r5, r5
.BB.LABEL.2_79:	; switch_clause_bb1170.ul
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	andi 0x0000FFBF, r2, r2
.BB.LABEL.2_80:	; bb1146.split.ul266
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6787
	trncf.dw r10, r6
	cmp 0xFFFFFFFF, r6
	bz9 .BB.LABEL.2_84
.BB.LABEL.2_81:	; bb1146.split.ul266
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	cmp 0x00000001, r6
	bz9 .BB.LABEL.2_83
.BB.LABEL.2_82:	; switch_clause_bb1183.ul265
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6799
	andi 0x0000FFDF, r5, r5
	br9 .BB.LABEL.2_85
.BB.LABEL.2_83:	; switch_clause_bb1157.ul247
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6790
	andi 0x0000FFDF, r5, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6789
	ori 0x00004000, r2, r2
	br9 .BB.LABEL.2_86
.BB.LABEL.2_84:	; switch_clause_bb1170.ul260
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6794
	ori 0x00000020, r5, r5
.BB.LABEL.2_85:	; switch_clause_bb1170.ul260
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	andi 0x0000BFFF, r2, r2
.BB.LABEL.2_86:	; bb1204
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_sil.h", 110
	st23.h r2, 0xFFC10004[r0]
	mov 0x00000001, r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6816
	mov r26, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/arch/v850_ccrh/prc_sil.h", 110
	st23.h r5, 0xFFC10008[r0]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6816
	jarl _WaitEvent, r31
	mov r26, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6819
	add 0x00000001, r20
	jr .BB.LABEL.2_1
	.align 4
_TaskMainBlsmControl_Core1_100_0_Task:
	.stack _TaskMainBlsmControl_Core1_100_0_Task = 52
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 6970
	prepare 0x000007FF, 0x00000008
	mov 0x00000002, r2
	mov r3, r5
.BB.LABEL.3_1:	; bb.i.split.clone
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 47
	st.b r0, 0x00000000[r5]
	st.b r0, 0x00000001[r5]
	st.b r0, 0x00000002[r5]
	st.b r0, 0x00000003[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 46
	add 0x00000004, r5
	loop r2, .BB.LABEL.3_1
.BB.LABEL.3_2:	; memset.1.exit
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	mov 0x00000001, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7251
	st.w r0, 0x00000004[r3]
	st.w r0, 0x00000000[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7319
	mov r20, r6
	jarl _WaitEvent, r31
	mov r20, r6
	jarl _ClearEvent, r31
	movea 0x00000064, r0, r20
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7336
	movhi HIGHW1(#_CH_BlsmControl_0015_0076), r0, r21
	mov 0x00000002, r22
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7383
	movhi HIGHW1(#_CH_BlsmControl_0043_0077), r0, r23
	mov 0x00000004, r24
.BB.LABEL.3_3:	; bb83.outer
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w LOWW(#_CH_BlsmControl_0015_0076)[r21], r2
	cmp 0x00000000, r2
	bgt9 .BB.LABEL.3_5
.BB.LABEL.3_4:	; bb79
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7350
	mov r22, r6
	jarl _WaitEvent, r31
	mov r22, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7352
	syncm
	br9 .BB.LABEL.3_3
.BB.LABEL.3_5:	; if_then_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7338
	mov #_CH_BlsmControl_0015_0076, r5
	ld23.dw 0x00000008[r5], r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7339
	addi 0xFFFFFFFF, r2, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7341
	cmp 0x00000001, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7340
	st.w r6, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7341
	bnz9 .BB.LABEL.3_7
.BB.LABEL.3_6:	; bb72
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7342
	syncm
	mov 0x00000001, r2
	mov r2, r6
	mov r24, r7
	jarl _SetEvent, r31
	br9 .BB.LABEL.3_8
.BB.LABEL.3_7:	; if_else_bb
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7345
	syncm
.BB.LABEL.3_8:	; bb115.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	ld.w LOWW(#_CH_BlsmControl_0043_0077)[r23], r2
	cmp 0x00000000, r2
	bgt9 .BB.LABEL.3_10
.BB.LABEL.3_9:	; bb111
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7397
	mov r22, r6
	jarl _WaitEvent, r31
	mov r22, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7399
	syncm
	br9 .BB.LABEL.3_8
.BB.LABEL.3_10:	; if_then_bb92
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7385
	mov #_CH_BlsmControl_0043_0077, r5
	ld23.dw 0x00000008[r5], r28
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7386
	addi 0xFFFFFFFF, r2, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7388
	cmp 0x00000001, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7387
	st.w r6, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7388
	bnz9 .BB.LABEL.3_12
.BB.LABEL.3_11:	; bb102
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7389
	syncm
	mov 0x00000000, r2
	mov r2, r6
	mov r24, r7
	jarl _SetEvent, r31
	br9 .BB.LABEL.3_13
.BB.LABEL.3_12:	; if_else_bb105
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7392
	syncm
.BB.LABEL.3_13:	; bb117
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7528
	subf.d r28, r26, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7551
	divf.d r28, r6, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7573
	jarl _fabs, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7433
	divf.d r28, r26, r8
	mov 0x00000000, r12
	movhi 0x00003FF0, r0, r13
	mov 0x9999999A, r14
	mov 0x3FB99999, r15
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7458
	subf.d r12, r8, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7624
	ld23.dw 0x00000000[r3], r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7482
	mulf.d r14, r8, r8
	mov 0x47AE147B, r14
	mov 0x3F847AE1, r15
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7597
	cmpf.d 0x00000004, r14, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7506
	addf.d r12, r8, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7601
	cmovf.d 0, r8, r12, r8
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7633
	mulf.d r6, r8, r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7659
	cmpf.d 0x00000007, r26, r12
	trfsr 0
	bz9 .BB.LABEL.3_15
.BB.LABEL.3_14:	; bb117.bb211.preheader_crit_edge
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	mov 0x00000000, r26
	movhi 0x00003FF0, r0, r27
	br9 .BB.LABEL.3_16
.BB.LABEL.3_15:	; if_else_bb171
	mov 0x00000000, r6
	mov 0x00000000, r7
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7661
	cmpf.d 0x00000007, r6, r26
	trfsr 0
	cmov 0x0000000A, 0x00000000, r26, r26
	cmov 0x0000000A, 0x00000000, r27, r27
.BB.LABEL.3_16:	; bb211.preheader
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	movhi HIGHW1(#_CH_BlsmControl_0039_0037), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0039_0037)[r2], r2
	cmp 0x00000000, r2
	bz9 .BB.LABEL.3_18
.BB.LABEL.3_17:	; bb207
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7694
	mov r24, r6
	jarl _WaitEvent, r31
	mov r24, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7696
	syncm
	br9 .BB.LABEL.3_16
.BB.LABEL.3_18:	; if_then_bb188
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7684
	mov #_CH_BlsmControl_0039_0037, r2
	st23.dw r26, 0x00000008[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7685
	syncm
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7686
	ld.w 0x00000004[r2], r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7688
	st.w r20, 0x00000004[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7686
	subr r20, r5
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7687
	st.w r5, 0x00000000[r2]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7689
	syncm
	mov 0x00000000, r2
	mov r2, r6
	mov r22, r7
	jarl _SetEvent, r31
	br9 .BB.LABEL.3_20
.BB.LABEL.3_19:	; bb238
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7760
	mov r22, r6
	jarl _WaitEvent, r31
	mov r22, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7762
	syncm
.BB.LABEL.3_20:	; bb238
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	movhi HIGHW1(#_CH_BlsmControl_0036_0038), r0, r2
	ld.w LOWW(#_CH_BlsmControl_0036_0038)[r2], r2
	cmp 0x00000000, r2
	ble9 .BB.LABEL.3_19
.BB.LABEL.3_21:	; if_then_bb219
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7748
	mov #_CH_BlsmControl_0036_0038, r5
	ld23.dw 0x00000008[r5], r26
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7749
	addi 0xFFFFFFFF, r2, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7751
	cmp 0x00000001, r2
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7750
	st.w r6, 0x00000000[r5]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7751
	bnz9 .BB.LABEL.3_23
.BB.LABEL.3_22:	; bb229
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7752
	syncm
	mov 0x00000000, r2
	mov r2, r6
	mov r24, r7
	jarl _SetEvent, r31
	br9 .BB.LABEL.3_24
.BB.LABEL.3_23:	; if_else_bb232
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7755
	syncm
.BB.LABEL.3_24:	; bb244
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 0
	mov 0x00000001, r25
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7790
	mov r25, r6
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7774
	st23.dw r26, 0x00000000[r3]
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7790
	jarl _WaitEvent, r31
	mov r25, r6
	jarl _ClearEvent, r31
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/OBJ_BLSM/configure/blsm/parallel-BlsmControl.c", 7793
	movea 0x00000064, r20, r20
	jr .BB.LABEL.3_3
	.section .bss, bss
	.align 2
_blsm_speed:
	.ds (2)
	.align 4
_CH_BlsmControl_0002_0088:
	.ds (12)
	.align 4
_CH_BlsmControl_0015_0076:
	.ds (16)
	.align 4
_CH_BlsmControl_0036_0038:
	.ds (16)
	.align 4
_CH_BlsmControl_0037_0072:
	.ds (12)
	.align 4
_CH_BlsmControl_0038_0035:
	.ds (12)
	.align 4
_CH_BlsmControl_0039_0037:
	.ds (16)
	.align 4
_CH_BlsmControl_0043_0077:
	.ds (16)
