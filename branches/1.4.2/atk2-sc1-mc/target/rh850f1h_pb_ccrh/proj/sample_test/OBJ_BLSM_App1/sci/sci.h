#ifndef __SCI_H__
#define __SCI_H__

extern void SciInit( unsigned char ch );
extern void SciEnable( unsigned char ch );
extern void SciDisable( unsigned char ch );
extern char SciRead( unsigned char ch );
extern void SciWrite( unsigned char ch, unsigned char data );
extern void SciPrint( unsigned char ch, char data );
extern void SciPrintByte( unsigned char ch, unsigned char dmy );
extern void SciPrintWord( unsigned char ch, unsigned short dmy );
extern void SciPrintDword( unsigned char ch, unsigned long dmy );
extern void SciPrintByteInt( unsigned char ch, unsigned char dmy );
extern void sciPrintShortInt( unsigned char ch, unsigned short dmy );


#endif