//sci.c

#define ERROR	-1
#define NOERROR	0

#define _PORT_S_0  0x0001
#define _PORT_S_1  0x0002
#define _PORT_S_2  0x0004
#define _PORT_S_3  0x0008
#define _PORT_S_4  0x0010
#define _PORT_S_5  0x0020
#define _PORT_S_6  0x0040
#define _PORT_S_7  0x0080
#define _PORT_S_8  0x0100
#define _PORT_S_9  0x0200
#define _PORT_S_10 0x0400
#define _PORT_S_11 0x0800
#define _PORT_S_12 0x1000
#define _PORT_S_13 0x2000
#define _PORT_S_14 0x4000
#define _PORT_S_15 0x8000

#include "../iodefine.h"

#pragma interrupt int_uart30_rcv(channel=35, enable=true, callt=true, fpu=true)
#pragma interrupt int_uart31_rcv(channel=122, enable=true, callt=true, fpu=true)

void int_uart30_rcv(void);
void int_uart31_rcv(void);
void sciInit( unsigned char ch );
char sciRead( unsigned char ch );
void sciWrite( unsigned char ch, unsigned char data );
void sciPrint( unsigned char ch, char *data );
void sciPrintByte( unsigned char ch, unsigned char dmy );
void sciPrintWord( unsigned char ch, unsigned short dmy );
void sciPrintDword( unsigned char ch, unsigned long dmy );
void sciPrintByteInt( unsigned char ch, unsigned char dmy );

static const char disp_table[] = "0123456789ABCDEF";		// 文字列テーブル
unsigned char sciReadBuff_30;
unsigned char sciReadBuff_31;
unsigned char rxiFlag_30=0;
unsigned char rxiFlag_31=0;

/** 文字出力
 * @param 文字
 * @retval void
 */
void sciWrite( unsigned char ch, unsigned char Buffer )
{
	switch(ch)
	{
		case 0:
			// 送信データ設定
			RLN30.LUTDR.UINT16 = Buffer;
	
			// 送信状態監視
			while( (RLN30.LST & 0x10) != 0)
			{
				;
			}
			break;
			
		case 1:
			// 送信データ設定
			RLN31.LUTDR.UINT16 = Buffer;
	
			// 送信状態監視
			while( (RLN31.LST & 0x10) != 0)
			{
				;
			}
			break;
	}
}

/** 文字入力
 * @param 文字
 * @retval void
 */
char sciRead( unsigned char ch )
{
	switch(ch)
	{
		case 0:
			if( rxiFlag_30 != 1 )
			{
				return	( char )( 0xff );
			}
			else
			{
				rxiFlag_30 = 0;
				return ( char )( sciReadBuff_30 );
			}
			break;
			
		case 1:
			if( rxiFlag_31 != 1 )
			{
				return	( char )( 0xff );
			}
			else
			{
				rxiFlag_31 = 0;
				return ( char )( sciReadBuff_31 );
			}
			break;
	}
}

/** 受信割り込み
 * @param 文字
 * @retval void
 */
void int_uart30_rcv(void)
{
	sciReadBuff_30 = (unsigned char)RLN30.LURDR.UINT16;

	rxiFlag_30 = 1;
}

void int_uart31_rcv(void)
{
	sciReadBuff_31 = (unsigned char)RLN31.LURDR.UINT16;

	rxiFlag_31 = 1;
}


/** 文字列出力
 * @param 文字列ポインタ
 * @retval void
 */
void sciPrint( unsigned char ch, char *Buffer )
{
	while( *Buffer != '\0' )
	{
		/* 終端検出 */
		if( *Buffer == '\n' )
		{
			/* 改行出力 */
			sciWrite(ch, 0x0d);
			sciWrite(ch, 0x0a);
		}
		/* 文字出力 */
		else
		{
			sciWrite(ch, *Buffer );
		}
		Buffer++;
	}
}

/** 文字列出力(Byteデータ)
 * @param Byteデータ
 * @retval void
 */
void sciPrintByte( unsigned char ch, unsigned char dmy )
{
	sciWrite( ch, disp_table[ ((dmy & 0xf0) >> 4) ] );
	sciWrite( ch, disp_table[ (dmy & 0x0f) ] );
}

void sciPrintByteInt( unsigned char ch, unsigned char dmy )
{
	unsigned char n[3];
	n[0] = dmy % 10;
	dmy /= 10;
	n[1] =  dmy % 10;
	dmy /= 10;
	n[2] =  dmy % 10;
	
	if(n[2] != 0)
	{
		sciWrite(ch, disp_table[n[2]]);
		sciWrite(ch, disp_table[n[1]]);
	}
	else if(n[1] != 0)
	{
		sciWrite(ch, disp_table[n[1]]);
	}

	sciWrite(ch, disp_table[n[0]]);
		
}

void sciPrintShortInt( unsigned char ch, unsigned short dmy )
{
	unsigned char n[6];
	n[0] = dmy % 10;
	dmy /= 10;
	n[1] =  dmy % 10;
	dmy /= 10;
	n[2] =  dmy % 10;
	dmy /= 10;
	n[3] =  dmy % 10;
	dmy /= 10;
	n[4] =  dmy % 10;
	dmy /= 10;
	n[5] =  dmy % 10;
	
	if(n[5] != 0)
	{
		sciWrite(ch, disp_table[n[5]]);
		sciWrite(ch, disp_table[n[4]]);
		sciWrite(ch, disp_table[n[3]]);
		sciWrite(ch, disp_table[n[2]]);
		sciWrite(ch, disp_table[n[1]]);
	}
	else if(n[4] != 0)
	{
		sciWrite(ch, disp_table[n[4]]);
		sciWrite(ch, disp_table[n[3]]);
		sciWrite(ch, disp_table[n[2]]);
		sciWrite(ch, disp_table[n[1]]);
	}
	else if(n[3] != 0)
	{
		sciWrite(ch, disp_table[n[3]]);
		sciWrite(ch, disp_table[n[2]]);
		sciWrite(ch, disp_table[n[1]]);
	}
	else if(n[2] != 0)
	{
		sciWrite(ch, disp_table[n[2]]);
		sciWrite(ch, disp_table[n[1]]);
	}
	else if(n[1] != 0)
	{
		sciWrite(ch, disp_table[n[1]]);
	}

	sciWrite(ch, disp_table[n[0]]);
		
}

/** 文字列出力(WORDデータ)
 * @param WORDデータ
 * @retval void
 */
void sciPrintWord( unsigned char ch, unsigned short dmy )
{
	unsigned long i;
	char	data[2];

	data[1] = (unsigned char)( (dmy & 0x00ff) );
	data[0] = (unsigned char)( (dmy & 0xff00) >> 8 );

	for( i = 0; i < 2; i++ )
	{
		sciWrite(ch, disp_table[ ((data[i] & 0xf0) >> 4) ] );
		sciWrite(ch, disp_table[ (data[i] & 0x0f) ] );
	}
}

/** 文字列出力(DWORDデータ)
 * @param DWORDデータ
 * @retval void
 */
void sciPrintDword( unsigned char ch, unsigned long dmy )
{
	unsigned long i;
	char	data[4];

	data[3] = (unsigned char)( dmy & 0x000000ff );
	data[2] = (unsigned char)( (dmy & 0x0000ff00) >> 8 );
	data[1] = (unsigned char)( (dmy & 0x00ff0000) >> 16 );
	data[0] = (unsigned char)( (dmy & 0xff000000) >> 24 );

	for( i = 0; i < 4; i++ )
	{
		sciWrite(ch,  disp_table[ ((data[i] & 0xf0) >> 4) ] );
		sciWrite(ch, disp_table[ (data[i] & 0x0f) ] );
	}
}

void sciInit( unsigned char ch )
{
	switch(ch)
	{
		case 0:
			//UART
			//P10_9  第2兼用機能 RLIN30RX
			//P10_10 第2兼用機能 RLIN30TX
			PMC10 |= _PORT_S_9;
			PMC10 |= _PORT_S_10;
			
			PFCAE10 &= ~_PORT_S_9;
			PFCAE10 &= ~_PORT_S_10;

			PFCE10 &= ~_PORT_S_9;
			PFCE10 &= ~_PORT_S_10;
	
			PFC10 |= _PORT_S_9;
			PFC10 |= _PORT_S_10;
	
			PU10 |= _PORT_S_9;		//pull-up
			PU10 |= _PORT_S_10;		//pull-up
	
			PM10 |= _PORT_S_9;
			PM10 &= ~_PORT_S_10;

			RLN30.LCUC = 0x0;		//リセット
			RLN30.LWBR = 0x0;
			RLN30.LBRP01.UINT16 = 0x0040;	//速度設定(38400bps)
			RLN30.LMD = 0x1;		//UARTモード
			RLN30.LBFC = 0x0;
			RLN30.LSC = 0x0;
			RLN30.LEDE = 0x0;
			RLN30.LDFC = 0x1;		//バッファ1データ, すぐに送信
	
			RLN30.LCUC = 0x1;		//リセット解除
	
			RLN30.LUOER = 0x3;		//送受信許可
	
			ICRLIN30UR1 = 0x48;		//受信割り込み許可, テーブル方式, 優先度=8
			
			break;
			
		case 1:
			//UART
			//P0_4 第1兼用機能 RLIN31RX
			//P0_5 第1兼用機能 RLIN31TX
			PMC0 |= _PORT_S_4;
			PMC0 |= _PORT_S_5;
			
			PFCAE0 &= ~_PORT_S_4;
			PFCAE0 &= ~_PORT_S_5;

			PFCE0 &= ~_PORT_S_4;
			PFCE0 &= ~_PORT_S_5;
	
			PFC0 &= ~_PORT_S_4;
			PFC0 &= ~_PORT_S_5;
	
			PU0 |= _PORT_S_4;		//pull-up
			PU0 |= _PORT_S_5;		//pull-up
	
			PM0 |= _PORT_S_4;
			PM0 &= ~_PORT_S_5;

			RLN31.LCUC = 0x0;		//リセット
			RLN31.LWBR = 0x0;
			RLN31.LBRP01.UINT16 = 0x0040;	//速度設定(38400bps)
			RLN31.LMD = 0x1;		//UARTモード
			RLN31.LBFC = 0x0;
			RLN31.LSC = 0x0;
			RLN31.LEDE = 0x0;
			RLN31.LDFC = 0x1;		//バッファ1データ, すぐに送信
	
			RLN31.LCUC = 0x1;		//リセット解除
	
			RLN31.LUOER = 0x3;		//送受信許可
	
			ICRLIN31UR1 = 0x48;		//受信割り込み許可, テーブル方式, 優先度=8
			
			break;
	}

}

void sciDisable( unsigned char ch )
{

	switch(ch)
	{
		case 0:
			RLN30.LUOER = 0x0;		//送受信禁止
			ICRLIN31UR0 = 1;		//受信割り込み禁止
			RLN30.LCUC = 0x0;		//リセット
			break;
		case 1:
			RLN31.LUOER = 0x0;		//送受信禁止
			ICRLIN31UR1 = 1;		//受信割り込み禁止
			RLN31.LCUC = 0x0;		//リセット
			break;
	}
	
}

void sciEnable( unsigned char ch )
{
	
	switch(ch)
	{
		case 0:
			RLN30.LCUC = 0x1;		//リセット解除
			RLN30.LUOER = 0x3;		//送受信許可
			ICRLIN30UR1 = 0;		//受信割り込み許可
			break;
		case 1:
			RLN31.LCUC = 0x1;		//リセット解除
			RLN31.LUOER = 0x3;		//送受信許可
			ICRLIN31UR1 = 0;		//受信割り込み許可
			break;
	}

}
