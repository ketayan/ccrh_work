#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\while_volatile.c -oDefaultBuild\while_volatile.obj -Xcommon=rh850 -Xcpu=g3m -g -g_line -I..\cfg -I..\..\..\arch\v850_ccrh -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\arch\ccrh -I..\..\..\arch\v850_gcc -I..\..\..\include -I..\..\..\arch -I..\..\..\arch\ccrh -I..\..\..\arch\gcc -I..\..\..\arch\logtrace -I..\..\..\arch\v850_ccrh -I..\..\..\arch\v850_gcc -I..\..\..\arch\v850_ghs -I..\..\..\arch\ccrh\configure -I..\..\..\sysmod -I..\..\.. -I..\..\..\cfg\cfg -I.. -I..\blsm -I..\sci -I..\cfg -Xalign4 -Xasm_path=DefaultBuild -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_udrawzhf.bp5
#@	compiled at Tue Aug 07 17:59:15 2018

	.file "..\..\..\while_volatile.c"

	$reg_mode 32

	.extern _a
	.public _func
	.public _main

	.section .text, text
	.align 4
_func:
	.stack _func = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while_volatile.c", 5
	movhi HIGHW1(#_a), r0, r2
	st.w r0, LOWW(#_a)[r2]
.BB.LABEL.1_1:	; bb1
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while_volatile.c", 6
	movhi HIGHW1(#_a), r0, r2
	ld.w LOWW(#_a)[r2], r2
	cmp 0x00000000, r2
	bz9 .BB.LABEL.1_1
.BB.LABEL.1_2:	; return
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while_volatile.c", 8
	jmp [r31]
	.align 4
_main:
	.stack _main = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while_volatile.c", 10
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while_volatile.c", 11
	jarl _func, r31
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while_volatile.c", 12
	dispose 0x00000000, 0x00000001, [r31]
