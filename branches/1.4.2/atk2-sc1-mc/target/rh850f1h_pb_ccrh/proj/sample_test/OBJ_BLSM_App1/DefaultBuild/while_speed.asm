#CC-RH Compiler RH850 Assembler Source
#@	CC-RH Version : V1.07.00 [30 Nov 2017]
#@	Command : ..\..\..\while.c -oDefaultBuild\while.obj -Xcommon=rh850 -Xcpu=g3m -g -g_line -Ospeed -Oinline_size -I..\cfg -I..\..\..\arch\v850_ccrh -I..\..\..\target\rh850f1h_pb_ccrh -I..\..\..\arch\ccrh -I..\..\..\arch\v850_gcc -I..\..\..\include -I..\..\..\arch -I..\..\..\arch\ccrh -I..\..\..\arch\gcc -I..\..\..\arch\logtrace -I..\..\..\arch\v850_ccrh -I..\..\..\arch\v850_gcc -I..\..\..\arch\v850_ghs -I..\..\..\arch\ccrh\configure -I..\..\..\sysmod -I..\..\.. -I..\..\..\cfg\cfg -I.. -I..\blsm -I..\sci -I..\cfg -Xalign4 -Xasm_path=DefaultBuild -c -Xexec_time=C:\Users\Sasuga\AppData\Local\Temp\CSPlusBuildTool_o0al3yr1.4cd
#@	compiled at Tue Aug 07 17:55:50 2018

	.file "..\..\..\while.c"

	$reg_mode 32

	.extern _a
	.public _func
	.public _main

	.section .text, text
	.align 4
_func:
	.stack _func = 0
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while.c", 5
	movhi HIGHW1(#_a), r0, r2
	st.w r0, LOWW(#_a)[r2]
.BB.LABEL.1_1:	; bb1
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while.c", 6
	br9 .BB.LABEL.1_1
	.align 4
_main:
	.stack _main = 4
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while.c", 10
	prepare 0x00000001, 0x00000000
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while.c", 11
	jarl _func, r31
	mov 0x00000000, r10
	.line "C:/Users/Sasuga/Documents/multicoreプロジェクト/RCCAR_SHORT/while.c", 12
	dispose 0x00000000, 0x00000001, [r31]
