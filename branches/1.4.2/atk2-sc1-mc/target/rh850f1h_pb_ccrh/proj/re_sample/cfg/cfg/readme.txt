This archive contains the following files:

readme.txt
    This file

AUTOSAR_4-0-3.xsd 
    This is the AUTOSAR XML Schema provided as AUTOSAR standard.
    Note that this is the only file in this archive which is STANDARD.

AUTOSAR_4-0-3_COMPACT.xsd
    This AUXILIARY file provides a compacted AUTOSAR XML schema which is 
    optimized for performance. In particular the named groups are replaced
    by its contents. Also nested unnamed groups are optimized such that 
    an XML parser does not need to evaluate too many options.

AUTOSAR_4-0-3_STRICT.xsd
    This AUXILIARY file provides a AUTOSAR XML schema which strictly follows
    the multiplicity in the meta-model. This may be used for more strict validation
    of the structures.

AUTOSAR_4-0-3_STRICT_COMPACT.xsd
    This AUXILIARY file provides a compacted strict AUTOSAR XML schema which is 
    optimized for performance. In particular the named groups are replaced
    by its contents. Also nested unnamed groups are optimized such that 
    an XML parser does not need to evaluate too many options.

AUTOSAR_4-0-3.css
    This AUXILIARY file provides an initial css stylesheet for visualization of 
    AUTOSAR XML files 

autosar.soc
    This AUXILIARY file provides an OASIS open catalog file which refers 
    to the AUTOSAR XSD files. It is intended as an illustration how to
    specify such a catalog file.

xml.xsd
    This is the schema for the xml namespace as provided by W3C.
