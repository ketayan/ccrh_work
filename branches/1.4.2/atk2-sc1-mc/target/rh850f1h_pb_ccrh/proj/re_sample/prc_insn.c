#include "kernel_impl.h"
#include "prc_insn.h"
int func1(int aaa){
	return 1+aaa;
}
int func2(int aaa){
	int temp;
	temp = func1(aaa+1);
	return temp;
}
int main()
{
    uint32 test=0;
    func2(test);
    acquire_lock_ldlstc(&test);
    enable_int();
    return 0;
}

