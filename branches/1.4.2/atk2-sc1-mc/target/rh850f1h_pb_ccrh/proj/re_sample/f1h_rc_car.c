/*
 *  TOPPERS Software
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *
 *  Copyright (C) 2014-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id$
 */


#include "Os.h"
#include "t_syslog.h"
#include "t_stdlib.h"
#include "sysmod/serial.h"
#include "sysmod/syslog.h"
#include "sysmod/banner.h"
#include "target_sysmod.h"
#include "target_serial.h"
#include "target_hw_counter.h"

//#include "target_rename.h"

#include "ModelCarControl.h"

#ifdef TOPPERS_ENABLE_TRACE
#include "logtrace/trace_config.h"
#endif

//#include "iodefine.h"

sint32 main(void);
//void port_init(void);

/*
 *  制御系へのコマンド更新通知用変数(同一ECU時)
 */
volatile boolean UpDateContCmd = FALSE;

/*
 *  制御系への指示用構造体
 */
volatile CONTCMD g_contcmd;

/*
 *  ボディ系への状態指示用変数
 */
volatile BODYCMD g_bodycmd;

/*
 * TOPPERS/ATK2 main function
 */
sint32
main (void)
{
  StatusType ercd;
  CoreIdType i;
  CoreIdType coreid = GetCoreID ();

  if (coreid == OS_CORE_ID_MASTER) {
    for (i = 0; i < TNUM_HWCORE; i++) {
      if (i != OS_CORE_ID_MASTER)
	StartCore (i, &ercd);
    }
    StartOS (BlsmControl_AppMode);
//    StartOS (Test_AppMode);
  } else {
    StartOS (BlsmControl_AppMode);
//    StartOS (Test_AppMode);
  }
  while(1){}
  return 0;
}

/*
 * Startup Hook
 */
void StartupHook(void)
{
  CoreIdType coreid = GetCoreID ();

  if (coreid == OS_CORE_ID_MASTER) {
    syslog_initialize ();
    syslog_msk_log (LOG_UPTO(LOG_INFO));
    InitSerial ();
    print_banner ();

    //port_init();
    //over_current_port_init();

    swcnt_init();
    swcnt_start();

#ifdef TOPPERS_ENABLE_TRACE
    mesurecnt_init();
    trace_initialize(TRACE_AUTOSTOP);
#endif

    OperationInit();
    DriveInit();
    BodyControlInit();

  } else {
    InitSerial ();
  }

  syslog(LOG_EMERG, "StartupHook @ core%d", coreid);
}

/*
 * Shutdown Hook
 */
void ShutdownHook(StatusType Error)
{
  CoreIdType coreid = GetCoreID ();
  if (coreid == OS_CORE_ID_MASTER) {

    blsm_motor_stop();
#ifdef TOPPERS_ENABLE_TRACE
    trace_dump(target_fput_log);
#endif
    swcnt_stop();

  }
  syslog(LOG_EMERG, "ShutdownHook @ core%d", coreid);
}


//void port_init(void)
//{
//  //LED出力設定
//  AP1 |= 0xf000;    //AP1_12~15: H
//  APM1 &= ~0xf000;  //AP1_12~15: out
//
//  //SW入力設定
//  APIBC1 |= 0x07e;  //AP1_1~6:DIP-SW, PUSH-SW 入力バッファ有効
//
//}


/*
 *  Port 20 Configration for PWGA21
 *   P20_2 : INTP4 : 第1兼用入力
 */
#define INTP4_P20_MASK     ((uint16) 0x0004)
#define INTP4_PIPC20_INIT    ((uint16) 0x0000)
#define INTP4_PMC20_INIT   ((uint16) 0x0004)
#define INTP4_PFCAE20_INIT   ((uint16) 0x0000)
#define INTP4_PFCE20_INIT    ((uint16) 0x0000)
#define INTP4_PFC20_INIT   ((uint16) 0x0000)
#define INTP4_PM20_INIT    ((uint16) 0x0004)

/*
 *  過電流検出割込み設定
*/
over_current_port_init()
{
  uint16 wk;

  /*
   * PORT20(INTP4)
   */
  /* PIPC20 設定 */
  wk = sil_reh_mem((void *) PIPC(20));
  wk &= ~INTP4_P20_MASK;
  wk |= (INTP4_PIPC20_INIT & INTP4_P20_MASK);
  sil_wrh_mem((void *) PIPC(20), wk);

  /* PFCAE20 設定 */
  wk = sil_reh_mem((void *) PFCAE(20));
  wk &= ~INTP4_P20_MASK;
  wk |= (INTP4_PFCAE20_INIT & INTP4_P20_MASK);
  sil_wrh_mem((void *) PFCAE(20), wk);

  /* PFCE20 設定 */
  wk = sil_reh_mem((void *) PFCE(20));
  wk &= ~INTP4_P20_MASK;
  wk |= (INTP4_PFCE20_INIT & INTP4_P20_MASK);
  sil_wrh_mem((void *) PFCE(20), wk);

  /* PFC20 設定 */
  wk = sil_reh_mem((void *) PFC(20));
  wk &= ~INTP4_P20_MASK;
  wk |= (INTP4_PFC20_INIT & INTP4_P20_MASK);
  sil_wrh_mem((void *) PFC(20), wk);

  /* PMC20 設定 */
  wk = sil_reh_mem((void *) PMC(20));
  wk &= ~INTP4_P20_MASK;
  wk |= (INTP4_PMC20_INIT & INTP4_P20_MASK);
  sil_wrh_mem((void *) PMC(20), wk);

  /* PM20 設定 */
  wk = sil_reh_mem((void *) PM(20));
  wk &= ~INTP4_P20_MASK;
  wk |= (INTP4_PM20_INIT & INTP4_P20_MASK);
  sil_wrh_mem((void *) PM(20), wk);
}

/*
 *  過電流検出割込み
*/
//ISR(OverCurrentHdr)
//{
//    DriveStop();
//    internal_shutdownallcores(E_OK);
//}