;   Copyright(c) 2013, 2016 	Renesas Electronics Corporation
;   RENESAS ELECTRONICS CONFIDENTIAL AND PROPRIETARY.
;   This program must be used solely for the purpose for which
;   it was furnished by Renesas Electronics Corporation. No part of this
;   program may be reproduced or disclosed to others, in any
;   form, without the prior written permission of Renesas Electronics
;   Corporation.

;   NOTE       : THIS IS A TYPICAL EXAMPLE.
;   DATE       : Tue, Jan 12, 2016

	; if using eiint as table reference method,
	; enable next line's macro.

.section "RESET_PE1", text
.align 512
RESET_PE1 .cseg text
    .extern __reset
__reset:
    jr32 __start
    .align 16
__syserr: ;/* 0x0010 */
    jr32 _kernel_fe_exception_entry
    .align 16
__hvtrap: ;/* 0x0020 */
    jr32 _kernel_fe_exception_entry
    .align 16
__fetrap: ;/* 0x0030 */
    jr32 _kernel_fe_exception_entry
    .align 16
__trap0:  ;/* 0x0040 */
    jr32 _kernel_ei_exception_entry
    .align 16
__trap1:  ;/* 0x0050 */
    jr32 _kernel_ei_exception_entry
    .align 16
__rie:    ;/* 0x0060 */
    jr32 _kernel_ei_exception_entry
    .align 16
__fppfpi: ;/* 0x0070 */
    jr32 _kernel_ei_exception_entry
    .align 16
__ucpop:  ;/* 0x0080 */
    jr32 _kernel_fe_exception_entry
    .align 16
__mip:    ;/* 0x0090 */
    jr32 _kernel_fe_exception_entry
    .align 16
__pie:    ;/* 0x00a0 */
    jr32 _kernel_fe_exception_entry
    .align 16
__debug:  ;/* 0x00b0 */
    jr32 __debug
    .align 16
__mae:    ;/* 0x00c0 */
    jr32 _kernel_fe_exception_entry
    .align 16
__rfu:    ;/* 0x00d0 */
    jr32 __rfu
    .align 16
__fenmi:  ;/* 0x00e0 */
    jr32 _kernel_fe_exception_entry
    .align 16
__feint:  ;/* 0x00f0 */
    jr32 _kernel_fe_exception_entry
    .align 16
__eiintn0:  ;/*（優先度0） 0x0100 */
    jr32 _kernel_default_int_handler
    .align 16
__eiintn1:  ;/*（優先度1） 0x0110 */
    jr32 _kernel_interrupt
    .align 16
__eiintn2:  ;/*（優先度2） 0x0120 */
    jr32 _kernel_interrupt
    .align 16
__eiintn3:  ;/*（優先度3） 0x0130 */
    jr32 _kernel_interrupt
    .align 16
__eiintn4:  ;/*（優先度4） 0x0140 */
    jr32 _kernel_interrupt
    .align 16
__eiintn5:  ;/*（優先度5） 0x0150 */
    jr32 _kernel_interrupt
    .align 16
__eiintn6:  ;/*（優先度6） 0x0160 */
    jr32 _kernel_interrupt
    .align 16
__eiintn7:  ;/*（優先度7） 0x0170 */
    jr32 _kernel_interrupt
    .align 16
__eiintn8:  ;/*（優先度8） 0x0180 */
    jr32 _kernel_interrupt
    .align 16
__eiintn9:  ;/*（優先度9） 0x0190 */
    jr32 _kernel_interrupt
    .align 16
__eiintn10: ;/*（優先度10） 0x01a0 */
    jr32 _kernel_interrupt
    .align 16
__eiintn11: ;/*（優先度11） 0x01b0 */
    jr32 _kernel_interrupt
    .align 16
__eiintn12: ;/*（優先度12） 0x01c0 */
    jr32 _kernel_interrupt
    .align 16
__eiintn13: ;/*（優先度13） 0x01d0 */
    jr32 _kernel_interrupt
    .align 16
__eiintn14: ;/*（優先度14） 0x01e0 */
    jr32 _kernel_interrupt
    .align 16
__eiintn15: ;/*（優先度15） 0x01f0 */
    jr32 _kernel_interrupt
    .align 16
    jarl32  __reset_pe2, r31  ;/* __reset_pe2をリンクするためのダミーコール */

__terminate:
    ;/* 終了:無限ループ */
    br __terminate
;-----------------------------------------------------------------------------
;	startup
;-----------------------------------------------------------------------------
	.section	".text", text
	.align	2
	.public	__start
__start:
$if 1	; initialize register
	$nowarning
	mov	r0, r1
	$warning
	mov	r0, r2
	mov	r0, r3
	mov	r0, r4
	mov	r0, r5
	mov	r0, r6
	mov	r0, r7
	mov	r0, r8
	mov	r0, r9
	mov	r0, r10
	mov	r0, r11
	mov	r0, r12
	mov	r0, r13
	mov	r0, r14
	mov	r0, r15
	mov	r0, r16
	mov	r0, r17
	mov	r0, r18
	mov	r0, r19
	mov	r0, r20
	mov	r0, r21
	mov	r0, r22
	mov	r0, r23
	mov	r0, r24
	mov	r0, r25
	mov	r0, r26
	mov	r0, r27
	mov	r0, r28
	mov	r0, r29
	mov	r0, r30
	mov	r0, r31
	ldsr	r0, 0, 0		;  EIPC
	ldsr	r0, 16, 0		;  CTPC
$endif

$if 1
	; jump to entry point of each PE
	stsr	0, r10, 2		; get HTCFG0
	shr	16, r10			; get PEID

	cmp	1, r10
	bz	.L.entry_PE1
	cmp	2, r10
	bz	.L.entry_PE2
	cmp	3, r10
	bz	.L.entry_PE3
	cmp	4, r10
	bz	.L.entry_PE4
	cmp	5, r10
	bz	.L.entry_PE5
	cmp	6, r10
	bz	.L.entry_PE6
	cmp	7, r10
	bz	.L.entry_PE7
__exit:
	br	__exit

.L.entry_PE1:
	jarl	_hdwinit_PE1, lp	; initialize hardware
$ifdef USE_TABLE_REFERENCE_METHOD		
	mov	#__sEIINTTBL_PE1, r6	
	jarl	_set_table_reference_method, lp ; set table reference method
$endif

	jr32	__OSstart

.L.entry_PE2:
	jarl	_hdwinit_PE2, lp	; initialize hardware
$ifdef USE_TABLE_REFERENCE_METHOD
	mov	#__sEIINTTBL_PE2, r6	
	jarl	_set_table_reference_method, lp ; set table reference method
$endif
	jr32	__OSstart
	br	__exit

.L.entry_PE3:
	br	__exit
.L.entry_PE4:
	br	__exit
.L.entry_PE5:
	br	__exit
.L.entry_PE6:
	br	__exit
.L.entry_PE7:
	br	__exit
$endif

;-----------------------------------------------------------------------------
;	hdwinit_PE1
; Specify RAM addresses suitable to your system if needed.
;-----------------------------------------------------------------------------
	GLOBAL_RAM_ADDR	.set	0
	GLOBAL_RAM_END	.set	0
	LOCAL_RAM_PE1_ADDR	.set	0
	LOCAL_RAM_PE1_END	.set	0

	.align	2
_hdwinit_PE1:
	mov	lp, r29			; save return address

	; clear Global RAM
	mov	GLOBAL_RAM_ADDR, r6
	mov	GLOBAL_RAM_END, r7
	jarl	_zeroclr4, lp

	; clear Local RAM PE1
	mov	LOCAL_RAM_PE1_ADDR, r6
	mov	LOCAL_RAM_PE1_END, r7
	jarl	_zeroclr4, lp

	mov	r29, lp
	jmp	[lp]

;-----------------------------------------------------------------------------
;	hdwinit_PE2
; Specify RAM addresses suitable to your system if needed.
;-----------------------------------------------------------------------------
	LOCAL_RAM_PE2_ADDR	.set	0
	LOCAL_RAM_PE2_END	.set	0

	.align	2
_hdwinit_PE2:
	mov	lp, r14			; save return address

	; clear Local RAM PE2
	mov	LOCAL_RAM_PE2_ADDR, r6
	mov	LOCAL_RAM_PE2_END, r7
	jarl	_zeroclr4, lp

	mov	r14, lp
	jmp	[lp]

;-----------------------------------------------------------------------------
;	zeroclr4
;-----------------------------------------------------------------------------
	.align	2
_zeroclr4:
	br	.L.zeroclr4.2
.L.zeroclr4.1:
	st.w	r0, [r6]
	add	4, r6
.L.zeroclr4.2:
	cmp	r6, r7
	bh	.L.zeroclr4.1
	jmp	[lp]


;-----------------------------------------------------------------------------
;	set table reference method
;-----------------------------------------------------------------------------
	; interrupt control register address
	ICBASE	.set	0xfffeea00

	.align	2
_set_table_reference_method:
	ldsr	r6, 4, 1		; set INTBP

	; Some interrupt channels use the table reference method.
	mov	ICBASE, r10		; get interrupt control register address
	set1	6, 0[r10]		; set INT0 as table reference
	set1	6, 2[r10]		; set INT1 as table reference
	set1	6, 4[r10]		; set INT2 as table reference

	jmp	[lp]
;-------------------- end of start up module -------------------;
