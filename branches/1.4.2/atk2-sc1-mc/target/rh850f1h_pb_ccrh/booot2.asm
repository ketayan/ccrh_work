;   Copyright(c) 2013, 2016 Renesas Electronics Corporation
;   RENESAS ELECTRONICS CONFIDENTIAL AND PROPRIETARY.
;   This program must be used solely for the purpose for which
;   it was furnished by Renesas Electronics Corporation. No part of this
;   program may be reproduced or disclosed to others, in any
;   form, without the prior written permission of Renesas Electronics
;   Corporation.

;   NOTE       : THIS IS A TYPICAL EXAMPLE.
;   DATE       : Tue, Jan 12, 2016

;-----------------------------------------------------------------------------
;	exception vector table
;-----------------------------------------------------------------------------
	.section "RESET_PE2", text
.align 512
RESET_PE2 .cseg text
	.extern  __reset_pe2
 __reset_pe2:
    jr32 __start   
    nop
    .align 16
__syserr_pe2: ;/* 0x0010 */
    nop
    .align 16
__hvtrap_pe2: ;/* 0x0020 */
    jr32 _kernel_fe_exception_entry
    nop
    .align 16
__fetrap_pe2: ;/* 0x0030 */
    jr32 _kernel_fe_exception_entry
    nop
    .align 16
__trap0_pe2:  ;/* 0x0040 */
    jr32 _kernel_ei_exception_entry
    nop
    .align 16
__trap1_pe2:  ;/* 0x0050 */
    jr32 _kernel_ei_exception_entry
    nop
    .align 16
__rie_pe2:    ;/* 0x0060 */
    nop
    .align 16
__fppfpi_pe2: ;/* 0x0070 */
    nop
    .align 16
__ucpop_pe2:  ;/* 0x080 */
    nop
    .align 16
__mip_pe2:    ;/* 0x0090 */
    nop
    .align 16
__pie_pe2:    ;/* 0x00a0 */
    nop
    .align 16
__debug_pe2:  ;/* 0x00b0 */
    nop
    .align 16
__mae_pe2:    ;/* 0x00c0 */
    nop
    .align 16
__rfu_pe2:    ;/* 0x00d0 */
    nop
    .align 16
__fenmi_pe2:  ;/* 0x00e0 */
    nop
    .align 16
__feint_pe2:  ;/* 0x00f0 */
    nop
    .align 16
__eiintn0_pe2:  ;/*（優先度0） 0x0100 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn1_pe2:  ;/*（優先度1） 0x0110 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn2_pe2:  ;/*（優先度2） 0x0120 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn3_pe2:  ;/*（優先度3） 0x0130 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn4_pe2:  ;/*（優先度4） 0x0140 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn5_pe2:  ;/*（優先度5） 0x0150 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn6_pe2:  ;/*（優先度6） 0x0160 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn7_pe2:  ;/*（優先度7） 0x0170 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn8_pe2:  ;/*（優先度8） 0x0180 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn9_pe2:  ;/*（優先度9） 0x0190 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn10_pe2: ;/*（優先度10） 0x01a0 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn11_pe2: ;/*（優先度11） 0x01b0 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn12_pe2: ;/*（優先度12） 0x01c0 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn13_pe2: ;/*（優先度13） 0x01d0 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn14_pe2: ;/*（優先度14） 0x01e0 */
    jr32 _kernel_interrupt
    nop
    .align 16
__eiintn15_pe2: ;/*（優先度15） 0x01f0 */
    jr32 _kernel_interrupt
    nop
    .align 16

