/* This file is generated from target_rename.def by genrename. */

/* This file is included only when target_rename.h has been included. */
#ifdef TOPPERS_TARGET_RENAME_H
#undef TOPPERS_TARGET_RENAME_H

/*
 *  target_config.c
 */
#undef target_ici_intno_table
#undef target_ici_intpri_table
#undef target_core_int_base_table
#undef target_tsk_lock_table
#undef target_cnt_lock_table
#undef target_spn_lock_table
#undef target_hardware_initialize
#undef target_initialize
#undef target_exit
#undef target_is_int_controllable

/*
 *  trace_dump.c
 */
#undef trace_dump

/*
 *  trace_config.c
 */
#undef log_dsp_enter
#undef log_dsp_leave


#include "prc_unrename.h"

#endif /* TOPPERS_TARGET_RENAME_H */
