
		TOPPERS/ATK2-SC1
        ＜FL-850/F1Lターゲット依存部(CCRH版)マニュアル＞

このドキュメントはFL-850/F1Lターゲット依存部の情報を記述したものである．

----------------------------------------------------------------------
TOPPERS ATK2
    Toyohashi Open Platform for Embedded Real-Time Systems
    Automotive Kernel Version 2

Copyright (C) 2013-2014 by Center for Embedded Computing Systems
            Graduate School of Information Science, Nagoya Univ., JAPAN
Copyright (C) 2014-2015 by FUJI SOFT INCORPORATED, JAPAN
Copyright (C) 2014-2015 by NEC Communication Systems, Ltd., JAPAN
Copyright (C) 2014-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
Copyright (C) 2014-2015 by Sunny Giken Inc., JAPAN
Copyright (C) 2014-2015 by TOSHIBA CORPORATION, JAPAN
Copyright (C) 2014-2015 by Witz Corporation
Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
Copyright (C) 2014-2015 by SCSK Corporation, JAPAN

上記著作権者は，以下の (1)〜(3)の条件を満たす場合に限り，本ドキュメ
ント（本ドキュメントを改変したものを含む．以下同じ）を使用・複製・改
変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
(1) 本ドキュメントを利用する場合には，上記の著作権表示，この利用条件
    および下記の無保証規定が，そのままの形でドキュメント中に含まれて
    いること．
(2) 本ドキュメントを改変する場合には，ドキュメントを改変した旨の記述
    を，改変後のドキュメント中に含めること．ただし，改変後のドキュメ
    ントが，TOPPERSプロジェクト指定の開発成果物である場合には，この限
    りではない．
(3) 本ドキュメントの利用により直接的または間接的に生じるいかなる損害
    からも，上記著作権者およびTOPPERSプロジェクトを免責すること．また，
    本ドキュメントのユーザまたはエンドユーザからのいかなる理由に基づ
    く請求からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

本ドキュメントは，AUTOSAR（AUTomotive Open System ARchitecture）仕様
に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するものではな
い．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利用する
者に対して，AUTOSARパートナーになることを求めている．

本ドキュメントは，無保証で提供されているものである．上記著作権者およ
びTOPPERSプロジェクトは，本ドキュメントに関して，特定の使用目的に対す
る適合性も含めて，いかなる保証も行わない．また，本ドキュメントの利用
により直接的または間接的に生じたいかなる損害に関しても，その責任を負
わない．

$Id: target_user.txt 178 2015-04-06 23:56:31Z ertl-honda $
----------------------------------------------------------------------

○概要

FL-850/F1Lターゲット依存部（FL-850/F1L依存部）は，ルネサスエレクトロニ
クス社のRH850/F1Lを搭載したテセラ・テクノロジーのFL-850/F1L-176-Sをサポー
トしている．


○動作確認バージョン

動作確認を行ったコンパイラのバージョンは次の通りである．

 CC-RH V1.01.00

  
○ログ出力

ログ出力のため，RLIN3のポート5を使用する．

データフォーマットは，

・115200bps、データ8bit、パリティなし、ストップ1bit、フロー制御なし

である．


○ハードウェアカウンタ

・サンプルプログラムにて，TAUJ0, TAUJ1を使用．
・1個のカウンタに2つのタイマを使用している．
    差分タイマ：目的の時間を設定する時の現在時間(現在値タイマ)と次の満
    了時間との相対時間をカウントすることで目的の絶対時間に満了したこと
    とする.
        count mode:count down once
    現在値タイマ：カウンタ周期分のベースタイマを実現(絶対時間をカウント)
        count mode:continuous count down
・使用カウンタ情報
    カウンタ名：MAIN_HW_COUNTER
        差分タイマ：TAUJ0
        現在値タイマ：TAUJ1
        1ティック当たりの秒数：1/TIMER_CLOCK_HZ

◯時間パーティショニング用タイマ
・タイムウィンドウ管理用にTAUJ0I2を使用．
・システム周期管理用にTAUJ0I3を使用．

○ビルド及びデバッグ環境

ビルド及びデバッグ環境はCS+を使用する．動作確認を行ったバージョンは次
の通りである．

 CS+ V3.00.00
 (IronPythonコンソール・プラグイン 使用)
 
ビルドの手順は次の通りである． 
 
1../arch/ccrh/configure をatk2-sc1のソース以下の適当なフォルダにコピー．

2../arch/ccrh/configure/def.py の内容を編集．最低限以下を変更する必要があ
  る．

 SRCDIR 
 TARGET_MCU
 
3. configure.mtpj を開き，atk2-sc1 のプロジェクトファイルが作成される
  のを待つ．
  
4.アクティブプロジェクトをatk2-sc1に変更．

5.ビルド
  メニューから ビルド -> ビルド・プロジェクト を選択する．
  cfg→kernel→atk2-sc1 の順にビルドされる．

6.デバッグ    
 
 
○変更履歴

2015/05/13
	ATK2-SC1 1.3.2をベースに作成．
  
以上 
