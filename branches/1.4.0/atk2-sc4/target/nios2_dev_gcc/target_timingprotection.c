/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_timingprotection.c 504 2015-12-24 01:22:56Z witz-itoyo $
 */

/*
 *		タイミング保護用タイマ
 */
#include "kernel_impl.h"
#include "prc_sil.h"
#include "nios2_system.h"
#include "target_timingprotection.h"
#include "timingprotection.h"


/*
 *  タイマの初期化処理
 */
void
target_tp_initialize(void)
{
	/* タイマストップ */
	sil_wrw_iop((void *) TIMER_CONTROL(INTNO_TP_FAULT), TIMER_CONTROL_STOP);
	/* タイムアウトステータスクリア */
	sil_wrw_iop((void *) TIMER_STATUS(INTNO_TP_FAULT), 0x00U);

	/* タイマストップ */
	sil_wrw_iop((void *) TIMER_CONTROL(INTNO_TP_TIMER), TIMER_CONTROL_STOP);
	/* タイムアウトステータスクリア */
	sil_wrw_iop((void *) TIMER_STATUS(INTNO_TP_TIMER), 0x00U);

	/* タイマカウンタセット 上位16bit */
	sil_wrw_iop((void *) TIMER_PERIODH(INTNO_TP_TIMER), (TP_TIMER_MAX_TICK >> 16U));
	/* タイマカウンタセット 下位16bit */
	sil_wrw_iop((void *) TIMER_PERIODL(INTNO_TP_TIMER), (TP_TIMER_MAX_TICK & 0xffffU));

	x_config_int(INTNO_TP_FAULT, ENABLE, intpri_tp);
	x_config_int(INTNO_TP_TIMER, ENABLE, intpri_tp);

	/*
	 *  到着間隔計測用タイマ開始(free run timer)
	 *  割込みが必要のため，割込みモードあり
	 */
	sil_wrw_iop((void *) TIMER_CONTROL(INTNO_TP_TIMER), TIMER_CONTROL_ITO | TIMER_CONTROL_CONT | TIMER_CONTROL_START);
}

/*
 *  タイマの終了処理
 */
void
target_tp_terminate(void)
{
	/* タイマ停止 */
	sil_wrw_iop((void *) TIMER_CONTROL(INTNO_TP_FAULT), TIMER_CONTROL_STOP);
	sil_wrw_iop((void *) TIMER_CONTROL(INTNO_TP_TIMER), TIMER_CONTROL_STOP);

	/* 割込み要求のクリア */
	sil_wrw_iop((void *) TIMER_STATUS(INTNO_TP_FAULT), 0x00U);
	sil_wrw_iop((void *) TIMER_STATUS(INTNO_TP_TIMER), 0x00U);
}

/*
 *  時間監視用タイマの起動処理
 *  - タイマ動作中にも呼び出される
 */
void
target_tp_start_timer(TickType tick)
{
	/*
	 *  タイマカウンタのセット値は，tick - 1
	 *   (tick > 0)
	 */
	if (tick > 1U) {
		tick--;
	}

	/* タイマストップ */
	sil_wrw_iop((void *) TIMER_CONTROL(INTNO_TP_FAULT), TIMER_CONTROL_STOP);

	/* タイマカウンタセット 上位16bit */
	sil_wrw_iop((void *) TIMER_PERIODH(INTNO_TP_FAULT), (tick >> 16U));
	/* タイマカウンタセット 下位16bit */
	sil_wrw_iop((void *) TIMER_PERIODL(INTNO_TP_FAULT), (tick & 0xffffU));

	/*
	 *  タイマ開始(count down once mode)
	 *  割込み必要のため，割込みモードあり
	 */
	sil_wrw_iop((void *) TIMER_CONTROL(INTNO_TP_FAULT), TIMER_CONTROL_ITO | TIMER_CONTROL_START);
}

/*
 *  時間監視用タイマの残り時間取得
 *  - タイマ動作中にも呼び出される
 */
TickType
target_tp_get_remaining_ticks(void)
{
	TickType	curval;
	uint32		status;

	/* スナップレジスタに書き込むと値をホールドする */
	sil_wrw_iop((void *) TIMER_SNAPL(INTNO_TP_FAULT), 0x00U);

	/* タイマからカウント値を読み込む */
	curval = sil_rew_iop((void *) TIMER_SNAPL(INTNO_TP_FAULT));
	curval |= sil_rew_iop((void *) TIMER_SNAPH(INTNO_TP_FAULT)) << 16U;

	/* タイムアウトステータス取得 */
	status = sil_rew_iop((void *) TIMER_STATUS(INTNO_TP_FAULT));
	if ((status & TIMER_STATUS_TO) != 0x00U) {
		/*
		 *  すでにタイムアウトになっている
		 */
		curval = 0x01U;
		/* タイムアウトステータスクリア */
		sil_wrw_iop((void *) TIMER_STATUS(INTNO_TP_FAULT), 0x00U);

	}
	else {
		/*
		 *  設定するときに-1するので，UINT_MAXが取得されることはない
		 */
		curval += 0x01U;
	}
	return(curval);
}

/*
 *  時間監視用タイマの停止処理
 */
TickType
target_tp_stop_timer(void)
{
	TickType curval;

	/* タイマストップ */
	sil_wrw_iop((void *) TIMER_CONTROL(INTNO_TP_FAULT), TIMER_CONTROL_STOP);

	curval = target_tp_get_remaining_ticks();

	return(curval);
}

/*
 *  到着間隔計測用タイマの経過ティックの読出し
 */
TickType
target_tp_get_elapsed_ticks(void)
{
	TickType curval;

	/* スナップレジスタに書き込むと値をホールドする */
	sil_wrw_iop((void *) TIMER_SNAPL(INTNO_TP_TIMER), 0x00U);

	/* タイマからカウント値を読み込む */
	curval = sil_rew_iop((void *) TIMER_SNAPL(INTNO_TP_TIMER));
	curval |= sil_rew_iop((void *) TIMER_SNAPH(INTNO_TP_TIMER)) << 16U;

	return(TP_TIMER_MAX_TICK - curval);
}

/*
 *  タイマの割込みチェック
 */
boolean
target_tp_sense_interrupt(void)
{
	return((sil_rew_iop((void *) TIMER_STATUS(INTNO_TP_TIMER)) & TIMER_STATUS_TO) != 0U);
}
