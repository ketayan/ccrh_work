$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2013-2015 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2013-2014 by FUJI SOFT INCORPORATED, JAPAN
$  Copyright (C) 2013-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2013-2014 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2013-2014 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2013-2014 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2013-2014 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: prc.tf 1709 2016-02-10 04:57:29Z t_ishikawa $
$

$
$     パス2のアーキテクチャ依存テンプレート（V850用）
$

$FILE "kernel_mem2.c"$
#pragma section kernel

$FILE "Os_Lcfg.c"$
#pragma section kernel

$ 
$  kernel/kernel.tf のターゲット依存部
$ 

$ 
$  ユーザスタック領域を確保するコードを出力する
$  ARGV[1]：タスクID
$  ARGV[2]：スタックサイズ
$ 
$FUNCTION ALLOC_USTACK$
    #pragma section $FORMAT(".user_stack.%s", ARGV[1])$$NL$
    StackType _kernel_ustack_$ARGV[1]$[COUNT_STK_T($ARGV[2]$)];$NL$
    #pragma section kernel$NL$

    $TSK.TINIB_USTKSZ[ARGV[1]] = VALUE(FORMAT("ROUND_STK_T(%s)", ARGV[2]), +ARGV[2])$
    $TSK.TINIB_USTK[ARGV[1]] = FORMAT("_kernel_ustack_%s", ARGV[1])$
$END$

$ 
$  ユーザスタック領域のセクション名を返す
$  ARGV[1]：タスクID
$ 
$FUNCTION SECTION_USTACK$
    $RESULT = FORMAT(".user_stack.%s.bss", ARGV[1])$
$END$

$
$  ユーザスタックのアライメント制約に合わせたサイズを返す
$  ARGV[1]：スタックサイズ（アライン前）
$  kernel.tfから呼ばれる
$
$FUNCTION USTACK_ALIGN_SIZE$
	$RESULT = (ARGV[1] + CHECK_USTKSZ_ALIGN - 1) & ~(CHECK_USTKSZ_ALIGN - 1)$
$END$

$
$  基本タスクの共有スタックの確保
$  ARGV[1]：共有スタック名
$  ARGV[2]：共有スタックID(タスク優先度)
$  ARGV[3]：スタックサイズ(アライメント調整済み)
$  kernel.tfから呼ばれる
$
$FUNCTION ALLOC_SHARED_USTACK$
    #pragma section $FORMAT(".shared_user_stack.%s", ARGV[2])$$NL$
	StackType $ARGV[1]$[COUNT_STK_T(ROUND_STK_T($ARGV[3]$))];$NL$
    #pragma section kernel$NL$
$END$

$
$  基本タスクの共有スタックのセクション名
$  ARGV[1]：共有スタックID(タスク優先度)
$  kernel.tfから呼ばれる
$
$FUNCTION SECTION_SHARED_USTACK$
	$RESULT = FORMAT(".shared_user_stack.%s.bss", ARGV[1])$
$END$

$ 
$  OsMemoryModule はサポートしない
$ 
$TOPPERS_SUPPORT_ATT_MOD = 0$
$FUNCTION HOOK_ERRORCHECK_MOD$
    $ERROR$
        OsMemoryModule is not supported.$NL$
    $END$
$END$

$ 
$  OSAP初期化コンテキストブロックのための宣言
$ 
$FUNCTION PREPARE_OSAPINICTXB$
    $NOOP()$
$END$

$FUNCTION ASM_MACRO$
	$ARGV[1]$ .macro $ARGV[2]$
$END$

$INCLUDE "arch/v850_gcc/prc_common.tf"$

$IF __v850e3v5__$

$
$ テーブル参照方式用ベクタテーブル(v850e3v5)
$

$FILE "Os_Lcfg_asm.asm"$

.extern _interrupt$NL$
.extern _int_tp_fault_handler$NL$
.extern _int_tp_timer_handler$NL$

$NL$$FOREACH intno INTNO_VALID$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid)$
		$IF EQ(ISR.CATEGORY[isrid], "CATEGORY_1")$
			.extern _$ISR.INT_ENTRY[isrid]$$NL$
		$END$
	$END$
$END$
$NL$
	.section "EIINTTBL", const$NL$
	.extern _intbp_tbl$NL$
	.align 512$NL$
	_intbp_tbl:$NL$
$JOINEACH intno INTNO_VALID "\n"$
	$isrid = INT.ISRID[intno]$
	$IF LENGTH(isrid) && EQ(ISR.CATEGORY[isrid], "CATEGORY_1")$
		$TAB$.dw #_$ISR.INT_ENTRY[isrid]$
	$ELIF intno == INTNO_TP_FAULT$
		$TAB$.dw #_int_tp_fault_handler
	$ELIF intno == INTNO_TP_TIMER$
		$TAB$.dw #_int_tp_timer_handler
	$ELSE$
		$TAB$.dw #_interrupt
	$END$
	$TAB$$FORMAT(";/* %d */", intno)$
$END$

$NL$
;/* in order to restrain warning */$NL$
$NL$
$FOREACH mo MO_ORDER$
	$IF MO.LINKER[mo]$
        $seg = ""$
        $e = REGEX_REPLACE(MO.SECTION[mo], ".*\\.text$", "")$
        $IF !EQ(e, MO.SECTION[mo])$
            $seg = "text"$
        $END$
        $e = REGEX_REPLACE(MO.SECTION[mo], ".*\\.const$", "")$
        $IF !EQ(e, MO.SECTION[mo])$
            $seg = "const"$
        $END$
        $e = REGEX_REPLACE(MO.SECTION[mo], ".*\\.data$", "")$
        $IF !EQ(e, MO.SECTION[mo])$
            $seg = "data"$
        $END$
        $e = REGEX_REPLACE(MO.SECTION[mo], ".*\\.bss$", "")$
        $IF !EQ(e, MO.SECTION[mo])$
            $seg = "bss"$
        $END$
        $IF !EQ(seg, "")$
            $TAB$.section "$MO.SECTION[mo]$", $seg$$NL$
            _dummy$MO.SECTION[mo]$:$NL$
            $NL$
        $END$
    $END$
$END$

$END$
$   //$IF __v850e3v5__$

$ 
$  arch/ccrh/ldscript.tfのターゲット依存部
$ 

$ 
$  CCRH依存部のテンプレートファイルのインクルード
$ 
$INCLUDE "ccrh/ldscript.tf"$ 

$FILE "kernel_mem2.c"$

$IF TNUM_MPU_SHARED > 0$
uint8 * const shared_meminib_table[$TNUM_MPU_SHARED * 3$] = {$NL$
$FOREACH memid RANGE(0, TNUM_MPU_SHARED - 1)$
    $TAB$((uint8 *)NULL),$TAB$/* MPUL $TNUM_MPU_REG - memid$ */$NL$
    $TAB$((uint8 *)NULL),$TAB$/* MPUA $TNUM_MPU_REG - memid$ */$NL$
    $TAB$((uint8 *)NULL),$TAB$/* MPAT $TNUM_MPU_REG - memid$ */$NL$
$END$
};$NL$
$NL$
$END$

$FILE "cfg2_out.tf"$
$ LNKSEC.*の出力
$$numls = $numls$$$$NL$
$NL$
$FOREACH lsid RANGE(1, numls)$
	$$LNKSEC.MEMREG[$lsid$] = $LNKSEC.MEMREG[lsid]$$$$NL$
	$$LNKSEC.SECTION[$lsid$] = $ESCSTR(LNKSEC.SECTION[lsid])$$$$NL$
	$NL$
$END$

$JOINEACH tskid TSK.ID_LIST "\n"$
	$$TSK.ID[$+tskid$] = $TSK.ID[tskid]$$$
$END$
$NL$

$lma = 0$
$FOREACH moid MO_ORDER$
	$IF MO.LINKER[moid] && ((MO.SEFLAG[moid] & 0x01) != 0)$
		$IF !OMIT_IDATA && !EQ(MO.ILABEL[moid], "")$
            $$LMA.START_IDATA[$lma$] = "__s$MO.SECTION[moid]$"$$$NL$
			$$LMA.START_DATA[$lma$] = "__s$MO.SECTION[moid]$.R"$$$NL$
			$$LMA.END_DATA[$lma$] = "__e$MO.SECTION[moid]$.R"$$$NL$
            $lma = lma + 1$
		$END$
    $END$
$END$
$$LMA.ORDER_LIST = RANGE(0, $lma - 1$)$$$NL$
$NL$
