$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2014-2015 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: prc_meminib.tf 1709 2016-02-10 04:57:29Z t_ishikawa $
$

$ 
$  MOの読み込み
$ 
$INCLUDE "cfg2_out.tf"$

$ =====================================================================
$ kernel_mem.cの共通部分の生成
$ =====================================================================

$FILE "kernel_mem.c"$

$ =====================================================================
$ シンボルと仮メモリオブジェクト初期化ブロックの読込みと前処理
$ =====================================================================

$
$ シンボルと仮メモリオブジェクト初期化ブロックの読込み
$
$ MO.BASEADDR[moid]：メモリオブジェクトの先頭番地
$ MO.LIMITADDR[moid]：メモリオブジェクトの上限番地
$ MO.POFFSET[moid]：物理アドレスとのオフセット
$ MO_MEMTOP_LIST：サイズが0でないメモリオブジェクトのリスト

$OMIT_STANDARD_MEMINIB = 1$
$IF OMIT_STANDARD_MEMINIB$
	$MO_MEMTOP_LIST = {}$
	$memtop_table = SYMBOL("memtop_table")$
	$offset = 0$
	$FOREACH moid MO_START_LIST$
		$IF MO.LINKER[moid]$
            $next = AT(MO_START_LIST, (FIND(MO_START_LIST, moid) + 1))$
            $IF LENGTH(next) && !EQ(next, "")$
                $last = FIND(MO_ORDER, next) - 1$
            $ELSE$
                $last = LENGTH(MO_ORDER) - 1$
            $END$
            $TRACE(MO.SECTION[moid])$
            $FOREACH moid2 RANGE(FIND(MO_ORDER, moid), last)$
                $moid2 = AT(MO_ORDER, moid2)$
                $TRACE(MO.SECTION[moid2])$
                $IF !LENGTH(MO.BASEADDR[moid])$
                    $MO.BASEADDR[moid] = START_MO_SYMBOL(moid2)$
                    $IF !EQ(MO.ILABEL[moid2], "")$
                        $MO.IBASEADDR[moid] = START_SYMBOL(MO.SECTION[moid2])$
                    $END$
                $END$
                $IF LENGTH(LIMIT_MO_SYMBOL(moid2))$
                    $MO.LIMITADDR[moid] = LIMIT_MO_SYMBOL(moid2)$
                $END$
            $END$
            $IF !LENGTH(MO.BASEADDR[moid])$
                $IF LENGTH(MO.LIMITADDR[moid])$
                    $ERROR$conflict MO BASE and LIMIT$END$
                $END$
                $MO.BASEADDR[moid] = 0xffffffff$
                $MO.LIMITADDR[moid] = 0xffffffff$
            $END$
            $IF LENGTH(MO.BASEADDR[moid])$
                $IF !LENGTH(MO.LIMITADDR[moid])$
                    $ERROR$conflict MO BASE and LIMIT$END$
                $END$
            $END$
			$MO.POFFSET[moid] = 0$
		$ELSE$
			$MO.BASEADDR[moid] = PEEK(memtop_table + offset, sizeof_void_ptr)$
			$offset = offset + sizeof_void_ptr$
			$MO.LIMITADDR[moid] = (MO.BASEADDR[moid] + MO.SIZE[moid]) & ((1 << sizeof_void_ptr * 8) - 1)$
			$IF LENGTH(MO.PADDR[moid])$
				$MO.POFFSET[moid] = MO.PADDR[moid] - MO.BASEADDR[moid]$
			$ELSE$
				$MO.POFFSET[moid] = 0$
			$END$
		$END$

		$IF MO.BASEADDR[moid] != MO.LIMITADDR[moid]$
			$IF MO.BASEADDR[moid] < MO.LIMITADDR[moid]
											|| MO.LIMITADDR[moid] == 0$
				$MO_MEMTOP_LIST = APPEND(MO_MEMTOP_LIST, moid)$
			$ELIF MO.TYPE[moid] == TOPPERS_ATTMEM$
				$ERROR MO.TEXT_LINE[moid]$ 
					$FORMAT(_("%1% `%2%\' is too large"),
								"size", MO.SIZE[moid])$
				$END$
			$ELSE$
				$ERROR MO.TEXT_LINE[moid]$
					$FORMAT(_("illegal memory object size"))$$NL$
                    $FORMAT("0x%x", MO.BASEADDR[moid])$$NL$
                    $FORMAT("0x%x", MO.LIMITADDR[moid])$
				$END$
			$END$
		$END$
	$END$
$END$

$ =====================================================================
$ メモリオブジェクト初期化ブロックの生成
$ =====================================================================

$FUNCTION GENERATE_MEMINIB_TABLE$
$	// メモリオブジェクト初期化ブロックに出力するエントリの決定
$
$	// tnum_meminib：メモリオブジェクト初期化ブロックに出力するエントリ数
$	// genzero：アドレス0に対応するエントリを出力する
	$tnum_meminib = 0$
	$prev = 0$
	$TRACE(MO_MEMTOP_ORDER)$
	$FOREACH moid MO_MEMTOP_ORDER$
		$IF !prev && MO.BASEADDR[moid] != 0$
			$genzero = 1$
			$tnum_meminib = tnum_meminib + 1$
		$END$
		$IF LENGTH(MO.SUPPRESSLIMIT[moid])$
			$tnum_meminib = tnum_meminib + 1$
		$ELSE$
			$tnum_meminib = tnum_meminib + 2$
		$END$
		$prev = moid$
	$END$

$	// tsize_meminibの妥当性チェック
	$IF tsize_meminib < tnum_meminib$
		$ERROR$$FORMAT(_("illegal memory object initialization block size"))$$END$
	$END$

$	// tnum_meminibの生成
	const uint32 tnum_meminib = $tnum_meminib$U;$NL$
	$NL$

$	// memtop_tableの生成
	void *const memtop_table[$tsize_meminib$] = {$NL$
	$IF LENGTH(genzero)$
		$TAB$0,$NL$
	$END$
	$JOINEACH moid MO_MEMTOP_ORDER ",\n"$
		$IF MO.LINKER[moid]$
			$TAB$$FORMAT("(void *)0x%x", MO.BASEADDR[moid])$
			$SPC$/* #__s$MO.SECTION[moid]$ */
			$IF !LENGTH(MO.SUPPRESSLIMIT[moid])$
				 ,$NL$$TAB$$FORMAT("(void *)0x%x", MO.LIMITADDR[moid])$
				$SPC$/* #__e$MO.SECTION[ALT(MO.MERGELAST[moid],moid)]$ */
			$END$
		$ELSE$
			$TAB$(void *)($MO.BASE[moid]$)
			$SPC$/* $FORMAT("%x", MO.BASEADDR[moid])$ */
			$IF !LENGTH(MO.SUPPRESSLIMIT[moid])$
				,$NL$$TAB$(void *)(((uint8 *)($MO.BASE[moid]$)) + ($MO.SIZE[moid]$))
				$SPC$/* $FORMAT("%x", MO.LIMITADDR[moid])$ */
			$END$
		$END$
	$END$
	$IF tnum_meminib < tsize_meminib$
		$FOREACH i RANGE(tnum_meminib + 1, tsize_meminib)$
			,$NL$$TAB$NULL
		$END$
	$END$$NL$
	};$NL$
	$NL$

$	// meminib_tableの生成
	const MEMINIB meminib_table[$tsize_meminib$] = {$NL$
	$IF LENGTH(genzero)$
		$TAB${ TA_NULL, TACP_KERNEL, TACP_KERNEL, TACP_KERNEL },$NL$
	$END$
	$JOINEACH moid MO_MEMTOP_ORDER ",\n"$
		$TAB${
		$IF MO.TYPE[moid] == TOPPERS_USTACK$
			$SPC$TOPPERS_USTACK|($MO.MEMATR[moid]$),
		$ELSE$
			$IF MO.TYPE[moid] == TOPPERS_ATTMEM$
				$SPC$TOPPERS_ATTMEM|($FORMAT("0x%xU", +MO.MEMATR[moid])$),
			$ELSE$
				$SPC$TOPPERS_ATTSEC|($FORMAT("0x%xU", +MO.MEMATR[moid])$),
			$END$
		$END$
		$SPC$$MO.ACPTN_R[moid]$,
		$SPC$$MO.ACPTN_W[moid]$,
		$SPC$$MO.ACPTN_X[moid]$
		$SPC$}
		$IF !LENGTH(MO.SUPPRESSLIMIT[moid])$
			,$NL$$TAB${ TA_NULL, TACP_KERNEL, TACP_KERNEL, TACP_KERNEL }
		$END$
	$END$
	$IF tnum_meminib < tsize_meminib$
		$FOREACH i RANGE(tnum_meminib + 1, tsize_meminib)$
			,$NL$$TAB${ 0U, 0U, 0U, 0U }
		$END$
	$END$$NL$
	};$NL$
	$NL$

$ 
$  dataセクション初期化ブロックの生成
$ 
    $IF !OMIT_IDATA && LENGTH(DATASEC_LIST) && !LENGTH(DATASEC_LIST_OPTIMIZED)$
$	// サイズが0でないdataセクションのリスト（MO_DATASEC_LIST）を作成
$	// パス3で作成していない場合には，ここで作成する．
        $MO_DATASEC_LIST = {}$
        $FOREACH moid DATASEC_LIST$
            $IF MO.BASEADDR[moid] != MO.LIMITADDR[moid]$
                $MO_DATASEC_LIST = APPEND(MO_DATASEC_LIST, moid)$
            $END$
        $END$
    $END$

    $IF OMIT_STANDARD_DATASECINIB$
        /*$NL$
        $SPC$*	Data Section Management Functions$NL$
        $SPC$*/$NL$
        $NL$

$	// dataセクションの数
        #define TNUM_DATASEC		$IF !OMIT_IDATA$$LENGTH(MO_DATASEC_LIST)$U$ELSE$0$END$$NL$
        #define TNUM_DATASEC_TBL	$IF !OMIT_IDATA$$LENGTH(DATASEC_LIST)$U$ELSE$0$END$$NL$
        $NL$

        $IF ISFUNCTION("DEFINE_CONST_VAR")$
            $DEFINE_CONST_VAR("const uint32", "tnum_datasec")$ = TNUM_DATASEC;$NL$
        $ELSE$
            const uint32 tnum_datasec = TNUM_DATASEC;$NL$
        $END$
        $NL$

        $IF !OMIT_IDATA && LENGTH(DATASEC_LIST)$
            $IF ISFUNCTION("DEFINE_CONST_VAR")$
                $DEFINE_CONST_VAR("const DATASECINIB", "datasecinib_table[TNUM_DATASEC_TBL]")$ = {
            $ELSE$
                const DATASECINIB datasecinib_table[TNUM_DATASEC_TBL] = {
            $END$
            $IF LENGTH(MO_DATASEC_LIST)$
                $NL$
                $JOINEACH moid MO_DATASEC_LIST ",\n"$
                    $IF LENGTH(START_MO_SYMBOL(moid))$ 
                        $TAB${ $FORMAT("(void *)0x%xU, (void *)0x%xU, (void *)0x%xU", MO.BASEADDR[moid], MO.LIMITADDR[moid], 
                        MO.IBASEADDR[moid])$ }
                    $ELSE$
                        $TAB${ 0U, 0U, 0U }
                    $END$
                    $SPC$ /* #__s$MO.SECTION[moid]$.R, &__e$MO.SECTION[moid]$.R, 
                    &__s$MO.SECTION[moid]$ */
                $END$$NL$
            $ELSE$
                { 0U, 0U, 0U }
            $END$
            };$NL$
        $ELSE$
            TOPPERS_EMPTY_LABEL(const DATASECINIB, datasecinib_table);$NL$
        $END$$NL$
    $END$

$ 
$  bssセクション初期化ブロックの生成
$ 
    $IF LENGTH(BSSSEC_LIST) && !LENGTH(BSSSEC_LIST_OPTIMIZED)$
$	// サイズが0でないbssセクションのリスト（MO_BSSSEC_LIST）を作成
        $MO_BSSSEC_LIST = {}$
        $FOREACH moid BSSSEC_LIST$
            $IF MO.BASEADDR[moid] != MO.LIMITADDR[moid]$
                $MO_BSSSEC_LIST = APPEND(MO_BSSSEC_LIST, moid)$
            $END$
        $END$
    $END$

    $IF OMIT_STANDARD_BSSSECINIB$
        /*$NL$
        $SPC$*	BSS Section Management Functions$NL$
        $SPC$*/$NL$
        $NL$

        $IF LENGTH(BSSSEC_LIST)$
$		// bssセクションの数
            const uint32 tnum_bsssec = $LENGTH(MO_BSSSEC_LIST)$U;$NL$
            $NL$

$		// bssセクション初期化ブロック
            const BSSSECINIB bsssecinib_table[$LENGTH(BSSSEC_LIST)$] = {
            $IF LENGTH(MO_BSSSEC_LIST)$
                $NL$
                $JOINEACH moid MO_BSSSEC_LIST ",\n"$
                    $IF LENGTH(START_MO_SYMBOL(moid))$ 
                        $TAB${ $FORMAT("(void *)0x%xU, (void *)0x%xU", MO.BASEADDR[moid], MO.LIMITADDR[moid])$ } 
                    $ELSE$
                        $TAB${ 0, 0 }
                    $END$
                    $SPC$ /* #__s$MO.SECTION[moid]$, #__e$MO.SECTION[moid]$ */
                $END$$NL$
            $ELSE$
                { 0, 0 }
            $END$
            };$NL$
        $ELSE$
            const uint32 tnum_bsssec = 0U;$NL$
            TOPPERS_EMPTY_LABEL(const BSSSECINIB, bsssecinib_table);$NL$
        $END$$NL$
    $END$

$END$

$OMIT_STANDARD_DATASECINIB = 1$
$OMIT_STANDARD_BSSSECINIB = 1$

