$ ======================================================================
$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2013-2015 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$   Copyright (C) 2013 by Embedded and Real-Time Systems Laboratory
$               Graduate School of Information Science, Nagoya Univ., JAPAN
$  
$   上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$   ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$   変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$   (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$       権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$       スコード中に含まれていること．
$   (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$       用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$       者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$       の無保証規定を掲載すること．
$   (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$       用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$       と．
$     (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$         作権表示，この利用条件および下記の無保証規定を掲載すること．
$     (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$         報告すること．
$   (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$       害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$       また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$       由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$       免責すること．
$  
$   本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$   よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$   に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$   アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$   の責任を負わない．
$
$  
$ =====================================================================


$ 
$  パス4のプロセッサ依存テンプレート（V850 CCRH用）
$  
$FILE "kernel_mem.c"$
#pragma section kernel

$ 
$  ユーザスタック領域のセクション名を返す
$  ARGV[1]：タスクID
$ 
$FUNCTION SECTION_USTACK$
    $RESULT = FORMAT(".user_stack.%s.bss", ARGV[1])$
$END$

$
$  基本タスクの共有スタックのセクション名
$  ARGV[1]：共有スタックID(タスク優先度)
$  kernel.tfから呼ばれる
$
$FUNCTION SECTION_SHARED_USTACK$
	$RESULT = FORMAT(".shared_user_stack.%s.bss", ARGV[1])$
$END$

$FUNCTION START_MO_SYMBOL$
    $symbol = CONCAT("__s", MO.SECTION[ARGV[1]])$
    $IF !EQ(MO.ILABEL[ARGV[1]], "")$
        $symbol = CONCAT(symbol, ".R")$
    $END$
	$RESULT = SYMBOL(symbol)$
$END$

$FUNCTION LIMIT_MO_SYMBOL$
    $symbol = CONCAT("__e", MO.SECTION[ARGV[1]])$
    $IF !EQ(MO.ILABEL[ARGV[1]], "")$
        $symbol = CONCAT(symbol, ".R")$
    $END$
	$RESULT = SYMBOL(symbol)$
$END$

$FUNCTION START_SYMBOL$
	$RESULT = SYMBOL(CONCAT("__s", ARGV[1]))$
$END$

$FUNCTION LIMIT_SYMBOL$
	$RESULT = SYMBOL(CONCAT("__e", ARGV[1]))$
$END$

$ 
$  OSAP初期化コンテキストブロックのための宣言
$ 
$FUNCTION PREPARE_OSAPINICTXB$
    $NOOP()$
$END$

$ 
$  meminibの依存部の読込み
$ 
$INCLUDE "arch/v850_ccrh/prc_meminib.tf"$

$INCLUDE "arch/v850_gcc/prc_mem_common.tf"$

