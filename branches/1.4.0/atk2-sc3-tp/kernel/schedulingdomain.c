/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2004-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: schedulingdomain.c 504 2015-12-24 01:22:56Z witz-itoyo $
 */


/*
 *		スケジューリングドメイン制御モジュール
 */

#include "kernel_impl.h"
#include "timewindow.h"
#include "schedulingdomain.h"
#include "task.h"

#ifdef TOPPERS_schedulingdomain_initialize

SCDCB	*p_curtwschedom;
SCDCB	*p_runschedom;
SCDCB	*p_idleschedom;
SCDCB	idle_scdcb;

boolean undispatch_flag;
TCB		*p_tschedtsk;

void
schedulingdomain_initialize(void)
{
	ApplicationType	i, j;
	SCDCB			*p_scdcb;

	p_tschedtsk = NULL;
	undispatch_flag = FALSE;

	/* タイムウィンドウが割り当てられたOSAP用のSCHEDMNを初期化 */
	for (i = 0U; i < tnum_schedom; i++) {
		p_scdcb = &scdcb_table[i];
		p_scdcb->p_schedtsk = NULL;

		for (j = 0U; j < TNUM_TPRI; j++) {
			queue_initialize(&(p_scdcb->ready_queue[j]));
		}
		p_scdcb->nextpri = TPRI_MINTASK;
		p_scdcb->ready_primap = 0U;
	}

	/* アイドル用SCHEDMNを初期化 */
	p_idleschedom = &idle_scdcb;
	p_idleschedom->p_schedtsk = NULL;

	for (j = 0U; j < TNUM_TPRI; j++) {
		queue_initialize(&(p_idleschedom->ready_queue[j]));
	}
	p_idleschedom->nextpri = TPRI_MINTASK;
	p_idleschedom->ready_primap = 0U;

	/* 最初に起動するSCHEDMNを設定 */
	p_curtwschedom = twinib_table[0].p_scdcb;
}

#endif /* TOPPERS_schedulingdomain_initialize */


#ifdef TOPPERS_update_tschedtsk

void
update_tschedtsk(void)
{

	/*
	 *  プロパースケジューリングドメインとアイドルスケジューリングドメインの間で
	 *  本当に実行すべきschedtskを決定する
	 */
	if (p_curtwschedom->p_schedtsk != NULL) {
		p_tschedtsk = p_curtwschedom->p_schedtsk;
	}
	else {
		p_tschedtsk = p_idleschedom->p_schedtsk;
	}
}

#endif /* TOPPERS_update_tschedtsk */
