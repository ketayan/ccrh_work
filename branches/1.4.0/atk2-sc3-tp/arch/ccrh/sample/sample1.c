/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2004-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: sample1.c 523 2015-12-25 01:18:53Z nces-okajima $
 */

/*
 * 本サンプルプログラムでは，以下に示すATK2-TPの基本機能の動作を確認可能である．
 * ・タイムウィンドウ切り替え機能
 * ・リソース獲得監視機能
 * ・割込み禁止監視機能
 *
 * 割込み発生回数監視機能については，最大発生回数のコンフィギュレーションがターゲットに依存するため，本サンプルプログラムでは確認しない．
 * もし確認する場合は，以下のようにOsIsrコンテナの下に以下のようにコンテナを追加する．ここでは最大発生回数が5回の場合の例を示す.
 * (.arxmlの場合)
 *		<ECUC-NUMERICAL-PARAM-VALUE>
 *			<DEFINITION-REF DEST="ECUC-INTEGER-PARAM-DEF">/AUTOSAR/EcucDefs/Os/OsIsr/OsIsrMaxFrequency</DEFINITION-REF>
 *			<VALUE>5</VALUE>
 *		</ECUC-NUMERICAL-PARAM-VALUE>
 * (.yamlの場合)
 *		OsIsrMaxFrequency: 5
 *
 * 以下にそれぞれの基本機能を確認する方法を示す．
 * ・タイムウィンドウ切り替え機能
 *   本サンプルプログラムでは，タイムウィンドウが2つ(TWD1，TWD2)用意されており，
 *   TWD1→TWD2の順で，それぞれ1.5秒ずつ実行する．
 *   OSAPは3つ(OSAP1，OSAP2，OSAP3)用意されており，
 *   OSAP1にはTWD1，OSAP2にはTWD2を割り当てる．
 *   OSAP3はアイドルOSAPとする．
 *   OSAP1にはTask1_Main(OSAP1のメインタスク)，Task1_High，Task1_Lowが所属し，それぞれの優先度の順位はTask1_Main > Task1_High > Task1_Lowである．
 *   OSAP2にはTask2_Main(OSAP2のメインタスク)，Task2_High，Task2_Lowが所属し，それぞれの優先度の順位はTask2_Main > Task2_High > Task2_Lowである．
 *   OSAP3にはTaskD_Main(OSAP3のメインタスク)，TaskD_High，TaskD_Lowが所属し，それぞれの優先度の順位はTaskD_Main > TaskD_High > TaskD_Lowである．
 *   Task1_Mainは自動起動とする．
 *   本サンプルプログラムでは，各タイムウィンドウの先頭で，そのタイムウィンドウが割り当てられたOSAPのメインタスクが起動するように，
 *   自動起動アラームがコンフィギュレーションされている．
 *     ・TWD1の先頭でTask1_Mainが起動するためのアラームとして，ActTask1Armがコンフィギュレーションされている．
 *       ・そのOsAlarmAlarmTimeは (システム周期), OsAlarmCycleTimeは(システム周期))である．
 *     ・TWD2の先頭でTask2_Mainが起動するためのアラームとして，ActTask2Armをコンフィギュレーションされている．
 *       ・そのOsAlarmAlarmTimeは (TWD1の長さ), OsAlarmCycleTimeは (システム周期))である．
 *     ・アイドルウィンドウの先頭でTaskD_Mainが起動するためのアラームとして，ActTaskDArmをコンフィギュレーションされている．
 *       ・そのOsAlarmAlarmTimeは(TWD1の長さ)+(TWD2の長さ), OsAlarmCycleTimeは(システム周期))である．
 *   以上により，本サンプルプログラムを実行すると，タスクがTask1_Main→Task2_Main→TaskD_Mainの順で繰り返し実行される．
 *   本サンプルプログラムでは，システム周期違反による，プロテクションフック呼び出し(引数がE_OS_PROTECTION_TIMEWINDOWのもの)は確認しない．
 *   また，シリアルによるコマンド入力によって，各タスクを任意のタイミングで起動させることが出来る
 *     '1'・・・ActivateTask(Task1_Main)
 *     'q'・・・ActivateTask(Task1_High)
 *     'a'・・・ActivateTask(Task1_Low)
 *     '2'・・・ActivateTask(Task2_Main)
 *     'w'・・・ActivateTask(Task2_High)
 *     's'・・・ActivateTask(Task2_Low)
 *     '3'・・・ActivateTask(TaskD_Main)
 *     'e'・・・ActivateTask(TaskD_High)
 *     'd'・・・ActivateTask(TaskD_Low)
 *   これにより，スケジューラの動作確認を行うことが出来る.
 *
 * ・リソース獲得監視機能
 *   本サンプルプログラムでは以下のリソースが用意されている．
 *   OSAPに所属するリソースとして，ResOsapInternal1とResOsapInternal2が用意されている．
 *   ResOsapInternal1はOSAP1に所属し，ResOsapInternal2はOSAP2に所属する．
 *   OSAPに所属しないリソースとして，ResOsapExternal12が用意されている．
 *   ResOsapExternal12はOSAP1，OSAP2の両方から獲得される．
 *   また，リソース獲得/解放用タスクとして以下のタスクを用意する．
 *     OSAP1に所属するTask1_Res
 *     OSAP2に所属するTask2_Res
 *   上記のタスクは起動時にResOsapExternal12を獲得/解放する
 *   OSAP1は信頼OSAPであるため，Task1_Resはこのリソースを獲得/解放可能であることが確認出来る
 *   OSAP2は非信頼OSAPであるため，Task2_Resはこのリソースを獲得時にエラーフックが呼び出されることを確認出来る
 *   上記のタスクは，シリアルによるコマンド入力によって任意のタイミングで起動させることが出来る
 *     'r'・・・ActivateTask(Task1_Res)
 *     'f'・・・ActivateTask(Task2_Res)
 *
 * ・割込み禁止監視機能
 *   割り込み禁止/解除用タスクとして以下のタスクを用意する
 *     OSAP1に所属するTask1_Int
 *     OSAP2に所属するTask2_Int
 *   上記のタスクは起動時にDisableAllInterruptsとEnableAllInterruptsを順に呼び出す
 *   OSAP1は信頼OSAPであるため，Task1_Intは割込み禁止解除が可能であることが確認出来る
 *   OSAP2は非信頼OSAPであるため，Task2_Intは割込み禁止時にエラーフックが呼び出されることを確認出来る
 *   上記のタスクは，シリアルによるコマンド入力によって任意のタイミングで起動させることが出来る
 *     'i'・・・ActivateTask(Task1_Int)
 *     'k'・・・ActivateTask(Task2_Int)．
 *
 * ・コマンド入力の仕組み
 *   コマンド入力用の低優先度アイドルタスク(TaskD_Cmd)が存在する．
 *   TaskD_Cmd内で関数GetCommandを呼び出し，そこでユーザが入力したコマンドをグローバル変数commandに格納する．
 *   関数SubRoutineを呼び出し，その中でcommandの値によってそれぞれに対応した処理をおこなう．
 */

#include "Os.h"
#include "t_syslog.h"
#include "t_stdlib.h"
#include "sysmod/serial.h"
#include "sysmod/syslog.h"
#include "sample1.h"

#include "sysmod/banner.h"
#include "target_sysmod.h"
#include "target_serial.h"
#include "target_hw_counter.h"
#include "target_test.h"


#define GetHwCnt(x, y)
#define GetAppModeInfo()	(0)

/*
 *  ファイル名，行番号の参照用の変数
 */
extern const char8	*fatal_file_name;   /* ファイル名 */
extern sint32		fatal_line_num;     /* 行番号 */

/*
 *  サービスコールのエラーのログ出力
 */
LOCAL_INLINE void
svc_perror(const char8 *file, sint32 line, const char8 *expr, StatusType ercd)
{
	if (ercd > 0U) {
		t_perror(LOG_ERROR, file, line, expr, ercd);
	}
}

#define SVC_PERROR(expr)	svc_perror(__FILE__, __LINE__, # expr, (expr))

/*
 *  内部関数プロトタイプ宣言
 */
sint32 main(void);
static uint8 GetCommand(void);
void SubRoutine(uint8);
TASK(Task1_Main);
TASK(Task1_High);
TASK(Task1_Low);
TASK(TaskD_Main);
TASK(TaskD_High);
TASK(TaskD_Low);
TASK(TaskD_Cmd);

TASK(Task_MemproOK);
TASK(Task_C2IsrCall);

#pragma section kernel
uint8 command;
#pragma section default

/*
 *  APIエラーログマクロ
 *
 *  ErrorHookが有効の場合はErrorHookから
 *  エラーログを出力し, ErrorHookが無効の場合は
 *  以下のマクロよりエラーログ出力を行う
 */
#if defined(CFG_USE_ERRORHOOK)
#define error_log(api)	(api)
#else /* !defined( CFG_USE_ERRORHOOK ) */
#define	error_log(api)										   \
	{														   \
		StatusType ercd;									   \
		ercd = api;     /* 各API実行 */						   \
		if (ercd != E_OK) {									   \
			syslog(LOG_INFO, "Error:%d", atk2_strerror(ercd)); \
		}													   \
	}
#endif /* defined( CFG_USE_ERRORHOOK ) */

/*
 *  ユーザメイン関数
 *
 *  アプリケーションモードの判断と，カーネル起動
 */
sint32
main(void)
{
	AppModeType	crt_app_mode;

	/*
	 *  アプリケーションモードの判断
	 */
	switch (GetAppModeInfo()) {
	case 0:
		crt_app_mode = AppMode1;
		break;
	case 1:
		crt_app_mode = AppMode2;
		break;
	default:
		crt_app_mode = AppMode3;
		break;
	}

	/*
	 *  カーネル起動
	 */
	StartOS(crt_app_mode);

	while (1) {
	}
}   /* main */

TASK(Task1_Main)
{
	syslog(LOG_INFO, "Task1_Main RUNNING");
	error_log(TerminateTask());
}   /* TASK( Task1_Main ) */

TASK(Task1_High)
{
	StatusType ercd;

	syslog(LOG_INFO, "Task1_High RUNNING");

	ercd = TerminateApplication(OsApplication4, RESTART);
	syslog(LOG_INFO, "TerminateApplication(OsApplication4, RESTART), state = %d", ercd);

	error_log(TerminateTask());
}   /* TASK( Task1_High ) */

TASK(Task1_Low)
{
	StatusType ercd;

	syslog(LOG_INFO, "Task1_Low RUNNING");

	ercd = TerminateApplication(OsApplication2, RESTART);
	syslog(LOG_INFO, "TerminateApplication(OsApplication2, RESTART), state = %d", ercd);
	error_log(TerminateTask());
}   /* TASK( Task1_Low ) */

TASK(Task1_Res)
{
	syslog(LOG_INFO, "Task1_Res RUNNING");
	GetResource(ResOsapExternal12);
	ReleaseResource(ResOsapExternal12);
	error_log(TerminateTask());
}   /* TASK( Task1_Res ) */

TASK(Task1_Int)
{
	syslog(LOG_INFO, "Task1_Int RUNNING");
	DisableAllInterrupts();
	EnableAllInterrupts();
	error_log(TerminateTask());
}   /* TASK( Task1_Int ) */


TASK(TaskD_Main)
{
	syslog(LOG_INFO, "TaskD_Main RUNNING");
	error_log(TerminateTask());
}   /* TASK( TaskD_Main ) */

TASK(TaskD_High)
{
	syslog(LOG_INFO, "TaskD_High RUNNING");
	error_log(TerminateTask());
}   /* TASK( TaskD_High ) */

TASK(TaskD_Low)
{
	syslog(LOG_INFO, "TaskD_Low RUNNING");
	error_log(TerminateTask());
}   /* TASK( TaskD_Low ) */

/* 信頼OSAPからOSメモリにアクセス可能なことを確認 */
TASK(Task_MemproOK)
{
	sint32 test;

	syslog(LOG_INFO, "Task_MemproOK RUNNING");
	test = fatal_line_num;
	syslog(test, "Task_MemproOK");
	error_log(TerminateTask());
}   /* TASK( Task_MemproOK ) */

TASK(Task_C2IsrCall)
{
	syslog(LOG_INFO, "Task_C2IsrCall RUNNING");

	/* 割込み要求 */
	Sample1RaiseInterrupt();

	syslog(LOG_INFO, "C2ISR returen");

	error_log(TerminateTask());
}   /* TASK( Task_C2IsrCall ) */

ISR(tptestC2ISR)
{
	StatusType ercd;

	syslog(LOG_INFO, "C2ISR");

	/* 割込み要求のクリア */
	Sample1ClearIntRequest();

	ercd = TerminateApplication(OsApplication4, RESTART);
	syslog(LOG_INFO, "TerminateApplication(OsApplication2, RESTART), state = %d", ercd);

}

/* コマンド入力用のタスク */
TASK(TaskD_Cmd)
{
#if 0
	syslog(LOG_INFO, "TaskD_Cmd RUNNING");
#endif
	while (1) {
		command = GetCommand();
		/* コマンド実行 */
		SubRoutine(command);
	}
	error_log(TerminateTask());
}   /* TASK( TaskD_Cmd ) */

/*
 *  エラーフックルーチン
 */
#ifdef CFG_USE_ERRORHOOK
void
ErrorHook(StatusType Error)
{
	/*
	 *  エラー要因ごとのパラメータログ出力
	 */
	switch (OSErrorGetServiceId()) {
	case OSServiceId_ActivateTask:
		syslog(LOG_INFO, "Error:%s=ActivateTask(%d)", atk2_strerror(Error), OSError_ActivateTask_TaskID());
		break;
	case OSServiceId_TerminateTask:
		syslog(LOG_INFO, "Error:%s=TerminateTask()", atk2_strerror(Error));
		break;
	case OSServiceId_ChainTask:
		syslog(LOG_INFO, "Error:%s=ChainTask(%d)", atk2_strerror(Error), OSError_ChainTask_TaskID());
		break;
	case OSServiceId_Schedule:
		syslog(LOG_INFO, "Error:%s=Schedule()", atk2_strerror(Error));
		break;
	case OSServiceId_GetTaskID:
		syslog(LOG_INFO, "Error:%s=GetTaskID(0x%p)", atk2_strerror(Error), OSError_GetTaskID_TaskID());
		break;
	case OSServiceId_GetTaskState:
		syslog(LOG_INFO, "Error:%s=GetTaskState(%d, 0x%p)", atk2_strerror(Error),
			   OSError_GetTaskState_TaskID(), OSError_GetTaskState_State());
		break;
	case OSServiceId_EnableAllInterrupts:
		syslog(LOG_INFO, "Error:%s=EnableAllInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_DisableAllInterrupts:
		syslog(LOG_INFO, "Error:%s=DisableAllInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_ResumeAllInterrupts:
		syslog(LOG_INFO, "Error:%s=ResumeAllInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_SuspendAllInterrupts:
		syslog(LOG_INFO, "Error:%s=SuspendAllInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_ResumeOSInterrupts:
		syslog(LOG_INFO, "Error:%s=ResumeOSInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_SuspendOSInterrupts:
		syslog(LOG_INFO, "Error:%s=SuspendOSInterrupts()", atk2_strerror(Error));
		break;
	case OSServiceId_GetISRID:
		syslog(LOG_INFO, "Error:%s=GetISRID()", atk2_strerror(Error));
		break;
	case OSServiceId_GetResource:
		syslog(LOG_INFO, "Error:%s=GetResource(%d)", atk2_strerror(Error), OSError_GetResource_ResID());
		break;
	case OSServiceId_ReleaseResource:
		syslog(LOG_INFO, "Error:%s=ReleaseResource(%d)", atk2_strerror(Error), OSError_ReleaseResource_ResID());
		break;
	case OSServiceId_SetEvent:
		syslog(LOG_INFO, "Error:%s=SetEvent(%d, 0x%x)", atk2_strerror(Error),
			   OSError_SetEvent_TaskID(), OSError_SetEvent_Mask());
		break;
	case OSServiceId_ClearEvent:
		syslog(LOG_INFO, "Error:%s=ClearEvent(0x%x)", atk2_strerror(Error), OSError_ClearEvent_Mask());
		break;
	case OSServiceId_GetEvent:
		syslog(LOG_INFO, "Error:%s=GetEvent(%d, 0x%p)", atk2_strerror(Error),
			   OSError_GetEvent_TaskID(), OSError_GetEvent_Event());
		break;
	case OSServiceId_WaitEvent:
		syslog(LOG_INFO, "Error:%s=WaitEvent(0x%x)", atk2_strerror(Error), OSError_WaitEvent_Mask());
		break;
	case OSServiceId_GetAlarmBase:
		syslog(LOG_INFO, "Error:%s=GetAlarmBase(0x%p)", atk2_strerror(Error), OSError_GetAlarmBase_AlarmID());
		break;
	case OSServiceId_GetAlarm:
		syslog(LOG_INFO, "Error:%s=GetAlarm(%d, 0x%p)", atk2_strerror(Error),
			   OSError_GetAlarm_AlarmID(), OSError_GetAlarm_Tick());
		break;
	case OSServiceId_SetRelAlarm:
		syslog(LOG_INFO, "Error:%s=SetRelAlarm(%d, %d, %d)", atk2_strerror(Error),
			   OSError_SetRelAlarm_AlarmID(), OSError_SetRelAlarm_increment(), OSError_SetRelAlarm_cycle());
		break;
	case OSServiceId_SetAbsAlarm:
		syslog(LOG_INFO, "Error:%s=SetAbsAlarm(%d, %d, %d)", atk2_strerror(Error),
			   OSError_SetAbsAlarm_AlarmID(), OSError_SetAbsAlarm_start(), OSError_SetAbsAlarm_cycle());
		break;
	case OSServiceId_CancelAlarm:
		syslog(LOG_INFO, "Error:%s=CancelAlarm(%d)", atk2_strerror(Error), OSError_CancelAlarm_AlarmID());
		break;
	case OSServiceId_StartScheduleTableRel:
		syslog(LOG_INFO, "Error:%s=StartScheduleTableRel(%d, %d)", atk2_strerror(Error),
			   OSError_StartScheduleTableRel_ScheduleTableID(), OSError_StartScheduleTableRel_Offset());
		break;
	case OSServiceId_StartScheduleTableAbs:
		syslog(LOG_INFO, "Error:%s=StartScheduleTableAbs(%d, %d)", atk2_strerror(Error),
			   OSError_StartScheduleTableAbs_ScheduleTableID(), OSError_StartScheduleTableAbs_Start());
		break;
	case OSServiceId_StopScheduleTable:
		syslog(LOG_INFO, "Error:%s=StopScheduleTable(%d)", atk2_strerror(Error), OSError_StopScheduleTable_ScheduleTableID());
		break;
	case OSServiceId_NextScheduleTable:
		syslog(LOG_INFO, "Error:%s=NextScheduleTable(%d, %d)", atk2_strerror(Error),
			   OSError_NextScheduleTable_ScheduleTableID_From(), OSError_NextScheduleTable_ScheduleTableID_To());
		break;
	case OSServiceId_GetScheduleTableStatus:
		syslog(LOG_INFO, "Error:%s=GetScheduleTableStatus(%d, 0x%p)", atk2_strerror(Error),
			   OSError_GetScheduleTableStatus_ScheduleTableID(), OSError_GetScheduleTableStatus_ScheduleStatus());
		break;
	case OSServiceId_GetActiveApplicationMode:
		syslog(LOG_INFO, "Error:%s=GetActiveApplicationMode()", atk2_strerror(Error));
		break;
	case OSServiceId_StartOS:
		syslog(LOG_INFO, "Error:%s=StartOS()", atk2_strerror(Error));
		break;
	case OSServiceId_ShutdownOS:
		syslog(LOG_INFO, "Error:%s=ShutdownOS()", atk2_strerror(Error));
		break;
	case OSServiceId_IncrementCounter:
		syslog(LOG_INFO, "Error:%s=IncrementCounter(%d)", atk2_strerror(Error), OSError_IncrementCounter_CounterID());
		break;
	case OSServiceId_TaskMissingEnd:
		syslog(LOG_INFO, "Error:%s=MissingEnd()", atk2_strerror(Error));
		break;
	default:
		syslog(LOG_INFO, "Error:%s=UnKnownFunc()", atk2_strerror(Error));
		break;
	}

}   /* ErrorHook */
#endif /* CFG_USE_ERRORHOOK */

/*
 *  プレタスクフックルーチン
 *
 *  空ルーチンを呼出す
 */
#ifdef CFG_USE_PRETASKHOOK
void
PreTaskHook(void)
{
}   /* PreTaskHook */
#endif /* CFG_USE_PRETASKHOOK */

/*
 *  ポストタスクフックルーチン
 *
 *  空ルーチンを呼出す
 */
#ifdef CFG_USE_POSTTASKHOOK
void
PostTaskHook(void)
{
}   /* PostTaskHook */
#endif /* CFG_USE_POSTTASKHOOK */

/*
 *  スタートアップフックルーチン
 */
#ifdef CFG_USE_STARTUPHOOK
#ifdef TOPPERS_ENABLE_SYS_TIMER
extern void target_timer_initialize(void);
#endif /* TOPPERS_ENABLE_SYS_TIMER */

void
StartupHook(void)
{
#ifdef TOPPERS_ENABLE_SYS_TIMER
	target_timer_initialize();
#endif /* TOPPERS_ENABLE_SYS_TIMER */
	syslog_initialize();
	syslog_msk_log(LOG_UPTO(LOG_INFO));
	InitSerial();
	print_banner();
}   /* StartupHook */
#endif /* CFG_USE_STARTUPHOOK */

/*
 *  シャットダウンフックルーチン
 */
#ifdef CFG_USE_SHUTDOWNHOOK
#ifdef TOPPERS_ENABLE_SYS_TIMER
extern void target_timer_terminate(void);
#endif /* TOPPERS_ENABLE_SYS_TIMER */

void
ShutdownHook(StatusType Error)
{
	/* 終了ログ出力 */
	syslog(LOG_INFO, "");
	syslog(LOG_INFO, "Sample System ShutDown");
	syslog(LOG_INFO, "ShutDownCode:%s", atk2_strerror(Error));
	syslog(LOG_INFO, "");

	if (Error == E_OS_SYS_ASSERT_FATAL) {
		syslog(LOG_INFO, "fatal_file_name:%s", fatal_file_name);
		syslog(LOG_INFO, "fatal_line_num:%d", fatal_line_num);
	}

#ifdef TOPPERS_ENABLE_SYS_TIMER
	target_timer_terminate();
#endif /* TOPPERS_ENABLE_SYS_TIMER */
	TermSerial();

}   /* ShutdownHook */
#endif /* CFG_USE_SHUTDOWNHOOK */

/*
 *  プロテクションフックルーチン
 */
#ifdef CFG_USE_PROTECTIONHOOK
ProtectionReturnType
ProtectionHook(StatusType FatalError)
{
	StatusType ercd;

	syslog(LOG_INFO, "");
	syslog(LOG_INFO, "ProtectionHook");

	if (FatalError == E_OS_STACKFAULT) {
		syslog(LOG_INFO, "E_OS_STACKFAULT");
		ercd = PRO_SHUTDOWN;
	}
	else if (FatalError == E_OS_PROTECTION_MEMORY) {
		ercd = PRO_TERMINATEAPPL_RESTART;
	}
	else if ((FatalError == E_OS_PROTECTION_EXCEPTION) || (FatalError == E_OS_PROTECTION_COUNT_ISR)) {
		syslog(LOG_INFO, "E_OS_PROTECTION_EXCEPTION");
		ercd = PRO_IGNORE;
	}
	else {
		ercd = PRO_SHUTDOWN;
	}

	return(ercd);
}
#endif /* CFG_USE_PROTECTIONHOOK */

/*
 *  コマンド受信処理
 */
static uint8
GetCommand(void)
{
	uint8 command;          /* コマンド受信バッファ */

	/*
	 *  コマンドを受信するまでループ
	 */
	command = '\0';
	RecvPolSerialChar(&command);    /* 受信バッファポーリング */
	if (command == '\n') {
		command = '\0';
	}

	return(command);
} /* GetCommand */


void
SubRoutine(uint8 command)
{

	switch (command) {
	case '1':
		syslog(LOG_INFO, "ActivateTask(Task1_Main)");
		ActivateTask(Task1_Main);
		break;
	case 'q':
		syslog(LOG_INFO, "ActivateTask(Task1_High)");
		ActivateTask(Task1_High);
		break;
	case 'a':
		syslog(LOG_INFO, "ActivateTask(Task1_Low)");
		ActivateTask(Task1_Low);
		break;
	case '2':
		syslog(LOG_INFO, "ActivateTask(Task2_Main)");
		ActivateTask(Task2_Main);
		break;
	case 'w':
		syslog(LOG_INFO, "ActivateTask(Task2_High)");
		ActivateTask(Task2_High);
		break;
	case 's':
		syslog(LOG_INFO, "ActivateTask(Task2_Low)");
		ActivateTask(Task2_Low);
		break;
	case '3':
		syslog(LOG_INFO, "ActivateTask(TaskD_Main)");
		ActivateTask(TaskD_Main);
		break;
	case 'e':
		syslog(LOG_INFO, "ActivateTask(TaskD_High)");
		ActivateTask(TaskD_High);
		break;
	case 'd':
		syslog(LOG_INFO, "ActivateTask(TaskD_Low)");
		ActivateTask(TaskD_Low);
		break;
	case 'r':
		syslog(LOG_INFO, "ActivateTask(Task1_Res)");
		ActivateTask(Task1_Res);
		break;
	case 'i':
		syslog(LOG_INFO, "ActivateTask(Task1_Int)");
		ActivateTask(Task1_Int);
		break;
	case 'f':
		syslog(LOG_INFO, "ActivateTask(Task2_Res)");
		ActivateTask(Task2_Res);
		break;
	case 'k':
		syslog(LOG_INFO, "ActivateTask(Task2_Int)");
		ActivateTask(Task2_Int);
		break;
	case 'o':
		syslog(LOG_INFO, "ActivateTask(Task_MemproOK)");
		ActivateTask(Task_MemproOK);
		break;
	case 'n':
		syslog(LOG_INFO, "ActivateTask(Task_MemproNG)");
		ActivateTask(Task_MemproNG);
		break;
	case 'm':
		syslog(LOG_INFO, "ActivateTask(Task3_Main)");
		ActivateTask(Task3_Main);
		break;
	case 'l':
		syslog(LOG_INFO, "ActivateTask(Task2_NonTrusted)");
		ActivateTask(Task2_NonTrusted);
		break;
	case 'p':
		syslog(LOG_INFO, "ActivateTask(Task_C2IsrCall)");
		ActivateTask(Task_C2IsrCall);
		break;
	}
}
