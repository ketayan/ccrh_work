/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2015 by Witz Corporation
 *  Copyright (C) 2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2015 by SCSK Corporation, JAPAN
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_tp_timer.c 409 2014-09-22 08:17:30Z t_ishikawa $
 */

#include "Os.h"
#include "prc_sil.h"
#include "timewindow.h"
#include "interrupt.h"
#include "target_tp_timer.h"
#include "target_hw_counter.h"

static uint32 current_tw_remain_time = 0U;
static uint32 next_tw_reserve_time = 0U;
static uint8 tw_timer_start = 0U;

void
target_initialize_systemcycle_timer(void)
{
	uint16	wk;

	/* システム周期タイマの停止 */
	SetTimerStopTAUJ(0, 3);
	/* 割込み禁止 */
	HwcounterDisableInterrupt(SYSTEMCYCLETIMER_INTNO);
	/* 割込み要求クリア */
	HwcounterClearInterrupt(SYSTEMCYCLETIMER_INTNO);

	/* 
     *  ユニット0のプリスケーラを設定 PCLK/2^0
     *  ハードウェアカウンタと同じ設定
     */
	wk = sil_reh_mem((void *) TAUJTPS(0));
	wk &= MCU_TAUJ_MASK_CK0;
	wk |= MCU_TAUJ_CK0_0;
	sil_wrh_mem((void *) TAUJTPS(0), wk);

	/* 
     *  システム周期タイマの設定
     *  クロック選択 CK0
     *  インターバルタイマ
     *  立ち上がりエッジ
     */
	sil_wrh_mem((void *) TAUJCMOR(0, 3), MCU_TAUJ00_CMOR);
	sil_wrb_mem((void *) TAUJCMUR(0, 3), MCU_TAUJ00_CMUR);

	/* タイマカウント周期設定 */
	sil_wrw_mem((void *) TAUJCDR(0, 3), syscyc);

	x_config_int(SYSTEMCYCLETIMER_INTNO, ENABLE, TPRI_TPTIMER);
}


void
target_start_systemcycle_timer(void)
{
	/* システム周期用タイマはタイムアウト後自動で再スタートするように設定 */
	SetTimerStartTAUJ(0, 3);
}

void
target_intclear_systemcycle_timer(void)
{
	/* タイマ割込み要求のクリア */
	HwcounterClearInterrupt(SYSTEMCYCLETIMER_INTNO);
}

void
target_stop_systemcycle_timer(void)
{
	/* タイマ停止 */
	SetTimerStopTAUJ(0, 3);

	/* タイマ割込み要求のクリア */
	HwcounterClearInterrupt(SYSTEMCYCLETIMER_INTNO);
}

boolean
target_get_timeoutbit_systemcycle_timer(void)
{
	/* 割込み制御レジスタ */
	uint32 eic_address = EIC_ADDRESS(SYSTEMCYCLETIMER_INTNO);

	/* 割込み要求ビットの確認 */
	if ((sil_reh_mem((void *) eic_address) & EIRFn) != 0x00U) {
		return(TRUE);
	}
	else {
		return(FALSE);
	}
}

void
target_initialize_timewindow_timer(void)
{
	/* タイムウィンドウタイマの停止 */
	SetTimerStopTAUJ(0, 2);
	/* 割込み禁止 */
	HwcounterDisableInterrupt(TIMEWINDOWTIMER_INTNO);
	/* 割込み要求クリア */
	HwcounterClearInterrupt(TIMEWINDOWTIMER_INTNO);

	/* 
     *  システム周期タイマの設定
     *  クロック選択 CK0
     *  ワンショットタイマ
     *  立ち上がりエッジ
     */
	sil_wrh_mem((void *) TAUJCMOR(0, 2), (MCU_TAUJ00_CMOR));
	sil_wrb_mem((void *) TAUJCMUR(0, 2), MCU_TAUJ00_CMUR);
    
    tw_timer_start = 0U;
    current_tw_remain_time = 0U;
    next_tw_reserve_time = 0U;

	x_config_int(TIMEWINDOWTIMER_INTNO, ENABLE, TPRI_TPTIMER);
}

void
target_set_timewindow_timer(TickType tick)
{
	/* 現在のパーティションのタイマカウント周期設定 */
    current_tw_remain_time = tick;
	/* 次のパーティションのタイマカウント周期設定 */
    if (current_timewindow < (tnum_timewindow - 1)) {
        /* 次のパーティションがアイドルウィンドウでない */
        next_tw_reserve_time = twinib_table[current_timewindow+1].budget;
    }
    else {
        next_tw_reserve_time = 0;
    }
}

void
target_start_timewindow_timer(void)
{
    if (tw_timer_start == 0U) {
        /* 現在のパーティションの残りタイマカウントを復帰 */
        /* ISRによる中断からの復帰もしくは初回パーティション起動時 */
        sil_wrw_mem((void *) TAUJCDR(0, 2), current_tw_remain_time);
	    SetTimerStartTAUJ(0, 2);
        tw_timer_start = 1U;
    	/*
		 * タイマをスタートさせてから再びCDRに書き込むと，新しくCDRに書き込んだ値を
		 * CNTにロードする可能性があるため，タイマのスタート(CDRのCNTへのロード)を待つ．
		 */
		while((sil_reh_mem((void *) TAUJTE(0)) & (1 << 2)) == 0);
    }
	/* 次のパーティションのタイマカウント周期設定 */
    sil_wrw_mem((void *) TAUJCDR(0, 2), next_tw_reserve_time);
}


void
target_stop_timewindow_timer(void)
{
	/* タイマ停止 */
	SetTimerStopTAUJ(0, 2);
    tw_timer_start = 0U;
    /* 現在のパーティションの残りタイマカウントを保存 */
    current_tw_remain_time = sil_rew_mem((void *) TAUJCNT(0, 2));

	/* タイマ割込み要求のクリア */
	HwcounterClearInterrupt(TIMEWINDOWTIMER_INTNO);
}


void
target_intclear_timewindow_timer(void)
{
    if (current_timewindow == (tnum_timewindow - 1)) {
        /*
         *  アイドルウィンドウ直前にタイマを停止
         */
        SetTimerStopTAUJ(0, 2);
        tw_timer_start = 0U;
    }
	/* タイマ割込み要求のクリア */
	HwcounterClearInterrupt(TIMEWINDOWTIMER_INTNO);
}
