/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: prc_config.c 465 2015-12-10 11:16:18Z witz-itoyo $
 */

/*
 *		プロセッサ依存モジュール（Nios2用）
 */
#include "kernel_impl.h"
/*
 *  例外コード/エラーコード変換テーブル
 */
const StatusType exc_code_tbl[TNUM_EXCH] = {
	E_OS_PROTECTION_EXCEPTION,  /*  0 Reset */
	E_OS_PROTECTION_EXCEPTION,  /*  1 Processor-only Reset Request */
	E_OS_PROTECTION_EXCEPTION,  /*  2 Interrupt */
	E_OS_PROTECTION_EXCEPTION,  /*  3 Trap Instruction */
	E_OS_PROTECTION_EXCEPTION,  /*  4 Unimplemented Instruction */
	E_OS_PROTECTION_EXCEPTION,  /*  5 Illegal Instruction */
	E_OS_PROTECTION_EXCEPTION,  /*  6 Misaligned Data Address */
	E_OS_PROTECTION_EXCEPTION,  /*  7 Misaligned Destination Address */
	E_OS_PROTECTION_EXCEPTION,  /*  8 Division Error */
	E_OS_PROTECTION_EXCEPTION,  /*  9 Supervisor-only Instruction Address */
	E_OS_PROTECTION_EXCEPTION,  /* 10 Supervisor-only Instruction */
	E_OS_PROTECTION_EXCEPTION,  /* 11 Supervisor-only Data Address */
	E_OS_PROTECTION_EXCEPTION,  /* 12 Fast/Double TBL miss (for MMU) */
	E_OS_PROTECTION_EXCEPTION,  /* 13 TBL Permission Violation (execute)(for MMU) */
	E_OS_PROTECTION_EXCEPTION,  /* 14 TBL Permission Violation (read)(for MMU) */
	E_OS_PROTECTION_EXCEPTION,  /* 15 TBL Permission Violation (write)(for MMU) */
	E_OS_PROTECTION_MEMORY,     /* 16 MPU Region Violation(instruction) */
	E_OS_PROTECTION_MEMORY      /* 17 MPU Region Violation(data) */
};

/*
 *  CPU例外要因情報保持変数外部参照宣言
 */
#ifdef CFG_USE_PROTECTIONHOOK
/* CPU例外発生時，CPU例外要因番号を保持する変数 */
DEFINE_VAR_SEC_NOBITS(uint32, nios2_cpu_exp_no[TotalNumberOfCores], ".srpw_bss_kernel");

/* CPU例外発生時，プログラムカウンタ（PC - 4）を保持する変数 */
DEFINE_VAR_SEC_NOBITS(uint32, nios2_cpu_exp_pc[TotalNumberOfCores], ".srpw_bss_kernel");

/* CPU例外発生時，CPU例外発生箇所（アドレス）を保持する変数 */
DEFINE_VAR_SEC_NOBITS(uint32, nios2_cpu_exp_bad_addr[TotalNumberOfCores], ".srpw_bss_kernel");

/* CPU例外発生時，スタックポインタを保持する変数 */
DEFINE_VAR_SEC_NOBITS(uint32, nios2_cpu_exp_sp[TotalNumberOfCores], ".srpw_bss_kernel");
#endif /* CFG_USE_PROTECTIONHOOK */

void
prc_hardware_initialize(void)
{
	vic_initialize();
}

#ifdef PERF_TEST
/*
 *  キャッシュパージフラグ
 *    セットされるとソフトウェア割込みエントリの終了処理で
 *    キャッシュをパージする
 */
uint32 cache_flash_flag;
#endif /* PERF_TEST */

/*
 *  プロセッサ依存の初期化
 */
void
prc_initialize(void)
{
	TCCB *p_tccb = &(get_my_p_ccb()->target_ccb);

	/*
	 *  カーネル起動時は非タスクコンテキストとして動作させるため1に
	 */
	p_tccb->except_nest_cnt = 1U;

	p_tccb->nested_lock_os_int_cnt = 0U;
	p_tccb->saved_status_il = 0U;

	/*
	 *  現在MPUに設定しているスタックを所有するタスクのTCB
	 *  タスクが存在しないので，NULLに初期化する
	 */
	p_tccb->p_ctxstk = NULL;

	/*
	 *  現在MPUに設定しているOSAPが存在しないので，NULLに初期化する
	 */
	p_tccb->p_ctxosap = NULL;

#ifdef PERF_TEST
	/*
	 *  キャッシュパージフラグ
	 *    セットされるとソフトウェア割込みエントリの終了処理で
	 *    キャッシュをパージする
	 */
	cache_flash_flag = 0U;
#endif /* PERF_TEST */

	/*
	 *  MPU初期化
	 *    MPU情報生成
	 *    MPU設定
	 *    MPU有効
	 */
	prc_init_mpu();
}

/*
 *  プロセッサ依存の終了処理
 */
void
prc_terminate(void)
{
	CoreIdType coreid;

	coreid = x_core_id();

	/* MPU 無効 */
	mpu_disable();

	/*
	 *  コアが停止しているかを判定するために用いるレジスタをクリア
	 */
	*((volatile uint32 *) (SYSVER_REG5 + (4U * (uint32) (coreid)))) = 0U;
}

/*
 *  割込み要求ラインの属性の設定
 */
void
x_config_int(InterruptNumberType intno, AttributeType intatr, PriorityType intpri, CoreIdType coreid)
{
	ASSERT(VALID_INTNO(intno));

	if (coreid == x_core_id()) {
		/*
		 *  いったん割込みを禁止する
		 */
		x_disable_int(intno);

		/* 割込み優先度設定，設定しない場合最低優先度0となる */
		vic_set_int_config((uint8) INTNO_MASK(intno), INT_IPM(intpri));

		if ((intatr & ENABLE) != 0U) {
			/*
			 *  初期状態が割込み許可であれば，割込みを許可
			 */
			x_enable_int(intno);
		}
	}
}

#ifndef OMIT_DEFAULT_INT_HANDLER

/*
 *  未定義の割込みが入った場合の処理
 */
void
default_int_handler(void)
{
	target_fput_str("Unregistered Interrupt occurs.");
	ASSERT_NO_REACHED;
}

#endif /* OMIT_DEFAULT_INT_HANDLER */

/*
 *  不正な機能コードを指定して，システムサービス発行時の処理
 */

StatusType
no_support_service(void)
{
	StatusType ercd = E_OS_SERVICEID;

#ifdef CFG_USE_ERRORHOOK
	x_nested_lock_os_int();
	call_errorhook(ercd, OSServiceId_INVALID);
	x_nested_unlock_os_int();
#endif /* CFG_USE_ERRORHOOK */

	return(ercd);
}
