/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_config.c 431 2015-12-08 11:59:08Z witz-itoyo $
 */

/*
 *		ターゲット依存モジュール（NIOS2_DEV用）
 */

#include "kernel_impl.h"
#include "mc.h"
#include "nios2_gcc/prc_sil.h"
#include "target_sysmod.h"
#include "target_ici.h"
#ifdef ENABLE_RETURN_MAIN
#include "interrupt.h"
#endif /* ENABLE_RETURN_MAIN */
#ifdef TOPPERS_ENABLE_TRACE
#include "logtrace/trace_config.h"
#endif /* TOPPERS_ENABLE_TRACE */

#ifdef ENABLE_RETURN_MAIN
#include "osap.h"
#endif /* ENABLE_RETURN_MAIN */

#ifdef USE_UART
#include "nios2_gcc/uart.h"

/*
 *  UARTのベースアドレス
 */
const uint32	target_fput_log_base_table[TotalNumberOfCores] = {
	UART_PORT0_BASE,
#if TotalNumberOfCores > 1
	UART_PORT1_BASE,
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	UART_PORT2_BASE,
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	UART_PORT3_BASE,
#endif /* TotalNumberOfCores > 3 */
};

#else /* USE_UART */
#include "nios2_gcc/jtag_uart.h"

/*
 *  JTAG UARTのベースアドレス
 */
const uint32	target_fput_log_base_table[TotalNumberOfCores] = {
	JTAG_UART_PORT0_BASE,
#if TotalNumberOfCores > 1
	JTAG_UART_PORT1_BASE,
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	JTAG_UART_PORT2_BASE,
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	JTAG_UART_PORT3_BASE,
#endif /* TotalNumberOfCores > 3 */
};

#endif /* USE_UART */

/* 内部関数のプロトタイプ宣言 */
static void hw_version_check(void);

/*
 *  コア間割込み番号
 */
const InterruptNumberType
target_ici_intno_table[TotalNumberOfCores] = {
	INTNO_ICI0,
#if TotalNumberOfCores > 1
	INTNO_ICI1,
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	INTNO_ICI2,
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	INTNO_ICI3,
#endif /* TotalNumberOfCores > 3 */
};

/*
 *  コア間割込み優先度
 */
const PriorityType
target_ici_intpri_table[TotalNumberOfCores] = {
	-(INTPRI_ICI0),
#if TotalNumberOfCores > 1
	-(INTPRI_ICI1),
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	-(INTPRI_ICI2),
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	-(INTPRI_ICI3),
#endif /* TotalNumberOfCores > 3 */
};

/*
 *  コア間割込みHWのベースアドレス
 */
const uint32	target_core_int_base_table[TotalNumberOfCores] = {
	CORE_INT_0_BASE,
#if TotalNumberOfCores > 1
	CORE_INT_1_BASE,
#endif /*TotalsNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	CORE_INT_2_BASE,
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	CORE_INT_3_BASE,
#endif /* TotalNumberOfCores > 3 */
};


#if TTYPE_KLOCK == C_KLOCK

/*
 *  タスクロックのテーブル
 */
const uint32	target_tsk_lock_table[TotalNumberOfCores] = {
	TSK_MUTEX_0_BASE,
#if TotalNumberOfCores > 1
	TSK_MUTEX_1_BASE,
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	TSK_MUTEX_2_BASE,
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	TSK_MUTEX_3_BASE,
#endif /* TotalNumberOfCores > 3 */
};

/*
 *  カウンタロックのテーブル
 */
const uint32	target_cnt_lock_table[TotalNumberOfCores] = {
	CNT_MUTEX_0_BASE,
#if TotalNumberOfCores > 1
	CNT_MUTEX_1_BASE,
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
	CNT_MUTEX_2_BASE,
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
	CNT_MUTEX_3_BASE,
#endif /* TotalNumberOfCores > 3 */
};

#if TTYPE_SPN == NATIVE_SPN
const uint32	target_spn_lock_table[TMAX_NATIVE_SPN] = {
	SPN_MUTEX_BASE,
#if TotalNumberOfCores <= 3
	TSK_MUTEX_3_BASE,
	CNT_MUTEX_3_BASE,
#endif /* TotalNumberOfCores <= 3 */
#if TotalNumberOfCores <= 2
	TSK_MUTEX_2_BASE,
	CNT_MUTEX_2_BASE,
#endif /* TotalNumberOfCores <= 2 */
};
#endif /* TTYPE_SPN == NATIVE_SPN */

#endif /* TTYPE_KLOCK == C_KLOCK */

/*
 *  文字列の出力
 */
void
target_fput_str(const char8 *c)
{
	while (*c != '\0') {
		nios2_dev_putc(*c);
		c++;
	}
	nios2_dev_putc('\n');
}

/*
 *  バージョンチェック
 */
static void
hw_version_check(void)
{
	/* コア数を確認 */
	if (sil_rew_iop((void *) (SYSVER_NUM_CORE)) != TNUM_HWCORE) {
		target_fput_str("Number of core is mismatch!!");
		while (1) {
		}
	}

	if (sil_rew_iop((void *) (SYSVER_MAJOR_VAR)) != MAJOR_VAR) {
		target_fput_str("Hardware Major version is mismatch!!");
		while (1) {
		}
	}
}
void
target_hardware_initialize(void)
{
	/*
	 *  ハードウェアバージョンの確認
	 */
	hw_version_check();

	prc_hardware_initialize();
}

/*
 *  ターゲット依存の初期化
 */
void
target_initialize(void)
{
	CoreIdType coreid = x_core_id();

	/*
	 *  Nios2依存の初期化
	 */
	prc_initialize();

#ifdef TOPPERS_ENABLE_TRACE
	/*
	 *  トレースログ機能の初期化(マスタコアのみ実施)
	 */
	if (coreid == OS_CORE_ID_MASTER) {
		trace_initialize((uintptr) (TRACE_AUTOSTOP));
	}
#endif /* TOPPERS_ENABLE_TRACE */

	x_config_int(GET_ICI_INTNO(coreid), ENABLE, GET_ICI_INTPRI(coreid), coreid);
}

/*
 *  ターゲット依存の終了処理
 */
void
target_exit(void)
{
#ifdef ENABLE_RETURN_MAIN
	CCB	*p_ccb = get_my_p_ccb();
#endif /* ENABLE_RETURN_MAIN */

#ifdef TOPPERS_ENABLE_TRACE
	/*
	 *  トレースログのダンプ(マスタコアのみ実施)
	 */
	if (x_core_id() == OS_CORE_ID_MASTER) {
		trace_dump(&target_fput_log);
	}
#endif /* TOPPERS_ENABLE_TRACE */

#ifndef ENABLE_RETURN_MAIN
	/*
	 *  シャットダウン処理の出力
	 */
	target_fput_str("Kernel Exit...");
#else
	target_fput_str("Kernel Shutdown...");
#endif /* ENABLE_RETURN_MAIN */

	/*
	 *  Nios2依存の終了処理
	 */
	prc_terminate();

#ifdef ENABLE_RETURN_MAIN
	p_ccb->kerflg = FALSE;
	p_ccb->sus_all_cnt = 0U;
	p_ccb->sus_os_cnt = 0U;
	p_ccb->target_ccb.except_nest_cnt = 0U;
	p_ccb->target_ccb.nested_lock_os_int_cnt = 0U;
	/* 全コアが終了されるのを同期する */
	barrier_sync(5U, FALSE);

	/* 全コアのOSスタックを初期に戻す */
	Asm("rdctl r3, cpuid");
	Asm("srli  r3, r3, 20");
	Asm("subi  r3, r3, 0x20");
	Asm("slli  r3, r3, 2");
	Asm("movhi r4, %hiadj(_ostkpt_table)");
	Asm("addi  r4, r4, %lo(_ostkpt_table)");
	Asm("add   r2, r4, r3");
	Asm("ldw   sp, 0(r2)");

	/* マスタコアは起動状態のままmain関数へ戻る */
	if (x_core_id() == OS_CORE_ID_MASTER) {
		/* 起動しているOS管理のコア数を1へ戻す */
		activated_cores = 1U;
		Asm("call  main");
	}
	else {
		/*
		 * スレーブコアは次のマスタコアからのStartCoreで
		 * コアが起動されるまで無限ループして待機する(slave_start相当)
		 */
		while (is_halt(x_core_id())) {
		}
		;
		Asm("call main");
	}
#endif /* ENABLE_RETURN_MAIN */

	while (1) {
	}
}

/*
 *  ターゲット依存の文字出力
 */
void
target_fput_log(char8 c)
{
	nios2_dev_putc(c);
}

/*
 *  特定の割込み要求ラインの有効/無効を制御可能かを調べる処理
 */
boolean
target_is_int_controllable(InterruptNumberType intno)
{
	/*
	 *  Nios2では全ての割込み要求ラインに対して
	 *  有効/無効を制御可能であるため，常にtrueを返す
	 */
	return(TRUE);
}

/*
 *	コア間割込みの実体定義
 */
ISR(target_ici_handler0)
{
	ici_handler_main();
}
#if TotalNumberOfCores > 1
ISR(target_ici_handler1)
{
	ici_handler_main();
}
#endif /* TotalNumberOfCores > 1 */
#if TotalNumberOfCores > 2
ISR(target_ici_handler2)
{
	ici_handler_main();
}
#endif /* TotalNumberOfCores > 2 */
#if TotalNumberOfCores > 3
ISR(target_ici_handler3)
{
	ici_handler_main();
}
#endif /* TotalNumberOfCores > 3 */
