/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2006 by GJ Business Division RICOH COMPANY,LTD. JAPAN
 *  Copyright (C) 2007-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: target_config.h 425 2015-12-07 08:06:19Z witz-itoyo $
 */

/*
 *		チップ依存モジュール（AT91SKYEYE用）
 *
 *  カーネルのチップ依存部のインクルードファイル
 *  kernel_impl.hのターゲット依存部の位置付けとなる
 */

#ifndef TOPPERS_TARGET_CONFIG_H
#define TOPPERS_TARGET_CONFIG_H

/*
 *  チップシステムのハードウェア資源の定義
 */
#include "at91skyeye.h"

/*
 *  トレースログに関する設定
 */
#ifdef TOPPERS_ENABLE_TRACE
#include "logtrace/trace_config.h"
#endif /* TOPPERS_ENABLE_TRACE */

/*
 *  サポートしているSKYEYのバージョン
 */
#define SUPPORT_SKYEYE_VER	0x1024
#define SUPPORT_DEVM_VER	0x2020

/*
 *  ベクタールーチンをカーネルで持つかの定義
 */
#define VECTOR_KERNEL

/*
 *  ベクタールーチンを持たない場合のベクターアドレスの先頭番地
 */
#define VECTOR_START	0x00

/*
 *  データセクションの初期化をしない
 */
#define TOPPERS_OMIT_DATA_INIT

/*
 * 割込み待ち命令
 */
#define ASM_TARGET_WAIT_INTERRUPT	nop

/*
 *  割込み番号に関する定義
 */
#define TMIN_INTNO	UINT_C(1)
#define TMAX_INTNO	18U
#define TNUM_INT	18U

/*
 * 割込み番号から，IRC操作のためのビットパターンを求めるマクロ
 */
#define INTNO_BITPAT(intno)	(1U << intno)


#ifndef TOPPERS_MACRO_ONLY

/*
 *  割込み番号の範囲の判定
 */
#define VALID_INTNO(intno)	(TMIN_INTNO <= (intno) && (intno) <= TMAX_INTNO)

/*
 *  TOPPERS標準割込み処理モデルの実現
 */

/*
 * 各割込みの割込み要求禁止フラグの状態
 */
extern uint32 idf;

/*
 *  割込み優先度マスク操作ライブラリ
 *
 *  AT91SAM7Sは割込み優先度マスクをIRC内でハードウェア的に持つが，
 *  ソフトウェア側から値を読み書きできないため，割込み要求禁止フラ
 *  グにより割込み優先度マスクを実現する
 */

/*
 *  現在の割込み優先度マスクの値
 */
extern PriorityType	ipm;

/*
 *  割込み優先度マスク毎にセットする，割込み要求禁止フラグの値
 *  のテーブル
 */
extern const uint32	ipm_mask_tbl[];

/*
 *  OS割込み禁止時の割込み要求禁止フラグの値
 *  Os_Lcfg.cで静的に生成する
 */
extern const uint32	os_int_lock_mask;


#endif /* TOPPERS_MACRO_ONLY */

/*
 *  IPMをimp_mask_tblのインデックスに変換するマクロ
 */
#define INDEX_IPM(ipm)	(-(ipm))

#ifndef TOPPERS_MACRO_ONLY

/*
 *  (モデル上の)割込み優先度マスクの設定
 *
 *  OS割込み禁止状態で呼ばれ，引数の優先度はOS割込み禁止以下の優先度で
 *  あるため，ハードウェアの割込みマスクを変更する必要はない
 *  優先度を内部変数ipmに保存する
 */
LOCAL_INLINE void
x_set_ipm(PriorityType intpri)
{
	ipm = intpri;
}

/*
 *  (モデル上の)割込み優先度マスクの参照
 *
 *  ipmの値を返す
 */
LOCAL_INLINE PriorityType
x_get_ipm(void)
{
	return(ipm);
}

/*
 *  割込み要求のチェック
 */
LOCAL_INLINE boolean
x_probe_int(InterruptNumberType intno)
{
	return(at91skyeye_probe_int(INTNO_BITPAT(intno)));
}

/*
 *  割込み要求ラインの属性の設定
 *
 */
extern void	x_config_int(InterruptNumberType intno,
						 AttributeType intatr, PriorityType intpri);

/*
 *  割込みハンドラの入り口で必要なIRC操作
 *
 *  AT91SAM7Sでは，必要な処理はない
 */
LOCAL_INLINE void
i_begin_int(InterruptNumberType intno)
{

}

/*
 *  割込みハンドラの出口で必要なIRC操作
 *
 *  AT91SAM7Sでは，必要な処理はない
 */
LOCAL_INLINE void
i_end_int(InterruptNumberType intno)
{

}

/*
 *  ターゲットシステム依存の初期化
 */
extern void target_initialize(void);

/*
 *  ターゲットシステムの終了
 *
 *  システムを終了する時に使う
 */
extern void target_exit(void) NoReturn;

/*
 *  割込みハンドラ（chip_support.S）
 */
extern void interrupt_handler(void);

/*
 *  未定義の割込みが入った場合の処理
 */
extern void default_int_handler(void);

#ifdef TOPPERS_ENABLE_GCOV_PART
/*
 *  GCOV一部取得用ライブラリ
 */
/*
 *  GCOV初期化関数
 */
extern void gcov_init(void);

/*
 *  GCOV中断関数
 */
extern void gcov_pause(void);

/*
 * GCOV再開関数
 */
extern void gcov_resume(void);

/*
 *  GCOV出力関数
 */
extern void gcov_dump(void);
#endif /* TOPPERS_ENABLE_GCOV_PART */

#endif /* TOPPERS_MACRO_ONLY */

/*
 *  コア依存モジュール（ARM用）
 */
#include "arm_gcc/common/core_config.h"

#endif /* TOPPERS_TARGET_CONFIG_H */
