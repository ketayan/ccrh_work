
		TOPPERS/ATK2-SC1
        ＜AT91SKYEYEターゲット依存部＞

このドキュメントはAT91SKYEYEターゲット依存部の情報を記述したものである．

----------------------------------------------------------------------
TOPPERS ATK2
    Toyohashi Open Platform for Embedded Real-Time Systems
    Automotive Kernel Version 2

Copyright (C) 2011-2015 by Center for Embedded Computing Systems
            Graduate School of Information Science, Nagoya Univ., JAPAN
Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
Copyright (C) 2011-2013 by Spansion LLC, USA
Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
Copyright (C) 2011-2015 by Witz Corporation
Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
Copyright (C) 2015 by SUZUKI MOTOR CORPORATION

上記著作権者は，以下の (1)〜(3)の条件を満たす場合に限り，本ドキュメ
ント（本ドキュメントを改変したものを含む．以下同じ）を使用・複製・改
変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
(1) 本ドキュメントを利用する場合には，上記の著作権表示，この利用条件
    および下記の無保証規定が，そのままの形でドキュメント中に含まれて
    いること．
(2) 本ドキュメントを改変する場合には，ドキュメントを改変した旨の記述
    を，改変後のドキュメント中に含めること．ただし，改変後のドキュメ
    ントが，TOPPERSプロジェクト指定の開発成果物である場合には，この限
    りではない．
(3) 本ドキュメントの利用により直接的または間接的に生じるいかなる損害
    からも，上記著作権者およびTOPPERSプロジェクトを免責すること．また，
    本ドキュメントのユーザまたはエンドユーザからのいかなる理由に基づ
    く請求からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

本ドキュメントは，AUTOSAR（AUTomotive Open System ARchitecture）仕様
に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するものではな
い．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利用する
者に対して，AUTOSARパートナーになることを求めている．

本ドキュメントは，無保証で提供されているものである．上記著作権者およ
びTOPPERSプロジェクトは，本ドキュメントに関して，特定の使用目的に対す
る適合性も含めて，いかなる保証も行わない．また，本ドキュメントの利用
により直接的または間接的に生じたいかなる損害に関しても，その責任を負
わない．

$Id: target_user.txt 425 2015-12-07 08:06:19Z witz-itoyo $
----------------------------------------------------------------------

○概要

AT91SKYEYEターゲット依存部は，オープンソースのプロセッサシミュレータで
ある，Skyeye上の ARM7TDMI をベースとした AT91 システムをサポートしてい
る．

○SkyEye

本カーネルを動作させるためには，TOPPERSプロジェクトから公開されている，
SkyEye -V1.2 DeviceManager Extension 2.2.0 以降 が必要となる．

○カーネルの使用リソース

カーネルは以下のリソースを使用する．

  ・RAM
     コードとデータを配置する．
     使用量はオブジェクト数に依存する．
  
  ・TC1
     カーネル内部のティックの生成に用いる．
     
  ・USART0
     コンソールの出力に使用．


○実行環境

動作確認を行った実行環境は，Widnows 7 上の Cygwin である．

○デバッグ環境

デバッグ環境としては，skyeyeのサポートする方法を用いることができる．
arm-elf-gdb によるデバッグの動作確認を行った．

○コンパイラ

動作確認した GCC は，以下のサイトからWindows版のバイナリをダウンロード
して使用した以下のバージョンである．

  http://www.codesourcery.com/
   4.4.1 (Sourcery G++ Lite 2010q1-188)
   4.5.1 (Sourcery G++ Lite 2010.09-51)

上記コンパイラをインストールする際には，空白が含まれるディレクトリにイ
ンストールしないこと（インストーラが作成するインストールディレクトリ自
体に空白が含まれるため注意すること）．

○カーネル終了時の処理

ShutdownOS が呼び出されカーネル終了時には，at91skyeye.h にある 
at91skyeye_exit() が実行される．ディフォルトでは，特に処理は記述されて
いない．独自の処理処理を追加したい場合は，at91skyeye_exit() の内容を書
き換えること．

○Makefileによる実行

実行やデバッグは，make により実行できる．用意されているコマンドは次の
通りである．

 ・run
   ・実行．
   ・skyeyeを起動してプログラムを実行する．
 ・db
   ・デバッグ
   ・skyeyeとgdbを起動して，プログラムをロードする．

○ ハードウェアカウンタ
・SkyEyeで使用できるカウンタ(コンフィギュレーション時指定できるカウンタ
  名と割込み番号)情報
	カウンタ名：MAIN_HW_COUNTER
		割込み番号：IRQ_TC1    （5）
		差分タイマ：サイクルカウンタを使用(CYCLE_COUNTER_REG)
		現在値タイマ：TIMER_1_xxx
		1ティック当たりの秒数：1/2048 (※)
		※シミュレータのため実時間ではない

○ gcovによるカバレッジ取得

gcovによるカバレッジを取得したい場合には，Makefileで ENABLE_GCOV を以
下の値のいずれかに定義すること．

    full              : 全体取得
    part              : 一部取得
    only_kernel_full  : 全体取得(カーネルコードのみ)
    only_kernel_part  : 一部取得(カーネルコードのみ)

full と only_kernel_full は，プログラム実行全体のカバレッジを取得する． 
only_kernel_full はカーネルコードのみのカバレッジを取得する．これらの
カバレッジ結果は，ext_ker()を呼び出してカーネル終了することにより，
*.gcda に出力される．

part と only_kernel_part 指定した区間（時間）のカバレッジを取得する． 
only_kernel_part はカーネルコードのみのカバレッジを取得する．区間（時
間）を指定やカバレッジの出力のために以下の関数を提供する．

void gcov_init(void);
 GCOV初期化関数．呼び出した時点以降のカバレッジを取得する．

void gcov_pause(void);
 GCOV中断関数．カバレッジの取得を中断する．
 
void gcov_resume(void);
GCOV再開関数．カバレッジの取得を再開する．

void gcov_dump(void);
 GCOV出力関数．カバレッジを.gcda に出力する．

カーネル実行後にカーネルがあるディレクトリで以下のコマンドを実行すると，
カバレッジの結果が xxx.c.gcov に出力される．

 $arm-none-eabi-gcov.exe *.gcda

カバレッジの結果をhtml化するlcovを使うことも可能である．

 http://ltp.sourceforge.net/coverage/lcov.php
 
動作確認を行ったバージョンは 1.9 であるが，日本語を出力するためと，
Windwosネイティブアプリである Sourcery G++ を Cygwin環境上で使用してい
る場合には，以下のパッチが必要である．

--------------------------------------------------------------------------
--- lcov-1.9_org/bin/genhtml	2010-08-06 20:05:17.000000000 +0900
+++ lcov-1.9/bin/genhtml	2010-11-24 11:29:58.075739700 +0900
@@ -4007,10 +4007,10 @@
 	write_html($_[0], <<END_OF_HTML)
 	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
 
-	<html lang="en">
+	<html lang="ja">
 
 	<head>
-	  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
+	  <meta http-equiv="Content-Type" content="text/html; charset=EUC-JP">
 	  <title>$_[3]</title>
 	  <link rel="stylesheet" type="text/css" href="$_[1]gcov.css">
 	</head>
@@ -4069,11 +4069,11 @@
 	write_html($_[0], <<END_OF_HTML)
 	<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 
-	<html lang="en">
+	<html lang="ja">
 
 	<head>
 	  <title>$_[3]</title>
-	  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
+	  <meta http-equiv="Content-Type" content="text/html; charset=ECU-JP">
 	  <link rel="stylesheet" type="text/css" href="$_[1]gcov.css">
 	</head>
 
@@ -5578,10 +5578,10 @@
 		$result = <<END_OF_HTML
 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 
-<html lang="en">
+<html lang="ja">
 
 <head>
-  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
+  <meta http-equiv="Content-Type" content="text/html; charset=ECU-JP">
   <title>\@pagetitle\@</title>
   <link rel="stylesheet" type="text/css" href="\@basedir\@gcov.css">
 </head>
diff -urN lcov-1.9_org/bin/geninfo lcov-1.9/bin/geninfo
--- lcov-1.9_org/bin/geninfo	2010-08-06 20:05:17.000000000 +0900
+++ lcov-1.9/bin/geninfo	2010-11-24 11:31:26.481094900 +0900
@@ -862,7 +862,7 @@
 
 	# Execute gcov command and suppress standard output
 	$gcov_error = system_no_output(1, $gcov_tool, $da_filename,
-				       "-o", $object_dir, @gcov_options);
+				       @gcov_options);
 
 	if ($da_renamed)
 	{
@@ -929,7 +929,7 @@
 		my $num;
 
 		($source, $object) = read_gcov_header($gcov_file);
-
+		$source =~ s/(.*)\r$/$1/;
 		if (defined($source))
 		{
 			$source = solve_relative_path($base_dir, $source);
--------------------------------------------------------------------------

カバレッジの取得後，以下のコマンドを実行すると，coverage_html 以下にカ
バレッジ結果のhtmlが生成される．

   $make lcov

○各種設定の変更

幾つかのパラメータは変更可能になっている．設定ファイル毎に設定可能項目
は次のようになっている．

●Makefile.target の設定項目

・GCC_TARGET
  GCCの suffix を定義

・INCLUDES
  インクルード指定

・COPTS
  Cコンパイラへのオプション

・LDFLAGS
  リンカへのオプション

○その他注意点

テストなどの目的で，OS終了後StartOS()の呼出し元（main関数）に戻りたい
場合，ENABLE_RETURN_MAINマクロを定義することで，実現できる．現状の実装
では，カーネルテスト時に1つのバイナリで複数回OSを起動する機能をOSで実
現したい要望を満たしている．

本カーネルが対応しているSkyeyeとは異なるバージョンのSkyeyeを用いた
場合，version_check()関数内で無限ループとなり，プログラムが停止する．
このとき，カーネルはエラーログの出力を試行するが，usart0_initialize関数が
呼ばれるまでは，このエラーログは出力されない．
よって，異なるバージョンかどうかのエラーログをカーネルに出力させたい場合，
usart0_initialize関数を，version_check関数を呼び出すよりも以前に呼び出す
必要がある．


○変更履歴

2015/03/30
・トレースログを有効化する設定をターゲット依存部のMakefileから
　削除
・sample1のために，target_test.hで，sample1で使用するハードウェア
　カウンタから約1秒でアラームを起動するために，約1秒に相当する
　ハードウェアカウンタのティック値をCOUNTER_MIN_CYCLEマクロに定義

2014/09/30
・usart0_initializeをtarget_hardware_initializeから，InitHwSerialに
　移動

2014/01/23
・target_hw_counter.c|h|cfg|arxmlへハードウェアカウンタに関する実装を追加

2014/01/10
・静的APIファイルを削除

2013/10/11
・target_support.S
  ・callevel_statへの書込み操作命令が間違っていたため，修正

・target_config.c
  ・OS起動前に参照される変数の初期化対応

2013/06/28
・target_support.S
  ・割込みのエントリでの多重割込み時のスタックのチェック方法を スタック
    残量チェック方式 に変更．

・target_config.c
  ・target_exit() で target_timer_terminate() を呼び出さないよう変更．

2013/03/29
	一般向けのリリース
  
以上．
