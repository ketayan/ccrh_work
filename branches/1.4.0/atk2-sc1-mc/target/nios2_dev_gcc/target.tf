$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2011-2015 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
$  Copyright (C) 2011-2013 by Spansion LLC, USA
$  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2011-2015 by Witz Corporation
$  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
$  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
$  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
$  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
$
$  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: target.tf 425 2015-12-07 08:06:19Z witz-itoyo $
$

$
$     パス2のターゲット依存テンプレート（NIOS2_DEV用）
$

$ 
$  有効な割込み番号の定義
$ 
$INTNO_VALID = { 
	0x10000,0x10001,...,0x1001f;
	0x20000,0x20001,...,0x2001f;
	0x30000,0x30001,...,0x3001f;
	0x40000,0x40001,...,0x4001f
}$

$ 
$  制御可能な割込み番号
$ 
$INTNO_CONTROLLABLE  = {
	0x10000,0x10001,...,0x1001f;
	0x20000,0x20001,...,0x2001f;
	0x30000,0x30001,...,0x3001f;
	0x40000,0x40001,...,0x4001f
}$

$ 
$  コア間割込み番号の定義
$ 
$	//コア0,コア1,コア2,コア3
$INTNO_ICI_LIST = {0x10002,0x20002,0x30002,0x40002}$

$ 
$  コア間割込み優先度の定義
$ 
$	//コア0,コア1,コア2,コア3
$INTPRI_ICI_LIST = {INTPRI_ICI0,INTPRI_ICI1,INTPRI_ICI2,INTPRI_ICI3}$

$ 
$ 有効な割込み番号の最大値
$ 
$TMAX_INTNO = 0x1f$


$ 
$ コア0に関する指定
$ 

$  オブジェクトロック
$ P_LOCKの場合に使用するオブジェクトロックを持つコアのIDを指定
$CORE_OBJ_LOCK[0] = 0$

$  CCBのセクション指定
$CORE_SECTION_CCB[0]  = "__attribute__((section(\"._kernel_core0s_ccb\"), nocommon))"$

$  非タスクコンテキスト用のスタックのセクション指定
$CORE_SECTION_OSTK[0] = "__attribute__((section(\"._kernel_core0p_osstack\"), nocommon))"$

$  タスクスタックのセクション指定
$CORE_SECTION_TSKSTK[0] = "__attribute__((section(\"._kernel_core0p_tstack\"), nocommon))"$

$  タスクコントロールブロックのセクション指定
$CORE_SECTION_TSKCB[0] = "__attribute__((section(\"._kernel_core0s_tskcb\"), nocommon))"$

$  オブジェクトコントロールブロックのセクション指定
$CORE_SECTION_OBJCB[0] = "__attribute__((section(\"._kernel_core0s_objcb\"), nocommon))"$


$ 
$ コア1に関する指定
$ 
$CORE_OBJ_LOCK[1] = 1$

$CORE_SECTION_CCB[1]  = "__attribute__((section(\"._kernel_core1s_ccb\"), nocommon))"$
$CORE_SECTION_OSTK[1] = "__attribute__((section(\"._kernel_core1p_osstack\"), nocommon))"$
$CORE_SECTION_TSKSTK[1] = "__attribute__((section(\"._kernel_core1p_tstack\"), nocommon))"$
$CORE_SECTION_TSKCB[1] = "__attribute__((section(\"._kernel_core1s_tskcb\"), nocommon))"$
$CORE_SECTION_OBJCB[1] = "__attribute__((section(\"._kernel_core1s_objcb\"), nocommon))"$

$ 
$ コア2に関する指定
$ 
$CORE_OBJ_LOCK[2] = 2$

$CORE_SECTION_CCB[2]  = "__attribute__((section(\"._kernel_core2s_ccb\"), nocommon))"$
$CORE_SECTION_OSTK[2] = "__attribute__((section(\"._kernel_core2p_osstack\"), nocommon))"$
$CORE_SECTION_TSKSTK[2] = "__attribute__((section(\"._kernel_core2p_tstack\"), nocommon))"$
$CORE_SECTION_TSKCB[2] = "__attribute__((section(\"._kernel_core2s_tskcb\"), nocommon))"$
$CORE_SECTION_OBJCB[2] = "__attribute__((section(\"._kernel_core2s_objcb\"), nocommon))"$

$ 
$ コア3に関する指定
$ 
$CORE_OBJ_LOCK[3] = 3$

$CORE_SECTION_CCB[3]  = "__attribute__((section(\"._kernel_core3s_ccb\"), nocommon))"$
$CORE_SECTION_OSTK[3] = "__attribute__((section(\"._kernel_core3p_osstack\"), nocommon))"$
$CORE_SECTION_TSKSTK[3] = "__attribute__((section(\"._kernel_core3p_tstack\"), nocommon))"$
$CORE_SECTION_TSKCB[3] = "__attribute__((section(\"._kernel_core3s_tskcb\"), nocommon))"$
$CORE_SECTION_OBJCB[3] = "__attribute__((section(\"._kernel_core3s_objcb\"), nocommon))"$


$ 
$  オブジェクト関連のメモリの配置先指定
$ 

$ 
$  CCBの配置先指定
$ 
$FUNCTION GENERATE_CCB$
	CCB _kernel_core$+ARGV[1]$_ccb $CORE_SECTION_CCB[ARGV[1]]$ = {0, FALSE, 0, 0, 0, 0, 0};$NL$
$END$

$ 
$  OSSTACKの配置先指定
$ 
$FUNCTION GENERATE_OSSTACK$
	static StackType _kernel_core$+ARGV[1]$_ostack[$FORMAT("COUNT_STK_T(0x%xU)", +ARGV[2])$] $CORE_SECTION_OSTK[ARGV[1]]$;$NL$
$END$

$ 
$  OSSTACKの配置先指定
$ 
$FUNCTION GENERATE_OSSTACK_DEF$
	static StackType _kernel_core$+ARGV[1]$_ostack[COUNT_STK_T($OSTK.STKSZ[OSTK.COREID[ARGV[1]]]$)] $CORE_SECTION_OSTK[ARGV[1]]$;$NL$
$END$

$ 
$  タスクスタックの配置先指定
$ 
$FUNCTION GENERATE_TSKSTK$
	static StackType _kernel_stack_$ARGV[1]$[COUNT_STK_T($TSK.STKSZ[ARGV[1]]$)] $CORE_SECTION_TSKSTK[OSAP.CORE[TSK.OSAPID[ARGV[1]]]]$;$NL$
$END$

$ 
$  基本タスクの共有スタックの配置先指定
$ 
$FUNCTION GENERATE_SHARED_TSKSTK$
	static StackType _kernel_shared_stack_$ARGV[2]$[COUNT_STK_T($ARGV[3]$)] $CORE_SECTION_TSKSTK[ARGV[1]]$;$NL$
$END$

$ 
$  TCBの配置先指定
$ 
$FUNCTION GENERATE_TCB$
 	TCB _kernel_tcb_$ARGV[1]$$TAB$$CORE_SECTION_TSKCB[OSAP.CORE[TSK.OSAPID[ARGV[1]]]]$;$NL$
$END$

$ 
$  CNTCBの配置先指定
$ 
$FUNCTION GENERATE_CNTCB$
	CNTCB _kernel_cntcb_$ARGV[1]$$TAB$$CORE_SECTION_OBJCB[OSAP.CORE[CNT.OSAPID[ARGV[1]]]]$;$NL$
$END$

$ 
$  ALMCBの配置先指定
$ 
$FUNCTION GENERATE_ALMCB$
	ALMCB _kernel_almcb_$ARGV[1]$$TAB$$CORE_SECTION_OBJCB[OSAP.CORE[ALM.OSAPID[ARGV[1]]]]$;$NL$
$END$

$ 
$  SCHTBLCBの配置先指定
$ 
$FUNCTION GENERATE_SCHTBLCB$
	SCHTBLCB _kernel_schtblcb_$ARGV[1]$$TAB$$CORE_SECTION_OBJCB[OSAP.CORE[SCHTBL.OSAPID[ARGV[1]]]]$;$NL$
$END$

$ 
$  RESCBの配置先指定
$ 
$FUNCTION GENERATE_RESCB$
	RESCB _kernel_rescb_$ARGV[1]$$TAB$$CORE_SECTION_OBJCB[RES.CORE[ARGV[1]]]$;$NL$
$END$

$ 
$  ISRCBの配置先指定
$ 
$FUNCTION GENERATE_ISRCB$
	ISRCB _kernel_isrcb_$ARGV[1]$$TAB$$CORE_SECTION_OBJCB[OSAP.CORE[ISR.OSAPID[ARGV[1]]]]$;$NL$
$END$


$
$  プロセッサ依存テンプレートのインクルード
$
$INCLUDE "nios2_gcc/prc.tf"$
