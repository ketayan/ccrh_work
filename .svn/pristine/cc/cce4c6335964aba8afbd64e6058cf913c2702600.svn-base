
		TOPPERS/ATK2-SC1
        ＜RC-Z-BASE-V850ターゲット依存部(GCC版)マニュアル＞

このドキュメントはRC-Z-BASE-V850ターゲット依存部の情報を記述したものである．              

----------------------------------------------------------------------
TOPPERS ATK2
    Toyohashi Open Platform for Embedded Real-Time Systems
    Automotive Kernel Version 2

Copyright (C) 2013-2015 by Center for Embedded Computing Systems
            Graduate School of Information Science, Nagoya Univ., JAPAN
Copyright (C) 2013-2014 by FUJI SOFT INCORPORATED, JAPAN
Copyright (C) 2013-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
Copyright (C) 2013-2014 by Renesas Electronics Corporation, JAPAN
Copyright (C) 2013-2014 by Sunny Giken Inc., JAPAN
Copyright (C) 2013-2014 by TOSHIBA CORPORATION, JAPAN
Copyright (C) 2013-2014 by Witz Corporation, JAPAN

上記著作権者は，以下の (1)〜(3)の条件を満たす場合に限り，本ドキュメ
ント（本ドキュメントを改変したものを含む．以下同じ）を使用・複製・改
変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
(1) 本ドキュメントを利用する場合には，上記の著作権表示，この利用条件
    および下記の無保証規定が，そのままの形でドキュメント中に含まれて
    いること．
(2) 本ドキュメントを改変する場合には，ドキュメントを改変した旨の記述
    を，改変後のドキュメント中に含めること．ただし，改変後のドキュメ
    ントが，TOPPERSプロジェクト指定の開発成果物である場合には，この限
    りではない．
(3) 本ドキュメントの利用により直接的または間接的に生じるいかなる損害
    からも，上記著作権者およびTOPPERSプロジェクトを免責すること．また，
    本ドキュメントのユーザまたはエンドユーザからのいかなる理由に基づ
    く請求からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

本ドキュメントは，AUTOSAR（AUTomotive Open System ARchitecture）仕様
に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するものではな
い．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利用する
者に対して，AUTOSARパートナーになることを求めている．

本ドキュメントは，無保証で提供されているものである．上記著作権者およ
びTOPPERSプロジェクトは，本ドキュメントに関して，特定の使用目的に対す
る適合性も含めて，いかなる保証も行わない．また，本ドキュメントの利用
により直接的または間接的に生じたいかなる損害に関しても，その責任を負
わない．

$Id$
----------------------------------------------------------------------

○概要

RC-Z-BASE-V850ターゲット依存部（RC-Z-BASE-V850依存部）は，ルネサスエレ
クトロニクス社のV850E2/FG4を搭載した株式会社ゼットエムピー
RC-Z-BASE-V850をサポートしている．

○動作確認コンパイラバージョン

動作確認を行ったコンパイラのバージョンはプロセッサ依存部のユーザーマニ
ュアルに記載している．

○サポートマイコン

FG4/FG4-Lはピン数やROM/RAMサイズの違いで複数存在するが，現状サポートし
ているものは次の通りである．

・FG4   : uPD70F3550

プロセッサの指定は，Makefile.targetにある以下のいずれか行を有効にする
こと指定する．

MCU = f3550#uPD70F3550 V8502E/FG4

○ログ出力

ログ出力のためのUARTEはポート10を使用する．ポートは基板上のUSB-UARTに
接続されている．

データフォーマットは，

・115200bps、データ8bit、パリティなし、ストップ1bit、フロー制御なし

である．


○サンプルプログラムで用いるタイマ

サンプルプログラムでは，TAUB0を用いる．


○ビルド環境

ビルド環境はWindows上のCygwinを使用する．Cygwinには最低限以下のパッケ
ージをインストールする必要がある．

 ・make
 ・perl
 
 
○デバッグ環境

動作確認はCS+で実施した．CS+の設定方法は次の通りである．

 1) CubeSuite+を起動
 2) 新規プロジェクトを作成．
    メニューから[ファイル] → [新規作成] → [新しいプロジェクトを作成]を選択
 3) プロジェクトの作成
  プロジェクト作成ダイアログ内で以下の設定をします。
   1. マイクロコントローラ        ：v850を選択
   2. 使用するマイクロコントローラ：使用するものを選択
   3. プロジェクトの種類          ：デバック専用
   4. プロジェクト名              ：任意
   5. 作成場所                    ：任意
   6. OKを選択
   上記5で指定したフォルダ内にプロジェクト名.mtpjファイルが作成される．
 4) デバックツールの選択
   1. 画面左側ツリー内のV850E2シュミレータを選択し右クリック
   2. 使用するデバックツールを選択(E1の場合はE1を選択)
   3. メインクロック周波数 10MHz
 5) ダウンロードファイルの設定
   1. 画面右側のタブから[ダウンロード・ファイル設定]を選択
   2. 画面右側のダウンロードするファイルを選択
   3. 選択行右端[...]を左クリック
   4. ダウンロード・ダイアログ内で[追加]ボタンを選択
   5. ダウンロード・ファイル情報のファイルを選択後[...]を左クリック
   6. ダウンロードファイルは[atk-sc1.exe]を選択
   7. OKを選択
 6) ファイルをダウンロード
   1. ターゲットボードの電源をON
   2. メニューから[デバック]→[デバック・ツールへダウンロード]を選択
      ※ダウンロード完了後、「ダウンロードが完了しましたが、以下の問題ある可能性があります。」と
      警告メッセージが表示されるが，OKを選択．
 7) プログラムの実行
   1. メニューから[デバック]→[リスタート]を選択
      ターミナル画面に[Input Command:]と表示が出れば正常に動作している ．
 上記手順の 2)〜5)は新規にプロジェクトを作成した時のみ実行．


○制限事項

●ソースコードデバッグ

CubeSuite+ では，GCCの出力したシンボルは解釈するが，ソースコードレベル
デバッグは出来ない．

●LMAの問題

CubeSuite+ では，GCCのLMAを理解できない．そのため，コンパイルオプショ
ンに -DOMIT_DATA_INIT を指定してる．ROMブートする場合や，CubeSuite+以
外のデバッガを使用する場合は，この定義を外すこと．

○ウオッチドックタイマの自動スタートの無効化

チップ出荷時は，電源投入後にWDTを自動実行する設定となっている．ATK2で
はWDTは停止させないので，アプリケーションで初期化・停止させない場合は，
フラッシュ・マスク・オプションで自動スタートを無効にすること．

OPBT0 - フラッシュ・マスク・オプションのレジスタ0
 アドレス 0xFF47000C
 OPBT0の値によっては起動時からWDTが有効となる．
 24bit/20bitを0とすることで自動スタートを無効に出来る．
 なお，E1等でJTAGデバッグをする場合は，31bitは'1'にする必要がある．

OPBT0の変更方法
CubeSuite+による方法
・デバッガでターゲットに接続
・デバッグ・ツールのプロパティフラッシュ・オプションタブに表示される
「...」ボタンをクリックしてメニューを呼び出して変更．

Renesas Flash Programmer(RFC)による方法(E1使用)
・ターゲット
 ・通信方式
   使用ツール : E1
   接続方式   : SIO-ch0
 ・クロック供給
   通信速度生成
    接続方式   : SIO-ch0
    通信速度   : 2MHz
   クロック設定 
     周波数 20Mhz
     逓倍 - 4.00倍（）
     
・マイクロコントローラ
  ・フラッシュオプションの取得
     ・スタート
     
・マイクロコントローラ  
  ・プロジェクトの設定
    ->その他の設定
      ->OPBT0 の値を変更．

・マイクロコントローラ
  ・オプションバイト設定
   ・スタート
  
○変更履歴
2015/04/07
・taget_test.h
  COUNTER_MIN_CYCLEの定義を追加．
・Makefile.target
  トレースログの設定を削除．  

2014/12/23
・コンパイラのバージョンアップに伴い，コンパイルオプションに 
  -msoft-float を指定するよう変更．

2014/07/11
・ターゲット依存部の名称を v850e2_xxx から v850_xxx に変更

2014/01/10
・静的APIファイルを削除

2013/10/11
・target_config.c
  ・OS起動前に参照される変数の初期化対応

2013/08/31
 新規作成
 
以上 
