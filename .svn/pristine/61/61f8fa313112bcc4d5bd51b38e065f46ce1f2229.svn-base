/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2007 by GJ Business Division RICOH COMPANY,LTD. JAPAN
 *  Copyright (C) 2005-2017 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2017 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id$
 */

/*
 *		シリアルI/Oデバイス（SIO）ドライバ（AT91SKYEYE用）
 *  USART0を使用
 */

#include "Os.h"
#include "t_syslog.h"
#include "at91skyeye.h"
#include "target_serial.h"

/*
 * 受信用バッファ
 */
static uint8 usart0_rx_buf;

/*
 * 低レベル用シリアルドライバ初期化
 */
void
usart0_initialize(void)
{
	/* 受信データの格納先アドレスの設定 */
	sil_wrw_mem((uint32 *) (USART0_RPR), (uint32) (&usart0_rx_buf));
	sil_wrw_mem((uint32 *) (USART0_RCR), 0x0001);
}

/*
 * 文字を受信したかチェック
 */
LOCAL_INLINE boolean
usart0_getready(void)
{
	return(sil_rew_mem((uint32 *) (USART0_RCR)) == 0);
}

/*
 *  受信した文字の取り出し
 */
LOCAL_INLINE uint8
usart0_getchar(void)
{
	char8 c;

	c = usart0_rx_buf;
	sil_wrw_mem((uint32 *) (USART0_RPR), (uint32) (&usart0_rx_buf));
	sil_wrw_mem((uint32 *) (USART0_RCR), 0x0001);
	return(c);
}

/*
 *  初期化処理
 */
void
InitHwSerial(void)
{
	/*
	 *  target_fput_logが使えるようにUSART0を初期化
	 */
	usart0_initialize();
}

/*
 *  終了処理
 */
void
TermHwSerial(void)
{
}

/*
 *  受信コールバックハンドラ
 */
ISR(RxHwSerialInt){
	if (usart0_getready()) {
		RxSerialInt(usart0_getchar());
	}
}
