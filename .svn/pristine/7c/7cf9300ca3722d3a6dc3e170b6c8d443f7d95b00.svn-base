#
#  TOPPERS ATK2
#      Toyohashi Open Platform for Embedded Real-Time Systems
#      Automotive Kernel Version 2
#
#  Copyright (C) 2018 by FUJI SOFT INCORPORATED, JAPAN
#
#  上記著作権者は，以下の(1)?(4)の条件を満たす場合に限り，本ソフトウェ
#  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
#  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
#  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
#      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
#      スコード中に含まれていること．
#  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
#      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
#      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
#      の無保証規定を掲載すること．
#  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
#      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
#      と．
#    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
#        作権表示，この利用条件および下記の無保証規定を掲載すること．
#    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
#        報告すること．
#  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
#      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
#      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
#      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
#      免責すること．
#
#  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
#  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
#  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
#  用する者に対して，AUTOSARパートナーになることを求めている．
#
#  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
#  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
#  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
#  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
#  の責任を負わない．
#
#  $Id$
#

ROOT = ../..

CFG = $(ROOT)/cfg/cfg/cfg.exe
CC = ccrh.exe
LINK = rlink.exe

OBJNAME = atk2-sc3
CFG1_OUT = cfg1_out
CFG2_OUT = cfg2_out
CFG3_OUT = cfg3_out
CFG4_IN = cfg_pass4
CFG2_OUT_SRCS = Os_Lcfg.h Os_Lcfg.c Os_Cfg.h kernel_mem2.c Ioc.c Ioc.h

INCLUDE_DIR := . $(ROOT)/ $(ROOT)/arch $(ROOT)/include $(ROOT)/kernel $(ROOT)/sysmod $(ROOT)/library \
				$(ROOT)/target/hsbrh850f1l_ccrh \
				$(ROOT)/arch/v850_ccrh \
				$(ROOT)/arch/ccrh \
				$(ROOT)/target/hsbrh850f1l_gcc \
				$(ROOT)/arch/v850_gcc

INCLUDES := $(foreach dir,$(INCLUDE_DIR),-I$(dir))

ARXML := sample1.arxml \
		$(ROOT)/target/hsbrh850f1l_gcc/target_hw_counter.arxml \
		$(ROOT)/target/hsbrh850f1l_gcc/target_serial.arxml \
		$(ROOT)/arch/v850_gcc/uart.arxml \
		$(ROOT)/target/hsbrh850f1l_gcc/target_mem.arxml \
		$(ROOT)/arch/v850_ccrh/prc_mem.arxml

COPTS := -g -g_line -Xcommon=rh850 -Xcpu=g3k -Xreserve_r2 -Ospeed -Oinline_size \
		-Xno_warning=20177 -Xno_warning=50013 \
		-DALLFUNC,TOPPERS_LABEL_ASM,USE_CCRH,MPU_REGION_NUM=4,USE_KERNEL_LIBRARY_SYSLOG \
		$(INCLUDES)

LOPTS := -nocompress -padding -nologo -memory=High

CFGOPTS := --kernel atk2 \
		--api-table $(ROOT)/kernel/kernel.csv \
		--cfg1-def-table $(ROOT)/kernel/kernel_def.csv \
		--cfg1-def-table $(ROOT)/arch/v850_ccrh/prc_def.csv \
		--cfg1-def-table $(ROOT)/arch/v850_gcc/prc_def.csv \
		--ini-file $(ROOT)/kernel/kernel.ini \
		$(INCLUDES)

KERNEL_FCSRCS = counter_manage.c counter.c alarm.c osctl_manage.c osctl.c task_manage.c \
				event.c resource.c interrupt_manage.c interrupt.c task.c scheduletable.c \
				osap.c memory.c ioc_manage.c svc_table.c \
				prc_config.c prc_tool.c prc_mpu.c target_config.c tauj_hw_counter.c uart_rlin.c rh850_f1l.c \
				sample1.c sample2.c banner.c serial.c syslog.c log_output.c strerror.c t_perror.c vasyslog.c \
				Os_Lcfg.c ioc.c 

KERNEL_FASMSRCS = prc_support.asm prc_tool_asm.asm sample_asm.asm start.asm Os_Lcfg_asm.asm

KERNEL_COBJS := $(foreach file,$(KERNEL_FCSRCS),$(file:.c=.obj))
KERNEL_ASMOBJS := $(foreach file,$(KERNEL_FASMSRCS),$(file:.asm=.obj))

vpath %.c $(INCLUDE_DIR)
vpath %.asm $(INCLUDE_DIR)
vpath %.arxml $(INCLUDE_DIR)

$(OBJNAME).abs: kernel_mem.obj
	sh ldscript.sh "$(LINK) -output=$(OBJNAME).abs -list=$(OBJNAME).map -show=symbol,reference $(LOPTS) $(KERNEL_COBJS) $(KERNEL_ASMOBJS) kernel_mem.obj"

kernel_mem.obj: kernel_mem.c
	$(CC) -c $< $(COPTS)

kernel_mem.c: $(CFG4_IN).srec $(CFG4_IN).syms $(CFG3_OUT).tf
	$(CFG) --pass 4 \
	-T $(ROOT)/target/hsbrh850f1l_ccrh/target_mem.tf \
	--rom-image $(CFG4_IN).srec --symbol-table $(CFG4_IN).syms \
	$(CFGOPTS) $(ARXML)

$(CFG4_IN).syms: $(CFG4_IN).map
	$(ROOT)/arch/ccrh/conv_syms.rb $< $@

$(CFG4_IN).srec $(CFG4_IN).map: offset.h kernel_mem2.obj $(KERNEL_COBJS) $(KERNEL_ASMOBJS) ldscript.sh
	sh ldscript.sh "$(LINK) -output=$(CFG4_IN).srec -form=stype -list=$(CFG4_IN).map -show=symbol,reference $(LOPTS) $(KERNEL_COBJS) $(KERNEL_ASMOBJS) kernel_mem2.obj"

kernel_mem2.obj: kernel_mem2.c
	$(CC) -c $< $(COPTS)

offset.h: $(CFG2_OUT_SRCS)
	$(CFG) --pass 3 \
	-T $(ROOT)/target/hsbrh850f1l_ccrh/target_offset.tf \
	--rom-image $(CFG1_OUT).srec --symbol-table $(CFG1_OUT).syms \
	$(CFGOPTS) $(ARXML)

$(CFG2_OUT_SRCS) $(CFG2_OUT).tf $(CFG3_OUT).tf ldscript.sh: Os_Lcfg.timestamp
Os_Lcfg.timestamp: $(CFG1_OUT).srec $(CFG1_OUT).syms $(ARXML)
	$(CFG) --pass 2 -T $(ROOT)/target/hsbrh850f1l_ccrh/target.tf $(CFGOPTS) $(ARXML)
	if ! cmp Os_Cfg.h Os_Cfg_tmp.h >/dev/null 2>&1 ; then \
		mv Os_Cfg_tmp.h Os_Cfg.h ;\
	else \
		rm Os_Cfg_tmp.h ;\
	fi
	touch -r Os_Lcfg.c Os_Lcfg.timestamp
	sed -i "s/\r\n/\n/g" ldscript.sh
	cp -pf $(CFG2_OUT).tf $(CFG3_OUT).tf

$(CFG1_OUT).syms: $(CFG1_OUT).map
	$(ROOT)/arch/ccrh/conv_syms.rb $< $@

$(CFG1_OUT).srec $(CFG1_OUT).map: $(CFG1_OUT).obj
	$(LINK) -output=$(CFG1_OUT).srec -form=stype -list=$(CFG1_OUT).map -show=symbol,reference $(LOPTS) $<

$(CFG1_OUT).obj: $(CFG1_OUT).c
	$(CC) -c $< $(COPTS)

$(CFG1_OUT).c: $(ARXML)
	$(CFG) --pass 1 $(CFGOPTS) $?


$(KERNEL_COBJS): %.obj: %.c offset.h
	$(CC) -c $(COPTS) $<

$(KERNEL_ASMOBJS): %.obj: %.asm offset.h
	$(CC) -c $(COPTS) $<


.PHONY: clean
clean:
	rm -f $(OBJNAME)* $(CFG1_OUT)* $(CFG2_OUT)* $(CFG3_OUT)* $(CFG4_IN)* *Os_* Ioc* offset.h kernel_mem* *.obj asm_config.inc ldscript.* *.mtud
