/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2017 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2017 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2017 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id$
 */

/*
 *		カーネル対応のスタートアップモジュール（Nios2用）
 */

#define OMIT_INCLUDE_OS_CFG		/* Os_Cfg.hインクルード抑止 */
#define TOPPERS_MACRO_ONLY
#define UINT_C(val)			(val)		/* 符号無し整数型の定数を作るマクロ */
#define CAST(type, val)		(val)		/* 型キャストを行うマクロ */
#include "kernel_impl.h"
#include "prc_asm.inc"

	 .section .entry, "xa"
	 .global __reset
	 .balign  FUNCTION_ALIGN_SIZE
__reset:
#if NIOS2_ICACHE_SIZE > 0
	/*
	 * The assumption here is that the instruction cache size is always
	 * a power of two.
	 */

#if NIOS2_ICACHE_SIZE > 0x8000
	movhi r2, %hi(NIOS2_ICACHE_SIZE)
#else
	movui r2, NIOS2_ICACHE_SIZE
#endif
0:
	initi r2
	addi r2, r2, -NIOS2_ICACHE_LINE_SIZE
	bgt r2, zero, 0b
1:

	.pushsection .debug_alt_sim_info
	.int 1, 1, 0b, 1b
	.popsection
#endif /* NIOS2_ICACHE_SIZE > 0 */

#ifndef BOOT_ROM
	movia r3, start
	jmp r3
#else
	br _boot_rom
#endif  /* BOOT_ROM */


#ifdef BOOT_ROM
	.section .text
	.global _boot_rom
	.balign  FUNCTION_ALIGN_SIZE
	.type _boot_rom, @function
_boot_rom:
#if NIOS2_DCACHE_SIZE > 0
#if NIOS2_DCACHE_SIZE > 0x8000
	movhi r2, %hi(NIOS2_DCACHE_SIZE)
#else
	movui r2, NIOS2_DCACHE_SIZE
#endif
0:
	initd 0(r2)
	addi r2, r2, -NIOS2_DCACHE_LINE_SIZE
	bgt r2, zero, 0b
1:
	.pushsection .debug_alt_sim_info
	.int 2, 1, 0b, 1b
	.popsection
#endif /* NIOS2_DCACHE_SIZE > 0 */

	movia r2, start
	jmp   r2
	nop

#endif /* BOOT_ROM */

	.section .text
	.global start
	.balign  FUNCTION_ALIGN_SIZE
	.type start, @function
start:
#if NIOS2_DCACHE_SIZE > 0
#if NIOS2_DCACHE_SIZE > 0x8000
	movhi r2, %hi(NIOS2_DCACHE_SIZE)
#else
	movui r2, NIOS2_DCACHE_SIZE
#endif
0:
	initd 0(r2)
	addi r2, r2, -NIOS2_DCACHE_LINE_SIZE
	bgt r2, zero, 0b
1:
	.pushsection .debug_alt_sim_info
	.int 2, 1, 0b, 1b
	.popsection
#endif /* NIOS2_DCACHE_SIZE > 0 */

	/*
	 *  STATUSレジスタの初期化（全割込み禁止）
	 */
	wrctl status, zero

	/*
	 * set up the global pointer.
	 */
	movia gp, _gp

	/*
	 *  スタックの設定
	 */
	my_core_id r3
	slli    r3, r3, 2
	movia   r4, ostkpt_table
	add     r2, r4, r3
	ldw     sp, 0(r2)

	movia r2, hardware_init_hook
	beq   zero, r2, start_0
	callr r2

start_0:
	/*
	 * TNUM_HWCORE以上のIDのコアはOSを起動せず無限ループへ
	 */
	my_core_id r5
	movui r2, TNUM_HWCORE - 1
	bgtu  r5, r2, crt0_end
    
	/*
	 *  DATAセクションとBSSセクションの初期化
	 *  マスタコアのみ実行
	 */
	movui r2, OS_CORE_ID_MASTER
	bne   r2, r5, 1f
	call initialize_sections

 1:   
	/*
	 * ローカルメモリのカーネルデータ領域の初期化
	 * 自コアのローカルメモリのカーネルデータ領域のみを0クリア
	 */
	my_core_id r5
	slli  r3, r5, 2
	movhi r4, %hiadj(jmp_tbl)
	addi  r4, r4, %lo(jmp_tbl)
	add   r4, r4, r3
	ldw   r4, 0(r4)
	jmp   r4	/* core1_clear: などにジャンプ */

	/* ジャンプテーブル */
	.balign 32
jmp_tbl:
	.word core0_clear
#if TNUM_HWCORE > 1
	.word core1_clear
#endif /* TNUM_HWCORE > 1 */
#if TNUM_HWCORE > 2
	.word core2_clear
#endif /* TNUM_HWCORE > 2 */
#if TNUM_HWCORE > 3
	.word core3_clear
#endif /* TNUM_HWCORE > 3 */

core0_clear:
	movhi r4, %hiadj(__data0_start)
	addi  r4, r4, %lo(__data0_start)

	movhi r2, %hiadj(__data0_end)
	addi  r2, r2, %lo(__data0_end)
	br    start_clear

#if TNUM_HWCORE > 1 
core1_clear:
	movhi r4, %hiadj(__data1_start)
	addi  r4, r4, %lo(__data1_start)

	movhi r2, %hiadj(__data1_end)
	addi  r2, r2, %lo(__data1_end)
	br	  start_clear
#endif /* TNUM_HWCORE > 1 */

#if TNUM_HWCORE > 2 
core2_clear:
	movhi r4, %hiadj(__data2_start)
	addi  r4, r4, %lo(__data2_start)

	movhi r2, %hiadj(__data2_end)
	addi  r2, r2, %lo(__data2_end)
	br	  start_clear
#endif /* TNUM_HWCORE > 2 */

#if TNUM_HWCORE > 3 
core3_clear:
	movhi r4, %hiadj(__data3_start)
	addi  r4, r4, %lo(__data3_start)

	movhi r2, %hiadj(__data3_end)
	addi  r2, r2, %lo(__data3_end)
	br    start_clear
#endif /* TNUM_HWCORE > 3 */


start_clear:
	beq   r4, r2, end_clear:
clear_loop:
	stw  zero, (r4)
	addi r4, r4, 4
	bltu r4, r2, clear_loop
end_clear:

	/*
	 *  マスタコア以外は初期化待ち
	 */
	my_core_id r3
	movui r2, OS_CORE_ID_MASTER
	bne   r2, r3, slave_start

	/*
	 *  マスタコアを起動済みに設定する
	 */
	movhi r2, %hiadj(MAGIC_START)
	addi  r2, r2, %lo(MAGIC_START)
	slli  r5, r3, 2
	movhi r4, %hiadj(SYSVER_REG5)
	addi  r4, r4, %lo(SYSVER_REG5)
	add   r4, r4, r5
	stw   r2, 0(r4)

start_1:
	movia r2, software_init_hook
	beq   zero, r2, start_2
	callr r2

start_2:
	call target_hardware_initialize
	call main

crt0_end:
	br crt0_end

	/*
	 *  マスタコア以外のStartCore待ちルーチン
	 *  (r3にコアIDが含まれている)
	 */
slave_start:
	movhi r2, %hiadj(MAGIC_START)
	addi  r2, r2, %lo(MAGIC_START)
	slli  r5, r3, 2
	movhi r4, %hiadj(SYSVER_REG5)
	addi  r4, r4, %lo(SYSVER_REG5)
	add   r4, r4, r5
	ldw   r6, 0(r4)
	bne   r2, r6, slave_start

	call target_hardware_initialize
	call main

	br crt0_end
