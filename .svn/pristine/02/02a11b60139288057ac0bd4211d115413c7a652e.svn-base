/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2004 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2006-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id$
 */

/*
 *		コア依存モジュール アセンブリ言語部（ARM用）
 */

#define TOPPERS_MACRO_ONLY
#define TOPPERS_ASM_MACRO
#define UINT_C(val)		(val)		/* 符号無し整数型の定数を作るマクロ */
#define CAST(type, val)		(val)	/* 型キャストを行うマクロ */
#include "kernel_impl.h"
#include "offset.h"

/*
 * 例外ベクタ
 */
    .section .vector,"a"
    .global vector_table
vector_table:
    ldr pc, reset_vector       /* リセット             */
    ldr pc, undef_vector       /* 未定義命令           */
    ldr pc, swi_vector         /* ソフトウェア割込み   */
    ldr pc, prefech_vector     /* プリフェッチアボート */
    ldr pc, data_abort_vector  /* データアボート       */
    ldr pc, reset_vector
    ldr pc, irq_vector         /* IRQ                  */
    ldr pc, fiq_vector         /* FIQ                  */


/*
 *  例外ベクタの命令から参照される
 *  ジャンプ先アドレス
 */
    .global vector_ref_tbl
vector_ref_tbl:
reset_vector:
    .long   start
undef_vector:
    .long   undef_handler
swi_vector:
    .long   swi_handler
prefech_vector:
    .long   prefetch_handler
data_abort_vector:
    .long   data_abort_handler
irq_vector:
    .long   irq_handler
fiq_vector:
    .long   fiq_handler

/*
 *  タスクディスパッチャ
 */
    .text
    .align 2
    .globl dispatch
dispatch:
    /*
     *  このルーチンは，タスクコンテキスト・OS割込み禁止状態・ディスパッチ
     *  許可状態・（モデル上の）割込み優先度マスク全解除状態で呼び出さ
     *  れる
     */
    stmfd sp!, {r4 - r11,lr}           /* レジスタの保存 */
    ldr   r0, =p_runtsk                /* p_runtskを読み込む */
    ldr   r1, [r0]
    str   sp, [r1,#TCB_sp]             /* タスクスタックを保存 */
    adr   r2, dispatch_r
    str   r2, [r1,#TCB_pc]             /* 実行再開番地を保存 */

    /*
     *  実行中タスクのタスクスタックのオーバフローチェック
     */
#ifdef CFG_USE_STACKMONITORING
    /*
     *  スタックポインタチェック方式
     */
    ldr   r3, [r1, #TCB_p_tinib]		/* タスク初期化ブロック先頭アドレス取得 */
    ldr   r3, [r3, #TINIB_stk]			/* タスクスタック先頭アドレス取得 */
    cmp   sp, r3						/* スタックポインタ <= スタックの先頭の場合， */
    ble   stack_monitoring_error		/* スタックオーバーフロー発生 */

    /*
     * マジックナンバーチェック方式
     */
    ldr   r4, [r3]						/* タスクスタックの先頭アドレスからマジックナンバー領域の値取得 */
    ldr   r3, =STACK_MAGIC_NUMBER
    cmp   r4, r3						/* マジックナンバーが破壊されていた場合， */
    bne   stack_monitoring_error		/* スタックオーバーフロー発生 */
#endif /* CFG_USE_STACKMONITORING */

    b     dispatcher

dispatch_r:
    ldmfd sp!,{r4 - r11,lr}     /* レジスタの復帰 */
    bx    lr

/*
 *  ディスパッチャの動作開始（cpu_support.S）
 */
    .globl start_dispatch
start_dispatch:
	/*
	 *  このルーチンは，カーネル起動時に，すべての割込みを禁止した状態
	 *  で呼び出される．また，sp＝非タスクコンテキスト用スタックポイン
	 *  タの状態で呼び出されることを想定している
	 *
	 *  dispatcherは，OS割込み禁止状態で呼び出す
     */    
    bl	  x_nested_lock_os_int
	bl    x_unlock_all_int
    mov   r0, #0
    ldr   r2, =except_nest_cnt            /* 例外（割込み/CPU例外）のネスト回数を0に */
    str   r0, [r2]
    b     dispatcher_0

/*
 *  現在のコンテキストを捨ててディスパッチ
 */
    .globl exit_and_dispatch
exit_and_dispatch:
#ifdef CFG_USE_STACKMONITORING
	/*
	 *  実行中タスクのタスクスタックのオーバフローチェック
	 */
	/*
	 *  スタックポインタチェック方式
	 */
    ldr   r0, =p_runtsk					/* p_runtskを読み込む */
    ldr   r1, [r0]
    ldr   r2, [r1, #TCB_p_tinib]		/* タスク初期化ブロック先頭アドレス取得 */
    ldr   r2, [r2, #TINIB_stk]			/* タスクスタック先頭アドレス取得 */
    cmp   sp, r2						/* スタックポインタ <= スタックの先頭の場合， */
    ble   stack_monitoring_error		/* スタックオーバーフロー発生 */

	/*
	 *  マジックナンバーチェック方式
	 */
    ldr   r3, [r2]						/* タスクスタックの先頭アドレスからマジックナンバー領域の値取得 */
    ldr   r2, =STACK_MAGIC_NUMBER
    cmp   r3, r2						/* マジックナンバーが破壊されていた場合， */
    bne   stack_monitoring_error		/* スタックオーバーフロー発生 */
#endif /* CFG_USE_STACKMONITORING */
    /* ディスパッチャ本体（dispatcher）へ */

/*
 *  ディスパッチャ本体
 */
dispatcher:
    /*
     *  このルーチンは，タスクコンテキスト・OS割込み禁止状態・ディスパッチ
     *  許可状態・（モデル上の）割込み優先度マスク全解除状態で呼び出さ
     *  れる．実行再開番地へもこの状態のまま分岐する
     */
#ifdef LOG_DSP_ENTER
    ldr   r1, =p_runtsk     /* p_runtskをパラメータに */
    ldr   r0, [r1]        
    cmp   r0, #0
    beq   1f
    bl    log_dsp_enter
1:
#endif /* LOG_DSP_ENTER */

    /*
     *  PostTaskHookの呼び出し
     */
#ifdef CFG_USE_POSTTASKHOOK
    ldr   r0, =PostTaskHook
    cmp   r0, #0
    beq   dispatcher_0
posttaskhook_call:
    ldr   r0, =call_posttaskhook
	bl    stack_change_and_call_func_1
#endif /* CFG_USE_POSTTASKHOOK */

dispatcher_0:
    ldr   r0, =callevel_stat
	ldrh  r1, [r0]
	and   r1, r1, #(~TSYS_DISALLINT)
	strh  r1, [r0]
	bl    x_unlock_all_int

    ldr   r0, =p_schedtsk   /* p_schedtskをp_runtskに */
    ldr   r1, [r0]
    ldr   r2, =p_runtsk   
    str   r1, [r2]        
    cmp   r1, #0            /* p_runtskがNULLならdispatcher_1へ */
    beq   dispatcher_1             
    ldr   sp, [r1,#TCB_sp]  /* タスクスタックを復帰 */
#ifdef LOG_DSP_LEAVE
    mov   r0, r1            /* p_runtskをパラメータに */
    mov   r4, r1            /* r1はスクラッチレジスタなので保存 */
    bl    log_dsp_leave
    mov   r1, r4
#endif /* LOG_DSP_LEAVE */
    /*
     *  PreTaskHookの呼び出し
     */
#ifdef CFG_USE_PRETASKHOOK
    ldr   r2, =PreTaskHook
    cmp   r2, #0
    beq   dispatcher_3
pretaskhook_call:
    mov   r4, r0            /* r0はスクラッチレジスタなので保存 */
    mov   r5, r1            /* r1はスクラッチレジスタなので保存 */
    ldr   r0, =call_pretaskhook
	bl    stack_change_and_call_func_1
    mov   r1, r5
    mov   r0, r4
#endif /* USE_PRETASKHOOK */
dispatcher_3:
    ldr   r4, [r1,#TCB_pc]  /* 実行再開番地を復帰 */
    bx    r4
dispatcher_1:
    /*
     * OS割込み禁止状態の解除と，非タスクコンテキスト実行状態への
     * 準備をする
     */
    ldr   r0, =_ostkpt            /* 非タスクコンテキストのスタックへ */
    ldr   sp, [r0]    

	/* スクラッチレジスタでないr4を使用する */
    ldr   r4, =except_nest_cnt        /* r4 <-except_nest_cnt */
    mov   r0, #0
dispatcher_2:
    /*
     *  割込みを許可し，非タスクコンテキスト実行状態とし割込みを待つ．
     *
     *  ここで非タスクコンテキスト実行状態に切り換えるのは，ここで発生
     *  する割込み処理にどのスタックを使うかという問題の解決と，割込み
     *  ハンドラ内でのタスクディスパッチの防止という2つの意味がある
     *
     *  プロセッサを割込み待ちに移行させる処理と，割込み許可とは，不可
     *  分に行なう必要がある
     *  これを不可分に行なわない場合，割込みを許可した直後に割込
     *  みが入り，その中でタスクが実行可能状態になると，実行すべきタス
     *  クがあるにもかかわらずプロセッサが割込み待ちになってしまう
     *
     *  割込み待ちの間は，p_runtskをNULL（＝0）に設定しなければならな
     *  い．このように設定しないと，割込みハンドラからiget_tidを呼び出
     *  した際の動作が仕様に合致しなくなる
     *
     *  ターゲットによっては，省電力モード等に移行するため，標準の方法と
     *  異なる手順が必要な場合がある
     *  そのようなターゲットでは，ターゲット依存において，TOPPERS_CUSTOM_IDLE
     *  を定義し，アセンブラマクロとして，toppers_asm_custom_idle を用意
     *  すればよい．toppers_asm_custom_idle の記述にあたっては，次のレジ
     *  スタは使用できない
     *     r0, r1, r2, r3, sp
     *
     */
    mov   r1, #1
    str   r1, [r4]                          /* except_nest_cnt = 1 */

#ifdef TOPPERS_CUSTOM_IDLE
    toppers_asm_custom_idle
#else
	bl    x_nested_unlock_os_int
    nop
	bl    x_nested_lock_os_int
#endif /* TOPPERS_CUSTOM_IDLE */

    ldr   r0, =p_runtsk       /* p_runtsk をr1に読み込む */
    ldr   r1, [r0]
    ldr   r0, =p_schedtsk     /* p_schedtsk をr2に読み込む */
    ldr   r2, [r0]
    cmp   r1, r2              /* p_runtskとp_schedtskが同じなら */
    beq   dispatcher_2
    mov   r0, #0
    str   r0, [r4]        /* except_nest_cnt = 0 */

    b     dispatcher_0

    /*
     *  スタックオーバフロー時の処理
     */
#ifdef CFG_USE_STACKMONITORING
    /*
     *  スタックオーバフロー時プロテクションフックを呼び出し
     *  スタックを更に壊さないため，割込みスタックの初期値を使用する
     */
    .global stack_monitoring_error_isr
stack_monitoring_error_isr:
    bl    x_nested_lock_os_int			/* ISRから来た場合はOS割込み禁止になっていない */

    .global stack_monitoring_error
stack_monitoring_error:
    ldr   r2, =except_nest_cnt			/* _ostkptを使う場合はexcept_nest_cntをインクリメントする */
    ldr   r3, [r2]
    add   r3, r3, #1
    str   r3, [r2]
    ldr   r2, =_ostkpt					/* スタックを更に壊さないため，割込みスタックの初期値を使用する */
    ldr   sp, [r2]

    mov   r0, #E_OS_STACKFAULT		 	/* プロテクションフックの引数を設定 */
    b     call_protectionhk_main		/* プロテクションフックを呼び出し */
    /*
     *  プロテクションフックからは戻ってこない
     */
#endif /* CFG_USE_STACKMONITORING */

/*
 *  タスク開始時処理
 *
 *  dispatcherから呼び出されるため，TCBのアドレスはr1に入っている
 *  タスクが不正終了した場合は保護処理を行う
 */
    .text
    .globl start_r
start_r:
    mov   r5, r1            /* r1はスクラッチレジスタなので保存 */
	bl    x_nested_unlock_os_int
    ldr   lr, = exit_task                    /* 戻り番地設定 */
    ldr   r2, [r5, #TCB_p_tinib]            /* p_runtsk->p_tinibをr2に  */
    ldrb  r4, [r2, #TINIB_exepri]           /* 実行開始時の優先度取得  */
    strb  r4, [r5, #TCB_curpri]             /* 現在の優先度に設定  */
    ldr   r1, [r2, #TINIB_task]             /* タスク起動番地にジャンプ */
    bx    r1

/*
 *  割込みハンドラ/CPU例外ハンドラ出口処理
 *
 *  ret_intは，割込みハンドラから戻った直後に実行するルーチンで，
 *  割込みハンドラ終了後，ターゲット依存の処理を実行した後，
 *  OS割込みを禁止，スタックを割込み前のスタックにした
 *  状態で呼び出される
 *  r3には復帰前のcallevel_statが入っている
 */
    .text
    .global ret_int
ret_int:
    /*
     *  例外・割込みのネストカウント（except_nest_cnt)のデクリメント
     */
    ldr   r0, =except_nest_cnt   /* r0 <-except_nest_cnt */
    ldr   r1, [r0]
    sub   r2, r1, #1
    str   r2, [r0]
    cmp   r2, #0                  /* 戻り先が非タスクコンテキストなら */
    bne   ret_int_1               /* すぐにリターン                   */

	/*
	 * C1ISR, CPU例外の後にタスクディスパッチはしない
	 */
	and   r3, r3, #TCL_ISR2
	cmp   r3, #TCL_ISR2
	bne   ret_int_1

    /*
     *  reqflgをチェックする前に割込みを禁止するのは，reqflgをチェック
     *  した直後に割込みハンドラが起動され，その中でディスパッチが要求
     *  された場合に，すぐにディスパッチされないという問題が生じるため
     *  である
     */

    ldr   r0, =p_runtsk       /* p_runtsk をr1に読み込む */
    ldr   r1, [r0]
    ldr   r0, =p_schedtsk     /* p_schedtsk をr2に読み込む */
    ldr   r2, [r0]
    cmp   r1, r2              /* p_runtskとp_schedtskが異なるならret_int_2へ */
    bne   ret_int_2
ret_int_1:
    /*
     *  割込み処理からのリターンにより，全割込み禁止解除状態に移行しなければ
     *  ならない
     *  ARMでは，CPSRのIRQビットで全割込み禁止解除状態とするため，単にリターン
     *  すればよい
     */
    ldmfd sp!,{r1}              /* CPSRの復帰処理 */
    msr   spsr, r1              /* 戻り先のcpsrをspsrに設定 */
    ldmfd sp!,{r0-r3,ip,lr,pc}^ /* コンテキストの復帰，^付きなので，cpsr <- spsr */

ret_int_2:
ret_int_3:
    /*
     *  ここへは，CPU例外ハンドラの出口処理からも分岐してくる
     *
     *  ここでは，戻り先がタスクであり，スタックは，タスクスタックの上
     *  にスクラッチレジスタのみが保存された状態になっている．また，
     *  プロセッサは，スーパーバイザーモード・OS割込みを禁止
     *  した状態となっている
     */

    /*
     *  OS割込み禁止状態に移行する
     *
     *  この時点でOS割込み禁止状態とするのは，dispatcherへ分岐する時に，
	 *  OS割込み禁止状態になっている必要があるためである
     */
	bl    x_nested_lock_os_int
	bl    x_unlock_all_int

    /*
     *  p_runtskとp_schedtskが同じ場合には，ディスパッチを行わない
     */

    ldr   r0, =p_runtsk       /* p_runtsk をr1に読み込む */
    ldr   r1, [r0]
    ldr   r0, =p_schedtsk     /* p_schedtsk をr2に読み込む */
    ldr   r2, [r0]
    cmp   r1, r2              /* p_runtskとp_schedtskが同じなら */
    beq   ret_int_4           /*                    ret_int_4へ */
    stmfd sp!, {r4-r11}       /* 残りのレジスタを保存 */
    str   sp, [r1,#TCB_sp]    /* タスクスタックを保存 */
    adr   r0, ret_int_r       /* 実行再開番地を保存   */
    str   r0, [r1,#TCB_pc]
    b     dispatcher

ret_int_r: 
    ldmfd sp!, {r4-r11}       /* レジスタの復帰 */
ret_int_4:
    ldmfd sp!, {r0}             /* spsr を復帰 */
	mrs   r1,cpsr
	and   r1,r1,#(CPSR_IRQ_BIT)
	orr   r0,r0,r1
    msr   spsr,r0               /* 戻り先のcpsrをspsrに設定 */
	mrs   r1,cpsr
	orr   r1,r1,#(CPSR_IRQ_BIT)
    msr   cpsr,r1
    bl    x_nested_unlock_os_int
    ldmfd sp!,{r0-r3,ip,lr,pc}^ /* タスクに復帰 ^付きなので，cpsr <- spsr */

/*
 * CPU例外ハンドラ
 *
 * CPU例外ハンドラは，非タスクコンテキストで実行する
 * 
 */

/*
 *  未定義命令 例外ハンドラ
 */
    .text
    .align 2
    .global undef_handler
undef_handler:
    /* 
     *  タスクの動作時モード(スーパーバイザーモード)へ
     */
    msr   cpsr, #(CPSR_SVC|CPSR_INTLOCK|CPSR_ALWAYS_SET) 
    stmfd sp!, {r0-r3,ip,lr,pc} /* pcはダミー */

    /*
     * spsrと戻り番地を取得するために未定義モードへ
     */
    msr   cpsr, #(CPSR_UND|CPSR_INTLOCK|CPSR_ALWAYS_SET)
    mov   r0, lr
    mrs   r1, spsr
    mov   r2, #EXCH_NO_UNDEF
    b     target_exc_handler


/*
 *  SWI 例外ハンドラ
 */
    .text
    .align 2
    .global swi_handler
swi_handler:
    /* 
     *  タスクの動作時モード(スーパーバイザーモード)へ
     *  元々スーパーバイザーモードだが，全割込み禁止状態とする
     */
    msr   cpsr, #(CPSR_SVC|CPSR_INTLOCK|CPSR_ALWAYS_SET)
    stmfd sp!, {r0-r3,ip,lr,pc} /* pcはダミー */
    mov   r0, lr
    mrs   r1, spsr
    mov   r2, #EXCH_NO_SWI
    b     target_exc_handler

/*
 *  プリフェッチアボード 例外ハンドラ
 */
    .text
    .align 2
    .global prefetch_handler
prefetch_handler:
    /* 
     *  タスクの動作時モード(スーパーバイザーモード)へ
     */
    msr   cpsr, #(CPSR_SVC|CPSR_INTLOCK|CPSR_ALWAYS_SET) 
    stmfd sp!, {r0-r3,ip,lr,pc} /* pcはダミー */

    /*
     * spsrと戻り番地を取得するためにアボートモードへ
     */
    msr   cpsr, #(CPSR_ABT|CPSR_INTLOCK|CPSR_ALWAYS_SET)
    mov   r0, lr
    mrs   r1, spsr
    mov   r2, #EXCH_NO_PABORT
    b     target_exc_handler


/*
 *  データアボード 例外ハンドラ
 */
    .text
    .align 2
    .global data_abort_handler
data_abort_handler:
    /* 
     *  タスクの動作時モード(スーパーバイザーモード)へ
     */
    msr   cpsr, #(CPSR_SVC|CPSR_INTLOCK|CPSR_ALWAYS_SET) 
    stmfd sp!, {r0-r3,ip,lr,pc} /* pcはダミー */

    /*
     * spsrと戻り番地を取得するためにアボートモードへ
     */
    msr   cpsr, #(CPSR_ABT|CPSR_INTLOCK|CPSR_ALWAYS_SET)
    mov   r0, lr
    mrs   r1, spsr    
    mov   r2, #EXCH_NO_DABORT
    b     target_exc_handler

#ifndef TARGET_FIQ_HANDLER
/*
 *  FIQ 例外ハンドラ
 */
    .text
    .align 2
    .global fiq_handler
fiq_handler:
    /* 
     *  タスクの動作時モード(スーパーバイザーモード)へ
     */
    msr   cpsr, #(CPSR_SVC|CPSR_FIQ_BIT|CPSR_INTLOCK|CPSR_ALWAYS_SET) 
    stmfd sp!, {r0-r3,ip,lr,pc} /* pcはダミー */

    /*
     * spsrと戻り番地を取得するためにFIQモードへ
     */
    msr   cpsr, #(CPSR_FIQ|CPSR_FIQ_BIT|CPSR_INTLOCK|CPSR_ALWAYS_SET)
    mov   r0, lr
    mrs   r1, spsr    
    mov   r2, #EXCH_NO_FIQ
    b     target_exc_handler
#endif /* TARGET_FIQ_HANDLER */

#ifdef __thumb__
    .text
    .align 2
    .global current_sr
current_sr:
    mrs   r0, cpsr
    bx    lr

   .global set_sr
set_sr:
    msr   cpsr, r0
    bx    lr
#endif /* __thumb__ */

    .text
    .align 2
    .global stack_change_and_call_func_1
    .global stack_change_and_call_func_2
stack_change_and_call_func_1:
stack_change_and_call_func_2:
    /*
     * 割込み発生時のコンテキストを判定
     */
	push  {r4, lr}
    ldr   r3, =except_nest_cnt
    ldr   r4, [r3]
    add   r4, r4, #1
    str   r4, [r3]
    cmp   r4, #1
    bne   1f

	mov   r3, sp
    ldr   r4, =_ostkpt
	ldr   sp, [r4]
	push  {r3}
1:
    
	mov   r3, r0
	mov   r0, r1
	mov   r1, r2
	mov   lr, pc
	bx    r3

    ldr   r3, =except_nest_cnt
    ldr   r4, [r3]
    sub   r4, r4, #1
    str   r4, [r3]
    cmp   r4, #0
    bne   1f

	pop   {r3}
	mov   sp, r3

1:

	pop   {r4, lr}
	bx    lr
