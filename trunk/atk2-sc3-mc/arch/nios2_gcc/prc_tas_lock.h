/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2017 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2012-2017 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2012-2013 by Spansion LLC, USA
 *  Copyright (C) 2012-2017 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2012-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2012-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2012-2017 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2012-2017 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2017 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: prc_tas_lock.h 727 2017-01-23 09:27:59Z witz-itoyo $
 */

/*
 *	OS内排他向けTest&Setロック制御モジュール（Nios2用）
 */

#ifndef TOPPERS_PRC_TAS_LOCK_H
#define TOPPERS_PRC_TAS_LOCK_H

#if TTYPE_KLOCK == G_KLOCK

/*
 *  ジャイアントロックの初期化（G_KLOCKの場合）
 */
LOCAL_INLINE void
x_initialize_giant_lock(LockType *p_giant_lock)
{
	*p_giant_lock = GIANT_LOCK;
	nios2_mutex_init_lock(*p_giant_lock);
}

#endif /* TTYPE_KLOCK == G_KLOCK */

#if TTYPE_KLOCK == C_KLOCK

/*
 *  タスクロックのテーブル
 *  ターゲット依存部で定義．
 */
extern const uint32	target_tsk_lock_table[];

/*
 *  タスクロックの初期化（C_KLOCKの場合）
 */
LOCAL_INLINE void
x_initialize_tsk_lock(LockType *p_tsk_lock)
{
	*p_tsk_lock = target_tsk_lock_table[x_core_id()];
	nios2_mutex_init_lock(*p_tsk_lock);
}

/*
 *  カウンタロックのテーブル
 *  ターゲット依存部で定義．
 */
extern const uint32	target_cnt_lock_table[];

/*
 *  カウンタロックの初期化
 */
LOCAL_INLINE void
x_initialize_cnt_lock(LockType *p_cnt_lock)
{
	*p_cnt_lock = target_cnt_lock_table[x_core_id()];
	nios2_mutex_init_lock(*p_cnt_lock);
}

#if TTYPE_SPN == EMULATE_SPN

/*
 *  スピンロックロックの初期化
 */
LOCAL_INLINE void
x_initialize_spn_lock(LockType *p_spn_lock)
{
	*p_spn_lock = SPN_LOCK;
	nios2_mutex_init_lock(*p_spn_lock);
}

#elif TTYPE_SPN == NATIVE_SPN

/*
 *  ネイティブスピンロックのテーブル
 *  ターゲット依存部で定義．
 */
extern const uint32	target_spn_lock_table[];

/*
 *  スピンロックの初期化
 */
LOCAL_INLINE void
x_initialize_spin(SpinlockIdType i, LockType *p_spn_lock)
{
	*p_spn_lock = target_spn_lock_table[i - 1U];
	nios2_mutex_init_lock(*p_spn_lock);
}

#else /* TTYPE_SPN == NATIVE_SPN */

#error TTYPE_SPN can be NATIVE_SPN or EMULATE_SPN

#endif /* TTYPE_SPN == NATIVE_SPN */

/*
 *  IOCロックの初期化
 */
LOCAL_INLINE void
x_initialize_ioc_lock(LockType *p_ioc_lock)
{
	*p_ioc_lock = IOC_LOCK;
	nios2_mutex_init_lock(*p_ioc_lock);
}

#endif /* TTYPE_KLOCK == C_KLOCK */


/*
 *  ロック取得解放関数
 */

/*
 *  1段目のロック取得（タスク・非タスクコンテキスト共用）
 */
LOCAL_INLINE void
x_acquire_lock(const LockType *p_lock)
{
	CoreIdType coreid = x_core_id();

	while (1) {
		if (nios2_mutex_try_lock(*p_lock, coreid) != FALSE) {
			/* ロック取得成功 */
			Asm("" ::: "memory");
			return;
		}
		/* 割込みを許可する */
		x_nested_unlock_os_int();
		x_nested_lock_os_int();
	}
}

/*
 *  2段目のロック取得（タスク・非タスクコンテキスト共用）
 */
LOCAL_INLINE void
x_acquire_lock_with_os_int(const LockType *p_lock)
{
	CoreIdType coreid = x_core_id();

	while (1) {
		if (nios2_mutex_try_lock(*p_lock, coreid) != FALSE) {
			/* ロック取得成功 */
			Asm("" ::: "memory");
			return;
		}
	}
}

/*
 *  ロックの解放（タスク・非タスクコンテキスト共用）
 */
LOCAL_INLINE void
x_release_lock(const LockType *p_lock)
{
	Asm("" ::: "memory");
	nios2_mutex_release_lock(*p_lock, x_core_id());
}

/*
 *  ロックの取得試行（タスク・非タスクコンテキスト共用）
 */
LOCAL_INLINE boolean
x_try_lock(const LockType *p_lock)
{
	if (nios2_mutex_try_lock(*p_lock, x_core_id()) != FALSE) {
		/* ロック取得成功 */
		Asm("" ::: "memory");
		return(TRUE);
	}
	return(FALSE);
}

/*
 *  タイトループ中におけるロック解放後の一定時間待ち
 */
LOCAL_INLINE void
wait_spn_lock(void)
{
	/*
	 *  GetSpinlock内部で，スピンロックロックを解放してから
	 *  再取得するタイトループにおいて，他のコアがスピンロック
	 *  を取得できる隙間を空けるための待ち時間を用意する
	 *
	 *  具体的には，
	 *  release_spn_lock()
	 *  {
	 *      sil_wrw_iop((void *) (base + MUTEX_MUTEX_OFFSET), ((uint32) coreid << MUTEX_MUTEX_OWNER_OFFSET));
	 *  }
	 *  x_nested_unlock_os_int
	 *  goto while(1)
	 *  x_nested_lock_os_int
	 *  acquire_spn_lock()
	 *  {
	 *      data = ((uint32) coreid << MUTEX_MUTEX_OWNER_OFFSET) | MUTEX_VALUE_DATA;
	 *      sil_wrw_iop((void *) (base + MUTEX_MUTEX_OFFSET), data);
	 *      ...
	 *  }
	 *  の間に，
	 *  acquire_spn_lock()
	 *  {
	 *      ...
	 *      sil_wrw_iop((void *) (base + MUTEX_MUTEX_OFFSET), data);
	 *      check = sil_rew_iop((void *) (base + MUTEX_MUTEX_OFFSET));
	 *      return((data == check) ? TRUE : FALSE);
	 *  }
	 *  if (nios2_mutex_try_lock(*p_lock, coreid) != FALSE) {
	 *  x_nested_unlock_os_int
	 *  x_nested_lock_os_int
	 *  goto while(1)
	 *  acquire_spn_lock()
	 *  {
	 *      data = ((uint32) coreid << MUTEX_MUTEX_OWNER_OFFSET) | MUTEX_VALUE_DATA;
	 *      sil_wrw_iop((void *) (base + MUTEX_MUTEX_OFFSET), data);
	 *      ...
	 *  }
	 *  が成功するだけの時間を空けることとなる
	 *  よって，メモリアクセス2回，比較演算1，2回＋α分程度の時間待つ
	 */
	Asm("nop");
	Asm("nop");
	Asm("nop");
	Asm("nop");
	Asm("nop");
	Asm("nop");
	Asm("" ::: "memory");
}

#endif /* TOPPERS_PRC_TAS_LOCK_H */
