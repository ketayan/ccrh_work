#
#  TOPPERS ATK2
#      Toyohashi Open Platform for Embedded Real-Time Systems
#      Automotive Kernel Version 2
#
#  Copyright (C) 2008-2017 by Center for Embedded Computing Systems
#              Graduate School of Information Science, Nagoya Univ., JAPAN
#  Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
#  Copyright (C) 2011-2013 by Spansion LLC, USA
#  Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
#  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
#  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
#  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
#  Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
#  Copyright (C) 2011-2017 by Witz Corporation
#  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
#  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
#  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
#
#  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
#  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
#  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
#  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
#      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
#      スコード中に含まれていること．
#  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
#      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
#      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
#      の無保証規定を掲載すること．
#  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
#      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
#      と．
#    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
#        作権表示，この利用条件および下記の無保証規定を掲載すること．
#    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
#        報告すること．
#  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
#      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
#      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
#      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
#      免責すること．
#
#  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
#  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
#  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
#  用する者に対して，AUTOSARパートナーになることを求めている．
#
#  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
#  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
#  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
#  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
#  の責任を負わない．
#
#  $Id: Makefile.target 758 2017-02-28 12:24:40Z witz-itoyo $
#

#
#		Makefile のターゲット依存部（NIOS2_DEV用）
#

#
#  ボード名の定義
#
BOARD = nios2_dev_de2_115

#
#  32bit境界への関数の配置を有効に
#
ALIGN_FUNCTIONS_32 = true

#
#  共有するオブジェクトをSDRAMに配置する
#
PLACE_SDRAM = true

#
#  シリアル出力をUARTにする
#
SERIAL_UART = false

ifeq ($(SERIAL_UART),true)
	CDEFS := $(CDEFS) -DUSE_UART
	UART_OBJ := uart.o
else
	UART_OBJ := jtag_uart.o
endif

#
#  ボード毎に設定が異なる項目
#
ifeq ($(BOARD),nios2_dev_de2_115)
	CDEFS := $(CDEFS) -DTOPPERS_NIOS2_DEV_DE2_115
	CFG1_OUT_LDSCRIPT = $(TARGETDIR)/nios2_dev_de2_115.ld
	ifeq ($(PLACE_SDRAM),true)
		CDEFS := $(CDEFS) -DPLACE_SDRAM
	endif
endif

#
#  コンパイルオプション
#
INCLUDES := $(INCLUDES) -I$(TARGETDIR)

#
#  サポート命令毎のコンパイルオプション
#
# ノーマル
COPTS   := $(COPTS) -mhw-mul -mhw-div

#
#  カーネルに関する定義
#
KERNEL_DIR := $(KERNEL_DIR) $(TARGETDIR)
KERNEL_ASMOBJS := $(KERNEL_ASMOBJS)
KERNEL_COBJS := $(KERNEL_COBJS) target_config.o target_hw_counter.o

#
#  システムモジュールに関する定義
#
SYSMOD_DIR := $(SYSMOD_DIR) $(TARGETDIR)
SYSMOD_COBJS := $(SYSMOD_COBJS) $(UART_OBJ)


#
#  コンフィギュレーション設定
#
ifeq ($(findstring target_timer,$(CFGNAME)),target_timer)
  CFGNAME := $(CFGNAME) avalon_timer
endif
ifeq ($(findstring target_serial,$(CFGNAME)),target_serial)
  CFGNAME := $(CFGNAME) prc_serial
endif

#
#  依存関係の定義
#
Os_Lcfg.timestamp: $(TARGETDIR)/target.tf $(SRCDIR)/arch/gcc/ldscript.tf
kernel_mem3.c: $(TARGETDIR)/target_opt.tf
kernel_mem.c: $(TARGETDIR)/target_mem.tf
offset.h: $(TARGETDIR)/target_offset.tf

#
#  オフセットファイル生成のための定義
#
OFFSET_TF := $(TARGETDIR)/target_offset.tf

#
#  ジェネレータ関係の変数の定義
#
CFG_TABS := $(CFG_TABS) --cfg1-def-table $(TARGETDIR)/target_def.csv

#
#  プロセッサ依存部のインクルード
#
include $(SRCDIR)/arch/nios2_gcc/Makefile.prc

#
#  各種コマンドの実行
#
CORE0=0
CORE1=1

run: $(OBJNAME).srec
	nios2-gdb-server --cable=USB-Blaster --instance=$(CORE0) --no-verify -r --stop $(OBJNAME).srec
	nios2-gdb-server --cable=USB-Blaster --instance=$(CORE1) --no-verify -r --stop
	nios2-gdb-server --cable=USB-Blaster --instance=$(CORE0) --no-verify --go
	nios2-gdb-server --cable=USB-Blaster --instance=$(CORE1) --no-verify --go
	cygstart --shownoactivate nios2-terminal --cable=USB-Blaster --instance=$(CORE1) &
	nios2-terminal --cable=USB-Blaster --instance=$(CORE0)

run1: $(OBJNAME).srec
	nios2-gdb-server --cable="USB-Blaster [USB-0]" --instance=$(CORE0) --no-verify -r --stop $(OBJNAME).srec
	nios2-gdb-server --cable="USB-Blaster [USB-0]" --instance=$(CORE1) --no-verify -r --stop
	nios2-gdb-server --cable="USB-Blaster [USB-0]" --instance=$(CORE0) --no-verify --go
	nios2-gdb-server --cable="USB-Blaster [USB-0]" --instance=$(CORE1) --no-verify --go
	cygstart --shownoactivate nios2-terminal "--cable=\"USB-Blaster [USB-0]\" --instance=$(CORE1)" &
	nios2-terminal --cable="USB-Blaster [USB-0]" --instance=$(CORE0)

run2: $(OBJNAME).srec
	nios2-gdb-server --cable="USB-Blaster [USB-1]" --instance=$(CORE0) --no-verify -r --stop $(OBJNAME).srec
	nios2-gdb-server --cable="USB-Blaster [USB-1]" --instance=$(CORE1) --no-verify -r --stop
	nios2-gdb-server --cable="USB-Blaster [USB-1]" --instance=$(CORE0) --no-verify --go
	nios2-gdb-server --cable="USB-Blaster [USB-1]" --instance=$(CORE1) --no-verify --go
	cygstart --shownoactivate nios2-terminal "--cable=\"USB-Blaster [USB-1]\" --instance=$(CORE1)" &
	nios2-terminal --cable="USB-Blaster [USB-1]" --instance=$(CORE0)

urun: $(OBJNAME).srec
	@nios2-gdb-server --cable=USB-Blaster --instance=$(CORE0) --no-verify -r --stop $(OBJNAME).srec
	@nios2-gdb-server --cable=USB-Blaster --instance=$(CORE1) --no-verify --stop
	@cygstart --shownoactivate ruby $(TARGETDIR)/com_port.rb 1
	@cygstart --shownoactivate ruby $(TARGETDIR)/com_port.rb
	@cygstart --shownoactivate nios2-gdb-server --cable=USB-Blaster --instance=$(CORE0) --wait 2 --no-verify --go &
	@cygstart --shownoactivate nios2-gdb-server --cable=USB-Blaster --instance=$(CORE1) --wait 2 -r --no-verify --go

srun: $(OBJNAME).srec
	@nios2-gdb-server.exe --cable=USB-Blaster --instance=$(CORE0) --no-verify -r --go $(OBJNAME).srec 1>&2 &
	@expect -c "set timeout -1; spawn nios2-terminal --cable=USB-Blaster --instance=$(CORE0) -q; expect \"Kernel Exit...\"; send \"\003\""

surun: $(OBJNAME).srec
	@cygstart --shownoactivate ruby $(TARGETDIR)/com_port.rb
	@cygstart --shownoactivate nios2-gdb-server --cable=USB-Blaster --instance=$(CORE0) --wait 2 --no-verify -r --go $(OBJNAME).srec

db: $(OBJFILE)
	cygstart --hide nios2-gdb-server --cable=USB-Blaster --instance=$(CORE0) --tcpport=1234 -r&
	cygstart --hide nios2-gdb-server --cable=USB-Blaster --instance=$(CORE1) --tcpport=1235 -r&
	cygstart --shownoactivate nios2-terminal --cable=USB-Blaster --instance=$(CORE0) &
	cygstart --shownoactivate nios2-terminal --cable=USB-Blaster --instance=$(CORE1) &
	cygstart nios2-elf-gdb $(OBJFILE) -command $(TARGETDIR)/gdb2.ini &
	nios2-elf-gdb $(OBJFILE) -command $(TARGETDIR)/gdb1.ini

