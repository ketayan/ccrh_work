/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2011-2017 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2017 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2017 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: svc_funccall.h 727 2017-01-23 09:27:59Z witz-itoyo $
 */

/*  This file is generated from svc.def by gensvc_atk. */

#ifndef TOPPERS_SVC_FUNCCALL_H
#define TOPPERS_SVC_FUNCCALL_H

#define SVC_CALL(system_service)	kernel_ ## system_service

#ifdef TOPPERS_SVC_FUNCCALL
#define StartOS						kernel_StartOS
#define ShutdownOS					kernel_ShutdownOS
#define ActivateTask				kernel_ActivateTask
#define TerminateTask				kernel_TerminateTask
#define ChainTask					kernel_ChainTask
#define Schedule					kernel_Schedule
#define GetTaskID					kernel_GetTaskID
#define GetTaskState				kernel_GetTaskState
#define EnableAllInterrupts			kernel_EnableAllInterrupts
#define DisableAllInterrupts		kernel_DisableAllInterrupts
#define ResumeAllInterrupts			kernel_ResumeAllInterrupts
#define SuspendAllInterrupts		kernel_SuspendAllInterrupts
#define ResumeOSInterrupts			kernel_ResumeOSInterrupts
#define SuspendOSInterrupts			kernel_SuspendOSInterrupts
#define GetISRID					kernel_GetISRID
#define DisableInterruptSource		kernel_DisableInterruptSource
#define EnableInterruptSource		kernel_EnableInterruptSource
#define GetResource					kernel_GetResource
#define ReleaseResource				kernel_ReleaseResource
#define SetEvent					kernel_SetEvent
#define ClearEvent					kernel_ClearEvent
#define GetEvent					kernel_GetEvent
#define WaitEvent					kernel_WaitEvent
#define GetAlarmBase				kernel_GetAlarmBase
#define GetAlarm					kernel_GetAlarm
#define SetRelAlarm					kernel_SetRelAlarm
#define SetAbsAlarm					kernel_SetAbsAlarm
#define CancelAlarm					kernel_CancelAlarm
#define IncrementCounter			kernel_IncrementCounter
#define GetCounterValue				kernel_GetCounterValue
#define GetElapsedValue				kernel_GetElapsedValue
#define CheckISRMemoryAccess		kernel_CheckISRMemoryAccess
#define CheckTaskMemoryAccess		kernel_CheckTaskMemoryAccess
#define CheckTaskAccess				kernel_CheckTaskAccess
#define CheckISRAccess				kernel_CheckISRAccess
#define CheckAlarmAccess			kernel_CheckAlarmAccess
#define CheckResourceAccess			kernel_CheckResourceAccess
#define CheckCounterAccess			kernel_CheckCounterAccess
#define CheckScheduleTableAccess	kernel_CheckScheduleTableAccess
#define CheckTaskOwnership			kernel_CheckTaskOwnership
#define CheckISROwnership			kernel_CheckISROwnership
#define CheckAlarmOwnership			kernel_CheckAlarmOwnership
#define CheckCounterOwnership		kernel_CheckCounterOwnership
#define CheckScheduleTableOwnership	kernel_CheckScheduleTableOwnership
#define GetApplicationID			kernel_GetApplicationID
#define CallTrustedFunction			kernel_CallTrustedFunction
#define GetApplicationState			kernel_GetApplicationState
#define GetActiveApplicationMode	kernel_GetActiveApplicationMode
#define StartScheduleTableRel		kernel_StartScheduleTableRel
#define StartScheduleTableAbs		kernel_StartScheduleTableAbs
#define StopScheduleTable			kernel_StopScheduleTable
#define NextScheduleTable			kernel_NextScheduleTable
#define GetScheduleTableStatus		kernel_GetScheduleTableStatus
#define GetFaultyContext			kernel_GetFaultyContext
#define ioc_send_generic			kernel_ioc_send_generic
#define ioc_write_generic			kernel_ioc_write_generic
#define ioc_receive_generic			kernel_ioc_receive_generic
#define ioc_read_generic			kernel_ioc_read_generic
#define ioc_empty_queue_generic		kernel_ioc_empty_queue_generic
#define AllowAccess					kernel_AllowAccess
#define TerminateApplication		kernel_TerminateApplication
#define GetCurrentApplicationID		kernel_GetCurrentApplicationID
#endif

#ifndef TOPPERS_MACRO_ONLY

extern void kernel_StartOS(AppModeType Mode);
extern void kernel_ShutdownOS(StatusType Error);
extern StatusType kernel_ActivateTask(TaskType TaskID);
extern StatusType kernel_TerminateTask(void);
extern StatusType kernel_ChainTask(TaskType TaskID);
extern StatusType kernel_Schedule(void);
extern StatusType kernel_GetTaskID(TaskRefType TaskID);
extern StatusType kernel_GetTaskState(TaskType TaskID, TaskStateRefType State);
extern void kernel_EnableAllInterrupts(void);
extern void kernel_DisableAllInterrupts(void);
extern void kernel_ResumeAllInterrupts(void);
extern void kernel_SuspendAllInterrupts(void);
extern void kernel_ResumeOSInterrupts(void);
extern void kernel_SuspendOSInterrupts(void);
extern ISRType kernel_GetISRID(void);
extern StatusType kernel_DisableInterruptSource(ISRType DisableISR);
extern StatusType kernel_EnableInterruptSource(ISRType EnableISR);
extern StatusType kernel_GetResource(ResourceType ResID);
extern StatusType kernel_ReleaseResource(ResourceType ResID);
extern StatusType kernel_SetEvent(TaskType TaskID, EventMaskType Mask);
extern StatusType kernel_ClearEvent(EventMaskType Mask);
extern StatusType kernel_GetEvent(TaskType TaskID, EventMaskRefType Event);
extern StatusType kernel_WaitEvent(EventMaskType Mask);
extern StatusType kernel_GetAlarmBase(AlarmType AlarmID, AlarmBaseRefType Info);
extern StatusType kernel_GetAlarm(AlarmType AlarmID, TickRefType Tick);
extern StatusType kernel_SetRelAlarm(AlarmType AlarmID, TickType increment, TickType cycle);
extern StatusType kernel_SetAbsAlarm(AlarmType AlarmID, TickType start, TickType cycle);
extern StatusType kernel_CancelAlarm(AlarmType AlarmID);
extern StatusType kernel_IncrementCounter(CounterType CounterID);
extern StatusType kernel_GetCounterValue(CounterType CounterID, TickRefType Value);
extern StatusType kernel_GetElapsedValue(CounterType CounterID, TickRefType Value, TickRefType ElapsedValue);
extern AccessType kernel_CheckISRMemoryAccess(ISRType ISRID, MemoryStartAddressType Address, MemorySizeType Size);
extern AccessType kernel_CheckTaskMemoryAccess(TaskType TaskID, MemoryStartAddressType Address, MemorySizeType Size);
extern ObjectAccessType kernel_CheckTaskAccess(ApplicationType ApplID, TaskType TaskID);
extern ObjectAccessType kernel_CheckISRAccess(ApplicationType ApplID, ISRType ISRID);
extern ObjectAccessType kernel_CheckAlarmAccess(ApplicationType ApplID, AlarmType AlarmID);
extern ObjectAccessType kernel_CheckResourceAccess(ApplicationType ApplID, ResourceType ResID);
extern ObjectAccessType kernel_CheckCounterAccess(ApplicationType ApplID, CounterType CounterID);
extern ObjectAccessType kernel_CheckScheduleTableAccess(ApplicationType ApplID, ScheduleTableType ScheduleTableID);
extern ApplicationType kernel_CheckTaskOwnership(TaskType TaskID);
extern ApplicationType kernel_CheckISROwnership(ISRType ISRID);
extern ApplicationType kernel_CheckAlarmOwnership(AlarmType AlarmID);
extern ApplicationType kernel_CheckCounterOwnership(CounterType CounterID);
extern ApplicationType kernel_CheckScheduleTableOwnership(ScheduleTableType ScheduleTableID);
extern ApplicationType kernel_GetApplicationID(void);
extern StatusType kernel_CallTrustedFunction(TrustedFunctionIndexType FunctionIndex, TrustedFunctionParameterRefType FunctionParams);
extern StatusType kernel_GetApplicationState(ApplicationType Application, ApplicationStateRefType Value);
extern AppModeType kernel_GetActiveApplicationMode(void);
extern StatusType kernel_StartScheduleTableRel(ScheduleTableType ScheduleTableID, TickType Offset);
extern StatusType kernel_StartScheduleTableAbs(ScheduleTableType ScheduleTableID, TickType Start);
extern StatusType kernel_StopScheduleTable(ScheduleTableType ScheduleTableID);
extern StatusType kernel_NextScheduleTable(ScheduleTableType ScheduleTableID_From, ScheduleTableType ScheduleTableID_To);
extern StatusType kernel_GetScheduleTableStatus(ScheduleTableType ScheduleTableID, ScheduleTableStatusRefType ScheduleStatus);
extern FaultyContextType kernel_GetFaultyContext(void);
extern Std_ReturnType kernel_ioc_send_generic(IocType IocWrapperId, const void *in);
extern Std_ReturnType kernel_ioc_write_generic(IocType IocWrapperId, const void *in);
extern Std_ReturnType kernel_ioc_receive_generic(IocType IocId, void *out);
extern Std_ReturnType kernel_ioc_read_generic(IocType IocId, void *out);
extern Std_ReturnType kernel_ioc_empty_queue_generic(IocType IocId);
extern StatusType kernel_AllowAccess(void);
extern StatusType kernel_TerminateApplication(ApplicationType Application, RestartType RestartOption);
extern ApplicationType kernel_GetCurrentApplicationID(void);

#endif /* TOPPERS_MACRO_ONLY */
#endif /* TOPPERS_SVC_FUNCCALL */
