$
$  TOPPERS ATK2
$      Toyohashi Open Platform for Embedded Real-Time Systems
$      Automotive Kernel Version 2
$
$  Copyright (C) 2012-2015 by Center for Embedded Computing Systems
$              Graduate School of Information Science, Nagoya Univ., JAPAN
$  Copyright (C) 2012-2014 by FUJISOFT INCORPORATED, JAPAN
$  Copyright (C) 2012-2013 by Spansion LLC, USA
$  Copyright (C) 2012-2013 by NEC Communication Systems, Ltd., JAPAN
$  Copyright (C) 2012-2014 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
$  Copyright (C) 2012-2014 by Renesas Electronics Corporation, JAPAN
$  Copyright (C) 2012-2014 by Sunny Giken Inc., JAPAN
$  Copyright (C) 2012-2014 by TOSHIBA CORPORATION, JAPAN
$  Copyright (C) 2012-2014 by Witz Corporation, JAPAN
$
$  上記著作権者は，以下の(1)～(4)の条件を満たす場合に限り，本ソフトウェ
$  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
$  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
$  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
$      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
$      スコード中に含まれていること．
$  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
$      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
$      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
$      の無保証規定を掲載すること．
$  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
$      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
$      と．
$    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
$        作権表示，この利用条件および下記の無保証規定を掲載すること．
$    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
$        報告すること．
$  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
$      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
$      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
$      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
$      免責すること．
$
$  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
$  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
$  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
$  用する者に対して，AUTOSARパートナーになることを求めている．
$
$  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
$  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
$  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
$  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
$  の責任を負わない．
$
$  $Id: target.tf 769 2017-03-03 06:41:08Z ertl-honda $
$

$
$     パス2のターゲット依存テンプレート（RH850F1H_PB用）
$

$ 
$  有効な割込み番号の定義
$ 
$INTNO_VALID = { 
	0x10000,0x10001,...,0x1001f;
	0x20000,0x20001,...,0x2001f;
	0xffff0020,0xffff0021,...,0xffff015e
}$

$ 
$  制御可能な割込み番号
$ 
$INTNO_CONTROLLABLE  = {
	0x10000,0x10001,...,0x1001f;
	0x20000,0x20001,...,0x2001f;
	0xffff0020,0xffff0021,...,0xffff015e
}$

$ 
$  コア間割込み番号の定義
$ 
$INTNO_ICI_LIST = {0x10000,0x20000}$

$ 
$  コア間割込み優先度の定義
$ 
$INTPRI_ICI_LIST = {INTPRI_ICI0,INTPRI_ICI1}$

$ 
$  システム周期の時間(秒)をシステム周期タイマのクロックサイクルに変換
$ 
$FUNCTION SYSCYC_SEC_TO_CLOCK$
	$sec_int = FLOAT_TO_FIXINT(ARGV[1])$
	$clock_tmp = sec_int * SYSCYC_TIMER_CLOCK_HZ$
	$clock = clock_tmp / ATOI(sec_int)$
$   // overflow check
	$IF clock > 0xffffffff$
		$ERROR$
			$FORMAT("SystemCycle clock cycle is overflow: %1% sec to %2% cycle!", ARGV[1], clock)$
		$END$
	$END$
	$RESULT = clock$
$END$

$ 
$  システム周期の時間(秒)をシステム周期タイマのクロックサイクルに変換
$ 
$FUNCTION TWD_SEC_TO_CLOCK$
	$sec_int = FLOAT_TO_FIXINT(ARGV[1])$
	$clock_tmp = sec_int * TWD_TIMER_CLOCK_HZ$
	$clock = clock_tmp / ATOI(sec_int)$
$   // overflow check
	$IF clock > 0xffffffff$
		$ERROR$
			$FORMAT("TimeWindow clock cycle is overflow: %1% sec to %2% cycle!", ARGV[1], clock)$
		$END$
	$END$
	$RESULT = clock$
$END$

$ 
$  タイムウィンドウ用の割込み番号かどうかのチェック
$ 
$FUNCTION IS_TIMEWINDOWTIMER_INTNO$
	$IF ((ARGV[1] & 0x0000ffff) == TIMEWINDOWTIMER_INTNO_CORE0) || ((ARGV[1] & 0xffff0000) == TIMEWINDOWTIMER_INTNO_CORE1)$
		$RESULT = 1$
	$ELSE$
		$RESULT = 0$
	$END$
$END$

$ 
$  システム周期用の割込み番号かどうかのチェック
$ 
$FUNCTION IS_SYSTEMCYCLETIMER_INTNO$
	$IF ((ARGV[1] & 0x0000ffff) == SYSTEMCYCLETIMER_INTNO_CORE0) || ((ARGV[1] & 0xffff0000) == SYSTEMCYCLETIMER_INTNO_CORE1)$
		$RESULT = 1$
	$ELSE$
		$RESULT = 0$
	$END$
$END$

$ 
$  タイムウィンドウ用の割込み番号かどうかのチェック
$ 
$FUNCTION IS_TIMEWINDOWTIMER_INTNO_CORE$
	$IF ARGV[2] == 0$
		$IF ((ARGV[1] & 0x0000ffff) == TIMEWINDOWTIMER_INTNO_CORE0)$
			$RESULT = 1$
		$ELSE$
			$RESULT = 0$
		$END$
	$ELSE$
		$IF ((ARGV[1] & 0x0000ffff) == TIMEWINDOWTIMER_INTNO_CORE1)$
			$RESULT = 1$
		$ELSE$
			$RESULT = 0$
		$END$
	$END$
$END$

$ 
$  システム周期用の割込み番号かどうかのチェック
$ 
$FUNCTION IS_SYSTEMCYCLETIMER_INTNO_CORE$
	$IF ARGV[2] == 0$
		$IF ((ARGV[1] & 0x0000ffff) == SYSTEMCYCLETIMER_INTNO_CORE0)$
			$RESULT = 1$
		$ELSE$
			$RESULT = 0$
		$END$
	$ELSE$
		$IF ((ARGV[1] & 0x0000ffff) == SYSTEMCYCLETIMER_INTNO_CORE1)$
			$RESULT = 1$
		$ELSE$
			$RESULT = 0$
		$END$
	$END$
$END$

$ 
$ 各コアのスタックに関する指定
$ 
$CORE_SECTION_TSTACK[0]  = "__attribute__((section(\".pe1_lram\"),nocommon))"$
$CORE_SECTION_ISTACK[0]  = "__attribute__((section(\".pe1_lram\"),nocommon))"$

$CORE_SECTION_TSTACK[1]  = "__attribute__((section(\".pe2_lram\"),nocommon))"$
$CORE_SECTION_ISTACK[1]  = "__attribute__((section(\".pe2_lram\"),nocommon))"$


$ 
$ タスク単独のスタック
$ 
$FUNCTION GENERATE_TSKSTK$
	$tskid = ARGV[1]$
	static StackType _kernel_stack_$tskid$[COUNT_STK_T($TSK.STKSZ[tskid]$)] $CORE_SECTION_TSTACK[OSAP.CORE[TSK.OSAPID[tskid]]]$;$NL$
$END$

$ 
$ アイドルスケジュールドメインのシェアードスタック
$ 
$FUNCTION GENERATE_IDLE_SHARD_TSKSTK$
	$tskpri = ARGV[1]$
	$coreid = ARGV[2]$
	static StackType _kernel_shared_stack_core$coreid$_idle_$tskpri$[COUNT_STK_T($AT(shared_stack_size_idle[tskpri], coreid)$)] $CORE_SECTION_TSTACK[coreid]$;$NL$
$END$

$ 
$ アイドルスケジュールドメイン以外のシェアードスタック
$ 
$FUNCTION GENERATE_SHARD_TSKSTK$
	$tskpri = ARGV[1]$
	$osapid = ARGV[2]$
	static StackType _kernel_shared_stack_$osapid$_$tskpri$[COUNT_STK_T($AT(shared_stack_size_scd[tskpri], osapid)$)] $CORE_SECTION_TSTACK[OSAP.CORE[osapid]]$;$NL$
$END$

$ 
$ 非タスクコンテキスト用のスタック
$ 
$FUNCTION GENERATE_OSSTACK$
	$coreid = ARGV[1]$
	$size = ARGV[2]$
	static StackType	_kernel_core$coreid$_ostack[$FORMAT("COUNT_STK_T(0x%x)", +size)$] $CORE_SECTION_ISTACK[coreid]$;$NL$
$END$

$ 
$ 非タスクコンテキスト用のスタック
$ 
$FUNCTION GENERATE_OSSTACK_DEF$
	$coreid = ARGV[1]$
	static StackType	_kernel_core$coreid$_ostack[COUNT_STK_T($OSTK.STKSZ[OSTK.COREID[coreid]]$)] $CORE_SECTION_ISTACK[coreid]$;$NL$
$END$




$
$  プロセッサ依存テンプレートのインクルード
$
$INCLUDE "v850_gcc/prc.tf"$
