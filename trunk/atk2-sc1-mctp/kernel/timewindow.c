/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2000-2003 by Embedded and Real-Time Systems Laboratory
 *                              Toyohashi Univ. of Technology, JAPAN
 *  Copyright (C) 2004-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2004-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: timewindow.c 769 2017-03-03 06:41:08Z ertl-honda $
 */

/*
 *		timewindow制御モジュール
 */

#include "kernel_impl.h"
#include "osap.h"
#include "task.h"
#include "timewindow.h"
#include "interrupt.h"
#include "target_tp_timer.h"
#include "counter.h"

LOCAL_INLINE void change_timewindow(void);
LOCAL_INLINE void systemcycle_expire(void);
LOCAL_INLINE void timewindow_expire(void);

#ifdef TOPPERS_timewindow_initialize
/*
 *  タイムウィンドウの同期処理
 */   
void
sync_timewindow(CCB *p_ccb)
{
	CCB		*my_p_ccb = get_my_p_ccb();

	if(p_ccb->p_twinib_table[p_ccb->current_timewindow].syncid != 0) {
		release_tsk_lock(my_p_ccb);
		x_nested_unlock_os_int();
		target_sync_timewindow(p_ccb->p_twinib_table[p_ccb->current_timewindow].syncid);
		x_nested_lock_os_int();
		acquire_tsk_lock(my_p_ccb);
	}
}

/*
 *  タイムウィンドウ制御モジュール初期化処理
 */
void
timewindow_initialize(void)
{
	CCB			*p_ccb;
	p_ccb = get_my_p_ccb();

	target_initialize_systemcycle_timer();
	target_initialize_timewindow_timer();

	p_ccb->current_timewindow = 0U;
}

#endif /* TOPPERS_timewindow_initialize */

/*
 *  TP用タイマ開始処理
 */
#ifdef TOPPERS_start_tp_timer

void
start_tp_timer(void)
{
	CCB			*p_ccb;
	p_ccb = get_my_p_ccb();

	target_start_systemcycle_timer();
	target_start_timewindow_timer(p_ccb->p_twinib_table[p_ccb->current_timewindow].budget);

	/*
	 * 起動時のタイムウィンドウ呼び出し
	 */
	if(p_ccb->p_twinib_table[p_ccb->current_timewindow].CounterID < tnum_counter) {
		/* OS割込み禁止状態にし，全割込み禁止状態解除 */
		x_nested_lock_os_int();
		x_unlock_all_int();
		ENTER_CALLEVEL(TCL_ISR2);
		(void) incr_counter_action(p_ccb->p_twinib_table[p_ccb->current_timewindow].CounterID);
		LEAVE_CALLEVEL(TCL_ISR2);
		/* 元の割込みマスク優先度と全割込み禁止状態に */
		x_lock_all_int();
		x_nested_unlock_os_int();
	}
}

#endif /* TOPPERS_start_tp_timer */

/*
 *  TP用タイマ終了処理
 */
#ifdef TOPPERS_stop_tp_timer

void
stop_tp_timer(void)
{
	target_stop_systemcycle_timer();
	target_intclear_systemcycle_timer();
	target_stop_timewindow_timer();
	target_intclear_timewindow_timer();
}

#endif /* TOPPERS_stop_tp_timer */

/* タイムウィンドウ切り替え処理 */
void
change_timewindow(void)
{
	CCB		*my_p_ccb = get_my_p_ccb();

	my_p_ccb->p_curtwschedom = my_p_ccb->p_twinib_table[my_p_ccb->current_timewindow].p_scdcb;

	sync_timewindow(my_p_ccb);

	target_start_timewindow_timer(my_p_ccb->p_twinib_table[my_p_ccb->current_timewindow].budget);

	if(my_p_ccb->p_twinib_table[my_p_ccb->current_timewindow].CounterID < tnum_counter) {
		release_tsk_lock(my_p_ccb);
		(void) incr_counter_action(my_p_ccb->p_twinib_table[my_p_ccb->current_timewindow].CounterID);
		acquire_tsk_lock(my_p_ccb);
	}

	release_tsk_lock(my_p_ccb);
	x_nested_unlock_os_int();
	ici_handler_main(TRUE);
	x_nested_lock_os_int();
	acquire_tsk_lock(my_p_ccb);
}

/*
 *  システム周期満了処理
 */
void
systemcycle_expire(void)
{
	ISRType	i;
	ISRCB	*p_isrcb;
	CCB			*my_p_ccb = get_my_p_ccb();

	/* p_curtwschedmn がNULLでない場合，実行中のタイムウィンドウがあるということ */
	if (my_p_ccb->p_curtwschedom != my_p_ccb->p_idleschedom) {
		call_protectionhk_main(E_OS_PROTECTION_TIMEWINDOW);
	}

	/* 各C2ISRの割込み発生回数をクリア */
	for (i = 0U; i < tnum_isr2; i++) {
		p_isrcb = p_isrcb_table[i];
		p_isrcb->occurrence_count = p_isrcb->overrun_count;
		p_isrcb->overrun_count = 0U;
	}

	/* 割込みクリア */
	target_intclear_systemcycle_timer();

	my_p_ccb->current_timewindow = 0U;
	change_timewindow();

	update_tschedtsk(my_p_ccb);
}

/*
 *  タイムウィンドウ切り替え処理
 */
void
timewindow_expire(void)
{
	CCB			*my_p_ccb = get_my_p_ccb();
	CoreIdType	coreid = x_core_id();

	/* 割込みクリア */
	target_intclear_timewindow_timer();

	/* タイムウィンドウを更新 */
	my_p_ccb->current_timewindow++;
	if (my_p_ccb->current_timewindow < tnum_timewindow_table[coreid]) {
		change_timewindow();
	}
	/* タイムウィンドウの総量を超えたらアイドルウィンドウとする */
	else {
		my_p_ccb->p_curtwschedom = my_p_ccb->p_idleschedom;
		target_stop_timewindow_timer();
	}

	update_tschedtsk(my_p_ccb);
}

/*
 *  TP用割込みハンドラtw_interruptから呼び出されるハンドラ
 *  割込み番号によって，タイムウィンドウ満了処理とシステム周期満了処理を呼び出す
 *  呼び出される状態は次の通り
 *  ・OS割込み禁止
 *  ・
 */
#ifdef TOPPERS_tp_int_handler

void
tp_int_handler(uint32 intno)
{
	CCB		*my_p_ccb = get_my_p_ccb();

	acquire_tsk_lock(my_p_ccb);
	if (target_is_timewindow_intno(intno)) {
		timewindow_expire();
	}
	/*
	 * 引数intnoはTIMEWINDOWTIMER_INTNOもしくは
	 * SYSTEMCYCLETIMER_INTNOしか受け取らないためelseとする
	 */
	else {
		systemcycle_expire();
	}
	release_tsk_lock(my_p_ccb);
}
#endif /* TOPPERS_tp_int_handler */
