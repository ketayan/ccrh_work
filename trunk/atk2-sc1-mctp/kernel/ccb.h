/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2015 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2012-2015 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2012-2013 by Spansion LLC, USA
 *  Copyright (C) 2012-2015 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2012-2015 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2012-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2012-2015 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2012-2015 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2012-2015 by Witz Corporation
 *  Copyright (C) 2014-2015 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2015 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2015 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: ccb.h 769 2017-03-03 06:41:08Z ertl-honda $
 */

/*
 *  コア管理ブロック
 */
#ifndef TOPPERS_CCB_H
#define TOPPERS_CCB_H

#include "target_ccb.h"

#ifndef TOPPERS_MACRO_ONLY

#include "queue.h"
#include "spinlock.h"

/*
 *  前方参照
 */
typedef struct task_control_block					TCB;
typedef struct isr_control_block					ISRCB;
typedef struct os_application_initialization_block	OSAPINIB;
typedef struct timewindow_initialization_block		TWINIB;
typedef struct scheduling_domain_control_block		SCDCB;

/*
 *		コア管理ブロック
 */
struct core_control_block {
	CoreIdType coreid;

	/*
	 *  カーネル動作状態フラグ
	 */
	boolean kerflg;

	/*
	 *  SuspendAllInterrupts のネスト回数
	 */
	uint8 sus_all_cnt;

	/*
	 *  SuspendOSInterrupts のネスト回数
	 */
	uint8 sus_os_cnt;

	/*
	 *  OS実行制御のための変数
	 */
	uint16 callevel_stat;       /* 実行中のコンテキスト */

#if TTYPE_KLOCK == C_KLOCK
	/*
	 *  コアロック変数
	 */
	LockType	tsk_lock;
	LockType	cnt_lock;
#endif /* TTYPE_KLOCK == C_KLOCK */

	/*
	 *		タスク関連
	 */

	/*
	 *  実行状態のタスク
	 *
	 *  実行状態のタスク（＝プロセッサがコンテキストを持っているタスク）の
	 *  TCBを指すポインタ．実行状態のタスクがない場合はNULLにする．
	 *
	 *  サービスコールの処理中で，自タスク（サービスコールを呼び出したタス
	 *  ク）に関する情報を参照する場合はp_runtskを使う．p_runtskを書き換え
	 *  るのは，ディスパッチャ（と初期化処理）のみである．
	 */
	TCB *p_runtsk;

	/*
	 *		割込み関連
	 */
	/*
	 *  実行中のC2ISR
	 *
	 *  C2ISRを実行していない時は，ISRID_NULL にする．
	 */
	ISRCB *p_runisr;

	/*
	 *  wrap_sus_all_int のネスト回数
	 */
	uint8 wrap_sus_all_cnt;

	/*
	 *  wrap_sus_os_int のネスト回数
	 */
	uint8 wrap_sus_os_cnt;

	/*
	 *  エラーフックに渡す情報を格納する変数
	 */
	OSServiceIdType	_errorhook_svcid;
	_ErrorHook_Par	_errorhook_par1;
	_ErrorHook_Par	_errorhook_par2;
	_ErrorHook_Par	_errorhook_par3;
	_ErrorHook_Par	errorhook_par1;
	_ErrorHook_Par	errorhook_par2;
	_ErrorHook_Par	errorhook_par3;

	/*
	 *  ユーザ定義コア間割込み要求ビットマップ
	 */
	uint32 ici_request_map;

	/*
	 *  コア間割込み許可ビットマスク
	 */
	uint32 ici_bit_mask;

	/*
	 *  ディスパッチ用コア間割込み要求フラグ
	 */
	boolean ici_disreqflg;

	/*
	 *  実行中のOSアプリケーション
	 */
	const OSAPINIB *p_runosap;

	/*
	 *  ターゲット依存 コアコントロールブロック
	 */
	TCCB target_ccb;

	/*
	 * プロテクションフック用スピンロック管理ブロックポインタ
	 */
	SPNCB *p_protectspncb;

	boolean					undispatch_flag;
	TCB 					*p_tschedtsk;
	const TWINIB			*p_twinib_table;     /* （Os_Lcfg.c） */

	/*
	 * 現在実行中のスケジューリングドメイン
	 */
	SCDCB	*p_runschedom;

	/*
	 * 現在実行中のタイムウィンドウに対応したスケジューリングドメイン
	 */
	SCDCB					*p_curtwschedom;

	/*
	 * アイドルウィンドウに対応したスケジューリングドメイン
	 */
	SCDCB	*p_idleschedom;

	 /*
	  *  現在実行中のタイムウィンドウ
	  */
	uint32	current_timewindow;
};

/*
 *  CCBへのアクセステーブル（Os_Lcfg.c）
 */
extern CCB * const p_ccb_table[];


#ifndef OMIT_GET_MY_P_CCB
/*
 *  自コアの CCBアドレス取得関数
 */
LOCAL_INLINE CCB *
get_my_p_ccb(void)
{
	return(p_ccb_table[x_core_id()]);
}
#endif /* OMIT_GET_MY_P_CCB */

#endif /* TOPPERS_MACRO_ONLY */

#endif /* TOPPERS_CCB_H */
