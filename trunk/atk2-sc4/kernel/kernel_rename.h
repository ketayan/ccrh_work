/* This file is generated from kernel_rename.def by genrename. */

#ifndef TOPPERS_KERNEL_RENAME_H
#define TOPPERS_KERNEL_RENAME_H

/*
 *  alarm.c
 */
#define alarm_initialize			kernel_alarm_initialize
#define alarm_expire				kernel_alarm_expire
#define force_term_osap_alarm		kernel_force_term_osap_alarm

/*
 *  counter.c
 */
#define insert_cnt_expr_que			kernel_insert_cnt_expr_que
#define delete_cnt_expr_que			kernel_delete_cnt_expr_que
#define counter_initialize			kernel_counter_initialize
#define counter_terminate			kernel_counter_terminate
#define get_reltick					kernel_get_reltick
#define get_abstick					kernel_get_abstick
#define expire_process				kernel_expire_process
#define force_term_osap_counter		kernel_force_term_osap_counter

/*
 *  counter_manage.c
 */
#define notify_hardware_counter		kernel_notify_hardware_counter
#define incr_counter_process		kernel_incr_counter_process
#define incr_counter_action			kernel_incr_counter_action

/*
 *  event.c
 */
#define set_event_action			kernel_set_event_action

/*
 *  interrupt.c
 */
#define interrupt_initialize		kernel_interrupt_initialize
#define release_interrupts			kernel_release_interrupts
#define exit_isr2					kernel_exit_isr2

/*
 *  ioc_manage.c
 */
#define ioc_initialize				kernel_ioc_initialize

/*
 *  memory.c
 */
#define check_address_stack			kernel_check_address_stack
#define search_meminib				kernel_search_meminib
#define check_osap_memory			kernel_check_osap_memory
#define check_isr_memory			kernel_check_isr_memory
#define check_task_memory			kernel_check_task_memory
#define probe_memory_access			kernel_probe_memory_access
#define probe_memory_read			kernel_probe_memory_read
#define probe_memory_write			kernel_probe_memory_write
#define probe_memory_read_write		kernel_probe_memory_read_write
#define initialize_sections			kernel_initialize_sections

/*
 *  osap.c
 */
#define osap_initialize				kernel_osap_initialize
#define force_term_osap_main		kernel_force_term_osap_main
#define force_term_osap				kernel_force_term_osap

/*
 *  osctl.c
 */
#define internal_call_errorhook		kernel_internal_call_errorhook
#define call_posttaskhook			kernel_call_posttaskhook
#define call_pretaskhook			kernel_call_pretaskhook
#define init_stack_magic_region		kernel_init_stack_magic_region
#define call_protectionhk_main		kernel_call_protectionhk_main
#define internal_shutdownos			kernel_internal_shutdownos
#define internal_call_shtdwnhk		kernel_internal_call_shtdwnhk

/*
 *  osctl_manage.c
 */
#define callevel_stat				kernel_callevel_stat
#define appmodeid					kernel_appmodeid
#define kerflg						kernel_kerflg
#define run_trusted					kernel_run_trusted
#define pre_protection_supervised	kernel_pre_protection_supervised
#define fatal_file_name				kernel_fatal_file_name
#define fatal_line_num				kernel_fatal_line_num

/*
 *  resource.c
 */
#define resource_initialize			kernel_resource_initialize

/*
 *  scheduletable.c
 */
#define schtbl_initialize			kernel_schtbl_initialize
#define schtbl_expire				kernel_schtbl_expire
#define schtbl_expiry_process		kernel_schtbl_expiry_process
#define schtbl_head					kernel_schtbl_head
#define schtbl_exppoint_process		kernel_schtbl_exppoint_process
#define schtbl_tail					kernel_schtbl_tail
#define force_term_osap_schtbl		kernel_force_term_osap_schtbl

/*
 * svc_table.c
 */
#define svc_table					kernel_svc_table

/*
 *  task.c
 */
#define p_runtsk					kernel_p_runtsk
#define p_schedtsk					kernel_p_schedtsk
#define nextpri						kernel_nextpri
#define ready_queue					kernel_ready_queue
#define ready_primap				kernel_ready_primap
#define task_initialize				kernel_task_initialize
#define search_schedtsk				kernel_search_schedtsk
#define make_runnable				kernel_make_runnable
#define make_non_runnable			kernel_make_non_runnable
#define make_active					kernel_make_active
#define preempt						kernel_preempt
#define suspend						kernel_suspend
#define release_taskresources		kernel_release_taskresources
#define exit_task					kernel_exit_task
#define remove_task_from_queue		kernel_remove_task_from_queue
#define force_terminate_task		kernel_force_terminate_task
#define force_term_osap_task		kernel_force_term_osap_task
#define move_schedtsk				kernel_move_schedtsk

/*
 *  task_manage.c
 */
#define activate_task_action		kernel_activate_task_action

/*
 *  timingprotection.c
 */
#define is_tp_timer_running			kernel_is_tp_timer_running
#define tp_time_count				kernel_tp_time_count
#define tp_initialize				kernel_tp_initialize
#define tp_terminate				kernel_tp_terminate
#define tp_start_timer				kernel_tp_start_timer
#define tp_stop_timer				kernel_tp_stop_timer
#define tp_stop_task_monitor		kernel_tp_stop_task_monitor
#define tp_get_current_time			kernel_tp_get_current_time
#define tp_check_arrival_time		kernel_tp_check_arrival_time
#define tp_fault_handler			kernel_tp_fault_handler
#define tp_timer_handler			kernel_tp_timer_handler

/*
 *  kernel_mem.c	Os_Lcfg.c
 */
#define tnum_meminib				kernel_tnum_meminib
#define memtop_table				kernel_memtop_table
#define meminib_table				kernel_meminib_table
#define tnum_datasec				kernel_tnum_datasec
#define datasecinib_table			kernel_datasecinib_table
#define tnum_bsssec					kernel_tnum_bsssec
#define bsssecinib_table			kernel_bsssecinib_table
#define shared_region				kernel_shared_region
#define tnum_shared_region			kernel_tnum_shared_region
#define tinib_table					kernel_tinib_table
#define osapinib_table				kernel_osapinib_table
#define osapcb_table				kernel_osapcb_table
#define tnum_alarm					kernel_tnum_alarm
#define tnum_counter				kernel_tnum_counter
#define tnum_hardcounter			kernel_tnum_hardcounter
#define tnum_isr2					kernel_tnum_isr2
#define tnum_stdresource			kernel_tnum_stdresource
#define tnum_task					kernel_tnum_task
#define tnum_task_inc_rt			kernel_tnum_task_inc_rt
#define tnum_exttask				kernel_tnum_exttask
#define tnum_appmode				kernel_tnum_appmode
#define tnum_scheduletable			kernel_tnum_scheduletable
#define tnum_implscheduletable		kernel_tnum_implscheduletable
#define tnum_tfn					kernel_tnum_tfn
#define tnum_osap					kernel_tnum_osap
#define intpri_tp					kernel_intpri_tp
#define tcb_table					kernel_tcb_table
#define cntinib_table				kernel_cntinib_table
#define cntcb_table					kernel_cntcb_table
#define hwcntinib_table				kernel_hwcntinib_table
#define alminib_table				kernel_alminib_table
#define almcb_table					kernel_almcb_table
#define schtblinib_table			kernel_schtblinib_table
#define schtblcb_table				kernel_schtblcb_table
#define resinib_table				kernel_resinib_table
#define rescb_table					kernel_rescb_table
#define object_initialize			kernel_object_initialize
#define object_terminate			kernel_object_terminate
#define tnum_intno					kernel_tnum_intno
#define intinib_table				kernel_intinib_table
#define isrinib_table				kernel_isrinib_table
#define isrcb_table					kernel_isrcb_table
#define tfinib_table				kernel_tfinib_table
#define ostk						kernel_ostk
#define ostksz						kernel_ostksz
#define ostkpt						kernel_ostkpt
#define tnum_ioc					kernel_tnum_ioc
#define tnum_queueioc				kernel_tnum_queueioc
#define tnum_ioc_wrapper_send		kernel_tnum_ioc_wrapper_send
#define tnum_ioc_wrapper			kernel_tnum_ioc_wrapper
#define ioc_inival_table			kernel_ioc_inival_table
#define ioccb_table					kernel_ioccb_table
#define iocinib_table				kernel_iocinib_table
#define iocwrpinib_table			kernel_iocwrpinib_table
#define appid_str					kernel_appid_str
#define tskid_str					kernel_tskid_str
#define isrid_str					kernel_isrid_str
#define cntid_str					kernel_cntid_str
#define almid_str					kernel_almid_str
#define resid_str					kernel_resid_str
#define schtblid_str				kernel_schtblid_str
#define evtid_str					kernel_evtid_str
#define osapid_str					kernel_osapid_str
#define iocid_str					kernel_iocid_str
#define tfnid_str					kernel_tfnid_str



#include "target_rename.h"

#endif /* TOPPERS_KERNEL_RENAME_H */
