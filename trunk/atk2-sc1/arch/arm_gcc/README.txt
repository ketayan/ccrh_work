
		TOPPERS/ATK2-SC1
        ＜ARM依存部 ディレクトリ解説＞

本ドキュメントは，本ディレクトリ(./arch/arm_gcc）の情報を記述したものである．

----------------------------------------------------------------------
TOPPERS ATK2
    Toyohashi Open Platform for Embedded Real-Time Systems
    Automotive Kernel Version 2

Copyright (C) 2011-2017 by Center for Embedded Computing Systems
            Graduate School of Information Science, Nagoya Univ., JAPAN
Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
Copyright (C) 2011-2013 by Spansion LLC, USA
Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
Copyright (C) 2011-2017 by Witz Corporation
Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
Copyright (C) 2015-2017 by SUZUKI MOTOR CORPORATION

上記著作権者は，以下の (1)〜(3)の条件を満たす場合に限り，本ドキュメ
ント（本ドキュメントを改変したものを含む．以下同じ）を使用・複製・改
変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
(1) 本ドキュメントを利用する場合には，上記の著作権表示，この利用条件
    および下記の無保証規定が，そのままの形でドキュメント中に含まれて
    いること．
(2) 本ドキュメントを改変する場合には，ドキュメントを改変した旨の記述
    を，改変後のドキュメント中に含めること．ただし，改変後のドキュメ
    ントが，TOPPERSプロジェクト指定の開発成果物である場合には，この限
    りではない．
(3) 本ドキュメントの利用により直接的または間接的に生じるいかなる損害
    からも，上記著作権者およびTOPPERSプロジェクトを免責すること．また，
    本ドキュメントのユーザまたはエンドユーザからのいかなる理由に基づ
    く請求からも，上記著作権者およびTOPPERSプロジェクトを免責すること．

本ドキュメントは，AUTOSAR（AUTomotive Open System ARchitecture）仕様
に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するものではな
い．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利用する
者に対して，AUTOSARパートナーになることを求めている．

本ドキュメントは，無保証で提供されているものである．上記著作権者およ
びTOPPERSプロジェクトは，本ドキュメントに関して，特定の使用目的に対す
る適合性も含めて，いかなる保証も行わない．また，本ドキュメントの利用
により直接的または間接的に生じたいかなる損害に関しても，その責任を負
わない．

$Id: README.txt 727 2017-01-23 09:27:59Z witz-itoyo $
----------------------------------------------------------------------

本ディレクトリ（./arch/arm_gcc）は，ARMコア及び，ARMコアを用いたチップ
に依存した記述を置くためのディレクトリである．

ターゲットディレクトリ（./target）との切り分けは，本ディレクトリは，特
定のボードに依存しない再利用可能な記述を置こととし，ターゲットディレク
トリは再利用の可能性が低いボードに依存した記述を置くこととする．

再利用可能な範囲としては，ARMコアを用いたチップをまでを想定している．
そのため，本ディレクトリに置く依存部をチップ依存部と呼ぶ．各チップ依存
部はチップ名でディレクトリを作成し，その下に関連するファイルを置く．

一方，ARMコアに依存した記述は，コア依存部と呼び，commonディレクトリ以
下に置く．各チップ依存部は必要に応じてコア依存部のファイルを使用する．
コア依存部に関しては，common/core_user.txt を参照のこと．

チップによっては，コア依存部と同等の機能を持たない場合もある．その場合
は，チップ依存部を作成せず，ターゲット依存部とコア依存部のファイルを組
み合わせればよい．
