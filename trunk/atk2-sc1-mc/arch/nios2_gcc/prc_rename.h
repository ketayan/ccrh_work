/* This file is generated from prc_rename.def by genrename. */

#ifndef TOPPERS_PRC_RENAME_H
#define TOPPERS_PRC_RENAME_H

/*
 *  prc_config.c
 */
#define prc_hardware_initialize		kernel_prc_hardware_initialize
#define prc_initialize				kernel_prc_initialize
#define prc_terminate				kernel_prc_terminate
#define x_config_int				kernel_x_config_int
#define default_int_handler			kernel_default_int_handler

/*
 *  Os_Lcfg.c
 */
#define tmin_status_il				kernel_tmin_status_il
#define isr_tbl						kernel_isr_tbl
#define isr_p_isrcb_tbl				kernel_isr_p_isrcb_tbl
#define ostkpt_table				kernel_ostkpt_table

/*
 *  prc_support.S
 */
#define exception_entry				kernel_exception_entry
#define interrupt					kernel_interrupt
#define dispatch					kernel_dispatch
#define start_dispatch				kernel_start_dispatch
#define exit_and_dispatch_nohook	kernel_exit_and_dispatch_nohook
#define start_r						kernel_start_r
#define stack_change_and_call_func_1	kernel_stack_change_and_call_func_1
#define stack_change_and_call_func_2	kernel_stack_change_and_call_func_2


#endif /* TOPPERS_PRC_RENAME_H */
