/*
 *  TOPPERS ATK2
 *      Toyohashi Open Platform for Embedded Real-Time Systems
 *      Automotive Kernel Version 2
 *
 *  Copyright (C) 2008-2017 by Center for Embedded Computing Systems
 *              Graduate School of Information Science, Nagoya Univ., JAPAN
 *  Copyright (C) 2011-2017 by FUJI SOFT INCORPORATED, JAPAN
 *  Copyright (C) 2011-2013 by Spansion LLC, USA
 *  Copyright (C) 2011-2017 by NEC Communication Systems, Ltd., JAPAN
 *  Copyright (C) 2011-2016 by Panasonic Advanced Technology Development Co., Ltd., JAPAN
 *  Copyright (C) 2011-2014 by Renesas Electronics Corporation, JAPAN
 *  Copyright (C) 2011-2016 by Sunny Giken Inc., JAPAN
 *  Copyright (C) 2011-2017 by TOSHIBA CORPORATION, JAPAN
 *  Copyright (C) 2011-2017 by Witz Corporation
 *  Copyright (C) 2014-2016 by AISIN COMCRUISE Co., Ltd., JAPAN
 *  Copyright (C) 2014-2016 by eSOL Co.,Ltd., JAPAN
 *  Copyright (C) 2014-2017 by SCSK Corporation, JAPAN
 *  Copyright (C) 2015-2017 by SUZUKI MOTOR CORPORATION
 *
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 *
 *  本ソフトウェアは，AUTOSAR（AUTomotive Open System ARchitecture）仕
 *  様に基づいている．上記の許諾は，AUTOSARの知的財産権を許諾するもので
 *  はない．AUTOSARは，AUTOSAR仕様に基づいたソフトウェアを商用目的で利
 *  用する者に対して，AUTOSARパートナーになることを求めている．
 *
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 *
 *  $Id: nios2.h 727 2017-01-23 09:27:59Z witz-itoyo $
 */

/*
 *		Nios2のハードウェア資源の定義
 */

#ifndef TOPPERS_NIOS2_H
#define TOPPERS_NIOS2_H

/*
 *  statusレジスタのビットフィールドのマスク
 */
#define STATUS_U	UINT_C(0x02)
#define STATUS_PIE	UINT_C(0x01)
#define STATUS_IL	UINT_C(0x3f0)

/*
 *  statusレジスタのビットフィールドのオフセット
 */
#define STATUS_IL_OFFSET	(uint32) 4


/*
 * SYSID
 */
#define SYSVER_REG1	(SYSVER_BASE + UINT_C(0x00))
#define SYSVER_REG2	(SYSVER_BASE + UINT_C(0x04))
#define SYSVER_REG3	(SYSVER_BASE + UINT_C(0x08))
#define SYSVER_REG4	(SYSVER_BASE + UINT_C(0x0C))
#define SYSVER_REG5	(SYSVER_BASE + UINT_C(0x10))
#define SYSVER_REG6	(SYSVER_BASE + UINT_C(0x14))
#define SYSVER_REG7	(SYSVER_BASE + UINT_C(0x18))
#define SYSVER_REG8	(SYSVER_BASE + UINT_C(0x1C))
#define MAGIC_START	UINT_C(0x87654321) /* 同期用のマジックナンバー */

/*
 *	Mutex関連
 */

#define MUTEX_MUTEX_OFFSET	UINT_C(0x00)
#define MUTEX_RESET_OFFSET	UINT_C(0x04)

#define MUTEX_MUTEX_OWNER_MASK		UINT_C(0xffff0000)
#define MUTEX_MUTEX_VALUE_MASK		UINT_C(0x0000ffff)
#define MUTEX_MUTEX_OWNER_OFFSET	UINT_C(16)

#define MUTEX_RESET_RESET_MASK		UINT_C(0x01)
#define MUTEX_VALUE_DATA			UINT_C(0x1234)

#ifndef TOPPERS_MACRO_ONLY
#include "prc_insn.h"
#include "prc_sil.h"

/*
 *  コアID（0オリジン）の取得
 *  合成時にCCBアドレスが設定されている．
 */
LOCAL_INLINE CoreIdType
x_core_id(void)
{
	return((CoreIdType) ((current_cpuid() >> 20) & 0xfU));
}

/*
 *  Mutexの初期化
 */
LOCAL_INLINE void
nios2_mutex_init_lock(uint32 base)
{
	uint32	data;
	uint32	check;

	check = sil_rew_iop((void *) (base + MUTEX_RESET_OFFSET));

	if (check == MUTEX_RESET_RESET_MASK) {
		sil_wrw_iop((void *) (base + MUTEX_RESET_OFFSET), MUTEX_RESET_RESET_MASK);
	}
	else {
		data = sil_rew_iop((void *) (base + MUTEX_MUTEX_OFFSET));
		sil_wrw_iop((void *) (base + MUTEX_MUTEX_OFFSET), (data & MUTEX_MUTEX_OWNER_MASK));
	}
}

/*
 *  Mutexの取得
 *  取得できればtrueを取得できなければfalseを返す
 */
LOCAL_INLINE boolean
nios2_mutex_try_lock(uint32 base, CoreIdType coreid)
{
	uint32	data;
	uint32	check;

	data = ((uint32) coreid << MUTEX_MUTEX_OWNER_OFFSET) | MUTEX_VALUE_DATA;

	sil_wrw_iop((void *) (base + MUTEX_MUTEX_OFFSET), data);
	check = sil_rew_iop((void *) (base + MUTEX_MUTEX_OFFSET));

	return((data == check) ? TRUE : FALSE);
}

/*
 *  Mutexの解放
 */
LOCAL_INLINE void
nios2_mutex_release_lock(uint32 base, CoreIdType coreid)
{
	sil_wrw_iop((void *) (base + MUTEX_MUTEX_OFFSET), ((uint32) coreid << MUTEX_MUTEX_OWNER_OFFSET));
}

#endif /* TOPPERS_MACRO_ONLY */


#endif /* TOPPERS_NIOS2_H */
