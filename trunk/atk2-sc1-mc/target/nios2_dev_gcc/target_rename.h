/* This file is generated from target_rename.def by genrename. */

#ifndef TOPPERS_TARGET_RENAME_H
#define TOPPERS_TARGET_RENAME_H

/*
 *  target_config.c
 */
#define target_ici_intno_table		kernel_target_ici_intno_table
#define target_ici_intpri_table		kernel_target_ici_intpri_table
#define target_core_int_base_table	kernel_target_core_int_base_table
#define target_tsk_lock_table		kernel_target_tsk_lock_table
#define target_cnt_lock_table		kernel_target_cnt_lock_table
#define target_spn_lock_table		kernel_target_spn_lock_table
#define target_hardware_initialize	kernel_target_hardware_initialize
#define target_initialize			kernel_target_initialize
#define target_exit					kernel_target_exit
#define target_is_int_controllable	kernel_target_is_int_controllable

/*
 *  trace_dump.c
 */
#define trace_dump					kernel_trace_dump

/*
 *  trace_config.c
 */
#define log_dsp_enter				kernel_log_dsp_enter
#define log_dsp_leave				kernel_log_dsp_leave


#include "prc_rename.h"

#endif /* TOPPERS_TARGET_RENAME_H */
